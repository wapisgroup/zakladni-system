lang_editace_polozky = 'Podrobnosti';
/* pro meni item */
Element.implement({
	depth: function(contain){		
		depth = 0;
		if(this != contain){
			if(this.getTag() != contain.getTag())
				depth++;
			depth += this.getParent().depth(contain);
		}	
 
		return depth;
	},
	
	getUlItems:function(){
		if (this.nodeName != 'UL') alert('Tato metoda lze pouze na UL element');
		var items = [];
		var list = this.getElements('li');
		list.each(function(item){
			if (this == item.parentNode){
				items[items.length] = item;
			}
		},this);
		return items;
	},
	
	getIndex:function(){
		var items = [], list;
		if (this.nodeName != 'LI') alert('Tato metoda lze pouze na LI element');
		list = this.parentNode.getElements('li');
		list.each(function(item,key){
			if (this.parentNode == item.parentNode)
				items[items.length] = item.id
		},this);
		
		if (this.id != undefined)
			return items.indexOf(this.id);
		else 
			return items.length;
	},
	
	getLiText: function(){
		if (this.nodeName != 'LI') alert('Tato metoda lze pouze na LI element');
		var p = this.firstChild;
		if (p.nodeType == 3)
			return p
		else 
			return false;
	},
	
	getParentElements: function(node,endNode){
		var pnode = this, output = [];
		while(pnode.parentNode && pnode.parentNode != endNode)
			pnode = pnode.parentNode;
			if (pnode.nodeName == node)
				output[output.length] = pnode;
		return output;
	},
	
	center: function(){
		/* velikost alert okna */
		var OH = this.getSize().y;
		var OW = this.getSize().x;

			
		/* velikosti window */
		var WH = window.getHeight();
		var WW = window.getWidth();
			
		var top = (WH/2 - OH/2)+window.getScrollTop();
			
		this.setStyles({
			'top'	: 	(top>0)?top:0,
			'left'	:	((WW/2 - OW/2)+window.getScrollLeft())
		});
			
		return this;
	}

});
Tree = new Class({
	Implements: [Events, Options],
	options: {
		depth: 3,
		icon_normal	: '/css/fastest/icons/menu/normal.gif',
		icon_plus	: '/css/fastest/icons/menu/plus.gif',
		icon_minus	: '/css/fastest/icons/menu/minus.gif',
		icon_drag	: '/css/fastest/icons/menu/drag.gif',
		prefix		: 'menu_item',
		modelClass	: 'MenuItem',
		loadUrl		: '/menu_items/get_json_index',
		controller	: 'menu_items',
		permission	: {
			'edit'		: 	1,
			'status'	:	1,
			'delete'	:	1
		},
		defined_lang: ['cz']
	},
	show_context_div: false,
	jsonMenu : '',
	
	initialize: function(list,options) {
		this.setOptions(options);
		this.list = $(list);	
		new Asset.css(this.options.css);
		this.modifeLi = this.modifeLi.bind(this);
		this.loadUrl();
		this.createContext('context_menu');
	},
	
	createLi: function(json){
		var item;
		if (json.parent_id == 0)
			ul = this.list;
		else {
			var t = $(this.options.prefix + json.parent_id);
			if (t){
				if (t.getElement('ul'))
					ul = t.getElement('ul')
				else
					ul = new Element('ul').inject(t);
			} else 
				ul = this.list;
		}
		
		item = new Element('li')
			.appendText(json.name)
			.setProperties({
				'id' : this.options.prefix + json.id
			})
			.inject(ul);
		if (json.status == 0)
			item.addClass('inactive');
	},
	
	createMenu: function(){
		var menu = this.jsonMenu;
		menu.each(function(item){
			this.createLi(item[this.options.modelClass]);
		},this);
		
		this.list.getElements('li').each(function(item,index){
			this.modifeLi(item);
		},this);
		//document.preloader.remove('menu_data_inizialize');
	},
	
	loadUrl: function(url){
		preloader(true);
		new Request.JSON({
			'url' 		: 	this.options.loadUrl,
			onComplete	:	(function(json){
				this.jsonMenu = json;
				this.createMenu();
				preloader(false);
			}).bind(this)
		}).send();
		
	},
	
	nodeOpenClose: function(e,item){
		var ul = item.getElement('ul');
		var img = item.getElement('img');
		if (ul){
			if (ul.getStyle('display') == 'none'){
				ul.setStyle('display','block');
				img.setProperty('src',this.options.icon_minus);
			} else {
				ul.setStyle('display','none');
				img.setProperty('src',this.options.icon_plus);
			}
		}
	},
	
	modifeLi:function(item){
		var input, tmp = item.getLiText();
		input = new Element('input',{type:'input','value':tmp.nodeValue,readonly:'readonly'})
			.addClass('menuinput')
			.addEvent('mouseover',function(){this.addClass('menuinputhover')})
			.addEvent('mouseout',function(){this.removeClass('menuinputhover')})
			.addEvent('click',function(){
				this.removeProperty('readonly');
				if (!this.old_text)
					this.old_text = this.value;
				this.addClass('menuinputclick')
			})
			.addEvent('blur',function(){
				if (window.ie)
					this.old_text = null;
				else
					delete this.old_text;	
				this.setProperty('readonly','readonly')
				this.removeClass('menuinputclick')
			})
			.addEvent('keyup',function(event){
				var event = new Event(event);	
    			if(event.key == 'esc'){
					this.value = this.old_text;
					this.blur();
				}
				if(event.key == 'enter'){
					this.blur();
				}
			})
			.addEvent('blur',(function(e){
				thisNode = new Event(e).target;
				preloader(true);
				url = "/" + this.options.controller + "/tree_menu/change_menu_item/" + thisNode.parentNode.id.substring(this.options.prefix.length) + '/';
				new Request.JSON({
					url			:	url,
					data		:  {'menu_name': thisNode.value},
					onComplete	: 	function(json){
						preloader(false);
					}
				}).send()
			}).bind(this))
			.injectTop(item);
		if (item.hasClass('inactive')){
			input.addClass('inactive');
			item.removeClass('inactive');
		}
		
			
		item.removeChild(tmp);
		delete tmp;
				
		// pridani ikon
		//new Element('img',{src:this.options.icon_drag})
		//	.setStyle('cursor','move')
		//	.injectTop(item);
		new Element('a',{'class':'move_'})
			.setStyle('cursor','move')
			.setHTML('&nbsp;X&nbsp;')
			.injectTop(item);
			
		if (item.getElement('ul')){
			new Element('img',{src:this.options.icon_plus,'style':'cursor:pointer'})
				.addEvent('click',this.nodeOpenClose.bindWithEvent(this,item))
				.injectTop(item);
			item.getElement('ul').setStyle('display','none');
		} else
			new Element('img',{src:this.options.icon_normal})
				.addEvent('click',this.nodeOpenClose.bindWithEvent(this,item))
				.injectTop(item);
		
		new Element('div',{'class':'context_button'})
			.addEvent('click',this.openContext.bindWithEvent(this))
			.setStyle('display','none')
			.injectTop(item);
				
		// li hover (show context_menu button);	
		item.addEvent('mouseover',(function(e,item){
			if (this.show_context_div == false){
				item.getElement('div').setStyle('display','block');
				this.show_context_div = true;
			}
			}).bindWithEvent(this,item));
		item.addEvent('mouseout',(function(e,item){
			this.show_context_div = false;
			item.getElement('div').setStyle('display','none')
		}).bindWithEvent(this,item));
			
		// drag
		//if (!Browser.Engine.trident){
		new DragTree(item,{
			object	:	this, 
		//	handle	:	item.getElement('a'),
			contain	: 	'contain', 
			marker	: 	'marker', 
			depth	: 	{minimum: 1,maximum:this.options.depth}
		});
	//	}
		
	},
	
	createContext: function(id){
		document.addEvent('click',this.closeContext.bindWithEvent(this));
		this.contextmenu = $(id);
		this.contextmenu.setStyles({
			'display'	:	'none',
			'position'	:	'absolute',
			'z-index'	:	1111111111
		});
	},
	
	openContext: function(e){
		var event = new Event(e);
		var li = event.target.parentNode;
		this.selectedMenu = li.id;
		this.contextmenu.setStyle('display','block');
		this.contextmenu.setStyles({
			'top'		:	event.client.y -140 ,
			'left'		:	event.client.x - 3
		});
		// orezani menu pro dane situace
		if (li.getElement('input').hasClass('inactive')){
			$(this.contextmenu).getElement('a[class=active]').getParent().setStyle('display','block');
			$(this.contextmenu).getElement('a[class=deactive]').getParent().setStyle('display','none');
		} else {
			$(this.contextmenu).getElement('a[class=active]').getParent().setStyle('display','none');
			$(this.contextmenu).getElement('a[class=deactive]').getParent().setStyle('display','block');
		}
		//if (li.depth(this.list) >= this.options.depth || li.id == "menuitem1")
		if (li.depth(this.list) >= this.options.depth || li.id == "menuitem2")
			$(this.contextmenu).getElement('a[class=copy]').getParent().setStyle('display','none');
		else 
			$(this.contextmenu).getElement('a[class=copy]').getParent().setStyle('display','block');
		if (li.id == "menuitem1" || li.id == "menuitem2" || (li.getElement('ul') && li.getElement('ul').getElement('li')))
			$(this.contextmenu).getElement('a[class=delete]').getParent().setStyle('display','none');
		else
			$(this.contextmenu).getElement('a[class=delete]').getParent().setStyle('display','block');
	},
	
	closeContext: function(e){
		var event = new Event(e);
		if(!$(event.target).hasClass('context_button'))
			$(this.contextmenu).setStyle('display','none');
	},
	
	// operation
	detail: function(){
		domwin.newWindow({
			id			: 'domwin',
			sizes		: [750,400],
			scrollbars	: 'true',
			defined_lang: this.options.defined_lang,
			languages	: true,
			title		: lang_editace_polozky,
			ajax_url	: "/" + this.options.controller + "/edit/" + this.selectedMenu.substring(this.options.prefix.length) + '?item_id=' + this.selectedMenu,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		});
	},
	
	new_page: function(root){
		var ul;
		if (root == true){
			ul = this.list;
		} else {
			var li = $(this.selectedMenu);
			if (li.getElement('ul')){
				ul = li.getElement('ul');
			} else {
				ul = new Element('ul').inject(li);
			}
			ul.parentNode.getElement('img').setProperties({src:this.options.icon_minus,style:'cursor:pointer'})
		}
		this.modifeLi(
			new Element('li')
				.appendText('Nova polozka')
				.inject(ul)
				.setProperty('id','temp_id')
		);
		preloader(true);
		new Request.JSON({
			url			:	root==true?"/" + this.options.controller + "/tree_menu/add_root/":"/" + this.options.controller + "/tree_menu/add_item/" + ul.parentNode.id.substring(this.options.prefix.length), 
			onComplete	: 	function(json){
				$('temp_id').id = json.id;
				preloader(false);
			}
		}).send();
	},
	
	active:function(){
		$(this.selectedMenu).getElements('input').addClass('inactive');
		preloader(true);
		new Request.JSON({
			url			:	"/" + this.options.controller + "/tree_menu/status_item/" + this.selectedMenu.substring(this.options.prefix.length) + "/0/", 
			onComplete	: 	function(json){
				preloader(false);
			}
		}).send();
	},
	
	deactive:function(){
		$(this.selectedMenu).getElement('input').removeClass('inactive');
		preloader(true);
		new Request.JSON({
			url			:	"/" + this.options.controller + "/tree_menu/status_item/" + this.selectedMenu.substring(this.options.prefix.length) + "/1/", 
			onComplete	: 	function(json){
				preloader(false);
			}
		}).send();
	},
	
	move_top: function(){
		var ul = $(this.selectedMenu).parentNode;
		var ils = ul.getUlItems();
		var first = ils[0];
		var index = $(this.selectedMenu).getIndex();
		$(this.selectedMenu).injectBefore(first);
		preloader(true);
		new Request.JSON({
			url			:	"/" + this.options.controller + "/tree_menu/move_item_top/" + this.selectedMenu.substring(this.options.prefix.length) + "/" + index, 
			onComplete	: 	function(json){
				preloader(false);
			}
		}).send();
		
	},
	
	move_up: function(){
		var ul = $(this.selectedMenu).parentNode;
		var ils = ul.getUlItems();
		var index = $(this.selectedMenu).getIndex();
		if (ils[index-1]){
			$(this.selectedMenu).injectBefore(ils[index-1]);
			preloader(true);
			new Request.JSON({
				url			:	"/" + this.options.controller + "/tree_menu/move_item_up/" + this.selectedMenu.substring(this.options.prefix.length), 
				onComplete	: 	function(json){
					preloader(false);
				}
			}).send();
		}
	},
	
	move_down:function(){
		var ul = $(this.selectedMenu).parentNode;
		var ils = ul.getUlItems();
		var index = $(this.selectedMenu).getIndex();
		if (ils[index+1]){
			$(this.selectedMenu).injectAfter(ils[index+1]);
			preloader(true);
			new Request.JSON({
				url			:	"/" + this.options.controller + "/tree_menu/move_item_down/" + this.selectedMenu.substring(this.options.prefix.length), 
				onComplete	: 	function(json){
					preloader(false);
				}
			}).send();
		}
	},
	
	move_bottom: function(){
		var ul = $(this.selectedMenu).parentNode;
		var ils = ul.getUlItems();
		var last = ils[ils.length -1];
		var sIndex = $(this.selectedMenu).getIndex();
		$(this.selectedMenu).injectAfter(last);
		var eIndex = $(this.selectedMenu).getIndex();
		var index = eIndex - sIndex;
		preloader(true);
		new Request.JSON({
			url			:	"/" + this.options.controller + "/tree_menu/move_item_bottom/" + this.selectedMenu.substring(this.options.prefix.length) + "/" + index, 
			onComplete	: 	function(json){
				preloader(false);
			}
		}).send();
	},
	
	delete_item: function(){
		if (!this.selectedMenu) return;
		var parentUl = $(this.selectedMenu).parentNode;
		$(this.selectedMenu).remove();
		if (parentUl.getElements('li').length == 0){
			parentUl.parentNode.getElement('img').setProperty('src',this.options.icon_normal);
			parentUl.remove();
		}
		preloader(true);
		new Request.JSON({
			url			:	"/" + this.options.controller + "/tree_menu/delete_item/" + this.selectedMenu.substring(this.options.prefix.length), 
			onComplete	: 	function(json){
				preloader(false);
			}
		}).send();
	}
});

  DragTree =new Class({	
	Extends: Drag,

	options: {
		droppables: [],
		container: false,
		depth: {
			minimum: 1,
			maximum: 999,
			difference: 999
		}
	},

	initialize: function(element, options){
		this.setOptions(options);
		this.element = element;
		this.marker = $(options.marker);
		this.contain = $(options.contain);

		this.droppables = $$(this.options.droppables);
		this.container = $(this.options.container);
		arguments.callee.parent(element, options);
		
	},
	attach: function(){
		this.handles.addEvent('mousedown', this.bound.start);
			return this;
	},

	start: function(event){
		if (event.target.nodeName != 'A') return;		
		this.element = event.target = event.target.parentNode;
		//this.element = event.target;
		//this.element = this.element;
				
		this.beforeDrag();
		this.element.effects({
			duration: 500,
			transition: Fx.Transitions.Expo.easeOut
		}).start({
			'opacity': 0.55
		})
		this.element.setStyles({
			'position'	: 'absolute', 
			'z-index'	: 1, 
			'left'		: this.element.getCoordinates().left - this.element.getScroll.x, 
			'top'		: this.element.getCoordinates().top - this.element.getScroll.y, 
			'width'		: this.element.getStyle('width'), 
			'height'	: this.element.getStyle('height')
		})
 
		
		
		if (this.container){
			var el = this.element, cont = this.container, ccoo = cont.getCoordinates(el.getOffsetParent()), cps = {}, ems = {};

			['top', 'right', 'bottom', 'left'].each(function(pad){
				cps[pad] = cont.getStyle('padding-' + pad).toInt();
				ems[pad] = el.getStyle('margin-' + pad).toInt();
			}, this);

			var width = el.offsetWidth + ems.left + ems.right, height = el.offsetHeight + ems.top + ems.bottom;
			var x = [ccoo.left + cps.left, ccoo.right - cps.right - width];
			var y = [ccoo.top + cps.top, ccoo.bottom - cps.bottom - height];

			this.options.limit = {x: x, y: y};
		}
		
		this.marker.effects({
			duration: 500,
			transition: Fx.Transitions.Expo.easeOut
		}).start({
			'opacity': 1
		})
		this.marker.inject(this.element,'after').setStyles({'display': 'block', 'height': this.element.getStyle('height')});
		
		this.related = $$('#' + this.contain.id + ' ' + this.element.getTag());
		
		//arguments.callee.parent(event);
		this.fireEvent('onBeforeStart', this.element);
		this.mouse.start = event.page;
		var limit = this.options.limit;
		this.limit = {'x': [], 'y': []};
		var scroll = this.element.getRelativePosition();
		for (var z in this.options.modifiers){
			if (!this.options.modifiers[z]) continue;
			this.value.now[z] = this.element.getStyle(this.options.modifiers[z])
			if (this.value.now[z] == 'auto'){	
				if (z == 'x') 	this.value.now[z] = (scroll['x']) + 'px';
				if (z == 'y') 	this.value.now[z] = (scroll['y']) + 'px';
			} 
			this.value.now[z] = this.value.now[z].toInt();
			this.mouse.pos[z] = event.page[z] - this.value.now[z];
			if (limit && limit[z]){
				for (var i = 2; i--; i){
					if ($chk(limit[z][i])) this.limit[z][i] = $lambda(limit[z][i])();
				}
			}
		}
		if ($type(this.options.grid) == 'number') this.options.grid = {'x': this.options.grid, 'y': this.options.grid};
		this.document.addEvents({mousemove: this.bound.check, mouseup: this.bound.cancel});
		this.document.addEvent(this.selection, this.bound.eventStop);
	},
	
	check: function(event){
		this.document.addEvents({
			mousemove: this.bound.drag,
			mouseup: this.bound.stop
		});
	},

	drag: function(event){
		//var target = event.target;

		var target = this.element;
		
		var mouse = this.mouse.now;
		var marker = this.marker;
		var placer;
		// Find which relation the cursor is over
		marker.position = marker.getCoordinates();
		if(mouse.y < marker.position.top || mouse.y > marker.position.bottom)
			this.related.each(function(element){
				element.position = element.getCoordinates();
				if(mouse.y > element.position.top && mouse.y < element.position.bottom && mouse.x > element.position.left && element != target){
					placer = $(element);
				}
			});
		
		if($defined(placer)){
			
			// Find placer depth and position
			placer.depth = this.depth(this.contain, placer);
			placer.position = placer.getCoordinates();
 
			// Make sure all checks are ok
			
			if(placer.depth >= this.options.depth.minimum && placer.depth <= this.options.depth.maximum) {// Depth
				if(Math.abs(placer.depth - this.depth(this.contain, this.element)) <= this.options.depth.difference) { // Difference
					if(mouse.y < placer.position.top + placer.getStyle('height').toInt() / 2) {
						this.marker.injectBefore(placer);
					} else {
						// Insert as a child (if possible) or after
						if(mouse.x > placer.position.left + 30 && placer.getFirst(this.contain.getTag()) && placer.depth + 1 >= this.options.depth.minimum && placer.depth + 1 <= this.options.depth.maximum && Math.abs(placer.depth + 1 - this.depth(this.contain, this.element)) <= this.options.depth.difference)					{
							this.marker.injectTop(placer.getFirst(this.contain.getTag()));
						} else {
							this.marker.injectAfter(placer);
						}
					}
				}
			}
		}
		
		//arguments.callee.parent(event);
		this.mouse.now = event.page;
		for (var z in this.options.modifiers){
			if (!this.options.modifiers[z]) continue;
			this.value.now[z] = this.mouse.now[z] - this.mouse.pos[z];
			if (this.options.limit && this.limit[z]){
				if ($chk(this.limit[z][1]) && (this.value.now[z] > this.limit[z][1])){
					this.value.now[z] = this.limit[z][1];
				} else if ($chk(this.limit[z][0]) && (this.value.now[z] < this.limit[z][0])){
					this.value.now[z] = this.limit[z][0];
				}
			}
			if (this.options.grid[z]) this.value.now[z] -= (this.value.now[z] % this.options.grid[z]);
			this.element.setStyle(this.options.modifiers[z], this.value.now[z] + this.options.unit);
		}
		this.fireEvent('onDrag', this.element);
	},

	stop: function(event){
		
		this.element.injectBefore(this.marker).setStyles.pass({'position': '', 'width': '', 'height': ''}, this.element).delay(505);
		this.element.effects({
			duration: 500,
			transition: Fx.Transitions.Expo.easeOut,
			wait: false
		}).start({
			'opacity': 1,
			'top': this.marker.getCoordinates().top - this.element.getScroll.y,
			'left': this.marker.getCoordinates().left - this.element.getScroll.x
		})

		// Style marker
		this.marker.setStyles.pass({'display': 'none', 'height': ''}, this.marker).delay(505);
		this.marker.effects({
			duration: 500,
			transition: Fx.Transitions.Expo.easeOut,
			wait: false
		}).start({
			'opacity': 0
		})
		this.afterDrag();
			return arguments.callee.parent(event);
	},
	depth: function(contain, target){		
		depth = 0;
		if(target != contain){
			if(target.getTag() != contain.getTag())
				depth++;
			depth += this.depth(contain, target.getParent());
		}	
 
		return depth;
	},
	beforeDrag:function(){
		var liList = this.contain.getElements('li');
		liList.each(function(item){
			if (!item.getElement('ul'))
				new Element('ul',{'class':'tempUl'}).inject(item);
		});
	},
	
	afterDrag:function(){
		var ulList = this.contain.getElements('ul');
		ulList.each(function(item){
			if (!item.getElement('li'))
				item.remove();
			else 
				item.removeClass('tempUl');
		});
		var mIndex 	= this.element.parentNode.getUlItems().length -1;
		var cIndex 	= this.element.getIndex();
		var index 	= mIndex - cIndex;
		var parent_id = (this.contain == this.element.parentNode)?'root':this.element.parentNode.parentNode.id.substring(this.options.object.options.prefix.length);
		preloader(true);
		new Request.JSON({
			url 		:	"/" + this.options.object.options.controller + "/tree_menu/move_item/" + this.element.id.substring(this.options.object.options.prefix.length) + "/" + parent_id + "/" + index,
			onComplete	: 	function(json){
				preloader(false);
			}
		}).send();
		
	}
});