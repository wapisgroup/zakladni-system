wysiwyg_subpage_path = '/js/wysiwyg/subpages/';//'wysiwyg/subpages/';//
	function log_now(text){
		if (console)
			console.log(text);
	}

	function generateHTLM(el){
			short_tag = ['IMG','HR','BR'];
			tags = ['p','a','table','tr','td','th', 'div','span', 'address','img','hr','br', 'h2', 'h3', 'h4', 'h5', 'h6', 'strong', 'em','code', 'strike', 'sub', 'sup', 'ul', 'ol', 'li','label'];
			atrs = ['class', 'style','name', 'link', 'ref', 'width', 'height', 'alt', 'title', 'href', 'target', 'rel', 'align', 'border', 'hspace', 'vspace', 'src', 'colspan', 'rowspan', 'id'];
			tablica = [["\n"," "],["\t",""],["  "," "]];
			var out ='';
			var item = el.childNodes;
			for (var i=0; i < item.length; i++){
				item[i].level = item[i].parentNode.level + 1;
				if (item[i].nodeType == 3){
					txt = item[i].nodeValue.strtr(tablica);
					if (txt != '')
						out += ("\t").str_repeat(item[i].level) + "" + txt + "\n";
				} else {
					if (short_tag.indexOf(item[i].nodeName) == -1){
							var nodeN = item[i].nodeName.toLowerCase();
						if (nodeN == 'b') nodeN = 'strong';
						if (tags.indexOf(item[i].nodeName.toLowerCase()) != -1){
							out += ("\t").str_repeat(item[i].level) + '<' + nodeN;
							atrs.each(function(atr){
								if (item[i].getAttribute(atr) != null && item[i].getAttribute(atr) != '')
									out += ' ' + atr + '="' + item[i].getAttribute(atr) + '"';
							});
							out += ">\n";
						}
						out += generateHTLM(item[i]);
						if (tags.indexOf(item[i].nodeName.toLowerCase()) != -1){
							out += ("\t").str_repeat(item[i].level) + "</" + nodeN + ">\n";
						}
					} else {
						out += ("\t").str_repeat(item[i].level) + '<' + item[i].nodeName.toLowerCase();
						atrs.each(function(atr){
							if (item[i].getAttribute(atr) != null && item[i].getAttribute(atr) != '')
								out += ' ' + atr + '="' + item[i].getAttribute(atr) + '"';
						});
						out += "\ />\n";
					}	
				}
			}			
			return out;
		}

Element.implement({	
	killWswg: function(){
		var textarea = this.getElement('.textarea');
		var htmltextarea = textarea.getParent('.wysiwyg').getElement('.text_area');
		textarea.level = -1;
		htmltextarea.value = generateHTLM(textarea);
	},
	makeWswg: function(){
		Element.implement({
			setEnabled : function(stav){
				if (!this.hasClass('nda')){
					this.activity = stav;
					if (stav)
						this.setStyle('background-position','left top');
					else
						this.setStyle('background-position','right top');
				} else
					this.activity = true;
				return this;
			}
		});
			
		function activeButtons(){
			var sel = document.Selection.getRng(), elm = document.Selection.getFocusElement();
			if (sel && sel.toString().length >0){
				toolbar.getElements('.link').setEnabled(true);
				toolbar.getElements('.image, .anchor').setEnabled(false);
			} else {
				if (elm && elm.parentNode.nodeName == 'A')
					toolbar.getElements('.link').setEnabled(true);
				else
					toolbar.getElements('.link').setEnabled(false);
					
				toolbar.getElements('.image, .anchor').setEnabled(true);
			}
			// unlink
			if (elm && elm.parentNode.nodeName == 'A')
				toolbar.getElements('.unlink').setEnabled(true);
			else
				toolbar.getElements('.unlink').setEnabled(false);
			
			toolbar.getElements('.html').setEnabled(true);
			
		}
	
		function updatePath(){
			var arr, out = 'BODY > ';
			// toto opravit na element a vlozit az po Selection Class
			textarea.elementPath = arr = textarea.getFocusElements();
			arr.rEach(function(item){
				if (item && item.nodeName != '#text' && item.className != 'textarea_inside')
					out += item.nodeName + ' > ';
			});
			footer.setHTML(out);
			activeButtons();
		}
		
		function makePopup(options){
			popup_window.setStyle('display','block');
			popup_window.getElement('h3').setHTML(options.caption + '<span class="popupClose"></span>');
			popup_window.getElement('.popupClose').addEvent('click',function(){
				destroyPopup()
			});
			new Request.HTML({url : options.url, update: popup_window.getElement('.popup'),	onComplete: options.onComplete}).send();
			over_textarea_pop.setStyles(textarea.getStyles('width','height','display'));
		}
		
		function destroyPopup(){
			popup_window.setStyle('display','none');
			over_textarea_pop.setStyle('display','none');
		}
		
		var toolbar 			= this.getElement('.toolbar');
		var footer 				= this.getElement('.footer');
		var textarea 			= this.getElement('.textarea');
		var original_textarea 	= this.getElement('.text_area');
		var popup_window 		= this.getElement('.popupWindow');
		var over_textarea_pop	= this.getElement('.over_div');
		
		/* skryje original textarea */
		original_textarea.setStyle('display','none');
		
		/* z originalni textarea vezme text a vlozi jej do editoru a nedefinovani events */
		textarea
			.setHTML(original_textarea.value)
			.addEvents({
				'keyup'		: 	updatePath,
				'mouseup'	:	updatePath,
				//'click'		:	this.activeButtons.bind(this),
				'keydown'	:	function(e){
					var event = new Event(e);
					if (event.control){
						switch (event.key){
							case 'b': case 'i': case 'u':
								event.stop();
								keyCommad = $H({'b':'bold','i':'italic','u':'underline'});
								document.execCommand(keyCommad[event.key],null,null);
								break;
							case 'v':
							case 'V':
								event.stop();
								makePopup({
									caption:'Vložit',
									url: wysiwyg_subpage_path + 'paste.html',
									onComplete: function(){
										var selText	= document.Selection.getRng();
										box = popup_window.getElement('.popup');
										box.getElement('.paste_html_div').focus()
										box.getElement('.paste_html_div')										
											.addEvents({
												'keydown': function(e2){
													var event2 = new Event(e2);
													if (event2.control && event2.key == 'v'){
														this.setHTML('');
													} else {
														event2.stop();
													}
												},
												'keyup':function(e2){
													event2 = new Event(e2);
													if (event2.control && event2.key == 'v'){
														if (this.getHTML() != '.'){
															this.setHTML(this.getHTML().clearHTML());
														}
													} else {
														event2.stop();
													}
												}
											});
										
										box.getElement('.close_button').addEvent('click',function(){
											document.Selection.selRgn(selText);
											document.Selection.insertHTML(box.getElement('.paste_html_div').getHTML());
											
											destroyPopup();
										});
									}
								});
								break;
							case 'space':
								event.stop();
								//textarea.insertHTML('&nbsp;');
								break;
							default:
								break;
						}
					}
				}
			});
			
			toolbar.getElements('button').addEvents({
				'mouseover':function(){ if (this.activity) {this.setStyle('background-position','center top');}},
				'mouseout':function(){ if (this.activity) {this.setStyle('background-position','left top'); }},
				'click':function(e){
					new Event(e).stop();
					if (!this.activity) return;
					var button = new Event(e).target;
					var execute = button.getProperty('class');
							
				switch (execute){
					case 'link':
						makePopup({
							caption:'Odkaz',
							url: wysiwyg_subpage_path + 'link.html',
							onComplete: (function(){
								function getAHref(){
									elm = document.Selection.getFocusElement();
									elm2 = textarea.getParentElement('A'); 
									if (elm.nodeName == 'A') {elm = elm;} else if (elm2 != null && elm2.nodeName == 'A'){elm = elm2;} else {elm = null;}
									return elm;
								}
								
								function createAnchorList(){
									var el = box.getElement('.link_add_edit_anchorlist');
									textarea.getElements('a').each(function(item){
										if (item.getProperty('name') != null)
											el.options[el.options.length]=new Option(item.getProperty('name'),item.getProperty('name'));
									});
								}
								
								textarea.curr_position	= this.getCursorPosition();
								var box = popup_window.getElement('.popup');
								
								elm = getAHref();
								createAnchorList();
							
								if (elm != null){
									href = elm.getProperty('href');
									if (href.indexOf('#') != -1){
										anchor = href.substring(href.indexOf('#')+1,href.length);
										el.setValue(anchor);
										href= href.substring(0,href.indexOf('#'));
									}
									box.getElement('.link_add_edit_href').value = href;
									box.getElement('.link_add_edit_title').value = elm.getProperty('title');									
									/* uprava target, class, rel */
									if (elm.getProperty('targer') != null) 	box.getElement('.link_add_edit_targetlist').setValue(elm.getProperty('targer'));
									if (elm.getProperty('class') != null) 	box.getElement('.link_add_edit_classlist').setValue(elm.getProperty('class'));
									if (elm.getProperty('rel') != null)		box.getElement('.link_add_edit_rel').value = elm.getProperty('rel');
								}
							
								/* APPLY LINK FUNCTION */
								box.getElement('.close_button').addEvent('click',function(){
									if (box.getElement('.link_add_edit_href').value == '') {
										alert("Neni vyplnena adresa odkazu");
										return;
									}
									this.setCursorPosition(textarea.curr_position);
									elm = getAHref();
									
									if (elm == null) {
										document.execCommand('createlink',false,'_TEMP_LINK');
										elm = textarea.getElement('a[href$=_TEMP_LINK]');
									}
									/* anchor, target, rel, class */
									elm.setProperties({href: (box.getElement('.link_add_edit_anchorlist').value=="")?box.getElement('.link_add_edit_href').value:box.getElement('.link_add_edit_href').value+'#'+box.getElement('.link_add_edit_anchorlist').value,title:box.getElement('.link_add_edit_title').value})
									if (box.getElement('.link_add_edit_targetlist').value !="") elm.setProperty('targer',box.getElement('.link_add_edit_targetlist').value);
									if (box.getElement('.link_add_edit_classlist').value !="") 	elm.setProperty('class',box.getElement('.link_add_edit_classlist').value);
									if (box.getElement('.link_add_edit_rel').value !="") 		elm.setProperty('rel',box.getElement('.link_add_edit_rel').value);	
									destroyPopup();
								});
							}).bind(this)
						});
						break;
					case 'anchor':
						makePopup({
							caption:'Odkaz',
							url: wysiwyg_subpage_path + 'anchor.html',
							onComplete: (function(){
								var box = popup_window.getElement('.popup');
								textarea.curr_position	= this.getCursorPosition();
								
								elm = document.Selection.getFocusElement();
								if (elm && elm.tagName == 'A')
									box.getElement('.anchor_add_edit_name').value = elm.getProperty('name');
																		
								box.getElement('.close_button').addEvent('click',function(){
									this.setCursorPosition(textarea.curr_position);
									elm = document.Selection.getFocusElement();
									var name = box.getElement('.anchor_add_edit_name').value;
									
									if (elm && elm.tagName == 'A') /* editace */
										elm.setProperties({title:name,name:name});
									else /* insert */
										document.Selection.insertHTML('<a class="anchor" title="'+name+'" name="'+name+'"/>');
									destroyPopup();
								});
							}).bind(this)
						});
						break;
					case 'image':
						makePopup({
							caption:'Obrazek',
							url: wysiwyg_subpage_path + 'image.html',
							onComplete: (function(){
								var selText	= document.Selection.getRng(), box = popup_window.getElement('.popup'), elm = document.Selection.getFocusElement();
								textarea.curr_position	= this.getCursorPosition();
								
								if (elm.nodeName == 'IMG'){
									properties = elm.getProperties('src','title','align','border','hspace','vspace','rel');
									box.getElement('.image_add_edit_src').value = properties.src;
									box.getElement('.image_add_edit_title').value = properties.title;
									box.getElement('.image_add_edit_zarovnanilist').setValue(properties.align);
									box.getElement('.image_add_edit_border').value = properties.border;
									box.getElement('.image_add_edit_hspace').value = properties.hspace;
									box.getElement('.image_add_edit_vspace').value = properties.vspace;
									box.getElement('.image_add_edit_rel').value = properties.rel;
									box.getElement('.image_add_edit_width').setValue(parseInt(elm.getStyle('width')));
									box.getElement('.image_add_edit_height').setValue(parseInt(elm.getStyle('height')));
								}
						
								box.getElement('.close_button').addEvent('click',function(){
									textarea.setCursorPosition(textarea.curr_position);
									elm = document.Selection.getFocusElement();
									if (elm.nodeName != 'IMG') { /* insert */
										document.Selection.insertHTML('<img class="_TEMP_IMAGE"/>');
										elm = textarea.getElement('._TEMP_IMAGE');
									} 
									elm.setProperties({
										'src'	:	box.getElement('.image_add_edit_src').value,
										'title'	:	box.getElement('.image_add_edit_title').value,
										'alt'	:	box.getElement('.image_add_edit_title').value,
										'align'	:	box.getElement('.image_add_edit_zarovnanilist').value,
										'border':	box.getElement('.image_add_edit_border').value,
										'hspace':	box.getElement('.image_add_edit_hspace').value,
										'vspace':	box.getElement('.image_add_edit_vspace').value,
										'rel'	:	box.getElement('.image_add_edit_rel').value,
										'class'	:	'obrazek'
									});
									var width = box.getElement('.image_add_edit_width').value;
									var height = box.getElement('.image_add_edit_height').value;
									
									if ((width != '' && width != 0) && (height != '' && height != 0))
										elm.setStyles({'width'	: width,'height': height});
									destroyPopup();
								})
							}).bind(this)
						});
						break;
					case 'html':
						if (!this.active){
							textarea.level = -1;
							original_textarea
								.setStyles(textarea.getStyles('width','height','display'))
								.setValue(generateHTLM(textarea));
								
							textarea.setStyle('display','none');
							toolbar.getElements('button').setEnabled(false);
							toolbar.getElement('.html').setEnabled(true);
							this.active = true;							
						} else {
							textarea
								.setStyle('display','block')
								.setHTML(original_textarea.value);
							original_textarea.setStyle('display','none');
							toolbar.getElements('button').setEnabled(true);
							activeButtons();
							this.active = false;
						}
						break;
					case 'fullscreen':
					
						if (!this.active){
							window.scroll(0,0);
							window_id = textarea.getParent('.CB_Window').getProperty('id');
							this.oldSize = {
								left: 		$(window_id).getStyle('left'),
								top: 		$(window_id).getStyle('top'),
								width	:	textarea.getParent('.wysiwyg').getStyle('width'),
								height: 	textarea.getParent('.wysiwyg').getStyle('height'),
								height2: 	textarea.getStyle('height'),
								winW:	$(window_id).getStyle('width')
							}
							
							$(window_id).setStyles({
								top:		0,
								left:		0,
								width:	window.getSize().x
							});
							$(document.body).setStyle('overflow-y','hidden');
							textarea.getParent('.wysiwyg').setStyles({
								'z-index'	:	999999,
								'position'	:	'absolute',
								'top'		:	0,
								'left'		:	0,
								'width'	: 	window.getSize().x -40,
								'height'	:	window.getSize().y
							});
							
							$(window_id).setStyles({
								width:	window.getSize().x
							});
							
							textarea.setStyles({
								'height': (window.getSize().y - 44)
							});
							this.active = true;
						} else {
							window_id = textarea.getParent('.CB_Window').getProperty('id');
							$(window_id).setStyles({
								'top':this.oldSize.top,
								'left':this.oldSize.left,
								width:	this.oldSize.winW
							});
							$(document.body).setStyle('overflow-y','auto');
							textarea.getParent('.wysiwyg').setStyles({
								'z-index':999999,
								'position':'relative',
								'top':0,
								'left':0,
								'width':'75%',
								'height':this.oldSize.height
							});
							textarea.setStyles({
								'height': this.oldSize.height2
							});
							this.active = false;
						}
						
						break;
					case 'justifyleft':
					case 'justifyright':
					case 'justifycenter':
					case 'justifyfull':
						try {document.execCommand("useCSS", false, true);} catch (ex) {}
						try {document.execCommand("styleWithCSS", false, false);} catch (ex) {}
						try{
							document.execCommand(execute, null, null);
						} catch(ex){
							document.execCommand('formatBlock', null, 'p');
							updatePath();
							if (execute == 'justifyfull') 
								execute = 'justifyjustify';
							textarea.getParentElement('p').setProperty('align',execute.substring(7));							
						}						
						break;
					default:
						this.position = textarea.getCursorPosition();
						try {document.execCommand("useCSS", false, true);} catch (ex) {}
						try {document.execCommand("styleWithCSS", false, false);} catch (ex) {}
						document.execCommand(execute, null, null);
						return false;
						break;
				}	
				return false;
			}
		})
		.setEnabled(false)
		.removeClass('nda');
		toolbar.getElement('.format').addEvent('change',function(){
			try {document.execCommand("useCSS", false, true);} catch (ex) {}
			try {document.execCommand("styleWithCSS", false, false);} catch (ex) {}
			document.execCommand('formatBlock', null, this.value);
		});
	}
});