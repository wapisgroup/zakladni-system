 var lang_zavrit_okno = 'Zavřít okno';
 var lang_refresh_okno = 'Refresh okna';
 var lang_max_min_okno = 'Maximalizovat / Minimalizovat okno';
 var lang_confirm_close = 'Opravdu chcete zavřít okno? Změny nebudou uloženy!';
 
 var MyAlert = new Class({ 
	Implements: [Options, Events],
	options:{
		type: 'alert', // alert|info|question,
		items: [
			'Email (musi byt vyplnen)',
			'Telefonní číslo (má nesprávný formát - zadejte ve tvaru: +420 XXX XXX XXX)'
		],
		caption: 'Následující položky jsou chybně vyplněny',
		button_label: 'Opravit'
	},
	show: function(items,options){
		this.setOptions(options);
		this.options.items = items;
		var main = new Element('div',{'class':'alert_message'}).inject($('addon'));
		var div_in = new Element('div',{'class':'in'}).inject(main).setHTML('<h2>' + this.options.caption + '</h2>');
		var ul_error = new Element('ul').inject(div_in);
		$each(this.options.items, function(value){
			new Element('li').inject(ul_error).setHTML(value);
		});
		var p_submit = new Element('p',{'class':'submit'}).inject(div_in);
		new Element('input',{type:'button',value:this.options.button_label,'class':'button'}).inject(p_submit).addEvent('click', function(e){
			new Event(e).stop();
			main.dispose();
			domwin_modal.dispose();
		});

		var window_size = window.getSize(),x,y;
		domwin_size = main.getSize();
		x = (window_size.x/2 - domwin_size.x / 2);
		y = (window_size.y/2 - domwin_size.y / 2) + window.getScroll().y;
		main.setStyles({
			left: (x>10)?x:10,
			top	: (y>10)?y:10
		});
		
		domwin_modal = new Element('div').addClass('domwin_modal').inject(main,'before')
			.setStyles({width:window_size.x,height:window_size.y, opacity:0.02,"z-index":1500})
		
	}
 });

 
var Domwin = new Class({
	Implements: [Options, Events],
	flagPath: '/css/fastest/flags/',
	basic_content: '<table class="domwin_table" style="width:100%; height:100%" height="100%" cellpadding="0" cellspacing="0" ><tbody><tr class="CB_Header" style="height: 26px;"><td class="CB_Top"/><div class="CB_title">Nahrávám...</div></tr><tr class="CB_Body"><td class="CB_Content" valign="top" align="left" style="height:100%"><div class="CB_ImgContainer domwin_preloader" style="height:100%"></div></td></tr><tr class="CB_Footer" style="height: 26px;"><td class="CB_Btm"/></tr></tbody></table>',
//	basic_content: '<table style="width:100%; height:100%" height="100%" cellpadding="0" cellspacing="0" ><tbody><tr class="CB_Header" style="height: 26px;"><td class="CB_Top"/><div class="CB_title">Nahrávám...</div></tr><tr class="CB_Body"><td class="CB_Content" valign="top" align="left" style="height:100%"><div class="CB_ImgContainer" style="height:100%"><div class="CB_Preloader"></div></div></td></tr><tr class="CB_Footer" style="height: 26px;"><td class="CB_Btm"/></tr></tbody></table>',
	options:{},
	start_z_index	: 1110, 		// default z-index
	default_options:{
		defined_lang	: ['cz','en'],		// language flags
		sizes			: [150,150],	// default sizes for small domwin before ajax onComplete
		scrollbars		: false,		// scrollbars for inner div true|false
		modal			: true,			// modal window, if true, background true|false
		modal_close		: true,			// if click on modal background close domwinDiv
		id				: 'domwin',		// id of domwin
		title			: 'Title okna', // title of domwin
		languages		: false,			// show languages flags true|false
		type			: 'AJAX',		// type of load content AJAX|DOM
		ajax_url		: '',			// if type == AJAX, this is url
		closeConfirm	: true,			// confirm message, if event on close button true|false
		post_data		: '',			// special post dato for ajax load
		max_minBtn 		: true,			// show minimalize/maximalize button true|false
		closeBtn 		: true,			// show close button true|false
		refreshBtn 		: true,			// show refresh button true|false
		fix_size		: true, 		// change inner size, if domwin size is greatet that window size
		remove_scroll	: true,		// remove window scrollbar
		effect_style	: 'fade', 		// style of offect domwin fade|simple|size,
		dom_id			: [], 			// id of dom element to type DOM
		dom_onRender	: $empty, 			// 
		self_data		: {}
	},
	
	set_data: function(id,data){
		$merge.run([this.options[id]['self_data']].extend(data));
	},
	get_data: function(id,data){
		return this.options[id]['self_data'][data];
	},
	setSpecOptions: function(id){
		var temp_option = $merge.run([this.default_options].extend(arguments));
		this.options[temp_option.id] = temp_option;
		if (!this.addEvent) return this;
		for (var option in this.options[temp_option.id]){
			if ($type(this.options[temp_option.id][option]) != 'function' || !(/^on[A-Z]/).test(option)) continue;
			this.addEvent(option, this.options[temp_option.id][option]);
			delete this.options[temp_option.id][option];
		}
		return this;
	},
	
	newWindow: function(options){
		var id = options.id;
		if (!$(options.id)) {
			this.setSpecOptions(options);
			this.start_z_index += 2;
			// create main div/table object
			var domwinDiv = new Element('div', {
				'class': 'CB_Window',
				'id': this.options[id].id
			}).inject($('addon')).setStyles({
				'opacity': 1,
				"height": "92px",
				"width": "120px",
				"z-index": this.start_z_index
			}).setHTML(this.basic_content);
			// pokud chci domwin pouze na max velikosti okna
			if (this.options[id].fix_size == true) 
				this.options[id].sizes[1] = (this.options[id].sizes[1] > window.getSize().y.toInt()) ? (window.getSize().y.toInt() - 30) : this.options[id].sizes[1];
			// inner scrollbar
			if (this.options[id].scrollbars) 
				domwinDiv.getElement('.CB_ImgContainer').setStyle('overflow', 'auto');
			else 
				domwinDiv.getElement('.CB_ImgContainer').setStyle('overflow', 'hidden');
			// START IE Hack HEIGHT
			domwinDiv.getElement('.CB_Content').setStyle('height', this.options[id].sizes[1] - 52);
			// END IE Hack HEIGHT
			// type of effect_style
			switch (this.options[id].effect_style) {
				case 'fade':
					domwinDiv.setStyles({
						width: this.options[id].sizes[0],
						height: this.options[id].sizes[1] + 12,
						opacity: 0
					});
					break;
				case 'simple':
					domwinDiv.setStyles({
						width: this.options[id].sizes[0],
						height: this.options[id].sizes[1]
					});
					break;
			}
			
			this.centerWin(id);
			// if modal create background
			if (this.options[id].modal == true) {
				var window_size = this.getScrollSize(id);
				domwin_modal = new Element('div').addClass('domwin_modal').inject(domwinDiv, 'before').setStyles({
					width: window_size.x,
					height: window_size.y,
					opacity: 0,
					"z-index": this.start_z_index - 1
				})
				
				if (this.options[id].modal_close == true) 
					domwin_modal.addEvent('click', this.closeWindow.bind(this, id));
				
				switch (this.options[id].effect_style) {
					case 'fade':
						domwin_modal.fade(0.2);
						break;
					case 'simple':
					case 'size':
						domwin_modal.setStyle('opacity', 0.3);
						break;
					default:
						break;
				}
			}
			// init DRAG & DROP
			new Drag(domwinDiv, {
				handle: domwinDiv.getElement('.CB_title')
			});
			
			// Tilte Buttons
			// Maximalizovat
			if (this.options[id].max_minBtn == true) 
				new Element('div', {
					'class': 'CB_MaxWindow',
					title: lang_max_min_okno
				}).inject(domwinDiv).addEvent('click', this.maxWindow.bind(this, id));
			// Zavřít
			if (this.options[id].closeBtn == true) 
				new Element('div', {
					'class': 'CB_CloseWindow',
					title: lang_zavrit_okno
				}).inject(domwinDiv).addEvent('click', this.closeWindow.bind(this, {
					'id': id,
					button: true
				}));
			
			// Refresh
			if (this.options[id].refreshBtn == true) 
				new Element('div', {
					'class': 'CB_Refresh',
					title: lang_refresh_okno
				}).inject(domwinDiv).addEvent('click', this.loadContent.bind(this, id));
			
			
			if (this.options[id].languages == true) {
				var lang_div = new Element('div').inject(domwinDiv).setStyles({
					position: 'absolute',
					top: '30px',
					right: '-20px'
				});
				$each(this.options[id].defined_lang, function(item){
					new Element('img', {
						src: this.flagPath + item + '.gif',
						style: 'margin:3px; display:block'
					}).inject(lang_div).addEvent('click', (function(){
						domwinDiv.getElement('.CB_ImgContainer').getElements('.languages').setStyle('display', 'none');
						domwinDiv.getElement('.CB_ImgContainer').getElements('.languages.lang_' + item).setStyle('display', 'block');
					}).bind(this));
				}, this);
			}
			
			this.loadContent(id);
			this.show_effect(id);
		}
	},
	
	loadContent: function(id,local_options){
		var onComplete = $empty;
		if (local_options && local_options.onComplete){
			onComplete = local_options.onComplete + '()';
		}
		switch(this.options[id].type){
			case 'AJAX':
				var domwinDiv = $(id);
				new Request.HTML({
					data: 		this.options[id].post_data,
					url:		this.options[id].ajax_url,
					update:		domwinDiv.getElement('.CB_ImgContainer'),
					onComplete:	(function(){
						domwinDiv.getElement('.CB_title').setHTML(this.options[id].title);
						domwinDiv.getElement('.CB_ImgContainer').removeClass('domwin_preloader');
						if (this.options[id].languages == true){
							domwinDiv.getElement('.CB_ImgContainer').getElements('.languages').setStyle('display','none');
							domwinDiv.getElement('.CB_ImgContainer').getElements('.languages.lang_cz').setStyle('display','block');
						}
						/*
						 * slouzi pro lokalni natahnuti obsahu, napr. refresh a predani specificke fnc. pro udalost 
						 * nacteni obsahu
						 */
						eval(onComplete);

                        if (local_options && local_options.goto){
                            domtab.goTo(local_options.goto);
                        }
						
					}).bind(this)
				}).send();
				break;
			case 'DOM':
				var domwinDiv = $(id);
				var clone = this.options[id].dom_id.clone().inject(domwinDiv.getElement('.CB_ImgContainer'));
				domwinDiv.getElement('.CB_title').setHTML(this.options[id].title);
				domwinDiv.getElement('.CB_ImgContainer').removeClass('domwin_preloader');
				domwinDiv.getElement('.CB_ImgContainer').getElement('div').removeClass('none');
				this.options[id].dom_onRender();
				break;
			default:
				
				break;
		}
	},
	
	closeWindow: function(id){
		if($type(id) == 'object'){
			button = true;
			id = id.id;
		} else {
			button = false;
		}
		var domwinDiv = $(id);
		if (domwinDiv){
			if (button == false || this.options[id].closeConfirm == false ||(this.options[id].closeConfirm == true && confirm(lang_confirm_close))){
				if (this.options[id].modal){
					var modalDiv = domwinDiv.getPrevious();
					modalDiv.fade(0);
					(function(){
						modalDiv.dispose();
						if(this.options[id].remove_scroll == true){
							$(document.body).removeProperty('style');
							$(document.html).removeProperty('style');
							$(document.body).setProperty('style',' ');
							$(document.html).setProperty('style',' ');
							//START CHROME FIX - show scroller
							window.scroll(0,window.getScroll().y-1);
							//END CHROME FIX 
						}
					}).delay(1000,this);
				}
				domwinDiv.fade(0);
				(function(){
					domwinDiv.dispose();
					if(this.options[id].remove_scroll == true){
						$(document.body).removeProperty('style');
						$(document.html).removeProperty('style');
						//START CHROME FIX - show scroller
						$(document.body).setProperty('style',' ');
						$(document.html).setProperty('style',' ');
						window.scroll(0,window.getScroll().y-1);
						//END CHROME FIX 
					}
				}).delay(1000, this);
			}
		} else
			alert('ID: "'+id+'" neexistuje');
	},
	/*
	get Window ScrollSize, condition of option.remove_scroll
	*/
	getScrollSize: function(id){
		var scroll_sizes = window.getScrollSize();
		if (this.options[id].remove_scroll == true) scroll_sizes.x += 14;
		return scroll_sizes;
	},
	/*
	set domwin to center of screen
	*/
	centerWin: function(id){
		var domwinDiv = $(id), window_size = window.getSize(),x,y;
		domwin_size = domwinDiv.getSize();
		x = (window_size.x/2 - domwin_size.x / 2);
		y = (window_size.y/2 - domwin_size.y / 2) + window.getScroll().y;
		domwinDiv.setStyles({
			left: (x>10)?x:10,
			top	: (y>10)?y:10
		});
	},
	/*
	effect_after_load
	*/
	show_effect: function(id){
		switch (this.options[id].effect_style){
			case 'fade':
				$(id).fade(1);
				break;
			case 'simple':
				/* nothing to do it */
				break;
			case 'size':
				var myEffect = new Fx.Morph($(id), {duration: 'short', onComplete:function(){alert('done');}});
				myEffect.start({
					'width': this.options[id].sizes[0], 
					'height': this.options[id].sizes[1], 
					'left':[window.getSize().x / 2 - this.options[id].sizes[0] / 2],
					'top':[(window.getSize().y / 2 - this.options[id].sizes[1] / 2) + window.getScroll().y] 
				});
				
				break;
			default:
				break;
		}
		
		$(id).getElement('.CB_ImgContainer')
			.setStyles({'height': this.options[id].sizes[1] - 54,'display':'block'});

		if(this.options[id].remove_scroll == true){
			var scroll_x = window.getScroll().y;
			$(document.html).setStyle('overflow-y','hidden');
			$(document.html).setStyle('overflow-x','hidden');
			$(document.body).setStyle('overflow-y','hidden');
			$(document.body).setStyle('overflow-x','hidden');
			window.scroll(0,scroll_x);
		}
	},
	/*
	maximalize domwin
	*/
	maxWindow: function(id){
		
	}
	
 });
 var domwin = new Domwin;