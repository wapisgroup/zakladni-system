var DomTabs = new Class({
	curr_tabs : null,
	Implements: [Events,Options],
	options: {
		style: 'basic', //'float',
		control: null,
		start_tab : 0,
		menu_sufix: 'links',
		menu_top_only_show: false,
		menu_down_self_func: false,
		className: '',
		parentClass: 'CB_Window',
		field_sizes:{},
        permission:{},
        stav_permission:false, // value of stav
        /**
         * @stav_strictly
         * True - nastaveni stavu v relu je striktne dodrzovano (stav 2, zobrazime pouze kdyz je stav_permission = 2)
         * False - znamena ze kdyz stav_permission je 3, zobrazime i rely s mensim stavem ( stav_permission = 3, rel <= 3)
         */
        stav_strictly:true
	},
	selected : 0,
	fields : null,
	initialize: function(options) {
		this.setOptions(options);
		this.curr_tabs = $$('.' + this.options.className);
		if (this.curr_tabs.length == 0){
			alert('zvolena trida (' + this.options.className + ') pro domtab nebyla nalezena');	return;
		} else if (this.curr_tabs.length > 1){
			alert('Bylo nalezeno vice domtab s touto tridou (' + this.options.className + ')');	return;
		} else {
			this.init_menu();
			switch(this.options.style){
				case 'basic':
					this.fields = this.curr_tabs.getElements('.field');
					this.goTo(this.options.start_tab);
					break;
				case 'float':
					this.fields = this.curr_tabs[0].getElements('.field');
					this.curr_tabs[0].setStyles({
							width:this.options.field_sizes.width.toInt(),
							'clear':'both'
					});
					this.move_div = new Element('div')
						.inject(this.curr_tabs[0])
						.setStyles({
							'width': (this.options.field_sizes.width.toInt()+2)*this.fields.length,
							'position' : 'relative',
							'white-space': 'nowrap'
						})
					this.fields.each((function(item){
						item.setStyles({
							'float':'left',
							'clear':'none',
							'width': this.options.field_sizes.width.toInt()-12,
							//'height': this.options.field_sizes.height.toInt(),
							'white-space': 'normal'
						});
						this.move_div.adopt(item)
					}).bind(this))
					this.curr_tabs[0].setStyles({
							'overflow':'hidden'
					
					});
					this.moveMotion = new Fx.Morph(this.move_div, {
						duration: 'long', 
						transition: Fx.Transitions.Sine.easeOut
					});
					this.goTo(this.options.start_tab);
					break;
			}
			
		}
	},
	init_menu: function(){
		// init top manu
		menu = $$('.' + this.options.className + '_' + this.options.menu_sufix);
        tabs_allow  = JSON.decode(this.options.permission);
		if (menu) {
			this.menu_item = menu.getElements('a')[0];
			if (this.menu_item)
				this.menu_item.each(function(item,index){
				    rel = item.getProperty('rel');
                    allow_stavs = false;
                    if(rel){
                        allow_stavs = rel.split(',');  
                        max = allow_stavs.max();
                        if(this.options.stav_strictly == false && max <= this.options.stav_permission){
                            allow_stavs.push(this.options.stav_permission);
                        } 
                    }    
                    /**
                     * Nastaveni tabu a jejich nasledne schovani na zaklade stavu
                     * ktere porovnavame s nastaveni u odkazu zalozek a v property rel
                     * v rel tedy urcujeme pri jakem stavu maji byt povoleny
                     */    
                    if(this.options.stav_permission !== false && allow_stavs !== false && !allow_stavs.contains(this.options.stav_permission)){
                        item.getParent('li').addClass('disallow');
                    }    
                    /**
                     * Schovani tabu na zaklade permission od nastaveni uzivatelske skupiny
                     */
				    else if(tabs_allow != null && tabs_allow[index+1] && tabs_allow[index+1] == '0'){
				        item.getParent('li').addClass('disallow');
				    }
                    else{
    					item.addEvent('click', (function(e){
    						new Event(e).stop();
    						if (!this.options.menu_top_only_show)
    							this.goTo(index);
    					}).bind(this));
    					if (this.options.menu_top_only_show)
    						item.getParent('li').addClass('disallow');
				    }
                },this);
		}
		// init step links
		step_links = $$('.' + this.options.className + '_move');
		step_links.each(function(item){
			item.addEvent('click', (function(e){
				new Event(e).stop();
				if (!this.options.menu_down_self_func){
					index = item.getProperty('href').substring(5);
					this.goTo(index-1);
				}
			}).bind(this));
		},this);
	},
	goTo: function(index){
		if (this.menu_item){
			this.menu_item[this.selected].removeClass('select');
			this.menu_item[this.selected].getParent('li').removeClass('select');
			this.menu_item[index].addClass('select');
			this.menu_item[index].getParent('li').addClass('select');
		}
        
        this.selected = index;
        
		switch(this.options.style){
			case 'basic':
				this.fields[0].setStyle('display','none');
                if(this.menu_item[this.selected].getParent('li').hasClass('disallow') === false)
				    this.fields[0][index].setStyle('display','block');
				break;
			case 'float':
				this.moveMotion.start({
					'left':[this.move_div.getStyle('left').toInt(),(-1) * (this.options.field_sizes.width.toInt() )*index]
				});
		}
        
	}
});