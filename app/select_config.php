<?php 
// historie
$this->set('select_ano',$this->select_ano = array(
	'1' => 'Ano'

));
/*
$this->set('select_order_status',$this->select_order_status= array(
	'-1,1' => 'Ano'

));
*/
$this->set('history_category',array(
	'1' => 'Vložení do DB',
	'2' => 'Editace karty klienta',
	'3' => 'Aktivity klienta',
	'4' => 'Hodnocení',
	'5' => 'Zaměstnání',
	'6' => 'Docházka',
	'7' => 'Nábor',
	'8' => 'Stará historie ATEPu'
));

$this->set('history_actions',array(
		'1' 	=> 'vložení klienta do DB',
		'2' 	=> 'vložení klienta přes web formulář',
		'3' 	=> 'autorizování klienta z web formuláře', 
		'4' 	=> 'vymazání klienta z DB',
		'5' 	=> 'editace karty klienta',
		'6' 	=> 'přidání aktivity ke klientovi',
		'7' 	=> 'hodnocení klienta',
		'8' 	=> 'zařazení klienta na černou listinu',
		'9' 	=> 'zaměstnání klienta',
		'10' 	=> 'změna pracovního poměru',
		'11' 	=> 'klonování klienta',
		'12' 	=> 'zaměstnání klonu klienta',
		'13'	=> 'ukončení zaměstnání',
		'14' 	=> 'vložení požadavku na zálohu',
		'15' 	=> 'odeslání zálohy',
		'16' 	=> 'hodnocení klienta při ukončení zaměstnání',
		'17' 	=> 'editace docházky klienta',
		'18' 	=> 'změna maximálky',
		'19' 	=> 'uložení docházky',
		'20' 	=> 'uzavření docházky',
		'21' 	=> 'autorizace docházky',
		'22' 	=> 'pozdržet výplatu',
		'23' 	=> 'výplata odeslána',
		'24' 	=> 'výplata cash doplatku',
		'25' 	=> 'umístění na požadavek pro nábor',
		'26' 	=> 'odstranění z požadavku pro nábor',
		'27' 	=> 'odeslání SMS kampaně'
));

// kampan
$this->set('campaign_item_stavs',array(
	'1' => 'Zadáno', 
	'2' => 'Email odeslán', 
	'3' => 'Nedovoláno', 
	'4' => 'Dovoláno',
	'5' => 'Předáno SM' 
));
$this->set('campaign_item_stavs2',array(
	'3' => 'Nedovoláno', 
	'4' => 'Dovoláno',
	'5' => 'Předáno SM' 
));



/* START Cms */
$this->set('select_umisteni_addon_list', array('cz'=>array(
	1 => 'Pod článkem',
	2 => 'Nad článkem'
)));

//kontaktni formular
$this->set('type_form_item', $this->type_form_item = array(
	'cz'	=> array(
	  0 => 'input',
		1 => 'textarea',
		2 => 'checkbox',
		3 => 'nadpis',
		4 => 'select'
	)
));
$this->set('lang_ano_ne', array(
  'cz'=>array(
  	1 => 'Ano',
  	0 => 'Ne'
    ),
  'en'=>array(
  	1 => 'Yes',
  	0 => 'No'
    )
));
/* END Cms */


$this->set('stav_importu_list', array(
	'1'	=> 'Neupravováno',
	'2'	=> 'K odstranění',
	'3' => 'Upraveno'
));

$this->set('kalkulace_stav_list',$this->kalkulace_stav_list = array(
	'' => 'Otevřeno',
	1 => 'Otevřeno',
	2 => 'Uzavřena docházka',
	3 => 'AV',
	4 => 'VY',
));
/*
1 => 'Otevřeno',
	2 => 'Uzavřena docházka',
	3 => 'Autorizovaná výplata',
	4 => 'Vyplaceno',
*/
$this->set('stav_company_auth_list',$this->stav_company_auth_list = array(
	0 => 'Čeká na opravy',
	1 => 'K autorizaci',
	2 => 'Autorizováno',
	3 => 'Vyplaceno',
));

$this->set('mesice_list',$this->mesice_list = array(
	1	=> 'Leden', 2	=> 'Únor', 3	=> 'Březen', 4	=> 'Duben', 5	=> 'Květen', 6	=> 'Červen', 7	=> 'Červenec', 8	=> 'Srpen', 9	=> 'Zaří', 10	=>'Říjen', 11	=>'Listopad', 12	=>'Prosinec'
));
$this->set('mesice_list2',$this->mesice_list2 = array(
	'01' => 'Leden', '02' => 'Únor', '03'	=> 'Březen', '04' => 'Duben', '05' => 'Květen', '06' => 'Červen', '07' => 'Červenec', '08'	=> 'Srpen', '09'	=> 'Zaří', '10'	=>'Říjen', '11'	=>'Listopad', '12'	=>'Prosinec'
));

$this->set('actual_years_list',$this->actual_years_list = array(
	'2007'=>'2007',
	'2008'=>'2008',
	'2009'=>'2009',	
	'2010'=>'2010',	
	'2011'=>'2011',	//
    '2012'=>'2012',	
    '2013'=>'2013',	
    '2014'=>'2014',	
    '2015'=>'2015',
    '2016'=>'2016'
));

$this->set('priorita_list',$this->priorita_list = array(
	1  => 'Priorita 1',
	2  => 'Priorita 2',
	3  => 'Priorita 3',
));
$this->set('stav_client_list',$this->stav_client_list = array(
	-1 => 'Internet',
	0  => 'Uchazeč',
	1  => 'Umístěn',
	2  => 'Zaměstnán',
));

$this->set('typ_externi_nabor',$this->typ_externi_nabor = array(
	1  => 'Předběžný',
	2  => 'Závazný',
));

$this->set('publikovani_typ_list',array(
	0 => 'Nepublikovat',
	1 => 'Publikovat pouze interně',
	2 => 'Publikovat externě',
	3 => 'Archivovat'
));

$this->set('filtr_uhrazeno',$this->stav_client_list = array(
	'0000-00-00'  => 'Neuhrazeno',
));

$this->set('mlm_group_list',$this->mlm_group_list = array(
	1  => 'L3',
	2  => 'L2',
	3  => 'L1',
));

$this->set('mlm_kraj_list',$this->mlm_kraj_list = array(
	1  => 'Slovensko',
	2  => 'Česká republika',
	3  => 'Poľsko',
	4  => 'Maďarsko',
));

$this->set('mlm_mena_list',$this->mlm_mena_list = array(
	1  => 'Sk & €',
	2  => 'Kč',
	3  => 'Zloté',
	4  => 'Forinty',
));


$this->set('mlm_krajina_list',$this->mlm_krajina_list = array(
	'SK'  => 'Slovensko',
	'CZ'  => 'Česká republika',
	'PL'  => 'Poľsko',
	'HU'  => 'Maďarsko',
));


$this->set('todo_stav_list',$this->todo_stav_list = array(
	1  	=> 'Zadáno',
	2  	=> 'V procesu',
	3  	=> 'Hotovo',
	4  	=> 'Dotaz',
	7  	=> 'Odpověď na dotaz',
	5  	=> 'Uzavřeno',
	6	=> 'K opravě',
	7	=> 'K odsouhlasení ceny',
	8	=> 'Cena odsouhlasená',
	9 	=> 'Interní Fastest konzultace o ceně',
	10 	=> 'Nutna konzultace',
	11 	=> 'Odloženo',
	12 	=> 'Zrušeno'
));

$this->set('todo_type_list',$this->todo_type_list = array(
	0  	=> 'Nespecifikována',
	1  	=> 'Klienti',
	2  	=> 'Firmy',
	3  	=> 'Odměny',
	4  	=> 'Ubytování',
	5  	=> 'Nábor',
	6  	=> 'Pomocník',
	7	=> 'Controlling',
	8	=> 'Administrace',
	9 	=> 'Správa stránek'
));

$this->set('vhodne_pro_list', array(
	1	=> 'muže',
	2	=> 'ženy',
	3	=> 'nezáleží'
));

$this->set('smennost_list', $this->smennost_list= array(
	1	=> 'jednosměnný provoz',
	2	=> 'dvousměnný provoz',
	3	=> 'třísměnný provoz'
));

$this->set('charakter_mzdy_list', array(
	1	=> 'hodinovka',
	2	=> 'úkol',
	3	=> 'hodinovka + úkol'
));

$this->set('ne_ano_list',array(
	2	=> 'Ne',
	1	=> 'Ano'
));

$this->set('ano_ne_list',  $this->ano_ne_list = array(
	1	=> 'Ano',
	2	=> 'Ne'
));
$this->set('ano_ne', array(
	1	=> 'Ne',
	0	=> 'Ano'
));
$this->set('ano_ne_checkbox', $this->ano_ne_checkbox = array(
	0	=> 'Ne',
	1	=> 'Ano'
));
$this->set('pohlavi_list', $this->pohlavi_list = array(
	1	=> 'Muž',
	2	=> 'Žena'
));
$this->set('hodnoceni_list', $this->hodnoceni_list = array(
    ''  => 'bez hodnocení',
	1	=> 1,
	2	=> 2,
	3	=> 3,
	4	=> 4,
	5	=> 5,
));
$this->set('price_for_list', array(
	''	=> 'Cena',
	1	=> 'Cena za den',
	2	=> 'Cena za měsíc'
));


//propusteni
$this->set('duvod_propusteni_list', $this->duvod_propusteni_list = array(
	1	=> 'Zrušení požadavků na zaměstnance',
	2	=> 'Výměna zaměstnance',
	3	=> 'Výpověď ze strany zaměstnance'
));

//uvolneni z pozadavku
$this->set('duvod_uvolneni_list', $this->duvod_uvolneni_list = array(
	1	=> 'Převod na zaměstnance',
	2	=> 'Byl vybrán jiný uchazač',
	3	=> 'Zrušení požadavku firmy',
	4	=> 'Nevyhověl požadavkům náboru',
	5	=> 'Nevyhověl požadavkům firmy',
	6	=> 'Problém >>',
));

$this->set('doporuceni_pro_nabor', $this->doporuceni_pro_nabor =  array(
	1	=> 'Ano',
	2	=> 'Ne',
	3	=> 'Rozhodně ne!'
));
$this->set('hodnoceni_list2', $this->hodnoceni_list2 =  array(
    ''  => 'bez hodnocení',
   	5	=> 'špatná',
	1	=> 'dobrá'
));
$this->set('hodnoceni_list3', $this->hodnoceni_list3 =  array(
    ''  => 'bez hodnocení',
	5	=> 'Ano',
	1	=> 'Ne'
));

//SMS

$this->set('status_sms_list',$this->status_sms_list = array(
	200 => 'Odesláno',
	333 => 'Neošetřená chyba při odesílání SMS',
	400 => 'Neplatné telefonní číslo'
));


/**
 * karta klienta
 */
$this->set('kind_of_client_activity', $this->kind_of_client_activity =  array(
	1	=> 'Kontakty Náboru',
	2	=> 'Zaměstnání',
	3	=> 'Řešení problému',
	4	=> 'Telefonát náboru',
	5	=> 'Problém >>',
    6   => 'Hodnoceni klienta',
    7	=> 'Kontakt koordinatora',
));

$this->set('activity_pomucky_list', $this->activity_pomucky_list =  array(
	1	=> 'Montérky',
	2	=> 'Boty',
	3	=> 'Rukavice',
	4	=> 'Tričko',
	5	=> 'Hygiena',
	6	=> 'Vesty'
));

/**
 * Nabor - objednavky
 */

$this->set('order_stav_list', $this->order_stav_list = array(
		-1 => 'Předběžný',
		1 => 'Objednáno',
		2 => 'Uzavřeno',
		3 => 'Zrušeno'
));
$this->set('order_type_list', $this->order_type_list = array(
		1 => 'zvysovanie poctu prac. miest',
		2 => 'nahrady',
		3 => 'znizovanie poctu miest'
));

$this->set('down_payments_type', $this->down_payments_type = array(
	0 => 'Převod na účet',
    1 => 'Hotově'
));



/**
 * clienti
 */
$this->set('client_en_type', $this->client_en_type = array(
	0 => 'Interní nábor',
    1 => 'Externí nábor'
));

$this->set('form_www_hledam', $this->form_www_hledam= 
    array(1=>'Krátkodobě',2=>'Brigáda',3=>'Dlouhodobě')
);
$this->set('form_www_profese', $this->form_www_profese= array(

        1 => 'dělnické pozice',
        2 => 'průmysl',
        3 => 'strojírenství',
        4 => 'stavebnictví',
        5 => 'výroba',
        6 => 'ostatní',
        
        7 => 'administrativa',
        8 => 'ekonomika',
        9 => 'HR',
        10 => 'IT a telekomunikace',
        11 => 'doprava a logistika',
        12 => 'obchod a marketing',
));

/**
 * Rozvazani PP - OPP
 */
$this->set('opp_rozvazat_pp', $this->opp_rozvazat_pp = array(
	1 => 'Srážka ze mzdy',
    2 => 'Vrácení Opp'
));

$this->set('opp_rozvazat_pp_ne', $this->opp_rozvazat_pp_ne = array(
	1 => 'Srážka ze mzdy'
));
 
$this->set('returned_opp_stav_list', $this->returned_opp_stav_list = array(
	0 => 'vyskladněno',
    1 => 'vráceno',
));


$this->set('currency_list', $this->currency_list = array(
    0 => ',- EUR',
    1 => ',- Kč',
    
));


/**
 * Nove kalkulace
 */
$this->set('list_stravne_typ', $this->list_stravne_typ = array(
    1=>'Příspěvek',
    2=>'Stravenky'
)); 

$this->set('list_mzda_typ', $this->list_mzda_typ = array(
    1=>'Hodinovka',
    2=>'Měsíčně'
));

$this->set('list_type_of_drawback_and_bonus', $this->list_type_of_drawback_and_bonus = array(
    1=>'Měsíčně',2=>'Hodinovka',3=>'% z ZHM',4=>'maximálka',5=>'Událost'
)); 

$this->set('list_type_of_extra_pay', $this->list_type_of_extra_pay = array(
   1=>'% z ZHM',2=>'Hodinovka',5=>'Událost'
));  
 
$this->set('list_type_of_loss_events', $this->list_type_of_loss_events = array(
   1=>'Pojistná událost',2=>'Škoda nehrazená pojištovnou',3=>'Bežné opotřebení majetku'
));

$this->set('currency_list2', $this->currency_list2 = array(
    0 => 'Nezvoleno',
    1 => 'CZK',
    2 => 'EUR'
));

$this->set('lektor_day_list', array(
    1 => 'Nezobrazuje',
    2 => 'Realizátoří',
    3 => 'Realizátoři - Asoc'
));

//skodove udalosti
$this->set('snp_stav_list', $this->snp_stav_list =  array(
    1 => 'Probíhajicí',
    2 => 'Ukončená'
));

$this->set('os_pohovor_list', $this->os_pohovor_list = array(
    1=>'Odpohovorovaný',
    2=>'Neodpohovorovaný'
));

$this->set('form_template_type',$this->form_template_type = array(
    1 => 'Evidence majetku',
    2 => 'Škodové události',
    3 => 'Evidence majetku - soukromé účely'
));

$this->set('audit_estate_type_list',$this->audit_estate_type_list = array(
    0 => 'Neuvedeno',
    1 => 'Majetek vracený v dobrém stavu',
    2 => 'Majetek poškozený'
));



//EB
$this->set('eb_stav_list',$this->eb_stav_list = array(
    1 => 'Vyhotovení',
    2 => 'Připrava a rozpočet',
    3 => 'Realizace',
    4 => 'Vyhodnocení',
    5 => 'Archiv',
));


$this->set('forma_kontaktu_coo_list',$this->forma_kontaktu_coo_list = array(
    1 => 'Osobné střetnutí',
    2 => 'Telefonicky'
));



?>