<?php
	$modul_name = 'Kalkulace - ConnectionTool';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'money_item_connection_tools' =>array(
				'name' 		=> 	'money_item_connection_tools',
				'url'		=>	'/money_item_connection_tools/',
				'caption'	=> 	'Kalkulace - ConnectionTool',
				'child'		=> 	null
			)
		)
	);

?>