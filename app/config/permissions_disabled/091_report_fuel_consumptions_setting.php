<?php
	$modul_name = 'Report - spotřeby vozidel';
	$modul_permission = array(
		'radio' => array(
			'index'			=>	'Zobrazení',
			'edit'			=>	'Editace',
			'fuel_trash'	=>	'Smazaní',
			'add'			=>	'Přidání',
            'attach'		=>	'Přílohy',
		),
		'checkbox' => array(
            'download_file'=>'Stahování příloh',
            'upload_file'=>'Nahrávaní příloh',
            'delete_file'=>'Smáznutí příloh'
        )
	);

	$modul_menu = array(		
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
			'report_fuel_consumptions' =>array(
				'name' 		=> 	'report_fuel_consumptions',
				'url'		=>	'/report_fuel_consumptions/',
				'caption'	=> 	'Report - spotřeby vozidel',
				'child'		=> 	null
			)
		)
	);



?>