<?php
$modul_name = 'Kampaně - Tipeři';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'email'	=>	'Odeslání emailu',
			'groups'=>	'Vytváření nové kampaně'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
			'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(
			'campaign_items_tippers' =>array(
				'name' 		=> 	'campaign_items_tippers',
				'url'		=>	'/campaign_items_tippers/',
				'caption'	=> 	'Kampaně - Tipeři',
				'child'		=> 	null
			)
		)
	);
	
	$basket = array('CampaignItem' => 'Kampaně');
?>