<?php
	$modul_name = 'EB - Docházky zaměstnanců';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'uzavrit_odpracovane_hodiny'=>	'Uzavření docházky',
            'type_works'=>	'Typ práce',
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
		'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'eb_employees' =>array(
				'name' 		=> 	'eb_employees',
				'url'		=>	'/eb_employees/',
				'caption'	=> 	'EB - Docházky zaměstnanců',
				'child'		=> 	'null'
			)
		)
	);
	

?>