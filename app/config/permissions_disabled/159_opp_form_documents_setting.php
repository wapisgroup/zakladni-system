<?php
	$modul_name = 'OPP formuáře dokumenty';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
            'items'=>	'Položky šablony',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'opp_templates' =>array(
				'name' 		=> 	'opp_form_documents',
				'url'		=>	'/opp_form_documents/',
				'caption'	=> 	'OPP formuláře dokumenty',
				'child'		=> 	null
			)
		)
	);

?>