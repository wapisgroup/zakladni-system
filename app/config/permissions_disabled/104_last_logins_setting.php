<?php
	$modul_name = 'Poslední přihlášení';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			//'edit'	=>	'Editace',
			//'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'last_logins' =>array(
				'name' 		=> 	'last_logins',
				'url'		=>	'/last_logins/',
				'caption'	=> 	'Poslední přihlášení',
				'child'		=> 	null
			)
		)
	);

?>