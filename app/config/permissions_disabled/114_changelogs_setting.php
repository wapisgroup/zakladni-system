<?php
	$modul_name = 'Changelog';


	$modul_permission = array(
		'radio' => array(
            'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(),	
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'changelogs' =>array(
				'name' 		=> 	'changelogs',
				'url'		=>	'/changelogs/',
				'caption'	=> 	'Changelog',
				'child'		=> 	null
			)
		)
	);

?>