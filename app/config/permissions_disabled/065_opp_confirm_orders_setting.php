<?php

	$modul_name = 'Objednávky na OPP - potvrzeni';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'trash'=>	'Smazaní',
            'return_order'=>'Zrušení potvrzení objednávky'
		),

		'checkbox' => array(
            'access_confirm_order'	=> 'Tl. Potvrzení objednávek',
		    'access_is_prepare'	=> 'Tl. Nachystané',  
		)

	);

	$modul_menu = array(
	    'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'opp_confirm_orders' =>array(
				'name' 		=> 	'opp_confirm_orders',
				'url'		=>	'/opp_confirm_orders/',
				'caption'	=> 	'Objednávky na OPP / potvrzení',
				'child'		=> 	null
			)
		)
	);
?>