<?php
	$modul_name = 'Soukromé firmy';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
            'add'			=>	'Přidání',
			'edit'			=>	'Editace',
			'trash'			=>	'Smazaní',
		),
		'checkbox' => array()
	);
	
	$modul_menu = array(
    	'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'company_foreigns' =>array(
				'name' 		=> 	'company_foreigns',
				'url'		=>	'/company_foreigns/',
				'caption'	=> 	'Soukromé firmy',
				'child'		=> 	null
			)
		)
	);

?>