<?php
	$modul_name = 'Report údržby';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
		),
		'checkbox' => array()
	);
	
	$modul_menu = array(
    	'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'report_maintenances' =>array(
				'name' 		=> 	'report_maintenances',
				'url'		=>	'/report_maintenances/',
				'caption'	=> 	'Report údržby',
				'child'		=> 	null
			)
		)
	);

?>