<?php

	$modul_name = 'Evidencia datumov nastup';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'export'=>	'Excel',
			
		),

		'checkbox' => array(
		)
	);

	$modul_menu = array(
	    'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'report_working_summary' =>array(
				'name' 		=> 	'report_working_summary',
				'url'		=>	'/report_working_summary/',
				'caption'	=> 	'Evidencia datumov nastup',
				'child'		=> 	null
			)
		)
	);
?>