<?php
	$modul_name = 'Celkem za AT';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'in_all_estates' =>array(
				'name' 		=> 	'in_all_estates',
				'url'		=>	'/in_all_estates/',
				'caption'	=> 	'Celkem za AT',
				'child'		=> 	null
			)
		)
	);

?>