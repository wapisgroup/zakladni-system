<?php

	$modul_name = 'Objednávky na MV';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'trash'=>	'Smazaní',
		),

		'checkbox' => array(
		)
	);

	$modul_menu = array(
	    'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'opp_orders' =>array(
				'name' 		=> 	'mv_orders',
				'url'		=>	'/mv_orders/',
				'caption'	=> 	'Objednávky na MV',
				'child'		=> 	null
			)
		)
	);
?>