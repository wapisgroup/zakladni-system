<?php
	$modul_name = 'Report - počet docházek';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(
			'report_count_working_hours' =>array(
				'name' 		=> 	'report_count_working_hours',
				'url'		=>	'/report_count_working_hours/',
				'caption'	=> 	'Report - počet docházek',
				'child'		=> 	null
			)
		)
	);

?>