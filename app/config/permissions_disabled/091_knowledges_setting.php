<?php
	$modul_name = 'Znalosti AT';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status',
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
		'name' 		=> 	'knowledges',
		'url'		=>	'#',
		'caption'	=> 	'Pomocník',
		'child'		=> 	array(
			'knowledges' =>array(
				'name' 		=> 	'knowledges',
				'url'		=>	'/knowledges/',
				'caption'	=> 	'Znalosti AT',
				'child'		=> 	'null'
			)
		)
	);
	

?>