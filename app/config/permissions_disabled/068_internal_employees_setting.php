<?php

	$modul_name = 'Docházky int. zaměstnanců';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'uzavrit_odpracovane_hodiny'=>	'Uzavření docházky',
            'autorizace_dochazky'=>	'Autorizování docházky',
			//'client_info'=>	'Detail Klienta',
            //'modify_from_date'=>	'Editace datumu nástupu/ukončení',
		),

		'checkbox' => array(
            'editace_maximalek'	=> 'Editace maximálek',
		)
	);

	$modul_menu = array(
	    'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'internal_employees' =>array(
				'name' 		=> 	'internal_employees',
				'url'		=>	'/internal_employees/',
				'caption'	=> 	'Docházky int. zaměstnanců',
				'child'		=> 	null
			)
		)
	);
?>