<?php
	$modul_name = 'Majetek podle osob';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'show'=>	'Detail',
            'print_detail'	=>	'Tisk'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'person_audit_estates' =>array(
				'name' 		=> 	'person_audit_estates',
				'url'		=>	'/person_audit_estates/',
				'caption'	=> 	'Majetek podle osob',
				'child'		=> 	null
			)
		)
	);

?>