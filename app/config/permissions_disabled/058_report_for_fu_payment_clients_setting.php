<?php
	$modul_name = 'Výplaty pro finanční účetní';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Vyplatit',
			'multi_edit'	=>	'Hrmonadné vyplácení',
			'stop_status' =>	'Pozastavení výplaty',
			'show'	=>	'Detail docházky',
			'export_excel'=>	'Export excel',
			//'edit'	=>	'Editace',
			//'add'	=>	'Přidání',
			//'trash'=>	'Smazaní',
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
		 'name' 		=> 	'modul_employees',
		'url'		=>	'#',
		'caption'	=> 	'Zaměstnanci',
		'child'		=> 	array(	
			'report_for_fu_payment_clients' =>array(
				'name' 		=> 	'report_for_fu_payment_clients',
				'url'		=>	'/report_for_fu_payment_clients/',
				'caption'	=> 	'Výplaty pro finanční účetní',
				'child'		=> 	'null'
			)
		)
	);
	

?>