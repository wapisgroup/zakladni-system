<?php
	$modul_name = 'Škodové události';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
            'add'			=>	'Přidání',
			'edit'			=>	'Editace',
            'show'	=>	'Detail',
			'trash'			=>	'Smazaní',
			'attach'		=>	'Přílohy',
            'export_excel'=>	'Export excel',
            'print'=>	'Tisk',
		),
		'checkbox' => array(),
        'select' => array(
			'at_project_id'=> array(
				'caption' 	=> 'Napojení na projekt',
				'data_list'	=> 'project_list'
			)
         )   
	);
	
	$modul_menu = array(
    	'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'audit_estates_loss_events' =>array(
				'name' 		=> 	'audit_estates_loss_events',
				'url'		=>	'/audit_estates_loss_events/',
				'caption'	=> 	'Škodové události',
				'child'		=> 	null
			)
		)
	);

?>