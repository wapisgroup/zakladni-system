<?php
	$modul_name = 'Měsíční fakturace pronájmu - soukromé účely';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'show'	=>	'Detail',
            'fakturovano'=>'Fakturace'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'monthly_private_statements' =>array(
				'name' 		=> 	'monthly_private_statements',
				'url'		=>	'/monthly_private_statements/',
				'caption'	=> 	'Měsíční fakturace pronájmu - soukromé účely',
				'child'		=> 	null
			)
		)
	);

?>