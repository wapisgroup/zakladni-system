<?php
	$modul_name = 'Nastavení prohřešků';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'misconducts' =>array(
				'name' 		=> 	'misconducts',
				'url'		=>	'/misconducts/',
				'caption'	=> 	'Nastavení prohřešků',
				'child'		=> 	null
			)
		)
	);

?>