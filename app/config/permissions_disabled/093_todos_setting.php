<?php
	$modul_name = 'Úkolovník ATEP';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'knowledges',
		'url'		=>	'#',
		'caption'	=> 	'Pomocník',
		'child'		=> 	array(
			'cms_users' =>array(
				'name' 		=> 	'todos',
				'url'		=>	'/todos/',
				'caption'	=> 	'Úkolovník ATEP',
				'child'		=> 	null
			)
		)
	);

?>