<?php
	$modul_name = 'Měsíční fakturace pronájmu';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'show'	=>	'Detail',
            'fakturovano_client'=>'Fakturace',
            'fakturovano'=>'Fakturace'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'monthly_statements' =>array(
				'name' 		=> 	'monthly_client_statements',
				'url'		=>	'/monthly_client_statements/',
				'caption'	=> 	'Měsíční fakturace osob',
				'child'		=> 	null
			)
		)
	);

?>