<?php

	$modul_name = 'Klienti z www stranek Jobs';



	$modul_permission = array(

		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			//'auth'=>	'Autorizovat klienta',
			//'overeni'=>	'Ověření klienta'

		),

		'checkbox' => array(
		),

		'select' => array(
		)

	);

	$modul_menu = array(
		'name' 		=> 	'report_requirements_for_recruitments',
		'url'		=>	'#',
		'caption'	=> 	'Nábor',
		'child'		=> 	array(
			'import_clients' => array(
				'name' 		=> 	'import_client_jobs',
				'url'		=>	'/import_client_jobs/',
				'caption'	=> 	'Klienti z www stránek Jobs Partners',
				'child'		=> 	null
			)
		)
	);
?>