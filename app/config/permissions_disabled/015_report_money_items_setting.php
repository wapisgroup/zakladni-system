<?php
	$modul_name = 'Reporty - kalkulace firem';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'show'	=>	'Detail',
			'edit'	=>	'Editace',
			//'trash'=>	'Smazaní',
			//'status'=>	'Status'
            'export_excel'=>	'Export excel',
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(
			'report_money_items' =>array(
				'name' 		=> 	'report_money_items',
				'url'		=>	'/report_money_items/?filtration_Company-stat_id=1',
				'caption'	=> 	'Kalkulace podniku - CZ',
				'child'		=> 	null
			),
            'report_money_items2' =>array(
				'name' 		=> 	'report_money_items2',
				'url'		=>	'/report_money_items/?filtration_Company-stat_id=2',
				'caption'	=> 	'Kalkulace podniku - SK',
				'child'		=> 	null
			)
		)
	);

?>