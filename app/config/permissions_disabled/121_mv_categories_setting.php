<?php
$modul_name = 'Kategorie MV';


$modul_permission = array(
    'radio' => array(
        'index' => 'Zobrazení',
        'add' => 'Přidat',
        'edit' => 'Editace',
        'trash' => 'Smazaní'
    ),
    'checkbox' => array(

    ),

);

$modul_menu = array(
    'name' => 'codebooks',
    'url' => '#',
    'parent' => true,
    'caption' => 'Čísleníky',
    'child' => array(
        'mv_items' => array(
            'name' => 'mv_categories',
            'url' => '/mv_categories/',
            'caption' => 'Kategorie MV',
            'child' => null
        )
    )
);

?>