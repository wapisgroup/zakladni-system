<?php
	$modul_name = 'OPP číselník';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'opp_categories' =>array(
				'name' 		=> 	'opp_categories',
				'url'		=>	'/opp_categories/',
				'caption'	=> 	'OPP číselník',
				'child'		=> 	null
			)
		)
	);

?>