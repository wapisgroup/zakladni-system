<?php
	$modul_name = 'Můj majetek';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'manage_estate'=>  'Správa majetku'
		),
		'checkbox' => array()
	);
	
	$modul_menu = array(
    	'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'my_audit_estates' =>array(
				'name' 		=> 	'my_audit_estates',
				'url'		=>	'/my_audit_estates/',
				'caption'	=> 	'Můj majetek',
				'child'		=> 	null
			)
		)
	);

?>