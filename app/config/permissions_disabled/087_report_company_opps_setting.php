<?php
	$modul_name = 'Report vydaných OPP za podniky';
	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
		),
		'checkbox' => array()
	);


	$modul_menu = array(
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
			'report_company_opps' =>array(
				'name' 		=> 	'report_company_opps',
				'url'		=>	'/report_company_opps/',
				'caption'	=> 	'Report vydaných OPP za podniky',
				'child'		=> 	'null'
			)
		)
	);
?>