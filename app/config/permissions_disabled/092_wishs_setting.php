<?php
	$modul_name = 'Kniha přání ATEP';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
		'name' 		=> 	'knowledges',
		'url'		=>	'#',
		'caption'	=> 	'Pomocník',
		'child'		=> 	array(
			'wishs' =>array(
				'name' 		=> 	'wishs',
				'url'		=>	'/wishs/',
				'caption'	=> 	'Kniha přání',
				'child'		=> 	'null'
			)
		)
	);
	

?>