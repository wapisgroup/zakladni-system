<?php
	$modul_name = 'Žádosti o vyřazený majetek';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
            'show'=>'Detail',
            'elimination'=>'Vyřadit majetek'
		),
		'checkbox' => array()
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'audit_estate_elimination_requests' =>array(
				'name' 		=> 	'audit_estate_elimination_requests',
				'url'		=>	'/audit_estate_elimination_requests/',
				'caption'	=> 	'Žádosti o vyřazený majetek',
				'child'		=> 	null
			)
		)
	);

?>