<?php
	$modul_name = 'New - docházky zaměstnanců';


	$modul_permission = array(
			'radio' => array(

			'index'	=>	'Zobrazení',

			'edit'	=>	'Editace',

			'uzavrit_odpracovane_hodiny'=>	'Uzavření docházky',

			'ucetni_dokumentace'=>	'Učetní  dokumentace',

			'client_info'=>	'Detail Klienta',
            
            'modify_from_date'=>	'Editace datumu nástupu/ukončení',

		),

		'checkbox' => array(

		  'editable_money_item_workposition'	=> 'Možnost editovat profesi a formu odmeny na kartě docházky',

		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'new_employees' =>array(
				'name' 		=> 	'new_employees',
				'url'		=>	'/new_employees/',
				'caption'	=> 	'New - docházky zaměstnanců',
				'child'		=> 	null
			)
		)
	);

?>