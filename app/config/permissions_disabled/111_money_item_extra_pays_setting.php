<?php
	$modul_name = 'Kalkulace - nastavení příplatků';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace klientu',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'money_item_extra_pays' =>array(
				'name' 		=> 	'money_item_extra_pays',
				'url'		=>	'/money_item_extra_pays/',
				'caption'	=> 	'Kalkulace - nastavení příplatků',
				'child'		=> 	null
			)
		)
	);

?>