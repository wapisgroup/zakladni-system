<?php
	$modul_name = 'Šablony požadavků pro nábor';

	$modul_permission = array(
		'radio' => array(
			'index'		=>	'Zobrazení',
			'show'		=>	'Detail'

		),
		'checkbox' => array(
		
		),
		
		'select' => array(
			'group_id'=> array(
				'caption' 	=> 'Typ napojení',
				'data'		=> array(
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'client_manager_id'	=>	'client_manager_id',
					'coordinator_double'	=>	'coordinator_double',
                    'coordinator_new'	=>	'coordinator_new'
				)
			)
		)
	
	);
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(	
			'report_company_templates' =>array(
				'name' 		=> 	'report_company_templates',
				'url'		=>	'/report_company_templates/',
				'caption'	=> 	'Šablony požadavků pro nábor',
				'child'		=> 	null
			)
		)
	);
	

?>