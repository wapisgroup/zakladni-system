<?php
$modul_name = 'Kampaně - Call Centrum';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'message'=>	'Přidání aktivity'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
			'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Firmy',
		'child'		=> 	array(
			'campaign_items_callcentrums' =>array(
				'name' 		=> 	'campaign_items_callcentrums',
				'url'		=>	'/campaign_items_callcentrums/',
				'caption'	=> 	'Kampaně - Call Centrum',
				'child'		=> 	null
			),
		)
	);
	
	$basket = array('CampaignItem' => 'Kampaně');
?>