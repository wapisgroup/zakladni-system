<?php
	$modul_name = 'Soukromé osoby';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
            'add'			=>	'Přidání',
			'edit'			=>	'Editace',
			'trash'			=>	'Smazaní',
		),
		'checkbox' => array()
	);
	
	$modul_menu = array(
    	'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'client_foreigns' =>array(
				'name' 		=> 	'client_foreigns',
				'url'		=>	'/client_foreigns/',
				'caption'	=> 	'Soukromé osoby',
				'child'		=> 	null
			)
		)
	);

?>