<?php
	$modul_name = 'Měsiční výkaz automobilů';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
            'add'			=>	'Přidání',
			'edit'			=>	'Editace',
			'trash'			=>	'Smazaní',
		),
		'checkbox' => array()
	);
	
	$modul_menu = array(
    	'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'car_monthly_statements' =>array(
				'name' 		=> 	'car_monthly_statements',
				'url'		=>	'/car_monthly_statements/',
				'caption'	=> 	'Měsiční výkaz automobilů',
				'child'		=> 	null
			)
		)
	);

?>