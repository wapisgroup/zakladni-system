<?php
	$modul_name = 'Objednávky firem';

	$modul_permission = array(
		'radio' => array(
			'index'		=>	'Zobrazení',
			'show'		=>	'Detail',
			'edit'		=>	'Formuláře pro WWW'
		),
		'checkbox' => array(
		
		),
		
		'select' => array(
			'group_id'=> array(
				'caption' 	=> 'Typ napojení',
				'data'		=> array(
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'client_manager_id'	=>	'client_manager_id',
					'coordinator_double'	=>	'coordinator_double',
                    'coordinator_new'	=>	'coordinator_new'
				)
			)
		)
	
	);
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Podniky(zakázky)',
		'child'		=> 	array(	
			'report_company_orders' =>array(
				'name' 		=> 	'report_company_orders',
				'url'		=>	'/report_company_orders/',
				'caption'	=> 	'Objednávky v podnicích',
				'child'		=> 	null
			)
		)
	);
	

?>