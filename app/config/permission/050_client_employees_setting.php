<?php

	$modul_name = 'Leasingový zaměstnanci';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'edit'	=>	'Editace',

			'add'	=>	'Přidání',

			'trash'=>	'Smazaní',

			'attach'=>	'Přílohy',

			'add_recruiter'=>	'Přidání recruitera',

			'add_historie'=>	'Přidání historie',

			'add_hodnoceni'=>	'Přidání hodnocení',

			'rozvazat_prac_pomer'=>	'Odebrání ze zaměstnaní',
			//'modify_from_date'=>	'Editace datumu nástupu',

			'zmena_pp'=>	'Změna pracovního poměru',

            'ucetni_dokumentace'=>	'Učetní  dokumentace',
            'add_activity'=>	'Přidat aktivitu',
            'add_opp'=>	'Přiděleni OPP',
            'add_doctor'=>	'Přiděleni lékařské prohlídky',
            'opp_transfer'=>	'Přenos OPP na jiného klienta',
            
            'export_excel'=>	'Export excel',
            'transport_card'=>	'Tisk karet dopravy',

		),

		'checkbox' => array(

		

		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(

					'self_manager_id'	=>	'self_manager_id',

					'coordinator_id'	=>	'coordinator_id',

					'coordinator_double'	=>	'coordinator_double',

					'client_manager_id'	=>	'client_manager_id'

				)

			)

		)

	);

	$modul_menu = array(

	    'name' 		=> 	'modul_employees',

		'url'		=>	'#',

		'caption'	=> 	'Zaměstnanci',

		'child'		=> 	array(

			'client_employees' => array(

				'name' 		=> 	'client_employees',

				'url'		=>	'/client_employees/',

				'caption'	=> 	'Leasingový zaměstnanci',

				'child'		=> 	null

			)

		)

	);

	



?>