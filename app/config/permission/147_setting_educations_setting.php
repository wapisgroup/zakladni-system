<?php
	$modul_name = 'Nastavení vzdělání';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_career_items' =>array(
				'name' 		=> 	'setting_educations',
				'url'		=>	'/setting_educations/',
				'caption'	=> 	'Nastavení vzdělání',
				'child'		=> 	null
			)
		)
	);

?>