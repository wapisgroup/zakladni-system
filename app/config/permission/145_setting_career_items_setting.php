<?php
	$modul_name = 'Nastavení pracovních pozic';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_career_items' =>array(
				'name' 		=> 	'setting_career_items',
				'url'		=>	'/setting_career_items/',
				'caption'	=> 	'Nastavení pracovních pozic',
				'child'		=> 	null
			)
		)
	);

?>