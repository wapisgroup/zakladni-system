<?php
	$modul_name = 'Nastavení emailových šablon';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
            'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'administration',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Administrace',
		'child'		=> 	array(
			'cms_users' =>array(
				'name' 		=> 	'mail_templates',
				'url'		=>	'/mail_templates/',
				'caption'	=> 	'Nastavení emailových šablon',
				'child'		=> 	null
			)
		)
	);

?>