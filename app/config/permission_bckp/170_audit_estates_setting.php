<?php
	$modul_name = 'Evidence majetku';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidat',
			'delete_estate'=>	'Smazaní',
            'print'=>	'Tisk',
			'manage_estate'=>	'Správa majetku',
            'manage_estate_admin'=>	'Úprava historie',
            //'add_estate'=>'Přiřazení majetku',
            'history'=>'Historie přiřazení majetku',
            'export_excel'=>	'Export excel',
		),
		'checkbox' => array(
		  'allow_save'	=> 'Tlačítko uložit na kartě majetku',
          'show_pin'	=> 'Zobrazení pinu Shell karty',
          'delete_stav'	=> 'Možnost smazání stavu majetku'
		  
		),
        'select' => array(
			'at_project_id'=> array(
				'caption' 	=> 'Napojení na projekt',
				'data_list'	=> 'project_list'
			)
         )  
	);
	
	$modul_menu = array(
		'name' 		=> 	'modul_ev_majektu',
		'url'		=>	'#',
		'caption'	=> 	'Evidence Majetku',
		'child'		=> 	array(
			'audit_estates' =>array(
				'name' 		=> 	'audit_estates',
				'url'		=>	'/audit_estates/',
				'caption'	=> 	'Evidence majetku',
				'child'		=> 	null
			)
		)
	);

?>