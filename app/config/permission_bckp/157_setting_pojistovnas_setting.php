<?php
	$modul_name = 'Seznam pojistoven';


	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_pojistovnas' =>array(
				'name' 		=> 	'setting_pojistovnas',
				'url'		=>	'/setting_pojistovnas/',
				'caption'	=> 	'Seznam pojistoven',
				'child'		=> 	null
			)
		)
	);

?>