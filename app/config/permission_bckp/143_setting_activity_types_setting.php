<?php
	$modul_name = 'Nastavení typů aktivit';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_activity_types' =>array(
				'name' 		=> 	'setting_activity_types',
				'url'		=>	'/setting_activity_types/',
				'caption'	=> 	'Nastavení typů aktivit',
				'child'		=> 	null
			)
		)
	);

?>