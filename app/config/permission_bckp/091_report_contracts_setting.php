<?php
	$modul_name = 'Smlouvy';
	$modul_permission = array(
		'radio' => array(
			'index'			=>	'Zobrazení',
			'edit'			=>	'Editace',
			'contract_trash'=>	'Smazaní',
			'add'			=>	'Přidání',
            'attach'		=>	'Přílohy',
		),
		'checkbox' => array(
            'download_file'=>'Stahování příloh',
            'upload_file'=>'Nahrávaní příloh',
            'delete_file'=>'Smáznutí příloh'
        )
	);

	$modul_menu = array(		
		'name' 		=> 	'modul_reports',
		'url'		=>	'#',
		'caption'	=> 	'Reporty',
		'child'		=> 	array(
			'report_contracts' =>array(
				'name' 		=> 	'report_contracts',
				'url'		=>	'/report_contracts/',
				'caption'	=> 	'Smlouvy',
				'child'		=> 	null
			)
		)
	);



?>