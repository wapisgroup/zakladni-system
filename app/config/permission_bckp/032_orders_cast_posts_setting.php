<?php

	$modul_name = 'Obsazeni objednávek';



	$modul_permission = array(

		'radio' => array(

			'index'	=>	'Zobrazení',

			'show'	=>	'Detail',

			'pozice'	=>	'Obsazení pracovních pozic'

		),

		'checkbox' => array(
		    'move_to_zam'	=> 'Převod na zaměstnance',
		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'client_manager_id'	=>	'client_manager_id',
					'coordinator_double'	=>	'coordinator_double',
                    'coordinator_new'	=>	'coordinator_new'
				)

			)

		)

	);

	$modul_menu = array(

		'name' 		=> 	'report_requirements_for_recruitments',

		'url'		=>	'#',

		'caption'	=> 	'Nábor',


		'child'		=> 	array(	

			'orders_cast_posts' =>array(

				'name' 		=> 	'orders_cast_posts',

				'url'		=>	'/orders_cast_posts/',

				'caption'	=> 	'Obsazeni objednávek',

				'child'		=> 	null

			)

		)

	);

	



?>