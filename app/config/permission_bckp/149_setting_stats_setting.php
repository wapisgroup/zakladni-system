<?php
	$modul_name = 'Definice států';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní',
			'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_stats' =>array(
				'name' 		=> 	'setting_stats',
				'url'		=>	'/setting_stats/',
				'caption'	=> 	'Definice států',
				'child'		=> 	null
			)
		)
	);

?>