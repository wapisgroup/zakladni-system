<?php
	$modul_name = 'Nastavení kategorií pracovních pozic';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'add'	=>	'Přidat',
			'edit'	=>	'Editace',
			'trash'=>	'Smazaní'
		),
		'checkbox' => array(
		
		),
		
	);
	
	$modul_menu = array(
		'name' 		=> 	'codebooks',
		'url'		=>	'#',
		'parent'	=>	true,
		'caption'	=> 	'Čísleníky',
		'child'		=> 	array(
			'setting_career_cats' =>array(
				'name' 		=> 	'setting_career_cats',
				'url'		=>	'/setting_career_cats/',
				'caption'	=> 	'Nastavení kategorií pracovních pozic',
				'child'		=> 	null
			)
		)
	);

?>