<?php
	$modul_name = 'Aktivity ve firmách';

	$modul_permission = array(
		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace'
			//'trash'=>	'Smazaní',
			//'status'=>	'Status'
		),
		'checkbox' => array(
		
		)
	);
	$modul_menu = array(
		'name' 		=> 	'companies',
		'url'		=>	'/companies/',
		'caption'	=> 	'Podniky(zakázky)',
		'child'		=> 	array(
			'company_activities' =>array(
				'name' 		=> 	'company_activities',
				'url'		=>	'/company_activities/',
				'caption'	=> 	'Aktivity v podnicích',
				'child'		=> 	null
			)
		)
	);

?>