<?php

	$modul_name = 'Klienti - všichni pro interní nábor';



	$modul_permission = array(

		'radio' => array(
			'index'	=>	'Zobrazení',
			'edit'	=>	'Editace',
			'add'	=>	'Přidání',
			'add_express'	=>	'Přidání Express',
			'trash'=>	'Smazaní',
			'attach'=>	'Přílohy',
			'add_recruiter'=>	'Přidání recruitera',
			'add_historie'=>	'Přidání historie',
			'add_hodnoceni'=>	'Přidání hodnocení',
			'rozvazat_prac_pomer'=>	'Odebrání ze zaměstnaní',
			'zmena_pp'=>	'Změna pracovního poměru',
			'duplicity_client'=>	'Vícenásobné zaměstnání',
			'add_activity'=>	'Přidat aktivitu',
            'employ_on_project'=>	'Zaměstnant do projektu',
            'export_excel'=>	'Export excel',
            'client_buy' => 'Objednávka klienta',
		),

		'checkbox' => array(

		  

		),

		'select' => array(

			'group_id'=> array(

				'caption' 	=> 'Typ napojení',

				'data'		=> array(
					'self_manager_id'	=>	'self_manager_id',
					'coordinator_id'	=>	'coordinator_id',
					'coordinator_double'	=>	'coordinator_double',
					'client_manager_id'	=>	'client_manager_id',
                    'cms_user_id' => 'Externí naborář'
				)

			)

		)

	);

	$modul_menu = array(
		'name' 		=> 	'report_requirements_for_recruitments',

		'url'		=>	'#',

		'caption'	=> 	'Nábor',

		'child'		=> 	array(

			'int_clients' => array(

				'name' 		=> 	'int_clients',

				'url'		=>	'/int_clients/',

				'caption'	=> 	'Klienti - všichni pro interní nábor',

				'child'		=> 	null

			),

			//'int_clients_zamestnans' => array(

//				'name' 		=> 	'int_clients',

//				'url'		=>	'/int_clients/?filtration_ClientView-stav=2',

//				'caption'	=> 	'Klienti - moji zamestnani pro interní nábor',

//				'child'		=> 	null

//			)

		)

	);

	



?>