<?php
class Footer{
    private $type;
    private $filler_1;
    private $filler_2;
    private $send_date;
    private $close_chr;
    private $count_payments;
    private $control_sum;
    
    static $max_lenght_output = 352;
    
    /**
     * nastaveni promemnych jak se bude sestavovat string
     */
    static $create_sequency_name = Array(
        'type',
        'filler_1',
        'send_date',
        'count_payments',
        'control_sum',
        'filler_2',
        'close_chr'
    );
    
    function Footer($date = null, $count_payments = null, $control_sum = null){
        /**
         * nastaveni defaultnich hodnot
         */
         
        $this->type = "TI";
        
        /**
         * filler 1
         * nyni nevyuzito pouzity mezery
         */
        $this->filler_1 = $this->create_null_string(0,9);
        
        /**
         * nastaveni datum odeslani
         * defaultne aktualni datum
         * datum musi byt ve formatu rrmmdd
         */
        $this->send_date = ($date != null ? $date : date('ymd'));
        
        /**
         * pocet zaznamu
         */
        $this->count_payments = ($count_payments != null ? str_pad($count_payments,6,0,STR_PAD_LEFT) : $this->create_null_string(0,6));
         
        /**
         * kontrolni soucet 
         */
        $this->control_sum = ($control_sum != null ? str_pad($control_sum,16,0,STR_PAD_LEFT).'00' : $this->create_null_string(0,18));
         
         
         /**
          * filler 2
          * nyni nevyuzito pouzity mezery
          */
        $this->filler_2 = $this->create_null_string(" ",310);
        
        
        /**
         * koncový znak
         * CRLF - nyni prazdny
         */
        $this->close_chr = "\n"; 
        
        
    }
    
      
    public function create(){
        $outline = null;

        foreach(self::$create_sequency_name as $item){
           // echo "pridavam : ".$item." ->".$this->$item.'<br />';
            $outline .= $this->$item;
        }
        
        /**
         * osetreni zda vysledek nepresahl maximalni pocet bytu(znaku) 
         */
        if(strlen($outline) != self::$max_lenght_output){
            echo "Chybne nastavene prommene, vystup nema pevne dany pocet znaku(".self::$max_lenght_output."). Delka je ".strlen($outline);         
        }
        else {
            return $outline;
        }
    }
    
    private function create_null_string($type,$lenght = 1){
        return  str_repeat($type,$lenght);
    }
        
}
?>