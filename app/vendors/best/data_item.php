<?php
class DataItem{
    private $type;
    private $no;
    private $created;
    private $date_to_pay;
    private $currency_iso;
    private $amount;
    private $operation;
    private $currency_iso_contra_account;
    private $code_conversation;
    private $ks;
    private $message_partner;
    private $filler_1;
    private $code_bank_committer;
    private $account_committer;
    private $vs_committer;
    private $ss_committer;
    private $comment_committer;
    private $filler_2;
    private $code_bank_contra_account;
    private $account_contra_account;
    private $vs_contra_account;
    private $ss_contra_account;
    private $comment_contra_account;
    private $express;
    private $forex;
    private $filler_3;
    private $close_chr;
    
    static $max_lenght_output = 352;
    
    /**
     * nastaveni promemnych jak se bude sestavovat string
     */
    static $create_sequency_name = Array(
        'type',
        'no',
        'created',
        'date_to_pay',
        'currency_iso',
        'amount',
        'operation',
        'currency_iso_contra_account',
        'code_conversation',
        'ks',
        'message_partner',
        'filler_1',
        'code_bank_committer',
        'account_committer',
        'vs_committer',
        'ss_committer',
        'comment_committer',
        'filler_2',
        'code_bank_contra_account',
        'account_contra_account',
        'vs_contra_account',
        'ss_contra_account',
        'comment_contra_account',
        'express',
        'forex',
        'filler_3',
        'close_chr'
    );
    
    function DataItem($account_committer, $vs){
        /**
         * nastaveni defaultnich hodnot
         */
         $this->type = "01";
          
        /**
         * nastaveni datum vyvoreni
         * defaultne aktualni datum
         * datum musi byt ve formatu rrrrmmdd
         */
         $this->created = date('Ymd');
           
        /**
         * nastaveni ISO kodu meny
         * nastaveno na CZK
         */
         $this->currency_iso = 'CZK'; 
         
        /**
         * nastavnei typu uhrady
         * 0 - uhrada, 1 - inkaso
         */
         $this->operation = '0';
        
        /**
         * ISO kod meny protiuctu 
         * pokud mezery nebo nuly potom měna protiúčtu = měna účtu
         */ 
         $this->currency_iso_contra_account = $this->create_null_string(" ",3); 
        
        /**
         * kod konverzace
         * Pokud „P“ potom částka v měně protiúčtu jinak částka v měně účtu
         */
         $this->code_conversation = " "; 
        
        /**
         * konstantni symbol
         * nastaven na 0308
         */
         $this->ks = str_pad('0308',10,0,STR_PAD_LEFT); 
        
        /**
         * zprava pro partnera(prijemce)
         */
         $this->message_partner = $this->create_null_string(" ",140);
         
        /**
         * filler 1
         * nyni nevyuzito pouzity mezery
         */
         $this->filler_1 = $this->create_null_string(" ",3);

        /**
         * kod banky prikazce
         */
         $this->code_bank_committer = "0100"; 

        /**
         * cislo uctu prikazce
         * AT tedy
         */
         $this->account_committer = str_pad($account_committer,16,0,STR_PAD_LEFT); 

        /**
         * variabilni symbol prikazce
         * dle CNB bude prepsan ss partnera
         */
         $this->vs_committer = str_pad($vs,10,0,STR_PAD_LEFT); 

        /**
         * specificky symbol prikazce
         * dle CNB bude prepsan ss partnera
         */
         $this->ss_committer  = $this->create_null_string(0,10);
         
        /**
         * poznamka prikazce
         */ 
        // $this->comment_committer = $this->create_null_string(" ",30);
    
        /**
         * filler 2
         * nyni nevyuzito pouzity mezery
         */
         $this->filler_2 = $this->create_null_string(" ",3);
       
        /**
         * variabilni symbol prijemce
         * dle CNB bude prepsan ss partnera
         */
         $this->vs_contra_account = str_pad($vs,10,0,STR_PAD_LEFT); 

        /**
         * specificky symbol prijemce
         * dle CNB bude prepsan ss partnera
         */
         $this->ss_contra_account  = $this->create_null_string(0,10);
         
        /**
         * poznamka prijemce
         */ 
         $this->comment_contra_account = $this->create_null_string(" ",30);
    
        /**
         * E = expresni platba
         * A = expresni platba se Swiftem
         * jinak standart
         */
         $this->express = " ";
        
        /**
         * Y = pro případ domluveného kurzu jinak dle kurzovního lístku 
         * kontrola zda je smlouva s dealingem
         */
         $this->forex = " ";
         
        /**
         * filler 3
         * rezerva
         */
         $this->filler_3 = $this->create_null_string(" ",7);
        
        /**
         * koncový znak
         * CRLF - nyni prazdny
         */
        $this->close_chr = "\n";         
    }
    
    
    /**
     * funkce pro vytvoreni platby
     * potrebne veci jsou
     * @param cislo platby - jedinecne
     * @param datum splatnosti
     * @param castka 
     * @param kod banky 
     * @param ucet
     * @param popis prikazce
     */
    public function add_data($no,$date_to_pay,$amount,$code_bank_contra_account,$account_contra_account,$message_commiter){
        
        /**
         * nastaveni poradoveho cisla
         */
         $this->no = str_pad($no,5,0,STR_PAD_LEFT); 
            
        /**
         * nastaveni datum splatnosti
         * datum musi byt ve formatu rrrrmmdd
         */
         $this->date_to_pay = $date_to_pay;
     
        /**
         * nastaveni castky platby
         * bez desetinych mist momentalne
         */
         $this->amount = str_pad($amount,13,0,STR_PAD_LEFT).'00';
         
        /**
         * kod banky prijemce
         */
         $this->code_bank_contra_account = $code_bank_contra_account; 

        /**
         * cislo uctu prijemce
         * AT tedy
         */
         $this->account_contra_account = str_pad($account_contra_account,16,0,STR_PAD_LEFT); 
         
        /**
         * nastaveni popisu prikazce
         */
         $this->comment_committer = str_pad($message_commiter,30," ");
         if(strlen($this->comment_committer) > 30)
            $this->comment_committer = substr($this->comment_committer,0,30); 
 
        /**
         * vytvoreni radku prikazu
         */            
         return self::create();
    }
    
    
    private function create(){
        $outline = null;

        foreach(self::$create_sequency_name as $item){
            //echo "pridavam : ".$item." ->".$this->$item.'('.strlen($this->$item).')<br />';
            $outline .= $this->$item;
        }
        
        /**
         * osetreni zda vysledek nepresahl maximalni pocet bytu(znaku) 
         */
        if(strlen($outline) != self::$max_lenght_output){
            return "Chybne nastavene prommene, vystup nema pevne dany pocet znaku(".self::$max_lenght_output."). Delka je ".strlen($outline);         
        }
        else {
            return $outline;
        }
    }
    
    private function create_null_string($type,$lenght = 1){
        return  str_repeat($type,$lenght);
    }
        
}
?>