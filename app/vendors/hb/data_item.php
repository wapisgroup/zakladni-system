<?php
class DataItem2{
    private $no;
    private $date_to_pay;
    // prikazce
    private $account_committer;
    private $prefix_account_committer;
    //prijemce
    private $prefix_account_contra_account;
    private $account_contra_account;
    private $code_bank_contra_account;
    
    //dalsi nastaveni
    private $amount;
    private $currency_iso;
    private $vs;
    private $ks;
    private $ss;
    private $comment;
    private $close_chr;
    
    //prasne
    private $breaking;
    
    /**
     * nastaveni promemnych jak se bude sestavovat string
     */
    static $create_sequency_name = Array(
       // 'no',
        'date_to_pay',
        'prefix_account_committer',
        'account_committer',
        'prefix_account_contra_account',
        'account_contra_account',
        'amount',
        'currency_iso',
        'code_bank_contra_account',
        'vs',
        'ks',
        'ss',
        'comment',
        'breaking',
        'breaking'
    );
    
    function DataItem2($prefix_account_committer,$account_committer, $vs){
       
        /**
         * nastaveni ISO kodu meny
         * nastaveno na EUR
         */
         $this->currency_iso = 'EUR'; 
       
        /**
         * konstantni symbol
         * nastaven na 0308
         */
         $this->ks = str_pad('0308',10,0,STR_PAD_LEFT); 
        

        /**
         * prefix uctu prikazce
         * AT SK tedy
         */
         $this->prefix_account_committer = str_pad($prefix_account_committer,6,0,STR_PAD_LEFT); 
         
        /**
         * cislo uctu prikazce
         * AT SK tedy
         */
         $this->account_committer = str_pad($account_committer,10,0,STR_PAD_LEFT); 
         

        /**
         * variabilni symbol prikazce
         * dle CNB bude prepsan ss partnera
         */
         $this->vs = str_pad($vs,10,0,STR_PAD_LEFT); 

        /**
         * specificky symbol prikazce
         * dle CNB bude prepsan ss partnera
         */
         $this->ss  = $this->create_null_string(0,10);
     
        /**
         * koncový znak
         * CRLF
         */
        $this->close_chr = "\n";  
        
        /**
         * pradne slovo, chybny fomat banky ktera to tam palu prazdne
         */       
        $this->breaking = '';
    }
    
    
    /**
     * funkce pro vytvoreni platby
     * potrebne veci jsou
     * @param poradove cislo prikazu
     * @param datum splatnosti
     * @param castka 
     * @param kod banky 
     * @param prefix_ucet
     * @param ucet
     * @param popis prikazce
     */
    public function add_data($no,$date_to_pay,$amount,$code_bank_contra_account,$prefix_account_contra_account,$account_contra_account,$message_commiter){
        /**
         * poradove cislo prikazu
         */
        $this->no = str_pad($no,3,0,STR_PAD_LEFT).'.'; 
            
        /**
         * nastaveni datum splatnosti
         * datum musi byt ve formatu DD.MM.RRRR
         */
         $this->date_to_pay = $date_to_pay;
     
        /**
         * nastaveni castky platby
         * bez desetinych mist momentalne
         */
         $this->amount = '+'.str_pad($amount,13,0,STR_PAD_LEFT).'.00';
         
      
        /**
         * kod banky prijemce
         */
         $this->code_bank_contra_account = $code_bank_contra_account; 

        /**
         * prefixcisla uctu prijemce
         */
         $this->prefix_account_contra_account = str_pad($prefix_account_contra_account,6,0,STR_PAD_LEFT); 


        /**
         * cislo uctu prijemce
         */
         $this->account_contra_account = str_pad($account_contra_account,10,0,STR_PAD_LEFT); 
         
        /**
         * nastaveni poznamky
         */
         $this->comment = str_pad($message_commiter,30," ");
         if(strlen($this->comment) > 30)
            $this->comment = substr($this->comment,0,30); 
 
        /**
         * vytvoreni radku prikazu
         */            
         return self::create();
    }
    
    
    private function create(){
        $outline = null;

        foreach(self::$create_sequency_name as $item){
            //echo "pridavam : ".$item." ->".$this->$item.'('.strlen($this->$item).')<br />';
            //if(end(self::$create_sequency_name) != $item)
            $outline .= $this->$item.';';
        }
        
        //CRLF
        $outline .= $this->close_chr;

        return $outline;
    }
    
    private function create_null_string($type,$lenght = 1){
        return  str_repeat($type,$lenght);
    }
        
}
?>