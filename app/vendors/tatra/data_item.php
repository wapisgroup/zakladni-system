<?php
class DataItem3{
    private $no;
    private $typ_record = "32";
    private $date_to_pay;
    private $date_created;
    private $id_kredit_bank;
    
    // prikazce
    private $name_commiter;
    private $account_committer;
    private $prefix_account_committer;
    //prijemce
    private $name_contra;
    private $prefix_account_contra_account;
    private $account_contra_account;
    private $code_bank_contra_account;
    
    //dalsi nastaveni
    private $amount;
    private $currency_iso;
    private $vs;
    private $ks;
    private $ss;
    private $comment;
    private $null_10;
    private $null_140;
    private $close_chr;
    
    //prasne
    private $breaking;
    
    /**
     * nastaveni promemnych jak se bude sestavovat string
     */
    static $create_sequency_name = Array(
        'no',
        'typ_record',
        'date_created',
        'id_kredit_bank',
        'code_bank_contra_account',
        'amount',
        'date_to_pay',
        'ks',
        'vs',
        'ss',
        'prefix_account_committer',
        'account_committer',
        'prefix_account_contra_account',
        'account_contra_account',
        'comment',
        'name_commiter',
        'name_contra',
        //'currency_iso',
        'null_10',
        'null_10',
        'null_140',
        'null_140',
        'close_chr'
    );
    
    function DataItem3($prefix_account_committer,$account_committer, $vs,$name){
        /**
         * nazev prikazce/firmy
         */
         $this->name_commiter = str_pad($name,20," ");
         if(strlen($this->name_commiter) > 20)
            $this->name_commiter = substr($this->name_commiter,0,20); 
        
        /**
         * ID kreditni banky - vzdy 1100
         */
        $this->id_kredit_bank = str_pad('1100',7," ");
        
        /**
         * datum vytovreni platby
         */
        $date = new DateTime(); 
        $this->date_created = $date->format('ymd'); 
       
       
        /**
         * nastaveni ISO kodu meny
         * nastaveno na EUR
         */
         $this->currency_iso = 'EUR'; 
       
        /**
         * konstantni symbol
         * nastaven na 0308
         */
         $this->ks = str_pad('0308',10,0,STR_PAD_LEFT); 
        

        /**
         * prefix uctu prikazce
         * AT SK tedy
         */
         $this->prefix_account_committer = str_pad($prefix_account_committer,6,0,STR_PAD_LEFT); 
         
        /**
         * cislo uctu prikazce
         * AT SK tedy
         */
         $this->account_committer = str_pad($account_committer,10,0,STR_PAD_LEFT); 
         

        /**
         * variabilni symbol prikazce
         */
         $this->vs = str_pad($vs,10,0,STR_PAD_LEFT); 

        /**
         * specificky symbol prikazce
         */
         $this->ss  = $this->create_null_string(0,10);
     
        /**
         * prazdny string 10
         */
         $this->null_10  = $this->create_null_string(0,10);
         
        /**
         * prazdny string 140
         */
         $this->null_140  = $this->create_null_string(" ",140); 
     
        /**
         * koncový znak
         * CRLF
         */
        $this->close_chr = "\r\n";  
        
        /**
         * pradne slovo, chybny fomat banky ktera to tam palu prazdne
         */       
        $this->breaking = '';
    }
    
    
    /**
     * funkce pro vytvoreni platby
     * potrebne veci jsou
     * @param poradove cislo prikazu
     * @param datum splatnosti
     * @param castka 
     * @param kod banky 
     * @param prefix_ucet
     * @param ucet
     * @param popis prikazce
     * @param nazev prijemce
     */
    public function add_data($no,$date_to_pay,$amount,$code_bank_contra_account,$prefix_account_contra_account,$account_contra_account,$message_commiter,$name_contra){
        /**
         * poradove cislo prikazu
         */
        $this->no = str_pad($no,6,0,STR_PAD_LEFT); 
            
        /**
         * nastaveni datum splatnosti
         * datum musi byt ve formatu DD.MM.RRRR
         */
         $this->date_to_pay = $date_to_pay;
     
        /**
         * nastaveni castky platby
         * bez desetinych mist momentalne
         */
         $this->amount = str_pad($amount,13,0,STR_PAD_LEFT).'00';
         
      
        /**
         * kod banky prijemce
         */
         $this->code_bank_contra_account = str_pad($code_bank_contra_account,7," "); ; 

        /**
         * prefixcisla uctu prijemce
         */
         $this->prefix_account_contra_account = str_pad($prefix_account_contra_account,6,0,STR_PAD_LEFT); 


        /**
         * cislo uctu prijemce
         */
         $this->account_contra_account = str_pad($account_contra_account,10,0,STR_PAD_LEFT); 
         
        /**
         * nastaveni poznamky
         */
         $this->comment = str_pad($message_commiter,140," ");
         if(strlen($this->comment) > 140)
            $this->comment = substr($this->comment,0,140); 
            
         /**
         * nazev prijemce
         */
         $this->name_contra = str_pad($name_contra,20," ");
         if(strlen($this->name_contra) > 20)
            $this->name_contra = substr($this->name_contra,0,20);    
 
        /**
         * vytvoreni radku prikazu
         */            
         return self::create();
    }
    
    
    private function create(){
        $outline = null;

        foreach(self::$create_sequency_name as $item){
            //echo "pridavam : ".$item." ->".$this->$item.'('.strlen($this->$item).')<br />';
            //if(end(self::$create_sequency_name) != $item)
            $outline .= $this->$item;
        }

        return $outline;
    }
    
    private function create_null_string($type,$lenght = 1){
        return  str_repeat($type,$lenght);
    }
        
}
?>