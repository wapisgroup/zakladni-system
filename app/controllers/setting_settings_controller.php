<?php

class SettingSettingsController extends AppController {
	var $name = 'SettingSettings';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('Setting');
	var $renderSetting = array(
		'controller'=>'setting_settings',
		//'bindModel'	=> array('belongsTo'=>array('CmsUser')),
		'SQLfields' => array('id','name','value'),
		'page_caption'=>'Nastavení',
		'sortBy'=>'Setting.id.DESC',
		'top_action' => array(

		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		//	'LastLogin-cms_user_id'	=>	'select|Uživatel|cms_user_list',
		),
		'items' => array(
			'id'		=>	'ID|Setting|id|text|',
			'name'		=>	'Name|Setting|name|text|'
			//'value'		=>	'IP|LastLogin|ip|text',
			//'created'	=>	'Vytvořeno|LastLogin|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editovat položku|edit',	
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení'=>'#'));

		// $this->loadModel('CmsUser');
		// $this->set('cms_user_list', $this->CmsUser->find('list', array('conditions'=>array('kos'=>0))));
		// unset($this->CmsUser);

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

	function edit($id){
		if (empty($this->data)){
			$this->data = $this->Setting->read(null,$id);

			if (file_exists(VIEWS . 'setting_settings' . DS . 'edit_'.$id. '.ctp')){
				$this->render('edit_'.$id);
			} else 
				die('nevi pro jaky name renderovat');

	    }
		else {
			$this->Setting->save($this->data);
			die();
		}
		
	}
	
}
?>