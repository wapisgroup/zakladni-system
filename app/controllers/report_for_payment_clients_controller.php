<?php
Configure::write('debug',1);

	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');

    define('CURRENT_YEAR', $_GET['current_year']);
    define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);
class ReportForPaymentClientsController extends AppController {
	var $name = 'ReportForPaymentClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionClientRequirement');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','Company','RequirementsForRecruitment','CompanyWorkPosition'
            ),
			'hasOne'	=>	array(
				'NewClientWorkingHour'=>array(
					'conditions' => array(
						'NewClientWorkingHour.year'=> CURRENT_YEAR,
						'NewClientWorkingHour.month'=> CURRENT_MONTH
					)
				)
			),
			'joinSpec'=>array(
				'NewMoneyItem'=>array(
					'foreignKey'=>'NewClientWorkingHour.new_money_item_id',
					'primaryKey'=>'NewMoneyItem.id',
				),
				'AtCompany' => array(
					'primaryKey' => 'Company.parent_id',
					'foreignKey' => 'AtCompany.id',
					'conditions' => array(
						'AtCompany.kos' => 0
					)
				)
			)
		),
		'controller'	=>	'report_for_payment_clients',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			'ConnectionClientRequirement.type'=> 2,
            'ConnectionClientRequirement.new'=> 1,
			'NewClientWorkingHour.stav IN (3,4)'
		),
		'page_caption'=>'Výplaty pro účetní - Nové',
		'checkbox_setting' => array(
			'model'			=>	'NewClientWorkingHour',
			'col'			=>	'stav',
			'col2'			=>	'stop_payment',
			'stav_array'	=>	array(3),
			'month_year'	=>	false
		),
		'sortBy'=>'Client.name.ASC',
		'top_action' => array(
			'multi_edit'		=>	'Vyplatit hromadně|money_all|Vyplatit hromadně|multi_edit',
		),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
			'Company-coordinator_id'					=>	'select|COO|koordinator_list',
			'Company-client_manager_id'					=>	'select|CM|client_manager_list',
			'GET-current_year'							=>	'select|Rok|actual_years_list',
			'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'				=>	'ID|NewClientWorkingHour|id|hidden|',
			//'cc'				=>	'Client č.|NewClientWorkingHour|client_number|text|',
			'company'			=>	'Společnost|Company|name|text|',
			'atcompany'			=>	'Nadřazená společnost|AtCompany|name|text|',
			'client'			=>	'Klient|Client|name|text|',	
            //'forma_mzdy'		=>	'Forma|NewMoneyItem|name|text|',
			'nastup'			=>	'Nástup|ConnectionClientRequirement|from|date|',
			'ukonceni'			=>	'Ukončení|ConnectionClientRequirement|to|date|',
			'norhod'			=>	'FPD|NewClientWorkingHour|standard_hours|text|',
            'average_hour_salary'			=>	'PHM|NewClientWorkingHour|average_hour_salary|text|',
			'svatky'			=>	'Sv|NewClientWorkingHour|svatky|text|',
			'vikendy'			=>	'Ví|NewClientWorkingHour|vikendy|text|',
			'pn'				=>	'PN|NewClientWorkingHour|pracovni_neschopnost|text|',
			'dovolena'			=>	'Do|NewClientWorkingHour|dovolena|text|',
			'prescasy'			=>	'Př|NewClientWorkingHour|prescasy|text|',
			'hour_s1'			=>	'S1|NewClientWorkingHour|hour_s1|text|',
			'hour_s2'			=>	'S2|NewClientWorkingHour|hour_s2|text|',
			'hour_s3'			=>	'S3|NewClientWorkingHour|hour_s3|text|',
           
			'celkem'			=>	'Celkem|NewClientWorkingHour|celkem_hodin|text|',
			'total'				=>	'Total|NewClientWorkingHour|salary_per_hour_p|text|',
             'zaklad'			=>	'Základ|NewClientWorkingHour|salary_start|text|',
            'sum_bonus'			=>	'Bonusy|NewClientWorkingHour|sum_bonus|text|',
            'sum_drawback'			=>	'Srážky|NewClientWorkingHour|sum_drawback|text|',
			'sum_extra_pay'			=>	'Příplatky|NewClientWorkingHour|sum_extra_pay|text|',
			'stav'				=>	'Stav|NewClientWorkingHour|stav|var|kalkulace_stav_list',
			'stop_payment'		=>	'Výp.|NewClientWorkingHour|stop_payment|var|ano_ne'	
		),
		'posibility' => array(
			'stop_status'			=>	'stop|Pozastavit výplatu|stop_status',
			'show'			=>	'odpracovane_hodiny|Karta docházky|show',
			'edit'			=>	'money|Vyplatit|edit'
		)
	);
	
	function index(){
		//pr($this->viewVars['items']);
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Výplaty pro účetní - Nové'=>'#'));
			
		$this->loadModel('CmsUser');
			$this->set('client_manager_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>3))));
			$this->set('koordinator_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>4))));
		
			$this->set('company_list',$this->get_list('Company'));
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

	
	function money($id){
		$this->loadModel('NewClientWorkingHour');
		$this->NewClientWorkingHour->id = $id;
		$this->NewClientWorkingHour->saveField('stav',4);
		unset($this->NewClientWorkingHour);
		die();
	}

	function stop($id,$status){
		$this->loadModel('NewClientWorkingHour');
		$this->NewClientWorkingHour->id = $id;
		$this->NewClientWorkingHour->saveField('stop_payment',$status==1 ? 0 :1);
		unset($this->NewClientWorkingHour);
		die();
	}

	function odpracovane_hodiny($id = null, $year = null, $month = null){
		echo $this->requestAction('new_employees/odpracovane_hodiny/'.$id.'/'.$year.'/'.$month.'/');
		die();
	}

	function money_all(){
		$model = $this->renderSetting["checkbox_setting"]['model'];
		$this->loadModel($model);
		foreach($this->params['url']['data'][$model]['id'] as $key => $on){
			$this->$model->id = $key;
			$this->$model->saveField('stav',4);
		}
		unset($this->$model);	
		die();
	}

	
	
}
?>