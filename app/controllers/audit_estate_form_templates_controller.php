<?php
Configure::write('debug',1);
class AuditEstateFormTemplatesController extends AppController {
	var $name = 'AuditEstateFormTemplates';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Wysiwyg');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('FormTemplate');
	var $renderSetting = array(
        'bindModel'	=> array(
			//'belongsTo'	=>	array('FormTemplateGroup')
		),
		'SQLfields' => array('FormTemplate.id','FormTemplate.name','FormTemplate.updated',
            'FormTemplate.created','FormTemplate.text'

        ),
        'SQLcondition' => array(
            'FormTemplate.type'=>array(1,2,3)
        ),
		'controller'=> 'audit_estate_form_templates',
		'page_caption'=>'Nastavení formulářů',
		'sortBy'=>'FormTemplate.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
		    //'FormTemplate-form_template_group_id'		=>	'select|Skupina|group_list'
		),
		'items' => array(
			'id'		=>	'ID|FormTemplate|id|text|',
			//'group'		=>	'Skupina|FormTemplate|form_template_group_id|viewVars|group_list',
			'name'		=>	'Nazev|FormTemplate|name|text|',
			'text'		=>	'Popis|FormTemplate|text|text|orez#50',
			'updated'	=>	'Upraveno|FormTemplate|updated|datetime|',
			'created'	=>	'Vytvořeno|FormTemplate|created|datetime|'
		),
		'posibility' => array(
			'status'	                => 	'status|Změna stavu|status',
			'edit'		                =>	'edit|Editace položky|edit',
			//'export_to_companies'		=>	'export_to_companies|Exportovat do firem|export_to_companies',
			'trash'	                    =>	'trash|Do kosiku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> 'true',
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('scripts', array('wysiwyg_old/wswg')); 
		$this->set('styles', array('../js/wysiwyg_old/wswg'));
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Šablony formulářů'=>'#'));
        

        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){

		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->FormTemplate->read(null,$id);
				$this->data['FormTemplate']['text'] = strtr($this->data['FormTemplate']['text'],array("\t"=>" ","\n"=>""));
                
                /**
                 * Nacteni jiz ulozenych vlastnosti
                 */
                $this->loadModel('ConnectionFormTemplate');
                $finds = $this->ConnectionFormTemplate->find('all',array('conditions'=>array('form_template_id'=>$id,'kos'=>0)));
                foreach($finds as $item){
                    $this->data['ConnectionFormTemplate'][$item['ConnectionFormTemplate']['typ']][$item['ConnectionFormTemplate']['at_company_id']][$item['ConnectionFormTemplate']['at_company_money_item_id']] = 1;
                }           
			}
            
            $this->set('forma_zamestani',$this->get_list('AtCompanyMoneyItem'));
            $this->set('at_company_list',$this->get_list('AtCompany'));
            
            /**
             * Definici dat ktere muzeme vkladat
             */
            $wsg_data = array(

                '##Client.name##'=>'Jméno',
                '##Client.rodne_cislo##'=>'Rodne číslo',
                '##Client.cislo_op##'=>'Číslo OP',
                '##Client.datum_narozeni|date##'=>'Datum narozeni',
                '##Client.email##'=>'Email',
                '##Client.ulice## ##Client.mesto## ##Client.psc##'=>'Adresa',
                '##Client.cislo_uctu##'=>'Číslo účtu',
                '##AuditEstate.name##'=>'Název majetku',
                '##AuditEstate.imei_sn_vin##'=>'IMEI/SN/VIN majetku',
                '##AtCompany.name##'=>'Název firmy',
                '##AtCompany.ico##'=>'ICO firmy',
                '##AtCompany.ulice## ##AtCompany.mesto## ##AtCompany.psc##'=>'Adresa firmy',
                '##CmsUser.name##'=>'uzivatel(přidělil)',
                '##AuditEstate.created|date##'=>'Datum přiřazení majetku do systému',
                '##AuditEstate.payment_date|date##'=>'Datum pořizení majetku',
                '##AuditEstate.first_cost##'=>'cena majetku(bez DPH)',
                '##AuditEstate.hire_price##'=>'cena pronájmu',
                '##AuditEstate.currency|mena##'=>'Měna ceny majetku',
                '##AuditEstate.accessories##'=>'prislušenství majetku',
                '##AuditEstate.registration_car_number##'=>'evidenční číslo vozidla',                  
                '##AtCompany.registration_in##'=>'registrace vedena',
                '##AtCompany2.name##'=>'Název firmy(majetek)',
                '##AtCompany2.ico##'=>'IČO firmy(majetek)',
                '##AtCompany2.ulice## ##AtCompany2.mesto## ##AtCompany2.psc##'=>'Adresa firmy(majetek)',
                '##AtCompany2.registration_in##'=>'registrace vedena(majetek)',
                '##AtProject2.name##'=>'Název projektu - pronajímatel',
                '##AtProjectCentre2.name##'=>'Název střediska - pronajímatel',
                '##AtProject.name##'=>'Název projektu - nájemce',
                '##AtProjectCentre.name##'=>'Název střediska - nájemce',
                '##AuditEstateStav.name##'=>'Poslední stav majetku',
                '##0.all_states##'=>'Všechny stavy majetku',
                '##ConnectionAuditEstate.created|date##'=>'Datum přiřazení majetku - uživateli',
                '##ConnectionAuditEstate.created|today_date##'=>'Aktualní datum',
                '##ConnectionAuditEstate.description##'=>'Doplňujicí informace u přiřazení',
                '0'=>'----------',
                '##AuditEstateLossEvent.id##'=>'Evidenční číslo - Škod.Udál.',
                '##AuditEstateLossEvent.expected_price##'=>'Předpokládaná cena opravy - Škod.Udál.',
                '##AuditEstateLossEvent.date_to_repair|date##'=>'Datum odeslání do opravy - Škod.Udál.',
                '##AuditEstateLossEventCmsUser.name##'=>'Tvůrce - Škod.Udál.',
                '##ClientSpravceMajetku.name##'=>'Správce majetku (jméno) - Škod.Udál.',
                '##ClientSpravceMajetku.mobil1##'=>'Správce majetku (telefon) - Škod.Udál.',
                '##AuditEstateLossEvent.faults_found##'=>'Zjištěné závady - Škod.Udál.'
            );
            $this->set('wsg_data',$wsg_data);
            
			$this->render('edit');
		} else {
		    //odstraneni zbytecnych mezer a tagu z WORD apod.  
		    $this->data['FormTemplate']['text'] = strtr($this->data['FormTemplate']['text'],array("\t"=>" ","\n"=>""));
                  
			$this->FormTemplate->save($this->data);
            
            if(isset($this->data['ConnectionFormTemplate'])){
                $this->loadModel('ConnectionFormTemplate');
                $find = $this->ConnectionFormTemplate->find('all',array('conditions'=>array('form_template_id'=>$this->FormTemplate->id)));
                //rozdeleni podle typu
                $find_ = Set::combine($find,'/ConnectionFormTemplate/at_company_id','/ConnectionFormTemplate','/ConnectionFormTemplate/typ');
                
                $find_1 = $find_2 = array();
                if(isset($find_[1]) && !empty($find_[1]))//rozdeleni vlastnosti
                    $find_1 = Set::combine($find_[1],'/ConnectionFormTemplate/at_company_money_item_id','/ConnectionFormTemplate/id','/ConnectionFormTemplate/at_company_id');
                if(isset($find_[2]) && !empty($find_[2]))//rozdeleni vlastnosti odevzdani
                    $find_2 = Set::combine($find_[2],'/ConnectionFormTemplate/at_company_money_item_id','/ConnectionFormTemplate/id','/ConnectionFormTemplate/at_company_id');

                $this->ConnectionFormTemplate->updateAll(
                    array('kos'=>1),
                    array('form_template_id'=>$this->FormTemplate->id)
                );
                
                //vlastnosti
                if(isset($this->data['ConnectionFormTemplate'][1]))
                    foreach($this->data['ConnectionFormTemplate'][1] as $at_company_id=>$conections){
                       self::save_connection_template_typ(1,$this->FormTemplate->id,$at_company_id,$conections,$find_1);
                    }
                
                //vlastnosti pro odevdani
                if(isset($this->data['ConnectionFormTemplate'][2]))
                    foreach($this->data['ConnectionFormTemplate'][2] as $at_company_id=>$conections){
                       self::save_connection_template_typ(2,$this->FormTemplate->id,$at_company_id,$conections,$find_2);
                    }
                
            }
            die();
		}
	}
    
    function save_connection_template_typ($typ,$template_id,$at_company_id,$conections,$find_){    
          if(array_key_exists($at_company_id,$find_)){
                $company_connections = $find_[$at_company_id];
                 foreach($conections as $money_item=>$con){
                    if($con == 1 || $con == '1'){
                        $data = array(
                            'at_company_id'=>$at_company_id,
                            'at_company_money_item_id'=>$money_item,
                            'form_template_id'=>$template_id,
                            'typ'=>$typ,
                            'kos'=>0
                        );
                        
                        if(isset($company_connections[$money_item]))
                            $this->ConnectionFormTemplate->id = $company_connections[$money_item];
                            
                        $this->ConnectionFormTemplate->save($data);
                        $this->ConnectionFormTemplate->id = null;
                    }
                }
           }
           else{
            /**
             * Neexistuje firma aktivni, vsechny zaple connection tedy muzeme ulozit
             */
                foreach($conections as $money_item=>$con){
                    if($con == 1 || $con == '1'){
                        $data = array(
                            'at_company_id'=>$at_company_id,
                            'at_company_money_item_id'=>$money_item,
                            'form_template_id'=>$this->FormTemplate->id,
                            'typ'=>$typ
                        );
                        $this->ConnectionFormTemplate->save($data);
                        $this->ConnectionFormTemplate->id = null;
                    }
                }
           }
    }


	function generate_form($id = null,$client_id, $data_id = null, $company_table = false){
		if($id == null && $data_id == null)
			die('Chyba, špatná kombinace id!!!');


		$this->autoLayout = false;
		$this->loadModel('Client');
		$this->client = $this->Client->read(null,$client_id);

        /**
         * defaultne tahano z globalnich sablon 
         */
        $data_table = 'CompanyFormTemplate'; 

        /**
         * jinak pri zamestnani se tahaji data z company form template
         */
        if($company_table){
          $this->loadModel('CompanyFormTemplate');
          $data_table = 'CompanyFormTemplate';      
		}

         
        $datas = $this->$data_table->read(null,$id);

	
		preg_match_all('@##(.*)##@',$datas[$data_table]['text'],$text2);

		foreach($text2[1] as $key=>$item){
			$temp = explode('|',$item);
            
            //pokud neni nastaven value
            if(!isset($temp[1]))
				$temp[1] = null;
            //pokud neni nastaven set
			if(!isset($temp[2]))
				$temp[2] = null;
                
			list($type, $value, $set) = $temp;
			//echo $value;
			$text2[1][$key] = $this->transfer_to_html($type,$value,$set);
		}

		$data = str_replace($text2[0], $text2[1], $datas[$data_table]['text']);
		$this->set('data',$data);
		
		$this->data['FormData']['client_id']=$client_id;
		$this->data['FormData']['name']=$datas[$data_table]['name'];
		
        if($company_table){
            /**
             * rovnou se uklada k danemu klientovi
             */
            $this->loadModel('FormData');
            $form_data_to_save = $this->data;
            $form_data_to_save['FormData']['company_id'] = $datas[$data_table]['company_id'];
            $form_data_to_save['FormData']['html_code'] = $data;
                        
            $this->FormData->id = null; 
            $this->FormData->save($form_data_to_save); 
        }
        else //pro globalni sablony se zobrazuje
		  $this->render('generate/index');
	}

	private function transfer_to_html($type,$value,$set){
		$r = null;
		$value = substr($value,1,-1);
		
		if(strpos($value,'.') != false){
			list($model,$col) = explode('.',$value);
			$value = $this->client[$model][$col];	
		}

		if($type != ''){
			switch($type){
				case 'INPUT' :
					$r = '<input type="text" name="data[FormData][values][]" value="'.$value.'" />';
				break;

				case 'CHECKBOX' :
					$r = '<input type="checkbox" name="data[FormData][values][]" '.($value==1 ? 'checked="checked"' : "").' />';
				break;

				case 'TEXTAREA' :
					$r = '<textarea rows="5" cols="35"  name="data[FormData][values][]">'.$value.'</textarea>';
				break;

				case 'SELECT' :
				case 'SELECTN' :
					$s_typ = substr($set,0,1);

					if($set != null){
						$r = '<select name="data[FormData][values][]" />';
						//$r .= '<option value="">lol</option>';
						if($s_typ == '_'){
							$select = substr($set,1,-1);
							if ($type == 'SELECTN')
								$r .= '<option value=""></option>';
							foreach($this->viewVars[$select] as $val=>$name){
								$r .= '<option value="'.$val.'" '.($value==$val ? 'selected="selected"' : "").' >'.$name.'</option>';
							}
						}
						else if($s_typ == '{'){
							$select = split(',',substr($set,1,-1));

							foreach($select as $val=>$name){
								$r .= '<option value="'.$val.'" '.($value==$val ? 'selected="selected"' : "").' >'.$name.'</option>';
							}
						}
		
						$r .= '</select>';
					}
				break;
			}
		}
		
		return $r;
	
	}


	function save_client_form(){

		if(!empty($this->data)){
			//$html_code = $this->FormTemplate->read(array('text'),$this->data['FormData']['template_id']);
		//	$this->data['FormData']['html_code'] = $html_code['FormTemplate']['text'];
			
			$this->loadModel('FormData');
			$this->FormData->save($this->data);
			$save_id = $this->FormData->id;
			unset($this->FormData);
			
			die(json_encode(array('result'=>true, 'message' => 'All it`s ok :-)', 'id'=> $save_id)));
		}
		else 
			die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB.')));
	}

	
	function delete_client_form($id){
		$this->loadModel('FormData');
		$this->FormData->id = $id;
		$this->FormData->saveField('kos',1);
		unset($this->FormData);		
		die();
	}

	
	function edit_client_form($id){
		$this->loadModel('FormData');
		$this->data = $this->FormData->read(null,$id);
		unset($this->FormData);		

		// render
		$this->render('generate/edit');
	}

	function print_client_form($id){
		$this->layout = 'print';

		$this->loadModel('FormData');
		$this->data = $this->FormData->read(null,$id);
		unset($this->FormData);		

		// render
		$this->render('generate/print');
	}
    
    
   
   
}
?>