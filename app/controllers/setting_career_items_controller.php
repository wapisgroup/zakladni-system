<?php
class SettingCareerItemsController extends AppController {
	var $name = 'SettingCareerItems';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingCareerItem');
	var $renderSetting = array(
		'controller'=>'setting_career_items',
		'bindModel' => array('belongsTo' => array('SettingCareerCat')),
		'SQLfields' => array('SettingCareerItem.id','SettingCareerItem.name','SettingCareerItem.updated','SettingCareerItem.created','SettingCareerCat.name'),
		'page_caption'=>'Nastavení pracovních pozic',
		'sortBy'=>'SettingCareerItem.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			'SettingCareerItem-setting_career_cat_id'	=>	'select|Kategorie|career_cats_list',
		//	'SettingCareerItem-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingCareerItem|id|text|',
			'name'		=>	'Název|SettingCareerItem|name|text|',
			'cat'		=>	'Kategorie|SettingCareerCat|name|text|',
			'updated'	=>	'Upraveno|SettingCareerItem|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingCareerItem|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|delete'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[550,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení pracovních pozic'=>'#'));
		$this->loadModel('SettingCareerCat');
		$this->set('career_cats_list',$this->SettingCareerCat->find('list',array('conditions'=>array('SettingCareerCat.kos'=>0))));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		$this->loadModel('SettingCareerCat');
		$this->set('career_cats_list',$this->SettingCareerCat->find('list',array('conditions'=>array('SettingCareerCat.kos'=>0))));
        $this->loadModel('ProfesiaCodeBook');
        $this->set('career_profesia_list',$this->ProfesiaCodeBook->find('list',array('conditions'=>array('ProfesiaCodeBook.type'=>'positions'))));

		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingCareerItem->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingCareerItem->save($this->data);
			die();
		}
	}
}
?>