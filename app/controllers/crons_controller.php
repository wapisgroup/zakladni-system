<?php
/**
 * 
 * @author Jakub Matuš - Wapis Group s.r.o.
 * @created 17.2.2011
 * 
 * Cron controller, slouzi pro pristup cronu na ATEP
 * je nutne v linku uvest tzv, hash klic - ktery se validuje
 * pak je klasicky spusteni request action
 */
class CronsController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'Crons';
	var $uses = array('Client');
	var $layout = 'default';
    private $hash = 'ML942';
		
	function index($hash = null,$url = null){
       if($hash == $this->hash){
            $url = strtr($url,array('|'=>'/'));
            echo $this->requestAction($url,array('no_login_access'=>true));
            die();
       } 
       else
         $this->redirect('/login/');
    }
}	
?>