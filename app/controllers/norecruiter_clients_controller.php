<?php
//Configure::write('debug',2);
class NorecruiterClientsController extends AppController {
	var $name = 'NorecruiterClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('ClientView','Client');
	var $renderSetting = array(
		'permModel'=>'ConnectionClientRecruiter',
		'bindModel' => array('hasOne' => array(
				'ConnectionClientCareerItem' => array('foreignKey' => 'client_id'),
				'ConnectionClientRecruiter' => array('className' => 'ConnectionClientRecruiter', 'foreignKey' => 'client_id'),
				'ConnectionClientRequirement' => array(
					'className' => 'ConnectionClientRequirement', 
					'foreignKey' => 'client_id',
					'conditions' => array(
						'ConnectionClientRequirement.kos'=>0,
						'(ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1)'
					)
				)
		)),
		'SQLfields' => '*',
		'SQLcondition' => array("
            (ClientView.mobil1 IS NULL OR ClientView.mobil1 = '') AND
            (ClientView.mobil2 IS NULL OR ClientView.mobil2 = '') AND
            (ClientView.mobil3 IS NULL OR ClientView.mobil3 = '') 
            "
        ),
		'controller'=> 'norecruiter_clients',
		'page_caption'=>'Klienti - všichni co nemaji telefonni cislo',
		'group_by' => 'ClientView.id',
		'sortBy'=>'ClientView.id.ASC',
	
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'ClientView-name|'		=>	'text|Jméno|',
			'ClientView-stav'		=>	'select|Status|stav_client_list',
			'ConnectionClientCareerItem-setting_career_item_id'		=>	'select|Profese|profese_list',
			'ConnectionClientRecruiter-cms_user_id#admin_ctrl'		=>	'select|Recruiter|cms_user_list',
			'ConnectionClientRequirement-company_id'		=>	'select|Firma|company_list'
			//'ClientView-client_manager|coordinator'		=>	'select|CM/KOO|cm_coo_list',
			//'ClientView-telefon1|'	=>	'text|Telefon|',
		),
		'items' => array(
			'client_manager_id'		=>	'cmid|ClientView|client_manager_id|hidden|',
			'id'		=>	'ID|ClientView|id|text|',
			'name'		=>	'Jméno|ClientView|name|text|',
			'profese'	=>	'Profese|ClientView|Profese|text|',
			'mobil'		=>	'Telefon|ClientView|mobil|text|',
			'status'	=>	'Status|ClientView|stav|var|stav_client_list',
			'datum_nastupu'	=>	'Datum umistění / nástupu|ConnectionClientRequirement|from|date|',
			'firma'		=>	'Firma|ClientView|company|text|',
			'cm'		=>	'CM|ClientView|client_manager|text|',
			'coo'		=>	'COO|ClientView|coordinator|text|',
			'coo2'		=>	'COO2|ClientView|coordinator2|text|',
			'status_imp'=>	'StatusImport|ClientView|import_stav|var|stav_importu_list',
		//	'updated'	=>	'Změněno|ClientView|updated|datetime|',
			'created'	=>	'Vytvořeno|ClientView|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'attach'	=>	'attachs|Přílohy|attach',
			'message'	=>	'messages|Zprávy|message',
			'vyhodit'	=>	'rozvazat_prac_pomer|Rozvázat pracovní poměr|rozvazat_prac_pomer',
			'zmena_pp'	=>	'zmena_pp|Změna pracovního poměru|zmena_pp',
			'delete'	=>	'trash|Odstranit položku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti - všichni co nemaji telefonni cislo'=>'#'));
			
		$this->loadModel('CmsUser');
		$this->set('cms_user_list', $this->CmsUser->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

		$this->set('cm_coo_list', $this->CmsUser->find('list',
			array('conditions'=>array('cms_group_id IN (3,4)'),'fields'=>array('name','name'), 'order'=>'name ASC')
		));

		$this->loadModel('Company'); 	
		$company_conditions = array('Company.kos'=>0);
		
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		 
		$this->set('company_list',		$this->Company->find('list',array(
			'conditions'=>$company_conditions,
			'order'=>array('Company.name ASC')
		)));
		unset($this->Company);


		$this->loadModel('SettingCareerItem');
		$this->set('profese_list', $this->SettingCareerItem->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

		//změna pro umístění datum na created misto from
		foreach($this->viewVars['items'] as &$item){
			if($item['ConnectionClientRequirement']['type'] == 1)
				$item['ConnectionClientRequirement']['from'] = $item['ConnectionClientRequirement']['created'];
		}
				
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	
	
	function edit($id = null,$domwin = null, $show = null){
		echo $this->requestAction('clients/edit/'.$id.'/'.$domwin.'/'.$show);
		die();
	}

	
	
	
	/**
 	* Seznam priloh
 	*
	* @param $client_id
 	* @return view
 	* @access public
	**/
	function attachs($client_id){
		echo $this->requestAction('clients/attachs/'.$client_id);
		die();
	}
	
	
	
	
	function rozvazat_prac_pomer($client_id){
		echo $this->requestAction('clients/rozvazat_prac_pomer/'.$client_id);
		die();
	}
	

	// funkce na zmenu pracovniho pomeru
	function zmena_pp($client_id = null,$company_id = null,$company_work_position_id = null){
		echo $this->requestAction('clients/zmena_pp/'.$client_id.'/'.$company_id.'/'.$company_work_position_id);
		die();
	}


}
?>