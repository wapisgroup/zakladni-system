<?php
Configure::write('debug',1);
class ReportMoneyItemsController extends AppController {
	var $name = 'ReportMoneyItems';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CompanyMoneyItem');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('Company','CompanyMoneyValidity','CompanyWorkPosition')),
		'controller'=>'report_money_items',
		'SQLfields' => array('CompanyMoneyItem.fakturacni_sazba_na_hodinu','CompanyMoneyItem.celkovy_pocet_hodin_mesicne',
                            'CompanyMoneyItem.provinzni_marze','CompanyMoneyItem.vypocitana_celkova_cista_maximalni_mzda_na_hodinu',
                            'CompanyMoneyItem.doprava','CompanyMoneyItem.cena_pracovnich_odevu_mesicne',
                            'CompanyMoneyItem.cena_ubytovani_na_mesic','Company.name','Company.self_manager_id','Company.client_manager_id',
                            'Company.coordinator_id','CompanyWorkPosition.name','CompanyMoneyValidity.platnost_od','CompanyMoneyItem.id',
                            'CompanyMoneyItem.name','CompanyMoneyItem.created','Company.client_manager_id','CompanyMoneyItem.id'),
		'page_caption'=>'Report - Kalkulace podniku',
		'sortBy'=>'CompanyMoneyItem.created.DESC',
		'permGroupId'=>true,
		'permModel'=>'Company',
		'TypUserCompany'=>true,
		'SQLcondition'=>array(
			'CompanyMoneyValidity.platnost_do'=>'0000-00-00',
			'CompanyMoneyItem.schvaleno'=>0,
			'CompanyMoneyItem.kos'=>0,
			'CompanyWorkPosition.kos'=>0
		),
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
            'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel', 
		),
		'filtration' => array(
			'CompanyWorkPosition-name|'	=>	'text|Pozice|',
			'Company-self_manager_id'		=>	'select|SM|cms_user_list',
			'CompanyMoneyItem-company_id'		=>	'select|Firma|company_list'
		),
		'items' => array(
			'id'		=>	'ID|CompanyMoneyItem|id|hidden|',
			'self_manager_id' 		=>	'SalesManager|Company|self_manager_id|hidden|',
			'company'	=>	'Firma|Company|name|text|',
			//'sm'		=>	'SM|Company|self_manager_id|viewVars|cms_user_list',
			'pozice'	=>	'Pozice|CompanyWorkPosition|name|text|',
			'platnost'	=>	'Platnost od|CompanyMoneyValidity|platnost_od|date|',
			'fakturacka'=>	'Fakturační sazba|CompanyMoneyItem|fakturacni_sazba_na_hodinu|money|',
            'ch'        =>	'Celkem hodin|CompanyMoneyItem|celkovy_pocet_hodin_mesicne|text|',
			'typ'		=>	'Typ|CompanyMoneyItem|name|text|',
			'ubytovani'	=>	'Ubytovani|CompanyMoneyItem|cena_ubytovani_na_mesic|money|',
            'doprava'	=>	'Doprava|CompanyMoneyItem|doprava|money|',
            'cpo'	=>	'Cena prac. oděvů|CompanyMoneyItem|cena_pracovnich_odevu_mesicne|money|',
			'max'		=>	'Max|CompanyMoneyItem|vypocitana_celkova_cista_maximalni_mzda_na_hodinu|money|',
			'marze'		=>	'Marže|CompanyMoneyItem|provinzni_marze|procenta|',
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
			'edit'		=>	'edit|Editovat položku|edit',			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Kalkulace firem'=>'#'));
		
			$this->loadModel('CmsUser'); 
			$this->loadModel('Company'); 
			$this->set('cms_user_list',		$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>array(2)))));
			$this->set('company_list',		$this->Company->find('list',array(
				'conditions'=>array(
					'Company.kos'=>0
				),
				'order'=>array('Company.name ASC')
			)));
			unset($this->Company);
			unset($this->CmsUser);
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id){
		echo $this->requestAction('companies/work_money_edit/-1/-1/' . $id);
		die();
	}
    
    /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
       //Configure::write('debug',1);
         
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
        
       $fields_sql = $this->renderSetting['SQLfields'];     
       $criteria = $this->ViewIndex->filtration();        
       $this->loadModel('CompanyMoneyItem');
      

         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');

      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;
        $this->CompanyMoneyItem->bindModel($this->renderSetting['bindModel']);        
        $count = $this->CompanyMoneyItem->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT CompanyMoneyItem.id) as count"),
        		'conditions'=>am($criteria,array(
        			'CompanyMoneyValidity.platnost_do'=>'0000-00-00',
        			'CompanyMoneyItem.schvaleno'=>0,
        			'CompanyMoneyItem.kos'=>0,
        			'CompanyWorkPosition.kos'=>0
        		)),
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];

        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->CompanyMoneyItem->bindModel($this->renderSetting['bindModel']);     
            foreach($this->CompanyMoneyItem->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>am($criteria,array(
            			'CompanyMoneyValidity.platnost_do'=>'0000-00-00',
            			'CompanyMoneyItem.schvaleno'=>0,
            			'CompanyMoneyItem.kos'=>0,
            			'CompanyWorkPosition.kos'=>0
            		)),
                    'group'=>'CompanyMoneyItem.id',
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }
	
}
?>