<?php
//Configure::write('debug', 2);

class MyOwnClientsController extends AppController
{
    var $name = 'MyOwnClients';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex');
    var $components = array('ViewIndex', 'RequestHandler');
    var $uses = array('MyClientsView');
    var $renderSetting = array(
        'SQLfields' => array(
            'MyClientsView.*',
            'IF( MyClientsView.id IS NOT NULL, MyClientsView.id  , "Vytvořen"  ) as zpusob',
            'IF( MyClientsView.from_date IS NULL, "" ,IF( MyClientsView.pay_date IS NOT NULL, "ano"  ,"ne" )) as payed',
            'IF( MyClientsView.from_date IS NOT NULL,(DATE_FORMAT( MyClientsView.from_date,"%d.%m.%Y")), (DATE_FORMAT(MyClientsView.created,"%d.%m.%Y")) ) as created',
        ), //"#CMS_USER#"
        'SQLcondition' => array( //CMS_USER
            'IF( ISNULL(to_cms_user_id) , cms_user_id=#CmsUserId# AND client_id NOT IN ( SELECT CB.client_id FROM wapis__client_buys as CB WHERE CB.from_cms_user_id = MyClientsView.cms_user_id AND CB.status = 1 ), to_cms_user_id = #CmsUserId#  AND to_date = "0000-00-00" )',
        ),
        'controller' => 'my_own_clients',
        'page_caption' => 'Klienti v mém vlastnictví',
        'top_action' => array(
            //'add_item'		=>	'Přidat|edit|Pridat majetek|add',
        ),
        'no_trash'=> false,
        'filtration' => array(

            'GET-current_month' => 'select|Měsíc|mesice_list',
            'GET-current_year' => 'select|Rok|actual_years_list',

        ),
        'items' => array(
            'id' => 'ID|MyClientsView|id|hidden|',
            'client_name' => 'Pracovník|MyClientsView|client_name|text',
            'zpusob' => 'Způsob|0|zpusob|text',
            'created' => 'Vytvořen|0|created|text',
            'payed' => 'Zaplacen|0|payed|text|'
        ),
        'sortBy'=>'MyClientsView.client_name.ASC',
        'domwin_setting' => array(
            'sizes' => '[1000,1000]',
            'scrollbars' => true,
            'languages' => true,
        )
    );

    function index()
    {

        $this->set('fastlinks', array('ATEP' => '/', 'Evidence majetku' => '#', $this->renderSetting['page_caption'] => '#'));

     /*   $this->set('company_list', $this->get_list('AtCompany'));
        $this->set('project_list', $this->get_list('AtProject'));
        $this->set('center_list', $this->get_list('AtProjectCentre'));*/
        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }

}

?>