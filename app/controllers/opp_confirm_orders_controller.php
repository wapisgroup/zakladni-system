<?php
Configure::write('debug',0);
class OppConfirmOrdersController extends AppController {
	var $name = 'OppConfirmOrder';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('OppOrder');
	var $renderSetting = array(
		'bindModel'	=> array(
            'belongsTo'=>array('CmsUser')
        ),
		'SQLfields' => '*',
        'controller'=> 'opp_confirm_orders',
		'page_caption'=>'Objednavky na OPP / potvrzení',
		'sortBy'=>'OppOrder.id.DESC',
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add'
		),
		'filtration' => array(
			'OppOrder-cms_user_id'	=>	'select|Koordinátor|coo_list',
		),
		'items' => array(
			'id'		=>	'ID|OppOrder|id|text|',
			'user'		=>  'Koordinátor|CmsUser|name|text|',
			'created'	=>	'Datum vytvoření|OppOrder|created|datetime|',
			'updated'	=>	'Datum zpracování|OppOrder|datum_potvrzeni|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
            'return_order'	=>	'return_order|Zrušení potvrzení objednávky|return_order',
			'delete'	=>	'trash|Odstranit položku|trash'			
		),
        'domwin_setting'=>array(
            'sizes'=>'[900,900]'
        ),
        'group_condition'=>array(
            array(
                'id'=>'18',
                'condition'=>"OppOrder.cms_user_id IN (SELECT (CASE OppOrder.cms_user_id 
                        WHEN client_manager_id THEN client_manager_id
                        WHEN coordinator_id THEN coordinator_id
                        WHEN coordinator_id2 THEN coordinator_id2
                        ELSE 0
                        END )
                   FROM wapis__companies WHERE manazer_realizace_id =#CmsUser.id# and kos = 0)
                   "
            )
        )
	);
	function index(){
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->loadModel('CmsUser');
            $this->CmsUser->bindModel(array('belongsTo'=>array('CmsGroup')));
			$coo_list = $this->CmsUser->find('all',array(
                'conditions'=>array('CmsGroup.cms_group_superior_id'=>2,'CmsUser.kos'=>0,'CmsUser.status'=>1),
                'fields'=>array('CmsUser.id','CmsUser.name'),
                'order'=>'CmsUser.name'
            ));
			$this->set('coo_list', Set::combine($coo_list,'/CmsUser/id','/CmsUser/name'));
            unset($this->CmsUser);
		
			$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			$this->loadModel('OppItem');
			$this->set('type_list',$this->OppItem->find('list', array('conditions'=>array('kos'=>0),'group'=>'type','fields'=>array('type','type'))));
			//pr($typs);
			if ($id != null){
				$this->OppOrder->bindModel(array('hasMany'=>array('OppOrderItem')));
				$this->data = $this->OppOrder->read(null,$id);
				
				$names = array();
				foreach(Set::extract('/OppOrderItem/type_id',$this->data) as $t)
					$names[$t] = self::load_typ($t,true);
				$this->set('names',$names);
				
				$sizes = array();
				foreach(Set::extract('/OppOrderItem/name',$this->data) as $t)
					$sizes[$t] = self::load_size($t,true);
				$this->set('sizes',$sizes);
			}	

			$this->render('../opp_confirm_orders/edit');
		} else {
			
			if($this->OppOrder->save(array(
				'id'		=> $this->data['OppOrder']['id'],
				'status' 	=> 1,
				'datum_potvrzeni' => date("Y-m-d H:i:s")
			))){
			    /**
			      * Odečteme opp_items, pocet polozek ktere si objednal dany coo v objednavce
			      */ 
                $this->loadModel('OppItem');
                $this->loadModel('OppOrderItem');
                
                $items = $this->OppOrderItem->find('all',array(
                    'conditions'=>array(
                        'opp_order_id'=>$this->data['OppOrder']['id'],
                        'count >'=>0
                    ),
                    'fields'=>array('name','type_id','size','count')
                ));
                foreach($items as $item){
                    $exist = $this->OppItem->find('first',array('conditions'=>array('name'=>$item['OppOrderItem']['name'],'size'=>$item['OppOrderItem']['size'],'type'=>$item['OppOrderItem']['type_id'],'kos'=>0)));
                    if($exist){
                        $this->OppItem->query('UPDATE wapis__opp_items SET count = count - '.$item['OppOrderItem']['count'].' WHERE id = '.$exist['OppItem']['id']);
                    }
                }
                
            
            	die(json_encode(array('result'=>true)));
            }    
			else 
				die(json_encode(array('result'=>false,'message'=>'Nepodarilo se ulozit do DB')));
		}
	}

	
	function load_typ($type = null, $return = false){
		$this->loadModel('OppItem');
		$list = $this->OppItem->find('list',array('conditions'=>array('type'=>$type),'fields'=>array('name','name'),'group'=>'name'));
		if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
	
    function load_size($type = null, $return = false){
		$this->loadModel('OppItem');
		$list = $this->OppItem->find('list',array('conditions'=>array('name'=>$type),'fields'=>array('size','size')));    

		if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
    
    
    function print_prehled($opp_order_item_id){
        $this->layout = 'print';
        
        $this->loadModel('OppOrderItem'); 
        $this->OppOrderItem->bindModel(array('belongsTo'=>array('CmsUser','OppOrder','Company')));
		$opp_order_items = $this->OppOrderItem->find('all', 
			array(
				'conditions' => array(
					'OppOrderItem.opp_order_id' => $opp_order_item_id, 
					'OppOrderItem.kos' =>0,
					'OppOrder.kos' =>0,
				)
			)
		);
		$this->set('opp_order_items',$opp_order_items); 
        $this->render('../opp_confirm_orders/print_prehled');      
    }
    
    function prepare_notification($order_id){
        $replace_list = array(
			'##id##' 		=>$order_id,
			'##datum##' 	=>date('d.m.Y')
		);
        
        $this->loadModel('OppOrder');
        $this->OppOrder->bindModel(array('belongsTo'=>array('CmsUser')));
        $email_cms = $this->OppOrder->find('first',array('conditions'=>array('OppOrder.id'=>$order_id),'fields'=>array('CmsUser.email')));
        unset($this->OppOrder);
          
        $this->Email->send_from_template_new(21,array($email_cms['CmsUser']['email']),$replace_list);
        die(json_encode(array('result'=>true)));
    }
    
    /**
     * Funkce ktera kontroluje zda uz neni nejake OPP rozdano,
     * pokud neni muzem objednavku vratit do stavu editace a zrusit potvrzeni tedy
     */
    function return_order($order_id = null){
        if($order_id != null){
                $detail = $this->OppOrder->read(null, $order_id);
                if($detail['OppOrder']['status'] == 0)
                    die(json_encode(array('result'=>false,'message'=>'Chyba: objednábka není potvrzena!')));
                    
			    /**
			      * Odečteme opp_items, pocet polozek ktere si objednal dany coo v objednavce
			      */ 
                $this->loadModel('OppItem');
                $this->loadModel('OppOrderItem');
                
                $items = $this->OppOrderItem->find('all',array(
                    'conditions'=>array(
                        'opp_order_id'=>$order_id,
                        'kos'=>0
                    ),
                    'fields'=>array('name','type_id','size','count','count_order')
                ));
                
                $opp_item_ids = array();
                
                foreach($items as $item){
                    if($item['OppOrderItem']['count'] == $item['OppOrderItem']['count_order']){
                        $exist = $this->OppItem->find('first',array('conditions'=>array('name'=>$item['OppOrderItem']['name'],'size'=>$item['OppOrderItem']['size'],'type'=>$item['OppOrderItem']['type_id'],'kos'=>0)));
                        if($exist){
                            $opp_item_ids[] = array(
                                'id'=>$exist['OppItem']['id'],
                                'count'=>$item['OppOrderItem']['count']
                            ); 
                        }
                    }
                    else{
                        die(json_encode(array('result'=>false,'message'=>'Bouhžel tato objednávka již nelze vrátit k editaci, některé OPP již byly rozdány klientům.')));
                    }
                }
                
                /**
                 * vratime opp na sklad
                 */
                foreach($opp_item_ids as $opp_item){
                   $this->OppItem->query('UPDATE wapis__opp_items SET count = count + '.$opp_item['count'].' WHERE id = '.$opp_item['id']);
                }
                
                /**
                 * vratim objednavku do stavu nevyrizeno, a umoznime timto editaci
                 */
                $this->OppOrder->save(array(
    				'id'		=> $order_id,
    				'status' 	=> 0,
    				'datum_potvrzeni' => "0000-00-00 00:00:00"
    			));
               
            
            	die(json_encode(array('result'=>true)));
    
        }
        else
            die(json_encode(array('result'=>false,'message'=>'Chyba: Neznáme ID objednávky')));
    }
}
?>