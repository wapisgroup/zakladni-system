<?php
Configure::write('debug',1);
class ImportClientJobsController extends AppController {
	var $name = 'ImportClientJobs';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email','Clients');
	var $uses = array('ImportClientJob');
	var $renderSetting = array(
		'SQLfields' => '*',
		'controller'=> 'import_client_jobs',
		'page_caption'=>'Klienti z www stránek Jobs Partners',
		//'count_group_by' => 'ImportClientJob.id',
		'group_by' => 'ImportClientJob.id',
		'sortBy'=>'ImportClientJob.id.DESC',
	
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'ImportClientJob-name|'		=>	'text|Jméno|',
			'ImportClientJob-save_ip|'		=>	'text|IP|',
		//	'ImportClientJob-stav'		=>	'select|Status|stav_client_list',
			//'ImportConnectionClientCareerItem-setting_career_item_id'		=>	'select|Profese|profese_list'
		),
		'items' => array(
			'id'		=>	'ID|ImportClientJob|id|text|',
			'name'		=>	'Jméno|ImportClientJob|name|text|',
			'mobil1'		=>	'Telefon|ImportClientJob|mobil1|text|',
			'created'	=>	'Vytvořeno|ImportClientJob|created|datetime|',
			'save_ip'	=>	'IP|ImportClientJob|save_ip|text|'
		),
        'class_tr'=>array(
            'class'=>'color_red',
            'model'=>'ImportClientJob',
            'col'=>'prevod',
            'value'=>1
        ),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Odstranit položku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/',$this->renderSetting['page_caption']=>'#'));
		
        if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			$this->set('admin', $this->logged_user['CmsGroup']['id']);
			
            // load stat list
			$this->loadModel('SettingStat'); 
			$this->SettingStat->query("SET NAMES 'utf8'");
			$this->set('client_stat_list',$this->SettingStat->find('list',array('conditions'=>array('SettingStat.status'=>1,'SettingStat.kos'=>0))));
			unset($this->SettingStat);
			
            // load kraj list
			$this->loadModel('Province'); 
			$this->Province->query("SET NAMES 'utf8'");
			$this->set('client_kraj_list',$this->Province->find('list',array('conditions'=>array())));
			unset($this->Province);
			
			if ($id != null){
				$this->data = $this->ImportClientJob->read(null,$id);
				$temp_stat_id = $this->data['ImportClientJob']['stat_id'];

			
            // load okresy list
			$this->loadModel('Countrie');
			$this->Countrie->query("SET NAMES 'utf8'");
			//$this->set('client_countries_list',null);
			$this->Countrie->bindModel(array('belongsTo'=>array('Province'=>array('foreignKey'=>'province_id'))));
			$this->set('client_countries_list',$cc = $this->Countrie->find('list',array(
				'conditions'=>array(
					'Countrie.status'=>1,
					'Countrie.kos'=>0,
					'Province.stat_id'=>$temp_stat_id
					
				), 'recursive'=>1
			)));
			unset($this->Countrie);
            
            }         
       						
			$this->render('edit');
   
        } else {
		  //pr($this->data);
          
			$this->ImportClientJob->save($this->data);		
			
		}
	}
	
	

	


}
?>