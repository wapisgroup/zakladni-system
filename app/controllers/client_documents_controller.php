<?php
class ClientDocumentsController extends AppController {
	var $name = 'ClientDocuments';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('ClientDocument');
	var $renderSetting = array(
		'controller'=>'client_documents',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'Nastavení dokumentů',
		'sortBy'=>'ClientDocument.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'ClientDocument-status'	=>	'select|Stav|select_stav_zadosti',
		//	'ClientDocument-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|ClientDocument|id|text|',
			'name'		=>	'Název|ClientDocument|name|text|',
			'updated'	=>	'Upraveno|ClientDocument|updated|datetime|',
			'created'	=>	'Vytvořeno|ClientDocument|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|delete'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[550,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení dokumentů'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->ClientDocument->read(null,$id);
			$this->render('edit');
		} else {
			$this->ClientDocument->save($this->data);
			die();
		}
	}
}
?>