<?php
//Configure::write('debug',1);

class ClientFireEmployeesController extends AppController {
	var $name = 'ClientFireEmployees';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('ConnectionClientRequirement');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','CompanyView'=>array('foreignKey'=>'company_id'),'CompanyWorkPosition','CompanyMoneyItem'),
			'hasOne'	=>	array(
                'ClientUcetniDokumentace'=>array(
                    'conditions' => array(
                        'ClientUcetniDokumentace.kos'=> 0,
                    )
                )
			)
		),
		'SQLfields' => '*',
		'SQLcondition' => array(
			//'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") = "#CURRENT_YEAR#-#CURRENT_MONTH#"))',
			'ConnectionClientRequirement.to <> "0000-00-00"',
			'ConnectionClientRequirement.type'=> 2
		),
		'controller'=> 'client_fire_employees',
		'page_caption'=>'Propuštení zaměstnanci AT',
		'sortBy'=>'ConnectionClientRequirement.to.DESC',
		'top_action' => array(),
		'filtration' => array(
			'Client-name'		=>	'text|Klient|',
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
            'CompanyWorkPosition-name|'			=>	'text|Pozice|',
			'ConnectionClientRequirement-to'		=>	'date_f_t|Datum|',
			'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
		),
		'items' => array(
			'id'				=>	'ID|ConnectionClientRequirement|id|hidden|',
			'client_id'			=>	'Klient ID|Client|id|text|',
			'client'			=>	'Klient|Client|name|text|',
			'ukonceni'			=>	'Datum ukončení|ConnectionClientRequirement|to|date|',
			'req'				=>	'Profese|CompanyWorkPosition|name|text|',
			'company'			=>	'Firma|CompanyView|name|text|',
			'cm'	        	=>	'CM|CompanyView|client_manager|text|',
			'coo'	           	=>	'COO|CompanyView|coordinator|text|',
			'coo2'		        =>	'COO2|CompanyView|coordinator2|text|',
            'forma'				=>	'Forma|CompanyMoneyItem|name|text|',
			'stav_dok'			=>	'#|ClientUcetniDokumentace|fire_stav|text|status_to_ico2#chybna_dokumentace'
	
		
		),
		'posibility' => array(
			'client_info'	=>	'client_info|Karta klienta|client_info',
		    'ucetni_dokumentace'	=>	'ucetni_dokumentace|Účetní dokumentace|ucetni_dokumentace'		
		),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);
	
     
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Propuštení zaměstnanci AT'=>'#'));
			
		$this->loadModel('CmsUser');
		$this->set('cms_user_list', $this->CmsUser->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

		$this->set('cm_coo_list', $this->CmsUser->find('list',
			array('conditions'=>array('cms_group_id IN (3,4)'),'fields'=>array('name','name'), 'order'=>'name ASC')
		));

		$this->loadModel('Company'); 	
		$company_conditions = array('Company.kos'=>0);
		
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		 
		$this->set('company_list',		$this->Company->find('list',array(
			'conditions'=>$company_conditions,
			'order'=>array('Company.name ASC')
		)));
		unset($this->Company);
        
        //změna profese podle nastaveneho company_work_position_id
		foreach($this->viewVars['items'] as &$item){
            
			//nastaveni forem odmeny a zobrazeni ubytovani a dopravy
			if(isset($item['CompanyMoneyItem'])){

					$doprava = 'Doprava '.($item['CompanyMoneyItem']['doprava'] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($item['CompanyMoneyItem']['cena_ubytovani_na_mesic'] != 0 ? 'Ano' : 'Ne');
	                $item['CompanyMoneyItem']['forma'] = $item['CompanyMoneyItem']['name'];
					$item['CompanyMoneyItem']['name'] = $item['CompanyMoneyItem']['name'].' - '.$ubytovani.' / '.$doprava;;
                
			}
     
		}

		$this->loadModel('SettingCareerItem');
		$this->set('profese_list', $this->SettingCareerItem->find('list',
			array('conditions'=>array('kos'=>0), 'order'=>'name ASC')
		));

			
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	
/**
     * @name ucetni dokumentace
     * @abstract na odpracovane hodiny id
     * @author Sol
     * @since 2.11.2009
     */
    function ucetni_dokumentace ($connection_client_requirement_id = null, $forma = null){
    //Configure::write('debug',1);
        if(empty($this->data)){
            if($connection_client_requirement_id == null)
                die('Chyba špatné spojení mezi klientem a náborem!!!');
            
            $this->loadModel('ClientUcetniDokumentace');
            $this->data = $this->ClientUcetniDokumentace->find('first',array(
                'conditions'=>array(
                    'connection_client_requirement_id'=>$connection_client_requirement_id,
                    'forma'=>$forma,
                    'kos'=>0
                )
            ));

   
            if(empty($this->data)){
               $this->data['ClientUcetniDokumentace'] = array(
                    'connection_client_requirement_id'=>$connection_client_requirement_id,
                    'forma'=>$forma,
               );
            }

            
           $this->set('forma',$forma);
            
            /**
             * nastavení prav zobrazeni
             * nyni editovat pouze ucetni a admin
             */
             
             $only_show_ukonceni = false;
             if(!in_array($this->logged_user['CmsGroup']['id'],array(1,6,7)))
                $only_show_ukonceni = true;
                
             $this->set('only_show',true);   
             $this->set('only_show_ukonceni',$only_show_ukonceni);   
             $this->set('ukonceni_pracovniho_pomeru',true);   
 
             
            //render
            $this->render('../employees/ucetni_dokumentace');
        }
        else {
            if($this->data['ClientUcetniDokumentace']['id'] == null)
                $this->data['ClientUcetniDokumentace']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
                
            $this->loadModel('ClientUcetniDokumentace');    
            $this->ClientUcetniDokumentace->save($this->data);    
            
        }

    }
	
	
	//zobrazeni karty klienta
	function client_info($id = null){
		echo $this->requestAction('clients/edit/'.$id.'/domwin/only_show');
		die();
	}
    

}
?>