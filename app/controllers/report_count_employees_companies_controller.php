<?php
//Configure::write('debug',2);
	$_GET['current_date'] 	= (isset($_GET['filtration_cce']) && !empty($_GET['filtration_cce']))?$_GET['filtration_cce']:date('Y-m-d');
	define('CURRENT_DATE', $_GET['current_date']);
	define('fields','CompanyView.*,
			( 
			  SELECT 
			   COUNT(client_id)
			  FROM wapis__connection_client_requirements
			  where type=2 and company_id=CompanyView.id and(
				`from` <= "'.CURRENT_DATE.'" AND (`to`>="'.CURRENT_DATE.'" OR `to`="0000-00-00")
			  ) 
			) as poc_zamestnancu	
		');
	define('con','
			( 
			  SELECT 
			   COUNT(client_id)
			  FROM wapis__connection_client_requirements as f
			  where type=2 and company_id=CompanyView.id and(
				f.from <= "'.CURRENT_DATE.'" AND (f.to >="'.CURRENT_DATE.'" OR f.to="0000-00-00")
			  ) 
			)<>0
		');
	
class ReportCountEmployeesCompaniesController extends AppController {
	var $name = ' ReportCountEmployeesCompanies';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CompanyView','Company');
	var $renderSetting = array(
		//'SQLfields' =>'*',
		'SQLfields' => array(fields),
		'SQLcondition' => array(con),
		// 'SQLhaving' => 'poc_zamestnancu<>0',
		// 'SQLcount' => count,
		
		// 'bindModel' => array('belongsTo' => array(
				// 'SalesManager' => array('className' => 'CmsUser', 'foreignKey' => 'self_manager_id'),
				// 'ClientManager' => array('className' => 'CmsUser', 'foreignKey' => 'client_manager_id'),
				// 'Coordinator' => array('className' => 'CmsUser', 'foreignKey' => 'coordinator_id'),
				// 'SettingStav'
		// )),
		'controller'=> 'report_count_employees_companies',
		'page_caption'=>'Report - Počet zaměstnanců',
		'sortBy'=>'NULL.poc_zamestnancu.DESC',
		'top_action' => array(	),
		'filtration' => array(
			'cce'		=>	'date|Datum|',
		 	
		),
		'items' => array(
			'id'					=>	'ID|CompanyView|id|text|',
			'name'					=>	'Název|CompanyView|name|text|',
			'mesto'					=>	'Město|CompanyView|mesto|text|',
			'sm_id' 		=>	'SalesManager|CompanyView|sales_manager|text|',
			'cm_id'		=>	'ClientManager|CompanyView|client_manager|text|',
			'coordinator'		=>	'Koordinátor|CompanyView|coordinator|text|',
			'coordinator2'		=>	'Koordinátor2|CompanyView|coordinator2|text|',
			'pocet_zamestnancu'		=>	'Zaměst.|0|poc_zamestnancu|text|'
		),
		'posibility' => array(

		)
	);
	
	function beforeFilter(){
		parent::beforeFilter();
		switch($this->logged_user['CmsGroup']['id']){
			case 3: // CM
				if (!isset($_GET['filtration_CompanyView-client_manager_id']))
					$_GET['filtration_CompanyView-client_manager_id'] = $this->params['url']['filtration_CompanyView-client_manager_id'] = $this->logged_user['CmsUser']['id'];
				break;
			case 4: // coordinator
				if (!isset($_GET['filtration_CompanyView-coordinator_id']))
					$_GET['filtration_CompanyView-coordinator_id'] = $this->params['url']['filtration_CompanyView-coordinator_id'] = $this->logged_user['CmsUser']['id'];
				break;
			case 2: // SM
				if (!isset($_GET['filtration_CompanyView-self_manager_id']))
					$_GET['filtration_CompanyView-self_manager_id'] = $this->params['url']['filtration_CompanyView-self_manager_id'] = $this->logged_user['CmsUser']['id'];
				break;
			default:
		}	
	}

	/**
 	* Returns a view, if not AJAX, load data for basic view. 
 	*
	* @param none
 	* @return view
 	* @access public
	**/
	function index(){
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			// set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'#','Report - Počet zaměstnanců'=>'#'));
	
					
			// load list for filter SM, KO, CM
			$this->loadModel('CmsUser');
			$this->set('self_manager_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>2))));
			$this->set('client_manager_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>3))));
			$this->set('koordinator_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>4))));
			
			
			
			$this->render('../system/index');
		}
	}
	
	
}
?>