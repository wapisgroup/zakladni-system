<?php
	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
	define('CURRENT_YEAR', $_GET['current_year']);
	define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $_GET['current_month'], $_GET['current_year']);
$last_day = date('N', mktime(0, 0, 0, $_GET['current_month'], 1, $_GET['current_year'])); 
$current_day = $last_day;
$pocet_pracovnich_dnu = 0;
$vikendy = array();
if($pocet_dnu_v_mesici > 0)
    for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
    	if(!in_array($current_day,array(6,7))){
    	   $pocet_pracovnich_dnu++;
    	} 
        else{
            $vikendy[] = $i;
        }
        $current_day = ($current_day == 7)?$current_day=1:$current_day+1;    
    }

define('fields','
    (SELECT COUNT(DISTINCT activity_datetime) FROM wapis__company_activities as CompanyActivity WHERE
         CompanyActivity.cms_user_id = CmsUser.id AND CompanyActivity.company_id = Company.id AND
         CompanyActivity.kos = 0 AND
         ((DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'")) AND
         DATE_FORMAT(CompanyActivity.created,"%Y-%m") = DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") AND
         DAYOFWEEK(CompanyActivity.activity_datetime) NOT IN (1,7) AND
         DAY(CompanyActivity.activity_datetime) NOT IN (SELECT den FROM wapis__setting_stat_svateks WHERE DAYOFWEEK(datum) NOT IN (1,7) AND rok = "'.CURRENT_YEAR.'" AND mesic = "'.CURRENT_MONTH.'" and setting_stat_id = Company.stat_id)
    ) as pocet_aktivit,
    ('.$pocet_pracovnich_dnu.' -(SELECT COUNT(den) FROM wapis__setting_stat_svateks WHERE DAYOFWEEK(datum) NOT IN (1,7) AND rok = "'.CURRENT_YEAR.'" AND mesic = "'.CURRENT_MONTH.'" and setting_stat_id = Company.stat_id) - (SELECT COUNT(DISTINCT activity_datetime) FROM wapis__company_activities as CompanyActivity WHERE
         CompanyActivity.cms_user_id = CmsUser.id AND CompanyActivity.company_id = Company.id AND
         CompanyActivity.kos = 0 AND
         ((DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'")) AND
         DATE_FORMAT(CompanyActivity.created,"%Y-%m") = DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") AND
         DAYOFWEEK(CompanyActivity.activity_datetime) NOT IN (1,7) AND
         DAY(CompanyActivity.activity_datetime) NOT IN (SELECT den FROM wapis__setting_stat_svateks WHERE DAYOFWEEK(datum) NOT IN (1,7) AND rok = "'.CURRENT_YEAR.'" AND mesic = "'.CURRENT_MONTH.'" and setting_stat_id = Company.stat_id)
    )) as chybejicih_reportu,
    (SELECT COUNT(client_id)
			  FROM wapis__connection_client_requirements
			  where type=2 and company_id=Company.id and(
				`from` <= "'. date('Y-m-d').'" AND (`to`>="'. date('Y-m-d').'" OR `to`="0000-00-00")
			  ) 
	) as poc_zamestnancu,
    (SELECT value FROM wapis__settings as setting Where id = IF(Company.stat_id = 1,7,8) LIMIT 1) as srazka,
    CONCAT_WS(" ",(('.$pocet_pracovnich_dnu.' -(SELECT COUNT(den) FROM wapis__setting_stat_svateks WHERE DAYOFWEEK(datum) NOT IN (1,7) AND rok = "'.CURRENT_YEAR.'" AND mesic = "'.CURRENT_MONTH.'" and setting_stat_id = Company.stat_id) - (SELECT COUNT(DISTINCT activity_datetime) FROM wapis__company_activities as CompanyActivity WHERE
             CompanyActivity.cms_user_id = CmsUser.id AND CompanyActivity.company_id = Company.id AND
             CompanyActivity.kos = 0 AND
             ((DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'")) AND
             DATE_FORMAT(CompanyActivity.created,"%Y-%m") = DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") AND
             DAYOFWEEK(CompanyActivity.activity_datetime) NOT IN (1,7) AND
             DAY(CompanyActivity.activity_datetime) NOT IN (SELECT den FROM wapis__setting_stat_svateks WHERE DAYOFWEEK(datum) NOT IN (1,7) AND rok = "'.CURRENT_YEAR.'" AND mesic = "'.CURRENT_MONTH.'" and setting_stat_id = Company.stat_id)
        ))
        *
        (SELECT value FROM wapis__settings as setting Where id = IF(Company.stat_id = 1,7,8) LIMIT 1)
    ),if(Company.stat_id = 2,",- EUR",",- Kč"))  as srazka_celkem,
    CmsUser.name,
    Company.id,
    Company.name,
    Company.stat_id,
    Company.manazer_realizace_id,
    ManazerRealizace.name,
    CmsUser.id,
    ('.$pocet_pracovnich_dnu.' - (SELECT COUNT(den) FROM wapis__setting_stat_svateks WHERE DAYOFWEEK(datum) NOT IN (1,7) AND rok = "'.CURRENT_YEAR.'" AND mesic = "'.CURRENT_MONTH.'" and setting_stat_id = Company.stat_id)) as pocet_pracovnich_dnu,
    ('.CURRENT_YEAR.') as year,
    ('.CURRENT_MONTH.') as month
');

Configure::write('debug',1);
class ReportCooActivityController extends AppController {
	var $name = 'ReportCooActivity';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CmsUser');
	var $renderSetting = array(
		'controller'=>'report_coo_activity',
        'bindModel' => array(
            //'belongsTo' => array('CmsUser','Company'),
            'joinSpec'=>array(
                'Company'=>array(
                    'primaryKey'=>'client_manager_id',
                    'foreignKey'=>'CmsUser.id',
                ),
                'ManazerRealizace'=>array(
                    'className'=>'CmsUser',
                    'primaryKey'=>'Company.manazer_realizace_id',
                    'foreignKey'=>'ManazerRealizace.id',
                )
            )
        ),
		'SQLfields' => fields,
	    'SQLcondition'=>array(
            'Company.id IS NOT NULL',
            'CmsUser.cms_group_id'=>4,//pouze COO aktivity
            //"((DATE_FORMAT(CompanyActivity.activity_datetime,'%Y-%m') = '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
            //'DATE_FORMAT(CompanyActivity.created,"%Y-%m") = DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m")',
            //'DAYOFWEEK(CompanyActivity.activity_datetime) NOT IN (1,7)',//pouze pracovni dny
        ),
        //'group_by'=>'CompanyActivity.cms_user_id, CompanyActivity.company_id',
        //'SQLhaving'=>'pocet_hodin > 0',
        'count_distinct'=>false,
        //'count_condition'=>'1=1 GROUP BY CompanyActivity.cms_user_id, CompanyActivity.company_id',
      	'page_caption'=>'Reporty aktivty KOO',
		'sortBy'=>'CmsUser.name.ASC',
		'top_action' => array(),
		'filtration' => array(
            'CmsUser-id'					=>	'select|Koordinátor|coo_list',
			'GET-current_year'					=>	'select|Rok|actual_years_list',
			'GET-current_month'					=>	'select|Měsíc|mesice_list',
            'Company-manazer_realizace_id'	=>	'select|Manažer real.|manager_realizace_list',
		),
		'items' => array(
	           'id'			    =>	'id|CmsUser|id|text|',
	           'name'			=>	'Koordinátor|CmsUser|name|text|',
               'firma'			=>	'Firma|Company|name|text|',
               'man_id' 		=>	'Manažér realizace|ManazerRealizace|name|text|',
               'poc_zamestnancu'		=>	'Počet zaměstanců|0|poc_zamestnancu|text|',
               'za_mesic'		=>	'Za měsíc|0|month|viewVars|mesice_list',
  			   'za_rok'			=>	'Za rok|0|year|text|',        
	           'pocet_pracovnich_dnu'	=>	'Pracovnich dní|0|pocet_pracovnich_dnu|text|',
               'pocet_aktivit'	=>	'Počet aktivti|0|pocet_aktivit|text|',
	  		   'chybejicih_reportu'		=>	'Chybí reportu|0|chybejicih_reportu|text|',
	  		   'srazka'		    =>	'Srážka za aktivitu|0|srazka|text|',
               'srazka_celkem'	=>	'Celková srážka|0|srazka_celkem|text|',
               
		),
		'posibility' => array(
			'show'		=>	'show|Detail|show'					
		),
        'posibility_link' => array(
            'show'		=>	'CmsUser.id/Company.id/0.year/0.month',
  		),
        'domwin_setting' => array(
			'sizes' 		=> '[800,250]',
			'scrollbars'	=> true,
			'languages'		=> true,
			'defined_lang'	=> "['cz','en','de']",
		),
        'checkbox_setting' => array(
			'model'			=>	'Company',
            'col'			=>	'id',
			'model2'		=>	'CmsUser',
            'col2'			=>	'id',
            'checked_on_start'=>true
			//'stav_array'	=>	array(3,4),
			//'month_year'	=>	true
		),
        'sum_variables_on_top'=>array(
            'path'=>'../report_coo_activity/filtration_top'
        )
	);
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Reporty aktivty KOO'=>'#'));
		$this->CmsUser->unbindModel(array('joinSpec'=>array('Company','ManazerRealizace')));
        $this->set('coo_list',$this->get_list('CmsUser',array('cms_group_id'=>4,'kos'=>0,'status'=>1)));
        //$this->CmsUser->unbindModel(array('joinSpec'=>array('Company','ManazerRealizace')));
		$this->set('manager_realizace_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>18))));
			
        $pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $_GET['current_month'], $_GET['current_year']);
        $last_day = date('N', mktime(0, 0, 0, $_GET['current_month'], 1, $_GET['current_year'])); 
        $current_day = $last_day;
        $pocet_pracovnich_dnu = 0;
        $vikendy = array();
        if($pocet_dnu_v_mesici > 0)
            for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
            	if(!in_array($current_day,array(6,7))){
            	   $pocet_pracovnich_dnu++;
            	} 
                else{
                    $vikendy[] = $i;
                }
                $current_day = ($current_day == 7)?$current_day=1:$current_day+1;    
            }
          
        $_ignored_items = array();
        if(isset($this->params['url']['chck'])){
            $_ignored_items = explode('|',rtrim($this->params['url']['chck'],'|'));
        }      
        $this->set('_ignored_items',$_ignored_items);
        
        //ty odškrtlé řádky vynulujeme
		foreach($this->viewVars['items'] as &$item){
            if(in_array($item['Company']['id'].'_'.$item['CmsUser']['id'],$_ignored_items)){
                $item[0]['srazka_celkem'] = '0 '.($item['Company']['stat_id'] == 1?',- CZK':',- EUR');
            }   
		}
        
        // celkova cena za srazky
            $this->CmsUser->bindModel($this->renderSetting['bindModel']);
    		$rr = $this->CmsUser->find('all',array(
                'conditions'=>$this->ViewIndex->criteria,
                'fields'=>array('Company.stat_id,Company.id,CmsUser.id,(('.$pocet_pracovnich_dnu.' - (SELECT COUNT(DISTINCT activity_datetime) FROM wapis__company_activities as CompanyActivity WHERE
                                     CompanyActivity.cms_user_id = CmsUser.id AND CompanyActivity.company_id = Company.id AND
                                     CompanyActivity.kos = 0 AND
                                     ((DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'")) AND
                                     DATE_FORMAT(CompanyActivity.created,"%Y-%m") = DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") AND
                                     DAYOFWEEK(CompanyActivity.activity_datetime) NOT IN (1,7) AND
                                     DAY(CompanyActivity.activity_datetime) NOT IN (SELECT den FROM wapis__setting_stat_svateks WHERE rok = "'.CURRENT_YEAR.'" AND mesic = "'.CURRENT_MONTH.'" and setting_stat_id = Company.stat_id)
                                    ))
                                    *
                                    (SELECT value FROM wapis__settings as setting Where id = IF(Company.stat_id = 1,7,8) LIMIT 1)
                                )  as srazka_celkem'
                ),
                //'group'=>'currency'
            ));
            $this->set('filtration_sum_variables2',$rr);    
        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

	function show($user_id,$company_id,$year,$month){
		if ($user_id != null){
            $month = ($month<10) ? '0'.$month : $month;

/**
 *          CompanyActivity.cms_user_id = CmsUser.id AND CompanyActivity.company_id = Company.id AND
 *          CompanyActivity.kos = 0 AND
 *          ((DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") = "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'")) AND
 *          DATE_FORMAT(CompanyActivity.created,"%Y-%m") = DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m") AND
 *          DAYOFWEEK(CompanyActivity.activity_datetime) NOT IN (1,7)
 */
         
			$this->loadModel('CompanyActivity');            
			 $act = $this->CompanyActivity->find('all',array(
                'conditions'=>array(
                    'CompanyActivity.kos'=>0,
                    'CompanyActivity.cms_user_id'=>$user_id,
                    'CompanyActivity.company_id'=>$company_id,
                    "((DATE_FORMAT(CompanyActivity.activity_datetime,'%Y-%m') = '".$year."-".$month."'))",
                    'DATE_FORMAT(CompanyActivity.created,"%Y-%m") = DATE_FORMAT(CompanyActivity.activity_datetime,"%Y-%m")',
                    'DAYOFWEEK(CompanyActivity.activity_datetime) NOT IN (1,7)',//pouze pracovni dny
                ),
                'fields'=>array('DAY(CompanyActivity.activity_datetime) as den'),
                'order'=>'CompanyActivity.activity_datetime ASC',
                'group'=>'CompanyActivity.activity_datetime'
            ));
			unset($this->CompanyActivity);
            
            $this->set('aktivity_list', Set::extract($act,'/0/den'));
            
            $this->loadModel('Company');  
            $comp = $this->Company->read('stat_id',$company_id);
            unset($this->Company);
            
             // load free days for state, month, year
    		$this->loadModel('SettingStatSvatek');
    		$this->set('svatky',$this->SettingStatSvatek->find('list', array(
                'conditions'=>array( 
                    'setting_stat_id'	=> $comp['Company']['stat_id'], 
                    'mesic'=> $month, 
                    'rok'=> $year
                ),
                'fields'=>array('id','den'),
                'order'=>'den ASC'
            ))); 				
		
 
            $this->set('month',$month);
			$this->set('year',$year);
			//render
			$this->render('show');	
	    }
		else 
			die('Bez id ? :-)');
	}
}
?>
