<?php
	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
	define('CURRENT_YEAR', $_GET['current_year']);
	define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

define('pocet_hodin',	'IFNULL((
                /* Pocet hodin k danemu coordinatorovi za dany rok a mesic, tedy sum dochazek k jeho firmam */
                SELECT 
                    SUM(cw.celkem_hodin) 
                FROM wapis__client_working_hours as cw
                LEFT JOIN wapis__connection_client_requirements as CC
                    ON(
                        CC.id = cw.connection_client_requirement_id 
                        AND 
                        ((DATE_FORMAT(CC.from,"%Y-%m")<= "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'"))
                        AND
                        ((DATE_FORMAT(CC.to,"%Y-%m") >= "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'") OR (CC.to = "0000-00-00"))
		                AND  
                        CC.type = 2 
                        AND
                        CC.company_id IN (SELECT id FROM wapis__companies WHERE CmsUser.id IN (client_manager_id,coordinator_id,coordinator_id2))
                    )   
                WHERE 
                /* id firem ktere patri k danemu coo */
                cw.company_id IN (SELECT id FROM wapis__companies WHERE CmsUser.id IN (client_manager_id,coordinator_id,coordinator_id2))
                AND cw.year = '.CURRENT_YEAR.' AND cw.month = '.CURRENT_MONTH.'
                AND CC.id <> 0 AND cw.celkem_hodin > 0
            ),0)');         
define('fields','
    CmsUser.name,
    CmsUser.id,
    ('.pocet_hodin.') as pocet_hodin,
    ('.CURRENT_YEAR.') as year,
    ('.CURRENT_MONTH.') as month,
    ROUND((1/2),2) as jednotka,
    ROUND((('.pocet_hodin.')*(1/2)),2) as odmena
');
define('con_count','('.pocet_hodin.')> 0');

//Configure::write('debug',2);
class ReportClientManagerFeesController extends AppController {
	var $name = 'ReportClientManagerFees';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CmsUser');
	var $renderSetting = array(
		'controller'=>'report_client_manager_fees',
		'SQLfields' => fields,
		//'SQLfields' => 'id,user_id,company_id,user_type,name,month,year,SUM(zaklad) as zaklad',
		'SQLcondition'=>array(
            'cms_group_id'=>4
        ),
        'SQLhaving'=>'pocet_hodin > 0',
        'count_condition'=>con_count,
        //'year = #CURRENT_YEAR# AND  month = #CURRENT_MONTH# and user_id IS NOT NULL',
		'page_caption'=>'Reporty - Odměny pro COO - hodiny',
		'sortBy'=>'CmsUser.name.ASC',
		'top_action' => array(
			//'add_item'		=>	'Přidat|edit|Přidat novou fakturu|add',
		),
		'filtration' => array(
			'GET-current_year'					=>	'select|Rok|actual_years_list',
			'GET-current_month'					=>	'select|Měsíc|mesice_list',
			//'Company-self_manager_id'			=>	'select|SalesManager|sm_list',
			//'CompanyInvoiceItem-company_id'		=>	'select|Společnost|company_list',
		),
		'items' => array(
	           'id'			    =>	'id|CmsUser|id|hidden|',
	           'name'			=>	'Koordinátor|CmsUser|name|text|',
               'za_mesic'		=>	'Za měsíc|0|month|viewVars|mesice_list',
  			   'za_rok'			=>	'Za rok|0|year|text|',        
	           'pocet_hodin'	=>	'Počet hodin|0|pocet_hodin|text|',
	  		   'jednotka'		=>	'Cena za jednotku|0|jednotka|text|',
	  		   'odmena'		    =>	'Odměna|0|odmena|text|',
	  	
    
    	/**
	  	        'id'			    =>	'user_id|ClientManagerFeeView|user_id|hidden|',
	  			'user'				=>	'User|ClientManagerFeeView|user_id|viewVars|user_list',
	  			'za_mesic'			=>	'Za měsíc|ClientManagerFeeView|month|text|',
	  			'za_rok'			=>	'Za rok|ClientManagerFeeView|year|text|',
	  			'zaklad'			=>	'Počet hodin|ClientManagerFeeView|zaklad|text|',
	  			'jednotka'			=>	'Cena za jednotku|ClientManagerFeeView|jednotka|text|',
	  			'odmena'			=>	'Odměna|ClientManagerFeeView|odmena|text|',
	      		
	   */	
                //'company'			=>	'Firma|ClientManagerFeeView|name|text|',
                //'procento'			=>	'Procento z FA|0|procento|procenta|',
    			//'odmena'			=>	'Odměna|0|odmena|money|',
		),
		'posibility' => array(
			'show'		=>	'show|Detaily odměny|show'					
		),
        'posibility_link' => array(
            'show'		=>	'CmsUser.id/0.year/0.month',
  		)
	);
	
	function index(){
		//pr($this->viewVars['items']);

		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Odměny pro COO - hodiny'=>'#'));
		

/**		
		//nacteni nastaveni cen za jednotku a predani hodnot pro pocitani
		$this->loadModel('Setting'); 
		$ceny = array();
		$ceny = $this->Setting->read(null,2);

		$jednotka_cm 	= $ceny['Setting']['value']['cena_za_j_cm'];
		$jednotka_coo 	= $ceny['Setting']['value']['cena_za_j_coo'];

	
 * 	foreach($this->viewVars['items'] as &$item){
 * 			$item[$this->uses[0]]['zaklad'] = $item[0]['zaklad'];
 * 			if ($item[$this->uses[0]]['user_type'] == 'client_manager'){
 * 				$item[$this->uses[0]]['jednotka'] = $jednotka_cm;
 * 				$item[$this->uses[0]]['odmena'] = $jednotka_cm * $item[$this->uses[0]]['zaklad'];
 * 			} else {
 * 				$item[$this->uses[0]]['jednotka'] = $jednotka_coo;
 * 				$item[$this->uses[0]]['odmena'] = $jednotka_coo * $item[$this->uses[0]]['zaklad'];
 * 			}
 * 		}
 */

		
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

	function show($user_id,$year,$month){
		if ($user_id != null){
            $month = ($month<10) ? '0'.$month : $month;
          /*
		    $this->loadModel('Company');
            $this->Company->bindModel(array(
                'hasOne'=>array(
                    'ClientWorkingHour'=>array(
                        'conditions'=>array(
                            'ClientWorkingHour.kos'=>0,
                            'year'=>$year,
                            'month'=>$month,
                            'celkem_hodin >'=>0
                        ),
                        'fields'=>array('celkem_hodin')
                    )
                )
            ));
			$firmy = $this->Company->find('all',array(
                'conditions'=>array(
                    'OR'=>array(
                        'client_manager_id'=>$user_id,
                        'coordinator_id'=>$user_id,
                        'coordinator_id2'=>$user_id
                    ),
                    'Company.kos'=>0
                ),
                'fields'=>array(
                    'id',
                    'name',
                    'SUM(celkem_hodin) as zaklad'
                ),
                'order'=>'name ASC',
                'group'=>'company_id HAVING zaklad > 0'
            ));
            unset($this->Company);
			$this->set('firmy_list',$firmy);
	        
            */
			$this->loadModel('ClientWorkingHour');
			$this->ClientWorkingHour->bindModel(array(
			    'belongsTo'=>array(
                    'Client',
                    'ConnectionClientRequirement'=>array(
                        'conditions'=>array(
                            "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".$year."-".$month."'))",
		                  	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.$year.'-'.$month.'") OR (ConnectionClientRequirement.to = "0000-00-00"))',
		                  	'type'=> 2
                        ),
                        'fields'=>array(
                            'ConnectionClientRequirement.id'
                        )
                    ),
                    'Company'=>array('fields'=>array('name'))
                )
		    ));

            
			$this->set('zamestnanci_list', $zam_list = $this->ClientWorkingHour->find('all',array(
                'conditions'=>array(
                     'ClientWorkingHour.company_id IN (SELECT id FROM wapis__companies WHERE '.$user_id.' IN (client_manager_id,coordinator_id,coordinator_id2))',
                     'ClientWorkingHour.year'=>$year,
                     'ClientWorkingHour.month'=>$month,
                     'ClientWorkingHour.celkem_hodin <>'=>0,
                     'ConnectionClientRequirement.id <>'=>0
                ),
                'fields'=>array('Client.name','celkem_hodin','Company.name','ClientWorkingHour.company_id'),
                'order'=>'Client.name ASC'
            )));
			unset($this->ClientWorkingHour);
            
            $tmp = Set::combine($zam_list,'{n}.Client.name','{n}.ClientWorkingHour.celkem_hodin','{n}.ClientWorkingHour.company_id');
            $firmy =  Set::combine($zam_list,'{n}.ClientWorkingHour.company_id','{n}.Company.name');


            $this->set('tmp',$tmp);
            $this->set('firmy_list',$firmy);

			
			//render
			$this->render('show');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}
	
	
	
}
?>
