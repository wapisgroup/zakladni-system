<?php
Configure::write('debug',1);
class MvConfirmOrdersController extends AppController {
	var $name = 'MvConfirmOrder';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('MvOrder');
	var $renderSetting = array(
		'bindModel'	=> array(
            'belongsTo'=>array('CmsUser')
        ),
		'SQLfields' => '*',
        'controller'=> 'mv_confirm_orders',
		'page_caption'=>'Objednavky na MV / potvrzení',
		'sortBy'=>'MvOrder.id.DESC',
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add'
		),
		'filtration' => array(
			'MvOrder-cms_user_id'	=>	'select|Koordinátor|coo_list',
		),
		'items' => array(
			'id'		=>	'ID|MvOrder|id|text|',
			'user'		=>  'Koordinátor|CmsUser|name|text|',
			'created'	=>	'Datum vytvoření|MvOrder|created|datetime|',
			'updated'	=>	'Datum zpracování|MvOrder|datum_potvrzeni|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
            'return_order'	=>	'return_order|Zrušení potvrzení objednávky|return_order',
			'delete'	=>	'trash|Odstranit položku|trash'			
		),
        'domwin_setting'=>array(
            'sizes'=>'[900,900]'
        ),
        'group_condition'=>array(
            array(
                'id'=>'18',
                'condition'=>"MvOrder.cms_user_id IN (SELECT (CASE MvOrder.cms_user_id 
                        WHEN client_manager_id THEN client_manager_id
                        WHEN coordinator_id THEN coordinator_id
                        WHEN coordinator_id2 THEN coordinator_id2
                        ELSE 0
                        END )
                   FROM wapis__companies WHERE manazer_realizace_id =#CmsUser.id# and kos = 0)
                   "
            )
        )
	);
	function index(){
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->loadModel('CmsUser');
            $this->CmsUser->bindModel(array('belongsTo'=>array('CmsGroup')));
			$coo_list = $this->CmsUser->find('all',array(
                'conditions'=>array('CmsGroup.cms_group_superior_id'=>2,'CmsUser.kos'=>0,'CmsUser.status'=>1),
                'fields'=>array('CmsUser.id','CmsUser.name'),
                'order'=>'CmsUser.name'
            ));
			$this->set('coo_list', Set::combine($coo_list,'/CmsUser/id','/CmsUser/name'));
            unset($this->CmsUser);
		
			$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			$this->loadModel('MvItem');
			$this->set('type_list',$this->MvItem->find('list', array('conditions'=>array('kos'=>0),'group'=>'druh','fields'=>array('druh','druh'))));
			//pr($typs);
			if ($id != null){
				$this->MvOrder->bindModel(array('hasMany'=>array('MvOrderItem')));
				$this->data = $this->MvOrder->read(null,$id);
				
				$names = array();
				foreach(Set::extract('/MvOrderItem/type_id',$this->data) as $t)
					$names[$t] = self::load_typ($t,true);
				$this->set('names',$names);
				

                /**
                 * $sizes = array();
				foreach(Set::extract('/MvOrderItem/name',$this->data) as $t)
					$sizes[$t] = self::load_size($t,true);
				$this->set('sizes',$sizes);
                 */
			}	

			$this->render('../mv_confirm_orders/edit');
		} else {
			
			if($this->MvOrder->save(array(
				'id'		=> $this->data['MvOrder']['id'],
				'status' 	=> 1,
				'datum_potvrzeni' => date("Y-m-d H:i:s")
			))){
			    /**
			      * Odečteme mv_items, pocet polozek ktere si objednal dany coo v objednavce
			      */ 
                $this->loadModel('MvItem');
                $this->loadModel('MvOrderItem');
                
                $items = $this->MvOrderItem->find('all',array(
                    'conditions'=>array(
                        'mv_order_id'=>$this->data['MvOrder']['id'],
                        'count >'=>0
                    ),
                    'fields'=>array('name','type_id','size','count')
                ));
                foreach($items as $item){
                    $exist = $this->MvItem->find('first',array('conditions'=>array('name'=>$item['MvOrderItem']['name'],'druh'=>$item['MvOrderItem']['type_id'],'kos'=>0)));
                    if($exist){
                        $this->MvItem->query('UPDATE wapis__mv_items SET pocet = pocet - '.$item['MvOrderItem']['count'].' WHERE id = '.$exist['MvItem']['id']);
                    }
                }
                
            
            	die(json_encode(array('result'=>true)));
            }    
			else 
				die(json_encode(array('result'=>false,'message'=>'Nepodarilo se ulozit do DB')));
		}
	}

	
	function load_typ($type = null, $return = false){
		$this->loadModel('MvItem');
		$list = $this->MvItem->find('list',array('conditions'=>array('druh'=>$type),'fields'=>array('name','name'),'group'=>'name'));
		if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
	
    function load_size($type = null, $return = false){
		$this->loadModel('MvItem');
		$list = $this->MvItem->find('list',array('conditions'=>array('name'=>$type),'fields'=>array('size','size')));    

		if ($return === false)
			die(json_encode($list));
		else
			return $list;
	}
    
    
    function print_prehled($mv_order_item_id){
        $this->layout = 'print';
        
        $this->loadModel('MvOrderItem'); 
        $this->MvOrderItem->bindModel(array('belongsTo'=>array('CmsUser','MvOrder','Company')));
		$mv_order_items = $this->MvOrderItem->find('all', 
			array(
				'conditions' => array(
					'MvOrderItem.mv_order_id' => $mv_order_item_id, 
					'MvOrderItem.kos' =>0,
					'MvOrder.kos' =>0,
				)
			)
		);
		$this->set('mv_order_items',$mv_order_items); 
        $this->render('../mv_confirm_orders/print_prehled');      
    }
    
    function prepare_notification($order_id){
        $replace_list = array(
			'##id##' 		=>$order_id,
			'##datum##' 	=>date('d.m.Y')
		);
        
        $this->loadModel('MvOrder');
        $this->MvOrder->bindModel(array('belongsTo'=>array('CmsUser')));
        $email_cms = $this->MvOrder->find('first',array('conditions'=>array('MvOrder.id'=>$order_id),'fields'=>array('CmsUser.email')));
        unset($this->MvOrder);
          
        $this->Email->send_from_template_new(62,array($email_cms['CmsUser']['email']),$replace_list);
        die(json_encode(array('result'=>true)));
    }
    
    /**
     * Funkce ktera kontroluje zda uz neni nejake OPP rozdano,
     * pokud neni muzem objednavku vratit do stavu editace a zrusit potvrzeni tedy
     */
    function return_order($order_id = null){
        if($order_id != null){
                $detail = $this->MvOrder->read(null, $order_id);
                if($detail['MvOrder']['status'] == 0)
                    die(json_encode(array('result'=>false,'message'=>'Chyba: objednábka není potvrzena!')));
                    
			    /**
			      * Odečteme mv_items, pocet polozek ktere si objednal dany coo v objednavce
			      */ 
                $this->loadModel('MvItem');
                $this->loadModel('MvOrderItem');
                
                $items = $this->MvOrderItem->find('all',array(
                    'conditions'=>array(
                        'mv_order_id'=>$order_id,
                        'kos'=>0
                    ),
                    'fields'=>array('name','type_id','size','count','count_order')
                ));
                
                $mv_item_ids = array();
                
                foreach($items as $item){
                    if($item['MvOrderItem']['count'] == $item['MvOrderItem']['count_order']){
                        $exist = $this->MvItem->find('first',array('conditions'=>array('name'=>$item['MvOrderItem']['name'],'size'=>$item['MvOrderItem']['size'],'type'=>$item['MvOrderItem']['type_id'],'kos'=>0)));
                        if($exist){
                            $mv_item_ids[] = array(
                                'id'=>$exist['MvItem']['id'],
                                'count'=>$item['MvOrderItem']['count']
                            ); 
                        }
                    }
                    else{
                        die(json_encode(array('result'=>false,'message'=>'Bouhžel tato objednávka již nelze vrátit k editaci, některé OPP již byly rozdány klientům.')));
                    }
                }
                
                /**
                 * vratime mv na sklad
                 */
                foreach($mv_item_ids as $mv_item){
                   $this->MvItem->query('UPDATE wapis__mv_items SET count = count + '.$mv_item['count'].' WHERE id = '.$mv_item['id']);
                }
                
                /**
                 * vratim objednavku do stavu nevyrizeno, a umoznime timto editaci
                 */
                $this->MvOrder->save(array(
    				'id'		=> $order_id,
    				'status' 	=> 0,
    				'datum_potvrzeni' => "0000-00-00 00:00:00"
    			));
               
            
            	die(json_encode(array('result'=>true)));
    
        }
        else
            die(json_encode(array('result'=>false,'message'=>'Chyba: Neznáme ID objednávky')));
    }
}
?>