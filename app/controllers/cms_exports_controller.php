<?php
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 2.10.2009
 */
class CmsExportsController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'CmsExports';
	var $uses = array('Client');
	var $layout = 'default';
		
	function index($id=null){
		die('Vyber si XML');
	}
	
	function certifikaty(){
		$this->loadModel('SettingCertificate');
		die(json_encode(
			$this->SettingCertificate->find(
				'list',
				array(
					'conditions' => array(
						'SettingCertificate.kos' => 0
					),
					'order' => 'name ASC'
				)
			)
		));
	}
	
	function vzdelani(){
		$this->loadModel('SettingEducation');
		die(json_encode(
			$this->SettingEducation->find(
				'list',
				array(
					'conditions' => array(
						'SettingEducation.kos' => 0
					),
					'order' => 'name ASC'
				)
			)
		));
	}
	
	function stat(){
		$this->loadModel('SettingStat');
		die(json_encode(
			$this->SettingStat->find(
				'list',
				array(
					'conditions' => array(
						'SettingStat.kos' => 0
					),
					'order' => 'name ASC'
				)
			)
		));
	}
	
	function kvalifikace(){
		$this->loadModel('SettingCareerItem');
		die(json_encode(
			$this->SettingCareerItem->find(
				'list',
				array(
					'conditions' => array(
						'SettingCareerItem.kos' => 0
					),
					'order' => 'name ASC'
				)
			)
		));
	}
}	
?>