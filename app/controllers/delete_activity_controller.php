<?php
Configure::write('debug',2);

if(!isset($_GET['show']) || $_GET['show'] == 'companies'){
DEFINE('fields','CONCAT(CompanyActivity.updated) as del_date, CONCAT(CompanyActivity.id) as id, CONCAT(CompanyActivity.company_id) as name, CONCAT(CompanyActivity.kos_cms_user_id) as kos_name');
DEFINE('model','CompanyActivity');
DEFINE('cons','CompanyActivity.kos = 1');
DEFINE('sorted','NULL.del_date.DESC');
}
else if(isset($_GET['show']) || $_GET['show'] == 'clients'){
DEFINE('fields','CONCAT(HistoryItem.updated) as del_date, CONCAT(HistoryItem.id) as id, CONCAT(HistoryItem.client_id) as name, CONCAT(HistoryItem.kos_cms_user_id) as kos_name');
DEFINE('model','HistoryItem');
DEFINE('cons','HistoryItem.kos = 1');
DEFINE('sorted','NULL.del_date.DESC');
}

class DeleteActivityController extends AppController {
	var $name = 'DeleteActivity';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array(model);
	var $renderSetting = array(
        'controller'=> 'delete_activity',
		'page_caption'=>'Smazané aktivity',
		'bindModel'	=> array('belongsTo'=>array('CmsUser'=>array('foreignKey'=>'kos_cms_user_id'))),
		'SQLfields' => array(fields),
        'SQLcondition'=>array(cons),
        'no_trash'=>true,
		'sortBy'=>sorted,
		'top_action' => array(
			'show_companies'		=>	'Firmy|show_companies|Firmy|show_companies',
			'show_clients'		    =>	'Klienti|show_clients|Klienti|show_clients'
		),
		'filtration' => array(),
		'items' => array(
			'id'		=>	'ID|0|id|text|',
            'name'		=>	'Aktivita u|0|name|var|name_list',
			'user'	     =>	'Aktivitu smazal|0|kos_name|var|cms_user_list',
			'updated'	=>	'Smazáno|0|del_date|datetime|'
		),
		'posibility' => array()
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
		
        $name_list = array();
        if(!isset($_GET['show']) || $_GET['show'] == 'companies'){
            $name_list = $this->get_list('Company');    
        }   
        else if(isset($_GET['show']) || $_GET['show'] == 'clients'){
            $name_list = $this->get_list('Client');    
        }    
        $this->set('name_list',$name_list);
        $this->set('cms_user_list',$this->get_list('CmsUser'));  
         
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}   
    
    function show_companies(){
        $link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&show=companies';
		$this->redirect($link);
		exit;
    } 
    
    function show_clients(){
        $link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&show=clients';
		$this->redirect($link);
		exit;
    } 
}
?>