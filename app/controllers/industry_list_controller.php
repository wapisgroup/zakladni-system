<?php
Configure::write('debug',1);
class IndustryListController extends AppController {
	var $name = 'IndustryList';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('IndustryList');
	var $renderSetting = array(
		'controller'=>'industry_list',
		'SQLfields' => '*',
		'page_caption'=>'Průmysl číselník',
		'sortBy'=>'IndustryList.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat|add',
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|IndustryList|id|text|',
			'name'		=>	'Název|IndustryList|name|text|',
			'updated'	=>	'Upraveno|IndustryList|updated|datetime|',
			'created'	=>	'Vytvořeno|IndustryList|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'			
		),
        'only_his'=>array(
            'all_set'=>true,
            'in_col'=>'id',
            'col'=>'id',
            'table'=>'wapis__at_projects',
            'where_col'=>'manager_id',
        )
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení průmyslu'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->IndustryList->read(null,$id);
                
			$this->render('edit');
		} else {
			$this->IndustryList->save($this->data);
			die();
		}
	}
}
?>