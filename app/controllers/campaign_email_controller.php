<?php
	Configure::write('debug',1);
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 16.9.2009
 */
 
define('fields',"*,
    (
       IF(CampaignEmail.type = 0,
         IFNULL((Select COUNT(sm.id) FROM wapis__campaign_email_messages as sm Where sm.group_id = `CampaignEmail`.id  GROUP BY sm.group_id),0),
         IFNULL(CampaignEmail.count_recipients - (Select COUNT(qu.id) FROM wapis__campaign_email_queue as qu Where qu.campaign_email_id = `CampaignEmail`.id),0)
       )
    )  as pocet_email   
");
class CampaignEmailController extends AppController {
	var $name = 'CampaignEmail';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Wysiwyg','FileInput');
	var $components = array('ViewIndex','RequestHandler','Email','Upload');
	var $uses = array('CampaignEmail');
	var $renderSetting = array(
		'bindModel' => array('belongsTo' => array('CmsUser')),
		'SQLfields' => array(fields),
		'controller'=> 'campaign_email',
		'page_caption'=>'Emailová - Kampaň',
		'sortBy'=>'CampaignEmail.created.DESC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Přidat novou emailovou kampaň|edit',
            'newsletter'		=>	'Newsletter|newsletter|Přidat novou newsletter kampaň|newsletter',
            'blacklist'		=>	'BlackList|blacklist|Správa BlackListu|blacklist',
		),
		'filtration' => array(
		),
		'items' => array(
			'id'			=>	'ID|CampaignEmail|id|text|',
            'type'			=>	'Typ|CampaignEmail|type|var|ce_type_list',
			'name'			=>	'Název|CampaignEmail|name|text|',
			'text'			=>	'Text|CampaignEmail|text|plaintext|',
			'vytvoril'		=>	'Vytvořil|CmsUser|name|text|',
			'count'		    =>	'Počet odeslaných emailů|0|pocet_email|text|',
			'created'		=>	'Vytvořeno|CampaignEmail|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace emailové kampaně|edit',
            'download'  =>  'download|Stáhnutí CSV|download',		
			'trash'		=>	'delete_campaign|Smazat|trash'		
		),
        'posibility_link' => array(
            'edit'		=> 'CampaignEmail.id/CampaignEmail.type',
            'delete_campaign'		=> 'CampaignEmail.id/CampaignEmail.type'
        ),
		'domwin_setting' => array(
			'sizes' 		=> '[900,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false',
		)
	);
    var $duplicity_emails = 0;
    
    function delete_campaign($id,$type){
        if($type == 1){//Newsletter, vyprazdni queue
            $this->loadModel('CampaignEmailQueue');
            $this->CampaignEmailQueue->deleteAll(array('campaign_email_id'=>$id));
        }
        $this->ViewIndex->render_trash($id);
        exit();
    }
	
	/**
	 * 
	 * @return view
	 */
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Emailová - Kampaň'=>'#'));
        $this->set('styles',array('../js/uploader/uploader.css','../js/wysiwyg_old/wswg.css','../js/clearbox/clearbox.css'));
        $this->set('scripts',array('uploader/uploader','wysiwyg_old/wswg','../js/clearbox/clearbox','wysiwyg_old/wswg'));
        
        $this->set('ce_type_list',array(0=>'Normální',1=>'Newsletter'));
        
  		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	/**
	 * Editace
	 * @param $id
	 * @return unknown_type
	 */
	function edit($id = null,$type = 0){
		$this->autoLayout = false;
        if($type == 1){
            self::newsletter($id);
        }else{
    		if (empty($this->data)){
    			// START natahnuti dat do filtrace
    			$to_load = array('SettingCertificate','SettingEducation','SettingCareerItem','SettingStat');
    			foreach($to_load as $model){
    				$this->loadModel($model);
    				$this->set(Inflector::underscore($model).'_list', $this->$model->find('list',array('conditions'=>array("$model.kos" => 0),'fields'=>array("$model.id","$model.name"))));
    				unset($this->$model);
    			}
    			// END natahnuti dat do filtrace
    			if ($id != null){
    				$this->data = $this->CampaignEmail->read(null,$id);
    				$this->loadModel('CampaignEmailMessage');
    				$this->set('sended_messages',$this->CampaignEmailMessage->find(
    					'all',
    					array(
    						'conditions'=>array(
    							'group_id' => $id
    						)
    					)
    				));
    			}
    			$this->render('edit');
    		} else {
    			$this->data['CampaignEmail']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
    			if ($this->CampaignEmail->save($this->data))
    				die(json_encode(array('result'=>true,'id'=>$this->CampaignEmail->id)));
    			else
    				die(json_encode(array('result'=>false,'message'=>'Chyba, nedošlo k uložení kampaně. Nelze pokračovat')));
    		}
        }
	}
    
    function newsletter($id = null){
        //$this->autoLayout = false;
		if (empty($this->data)){
		  
		    if ($id != null){
				$this->data = $this->CampaignEmail->read(null,$id);
                $groups = explode('|',$this->data['CampaignEmail']['group']);
                $i = 1;
                foreach($groups as $item){
                    $this->data['CampaignEmail']['group'][$i] = $item;
                    $i++;
                }
			}
            
            $this->set('group_list',array(
                1=>'Interní zaměstnanci',
                2=>'Leasingový zaměstnanci',
                3=>'Interní Nábor',
                4=>'Klienti WWW',
                5=>'Klienti JobsPartners',
                6=>'Klienti Estor Building',
                7=>'SDL',
                8=>'Propuštění zaměstnanci',
                9=>'Atecko.sk'
            ));
            $this->set('at_company_list',$this->get_list('AtCompany'));
            
            $this->render('newsletter');
		}
        else{
            $groups = $this->data['CampaignEmail']['group'];
            $this->data['CampaignEmail']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            $this->data['CampaignEmail']['group'] = join('|',$groups);
            $counter = 0;
            if($this->CampaignEmail->save($this->data)){
                
                foreach($groups as $group_id => $check){
                    if($check == 1 || $check == 'on'){
                        $model = $email_model = '';
                        $find_array = $bindModel = array();
                        switch($group_id){
                            case 1:
                                $model = 'ConnectionClientAtCompanyWorkPosition';
                                $find_array = array('conditions'=>array('ConnectionClientAtCompanyWorkPosition.datum_to'=>'0000-00-00'),'fields'=>array('Client.email'));
                                $bindModel = array('belongsTo'=>array('Client'));
                                $email_model = 'Client';
                            break;
                            case 2:
                                $model = 'ConnectionClientRequirement';
                                $find_array = array('conditions'=>array('ConnectionClientRequirement.kos'=>0,
            						'ConnectionClientRequirement.to'=>'0000-00-00',
            						'(ConnectionClientRequirement.type = 2)',
                                    'Client.newsletter_disable'=>0),'fields'=>array('Client.email'));
                                $bindModel = array('belongsTo'=>array('Client'));
                                $email_model = 'Client';
                            break;
                            case 3:
                                $model = 'ClientView';
                                $find_array = array('conditions'=>array(
                                    'OR'=>array(
                        			     'ConnectionClientRecruiter.cms_user_id'=>-1,
                        			     'ClientView.parent_id is not NULL'
                                    ),
                                    'ClientView.stav !='=>3),
                                    'fields'=>array('ClientView.email'),
                                    'group'=>'ClientView.id'
                                );   
                                $bindModel = array('hasOne'=>array('ConnectionClientRecruiter' => array('className' => 'ConnectionClientRecruiter', 'foreignKey' => 'client_id')));
                                $email_model = 'ClientView';
                            break;
                            case 4:
                                $model = 'ImportClient';
                                $find_array = array('conditions'=>array('ImportClient.kos'=>0),'fields'=>array('ImportClient.email'));   
                                $email_model = 'ImportClient';
                            break;
                            case 5:
                                $model = 'ImportClientJob';
                                $find_array = array('conditions'=>array('ImportClientJob.kos'=>0),'fields'=>array('ImportClientJob.email'));   
                                $email_model = 'ImportClientJob';
                            break;
                            case 6:
                                $model = 'ImportClientEstor';
                                $find_array = array('conditions'=>array('ImportClientEstor.kos'=>0),'fields'=>array('ImportClientEstor.email'));   
                                $email_model = 'ImportClientEstor';
                            break;
                            case 7:
                                $model = 'ImportClientSdl';
                                $find_array = array('conditions'=>array('ImportClientSdl.kos'=>0),'fields'=>array('ImportClientSdl.email'));   
                                $email_model = 'ImportClientSdl';
                            break;
                            case 8:
                                $model = 'ConnectionClientRequirement';
                                $find_array = array('conditions'=>array('ConnectionClientRequirement.to <> "0000-00-00"','ConnectionClientRequirement.type'=> 2),'fields'=>array('Client.email'));   
                                $bindModel = array('belongsTo'=>array('Client'));
                                $email_model = 'Client';
                            break;
                            case 9:
                                 $url_atecko_file = 'http://www.atecko.sk/export-clients/ccf41cdfa6cc5e9ea81b7327a5d97559099fa9ff4ccf14e2e07c153d1a23db43/';
                                 $xml = simplexml_load_file($url_atecko_file);
                                 $items = $xml->xpath('/clients/client');
                                 $i = 0;
                                 $this->loadModel('CampaignEmailQueue');
                                 foreach($items as $_email){
                                   $_email = get_object_vars($_email); 
                                   if($_email['mail'] != ''){
                                        $this->CampaignEmailQueue->id = null;
                                        Configure::write('debug',0);
                                        @$this->CampaignEmailQueue->save(array(
                                            'campaign_email_id'=>$this->CampaignEmail->id,
                                            'group_id'=>$group_id,
                                            'email'=>trim($_email['mail'])
                                        ));
                                    
                                        $i++;
                                   } 
                                 }
                                 $counter += $i;
                            break;
                        }
                        if($model != '' || !empty($find_array))
                            $counter += self::add_group_to_mail_lists($this->CampaignEmail->id,$group_id,$model,$find_array,$bindModel,$email_model);
                    }
                }
                
                //ulozeni dodatecnych adres do fronty
                $additional_recipients = explode(',',$this->data['CampaignEmail']['additional_recipients']);
                $counter += count($additional_recipients);
                $this->loadModel('CampaignEmailQueue');
                if(!empty($additional_recipients))
                    foreach($additional_recipients as $recipe){
                        if($recipe != ''){
                            $this->CampaignEmailQueue->id = null;
                            Configure::write('debug',0);
                            @$this->CampaignEmailQueue->save(array(
                                'campaign_email_id'=>$this->CampaignEmail->id,
                                'group_id'=>'-1',
                                'email'=>trim($recipe)
                            ));
                        }
                    }
               
               /**
                * Lidi co jsou v blacklistu u dane spolecnosti
                * budou vyrazeni z fronty
                */   
                $this->CampaignEmail->query('DELETE FROM `wapis__campaign_email_queue`
                WHERE campaign_email_id = '.$this->CampaignEmail->id.' and email IN(
                    SELECT email FROM `wapis__campaign_email_blacklist` Where kos = 0 and at_company_id = '.$this->data['CampaignEmail']['at_company_id'].')');
                
                /**
                 * spocitani emailovych adres
                 * a vytvoreni csv souboru se vsemi emailovymi adresami. 
                 */  
                $emails_list = $this->CampaignEmailQueue->find('all',array('conditions'=>array('campaign_email_id'=>$this->CampaignEmail->id)));    
                $path_file = './uploaded/campaign_email/csv/';
                $name_file = 'CampaignEmail_'.$this->CampaignEmail->id.'.csv';
                $data_file = null;
                $count = 0;
                foreach($emails_list as $email){
                    $data_file .= $email['CampaignEmailQueue']['email']."\n";
                    $count++;
                }
                $handle = fopen($path_file.$name_file, 'w+');
                fwrite($handle, $data_file);
                fclose($handle);
                
                
                $this->CampaignEmail->saveField('count_recipients',$count);
                $za_minutu = 40;
                $minut = ceil($count / 40);
                $time = new DateTime();
                $time->modify('+'.$minut.' minutes');
                die(json_encode(array('result'=>true,'message'=>'Počet příjmeců: '.$count.', odhadovaný čas dokončení: '.$time->format('d.m. H:i'))));    
            }
            else    
                die(json_encode(array('result'=>false)));
            
        }  
    }
    
    function add_group_to_mail_lists($campaing_id,$group_id,$model,$find_array = array(),$bindModel = array(),$email_model = null){
        $this->loadModel('CampaignEmailQueue');
        $this->loadModel($model);
        $this->{$model}->bindModel($bindModel);
        $new_emails = $this->{$model}->find('all',$find_array);


        foreach($new_emails as $item){
                $mail = $item[($email_model==null?$model:$email_model)]['email'];
                if($mail != ''){
                    //echo $mail."\n";
                    $this->CampaignEmailQueue->id = null;
                    Configure::write('debug',0);
                    @$this->CampaignEmailQueue->save(array(
                        'campaign_email_id'=>$campaing_id,
                        'group_id'=>$group_id,
                        'email'=>$mail
                    ));                    
                }            
        }
        unset($this->{$model});
        return count($new_emails);
    }
	
function load_client(){
		$this->set('load_limit',$load_limit = 400);
		$this->autoLayout = false;
		
		// primarni condition
		$conditions = array('kos' => 0, 'newsletter_disable'=>0);
		// filtrace, pouze pro platna tel cisla
		
		// START filtrace
		// ** mesto
		if (isset($this->data['LoadClient']['mesto']) && !empty($this->data['LoadClient']['mesto'])){
			$conditions['mesto LIKE'] = $this->data['LoadClient']['mesto'];
		}
		// ** pohlavi
		if (isset($this->data['LoadClient']['pohlavi']) && !empty($this->data['LoadClient']['pohlavi'])){
			$conditions['pohlavi_list'] = $this->data['LoadClient']['pohlavi'];
		}
		// ** stat
		if (isset($this->data['LoadClient']['stat']) && !empty($this->data['LoadClient']['stat'])){
			$conditions['stat_id'] = $this->data['LoadClient']['stat'];
		}
		// ** vzdelani
		if (isset($this->data['LoadClient']['vzdelani']) && !empty($this->data['LoadClient']['vzdelani'])){
			$conditions['dosazene_vzdelani_list'] = $this->data['LoadClient']['vzdelani'];
		}
		
		// ** certifikat
		if (isset($this->data['LoadClient']['certifikat']) && !empty($this->data['LoadClient']['certifikat'])){
			$conditions['ConnectionClientCertifikatyItem.setting_certificate_id'] = $this->data['LoadClient']['certifikat'];
		}
		
		// ** profese
		if (isset($this->data['LoadClient']['profese']) && !empty($this->data['LoadClient']['profese'])){
			$conditions['ConnectionClientCareerItem.setting_career_item_id'] = $this->data['LoadClient']['profese'];
		}

		// ** pouze uchazeci
		if (isset($this->data['LoadClient']['uchazeci']) && $this->data['LoadClient']['uchazeci'] == 1){
			$conditions['stav'] = 0;
		}

		// ** interní nábor
		if (isset($this->data['LoadClient']['interni']) && $this->data['LoadClient']['interni'] == 1){
			$conditions['ConnectionClientRecruiter.cms_user_id'] = -1;
		}
		
		// END filtrace
		$this->loadModel('Client');
	    $this->Client->bindModel(array(
			'hasOne'=>array(
				'ConnectionClientCareerItem',
				'ConnectionClientCertifikatyItem',
				'ConnectionClientRecruiter'
			)
		));
		$count = $this->Client->find('count',array('conditions'=>$conditions));
		
		// nastav, pokud je pocet nalezenych vetsi nez $load_limit
		$this->set('count_limit_over',($count > $load_limit));
	
		$client_list = $this->Client->find(
			'all',
			array(
				'conditions'=> $conditions,
				'limit' 	=> $load_limit,
				'fields' 	=> array(
					'DISTINCT Client.id',
					'Client.email',
					'Client.name'
				),
				'order'		=> 'Client.name ASC'
			)
		);	
		$this->set('choose_list',$client_list);
		$this->render('choose_list');
	}
	/**
	 * Odeslani jednotlive CampaignEmail
	 * @return JSON
	 */
	function send_item(){
      
        $data_for_send = array(
			'text' => $this->data['CampaignEmail']['text'],
			'subject' => $this->data['CampaignEmail']['name'],
			'from' => $this->data['CampaignEmail']['email_from'],
			'from_name' => $this->data['CampaignEmail']['email_from_name'],
			'attach_path' => '/uploaded/campaign_email/'
		);
        $attach = $sended = array();
        if(isset($this->data['CampaignEmail']['attach'])){
            $i=0;
            foreach($this->data['CampaignEmail']['attach'] as $item){
                $attach[$i]['asfile'] = $item;
                $attach[$i]['filename'] = $item;
                $i++;
            }
            
        }
        
		$tmp_send = $this->Email->send_without_template($data_for_send,array($this->data['CampaignEmail']['email']),array(),$attach);
        $sended['result'] = $tmp_send;
        $sended['email'] = $this->data['CampaignEmail']['email'];
       
        /*
         * Uloz zaznam o zaslani emailu 
         */
        
        $this->loadModel('CampaignEmailMessage');
        $this->CampaignEmailMessage->save(array(
            'email' => $this->data['CampaignEmail']['email'] ,
            'client_name' =>$this->data['CampaignEmail']['client_name'],
            'client_id' =>$this->data['CampaignEmail']['client_id'],
            'model' => 'CompaignEmail',
            'text' =>$this->data['CampaignEmail']['text'],
            'group_id' => $this->data['CampaignEmail']['group_id']
        ));
		die(json_encode($sended));
		
	}
	
	/**
	 * Ulozeni logace ke CampaignEmail kampani
	 * @return JSON
	 */
	function save_log(){
		if ($this->CampaignEmail->save($this->data))
			die(json_encode(array('result'=>true)));
		else
			die(json_encode(array('result'=>false,'Chyba během ukladani logace ke kampani')));
	}
    
    	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
    
    /**
     * Sprava blacklistu
     */
    function blacklist(){
        $this->loadModel('CampaignEmailBlacklist');
        $this->CampaignEmailBlacklist->bindModel(array('belongsTo'=>array('AtCompany')));
        $this->set('item_list',$this->CampaignEmailBlacklist->find('all',array(
            'conditions'=>array('CampaignEmailBlacklist.kos'=>0),
            'order'=>'CampaignEmailBlacklist.created DESC'
        )));
    }
    
    function blacklist_add($id = null){
        if(empty($this->data)){
            $this->set('realizator_list',$this->get_list('AtCompany'));
        }
        else{
            $this->loadModel('CampaignEmailBlacklist');
            $this->CampaignEmailBlacklist->save($this->data);
        }
        
    }
    
    function blacklist_trash($id){
        $this->loadModel('CampaignEmailBlacklist');
        $this->CampaignEmailBlacklist->id = $id;
        $this->CampaignEmailBlacklist->saveField('kos',1);
        die();
    }
    
    /**
     * Stazeni CSV logu emailovych adres
     */
    function download($id){
        $file_name = 'CampaignEmail_'.$id;
        $pripona = 'csv';
        $file = '/campaign_email/csv/'.$file_name.'.'.$pripona;
        
        if(!file_exists('../atep/uploaded/'.$file))
            die('Soubor neexistuje!');
            
		$filesize = filesize('../atep/uploaded/'.$file);
		$cesta = "http://".$_SERVER['HTTP_HOST']."/uploaded/".$file;
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();
    }
	
}
/*
 * SELECT C.name,C.recruiter_text, CU.name, CU.id FROM `wapis__clients` as C
JOIN 
    wapis__cms_users as CU
ON (C.recruiter_text  LIKE CU.name)
WHERE CU.cms_group_id = 8
 */
?>