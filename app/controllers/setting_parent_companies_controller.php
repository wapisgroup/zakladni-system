<?php
class SettingParentCompaniesController extends AppController {
	var $name = 'SettingParentCompanies';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingParentCompany');
	var $renderSetting = array(
		'controller'=>'setting_parent_companies',
		'SQLfields' => array('id','name','email','updated','created','status','account'),
		'page_caption'=>'Nastavení nadřazené společnosti',
		'sortBy'=>'SettingParentCompany.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
			//'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
			//'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			//'SettingParentCompany-status'	=>	'select|Stav|select_stav_zadosti',
			//'SettingParentCompany-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingParentCompany|id|text|',
			'name'		=>	'Název|SettingParentCompany|name|text|',
			'acount'		=>	'Účet|SettingParentCompany|account|text|',
			'email'		=>	'Email|SettingParentCompany|email|text|',
			'updated'	=>	'Upraveno|SettingParentCompany|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingParentCompany|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do kosiku|delete'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení nadřazené společnosti'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->SettingParentCompany->read(null,$id);
			}
			$this->render('edit');
		} else {
			$this->SettingParentCompany->save($this->data);
			die();
		}
	}
}
?>