<?php	
//Configure::write('debug',2);
class ReportStandardHoursController extends AppController {
	var $name = 'ReportStandardHours';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CompanyWorkPosition');
	var $renderSetting = array(
		'bindModel' => array(
			'belongsTo' => array(
				'CompanyView' => array('className' => 'CompanyView', 'foreignKey' => 'company_id')
			),
			'hasOne'=>array('CompanyMoneyItem')
		),
		'controller'=>'report_standard_hours',
		'SQLfields' => array(
			'DISTINCT CompanyWorkPosition.id',
			'CompanyView.name',
			'CompanyWorkPosition.name',
			'CompanyWorkPosition.standard_hours',
			'MAX(CompanyMoneyItem.normo_hodina) as normo_hodina'
		),
		'SQLcondition'=>array(
			'CompanyWorkPosition.kos'=>0,
			'CompanyWorkPosition.test'=>0
		),
		'page_caption'=>'Reporty - nastavení normo hodinu u profese',
		'sortBy'=>'CompanyWorkPosition.created.DESC',
		'group_by'=>'CompanyWorkPosition.id',
		'top_action' => array(	),
		'filtration' => array(
			'CompanyWorkPosition-company_id'		=>	'select|Společnost|company_list',
		),
		'items' => array(
			'id'				=>	'ID|CompanyWorkPosition|id|hidden|',
			'company'			=>	'Firma|CompanyView|name|text|',
			'name'			=>	'Název|CompanyWorkPosition|name|text|',
			'standard_hours'			=>	'Upravene normohodiny ucetni|CompanyWorkPosition|standard_hours|text|',
			'standard_hours2'			=>	'Normohodiny z kalkulace|0|normo_hodina|text|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Změna normo hodiny|edit',			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[500,500]',
			'scrollbars'	=> 'false',
			'languages'		=> 'false'
		)
	);
	
	function index(){
		//pr($this->viewVars['items']);
	
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Reporty - nastavení normo hodinu u profese'=>'#'));
		
		$this->loadModel('Company'); 	
		$company_conditions = array('Company.kos'=>0);	 
		$this->set('company_list',		$this->Company->find('list',array(
			'conditions'=>$company_conditions,
			'order'=>array('Company.name ASC')
		)));
		unset($this->Company);
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id){
		if (empty($this->data)){
			$this->data = $this->CompanyWorkPosition->read(null,$id);
		}
		else {
			$this->CompanyWorkPosition->save($this->data);
		}
	
	}
	
	
	
	function repair($id,$working_hour_id){
		if (empty($this->data))
	  	{
			
			// promenne
	        $this->set('id',$id);
	        $this->set('working_hour_id',$working_hour_id);
	       
			//render
	        $this->render('repair');
	  	}
	  	else {
			include('select_config.php');
			// ulozeni stavu klienta / working hour
			$this->loadModel('ClientWorkingHour');
			$this->ClientWorkingHour->id = $working_hour_id;
			$this->ClientWorkingHour->saveField('stav',1);
			

			// ulozeni stavu firmy
			$this->ConnectionCompanyToAuthorization->id = $id;
			$this->ConnectionCompanyToAuthorization->saveField('stav',0);

			//vytvoreni ukolu
			$this->ConnectionCompanyToAuthorization->bindModel(array(
			'belongsTo'=>array('Company')
		    ));
			$detail = $this->ConnectionCompanyToAuthorization->read(array('company_id','year','month','Company.name','Company.coordinator_id','Company.client_manager_id'),$id);
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array('Client')
		    ));
			$client = $this->ClientWorkingHour->read(array('Client.name'),$working_hour_id);


			$termin = new DateTime();
			$termin->modify("+2 day");

			if($detail['Company']['client_manager_id'] == '')
				$cms_user_id = $detail['Company']['client_manager_id'];
			else
				$cms_user_id = $detail['Company']['coordinator_id'];
				
			$task_to_save = array('CompanyTask'=>array(
				'cms_user_id' 	=> 	$cms_user_id, 
				'name'			=> 	'Odpracované hodiny k opravě',
				'termin'		=>	$termin->format("Y-m-d"),
				'company_id'	=>	$detail['ConnectionCompanyToAuthorization']['company_id'],
				'setting_task_type_id' => 9,
				'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
				'text'			=> 'Odpracované hodiny k opravě ve společnosti <strong>'.$detail['Company']['name'].'</strong> u klienta <strong>'.$client['Client']['name'].'</strong> za měsíc <strong>'.$this->mesice_list[$detail['ConnectionCompanyToAuthorization']['month']].'</strong> a rok <strong>'.$detail['ConnectionCompanyToAuthorization']['year'].'</strong><br /><br /> Komentář :<br />'.$this->data['text'].' .'
			));
			$this->loadModel('CompanyTask');
			$this->CompanyTask->save($task_to_save);
			
			unset($this->CompanyTask);
			unset($this->ClientWorkingHour);
			die();
	    }		
	}
	
	
	
}
?>