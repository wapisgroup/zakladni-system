<?php
//Configure::write('debug',1);

$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');

$stav = '';
if(isset($_GET['stav']) && $_GET['stav'] != ''){ 
    if($_GET['stav'] == 0)
        $stav = ' vraceno = 0 AND srazka_ze_mzdy = 0 ';
    else
        $stav = ' (vraceno = 1 OR srazka_ze_mzdy = 1) ';      
}

$coo = '';
if(isset($_GET['coo']) && $_GET['coo'] != ''){ 
        $coo = " (IF(vraceno = 0 AND srazka_ze_mzdy = 0,ConnectionClientOpp.cms_user_id,ConnectionClientOpp.cms_user_id2) = '".$_GET['coo']."') ";      
}

@define('CURRENT_YEAR', $_GET['current_year']);
@define('STAV', $stav);  
@define('COO', $coo); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class ReportReturnedOppsController extends AppController {
    var $name = 'ReportReturnedOpps';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('ConnectionClientOpp');
	var $renderSetting = array(
		'SQLfields' => '*,
           IF(vraceno = 0 AND srazka_ze_mzdy = 0,0,1) as stav,
           IF(vraceno = 0 AND srazka_ze_mzdy = 0,ConnectionClientOpp.created,ConnectionClientOpp.updated) as datum,
           IF(vraceno = 0 AND srazka_ze_mzdy = 0,COO1.name,COO2.name) as coo_name
        ',
		'bindModel' => array(
            'belongsTo' => array(
                'COO1'=>array('className'=>'CmsUser','foreignKey'=>'cms_user_id'),
                'COO2'=>array('className'=>'CmsUser','foreignKey'=>'cms_user_id2'),
            )
        ),
		'controller'=> 'report_returned_opps',
		'page_caption'=>'Report vrácených OPP',
		'sortBy'=>'ConnectionClientOpp.created.DESC',
		'top_action' => array(),
        'SQLcondition'=>array(
            "((DATE_FORMAT(IF(vraceno = 0 AND srazka_ze_mzdy = 0,ConnectionClientOpp.created,ConnectionClientOpp.updated),'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
            STAV,COO
        ),	
        'filtration' => array(
            'ConnectionClientOpp-typ' => 'select|Položka|type_list',
            'GET-stav'		          =>	'select|Stav|returned_opp_stav_list',
            'GET-coo'                 => 'select|CM/COO|coo_list',
            'GET-current_month'		  =>	'select|Měsíc|mesice_list',
            'GET-current_year'		  =>	'select|Rok|actual_years_list',
            
            //'CompanyOrderItem-nabor' => 'select|Je nábor|select_ano',
            //'CompanyOrderItem-order_status' => 'select|Nábor status|select_order_status',
            //'Company-id' => 'select|Společnost|company_list',
        ),
		'items' => array(
			'id'				=>	'ID|ConnectionClientOpp|id|text|',
	        'typ'				=>	'Položka|ConnectionClientOpp|typ|text|',
            'size'				=>	'Velikost|ConnectionClientOpp|size|text|',	
			'datum'		        =>	'Datum vyskladneni/vraceni|0|datum|date|',
            'stav'		        =>	'Stav|0|stav|var|returned_opp_stav_list',
            'coo'		        =>	'Koordinator|0|coo_name|text|',
            'price'				=>	'Cena|ConnectionClientOpp|price|text|',
		),
		'posibility' => array(),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'		=> true,
			'languages'	=> 'false'
		)
	);
    
    function index(){
        $this->loadModel('OppItem');
		$this->set('type_list',$this->OppItem->find('list', array('group'=>'type','fields'=>array('type','type'))));        
        unset($this->OppItem);
        
        
        $this->loadModel('CmsUser');
		$this->set('coo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
        unset($this->CmsUser);
        
        if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {			
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
           
		}
	}
   
	
}
?>