<?php
Configure::write('debug',1);
class WwwInsertionsController extends AppController {
	var $name = 'WwwInsertions';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('WwwInsertion');
	var $renderSetting = array(
        'bindModel'=>array('belongsTo'=>array('CmsUser')),
		'controller'=>'www_insertions',
		'SQLfields' => '*',
		'page_caption'=>'WWW Inzerce',
		'sortBy'=>'WwwInsertion.created.DESC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat inzerci|add',
		),
		'filtration' => array(
			'WwwInsertion-cms_user_id'	=>	'select|Náborář|naborari_list',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|WwwInsertion|id|text|',
			'name'		=>	'Název|WwwInsertion|name|text|',
            'city'		=>	'Místo|WwwInsertion|city|text|',
            'napln'		=>	'Náplň|WwwInsertion|napln|text|',
            'pozadavky'		=>	'Požadujeme|WwwInsertion|pozadavky|text|',
            'nabizime'		=>	'Nabizíme|WwwInsertion|nabizime|text|',
            'plat'		=>	'Plat|WwwInsertion|plat|text|',
            'user'		=>	'Přidal|CmsUser|name|text|',
			'updated'	=>	'Upraveno|WwwInsertion|updated|datetime|',
			'created'	=>	'Vytvořeno|WwwInsertion|created|datetime|'
		),
		'posibility' => array(
            'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'			
		),
        'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
			'defined_lang'	=> "['cz','en','de']",
		),    
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení projektu'=>'#'));
        
        $this->set('naborari_list',$this->get_list('CmsUser',array('cms_group_id'=>array(8,40,41,43,44,42,46,9),'kos'=>0,'status'=>1)));
        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

    function load_region_profesia($parent_id = null,$return = false){
        $this->loadModel('ProfesiaCodeBook');
        $pro = $this->ProfesiaCodeBook->find('all',
                      array('fields'=>array('original_id','name'),
                            'conditions'=>array('ProfesiaCodeBook.type'=>'regions','ProfesiaCodeBook.parent_id'=>$parent_id)));
        foreach($pro as  $item){
            $countries[$item["ProfesiaCodeBook"]['original_id']]= $item ["ProfesiaCodeBook"]['name'];
        }
        if(empty($countries))
            $countries = array(-1 => 'Nezadáno');

        if($return !== false)
            return $countries;
        else
            die(json_encode($countries));
    }
    function load_skill_profesia($parent_id = null,$return = false){
        $this->loadModel('ProfesiaCodeBook');
        $pro = $this->ProfesiaCodeBook->find('all',
            array('fields'=>array('original_id','name'),
                'conditions'=>array('ProfesiaCodeBook.type'=>'skills','ProfesiaCodeBook.parent_id'=>$parent_id)));
        foreach($pro as  $item){
            $skills[$item["ProfesiaCodeBook"]['original_id']]= $item ["ProfesiaCodeBook"]['name'];
        }
        if(empty( $skills))
            $skills = array(-1 => 'Nezadáno');

        if($return !== false)
            return  $skills;
        else
            die(json_encode( $skills));
    }

    function load_position_profesia($parent_id = null,$return = false){
        $this->loadModel('ProfesiaCategoryPosition');
        $pro = $this->ProfesiaCategoryPosition->find('all',
            array('fields'=>array('position_id','name'),
                'conditions'=>array('ProfesiaCategoryPosition.category_id'=>$parent_id)));
        foreach($pro as  $item){
            $position[$item["ProfesiaCategoryPosition"]['position_id']]= $item ["ProfesiaCategoryPosition"]['name'];
        }
        if(empty( $position))
            $position = array(-1 => 'Nezadáno');

        if($return !== false)
            return  $position;
        else
            die(json_encode( $position));
    }

	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->WwwInsertion->read(null,$id);

                if($id){
                    $this->loadModel('ProfesiaWwwInsertion');
                    $ins = $this->ProfesiaWwwInsertion->find('all',array('conditions'=>array('ProfesiaWwwInsertion.www_insertion_id'=>$id),'fields'=>array('id','atribut','value')));
                    foreach($ins as $item){
                        $parseAtr = explode('|' ,$item['ProfesiaWwwInsertion']['atribut']);
                        if(Count($parseAtr) >1 ){
                            if($parseAtr[0] > 3){
                                $insertion[$parseAtr[1]][$parseAtr[0]]['value'] = $item['ProfesiaWwwInsertion']['value'];
                                $insertion[$parseAtr[1]][$parseAtr[0]]['id'] = $item['ProfesiaWwwInsertion']['id'];
                            }
                        }
                        $insertion[$item['ProfesiaWwwInsertion']['atribut']]['value'] = $item['ProfesiaWwwInsertion']['value'];
                        $insertion[$item['ProfesiaWwwInsertion']['atribut']]['id'] = $item['ProfesiaWwwInsertion']['id'];
                        $this->set('insertion',$insertion);
                    }

                }

               /// load province list
				$this->loadModel('Province'); 
				$this->set('province_list',$this->Province->find('list',array('conditions'=>array('stat_id'=>$this->data['WwwInsertion']['stat_id']))));
		        unset($this->Province);

            }
            else{
                $this->loadModel('Province');
				$this->set('province_list',$this->Province->find('list',array('conditions'=>array('stat_id'=>1))));	
            }
            // load stat list
			$this->loadModel('SettingStat');
			$this->set('company_stat_list',$this->SettingStat->find('list',array('conditions'=>array('SettingStat.status'=>1,'SettingStat.kos'=>0))));
			unset($this->SettingStat);

			// zamereni list
			$this->loadModel('SettingCareerItem');
			$this->set('setting_career_item_list',$this->SettingCareerItem->find('list',array('conditions'=>array('SettingCareerItem.kos'=>0))));
			unset($this->SettingStat);

            $this->loadModel('ProfesiaCodeBook');
            $this->loadModel('ProfesiaContractType');
            $this->loadModel('ProfesiaCategoryPosition');
            $this->loadModel('ProfesiaWwwInsertion');
            $this->loadModel('CmsUser');

            $sta = $this->ProfesiaCodeBook->find('all',array('fields'=>array('original_id','name'),'conditions'=>array('ProfesiaCodeBook.type'=>'regions','ProfesiaCodeBook.parent_id'=>0)));
            foreach($sta as  $item){
                $stats[$item["ProfesiaCodeBook"]['original_id']]= $item ["ProfesiaCodeBook"]['name'];
            }
            $this->set('stat_list_profesia',$stats);

            $this->set('statOptions', $this->makeOptions($stats));

            $jbT=$this->ProfesiaContractType->find('list');
            $this->set('jobtype_list_profesia',$jbT);
            $this->set('jobTypeOptions', $this->makeOptions($jbT));

            $this->set('drive_licence',array('Ne'=>'Ne','A'=>'A','B'=>'B','C'=>'C','D'=>'D','T'=>'T'));
            $this->set('sendcv_list_profesia',array(0=>'Posílat všechny',1=>'Posílat pouze nové',2=>'Neposílat'));
            $cb = $this->ProfesiaCodeBook->find('all',array('fields'=>array('original_id','name','type')));
            foreach($cb as  $item){
                $code_book[$item["ProfesiaCodeBook"]['type']][$item["ProfesiaCodeBook"]['original_id']]= $item ["ProfesiaCodeBook"]['name'];
            }
            $this->set('categoryOptions', $this->makeOptions($code_book['categories']));
            $this->set('bussinesAreaOptions', $this->makeOptions($code_book['businessareas']));
            $this->set('code_book_profesia',$code_book);
            $this->set('contact_list_profesia', $this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0))));

            $this->set('insertion_id',$id);
			$this->render('edit');
		} else {


                $this->data['WwwInsertion']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
                $this->WwwInsertion->save($this->data);

                $this->loadModel('ProfesiaWwwInsertion');
                if(Count($this->data['ProfesiaWwwInsertion']) > 0){
                    foreach($this->data['ProfesiaWwwInsertion'] as $atribut=>$value){
                        $insertion = array();
                        if($value != ''){
                            if(is_array($value)){
                                foreach($value as $atr=>$val){
                                    $insertion = array();
                                    if($val != '' ){
                                        if(!empty($this->data['Ids'][$atribut.'|'.$atr])){
                                            $insertion['ProfesiaWwwInsertion']['id'] = $this->data['Ids'][$atribut.'|'.$atr];
                                        }
                                        $insertion['ProfesiaWwwInsertion']['value'] = $val;
                                       $insertion['ProfesiaWwwInsertion']['atribut'] = $atribut.'|'.$atr;

                                        $insertion['ProfesiaWwwInsertion']['www_insertion_id'] =  $this->data['WwwInsertion']['id'];
                                        $this->ProfesiaWwwInsertion->save($insertion);
                                        $this->ProfesiaWwwInsertion->id = null;
                                    }else{
                                        if(!empty($this->data['Ids'][$atribut.'|'.$atr])){
                                            $insertion['ProfesiaWwwInsertion']['id'] = $this->data['Ids'][$atribut.'|'.$atr];
                                            $insertion['ProfesiaWwwInsertion']['value'] = '';
                                            $insertion['ProfesiaWwwInsertion']['atribut'] = $atribut.'|'.$atr;
                                            $insertion['ProfesiaWwwInsertion']['www_insertion_id'] =   $this->data['WwwInsertion']['id'];
                                            $this->ProfesiaWwwInsertion->save($insertion);
                                            $this->ProfesiaWwwInsertion->id = null;
                                        }
                                    }
                                }
                            }else{
                                if(!empty($this->data['Ids'][$atribut])){
                                    $insertion['ProfesiaWwwInsertion']['id'] = $this->data['Ids'][$atribut];
                                }
                                $insertion['ProfesiaWwwInsertion']['value'] = $value;
                                $insertion['ProfesiaWwwInsertion']['atribut'] = $atribut;
                                $insertion['ProfesiaWwwInsertion']['www_insertion_id'] =  $this->data['WwwInsertion']['id'];
                                $this->ProfesiaWwwInsertion->save($insertion);
                                $this->ProfesiaWwwInsertion->id = null;
                            }
                        }else{
                            if(!empty($this->data['Ids'][$atribut])){
                                $insertion['ProfesiaWwwInsertion']['id'] = $this->data['Ids'][$atribut];
                                $insertion['ProfesiaWwwInsertion']['value'] = '';
                                $insertion['ProfesiaWwwInsertion']['atribut'] = $atribut;
                                $insertion['ProfesiaWwwInsertion']['www_insertion_id'] = $this->data['WwwInsertion']['id'];
                                $this->ProfesiaWwwInsertion->save($insertion);
                                $this->ProfesiaWwwInsertion->id = null;
                            }
                        }
                    }
                }
                if(!empty($this->data['DeleteExtends'])){
                    $ids = explode('|',$this->data['DeleteExtends']);
                    foreach($ids as $id){
                        if(intval($id) > 0){
                            $this->ProfesiaWwwInsertion->delete(intval($id));
                        }
                    }
                }
			die(json_encode(array('result'=>true)));
		}
	}
    function makeOptions($data){
        $opt='<option></option>';
        foreach($data as $id=>$name){
            $opt .= '<option value="'.$id.'">'.$name.'</option>';
        }
       return $opt;
    }
    function profesia_export($id = ''){
            $insertions = array();
            $cond = array('kos'=>0,'status'=>1);
            if($id){
                $cond['WwwInsertion.id'] = $id ;
            }
            $ins_all = $this->WwwInsertion->find('all',array('conditions'=>$cond));


            foreach($ins_all as $insertion){

                $this->loadModel('ProfesiaWwwInsertion');
                $multiple = $this->ProfesiaWwwInsertion->find('all',array('fields'=>array('atribut','value'),'condition'=>array('ProfesiaWwwInsertion.www_insertion_id'=>$insertion['WwwInsertion']['id'])));

                $multi = array();
                foreach($multiple as $mul){
                    $tmp = explode('|',$mul['ProfesiaWwwInsertion']['atribut']);
                    if(Count($tmp) > 1){
                         if($tmp[1] == 'profesia_province_id' || $tmp[1] == 'profesia_country_id' || $tmp[1] == 'profesia_stat_id' ){
                            if(isset($multi['region_max'][$tmp[0]])){
                                if($tmp[1] == 'profesia_province_id' && !empty($mul['ProfesiaWwwInsertion']['value'])){
                                    $multi['region_max'][$tmp[0]] = $tmp[1];
                                    $multi['region_id'][$tmp[0]] = $mul['ProfesiaWwwInsertion']['value'];
                                }else if($tmp[1] == 'profesia_country_id' && $multi['region_max'][$tmp[0]] != 'profesia_province_id'  && !empty($mul['ProfesiaWwwInsertion']['value'])){
                                    $multi['region_max'][$tmp[0]] = $tmp[1];
                                    $multi['region_id'][$tmp[0]] = $mul['ProfesiaWwwInsertion']['value'];
                                }else if($tmp[1] == 'stat_id' && $multi['region_max'][$tmp[0]] != 'profesia_province_id' && $multi['region_max'][$tmp[0]] != 'profesia_country_id'){
                                    $multi['region_max'][$tmp[0]] = $tmp[1];
                                    $multi['region_id'][$tmp[0]] = $mul['ProfesiaWwwInsertion']['value'];
                                }
                            }else{
                                $multi['region_max'][$tmp[0]] = $tmp[1];
                                $multi['region_id'][$tmp[0]] = $mul['ProfesiaWwwInsertion']['value'];
                            }
                        }else{
                            $multi[$tmp[1]][$tmp[0]] = $mul['ProfesiaWwwInsertion']['value'];
                        }
                    }
                }
                $ins['multi'] = $multi;

                if(isset($insertion['WwwInsertion']['cms_user_id']) && !empty($insertion['WwwInsertion']['cms_user_id'])){
                    $this->loadModel('CmsUser');
                    $contact = $this->CmsUser->read(null , $insertion['WwwInsertion']['cms_user_id']);
                }
                $insertion['contact'] = $contact['CmsUser'];

                $ins['insertion'] = $insertion;
                $insertions[] = $ins;
            }
            $this->autoLayout = false;
            $this->set('insertions',$insertions);
            header ("Content-Type:text/xml");
            echo '<?xml version="1.0" encoding="UTF-8"?>
                    <export>';

            echo $this->render('/www_insertions/profesia_export');
            echo '</export>';
            die();
        //exit;

    }

    function trasnfer_order_to_insertion(){
        die('vypnuto, hrozi duplikace');
        $this->loadModel('CompanyOrderItem');
        $ordes = $this->CompanyOrderItem->find('all',array(
            'conditions'=>array(
                'CompanyOrderItem.www_show'=>1,
                'CompanyOrderItem.kos'=>0
            )
        ));
        
        foreach($ordes as $order){
            $save_arr = array(
                'company_order_item_id'=>$order['CompanyOrderItem']['id'],
                'name'=>$order['CompanyOrderItem']['www_name'],
                'city'=>$order['CompanyOrderItem']['start_place'],
                'napln'=>$order['CompanyOrderItem']['www_napln'],
                'pozadavky'=>$order['CompanyOrderItem']['www_pozadujeme'],
                'plat'=>$order['CompanyOrderItem']['www_mzda'],
                'dalsi_informace'=>$order['CompanyOrderItem']['www_dalsi_informace'],
                'setting_career_item_id'=>$order['CompanyOrderItem']['setting_career_item_id'],
                'created'=>$order['CompanyOrderItem']['created'],
                'updated'=>$order['CompanyOrderItem']['updated'],
                'cms_user_id'=>$order['CompanyOrderItem']['www_cms_user_id'],
            );
            
            $this->WwwInsertion->id = null;
            $this->WwwInsertion->save($save_arr);
        }
        die('done');
    }
}
?>