<?php
Configure::write('debug', 2);
$_GET['current_year'] = (isset($_GET['current_year']) && !empty($_GET['current_year'])) ? $_GET['current_year'] : date('Y');
$_GET['current_month'] = (isset($_GET['current_month']) && !empty($_GET['current_month'])) ? $_GET['current_month'] : (int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']);
@define('CURRENT_MONTH', ($_GET['current_month'] < 10) ? '0' . $_GET['current_month'] : $_GET['current_month']);

/*
$pocet_dnu = date_dif(mydate(0,$item['ConnectionAuditEstate']['created'],$year.'-'.$month), mydate(1,$item['ConnectionAuditEstate']['to_date'],$year.'-'.$month));
$pocet_dnu_v_danem_mesici = date('t',strtotime("$year-$month-01"));
$price = $item['AuditEstate']['hire_price'] / $pocet_dnu_v_danem_mesici * $pocet_dnu;
*/
define('YM', CURRENT_YEAR . "-" . CURRENT_MONTH);

class MonthlyClientStatementsController extends AppController
{
    var $name = 'MonthlyClientStatements';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex');
    var $components = array('ViewIndex', 'RequestHandler');
    var $uses = array('ClientBuy');
    var $renderSetting = array(
        'bindModel' => array(
            'joinSpec' => array(
                'Client' => array(
                    'primaryKey' => 'Client.id',
                    'foreignKey' => 'ClientBuy.client_id'
                ),
                'OdberatelCmsUser' => array(
                    'className' => 'CmsUser',
                    'primaryKey' => 'OdberatelCmsUser.id',
                    'foreignKey' => 'ClientBuy.to_cms_user_id'
                ),
                'Odberatel' => array(
                    'className' => 'ConnectionClientAtCompanyWorkPosition',
                    'primaryKey' => 'OdberatelCmsUser.at_employee_id',
                    'foreignKey' => 'Odberatel.client_id',
                    'conditions'=> array('Odberatel.datum_to'=>'0000-00-00' )
                ),

                'PoskytovatelCmsUser' => array(
                    'className' => 'CmsUser',
                    'primaryKey' => 'PoskytovatelCmsUser.id',
                    'foreignKey' => 'ClientBuy.from_cms_user_id'
                ),
                'Poskytovatel' => array(
                    'className' => 'ConnectionClientAtCompanyWorkPosition',
                    'primaryKey' => 'Poskytovatel.client_id',
                    'foreignKey' => 'PoskytovatelCmsUser.at_employee_id',
                    'conditions'=> array('Poskytovatel.datum_to'=>'0000-00-00')
                ),
            ),
        ),
        'SQLfields' => array(
            'ClientBuy.*, Poskytovatel.*, Odberatel.*','Client.name',
            'SUM(IF( ISNULL(ClientBuy.price) , (SELECT price_2 FROM wapis__setting_career_items as SCI WHERE SCI.id = ClientBuy.setting_career_item_id ) , ClientBuy.price )) as price_sum',
            'if(ClientBuy.pay_date != "","Fakturováno","Nefakturováno") as stav',
        ),
        'SQLcondition' => array(
            "((DATE_FORMAT(ClientBuy.from_date,'%Y-%m')= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
            //'((DATE_FORMAT(ClientBuy.to_date,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#")',

            'Odberatel.id IS NOT NULL',
            'Poskytovatel.id IS NOT NULL',
            'Poskytovatel.at_project_id != Odberatel.at_project_id'
            /*'Client.cms_user_id !=' => -2*/
        ),
        'controller' => 'monthly_client_statements',
        'page_caption' => 'Měsíční fakturace za klienty',
        'sortBy' => 'Poskytovatel.at_project_id.ASC',
        'group_by' => 'Odberatel.at_project_id, Poskytovatel.at_project_id, Poskytovatel.at_project_centre_id',
        'top_action' => array(
            //'add_item'		=>	'Přidat|edit|Pridat majetek|add',
        ),
        'no_trash'=> false,
        'filtration' => array(
            //'CompanyPerson-name'				=>	'text|Jméno|',
            'GET-current_month' => 'select|Měsíc|mesice_list',
            'GET-current_year' => 'select|Rok|actual_years_list',

        ),
        'items' => array(
            'id' => 'ID|ClientBuy|id|hidden|',
            'at_project_id_poskyt' => 'Firma Poskytovatel|Poskytovatel|at_project_id|viewVars|project_list',
            'at_project_centre_id_user' => 'Středisko Poskytovatel|Poskytovatel|at_project_centre_id|viewVars|center_list',
            'at_project_id_odber' => 'Firma Odberatel|Odberatel|at_project_id|viewVars|project_list',
            'sum_hire_price' => 'Pronájem|0|price_sum|text|',
            'stav' => 'Stav|0|stav|text|'
        ),
        'posibility' => array(
            'show' => 'show|Detail položky|show',
            'fakturovano'		=>	'fakturovano|Fakturováno|fakturovano',
            //   'fakturovano_client'		=>	'fakturovano_client|Fakturováno|fakturovano_client',
        ),
        'posibility_link' => array(
            'show' => '/Poskytovatel.at_project_id/Odberatel.at_project_id/Poskytovatel.at_project_centre_id/#CURRENT_YEAR#/#CURRENT_MONTH#',
            'fakturovano' => '/Poskytovatel.at_project_id/Odberatel.at_project_id/Poskytovatel.at_project_centre_id/#CURRENT_YEAR#/#CURRENT_MONTH#/',
            // 'fakturovano_client' => '/Poskytovatel.at_project_id/Odberatel.at_project_id/Poskytovatel.at_project_centre_id/#CURRENT_YEAR#/#CURRENT_MONTH#/'
        ),

        'domwin_setting' => array(
            'sizes' => '[1000,1000]',
            'scrollbars' => true,
            'languages' => true,
        )
    );

    function index()
    {

        $this->set('fastlinks', array('ATEP' => '/', 'Evidence majetku' => '#', $this->renderSetting['page_caption'] => '#'));

        $this->set('company_list', $this->get_list('AtCompany'));
        $this->set('project_list', $this->get_list('AtProject'));
        $this->set('center_list', $this->get_list('AtProjectCentre'));
        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }


    /**
     * detail majetku za fakturovanou castku
     */
    function show($poskyt_projekt_id = null, $odber_projekt_id = null, $at_project_centre_id = null, $year = null, $month = null, $print = false, $order_by = false, $filtr_spolecnost = false, $filtr_stredisko = false, $filtr_cinnost = false, $fakturace = false)
    {
        //echo $poskyt_projekt_id . ' ' . $odber_projekt_id . ' ' . $at_project_centre_id;

        $this->set('project_list', $this->get_list('AtProject'));
        $this->set('center_list', $this->get_list('AtProjectCentre'));
        $info['Poskytovatel'] = $poskyt_projekt_id;
        $info['Odberatel'] = $odber_projekt_id;
        $info['Centre'] = $at_project_centre_id;


        $order_list = array(
            'Client.name ASC'=>'Pracovník - vzestupně',
            'Client.name DESC'=>'Pracovník - sestupně',
            'SettingCareerItem.price_2 ASC'=>'Cena - vzestupně',
            'SettingCareerItem.price_2 DESC'=>'Cena - sestupně',
            'SettingCareerItem.name ASC'=>'Pozice - vzestupně',
            'SettingCareerItem.name DESC'=>'Pozice - sestupně',
            'ClientBuy.from_date ASC'=>'Pronájem od - vzestupně',
            'ClientBuy.from_date DESC'=>'Pronájem od - sestupně',
            'ClientBuy.to_date ASC'=>'Pronájem do - vzestupně',
            'ClientBuy.to_date DESC'=>'Pronájem do - sestupně',
        );
        $this->set('order_list',$order_list);

        $this->loadModel('AtProjectCentre');
        $stredisko_list = $this->AtProjectCentre->find('list',array('id','name'));
        $this->set('stredisko_list',$stredisko_list);
        $filtr = '';
        if($filtr_stredisko != false){
            $filtr = ' Odberatel.at_project_centre_id ='.$filtr_stredisko;
        }

        if ($year > date('Y') || ($year == date('Y') && $month > date('m')))
            die('Toto datum je mimo aktuální, mužete pouze do minulosti nebo v stávájicím měsicí.');

        if ($poskyt_projekt_id != null && $odber_projekt_id != null) {
            if ($print == true) {
                $this->layout = 'print';
            }

            $this->loadModel('ClientBuy');
            $this->ClientBuy->bindModel( array(
                'joinSpec' => array(
                    'Client' => array(
                        'primaryKey' => 'Client.id',
                        'foreignKey' => 'ClientBuy.client_id'
                    ),
                    'OdberatelCmsUser' => array(
                        'className' => 'CmsUser',
                        'primaryKey' => 'OdberatelCmsUser.id',
                        'foreignKey' => 'ClientBuy.to_cms_user_id'
                    ),
                    'Odberatel' => array(
                        'className' => 'ConnectionClientAtCompanyWorkPosition',
                        'primaryKey' => 'OdberatelCmsUser.at_employee_id',
                        'foreignKey' => 'Odberatel.client_id',
                        'conditions'=> array('Odberatel.datum_to'=>'0000-00-00')
                    ),

                    'PoskytovatelCmsUser' => array(
                        'className' => 'CmsUser',
                        'primaryKey' => 'PoskytovatelCmsUser.id',
                        'foreignKey' => 'ClientBuy.from_cms_user_id'
                    ),
                    'Poskytovatel' => array(
                        'className' => 'ConnectionClientAtCompanyWorkPosition',
                        'primaryKey' => 'Poskytovatel.client_id',
                        'foreignKey' => 'PoskytovatelCmsUser.at_employee_id',
                        'conditions'=> array('Poskytovatel.datum_to'=>'0000-00-00')
                    ),
                    'SettingCareerItem' => array(
                        'primaryKey' => 'SettingCareerItem.id',
                        'foreignKey' => 'ClientBuy.setting_career_item_id'
                    ),
                ),
            ));
            $orders = $this->ClientBuy->find('all', array(
                    'fields'=>array('ClientBuy.*, Poskytovatel.*, Odberatel.*','Client.name','SettingCareerItem.name', 'SettingCareerItem.price_2','OdberatelCmsUser.name'),
                    'conditions'=>array(
                        "((DATE_FORMAT(ClientBuy.from_date,'%Y-%m') = '".$year."-".$month."'))",
                        'Odberatel.at_project_id'=>$odber_projekt_id,
                        'Poskytovatel.at_project_id' =>$poskyt_projekt_id,
                        'Poskytovatel.at_project_centre_id'=>$at_project_centre_id,
                        'Poskytovatel.at_project_id != Odberatel.at_project_id',
                        $filtr
                    ),
                    'order'=>($order_by == false?null:$order_by),
                    //'group'=>'ClientBuy.id'
                )
            );


            $this->set('print',$print);
            $this->set('orders',$orders);
            $this->set('info', $info);
            $this->set('year', $year);
            $this->set('month', $month);
            $this->set('stredisko', $at_project_centre_id);
            $this->set('refresh_url','/monthly_client_statements/show/'.$poskyt_projekt_id.'/'.$odber_projekt_id.'/'.$at_project_centre_id.'/'.$year.'/'.$month);

            if ($print == false && ($order_by != false || $filtr_spolecnost != false || $filtr_stredisko != false || $filtr_cinnost != false)) {
                $this->render('items');
            }
            else {
                $this->render('show');
            }


        }
        else
            die('Chyba: Bez id?');

    }


    function fakturovano( $poskyt_projekt_id = null, $odber_projekt_id = null, $at_project_centre_id = null, $year = null, $month = null )
    {
        if ($year > date('Y') || ($year == date('Y') && $month > date('m')))
            die('Toto datum je mimo aktuální, mužete pouze do minulosti nebo v stávájicím měsicí.');

        if ($poskyt_projekt_id != null && $odber_projekt_id != null) {

            $this->loadModel('ClientBuy');
            $this->ClientBuy->bindModel( array(
                'joinSpec' => array(
                    'Client' => array(
                        'primaryKey' => 'Client.id',
                        'foreignKey' => 'ClientBuy.client_id'
                    ),
                    'OdberatelCmsUser' => array(
                        'className' => 'CmsUser',
                        'primaryKey' => 'OdberatelCmsUser.id',
                        'foreignKey' => 'ClientBuy.to_cms_user_id'
                    ),
                    'Odberatel' => array(
                        'className' => 'ConnectionClientAtCompanyWorkPosition',
                        'primaryKey' => 'OdberatelCmsUser.at_employee_id',
                        'foreignKey' => 'Odberatel.client_id',
                        'conditions'=> array('Odberatel.datum_to'=>'0000-00-00')
                    ),

                    'PoskytovatelCmsUser' => array(
                        'className' => 'CmsUser',
                        'primaryKey' => 'PoskytovatelCmsUser.id',
                        'foreignKey' => 'ClientBuy.from_cms_user_id'
                    ),
                    'Poskytovatel' => array(
                        'className' => 'ConnectionClientAtCompanyWorkPosition',
                        'primaryKey' => 'Poskytovatel.client_id',
                        'foreignKey' => 'PoskytovatelCmsUser.at_employee_id',
                        'conditions'=> array('Poskytovatel.datum_to'=>'0000-00-00')
                    ),
                    'SettingCareerItem' => array(
                        'primaryKey' => 'SettingCareerItem.id',
                        'foreignKey' => 'ClientBuy.setting_career_item_id'
                    ),
                ),
            ));
            $orders = $this->ClientBuy->find('all', array(
                    'fields'=>array('ClientBuy.*, Poskytovatel.*, Odberatel.*','Client.name','SettingCareerItem.name', 'SettingCareerItem.price_2','OdberatelCmsUser.name'),
                    'conditions'=>array(
                        "((DATE_FORMAT(ClientBuy.from_date,'%Y-%m') = '".$year."-".$month."'))",
                        'Odberatel.at_project_id'=>$odber_projekt_id,
                        'Poskytovatel.at_project_id' =>$poskyt_projekt_id,
                        'Poskytovatel.at_project_centre_id'=>$at_project_centre_id,
                        //'ClientBuy.status'=>1
                    ),
                    'order'=>'Poskytovatel.id ASC',
                )
            );

            foreach($orders as $clientBuy){
                $this->loadModel('ClientBuy');
                $old_owner = $this->ClientBuy->find('first',array('conditions'=>array('client_id'=>$clientBuy['ClientBuy']['client_id'],'status'=>1,'pay_date !='=>null, 'to_date'=>'0000-00-00')));
                $data = array(
                    'ClientBuy'=>array(
                        'id' => $clientBuy['ClientBuy']['id'],
                        'pay_date' => date('Y-m-d'),
                        'price' => $clientBuy['SettingCareerItem']['price_2']
                    )
                );
                $ret = $this->ClientBuy->save($data);
                if($ret){
                    $data = array(
                        'ClientBuy'=>array(
                            'id' => $old_owner['ClientBuy']['id'],
                            'to_date'=>date('Y-m-d')
                        )
                    );
                    $this->ClientBuy->id= null;
                    $this->ClientBuy->save($data);
                }
            }
            die(json_encode(array('result' => true)));

        }


    }
}

?>