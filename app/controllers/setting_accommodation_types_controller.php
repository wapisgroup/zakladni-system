<?php
class SettingAccommodationTypesController extends AppController {
	var $name = 'SettingAccommodationTypes';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingAccommodationType');
	var $renderSetting = array(
		'controller'=>'setting_accommodation_types',
		'SQLfields' => array('id','name','updated','created','status','price_type'),
		'page_caption'=>'Nastavení typů ubytování',
		'sortBy'=>'SettingAccommodationType.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingAccommodationType|id|text|',
			'name'		=>	'Název|SettingAccommodationType|name|text|',
			'cena'		=>	'Cena|SettingAccommodationType|price_type|viewVars|price_for_list',
			'updated'	=>	'Upraveno|SettingAccommodationType|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingAccommodationType|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení typů ubytování'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingAccommodationType->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingAccommodationType->save($this->data);
			die();
		}
	}
}
?>