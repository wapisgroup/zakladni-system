<?php
//Configure::write('debug',2);
class OrdersCastPostsController extends AppController {
	var $name = 'OrdersCastPosts';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CompanyOrderItem');
	var $renderSetting = array(
		//'SQLfields' => array('id','Company.name','updated','created','status','SalesManager.name','ClientManager.name','Coordinator.name','mesto','SettingStav.namre'),
		'SQLfields' => '*,
            CONCAT_WS(" + ",CompanyOrderItem.count_of_free_position,CompanyOrderItem.count_of_substitute) as objednavka,
            CONCAT_WS(" / ",CompanyOrderItem.count_of_free_position,
                     (SELECT 
                        COUNT(c.id) 
                     FROM wapis__connection_client_requirements as c
                     Where 
                        c.company_id = CompanyOrderItem.company_id AND 
                        c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                        type = 1 AND
                        objednavka = 1
                     ),
                    (SELECT 
                        COUNT(c.id) 
                     FROM wapis__connection_client_requirements as c
                     Where 
                        c.company_id = CompanyOrderItem.company_id AND 
                        c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                        type = 2 AND objednavka = 1 AND c.`to` = "0000-00-00"
                     )
            ) as statistika,
            IF((CompanyOrderItem.start_datetime < NOW() && CompanyOrderItem.order_status IN (-1,1)), 1 , 0) as expire                
        ',
        'SQLcondition'=>array(
            'order_status'=>array(-1,1)
        ),		
		'bindModel' => array('belongsTo' => array(
            'Company'=>array('foreignKey'=>'company_id')
		)),
		'controller'=> 'orders_cast_posts',
		'page_caption'=>'Obsazeni objednávek',
		'sortBy'=>'CompanyOrderItem.order_date.DESC',
		'top_action' => array(),
		'filtration' => array(
            'CompanyOrderItem-company_id'	=>	'select|Společnost|company_list',      
        ),
		'items' => array(
			'id'				=>	'ID|CompanyOrderItem|id|text|',
			'firma'				=>	'Firma|Company|name|text|',
			'order'				=>	'Profese|CompanyOrderItem|name|text|',	
			'order_date'		=>	'Datum objednávky|CompanyOrderItem|order_date|date|',
			'start_datetime'	=>	'Datum nástupu|CompanyOrderItem|start_datetime|date|',
			'objednavka'		=>	'Objednávka|0|objednavka|text|',
			'statistika'		=>	'Statistika|0|statistika|text|',
			'start_comment'		=>	'Komentář|CompanyOrderItem|start_comment|text|orez#30',
            'status'			=>	'Status|CompanyOrderItem|order_status|var|order_stav_list',
            'created'			=>	'Vytvořeno|CompanyOrderItem|created|datetime|',
            'nabor'			    =>	'Nábor|CompanyOrderItem|nabor|text|value_to_yes_no#long'
		),
        'class_tr'=>array(
            'class'=>'color_red',
            'model'=>0,
            'col'=>'expire',
            'value'=>1
        ),
		'posibility' => array(
			'pozice'		=>	'nabor_pozice|Obsazení pracovních pozic|pozice',
			//'show'		    =>	'show|Detail šablony|show'
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'		=> true,
			'languages'	=> 'false'
		)
	);
    var $orderByCekaciListina = array(
        'Client.os_pohovor ASC'=>'Pohovor sestupně',
        'Client.os_pohovor DESC'=>'Pohovor vzsetupně',
        'Countrie.name ASC'=>'Okres sestupně',
        'Countrie.name DESC'=>'Okres vzsetupně',
    );
    
    function set_path($path){
        $this->viewPath = $path;
    }
    
    function index(){
       	if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
            $cntrl_perm = $this->logged_user['CmsGroup']['permission']['orders_cast_posts'];
            $criteria = array();
            if(isset($cntrl_perm['group_id']) && ($cntrl_perm['group_id'] == 'coordinator_double' || $cntrl_perm['group_id'] == 'client_manager_id')){
                $criteria['or'] = array(
                   'Company.coordinator_id' => $this->logged_user['CmsUser']['id'],
                   'Company.coordinator_id2' => $this->logged_user['CmsUser']['id'],
                   'Company.client_manager_id' => $this->logged_user['CmsUser']['id']
                );
				}
            else if(isset($cntrl_perm['group_id']) && $cntrl_perm['group_id'] == 'coordinator_new'){
                    $criteria[] = 'Company.id IN (SELECT company_id FROM wapis__company_coordinators WHERE cms_user_id = '.$this->logged_user['CmsUser']['id'].' AND kos = 0)';
                }    
		    $this->set('company_list',$this->get_list('Company',$criteria));
          
			// set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Nábor'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
		}
	}
    
	/**
	 * Zobrazeni obsazovani pracovnich pozic
	 * @param integer $order_item_id
	 * @return 
	 */
    public function nabor_pozice($order_item_id, $render = 'nabor'){
        /**
         * sets 
         */       
         $this->CompanyOrderItem->bindModel(array(
            'belongsTo'=>array(
                'CompanyOrderTemplate'=>array('foreignKey'=>'order_template_id'),
                'Company'=>array('fields'=>array('name'))
            )
         ));
        $this->set('detail',$this->data = $this->CompanyOrderItem->read(null,$order_item_id));
        $this->set('group_id',$this->logged_user['CmsGroup']['id']);
        $this->set('superior_id',$s = $this->logged_user['CmsGroup']['cms_group_superior_id']);
		$this->set('umistovatel',$this->logged_user['CmsUser']['name']);
		$this->set('order_template_id',$order_item_id);
        $this->set('orderByCekaciListina',$this->orderByCekaciListina);

        /**
         * START DETAIL SABLONY
         */
        
        $this->set('show',true);
        $this->set('none',false);
        
            /*
			 * natahnuti informaci o firme
			 * nazev spolecnosti, mesto, kontakt na Cm a Koo1, Koo2
			 */ 
            $this->loadModel('Company');  
			$this->Company->bindModel(array(
                'joinSpec'=>array(
                    'ClientManager' => array('className'	=>'CmsUser','primaryKey'=>'ClientManager.id','foreignKey'	=> 'client_manager_id'),
    				'Koordinator1' => array('className'	=>'CmsUser','primaryKey'=>'Koordinator1.id','foreignKey'	=> 'coordinator_id'),
    				'Koordinator2' => array('className'	=>'CmsUser','primaryKey'=>'Koordinator2.id','foreignKey'	=> 'coordinator_id'),
    	            'Koo1' => array('className'	=>'Client','primaryKey'=>'Koo1.id','foreignKey'	=> 'ClientManager.at_employee_id'),
    				'Koo2' => array('className'	=>'Client','primaryKey'=>'Koo2.id','foreignKey'	=> 'Koordinator1.at_employee_id'),
    				'Koo3' => array('className'	=>'Client','primaryKey'=>'Koo3.id','foreignKey'	=> 'Koordinator2.at_employee_id'),
    	   
                 )
            ));
			$this->set('company_info', $company_info = $this->Company->find('first',array(
                'fields'=>array('Company.name','Company.mesto', 'ClientManager.*', 
                        'ClientManager.email', 'Koo1.mobil2','Koordinator1.name',
                         'Koordinator1.email', 'Koo2.mobil2', 'Koordinator2.name', 
                         'Koordinator2.email', 'Koo3.mobil2'
                 ),
                 'conditions'=>array('Company.id'=>$this->data['CompanyOrderItem']['company_id'])
            )));
			$this->set('company_id',$this->data['CompanyOrderItem']['company_id']);
			
            
                 
			/*
			 * Nacteni seznamu profesi ze settingu
			 */
			$this->loadModel('SettingCareerItem'); 
			$this->set('setting_career_list', $this->SettingCareerItem->find('list',array('order'=>'name ASC')));
			unset($this->SettingCareerItem);
						
			/*
			 * Nacteni pracovnich pozic nadefinovanych v kalkulacich pro danou spolecnost
			 * serazene dle nazvu profese, parent_id - ty co jsou hlavni, ne varianty, test - a ty, ktere nejsou testovaci
			 */
			$this->loadModel('CompanyWorkPosition'); 
			$this->set('company_position_list', $this->CompanyWorkPosition->find('list',array('order'=>'name ASC','conditions'=>array('company_id'=>$this->data['CompanyOrderItem']['company_id'], 'kos'=>0 , 'test'=>0))));
			unset($this->CompanyWorkPosition);	
            
            if($this->data['CompanyOrderItem']['company_work_position_id'] == '')//position_id ze se spatne ulozilo, nacteme ze sablony
                $this->data['CompanyOrderItem']['company_work_position_id'] = $this->data['CompanyOrderTemplate']['company_work_position_id'];
            self::load_kalkulace($this->data['CompanyOrderItem']['company_work_position_id'], false);
		/**
		 * END DETAIL SABLONY
		 */
        
        $this->loadModel('ConnectionClientRequirement');
		/*
		 * Natahnuti klientu, kteri jsou na cekaci listine
		 */
        $this->ConnectionClientRequirement->bindModel(array(
            'belongsTo'=>array('Client','CmsUser'),
            'joinSpec'=>array(
                'Countrie'=>array(
                    'className'=>'Countrie',
                    'primaryKey'=>'Countrie.id',
                    'foreignKey'=>'Client.countries_id'
                )
            )
        ));
		$cekaci_listina_list = $this->ConnectionClientRequirement->find('all', 
			array(
				'conditions' => array(
					'requirements_for_recruitment_id' => $order_item_id, 
					'objednavka' =>1,
					'type'=>1
				),
				'fields'=>array(
					'CmsUser.name',
					'Client.name',
                    'Client.mesto',
                    'Client.os_pohovor',
					'Client.mobil1',
					'Client.mobil2',
					'Client.mobil3',
					'Client.mobil_active',
                    'Client.opp_nohavice','Client.opp_bluza','Client.opp_tricko','Client.opp_obuv',
					'Client.id',
					'Client.stav',
                    'Client.countries_id',
					'ConnectionClientRequirement.id',
					'ConnectionClientRequirement.created',
                    'ConnectionClientRequirement.cl_notification',
                    'Countrie.name'
				),
                'order'=>'Client.name ASC'
			)
		);
		$this->set('cekaci_listina_list',$cekaci_listina_list);
		
		/*
		 * nacteni zamestnancu
		 */
		$this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Client','CompanyWorkPosition','CompanyMoneyItem','CmsUser')));
		$zamestnanci_list = $this->ConnectionClientRequirement->find('all', 
			array(
				'conditions' => array(
					'requirements_for_recruitment_id' => $order_item_id, 
					'type'=>2,
					'objednavka'=>1,
					'to' => '0000-00-00'
				),
				'fields'=>array(
					'CompanyWorkPosition.name',
					'CompanyMoneyItem.name',
					'CompanyMoneyItem.stravenka',
					'CompanyMoneyItem.doprava',
					'CompanyMoneyItem.cena_ubytovani_na_mesic',
					'Client.name',
					'CmsUser.name',
					'Client.id',
					'ConnectionClientRequirement.id',
                    'ConnectionClientRequirement.new',
					'ConnectionClientRequirement.created',
					'ConnectionClientRequirement.from'
				)
			)
		);
		$this->set('zamestnanci_list',$zamestnanci_list);
		
		/*
		 * defaultni natahnuti klientu dle profese v objednavce
		 */
		
		$this->loadModel('ConnectionClientCareerItem');
		$this->ConnectionClientCareerItem->bindModel(array('belongsTo'=>array('Client')));
		
		$kvalifikace_id = $this->data['CompanyOrderItem']['setting_career_item_id'];
		$this->set('kvalifikace_id',$kvalifikace_id);
			
        /**
         * Nova impelemntace ktera zastituje nahrani detailu obsazeni pro novy layout obsazeni objedanvek
         */
        if($render == 'new_order')
            $render = '../new_order/orders_cast_posts/index';
        
        
        $this->set('regexp','a-k');
        //render
        $this->render($render);
    }
	
    /**
 	* Natahnuti seznamu klientu dane kvalifikace 
 	* @param integer $kvalifikace_id
 	* @param integer $company_id
 	* @param integer $order_item_id
  	* @return view
  	* @access public
 	**/ 
    public function load_client($kvalifikace_id=null, $company_id = null, $order_item_id = null, $typ = 'cm',$regexp = 'a-k',$regexp_show = false){
    	$this->set('typ',$typ);
		if ($kvalifikace_id == null){
			die ('Prosím vyberte kvalifikaci');
		}

		$this->loadModel('ConnectionClientCareerItem');
		$this->loadModel('ConnectionClientRequirement');
		$this->loadModel('ConnectionClientRecruiter');
		//odstraneni tech co jsou jiz zamestnani - 7.10.09 - vypnuto
		$podminka = false;
 
 		/*
 		 * Podminka na to, co jaka skupina vidi za klienty
 		 */
		
		switch($this->logged_user['CmsGroup']['id']){
			// cm
			case 3:
					$condition = array();
					break;
			// in
			case 9:
            case 41:
					$condition = array('ConnectionClientRecruiter.cms_user_id'=>-1);
					break;
			// ex
			case 8:
					$condition = array('ConnectionClientRecruiter.cms_user_id'=>$this->logged_user['CmsUser']['id']);
					break;
			default:
					$condition = array();
					break;
		}
		
        $sub_query_regexp = $_regexp = ' REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(SUBSTRING(Client.name,1,1)),"ť","t"),"ď", "d"), "č", "c"), "ř", "r"), "š", "s"), "ž", "z"), "ľ", "l") REGEXP "^['.$regexp.']"';
 		if($regexp_show === false){
 		  $sub_query_regexp = null;
 		}
         if (count($condition) >0){
 		     /**
 		      * 24.8.2010
              * Pro IN misto podminky IN (..)
              * se vytvori SUB-QUERY
 		      */ 
 		    if(in_array($this->logged_user['CmsGroup']['id'],array(9,41))){
 		         $sub_query = 'ConnectionClientCareerItem.client_id IN (SELECT client_id FROM wapis__connection_client_recruiters WHERE cms_user_id = "-1")';
 		         if($regexp_show === false){
 		             $sub_query .= ' AND '.$_regexp;
 		         }    
            } 
            else{
                $this->loadModel('ConnectionClientRecruiter');
 			    $seznam_vlastnich_klientu = $this->ConnectionClientRecruiter->find('list',array('conditions'=>$condition,'fields'=>array('id','client_id')));
			}
            $podminka = true;
 		}

		
		$this->ConnectionClientCareerItem->bindModel(array('belongsTo'=>array('Client')));
		//var_dump(array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,$sub_query,$sub_query_regexp));
		//load client listu

		if($podminka){
		    if(in_array($this->logged_user['CmsGroup']['id'],array(9,41))) 
			    $this->set('client_list',$this->ConnectionClientCareerItem->find('all',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,$sub_query,$sub_query_regexp),'fields'=>array('DISTINCT Client.id','Client.name','Client.stav'),'recursive'=>1,'order'=>'Client.name')));
			else
                $this->set('client_list',$this->ConnectionClientCareerItem->find('all',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,'ConnectionClientCareerItem.client_id' => $seznam_vlastnich_klientu,$sub_query_regexp),'fields'=>array('DISTINCT Client.id','Client.name','Client.stav'),'recursive'=>1,'order'=>'Client.name')));
        }
        else	
			$this->set('client_list',$cc = $this->ConnectionClientCareerItem->find('all',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,$sub_query_regexp),'fields'=>array('DISTINCT Client.id','Client.name','Client.stav'),'recursive'=>1,'order'=>'Client.name')));
        
        
		/*
		 * Ti co jsou uz v objednavce namrdani
		 */
		$this->set(
			'pouziti_klienti',
			$this->ConnectionClientRequirement->find(
				'list',
				array(
					'conditions' => array(
						'requirements_for_recruitment_id' => $order_item_id,
						'OR' => array(
							'type' => array(1,3),
							'and' => array(
								'type'=>2,
								'to'=>'0000-00-00'
							)
						)
					),
					'fields'=>array('id','client_id')
				)
			)
		);
		
		
		//unset
		unset($this->ConnectionClientCareerItem);
		unset($this->ConnectionClientRequirement);
		unset($this->ConnectionClientRecruiter);
		unset($neco);
		unset($neco2);
	

		$this->set('company_id', $company_id);
		$this->set('order_template_id', $order_item_id);
        $this->set('regexp', $regexp);
        $this->set('regexp_show', $regexp_show);
		
		$this->render('tabs/client_list');
	}
	
	
	/**
	 * Vlozeni klienta na nominacni listinu 
	 * @param integer $company_order_id
	 * @param integer $company_id
	 * @param integer $client_id
 	 * @return JSON
 	 * @access public
	 **/ 
	public function add_to_nl($company_order_id = null, $company_id = null, $client_id=null){
		/*
		 * Pokud nejsou vsechny vstupni parametry, jdi do dupy
		 */
		if ($company_order_id == null || $company_id == null || $client_id == null)
			die(json_encode(array('result'=>false,'message'=>'Nejsou vsechny udaje potrebne pro tuto funkce')));		
		/*
		 * Ma tato skupina uzivatelu pravo pridavat uzivatele na nominacni
		 * listinu? Pokud ne, tak fuck!!
		 * Otazka: Co s adminem? 
		 */
		$access_group_premission = array('CM'=>3, 'IN'=>9,'IN2'=>41,'EN'=>8,'ADMIN'=>1,'KOO'=>4);
        $acces_superior_id = array(2);
		if (in_array($this->logged_user['CmsGroup']['id'], $access_group_premission) || in_array($this->logged_user['CmsGroup']['cms_group_superior_id'], $acces_superior_id)){
			$this->loadModel('Client');
			$data = $this->Client->read(array('name','id'), $client_id);
			unset($this->Client);
			/*
			 * Zjisteni, zda byl nalezen klient s ID $client_id
			 */
			if ($data){
				$this->loadModel('ConnectionClientRequirement');
				
				/*
				 * Osetreni, zda neexistuje connection jiz na tuto objednavku, vsechny az na type -1 a na type 2 s neukoncenim zamestnanim, 
				 * kde to neni 0000-00-00, kde nam to znamena, ze tu byl, ale byl odstranen
				 */
				$try_find_con = $this->ConnectionClientRequirement->find('count',array(
					'conditions'=>array(
						'requirements_for_recruitment_id' => $company_order_id,
						'client_id' => $client_id,
						'objednavka'=>1,
						'or' => array(
							'type IN (1,3)', 	// je umisten nebo nominovan 	
							'AND' => array(		// nebo je zamestnan 
								'type' 	=> 2,
								'to'	=>'0000-00-00'
							) 					
						)
					)
				));
				if ($try_find_con > 0){
					$this->error_log("Connection jiz existuje, asi chyba JS nebo seznam klientu, kde nemel byt, snazil se umistit klienta 2x na objednavku!! ORDER:$company_order_id ,COMPANY:$company_id, CLIENT:$client_id");
					die(json_encode(array('result'=>false,'message'=>'Tento klient jiz je umisten na teto objednavce')));
				}
				
				/*
				 * Vytvoreni nove connection s typem 3, tzn. nominovan
				 */
				$connection_to_save = array('ConnectionClientRequirement'=>array(
					'requirements_for_recruitment_id' => $company_order_id,
					'client_id' => $client_id,
					'objednavka' => 1,
					'company_id' => $company_id,
					'cms_user_id' => $this->logged_user['CmsUser']['id'],
					'type'=>3 // nominovan
				));
				
                
                
				/*
				 * Vlastni ulozeni Connection
				 */
				if ($this->ConnectionClientRequirement->save($connection_to_save)){
				    
                    /**
                     * inkrementace počtu lidi na nl
                     */         
                    $this->CompanyOrderItem->updateAll(array('CompanyOrderItem.nl_count'=>'CompanyOrderItem.nl_count+1'), array('CompanyOrderItem.id'=>$company_order_id));
                    
					/*
					 * Rsponse JSON dat s hodnotou true, vraci informace o klientovi
					 * plus doplnkovou informaci o tom, jake je prave datum
					 */
					$data['date'] = date("d.m.Y");
					$data['connection_id'] = $this->ConnectionClientRequirement->id;
					die(json_encode(array('result'=>true,'data'=>$data)));
				} else {
					$this->error_log("Nastala chyba behem ukladani connection ORDER:$company_order_id ,COMPANY:$company_id, CLIENT:$client_id");
					die(json_encode(array('result'=>false,'message'=>'Nastala chyba behem ukladani connection')));
				}
			}else{
				$this->error_log("Klient s ID: $client_id nebyl nalezen");	
				die(json_encode(array('result'=>false,'message'=>'Klient s ID:'.$client_id.' nebyl nalezen')));
			}
		} else {
			$this->error_log("Neopravneny pristup na tuto funkci");
			die(json_encode(array('result'=>false,'message'=>'Neopravneny pristup na tuto funkci!')));
		}
	}
	
		
	/**
	 * Odebrani klienta z nominacni listiny 
	 * @param integer $connection_id
	 * @param integer $client_id
 	 * @return JSON
 	 * @access public
	 **/ 
	public function remove_from_nl($connection_id=null, $client_id = null){
		/*
		 * Pokud nejsou vsechny vstupni parametry, jdi do dupy
		 */
		if ($connection_id == null ){
			$this->error_log('Nejsou vsechny udaje potrebne pro tuto funkci');
			die(json_encode(array('result'=>false,'message'=>'Nejsou vsechny udaje potrebne pro tuto funkci')));
		}	
		
		/*
		 * Natahnuti modelu a najiti daneho connection  
		 */	
		$this->loadModel('ConnectionClientRequirement');
		$data = $this->ConnectionClientRequirement->read(array('client_id','type'),$connection_id);
		
		/*
		 * Pokud nasel dane connection v DB, pokracuj, jinak to kill-ni
		 */
		if ($data){
			
			/*
			 * Pokud je connection ve spravnem typu, tedy 3 (nominacni listina) jed dal
			 * jinak se proces kill-ne
			 */
			if ($data['ConnectionClientRequirement']['type'] == 3){
				$this->ConnectionClientRequirement->id = $connection_id;
				
				/*
				 * Zkontroluj, zda ulozeni probehne ok,
				 * pokud ano, vraci na bazi JSON id klienta pro dalsi praci s JS
				*/
				if ($this->ConnectionClientRequirement->saveField('type',-1))
					die(json_encode(array('result'=>true, 'data'=>$data)));
				else {
					$this->error_log("Nepodarilo se odstranit klienta z nominacni listiny, vhyba behem ukladani (CONNECTION:$connection_id, CLIENT:".$data['ConnectionClientRequirement']['client_id'].")");
					die(json_encode(array('result'=>false,'message'=>'Nepodarilo se odstranit klienta z nominacni listiny, vhyba behem ukladani')));
				}
			} else {
				$this->error_log("Pokus o odstraneni klienta ID:".$data['ConnectionClientRequirement']['client_id']." z nominacni listiny (CONNECTION:$connection_id), ten tu ale neni");
				die(json_encode(array('result'=>false,'message'=>'Snažíte se odstranit klienta z nominacni listiny, na ktere se vsak nenaleza')));
			}
		} else {
				$this->error_log("Connection nebylo nalezeno (CONNECTION:$connection_id)");
				die(json_encode(array('result'=>false,'message'=>'Nebylo nalezeno connection v DB')));
		}
	}
	/**
	 * Presun z nominacni listiny na cekaci listinu
	 * @param integer $connection_id
	 * @param integer $client_id
	 * @return JSON
	 */
	public function move_from_nl_to_cl($connection_id=null, $client_id = null){
		/*
		 * Pokud nejsou vsechny vstupni parametry, jdi do dupy
		 */
		if ($connection_id == null ){
			$this->error_log('Nejsou vsechny udaje potrebne pro tuto funkci');
			die(json_encode(array('result'=>false,'message'=>'Nejsou vsechny udaje potrebne pro tuto funkci')));
		}	
		
		/*
		 * Natahnuti modelu a najiti daneho connection  
		 */	
		$this->loadModel('ConnectionClientRequirement');
		$data = $this->ConnectionClientRequirement->read(array('requirements_for_recruitment_id','client_id','type','id','company_id'),$connection_id);
		
		/*
		 * Pokud nasel dane connection v DB, pokracuj, jinak to kill-ni
		 */
		if ($data){
			
			/*
			 * Pokud je connection ve spravnem typu, tedy 3 (nominacni listina) jed dal
			 * jinak se proces kill-ne
			 */
			if ($data['ConnectionClientRequirement']['type'] == 3){
				$this->ConnectionClientRequirement->id = $connection_id;
				
				/*
				 * Zkontroluj, zda ulozeni probehne ok,
				 * pokud ano, vraci na bazi JSON id klienta pro dalsi praci s JS
				*/
				if ($this->ConnectionClientRequirement->save(array(
                    'type'=>1,
                    'add_to_cl_datetime'=>date('Y-m-d H:i:s'),
                    'add_to_cl_cms_user_id'=>$this->logged_user['CmsUser']['id']
                ))){
                    /**
                     * inkrementace počtu lidi na cl
                     */         
                    $this->CompanyOrderItem->updateAll(array('CompanyOrderItem.cl_count'=>'CompanyOrderItem.cl_count+1'), array('CompanyOrderItem.id'=>$data['ConnectionClientRequirement']['requirements_for_recruitment_id']));
                    
                
              			
        	       //client
        	       $this->loadModel('Client');
                   $this->Client->bindModel(array('belongsTo'=>array('Countrie'=>array('foreignKey'=>'countries_id'))));
        	       $cl= $this->Client->read(array(
                            'Client.name','Client.mesto','Client.mobil1','Client.mobil2',
                            'Client.mobil3','Client.mobil_active',
                            'opp_nohavice','opp_bluza','opp_tricko','opp_obuv',
                            'Client.os_pohovor','Countrie.name'
                        ),
                        $data['ConnectionClientRequirement']['client_id']
                   );
                   $cl['Client']['mobil'] = $cl['Client'][$cl['Client']['mobil_active']];
                   $cl['Client']['opp'] = $cl['Client']['opp_nohavice'].'-'.$cl['Client']['opp_bluza'].'-'.$cl['Client']['opp_tricko'].'-'.$cl['Client']['opp_obuv'];;
                   $cl['Client']['os_pohovor'] = ($cl['Client']['os_pohovor'] == 1?'Ano':'Ne');
                   $cl['Client']['okres'] = $cl['Countrie']['name'];
                   $data['Client'] = $cl['Client'];
                   
                   
                   //company
        	       $this->loadModel('Company');
        	       $comp= $this->Company->read($this->Company->company_data_for_email_notification,$data['ConnectionClientRequirement']['company_id']);
                   
                   //objednavka
        	       $this->loadModel('CompanyOrderItem');
        	       $coi= $this->CompanyOrderItem->read(null,$data['ConnectionClientRequirement']['requirements_for_recruitment_id']);
        	      	
                   $replace_list = array(
        				'##Company.name##' 	=>$comp['Company']['name'],
        				'##RequirementsForRecruitment.name##' 	=>$coi["CompanyOrderItem"]["name"],
        				'##Client.name##' 	=>$cl["Client"]["name"],
        				'##Recruiter.name##' 	=>$this->logged_user['CmsUser']['name'],
        				'##Info.profese##' 	=>$coi['CompanyOrderItem']['name'],
        				'##Info.mesto##' 	=>$coi['CompanyOrderItem']['job_city'],
        				'##Info.poznamkakvolnemumistu##' 	=>$coi['CompanyOrderItem']['comment'],
        				'##Info.napln##' 	=>$coi['CompanyOrderItem']['napln_prace'],
        				'##Info.komentar##' 	=>$coi['CompanyOrderItem']['mzdove_podminky_poznamka'],
        				'##Info.smennost##' 	=>$this->smennost_list[$coi['CompanyOrderItem']['setting_shift_working_id']],
        				'##Info.komentarksmenosti##' 	=>$coi['CompanyOrderItem']['komentar_smennosti']
        			);
        			$this->Email->set_company_data($comp['Company']);
        			$this->Email->send_from_template_new(5,array(),$replace_list);
        			
                
                
					die(json_encode(array('result'=>true, 'data'=>$data)));
                }    
				else {
					$this->error_log("Nepodarilo se odstranit klienta z nominacni listiny, chyba behem ukladani (CONNECTION:$connection_id, CLIENT:".$data['ConnectionClientRequirement']['client_id'].")");
					die(json_encode(array('result'=>false,'message'=>'Nepodarilo se odstranit klienta z nominacni listiny, chyba behem ukladani')));
				}
			} else {
				$this->error_log("Pokus o odstraneni klienta ID:".$data['ConnectionClientRequirement']['client_id']." z nominacni listiny (CONNECTION:$connection_id), ten tu ale neni");
				die(json_encode(array('result'=>false,'message'=>'Snažíte se odstranit klienta z nominacni listiny, na ktere se vsak nenaleza')));
			}
		} else {
				$this->error_log("Connection nebylo nalezeno (CONNECTION:$connection_id)");
				die(json_encode(array('result'=>false,'message'=>'Nebylo nalezeno connection v DB')));
		}
	}

	/**
	 * Domwin pro zamestnani daneho klienta na pracovni pozici.
	 * Dochazi k volbe pracovni pozice z kalkulaci a nasledne vybrani dane formy odmeny
	 * @param integer $connection_id 
	 * @return View || JSON
	 */
	public function move_to_zam_domwin($connection_id=null){
		if (empty($this->data)){
		  
            /**
             * Natahnuti perrmision 
             * zjistujeme zda ma dana uzivatelska skupina povolene prevest klienta do stavu zamestanani
             */
            $permision = $this->logged_user['CmsGroup']['permission']['orders_cast_posts']; 
            if(!isset($permision['move_to_zam']) || (isset($permision['move_to_zam']) && $permision['move_to_zam'] == false))
                die('<br/><br/><br/><br/><p align="center"><em>Nemáte oprávnění k zaměstnávání klientů.<br /> Kontaktujte administrátora</em></p>');
            
			/*
			 * Natahnuti detailu ohledne connection
			 */
			$this->loadModel('ConnectionClientRequirement');
			$this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('CompanyOrderItem'=>array('foreignKey'=>'requirements_for_recruitment_id'))));
			$this->data = $connection = $this->ConnectionClientRequirement->read(array('ConnectionClientRequirement.requirements_for_recruitment_id','ConnectionClientRequirement.client_id','ConnectionClientRequirement.created','ConnectionClientRequirement.type','ConnectionClientRequirement.id','ConnectionClientRequirement.company_id','CompanyOrderItem.count_of_free_position','CompanyOrderItem.id'),$connection_id);
			/*
			 * Omezeni, zda neni jiz plnny stav, pokud ano, neni mozne pridat dalsiho zamestnance
			 */
			$pocet_jiz_zamestnanych = $this->ConnectionClientRequirement->find(
				'count',
				array(
					'conditions'=>array(
						'type'		=> 2,
						'objednavka'=>1,
						'to'		=> '0000-00-00',
						'requirements_for_recruitment_id' => $connection['CompanyOrderItem']['id']
					)
				)
			);
			if ($pocet_jiz_zamestnanych >= $connection['CompanyOrderItem']['count_of_free_position']){
				die('<br/><br/><br/><br/><p align="center"><em>Litujeme, ale všechny pozice jsou již plnně obsazeny.</em></p>');
			}

			/*
			 * Zjisteni, zda muzu daneho klienta zamestnat, tzn. neni uz zamestnany? 
			 * Jedna se o omezeni ohledne vice nasobnemu zamestnavani, prozatim musi byt omezeno i na
			 * bazi JS, kde zamestnani nepujdou prevest z cekaci listiny na zamestnance
			 */
			$je_zamestnan = $this->ConnectionClientRequirement->find(
				'count',
				array(
					'conditions'=>array(
						'client_id' => $connection['ConnectionClientRequirement']['client_id'],
						'type'		=> 2,
						'to'		=> '0000-00-00'
					)
				)
			);
			if ($je_zamestnan > 0){
				$this->error_log("Pokus o zamestnani klienta ID:".$connection['ConnectionClientRequirement']['client_id']." z nominacni listiny (CONNECTION:$connection_id), klient je jiz ale zamestnan jinde");
				die('<br/><br/><br/><br/><p align="center"><em>Tento klient je již zamětnán, není možné jej zaměstnat!!</em></p>');
			}
			/*
			 * Natahnuti platnych  profesi pro danou spolecnost, id spolecnosti se bere z connection
			*/
			
			$this->loadModel('CompanyWorkPosition');
			$this->CompanyWorkPosition->bindModel(array('hasOne'=>array('CompanyMoneyValidity')));
			$this->set(
				'company_work_position_list', 
				$this->CompanyWorkPosition->find(
					'list',
					array(
						'conditions'=>array(
							'CompanyWorkPosition.company_id' => $connection['ConnectionClientRequirement']['company_id'],
							//'CompanyWorkPosition.parent_id'=> -1, // ?? DOTAZ, ZDA TO TAK JESTE JE
							'CompanyWorkPosition.test'=>0,
                            'CompanyWorkPosition.new' => 0,
							'CompanyWorkPosition.kos' => 0,
							'CompanyMoneyValidity.platnost_do' => '0000-00-00'
						),
						'order'=>'CompanyWorkPosition.name ASC',
						'recursive'=>1
					)
				)
			);
			
			$this->render('domwins/move_to_zam_domwin');
		} else {

            $prvni_zamestnani = false;
          
			/*
			 * Natahnuti informaci ohledne firmy pro vytvoreni historie
			 */
		   	$this->loadModel('Company');
	       	$company_info = $this->Company->read(null,$this->data['ConnectionClientRequirement']['company_id']);
	       	unset($this->Company);
			
		  	/*
		  	 * Ulozeni connection, uklada se 
		  	 * company_money_item_id, company_work_position_id, from a dojde zmena k type na 2 
		  	 */
            $this->loadModel('ConnectionClientRequirement');
            $this->data['ConnectionClientRequirement']['from_when'] = date('Y-m-d H:i:s');
            $this->data['ConnectionClientRequirement']['from_cms_user_id'] =$this->logged_user['CmsUser']['id'];
            

            /*
             * Ulozeni zaznamu do tabulky ClientBuy kvuli fakturaci daneho klienta mezi firmami v ramci AT
             *
             */
            //die(json_encode(array('result'=>false,'data'=>$this->data['ConnectionClientRequirement']['requirements_for_recruitment_id'])));
            if (!$this->ConnectionClientRequirement->save($this->data)){
  				$this->error_log("Nepodarila se ulozit zmena connection do DB, CONNECTION:".$this->data['ConnectionClientRequirement']['id']);
				die(json_encode(array('result'=>false,'message'=>'Nepodarila se ulozit zmena connection do DB')));
  			}else{
                  $this->loadModel('ClientBuy');
                  $old_record = $this->ClientBuy->find('first', array(
                      'conditions'=>array('client_id'=>$this->data['ConnectionClientRequirement']['client_id'], 'status'=>1)
                  ));

                  if($old_record){
                      $from_user = $old_record['ClientBuy']['to_cms_user_id'];
                  }else{
                      $from_user = 1; //Defaultni vlastnik
                  }

                  if($this->data['ConnectionClientRequirement']['requirements_for_recruitment_id']){
                        $this->loadModel('CompanyOrderItem');

                        $setting_career = $this->CompanyOrderItem->find('first',
                            array(
                                'fields' => array('CompanyOrderItem.setting_career_item_id'),
                                'conditions'=>array('CompanyOrderItem.id'=>$this->data['ConnectionClientRequirement']['requirements_for_recruitment_id'])
                            )
                        );
                        $set_car_it = $setting_career['CompanyOrderItem']['setting_career_item_id'];
                  }else{
                        $set_car_it = 0;
                  }

                  $to_save = array(
                      'ClientBuy'=> array(
                          'client_id' => $this->data['ConnectionClientRequirement']['client_id'],
                          'setting_career_item_id' => $set_car_it,
                          'from_cms_user_id' => $from_user,
                          'to_cms_user_id' => $this->logged_user['CmsUser']['id'],
                          'from_date' => date('Y-m-d'),
                          'status' => 1
                      )
                  );
                  $res = $this->ClientBuy->save($to_save);

                  if($res && $old_record){
                      $this->ClientBuy->save(
                          array( 'ClientBuy'=>
                            array(
                                'id' => $old_record['ClientBuy']['id'],
                                'status' => 0,
                                'to_date' => date('Y-m-d')
                            )
                          )
                      );
                  }
            }

 			/*
 			 * Odstranit pozice, na kterych je umisten. Toto je ok, pokud berem v uvahu,
 			 * ze se jedna pouze o moznost zamestnat pouze na jedo misto
 			 * Pokud toto bude rozsireno, bude se muset upravit
 			 */
 			$this->ConnectionClientRequirement->updateAll(
				array('ConnectionClientRequirement.type' => '-1'),
		    	array('ConnectionClientRequirement.client_id' => $this->data['ConnectionClientRequirement']['client_id'],'ConnectionClientRequirement.type' => array(1,3))
		 	);
  		
			/*
			 * Pokud klienta jednou zamestnam, tak se vytvori vazba mezi internim recruitem a klientem.
			 * Podmineno tim, ze tato vazba neni jiz vytvorena. Najde se tedy spojeni klient-recruiter a pokud
			 * neexistuje, potom jej vytvor
			 */
			$this->loadModel('ConnectionClientRecruiter');
			$ma_interni_nabor = $this->ConnectionClientRecruiter->find('count',array('conditions'=>array('client_id'=>$this->data['ConnectionClientRequirement']['client_id'],'cms_user_id'=>-1)));
			if($ma_interni_nabor == 0){ 
			    $prvni_zamestnani = true; 
				$this->ConnectionClientRecruiter->save(array('ConnectionClientRecruiter'=>array('client_id'=> $this->data['ConnectionClientRequirement']['client_id'],'cms_user_id' => -1)));
			}

			/*
			 * Odeslani emailu o nove situaci, odchazim vsem uzivatelum, 
			 * jenz jsou v CmsGroup id 5
			 */
  		

			//
			$this->loadModel('CompanyWorkPosition');
			$cwp = $this->CompanyWorkPosition->read(array('name'),$this->data['ConnectionClientRequirement']['company_work_position_id']);
			unset($this->CompanyWorkPosition);
			
	       //client
	       $this->loadModel('Client');
	       $cl= $this->Client->read(null,$this->data['ConnectionClientRequirement']['client_id']);
	      	
			$replace_list = array(
					'##Company.name##' 	=>$company_info['Company']['name'],
					'##CompanyWorkPosition.name##' 	=>$cwp["CompanyWorkPosition"]["name"],
					'##Client.name##' 	=>$cl["Client"]["name"],
					'##Message.text##' 	=>$this->data['ConnectionClientRequirement']["text"]
			);
			$this->Email->set_company_data($company_info['Company']);	
			$this->Email->send_from_template_new(7,array(),$replace_list);
			
			/*
			 * ulozeni stavu clienta
			 */
			$this->Client->saveField('stav',2);
            
            /**
             * Pokud je prvne zamestnan tak se mu priradi ohodnoceni na cislo +3
             * Nyni je to osetreno pouze na zaklade toho zda uz ma Interni nabor
             * ktery se prirazuje jen poprve.
             * 
             * Pravdepodobnost toho ze nekdo nema IN v recruiterech je dost mala
             */
            if($prvni_zamestnani === true)
			    $this->Client->saveField('hodnoceni',3);
			
            
            unset($this->Client);
			/*
			 * Data pro javascript, respektive presun radku do zamestnancu
			 */
			
			$this->loadModel('CompanyMoneyItem');
			$this->CompanyMoneyItem->bindModel(array('belongsTo'=>array('CompanyWorkPosition')));
			$money = $this->CompanyMoneyItem->read(
				array(
					'CompanyWorkPosition.name',
					'CompanyMoneyItem.name',
					'CompanyMoneyItem.checkbox_faktura',
					'CompanyMoneyItem.checkbox_dohoda',
					'CompanyMoneyItem.checkbox_cash',
					'CompanyMoneyItem.checkbox_pracovni_smlouva',
				), 
				$this->data['ConnectionClientRequirement']['company_money_item_id']
			);

            
            $connection = $this->ConnectionClientRequirement->read(array('requirements_for_recruitment_id','created'),$this->data['ConnectionClientRequirement']['id']);
	
            /**
             * inkrementace počtu zaměstnanců
             */         
            $this->CompanyOrderItem->updateAll(array('CompanyOrderItem.zam_count'=>'CompanyOrderItem.zam_count+1'), array('CompanyOrderItem.id'=>$connection['ConnectionClientRequirement']['requirements_for_recruitment_id']));
                   
            /**
             * nalezeni formularu u firem - aktivnich
             * podle formy odmeny vyhledat prislusne typy formularu podle skupin 
             */ 
                 $this->loadModel('CompanyFormTemplate');
                 $company_template = $this->CompanyFormTemplate->find('all',array(
                    'conditions'=>array(
                        'company_id'=>$this->data['ConnectionClientRequirement']['company_id'],
                        'history'=>0,
                        'kos'=>0,
                        'form_template_group_id'=>self::create_form_group_array_conditions($money['CompanyMoneyItem'])
                    )
                 ));
                  /**
                  * vygenerovani jednotlivych formularu ke klientovi
                  */
                 foreach($company_template as $template){
                     $this->requestAction('form_templates/generate_form/' . $template['CompanyFormTemplate']['id'] . '/' . $this->data['ConnectionClientRequirement']['client_id'].'/null/true');
                 }

           
            /**
             * Musi se doladit nahravani novych kalkulaci
             * at se tam ted neloudujou nesmysli
             * tip: odstranit data a udelat loadcontent a opravit nacitani
             * pro nove kalkulace jsou nefunkcni i nacitani formularu
             */ 
            if($this->data['ConnectionClientRequirement']['new'] == 1 || $this->data['ConnectionClientRequirement']['new'] == 'on'){
                $money['CompanyWorkPosition']['name'] = $money['CompanyMoneyItem']['name'] = '';
            }       
            
			$data = array(
				'client_name'		=> $cl["Client"]["name"],
				'client_id'			=> $this->data['ConnectionClientRequirement']['client_id'],
				'connection_id'		=> $this->data['ConnectionClientRequirement']['id'],
				'cms_user_name'		=> $this->logged_user['CmsUser']['name'],
				'created' 			=> date('d.m.Y',strtotime($connection['ConnectionClientRequirement']['created'])),
				'od' 				=> date('d.m.Y',strtotime($this->data['ConnectionClientRequirement']['from'])),
				'money_item'		=> $money['CompanyMoneyItem']['name'],
				'pozice'			=> $money['CompanyWorkPosition']['name']
			);
			
      		die(json_encode(array('result'=>true,'data'=>$data)));
		}
	}
    
    /**
     * podle formy odmeny nastav skupiny formuralu
     * @param money_item_checkboxs
     * @return array
     */
    private function create_form_group_array_conditions($money){
        $tmp = array();
        
        if($money['checkbox_pracovni_smlouva'] == 1)
            $tmp[] = 1;
        if($money['checkbox_dohoda'] == 1)
            $tmp[] = 2;
        if($money['checkbox_faktura'] == 1)
            $tmp[] = 3;    
        if($money['checkbox_cash'] == 1)
            $tmp[] = 4;     
        
        
        return $tmp;
    } 
    

	/**
	 * Natahnuti platnych kalkulaci money_items k dane pracovni pozici
	 * @param integer $company_work_position_id
	 * @return JSON
	 */	
	public function load_calculation_for_profese($company_work_position_id = null,$new = null) {
		if ($company_work_position_id == null){ 
			$this->error_log("nebyla vstupni promenna company_work_position_id");
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
		}
	
		/*
		 * Natahnutnuti jednotlivych platnosti k dane pozici s danou platnosti 
		*/
		$this->loadModel('CompanyMoneyValidity');
        if($new == 'true')
		    $this->CompanyMoneyValidity->bindModel(array('hasOne'=>array('NewMoneyItem'=>array('fields'=>array('id')))));
		else
            $this->CompanyMoneyValidity->bindModel(array('hasMany'=>array('CompanyMoneyItem'=>array('fields'=>array('id','name','vypocitana_celkova_cista_maximalni_mzda_na_hodinu','cista_mzda_z_pracovni_smlouvy_na_hodinu','cista_mzda_faktura_zivnostnika_na_hodinu','cista_mzda_dohoda_na_hodinu','cista_mzda_cash_na_hodinu','stravenka','fakturacni_sazba_na_hodinu','doprava','cena_ubytovani_na_mesic','pausal','ps_pausal_hmm','odmena_fakturace_1','odmena_fakturace_2')))));
		
        $money_items = $this->CompanyMoneyValidity->find(
			'all', 
			array(
				'conditions'=>array(
					'CompanyMoneyValidity.company_work_position_id' => $company_work_position_id,
					'platnost_od !='=>'0000-00-00', 
					'platnost_do '=>'0000-00-00'
				)
			)
		);
  
		/*
		 * Pokud existuji kalkulace s platnosti k teto pozici 
		 */
		if ($money_items == false){
				$this->error_log("Nebyla nalezena kalkulace s odpovídající platností.");
				die(json_encode(array('result'=>false,'message'=>"Nebyla nalezena kalkulace s odpovídající platností.")));
		} else {
			if (count($money_items) > 1){
					$this->error_log("Pro tuto profesi byly naleznuty 2 nebo více odpovidajících platností pro $year/$month. Pravděpodobně se jedná o chybu v zadání nové platnosti fakturační sazby.");
					die(json_encode(array('result'=>false,'message'=>"Pro tuto profesi byly naleznuty 2 nebo více odpovidajících platností pro $year/$month. Pravděpodobně se jedná o chybu v zadání nové platnosti fakturační sazby.")));
			} else {
					die(json_encode(array('result'=>true,'data'=>$money_items[0][($new == 'true' ? 'NewMoneyItem' : 'CompanyMoneyItem')])));
			}
		}
	}

	/**
	 * Odebrani klienta z cekaci listiny a predani zpet do nominacni listiny
	 * @param integer $connection_id
	 * @return 
	 */
	public function remove_from_cl($connection_id = null) {
		$this->loadModel('ConnectionClientRequirement');
		if (empty($this->data)){
			/*
			 * Natahnuti detailu ohledne connection
			 */
			$this->data = $connection = $this->ConnectionClientRequirement->read(array('client_id','type','id','company_id','requirements_for_recruitment_id'),$connection_id);
		
            $this->loadModel('Client');
            $cl = $this->Client->read('hodnoceni',$this->data['ConnectionClientRequirement']['client_id']);
            $this->set('hodnoceni',$cl['Client']['hodnoceni']);
            $this->set('client_id',$this->data['ConnectionClientRequirement']['client_id']);
            unset($this->Client);
            
            $this->render('domwins/remove_from_cl_domwin');
		} else {
			/*
			 * Natahnuti informaci ohledne firmy pro vytvoreni historie
			 */
		   	$this->loadModel('Company');
	       	$company_info = $this->Company->read($this->Company->company_data_for_email_notification,$this->data['ConnectionClientRequirement']['company_id']);
	       	unset($this->Company);
			
			/*
			 * zmena typu connection z cekaci lilstiny na nominacni (type:3)
			 */
			$this->ConnectionClientRequirement->id = $this->data['ConnectionClientRequirement']['id'];
			if (!$this->ConnectionClientRequirement->saveField('type',3)){
				$this->error_log("Chyba behem ukladani na typ 3 CONNECTION:".$this->data['ConnectionClientRequirement']['id'].", CLIENT:".$this->data['ConnectionClientRequirement']['client_id']);
				die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani na typ 3.')));	
			}
			
	  		/*
			 * Odeslani emailu o nove situaci, odchazim vsem uzivatelum, 
			 * jenz jsou v CmsGroup id 5
			 */
  		
						
		    //client
		    $this->loadModel('Client');
		    $cl= $this->Client->read(null,$this->data['ConnectionClientRequirement']['client_id']);
      	
			/*
			 * Nacteni informaci k objednavce
			 */
			
			$this->ConnectionClientRequirement->bindModel(array(
				'belongsTo'=>array(
					'CompanyOrderItem'=>array('foreignKey'=>'requirements_for_recruitment_id'),
					'CmsUser'
				)
			));
			$order_item = $this->ConnectionClientRequirement->read(array('CompanyOrderItem.name','CmsUser.cms_group_id','CmsUser.name'),$this->data['ConnectionClientRequirement']['id']);
			$replace_list = array(
					'##Company.name##' 						=> $company_info['Company']['name'],
					'##RequirementsForRecruitment.name##' 	=> $order_item["CompanyOrderItem"]["name"],
					'##Client.name##' 						=> $cl["Client"]["name"],
					'##CmsUser.name##' 						=> $this->logged_user["CmsUser"]["name"],
					'##Message.text##' 						=> $this->data['ConnectionClientRequirement']["text"]
			);
			$this->Email->set_company_data($company_info['Company']);	
			$this->Email->send_from_template_new(6,array(0=>$this->logged_user['CmsUser']['email']),$replace_list);
		  

			/*
			 * Podle toho, jaky cms_user vlozil klienta do nominacni listiny, 
			 * respektive z jake cms_group byl, tak tam bude do dane nominacni listiny
			 * vracen.
			*/
			switch($order_item['CmsUser']['cms_group_id']){
				case 3: $target = 'cm'; break;	
				case 9: $target = 'in'; break;
                case 41: $target = 'in'; break;	
				case 8: $target = 'en'; break;
				default: $target = 'cm'; break;
			}
            
            
            
            /**
             * Ulozeni problemu
             */
            if($this->data['ConnectionClientRequirement']['reason'] == 6){
                if($this->data['hodnoceni'] != 0){
                   $m_data = $this->data;
                   $m_data['text'] = $m_data['ConnectionClientRequirement']['text']; 
                    
                   echo $this->requestAction('/misconducts/save/',array('request_data'=>$m_data));
                }
            } 
			
      		die(json_encode(array('result'=>true,'nl_target'=>$target,'connection_id'=>$this->data['ConnectionClientRequirement']['id'],'client_id'=>$this->data['ConnectionClientRequirement']['client_id'],'priradil'=>$order_item['CmsUser']['name'])));
		}
	}

    /**
	 * nacteni kalkulaci (money items) pro danou pozici do 
	 * editace template
	 * @param $company_id
	 * @param $render, if render is true, render element
 	 * @return view list of company money items
 	 * @access public
	 */
	function load_kalkulace($position_id, $render = true){
		$this->loadModel('CompanyMoneyItem');
        $this->CompanyMoneyItem->bindModel(array('belongsTo'=>array('CompanyMoneyValidity')));
		$this->set('kalkulace_list', $this->CompanyMoneyItem->find(
			'all',
			array(
				'conditions'=>array(
					'CompanyMoneyItem.company_work_position_id' => $position_id,
                    'CompanyMoneyItem.kos'=>0,
                    'CompanyMoneyValidity.platnost_do' => '0000-00-00'
				)
			)
		));
		if ($render == true)
			$this->render('templates/kalkulace');
	}  
    
    function print_prehled($company_order_item_id){
        $this->layout = 'print';
        
        /*
		 * Natahnuti klientu, kteri jsou na cekaci listine
		 */
        $this->loadModel('ConnectionClientRequirement'); 
        $this->loadModel('CompanyOrderItem'); 
        $this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Client')));
		$cekaci_listina_list = $this->ConnectionClientRequirement->find('all', 
			array(
				'conditions' => array(
					'requirements_for_recruitment_id' => $company_order_item_id, 
					'objednavka' =>1,
					'type'=>1
				),
				'fields'=>array(
					'Client.name',
					'Client.mobil1',
					'Client.mobil2',
					'Client.mobil3',
					'Client.mobil_active'					
				),
                'order'=>'Client.name ASC'
			)
		);
		$this->set('cekaci_listina_list',$cekaci_listina_list);
		$this->set('nastup',$this->CompanyOrderItem->read('start_datetime',$company_order_item_id));
                    
    }
    
    
    /**
	 * Vlozeni klienta na nominacni listinu 
	 * @param integer $company_order_id
	 * @param integer $company_id
	 * @param integer $client_id
 	 * @return JSON
 	 * @access public
	 **/ 
	public function single_add_to_nl($company_order_id = null, $company_id = null, $client_id=null){
		
		
			$this->loadModel('Client');
			$data = $this->Client->read(array('name','id'), $client_id);
			unset($this->Client);
			/*
			 * Zjisteni, zda byl nalezen klient s ID $client_id
			 */
			if ($data){
				$this->loadModel('ConnectionClientRequirement');
				
				/*
				 * Osetreni, zda neexistuje connection jiz na tuto objednavku, vsechny az na type -1 a na type 2 s neukoncenim zamestnanim, 
				 * kde to neni 0000-00-00, kde nam to znamena, ze tu byl, ale byl odstranen
				 */
				$try_find_con = $this->ConnectionClientRequirement->find('count',array(
					'conditions'=>array(
						'requirements_for_recruitment_id' => $company_order_id,
						'client_id' => $client_id,
						'objednavka'=>1,
						'or' => array(
							'type IN (1,3)', 	// je umisten nebo nominovan 	
							'AND' => array(		// nebo je zamestnan 
								'type' 	=> 2,
								'to'	=>'0000-00-00'
							) 					
						)
					)
				));
				if ($try_find_con > 0){
					$this->error_log("Connection jiz existuje, asi chyba JS nebo seznam klientu, kde nemel byt, snazil se umistit klienta 2x na objednavku!! ORDER:$company_order_id ,COMPANY:$company_id, CLIENT:$client_id");
					return array('result'=>false,'wrong'=>true,'message'=>$data['Client']['id'].' - '.$data['Client']['name']);
				}
				
				/*
				 * Vytvoreni nove connection s typem 3, tzn. nominovan
				 */
				$connection_to_save = array('ConnectionClientRequirement'=>array(
					'requirements_for_recruitment_id' => $company_order_id,
					'client_id' => $client_id,
					'objednavka' => 1,
					'company_id' => $company_id,
					'cms_user_id' => $this->logged_user['CmsUser']['id'],
					'type'=>3 // nominovan
				));
				
                
                
				/*
				 * Vlastni ulozeni Connection
				 */
				if ($this->ConnectionClientRequirement->save($connection_to_save)){
				    
                    /**
                     * inkrementace počtu lidi na nl
                     */         
                    $this->CompanyOrderItem->updateAll(array('CompanyOrderItem.nl_count'=>'CompanyOrderItem.nl_count+1'), array('CompanyOrderItem.id'=>$company_order_id));
                    

					return array('result'=>true);
				} else {
					$this->error_log("Nastala chyba behem ukladani connection ORDER:$company_order_id ,COMPANY:$company_id, CLIENT:$client_id");
					//return array('result'=>false,'message'=>'Nastala chyba behem ukladani connection');
				}
			}else{
				$this->error_log("Klient s ID: $client_id nebyl nalezen");	
				 //return array('result'=>false,'message'=>'Klient s ID:'.$client_id.' nebyl nalezen');
			}
		
	}
    
    
    function multi_add_to_nl(){
        //Configure::write('debug',1);
        //pr($this->data);
        
        $company_order_id = $this->data['order_id'];
		$ccr = $this->CompanyOrderItem->read(array('company_id'), $company_order_id);
        $company_id = $ccr['CompanyOrderItem']['company_id'];
        
        
        /*
		 * Pokud nejsou vsechny vstupni parametry, jdi do dupy
		 */
		if ($company_order_id == null || $company_id == null)
			die(json_encode(array('result'=>false,'message'=>'Nejsou vsechny udaje potrebne pro tuto funkce')));	
        
            /*
    		 * Ma tato skupina uzivatelu pravo pridavat uzivatele na nominacni
    		 * listinu? Pokud ne, tak fuck!!
    		 * Otazka: Co s adminem? 
    		 */
    		$access_group_premission = array('IN'=>9,'IN2'=>41,'EN'=>8);
            $access_superior = array(1,2,7);
    		if (in_array($this->logged_user['CmsGroup']['id'], $access_group_premission)
                ||
                in_array($this->logged_user['CmsGroup']['cms_group_superior_id'], $access_superior)
            ){
            
            $wrong_client = false;
            if(!empty($this->data['clients'])){
                foreach($this->data['clients'] as $client_id){
                    $return = self::single_add_to_nl($company_order_id,$company_id,$client_id);
                    if($return['result'] == false && isset($return['wrong'])){
                        if($wrong_client == '')
                            $wrong_client = "\nTito klienti již jsou umistěni na této objednávce:\n\n";
                        
                        $wrong_client .= $return['message']."\n";
                    }
                }
                
                die(json_encode(array('result'=>true,'wrong_clients'=>$wrong_client)));
            }
            else{
                die(json_encode(array('result'=>false,'message'=>'Nebyli zvoleni zadni klienti!!')));
            }
            
        } else {
			$this->error_log("Neopravneny pristup na tuto funkci");
			die(json_encode(array('result'=>false,'message'=>'Hej kamo, na toto nemas pravo!')));
		}
        
        
    }
    
    function load_profese($company_id = null, $new = false){
			$this->loadModel('CompanyWorkPosition');
			$this->CompanyWorkPosition->bindModel(array('hasOne'=>array('CompanyMoneyValidity')));
			$cwp = $this->CompanyWorkPosition->find(
				'list',
				array(
					'conditions'=>array(
						'CompanyWorkPosition.company_id' => $company_id,
						'CompanyWorkPosition.test'=>0,
                        'CompanyWorkPosition.new'=>($new == 'true' ? 1 : 0),
						'CompanyWorkPosition.kos' => 0,
						'CompanyMoneyValidity.platnost_do' => '0000-00-00'
					),
					'order'=>'CompanyWorkPosition.name ASC',
					'recursive'=>1
				)
			);
        
        /*
		 * Pokud existuji kalkulace s platnosti k teto pozici 
		 */
		if ($cwp == false){
			die(json_encode(array('result'=>false,'data'=>array())));
		} else {
			die(json_encode(array('result'=>true,'data'=>$cwp)));
		}
    }
    
    
    function show_print_forms($company_id = null){
        if($company_id == null)
            die(json_encode(array('result'=>false,'message'=>'Nebyla zvolena firma')));
            
        $this->set('forms_list',$s = $this->get_list('CompanyFormTemplate',array('company_id'=>$company_id,'kos'=>0,'status'=>1)));
        
        $this->render('domwins/print_forms');
    }
    
    function cl_ajax_sort($order_item_id,$sort = 'Client.name ASC',$status = null){
        /*
		 * Natahnuti klientu, kteri jsou na cekaci listine
		 */
        $this->loadModel('ConnectionClientRequirement'); 
        $this->ConnectionClientRequirement->bindModel(array(
            'belongsTo'=>array('Client','CmsUser'),
            'joinSpec'=>array(
                'Countrie'=>array(
                    'className'=>'Countrie',
                    'primaryKey'=>'Countrie.id',
                    'foreignKey'=>'Client.countries_id'
                )
            )
        ));
		$cekaci_listina_list = $this->ConnectionClientRequirement->find('all', 
			array(
				'conditions' => array(
					'requirements_for_recruitment_id' => $order_item_id, 
					'objednavka' =>1,
					'type'=>1
				),
				'fields'=>array(
					'CmsUser.name',
					'Client.name',
                    'Client.mesto',
                    'Client.os_pohovor',
					'Client.mobil1',
					'Client.mobil2',
					'Client.mobil3',
					'Client.mobil_active',
                    'Client.opp_nohavice','Client.opp_bluza','Client.opp_tricko','Client.opp_obuv',
					'Client.id',
					'Client.stav',
                    'Client.countries_id',
					'ConnectionClientRequirement.id',
					'ConnectionClientRequirement.created',
                    'ConnectionClientRequirement.cl_notification',
                    'Countrie.name'
				),
                'order'=>$sort
			)
		);
        $this->set('detail',array('CompanyOrderItem'=>array('order_status'=>$status)));
		$this->set('cekaci_listina_list',$cekaci_listina_list);
        $this->render('tabs/cekaci_listina_item');
    }
    
    
    /**
     * Funkce ktera po kliknuti na tab, nahrava zalozku
     * Ulehcujeme zatizeni a rychlosti nacitani
     * Definitivne pro klient list, zavadime filtaci podle pismen(pokdu jich je vice jak 100)
     */
    function ajax_load_nl($company_id,$order_id,$type,$kvalifikace_id){
        /*
         * Natahnuti klientu, kteri jsou na nominacni listine, tzn. sedi k nim $order_item_id a type 3
         */	
		$this->loadModel('ConnectionClientRequirement');
		$this->ConnectionClientRequirement->bindModel(array(
            'belongsTo'=>array('Client','CmsUser'),
            'joinSpec'=>array(
                'CmsGroup'=>array('className'=>'CmsGroup','primaryKey'=>'CmsGroup.id','foreignKey'=>'CmsUser.cms_group_id')
            )
        ));
		$client_nl_list = $this->ConnectionClientRequirement->find('all', 
			array(
				'conditions' => array(
					'requirements_for_recruitment_id' => $order_id, 
					'objednavka' =>1,
					'type'=>3
				),
				'fields'=>array(
					'CmsUser.cms_group_id',
                    'CmsGroup.cms_group_superior_id',
					'CmsUser.name',
					'Client.name',
					'Client.id',
					'Client.stav',
					'ConnectionClientRequirement.id',
					'ConnectionClientRequirement.created',
                    'ConnectionClientRequirement.cl_notification'
				)
			)
		);
        
		/*
		 * Musi se doresit pretransfmovani na jednotlive typy NL
		 */	
		$seznam_nominacni_listiny = array('cm'=>array(),'in'=>array(),'en'=>array(),'other'=>array());
		//pr($client_nl_list);
        foreach($client_nl_list as $client){
             //nasrazena skupina koordinator
            if($client['CmsGroup']['cms_group_superior_id'] != '' && in_array($client['CmsGroup']['cms_group_superior_id'], array(2))){
                $seznam_nominacni_listiny['cm'][] = $client; 
            }
            else if($client['CmsGroup']['cms_group_superior_id'] != '' && in_array($client['CmsGroup']['cms_group_superior_id'], array(7))){
                $seznam_nominacni_listiny['in'][] = $client; 
            }
            else{		  
    			switch($client['CmsUser']['cms_group_id']){
    				case 1:
    				case 3: 
    				case 4: 
    				case 5: 
    						// cm, admin, koo, director
    						$seznam_nominacni_listiny['cm'][] = $client; break; 
    				case 9: 
                    case 41:    
                        $seznam_nominacni_listiny['in'][] = $client; break; 
    				case 8: $seznam_nominacni_listiny['en'][] = $client; break; 
    				default: $seznam_nominacni_listiny['other'][] = $client; break;
    			}
            }
		}
		$this->set('nominacni_listina_list',$seznam_nominacni_listiny);
        
        
        $podminka = false;
 		/*
 		 * Podminka na to, co jaka skupina vidi za klienty
 		 */
		switch($this->logged_user['CmsGroup']['id']){
			// cm
			case 3:
					$condition = array();
					break;
			// in
			case 9:
            case 41:
					$condition = array('ConnectionClientRecruiter.cms_user_id'=>-1);
					break;
			// ex
			case 8:
					$condition = array('ConnectionClientRecruiter.cms_user_id'=>$this->logged_user['CmsUser']['id']);
					break;
			default:
					$condition = array();
					break;
		}
		
        $sub_query_regexp = ' REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(LOWER(SUBSTRING(Client.name,1,1)), "ď", "d"), "č", "c"), "ř", "r"), "š", "s"), "ž", "z") REGEXP "^[a-k]"';
 		if (count($condition) >0){
 		     /**
 		      * 24.8.2010
              * Pro IN misto podminky IN (..)
              * se vytvori SUB-QUERY
 		      */ 
 		    if(in_array($this->logged_user['CmsGroup']['id'],array(9,41))){
 		         $sub_query = 'ConnectionClientCareerItem.client_id IN (SELECT client_id FROM wapis__connection_client_recruiters WHERE cms_user_id = "-1")';
 		    } 
            else{
                $this->loadModel('ConnectionClientRecruiter');
 			    $seznam_vlastnich_klientu= $this->ConnectionClientRecruiter->find('list',array('conditions'=>$condition,'fields'=>array('id','client_id')));
			}
            $podminka = true;
 		}

		$this->loadModel('ConnectionClientCareerItem');
		$this->ConnectionClientCareerItem->bindModel(array('belongsTo'=>array('Client')));

		//load client listu
		if($podminka){
		    if(in_array($this->logged_user['CmsGroup']['id'],array(9,41))) 
			    $this->set('client_list',$this->ConnectionClientCareerItem->find('all',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,$sub_query,$sub_query_regexp),'fields'=>array('DISTINCT Client.id','Client.name','Client.stav'),'recursive'=>1,'order'=>'Client.name')));
			else
                $this->set('client_list',$this->ConnectionClientCareerItem->find('all',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,'ConnectionClientCareerItem.client_id' => $seznam_vlastnich_klientu,$sub_query_regexp),'fields'=>array('DISTINCT Client.id','Client.name','Client.stav'),'recursive'=>1,'order'=>'Client.name')));
        }
        else	
			$this->set('client_list',$cc = $this->ConnectionClientCareerItem->find('all',array('conditions'=>array('Client.kos'=>0,'ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id,$sub_query_regexp),'fields'=>array('DISTINCT Client.id','Client.name','Client.stav'),'recursive'=>1,'order'=>'Client.name')));
        
        $this->set('access_superior', array(2));
        $cl_list_show = false;
        switch($type){
           case 'cm': 
            $cl_list_show = in_array($this->logged_user['CmsGroup']['id'], array(3,4,1));
           break;   
           case 'in': 
            $cl_list_show = in_array($this->logged_user['CmsGroup']['id'], array(9,41,56,57)); 
           break;   
           case 'en':  
            $cl_list_show = in_array($this->logged_user['CmsGroup']['id'], array(8));
           break;   
        }
        //$this->set('access', $access);
        $this->set('client_list_show',$cl_list_show);
        
        /*
		 * Ti co jsou uz v objednavce namrdani
		 */
		$this->set(
			'pouziti_klienti',
			$this->ConnectionClientRequirement->find(
				'list',
				array(
					'conditions' => array(
						'requirements_for_recruitment_id' => $order_id,
						'OR' => array(
							'type' => array(1,3),
							'and' => array(
								'type'=>2,
								'to'=>'0000-00-00'
							)
						)
					),
					'fields'=>array('id','client_id')
				)
			)
		);
        
        // Nacteni seznamu profesi ze settingu
		$this->set('kvalifikace_list', $this->get_list('SettingCareerItem'));
        $this->set('kvalifikace_id',$kvalifikace_id);
        $this->set('company_id',$company_id);
        $this->set('regexp_show',true);
        $this->set('regexp','a-k');
        $this->set('order_template_id',$order_id);
        $this->set('superior_id',$this->logged_user['CmsGroup']['cms_group_superior_id']);
        $this->set('typ',$type);
        $this->render('tabs/nominacni_listina_new');
    }
    
    function test(){
        
         $this->loadModel('FormData');
       
         $this->loadModel('ConnectionClientRequirement');
         $zam = $this->ConnectionClientRequirement->find('all',array(
            'conditions'=>array(
                'client_id !='=>0,
                'company_id !='=>0
                //'company_id'=>$this->data['ConnectionClientRequirement']['company_id'],
                //'history'=>0,
                //'kos'=>0,
                //'form_template_group_id'=>self::create_form_group_array_conditions($money['CompanyMoneyItem'])
            ),
            'fields'=>array('client_id','GROUP_CONCAT(DISTINCT company_id,"") as comps'),
            'group'=>'client_id'
         ));
        // pr($zam);
       
         foreach($zam as $item){
            //$ff = $this->FormData->query('DELETE FROM wapis__form_datas WHERE client_id = "'.$item['ConnectionClientRequirement']['client_id'].'" AND company_id NOT IN('.$item['0']['comps'].')');
         }
        
         die('done');
    }
         
}
?>