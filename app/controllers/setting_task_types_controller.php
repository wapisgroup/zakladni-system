<?php
class SettingTaskTypesController extends AppController {
	var $name = 'SettingTaskTypes';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingTaskType');
	var $renderSetting = array(
		'controller'=>'setting_task_types',
		'SQLfields' => array('id','name','updated','created','status','poradi'),
		'page_caption'=>'Nastavení typů úkolů',
		'sortBy'=>'SettingTaskType.poradi.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingTaskType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingTaskType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingTaskType|id|text|',
			'poradi'	=>	'Pořadí|SettingTaskType|poradi|text|',
			'name'		=>	'Název|SettingTaskType|name|text|',
			'updated'	=>	'Upraveno|SettingTaskType|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingTaskType|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|delete'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení stavů firem'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingTaskType->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingTaskType->save($this->data);
			die();
		}
	}
}
?>