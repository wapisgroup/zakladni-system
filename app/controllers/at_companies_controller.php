<?php
Configure::write('debug',1);
	define('fields','AtCompany.*,
            ( SELECT GROUP_CONCAT(name SEPARATOR "<br />") from wapis__at_project_companies as atpc LEFT JOIN wapis__at_projects as atp ON(atp.id = atpc.at_project_id) WHERE at_company_id=AtCompany.id AND atpc.kos = 0 GROUP BY at_company_id
			)as projekt,  
            ( 
			  SELECT COUNT(client_id)
			  FROM wapis__connection_client_at_company_work_positions
			  where at_company_id=AtCompany.id and(
				datum_nastupu <= "'. date('Y-m-d').'" AND (datum_to >="'. date('Y-m-d').'" OR datum_to = "0000-00-00")
			  ) 
			) as poc_zamestnancu	
		');
class AtCompaniesController extends AppController {
	var $name = 'AtCompanies';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('AtCompany');
	var $renderSetting = array(
		'SQLfields' =>array(fields),
		 'bindModel' => array(
            //'joinSpec' => array(
            //     'AtProject' => array('className'=>'AtProject','foreignKey' => 'at_project_id')
		    //)
         ),
		'controller'=> 'at_companies',
		'page_caption'=>'Nastavení firem',
		'sortBy'=>'AtCompany.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add'
		),
		'filtration' => array(
            //'AtCompany-project_id'	=>	'select|Projekt|project_list',		 	
		),
		'items' => array(
			'id'					=>	'ID|AtCompany|id|text|',
			'ico'					=>	'IČO|AtCompany|ico|text|',
            'name'					=>	'Název|AtCompany|name|text|',
            'projekt'		        =>	'Projekt|0|projekt|text|',	
            'poc_zamestnancu'		=>	'Počet zaměstanců|0|poc_zamestnancu|text|',
			//'manager'	   	        =>	'Správce|Manager|name|text|', 
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',	
            'sharing_company'		=>	'sharing_company|Sdílení pro projekt|sharing_company',		
			'delete'	=>	'trash|Odstranit položku|trash',
		),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
			'defined_lang'	=> "['cz','en','de']",
		),
        'only_his'=>array(
            'in_col'=>'id',
            'subQuery'=>'(SELECT at_company_id FROM wapis__at_project_companies as atpc LEFT JOIN wapis__at_projects as atp ON(atp.id = atpc.at_project_id) Where atp.manager_id = #CmsUser.id# AND atpc.kos = 0)'
        )
	);
	



	/**
 	* Returns a view, if not AJAX, load data for basic view. 
 	*
	* @param none
 	* @return view
 	* @access public
	**/
	function index(){
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'#','Nastavení firem'=>'#'));

            //list projektu
            $this->set('project_list',$this->get_list('AtProject'));	
			
			$this->render('../system/index');
		}
	}
	
	/**
 	* Returns a view if empty this->data, ales JSON result of save
 	*
	* @param $id The number of ID column
 	* @return view / JSON
 	* @access public
	**/
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
		    if ($id != null){
				$this->data = $this->AtCompany->read(null,$id);
				
				// load province list
				$this->loadModel('Province'); 
				$this->set('province_list',$this->Province->find('list',array('conditions'=>array('stat_id'=>$this->data['AtCompany']['stat_id']))));
				unset($this->Province );
                
               	// load country list
                if (isset($this->data['AtCompany']['province_id']) && !empty($this->data['AtCompany']['province_id'])){
				    $this->loadModel('Countrie'); 
				    $this->set('country_list',$this->Countrie->find('list',array('conditions'=>array('province_id'=>$this->data['AtCompany']['province_id']))));
				    unset($this->Countrie);
                }
            
                $this->set('company_id',$id);
			} else {
				$this->loadModel('Province');
				$this->set('province_list',$this->Province->find('list',array('conditions'=>array('stat_id'=>1))));		
                unset($this->Province );	
			} 
            // load stat list
			$this->set('company_stat_list',$this->get_list('SettingStat'));
			$this->render('edit');
		} else {
			$this->AtCompany->save($this->data);
			die();
		}
	}
    
    function sharing_company($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
		    $this->data['AtProjectCompany']['at_company_id'] = $id;
            
            $this->loadModel('AtProjectCompany');
            $connections = $this->AtProjectCompany->find('list',array('conditions'=>array('at_company_id'=>$id,'kos'=>0),'fields'=>array('at_project_id','id')));	
            $this->data['AtProjectCompany']['projects'] = $connections;    	
            
            $con = array(
                'kos'=>0
            );
            
            if($this->logged_user['CmsGroup']['cms_group_superior_id'] != 1){
                $con = array(
                    'kos'=>0,
                    'OR'=>array(
                        'manager_id'=>$this->logged_user['CmsUser']['id'],
                        'office_manager_id'=>$this->logged_user['CmsUser']['id']
                    )
                );
            }
            
		    $this->set('project_list',$this->get_list('AtProject',$con));
		} else {//SAVE
		         if(isset($this->data['AtProjectCompany']['projects'])){
                    $this->loadModel('AtProjectCompany');
                	$finds_co = $this->AtProjectCompany->find('list',array('conditions'=>array('at_company_id'=>$this->data['AtProjectCompany']['at_company_id']),'fields'=>array('at_project_id','id')));	
                	//vsechny pridame do kose
                    $this->AtProjectCompany->query('UPDATE wapis__at_project_companies SET kos=1 WHERE at_company_id = '.$this->data['AtProjectCompany']['at_company_id']);
                    
                    foreach($this->data['AtProjectCompany']['projects'] as $project_id=>$result){
    	            	        if($result != 1)
                                    continue;   
                          
    	            	        if(array_key_exists($project_id,$finds_co)){ $this->AtProjectCompany->id = $finds_co[$project_id]; }
                                $data['kos'] = 0; // stavjici a nove prirazujeme s kosem 0
    	            	        $data['at_project_id'] = $project_id;
    	            			$data['at_company_id'] = $this->data['AtProjectCompany']['at_company_id'];
                         
                        		if (!$this->AtProjectCompany->save($data)){
    		            			die(json_encode(array('result'=>false, 'message'=>'Chyba behem ukladani modelu ')));
    		            		}
    		            		$this->AtProjectCompany->id = null; 
                	}
                    unset($this->AtProjectCompany);
                }
			die(json_encode(array('result'=>true)));
        }
  }
  
  /**
   * Zjistuji jaky stat je dana firma
   */
   function check_stat_id($at_company_id = null){
        if($at_company_id != null){
            $comp = $this->AtCompany->read(array('stat_id'),$at_company_id);
            die(json_encode(array('result'=>true,'stat_id'=>$comp['AtCompany']['stat_id'])));
        }
        else
            die(json_encode(array('result'=>false,'message'=>'Chyba, ID firma je prazdne')));
   }     
}
?>