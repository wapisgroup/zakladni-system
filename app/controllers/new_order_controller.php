<?php 
Configure::write('debug',1);
class NewOrderController extends AppController
{
    var $name = 'NewOrder';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('RequestHandler','Email','NewOrder','Pagination');
	var $uses = array('CompanyOrderItem','ClientView');    
    var $renderSetting = array(
        'controller'=> 'new_order',
        'orders'=>array(
            'filtration_url'=>'/new_order/set_items/orders/1/',
            'filtration_update'=>'items_orders',
            'filtration_button_br'=>true,
            'tr_id'=>array(
                'show'=>true,
                'model'=>'CompanyOrderItem',
                'col'=>'id'
            ),
            'modelClass'=>'CompanyOrderItem',
    		//'SQLfields' => array('id','Company.name','updated','created','status','SalesManager.name','ClientManager.name','Coordinator.name','mesto','SettingStav.namre'),
    		'SQLfields' => '*,
                CONCAT_WS(" + ",CompanyOrderItem.count_of_free_position,CompanyOrderItem.count_of_substitute) as objednavka,
                CONCAT_WS(" / ",CompanyOrderItem.count_of_free_position,
                    IF(
                     CompanyOrderItem.order_status = 1,
                        (SELECT 
                            COUNT(c.id) 
                         FROM wapis__connection_client_requirements as c
                         Where 
                            c.company_id = CompanyOrderItem.company_id AND 
                            c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                            type = 2 AND
                            objednavka = 1
                         ),
                     CompanyOrderItem.statistika
                    )
                ) as statistika,
                IF((CompanyOrderItem.start_datetime < NOW() && CompanyOrderItem.order_status IN (-1,1)), 1 , 0) as expire                
            ',
            'SQLcondition'=>array(
                'order_status'=>array(-1,1)
            ),		
    		'bindModel' => array('belongsTo' => array(
                'Company'=>array('foreignKey'=>'company_id')
    		)),
    		'controller'=> 'new_order',
    		'page_caption'=>'Obsazeni objednávek',
    		'sortBy'=>'CompanyOrderItem.order_date.DESC',
    		'top_action' => array(),
    		'filtration' => array(
                'CompanyOrderItem-company_id'	=>	'select|Společnost|company_list',      
            ),
    		'items' => array(
    			'id'				=>	'ID|CompanyOrderItem|id|text|',
    			'firma'				=>	'Firma|Company|name|text|',
    			'order'				=>	'Profese|CompanyOrderItem|name|text|',	
    			'order_date'		=>	'Datum obj|CompanyOrderItem|order_date|date|',
    			//'start_datetime'	=>	'Datum nástupu|CompanyOrderItem|start_datetime|date|',
    			//'objednavka'		=>	'Objednávka|0|objednavka|text|',
    			//'statistika'		=>	'Statistika|0|statistika|text|',
    			//'start_comment'		=>	'Komentář|CompanyOrderItem|start_comment|text|orez#30',
                'status'			=>	'Status|CompanyOrderItem|order_status|var|order_stav_list',
                //'created'			=>	'Vytvořeno|CompanyOrderItem|created|datetime|',
                //'nabor'			    =>	'Nábor|CompanyOrderItem|nabor|text|value_to_yes_no#long'
    		),
            'class_tr'=>array(
                'class'=>'color_red',
                'model'=>0,
                'col'=>'expire',
                'value'=>1
            ),
    		'posibility' => array(
    			//'pozice'		=>	'nabor_pozice|Obsazení pracovních pozic|pozice',
    			//'show'		    =>	'show|Detail šablony|show'
    		),
    		'domwin_setting' => array(
    			'sizes' 		=> '[1000,1000]',
    			'scrollbars'		=> true,
    			'languages'	=> 'false'
    		)
       ),
       'clients'=>array(
            'filtration_url'=>'/new_order/set_items/clients/1/',
            'filtration_update'=>'items_clients',
            'filtration_button_br'=>true,
            'filtration_next_button'=>array(
                0 =>array(
                    'label'=>'',
                    'options'=>array('id'=>'move_client_to_order','class'=>'ta status_new1')
                )
            ),
            'tr_id'=>array(
                'show'=>true,
                'model'=>'ClientView',
                'col'=>'id'
            ),
            'modelClass'=>'ClientView',
    		'permModel' => 'ConnectionClientRecruiter',
            'bindModel' => array(
                'hasOne' => array(
                    'ConnectionClientCareerItem' => array('foreignKey' =>       'client_id'), 
                    'ConnectionClientRecruiter' => array('className' => 'ConnectionClientRecruiter', 'foreignKey' => 'client_id'),
                    'ConnectionClientRequirement' => array('className' =>     
                        'ConnectionClientRequirement', 'foreignKey' => 'client_id',
                         'conditions' =>        array(
                            'ConnectionClientRequirement.kos' => 0,        	'ConnectionClientRequirement.to'=>'0000-00-00',
                             '(ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1)'
                         )
                    )
                )
            ),
                //'SQLfields' => '*,GROUP_CONCAT(profese_name SEPARATOR "<br/>") as Profese ',
            'SQLfields' => '*', 
            'controller' => 'new_order', 
            'page_caption' => 'Klienti',
                //'count_group_by' => 'ClientView.id',
            'group_by' => 'ClientView.id', 
            'sortBy' => 'ClientView.id.ASC', 
            'top_action' =>array( 
                // caption|url|description|permission
                'add_item' => 'Přidat|edit|Pridat popis|add', 
                'add_express_item' => 'Přidat Express|edit_express|Pridat popis|add_express', 
                'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel', 
            ), 
            'filtration' => array(
                'ClientView-name|' => 'text|Jméno|',
    	        'ClientView-pohlavi_list' => 'select|Pohlaví|pohlavi_list',  
    	        'ClientView-datum_narozeni|year' => 'year|Rok narození|',  
                'ClientView-stav' => 'select|Status|stav_client_list',
                'ConnectionClientCareerItem-setting_career_item_id' => 'select|Profese|profese_list',
                'ConnectionClientRecruiter-cms_user_id#admin_ctrl' => 'select|Recruiter|cms_user_list', 
                'ConnectionClientRequirement-company_id' => 'select|Firma|company_list',
                'ClientView-next|'		=>	'select|Další|next_list',
                'ClientView-mesto|' => 'text|Město|', 
                'ClientView-countries_id' => 'select|Okres|countries_list',  
               
                //'ClientView-telefon1|'	=>	'text|Telefon|',
            ), 
            'items' => array(
                'client_manager_id' => 'cmid|ClientView|client_manager_id|hidden|', 
                'id' => 'ID|ClientView|id|text|',
                'name' => 'Jméno|ClientView|name|text|', 
                'profese' => 'Profese|ClientView|Profese|text|',
                'mobil' => 'Telefon|ClientView|mobil|text|',
                //'mesto' => 'Město|ClientView|mesto|text|',
                //'datum_narozeni' => 'Datum narození|ClientView|datum_narozeni|date|',
                'status' => 'Status|ClientView|stav|var|stav_client_list', 
                //'datum_nastupu' => 'Datum nástupu|ConnectionClientRequirement|from|date|', 
                //'firma' => 'Firma|ClientView|company|text|', 
                //'cm' => 'CM|ClientView|client_manager|text|',
                //'coo' => 'COO|ClientView|coordinator|text|', 
                //'coo2' => 'COO2|ClientView|coordinator2|text|', 
                //'status_imp' => 'StatusImport|ClientView|import_stav|var|stav_importu_list',
                //'externi_nabor' => 'Typ|ClientView|externi_nabor|var|client_en_type',
                //	'updated'	=>	'Změněno|ClientView|updated|datetime|',
                //'created' => 'Vytvořeno|ClientView|created|datetime|',
                //'express' => '#|ClientView|express|text|status_to_ico#express'
            ),     
            'posibility' => array(
                'edit' => 'edit|Editace položky|edit', 
                //'attach' => 'attachs|Přílohy|attach', 
               // 'message' => 'messages|Zprávy|message', 
               // 'vyhodit' => 'rozvazat_prac_pomer|Rozvázat pracovní poměr|rozvazat_prac_pomer', 
               // 'zmena_pp' => 'zmena_pp|Změna pracovního poměru|zmena_pp', 
               // 'stats' => 'stats|Statistiky spojení|stats',
               // 'domwin_add_activity'	=>	'domwin_add_activity|Přidání aktivity|domwin_add_activity',
               // 'delete' => 'trash|Odstranit položku|trash',
                 
            ), 
            'domwin_setting' => array(
                'sizes' => '[1000,1000]', 
                'scrollbars' => true, 
                'languages' => true 
            )
       )      
	);
	 
     function beforeFilter(){
        parent::beforeFilter();
        
        /**
         * povolit klienty i EN
         */
        $this->ClientView->set_ignore_status(true); 
               
        
        if(isset($_GET['filtration_ClientView-next|']) && !empty($_GET['filtration_ClientView-next|'])){
            list($col,$value) = explode('-',$_GET['filtration_ClientView-next|']);

            $this->params['url']['filtration_ClientView-'.$col] = $value;
            
            unset($this->params['url']['filtration_ClientView-next|']); 
            unset($col);
            unset($value);
        }

      
        //pr($_GET);    
    } 
    
    function index(){
        $this->layout = 'order';
        
        $this->set('company_list',$this->get_list('Company'));
        
        //list obci
        $this->set('countries_list',$this->get_list('Countrie'));
        //profese
        $this->set('profese_list',$this->get_list('SettingCareerItem'));
        
        $this->set('cms_user_list',$this->get_list('CmsUser'));
       /** 
        * dalsi moznosit filtrace
        * moznsosti zadavame sloupec-stav => nazev moznosti
        */
        $this->set('next_list',array(
            'express-1'=> 'Expresní klienti',
            'import_adresa-NOTNULL'=> 'Neprázdna importní adresa',
            'email-NOTNULL'=> 'Neprázdna emailova adresa',
            'externi_nabor-0'=> 'Interní nábor',
            'externi_nabor-1'=> 'Externí nábor'
        ));
        
        // set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
        
        self::set_items('orders');
        self::set_items('clients');
   
    }
 
 
    function set_items($name = null,$render = false){
        if($name == null)
            die('chybny parametr');
        
        $model_class = ($name == 'orders') ? $this->uses[0] : $this->uses[1];
        $renderSetting = $this->renderSetting[$name];
        

        $criteria = $this->NewOrder->filtration($name);
        
        if(!isset($renderSetting['no_trash']))
            $criteria[$model_class.'.kos'] = 0;
		if(isset($renderSetting['count_condition'])){
			$count_criteria = $criteria;
			$count_criteria[] = $renderSetting['count_condition'];
			
		}
			
            
        /**
         * Nastavnei count sloupce, pokud je nastaven v rendersettingu 
         */    
        if(isset($renderSetting['count_col']))
            $this->Pagination->count_col = $renderSetting['count_col'];
	
		$this->ScaffoldModel = $this->{$model_class};
        
        if($render === false)
		  $this->Pagination->paging_name = 'paging_'.$name;
		
        $this->Pagination->ajaxDivUpdate = 'items_'.$name;
			
		$this->Pagination->show = isset($renderSetting['sortShow'])?$renderSetting['sortShow']:20;
		list($field,$model,$direction) = explode('.',isset($renderSetting['sortBy'])?$renderSetting['sortBy']:$model_class.'.name.ASC');
		

		$this->Pagination->recursive = 2;
		$this->Pagination->sortBy = $model;
		$this->Pagination->sortByClass = $field;
		$this->Pagination->direction = $direction;
		
		$this->Pagination->url = '/'.(isset($renderSetting['controller'])?$renderSetting['controller']:$this->controller->params['url']['url']).'/set_items/'.$name.'/1/';
		$this->Pagination->controller = $this;
		if (isset($renderSetting['bindModel']))
			$this->ScaffoldModel->bindModel($renderSetting['bindModel']);
		
		
		if(!isset($renderSetting['bindModel']['hasOne'])) 	$renderSetting['bindModel']['hasOne'] = array();
		if(!isset($renderSetting['bindModel']['belongsTo'])) 	$renderSetting['bindModel']['belongsTo'] = array();
		if(!isset($renderSetting['bindModel']['hasMany'])) 	$renderSetting['bindModel']['hasMany'] = array();
		

		$pagination_bind = $renderSetting['bindModel'];
		
		list($order,$limit,$page) = $this->Pagination->init((isset($count_criteria) ? $count_criteria : $criteria),null,array('modelClass'=>$this->ScaffoldModel->name)); 
		
		if (isset($renderSetting['SQLhaving'])){
			if (is_array($renderSetting['SQLhaving'])){
				$criteria[] = '1=1 Having '.implode(' AND ', $renderSetting['SQLhaving']);
			} else {
				$criteria[] = '1=1 Having '.$renderSetting['SQLhaving'];
			}
		}	

		if (isset($renderSetting['bindModel']))
			$this->{$model_class}->bindModel($renderSetting['bindModel']);
		
			
        if(isset($renderSetting['no_limit']))
            $limit = null;    
            
		$items = $this->{$model_class}->find(
				'all', 
				array(
					'conditions'	=> $criteria, 
					'fields'		=> $renderSetting['SQLfields'], 
					'order'			=> $order, 
					'limit'			=> $limit, 
					'page'			=> $page,
					'recursive' 	=> 1,
					'group'			=> (isset($renderSetting['group_by'])?$renderSetting['group_by']:null)
				)
			);
			
	
        if($render == true){
            $this->set('items',$items);
            $this->set('renderSetting',$renderSetting);
            
            $this->render('items');
        }
        else{
            $this->set('items_'.$name,$items);
        }
        
    }
    
    /**
     * Editacni karta klienta
     */
    function edit($id = null,$domwin = null, $show = null){
		echo $this->requestAction('clients/edit/'.$id.'/'.$domwin.'/'.$show);
		die();
	}
    
}
?>