<?php
Configure::write('debug',1);

$_GET['company_id'] 	= (isset($_GET['filtration_ConnectionClientRequirement-company_id']) && !empty($_GET['filtration_ConnectionClientRequirement-company_id']))?$_GET['filtration_ConnectionClientRequirement-company_id']:'16';
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:date('m');
define('COMPANY_ID', $_GET['company_id']); 
define('CURRENT_YEAR', $_GET['current_year']); 
define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);


class PrintEmployeesController  extends AppController {
	var $name = 'PrintEmployees';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Excel');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionClientRequirement');
	var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','Company','CompanyWorkPosition','CompanyMoneyItem'),
			'hasOne'	=>	array(
				'ClientWorkingHour'=>array(
					'conditions' => array(
						'ClientWorkingHour.year'=> CURRENT_YEAR,
						'ClientWorkingHour.month'=> CURRENT_MONTH,
						'ClientWorkingHour.company_id'=> COMPANY_ID
					)
				)
			)
		),
		'controller'	=>	'print_employees',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			'ConnectionClientRequirement.type'=> 2,
			'ConnectionClientRequirement.company_id'=> COMPANY_ID
		),
		'page_caption'=>'Tisk docházky zaměstnanců',
		'sortBy'=>'Client.name.ASC',
		'top_action' => array(
			'print'		=>	'Tisknout|print|Tisknount|print',
            'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',   
		),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
            'GET-current_month'							=>	'select|Měsíc|mesice_list',
			'CompanyWorkPosition-name|'			=>	'text|Pozice|',	
			'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
            'GET-current_year'							=>	'select|Rok|actual_years_list',
        ),
		'items' => array(
			'id'				=>	'ID|ConnectionClientRequirement|id|hidden|',
            'client'			=>	'Klient|Client|name|text|',
			'company'			=>	'Společnost|Company|name|text|',
			'norhod'			=>	'Normo hodina|ClientWorkingHour|standard_hours|text|',
			'svatky'			=>	'Svátky|ClientWorkingHour|svatky|text|',
			'vikendy'			=>	'Víkendy|ClientWorkingHour|vikendy|text|',
			'pn'				=>	'PN|ClientWorkingHour|pracovni_neschopnost|text|',
			'dovolena'			=>	'Dovolená|ClientWorkingHour|dovolena|text|',
			'prescasem'			=>	'Přesčas|ClientWorkingHour|prescasy|text|',
			'celkem'			=>	'Celkem|ClientWorkingHour|celkem_hodin|text|',
		
		),
		'posibility' => array(	),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		),
        'no_checkbox'=>true,
        'no_limit'=>true
	);

	

	function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		  $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}
    
    function export_excel(){
		$link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&excel=true';
		$this->redirect($link);
		exit;
	}
	
	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#', $this->renderSetting['page_caption'] =>'#'));
				
		$company_conditions = array(
			 "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".CURRENT_YEAR."-".CURRENT_MONTH."'))",
			 '((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			 'ConnectionClientRequirement.type'=> 2
             
		);
		
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		
        $this->ConnectionClientRequirement->unbindModel(array('hasOne'=>array('ClientUcetniDokumentace')));
		$this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Company'))); 
		$company_list = $this->ConnectionClientRequirement->find('list',array(
			 'conditions'=> $company_conditions,
			 'fields'=>array('Company.id','Company.name'),
			 'recursive'=>1
		 ));
		 $this->set('company_list',$company_list);
		 $this->set('month',CURRENT_MONTH);
		 $this->set('year',CURRENT_YEAR);
         
         // load free days for state, month, year
		$this->loadModel('SettingStatSvatek');
		$this->set('svatky',$this->SettingStatSvatek->find('list', array('conditions'=>array( 'setting_stat_id'	=> av(av(COMPANY_ID,'Company'),'stat_id'), 'mesic'=> CURRENT_MONTH, 'rok'=> CURRENT_YEAR ),'fields'=>array('id','den'),'order'=>'den ASC'))); 				
		
		if (isset($_GET['excel'])){
			$this->autoLayout = false;
			echo $this->render('excel');
			die();
		} 
		
		if ($this->RequestHandler->isAjax()){
			$this->set('change_list_js', json_encode(array('filtr_ConnectionClientRequirement-company_id'=>$company_list)));
			$this->render('items');
		} else {
			$this->render('index');
		}
		
	}
	

}
?>