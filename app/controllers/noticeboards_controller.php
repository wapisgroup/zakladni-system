<?php
class NoticeboardsController extends AppController {
	var $name = 'Noticeboards';
	var $helpers = array('htmlExt','Pagination');
	var $uses = array();
	 
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Pomocník'=>'#','Nástěnka'=>'#'));
		$this->loadModel('CmsUser');
		$this->CmsUser->query('SET NAMES UTF8');
		// SM vs company
		$this->CmsUser->bindModel(array('hasOne'=>array('Company'=>array('foreignKey'=>'self_manager_id'))));
		$pocty_firem_pro_sm = 	$this->CmsUser->find(
				'all',
				array(
					'fields'=>array(
						'CmsUser.name',
						'COUNT(Company.id) AS pocet_firem',
						'(SELECT COUNT(*) FROM `wapis__companies` AS CompanyS WHERE CompanyS.self_manager_id = CmsUser.id AND setting_stav_id IN (6,7,10)) AS pocet_firem_se_smlouvou',
						'(SELECT COUNT(*) FROM `wapis__companies` AS CompanySZ WHERE CompanySZ.self_manager_id = CmsUser.id AND setting_stav_id IN (6)) AS pocet_firem_se_smlouvou_a_zamestnanci',
						'(SELECT COUNT(Contact.id) FROM `wapis__cms_users` as CmsUserC LEFT JOIN wapis__companies AS CompanyC ON (CmsUserC.id = CompanyC.self_manager_id) LEFT JOIN wapis__company_contacts AS Contact ON(Contact.company_id = CompanyC.id) where CmsUserC.id = CmsUser.id) as pocet_kontaktu'
						//'(SELECT COUNT(*) FROM wapis__company_contacts AS Contact WHERE Contact.company_id = Company.id) as pocet_kontaktu'
					) , 
					'conditions'=>array(
						'CmsUser.cms_group_id'=>2,
						'1=1 GROUP BY CmsUser.id'
					)
				)
			);
		
		$this->set('pocty_firem_pro_sm', $pocty_firem_pro_sm);
		
		// SM,CM, KOO vs activity
		$this->CmsUser->bindModel(array(
			'belongsTo' => array('CmsGroup'),
			'hasOne'=>array('CompanyActivity'=>array('foreignKey'=>'cms_user_id'))
		));
		
		$pocty_aktivit_pro_users = $this->CmsUser->find('all',array(
					'conditions'=>array(
						'CmsUser.cms_group_id'=> array(2,3,4),
						'1=1 GROUP BY CmsUser.id'
					),
					'fields'=>array(
						'CmsUser.name',
						'CmsGroup.name',
						'COUNT(CompanyActivity.id) AS pocet_aktivit'
					),
					'order'=> array(
						'CmsGroup.name ASC',
						'pocet_aktivit DESC'
					)
				)
			);
		$this->set('pocty_aktivit_pro_users',$pocty_aktivit_pro_users);

	}

	
}
?>