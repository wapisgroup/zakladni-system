<?php
Configure::write('debug',1);
class SettingStatsController extends AppController {
	var $name = 'SettingStats';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingStat');
	var $renderSetting = array(
		'controller'=>'setting_stats',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'Definice států',
		'sortBy'=>'SettingStat.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingStat-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingStat-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingStat|id|text|',
			'name'		=>	'Název|SettingStat|name|text|',
			'updated'	=>	'Upraveno|SettingStat|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingStat|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|delete',			
			'svatky'	=>	'svatky|Definice svátků|edit',
			'zobsvatky'	=>	'show|Zobrazení svátků|edit',			
			'pocet_dni'	=>	'pocet_dni|Definice počtů dnu pro roka měsíc|edit'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[550,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení stavů firem'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingStat->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingStat->save($this->data);
			die();
		}
	}
	
	
	
	function svatky($stat_id){
		$this->data['SettingStatSvatek']['setting_stat_id'] = $stat_id;
		$this->render('svatky_index');
	}
	
	function svatky_refresh($stat_id, $year =null, $month=null){
		if ($year == null || $month == null) die('Zvolte platné datum');
		$this->set('thismonth', $month);
		$this->set('thisyear', $year);
		$this->loadModel('SettingStatSvatek');
		$data_list = $this->SettingStatSvatek->findAll(array('setting_stat_id' => $stat_id, 'rok'=>$year, 'mesic'=>$month )); 		
		$data = array();
		foreach($data_list as $item){
			$data['SettingStatSvatek']['day'][$item['SettingStatSvatek']['den']] = 1;
		}
		$this->data = $data;
		$this->render('svatky');
	}
	
	function svatky_save(){
		$dny = array_keys($this->data['SettingStatSvatek']['day'],1);
		$rok = $this->data['SettingStatSvatek']['year'];
		$mesic = ($this->data['SettingStatSvatek']['month']<10)?'0'.$this->data['SettingStatSvatek']['month']:$this->data['SettingStatSvatek']['month'];
		$this->loadModel('SettingStatSvatek');
		
		$this->SettingStatSvatek->deleteAll(array(
			'setting_stat_id' => $this->data['SettingStatSvatek']['setting_stat_id'],
			'rok'=>$rok,
			'mesic'=>$this->data['SettingStatSvatek']['month']
		));
		
		foreach($dny as $den){
			$old_den = $den;
			if ($den < 10) $den = '0'.$den;
			$to_save['SettingStatSvatek'] = array(
				'setting_stat_id' => $this->data['SettingStatSvatek']['setting_stat_id'],
				'datum'=>$rok.'-'.$mesic.'-'.$den,
				'rok'=>$rok,
				'den'=>$old_den,
				'mesic'=>$this->data['SettingStatSvatek']['month'],
				'type'=>1
			);
			$this->SettingStatSvatek->save($to_save);
			$this->SettingStatSvatek->id =null;
		}
		die(json_encode(array('result'=>true)));
	}

	function show($stat_id){		
		$this->set('stat_id', $stat_id);
		$this->render('show');
	}

	function show_item($stat_id, $year){	
		$this->loadModel('SettingStatSvatek');
		$this->set('svatky_list', $this->SettingStatSvatek->findAll(array('setting_stat_id' => $stat_id, 'rok'=>$year ),null,'datum')); 		
		$this->render('show_item');
	}
	
	
	function pocet_dni($stat_id){
		$this->autoLayout = false;
		$this->set('stat_id', $stat_id);
		$this->loadModel('SettingStatPd');
		$this->set('pocet_dni_list',$this->SettingStatPd->find('all',array(
			'conditions'=>array(
				'setting_stat_id' => $stat_id,
				'kos'=>0
			)
		))); 		
		
		$this->render('pocet_dni');
	}
	
	function pocet_dni_edit($stat_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('SettingStatPd'); 
		if (empty($this->data)){
			$this->data['SettingStatPd']['setting_stat_id'] = $stat_id;
			if ($id != null){
				$this->data = $this->SettingStatPd->read(null,$id);
			}
			$this->render('pocet_dni_edit');
		} else {
			$this->SettingStatPd->save($this->data);
			$this->pocet_dni($this->data['SettingStatPd']['setting_stat_id']);
		}
		unset($this->SettingStatPd);
	}

	function pocet_dni_trash($stat_id,$id){
		$this->loadModel('SettingStatPd');
		$this->SettingStatPd->id = $id;
		$this->SettingStatPd->saveField('kos',1);
		$this->pocet_dni($stat_id);
		unset($this->SettingStatPd);
	}
	
}
?>