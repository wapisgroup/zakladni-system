<?php
Configure::write('debug',1);

class AllocationWorkingItemsController  extends AppController {
	var $name = 'AllocationWorkingItems';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('ClientWorkingItem');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array(
                'Client'=>array('foreignKey'=>'client_id'),
                'CmsUser','ConnectionClientRequirement',
                
            ),
            'joinSpec'=>array(
                'Company'=>array(
                    'primaryKey'=>'Company.id',
                    'foreignKey'=>'ConnectionClientRequirement.company_id'
                ),
                'CompanyMoneyItem'=>array(
                    'primaryKey'=>'CompanyMoneyItem.id',
                    'foreignKey'=>'ConnectionClientRequirement.company_money_item_id'
                )
            )
            
		),
		'controller'	=>	'allocation_working_items',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(

        ),
		'page_caption'=>'Přidělování pracovních pomůcek',
		'sortBy'=>'ClientWorkingItem.id.DESC',
		'top_action' => array(
            'add_item' => 'Přidat|edit|Pridat popis|add', 
        ),
		'filtration' => array(
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
			'ClientWorkingItem-pomucky'	=>	'select2|Typ|activity_pomucky_list',
			'ClientWorkingItem-cms_user_id'		=>	'select|CM/KOO|cm_koo_list',
            'ClientWorkingItem-client_id'		=>	'select|Client|client_list'
        ),
		'items' => array(
            'client'			=>	'Klient|Client|name|text|',
            'company'			=>	'Společnost|Company|name|text|',
            'money_item'		=>	'Forma|CompanyMoneyItem|name|text|',
            'nastup'			=>	'Datum nástupu|ConnectionClientRequirement|from|date|',
			'ukonceni'		    =>	'Datum ukončení|ConnectionClientRequirement|to|date|',
            'cms_user'			=>	'Přiřadil|CmsUser|name|text|',
            'pomucky'			=>	'Typ|ClientWorkingItem|pomucky|var|activity_pomucky_list',
            'created'           =>  'Datum přidělení|ClientWorkingItem|created|datetime|',
		),
		'posibility' => array(
			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[500,300]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);

	function index(){
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Nábor'=>'#',$this->renderSetting['page_caption']=>'#'));
	    $this->loadModel('CmsUser');
		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
              
  		$this->loadModel('Company');
		$company_list = $this->Company->find('list',array(
			 'conditions'=> array('kos'=>0,'status'=>1),
			 'order'=>'name ASC'
		 ));
		 $this->set('company_list',$company_list);
		 
         /**
             * nacteni listu aktivnich zamestnanců
             */
            $this->loadModel('ConnectionClientRequirement');
            $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('Client'=>array('foreignKey'=>'client_id'))
            ));
    		$client_list = $this->ConnectionClientRequirement->find('list',array(
    			 'conditions'=> array(
                    'ConnectionClientRequirement.kos'=>0,
                    'to'=>'0000-00-00',
                    'type'=>2
                 ),
                 'fields'=>array('Client.id','Client.name'),
    			 'group'=>'client_id',
                 'recursive'=>1,
                 'order'=>'Client.name ASC'
    		 ));
    		 $this->set('client_list',$client_list);
               
        /**
         * kompletni forma odmeny
         */
		foreach($this->viewVars['items'] as &$item){
		  //pr($item);
         	//nastaveni forem odmeny a zobrazeni ubytovani a dopravy
			if(isset($item['CompanyMoneyItem']) && $item['CompanyMoneyItem']['name'] != ''){
					$doprava = 'Doprava '.($item['CompanyMoneyItem']['doprava'] != 0 ? 'Ano' : 'Ne');
					$ubytovani = 'Ubytování '.($item['CompanyMoneyItem']['cena_ubytovani_na_mesic'] != 0 ? 'Ano' : 'Ne');
	                $item['CompanyMoneyItem']['forma'] = $item['CompanyMoneyItem']['name'];
					$item['CompanyMoneyItem']['name'] = $item['CompanyMoneyItem']['name'].' - '.$ubytovani.' / '.$doprava;     
			}
		}
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
    
    function edit($no_save = 0){        
        if(empty($this->data)){
            
            /**
             * nacteni listu aktivnich zamestnanců
             */
            $this->loadModel('ConnectionClientRequirement');
            $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('Client'=>array('foreignKey'=>'client_id'))
            ));
    		$client_list = $this->ConnectionClientRequirement->find('list',array(
    			 'conditions'=> array(
                    'ConnectionClientRequirement.kos'=>0,
                    'to'=>'0000-00-00',
                    'type'=>2
                 ),
                 'fields'=>array('Client.id','Client.name'),
    			 'group'=>'client_id',
                 'recursive'=>1,
                 'order'=>'Client.name ASC'
    		 ));
    		 $this->set('client_list',$client_list);
             
             $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('Client'=>array('foreignKey'=>'client_id'))
             ));
             $client_list_title = $this->ConnectionClientRequirement->find('list',array(
    			 'conditions'=> array(
                    'ConnectionClientRequirement.kos'=>0,
                    'to'=>'0000-00-00',
                    'type'=>2
                 ),
                 'fields'=>array('Client.id','ConnectionClientRequirement.id'),
    			 'group'=>'client_id',
                 'recursive'=>1,
                 'order'=>'Client.name ASC'
    		 ));
    		 $this->set('client_list_title',$client_list_title);
        }
        else {
            /**
             * posila se parametrem, jedna se o slepou funkci pro zahyceni dat historii
             */
            if($no_save == 1)
                die(); 
            
            $this->data['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            
            if(isset($this->data['_pomucky']))
                foreach($this->data['_pomucky'] as $id=>$check){
                    if($check == 1 || $check == 'on'){
                        $this->ClientWorkingItem->id = null;
                        $this->data['pomucky'] = $id;
                        $this->ClientWorkingItem->save($this->data);
                        //self::record_to_history($this->data['client_id'],$id);
                        echo $this->requestAction('allocation_working_items/record_to_history/'.$this->data['client_id'].'/'.$id.'/');
                    }
                }
               die(json_encode(array('result'=>true)));
        }

        $this->render('edit');
    }
	
    /**
     * Slepa funkce pro historii, protoze ty pomucky pridavame najednou
     */
    public function record_to_history($client_id,$pomucka){
        
    }
}
?>