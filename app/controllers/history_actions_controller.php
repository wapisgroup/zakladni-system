<?php
//Configure::write('debug',1);
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 12.11.2009
 */

class HistoryActionsController extends AppController {
	var $name = 'HistoryActions';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','SmsClient');
	var $uses = array('HistoryAction');
	var $renderSetting = array(
		'SQLfields' => array('*'),
		'controller'=> 'history_actions',
		'page_caption'=>'Historie - aplha version',
		'sortBy'=>'HistoryAction.created.DESC',
		'top_action' => array(
		),
		'filtration' => array(
			'HistoryAction-cms_user_id'		=>	'select|Uživatel|cms_user_list',
			'HistoryAction-url|'		=>	'text|url|',
			'HistoryAction-controller|'		=>	'text|Controller|',
			'HistoryAction-created|'		=>	'date|Datum|',
		),
		'items' => array(
			'id'			=>	'ID|HistoryAction|id|hidden|',
			'controller'	=>	'Sekce|HistoryAction|controller|text|',
			'actions'		=>	'Akce|HistoryAction|action|text|',
			'url'			=>	'Link|HistoryAction|url|text|',
			'uzivatel'		=>	'User|HistoryAction|cms_user_id|viewVars|cms_user_list',
			'datum'			=>	'Datum|HistoryAction|created|datetime|'
		),
		'posibility' => array(
			//'edit'		=>	'edit|Editace SMS kampaně|edit'		
		)
	);
	
	/**
	 * 
	 * @return view
	 */
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/',$this->renderSetting['page_caption']=>'#'));
	
		$this->loadModel('CmsUser');
		$this->CmsUser->query("SET NAMES 'utf8'");
		$this->set('cms_user_list',array(-99=>'Nepřihlášen')+$this->CmsUser->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0,'status'=>1))));
		unset($this->CmsUser);
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	
	
}
?>