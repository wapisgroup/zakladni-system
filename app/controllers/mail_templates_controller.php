<?php
//Configure::write('debug',1);
class MailTemplatesController extends AppController {
	var $name = 'MailTemplates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('MailTemplate');
	var $renderSetting = array(
		'SQLfields' => array('id','name','updated','created','text','status'),
		'controller'=> 'mail_templates',
		'page_caption'=>'Šablony emailových zpráv',
		'sortBy'=>'MailTemplate.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			//'MailTemplate-name'		=>	'text|Název|'
		),
		'items' => array(
			'id'		=>	'ID|MailTemplate|id|text|',
			'name'		=>	'Nazev|MailTemplate|name|text|',
			'text'		=>	'Popis|MailTemplate|text|text|orez#50',
			'updated'	=>	'Upraveno|MailTemplate|updated|datetime|',
			'created'	=>	'Vytvořeno|MailTemplate|created|datetime|'
		),
		'posibility' => array(
			'status_new'	=> 	'status|Změna stavu|status',     
			'edit'		=>	'edit|Editace položky|edit',
		//	'delete'	=>	'trash|Do kosiku|delete'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Šablony emailových zpráv'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->MailTemplate->read(null,$id);
                
                /**
                 * nacteni prijemcu
                 */
                 $this->loadModel('ConnectionMailTemplate');
                 $this->ConnectionMailTemplate->bindModel(array(
                    'belongsTo'=>array(
                        'CmsUser',
                        'CmsGroup',
                        'AddCmsUser'=>array(
                            'className' => 'CmsUser',
                            'foreignKey'=>'add_cms_user_id'
                        )
                    )
                 ));
                 $this->set('connection_list',$this->ConnectionMailTemplate->find('all',array(
                    'conditions'=>array(
                        'ConnectionMailTemplate.mail_template_id'=>$id,
                        'ConnectionMailTemplate.kos'=>0
                    )
                 )));
			}
            
            /**
             * list skupin
             */
            $this->loadModel('CmsGroup');
			$cms_group_list = $this->CmsGroup->find('list',array(
				'conditions'=>am(array('kos'=>0,'status'=>1),array(
                (isset($id) ? 'id NOT IN(SELECT cms_group_id FROM wapis__connection_mail_templates Where mail_template_id="'.$id.'" AND kos=0 )' : null)))
			));
			$this->set('cms_group_list',$cms_group_list);	
			
            /**
             * list useru
             */
            $this->loadModel('CmsUser');
			$cms_user_list = $this->CmsUser->find('list',array(
				'conditions'=>am(array('kos'=>0,'status'=>1),array(
                (isset($id) ? 'id NOT IN(SELECT cms_user_id FROM wapis__connection_mail_templates Where mail_template_id="'.$id.'" AND kos=0 )' : null)))
			));
			$this->set('cms_user_list',$cms_user_list);	
			
       
            
			$this->render('edit');
		} else {
			$this->MailTemplate->save($this->data);
			die();
		}
	}
	
    
    /**
     * funcke ktera prida prijemce k dane sablone
     * @param template_id - id emailove sablony
     * @param type - group nebo user
     * @param id - cms_group_id nebo cms_user_id
     * @author Jakub Matus
     * @created 21.1.10
     */
    public function add_to_list($template_id = null, $type = null, $id = null){
        
        if(in_array($type,array('group','user')) && $id != null && $template_id != null){
            $this->loadModel('ConnectionMailTemplate');
            
            /**
             * data k ulozeni
             */
            $this->data = array(
                    'add_cms_user_id'=>$this->logged_user['CmsUser']['id'],
                    'mail_template_id'=>$template_id
            ); 
            
            /**
             * prirazeni id prijemce do spravneho sloupce
             */
            if($type == 'group')
                $this->data['cms_group_id'] = $id;
            else
                $this->data['cms_user_id'] = $id;
            
            $this->ConnectionMailTemplate->save($this->data);
            
            die(json_encode(array('result'=>true,'id'=>$this->ConnectionMailTemplate->id)));
        }
        else {
            die(json_encode(array('result'=>false)));
        }
    }
    
    /**
     * funkce pro smazani prijemce
     */
    public function trash_to_list($id){
        $this->loadModel('ConnectionMailTemplate');
        $this->ConnectionMailTemplate->id = $id;
        $this->ConnectionMailTemplate->saveField('kos',1);
        die(json_encode(array('result'=>true)));           
    }
    
    /**
     * funkce pro zmenu stavu ignore
     */
     public function switch_ignore($type,$id, $value){
        $this->loadModel('ConnectionMailTemplate');
        
        $col = ($type == 'i' ? 'ignore' : 'company_connection');
        
        $this->ConnectionMailTemplate->id = $id;
        $this->ConnectionMailTemplate->saveField($col,$value);
        die(json_encode(array('result'=>true)));  
     }
}
?>