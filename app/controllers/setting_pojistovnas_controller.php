<?php
class SettingPojistovnasController extends AppController {
	var $name = 'SettingPojistovnas';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingPojistovna');
	var $renderSetting = array(
		'controller'=>'setting_pojistovnas',
		'SQLfields' => array('id','name','updated','created','status'),
		'page_caption'=>'Nastavení seznamu pojistoven',
		'sortBy'=>'SettingPojistovna.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingStav-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingStav-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingPojistovna|id|text|',
			//'poradi'	=>	'Pořadí|SettingPojistovna|poradi|text|',
			'name'		=>	'Název|SettingPojistovna|name|text|',
			'updated'	=>	'Upraveno|SettingPojistovna|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingPojistovna|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|delete'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení seznamu pojistoven'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingPojistovna->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingPojistovna->save($this->data);
			die();
		}
	}
}
?>