<?php
Configure::write('debug',1);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class NewEmployeesController  extends AppController {
	var $name = 'NewEmployees';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email','NewEmployees');
	var $uses = array('ConnectionClientRequirement');
	var $mesic = CURRENT_MONTH;
	var $rok = CURRENT_YEAR;
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','Company','CompanyWorkPosition'),
			'hasOne'	=>	array(
				'NewClientWorkingHour'=>array(
					'conditions' => array(
						'NewClientWorkingHour.year'=> CURRENT_YEAR,
						'NewClientWorkingHour.month'=> CURRENT_MONTH
					)
				)
			)
		),
		'controller'	=>	'new_employees',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			'ConnectionClientRequirement.type'=> 2,
            'ConnectionClientRequirement.new'=> 1
		),
		'page_caption'=>'Docházky zaměstnanců',
		'sortBy'=>'Company.name.ASC',
		'top_action' => array(
		//	'add_item'		=>	'Přidat|edit|Přidat novou položku|add',
		),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
            'GET-current_month'							=>	'select|Měsíc|mesice_list',
			'CompanyWorkPosition-name|'			=>	'text|Pozice|',	
			'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
            'GET-current_year'							=>	'select|Rok|actual_years_list',
        ),
		'items' => array(
			'id'				=>	'ID|ConnectionClientRequirement|id|hidden|',
			'company'			=>	'Společnost|Company|name|text|',
			'req'				=>	'Profese|CompanyWorkPosition|name|text|',		
			'client_id'			=>	'Klient ID|Client|id|text|',
			'client'			=>	'Klient|Client|name|text|',
			'stav'				=>	'Stav|NewClientWorkingHour|stav|var|kalkulace_stav_list',
			'nastup'			=>	'Datum nástupu|ConnectionClientRequirement|from|date|',
			'ukonceni'			=>	'Datum ukončení|ConnectionClientRequirement|to|date|',
			'celkem'			=>	'Celkem|NewClientWorkingHour|celkem_hodin|text|',
			'change_max_salary'	=>	'#|NewClientWorkingHour|change_max_salary|text|status_to_ico2#zmena_maximalky-Změna maximálky-1',
			//'stav_dok'			=>	'#|ClientUcetniDokumentace|stav|text|status_to_ico2#chybna_dokumentace-Neuplná účetní dokumentace'
			//'marze'				=>	'Marže|NewClientWorkingHour|marze|text|'
		),
		'posibility' => array(
			'edit'			=>	'odpracovane_hodiny|Editovat položku|edit',
			'uzavrit_odpracovane_hodiny'	=>	'uzavrit_odpracovane_hodiny|Uzavřít docházku|uzavrit_odpracovane_hodiny',
			//'ucetni_dokumentace'	=>	'ucetni_dokumentace|Účetní dokumentace|ucetni_dokumentace',
			'client_info'	=>	'client_info|Karta klienta|client_info',
			//'autorizace'			=>	'autorizace|Autorizovat|autorizace',
            'modify_from_date'	=>	'modify_from_date|Editace zamestanani klienta|modify_from_date',	
		),
        'posibility_link' => array(
            'modify_from_date'=>'ConnectionClientRequirement.id/Client.id'
        ),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);

	

	function beforeRender(){
		parent::beforeRender();

		// úprava nadpisu reportu a přidání do něj datumu
        if(isset($this->viewVars['renderSetting']))
		  $this->set('spec_h1', $this->viewVars['renderSetting']['page_caption'].' za měsíc '.$this->mesice_list[ltrim($this->mesic,'0')].' a rok '.$this->rok);
	}
	
	function index(){
	    $access_list = array(28,215,14,236,170,265);
        $group_access = array(1,56,57);
	    if(!in_array($this->logged_user['CmsUser']['id'],$access_list) && !in_array($this->logged_user['CmsGroup']['id'],$group_access))
            die('K tomuto modulu nemáte přístup.');
       
       
		// set FastLinks
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#','Docházky zaměstnanců'=>'#'));
		
		$this->loadModel('CmsUser');
		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));

	
		
		$company_conditions = array(
			 "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".CURRENT_YEAR."-".CURRENT_MONTH."'))",
			 '((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			 'ConnectionClientRequirement.type'=> 2
		);
		
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		
      
        $this->ConnectionClientRequirement->unbindModel(array('hasOne'=>array('ClientUcetniDokumentace')));
		$this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Company')));
        
		$company_list = $this->ConnectionClientRequirement->find('list',array(
			 'conditions'=> $company_conditions,
			 'fields'=>array('Company.id','Company.name'),
			 'recursive'=>1
		 ));
		 $this->set('company_list',$company_list);
		 
		
		if ($this->RequestHandler->isAjax()){
			$this->set('change_list_js', json_encode(array('filtr_ConnectionClientRequirement-company_id'=>$company_list)));
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
	function zam_refresh($requirements_for_recruitment_id, $year =null, $month=null){
   	if ($year == null || $month == null) die(); 
		$detail = $this->RequirementsForRecruitment->read(null,$requirements_for_recruitment_id);

		$this->loadModel('ConnectionClientRequirement');
		$this->ConnectionClientRequirement->bindModel(array(
			'belongsTo'=>array('Client'),
			'hasOne'	=>	array(
            'NewClientWorkingHour' => array( 'conditions' => 
                    array('NewClientWorkingHour.year'=>$year,'NewClientWorkingHour.month'=>$month)))
		    ));
		 if($month<10) $month="0".$month;
			$from=	'((DATE_FORMAT(ConnectionClientRequirement.from,"%Y-%m")<= "'.$year.'-'.$month.'"))';
			$to=	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.$year.'-'.$month.'") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))';
  	
		$zamestnanci_list = $this->ConnectionClientRequirement->find(
			'all', 
			array(
				'conditions'=>array('ConnectionClientRequirement.type'=>2,'ConnectionClientRequirement.requirements_for_recruitment_id'=>$requirements_for_recruitment_id,$from,$to),
				'fields'=>array(
					'ConnectionClientRequirement.id',
					'NewClientWorkingHour.svatky',
					'NewClientWorkingHour.pracovni_neschopnost',
					'NewClientWorkingHour.prescasy',
					'NewClientWorkingHour.vikendy',
					'NewClientWorkingHour.dovolena',
					'NewClientWorkingHour.celkem_hodin',
					'Client.id',
					'Client.name',
					'ConnectionClientRequirement.from'
				),
				'order'=>'ConnectionClientRequirement.id ASC'
			)
		);
		$this->set('zamestnanci_list',$zamestnanci_list);
		$this->set('detail',$detail);
		$this->set('rok',$year);
		$this->set('mesic',$month);
		
		$this->render('zamestnanci_list_refresh');
		//pr($zamestnanci_list);

	}
	
	private function read_connection($connection_client_requirement_id = null, $year = null, $month = null){
		// loadmodel && bindModel
			$this->ConnectionClientRequirement->bindModel(array(
				'belongsTo'	=>	array('RequirementsForRecruitment','Client',
                                'CompanyOrderItem'=>array(
                                    'foreignKey'=>'requirements_for_recruitment_id'                                   
                                )
                ),
				'hasOne'	=>	array('NewClientWorkingHour' => array('conditions' =>array('NewClientWorkingHour.year'=>$year,'NewClientWorkingHour.month'=>$month)))
			));

			// read from db
			return  $this->ConnectionClientRequirement->read(null,$connection_client_requirement_id);
	}
	
    //funkce na prepocet prescasu
	private function prepocet_prescasy($setting){
		$pd = 0;
		$this->loadModel('SettingStatPd');
		$pd_list = $this->SettingStatPd->find('list',array(
				'conditions'=>array(
					'kos'=>0,
					'mesic'=>CURRENT_MONTH,
					'rok'=>CURRENT_YEAR
				),
				'fields'=>array('setting_stat_id','pocet')
		));
		$nh = $setting['normo_hodina'];
	
				
		//pokud exituji nastaveny pracovni fond pro tento mesic a rok
		if(array_key_exists($setting['stat_id'],$pd_list))
			$pd = $pd_list[$setting['stat_id']];
		if($pd <> 0 && $nh <> 0){
			return  ($nh * $pd);
		}
		else 
			return 0;
	}
    
  
    
    
	
	/**
 	* Returns a view of "odpracovane hodiny"
 	*
	* @param $connection_client_requirement_id (INT)
	* @param $year (DATE(Y))
	* @param $month DATE(m))
	* @param $inner (STRING)
 	* @return view
 	* @access public
	**/
	public function odpracovane_hodiny($connection_client_requirement_id = null, $year = null, $month = null, $inner = null){
            $requirements_table = 'RequirementsForRecruitment';


            if (empty($this->data)){
                if ($connection_client_requirement_id == null || $year == null || $month== null){
				    die('Chyba, neznami cinitele');
			}
            
           /**
            * nastaveni predchoziho mesice se spravnym rokem 
            */    
            $prev_month_date = new DateTime();
            $prev_month_date->setDate($year, $month, 01);
            $prev_month_date->modify("-1 month");
		

			$this->data = $this->read_connection($connection_client_requirement_id, $year, $month);
			
            if($this->data['ConnectionClientRequirement']['objednavka'] == 1)
                $requirements_table = 'CompanyOrderItem';   
          

   			$this->loadModel('Company');
			$company_stat_id = $this->Company->read(array('stat_id','payment_cash','payment_amount','payment_once_in_month','manazer_realizace_id'),$this->data['ConnectionClientRequirement']['company_id']);
            $this->set('company_detail',$company_stat_id);

            $transfer_from_last_month = false;

			if (empty($this->data['NewClientWorkingHour']['id'])){
				//company_work_position_id se nyni bere z connection_client_requirements
				$this->data['NewClientWorkingHour']['company_work_position_id'] = $this->data['ConnectionClientRequirement']['company_work_position_id'];

 
				$prev_month =  $this->read_connection($connection_client_requirement_id, $prev_month_date->format('Y'),$prev_month_date->format('m'));
				if (!empty($prev_month['NewClientWorkingHour']['id'])){
				    $tran_prev_array = array('setting_shift_working_id','company_work_position_id','accommodation_id');
					foreach($tran_prev_array as $tran)
						$this->data['NewClientWorkingHour'][$tran] = $prev_month['NewClientWorkingHour'][$tran];
                        
					$transfer_from_last_month = true;
					$this->set('transfer_from_last_month',$transfer_from_last_month);
				
					if($prev_month['NewClientWorkingHour']['company_work_position_id'] <> $this->data['ConnectionClientRequirement']['company_work_position_id'])
						$this->data['NewClientWorkingHour']['company_work_position_id'] = $this->data['ConnectionClientRequirement']['company_work_position_id'] ;
				}				
			}
			else {
				if($this->logged_user['CmsGroup']['id']==1 && $this->data['NewClientWorkingHour']['company_work_position_id'] == 0){
					$this->data['NewClientWorkingHour']['company_work_position_id']= $this->data['ConnectionClientRequirement']['company_work_position_id'];
					echo "Profese byla špatně nastavena pracujete s přenesenou profesi která je pro neuložené docházky.";
				}
			}
	

			$this->data['NewClientWorkingHour']['connection_client_requirement_id'] = $connection_client_requirement_id;
			$this->data['NewClientWorkingHour']['company_id'] = $this->data['ConnectionClientRequirement']['company_id'] ;
			$this->data['NewClientWorkingHour']['client_id'] = $this->data['ConnectionClientRequirement']['client_id'] ;
			$this->data['NewClientWorkingHour']['requirements_for_recruitment_id'] = $this->data['ConnectionClientRequirement']['requirements_for_recruitment_id'] ;
			
			

			// if not exist ClientWorkingHour load dota from RequirementsForRecruitment
			if (empty($this->data['NewClientWorkingHour']['id']) && empty($prev_month['NewClientWorkingHour']['id'])){
				$transfer_arrray_con_rec = array('company_id','client_id', 'requirements_for_recruitment_id', 'company_work_position_id'); 		
            	$transfer_arrray_rec = array( 'setting_shift_working_id' ); 		
				foreach($transfer_arrray_con_rec as $transform) $this->data['NewClientWorkingHour'][$transform] = $this->data['ConnectionClientRequirement'][$transform];
				foreach($transfer_arrray_rec as $transform)		$this->data['NewClientWorkingHour'][$transform] = $this->data[$requirements_table][$transform];
			}

			if($this->data['NewClientWorkingHour']['company_work_position_id']==0 )
				die('Chybně nastavena profese v náboru, při zaměstnání!!! Většinou se jedná o staré umístění klientů.');
			
            
            	// load list of accomodations, in future will be  add condition for cms_user_id
				 //podminka pro jednotliveho usera jen k nemu prirazene ubytovny
                $acc_con = array(
                    'ConnectionAccommodationUser.cms_user_id'=>array(-2,$this->logged_user['CmsUser']['id'])
                );
                
                //pokud se jedna o adminy nebo DIR tato podminak pro ne neplati
                if(in_array($this->logged_user['CmsGroup']['id'],array(1,5)) || in_array($this->logged_user['CmsUser']['id'],array(209)))
                    $acc_con = array();
                
                $this->loadModel('Accommodation');
				$this->Accommodation->bindModel(array(
					'hasOne'=>array('ConnectionAccommodationUser'=>array('foreignKey'=>'accommodation_id'))
				));                 
				$this->set('accommodation_list',$x = $this->Accommodation->find('list',array(
					'fields'=>array('Accommodation.id','Accommodation.name'),
					'conditions'=>am(array('Accommodation.kos'=>0),$acc_con),
                    'recursive'=>1
				)));
	       $this->set('ubytovani_select', true); 
           
           /**
             * nacteni zaloh k danemu connection
             */
			$this->loadModel('ReportDownPayment');
            $this->ReportDownPayment->bindModel(array(
                'belongsTo'=>array('CmsUser')
            ));
			$rdp = $this->ReportDownPayment->find('all',array(
				'conditions'=>array(
					'ReportDownPayment.kos'=>0,
                    'OR'=>array(
                        array(
        					'(DATE_FORMAT(ReportDownPayment.created,"%Y-%m"))' => $year.'-'.$month,
        					'ReportDownPayment.month' =>'0',
        					'ReportDownPayment.year' =>'0'
                        ),
                        array(
                            'ReportDownPayment.month' =>$month,
        					'ReportDownPayment.year' =>$year
                        )    
					),
                    'ReportDownPayment.client_id'=>$this->data['NewClientWorkingHour']['client_id'],
					'ReportDownPayment.connection_client_requirement_id'=>$connection_client_requirement_id
				)
			));
			$this->set('payment_list',$rdp);
			$this->set('payment_sum',0);
			unset($this->ReportDownPayment);
            
            /**
             * osetreni podminky c.1 pro zalohy
             * pokud existuje dochazka v minulem mesici  = zakaz vlozit pozadavek na zalohu 
             * v terminu 15. – 25. dne v mesici
             * 
             * dotazy usetrime tim ze to budeme provadet az 15-25 dnu
             */
             $allow_payment_0 = $company_stat_id['Company']['payment_amount'];
             $allow_payment_1 = $company_stat_id['Company']['payment_cash'];
             if(self::is_in_interval(date('d'),15,25) && (isset($company_stat_id['Company']['payment_once_in_month']) && $company_stat_id['Company']['payment_once_in_month'] == 0)){
                $this->loadModel('NewClientWorkingHour');
                $previous_client_working = $this->NewClientWorkingHour->find('first',array(
                    'conditions'=>array(
                        'connection_client_requirement_id'=>$connection_client_requirement_id,
                        'year'=> $prev_month_date->format('Y'),
                        'month'=> $prev_month_date->format('m')
                    )
                ));
                
                
                /**
                 * pokud je ve vyjimce tak tuto podminku vynech
                 */
                $this->loadModel('CompanyExceptionPayment');
                $exception_company = $this->CompanyExceptionPayment->findByCompanyId($this->data['NewClientWorkingHour']['company_id']);  
            
            
                /**
                 * jestlize ma v predchozim mesici dochazku tak zamez vystaveni zalohy
                 */
                 if($previous_client_working && !$exception_company)
                   $allow_payment_0 = false;
             }
             else if(isset($company_stat_id['Company']['payment_once_in_month']) && $company_stat_id['Company']['payment_once_in_month'] == 1){
                if(!isset($this->ReportDownPayment))
                    $this->loadModel('ReportDownPayment');
                
                 /**
                  * Nova podminka pokud je zaskrtnuta nova podminka u firem
                  * pouze jednou mesicne
                  * pokud najdeme zalohu v terminu od 15 v danem mesici tak zamez vlozeni
                  * pokud najdeme zalohu z predchazejiciho obdobi (15 minuleho - 14 toho mesice) - zamez vlozeni
                  */
                  $have_payment = false;
                  if(self::is_in_interval(date('d'),1,14)){
                        $have_payment = $this->ReportDownPayment->find('first',array(
                            'conditions'=>array(
                                'kos'=>0,
                                'client_id'=>$this->data['NewClientWorkingHour']['client_id'],
                                'company_id'=>$this->data['NewClientWorkingHour']['company_id'],
                                'connection_client_requirement_id'=>$this->data['NewClientWorkingHour']['connection_client_requirement_id'],
                                'DATE_FORMAT(created,"%Y-%m-%d") >= DATE_SUB(DATE_FORMAT(NOW(),"%Y-%m-15"), INTERVAL 1 MONTH) AND DATE_FORMAT(NOW(),"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-14")'
                            ),
                            'order'=>'id DESC'
                         ));  
                  }
                  else if(self::is_in_interval(date('d'),15,31)){
                        $have_payment = $this->ReportDownPayment->find('first',array(
                            'conditions'=>array(
                                'kos'=>0,
                                'client_id'=>$this->data['NewClientWorkingHour']['client_id'],
                                'company_id'=>$this->data['NewClientWorkingHour']['company_id'],
                                'connection_client_requirement_id'=>$this->data['NewClientWorkingHour']['connection_client_requirement_id'],
                                'DATE_FORMAT(created,"%Y-%m-%d") >= DATE_FORMAT(NOW(),"%Y-%m-15") AND  DATE_FORMAT(created,"%Y-%m-%d") <= DATE_FORMAT(NOW(),"%Y-%m-31")'
                            ),
                            'order'=>'id DESC'
                         ));  
                  }
                  
                  if($have_payment){
                       $allow_payment_0 = false;     
                  }
             }
             
             $this->set('allow_payment_0', $allow_payment_0);
             $this->set('allow_payment_1', $allow_payment_1);
			 // END podminka 1
           
           
			// default variable na index
			$this->set('stat_id', $company_stat_id['Company']['stat_id']); 
			$this->set('connection_client_requirement_id', $connection_client_requirement_id); 
			$this->set('year', $year); 
			$this->set('month', $month); 
			
			// kill rendering, if client dont work in this year and month, or you select future;
			list($start_y, $start_m, $start_d) 	= explode('-', $this->data['ConnectionClientRequirement']['from']);
			list($end_y, $end_m, $end_d) 		= explode('-', $this->data['ConnectionClientRequirement']['to']);
			
			// for Test Only = $end_y = '2009'; 	$end_m = '06';
			// condition for kill rendering
			$render_element = 0;

			if (( $year <= date('Y') && $month <= date('m') ) || ($month > date('m') && $year < date('Y'))){

				if (($end_y == '0000' && $start_y <= $year && $start_m <= $month) || ($end_y == '0000' && $start_y < $year && $start_m > $month)) {
					$render_element = 1;	
				} else if ($end_y != '0000' && $start_y <= $year && $start_m <= $month && $end_y >= $year && $end_m >= $month){
					$render_element = 1;
				}
			} else {
				$render_element = -1;
			}
			// load free days for state, month, year
			$this->loadModel('SettingStatSvatek');
			$this->set('svatky',$this->SettingStatSvatek->find('list', array('conditions'=>array( 'setting_stat_id'	=> av(av($company_stat_id,'Company'),'stat_id'), 'mesic'=> $month, 'rok'=> $year ),'fields'=>array('id','den'),'order'=>'den ASC'))); 				
			unset($this->SettingStatSvatek);
			            		
			            
             /**
              * nastaveni promennych s kteryma pracuje komponenta
              * a zavolani setu pro view
              */
             $this->NewEmployees->permission = $this->logged_user['CmsGroup']['permission']['employees'];
             $this->NewEmployees->logged_user = $this->logged_user;
             $this->NewEmployees->set('year',$year);
             $this->NewEmployees->set('month',$month);
             $this->NewEmployees->set('this_data',$this->data);
             $this->NewEmployees->set('connection_id',$connection_client_requirement_id);
             $this->NewEmployees->set_variables_for_view();             
            		
			// render view
			if ($inner != null)
				if ($render_element === 1)
					$this->render('element');
				else if ($render_element === -1)
					die('<p>Ty umíš věštit? Jak to kruci víš, kolik toho udělá?</p>');
				else 
					die('<p>Hej!! Tady vubec nic nedelal!!</p>');
			else
				$this->render('index');
		} else {		
			$this->loadModel('NewClientWorkingHour');

            if($this->data['NewClientWorkingHour']['id'] == ''){
                 $this->NewEmployees->set('year',$this->data['NewClientWorkingHour']['year']);
                 $this->NewEmployees->set('month',$this->data['NewClientWorkingHour']['month']);
                 $this->NewEmployees->set('connection_id',$this->data['NewClientWorkingHour']['connection_client_requirement_id']);
                
                 if($this->NewEmployees->if_exist_client_working_hour() === true)
                    die(json_encode(array('result'=>false, 'message' => 'Chyba: Snažíte se uložit duplicitní docházku!!! Obnovte okno, nebo zavřete okno a načtěte docházku znova.')));
            }

            /**
             * Pokud uklada nekdo jiz autorizovanou dochazku zasli emailovou notifikaci
             */
            if($this->data['NewClientWorkingHour']['stav'] == 3){
                $this->loadModel('Company');
    			$company = $this->Company->read($this->Company->company_data_for_email_notification,$this->data['NewClientWorkingHour']['company_id']);
                unset($this->Company);
                
                $this->loadModel('Client');
    			$cl = $this->Client->read(array('name'),$this->data['NewClientWorkingHour']['client_id']);
                unset($this->Client);
                
                            
    			$replace_list = array(
    					'##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name'],
    					'##Company.name##' 	=>$company['Company']['name'],
    					'##Client.name##' 	=>$cl['Client']['name'],
    					'##Rok##' 	=>$this->data['NewClientWorkingHour']['year'],
    					'##Mesic##' 	=>$this->data['NewClientWorkingHour']['month'],
    					'##Text##' 	=>$this->NewEmployees->check_changes_in_wh($this->data),
    					'##Status##' 	=>($this->data['money_item_name'] == '0010' ? 'faktura' : 'výplatní páska')
    			);
                
                $this->Email->set_company_data($company['Company']);
    			$this->Email->send_from_template_new(18,array(),$replace_list);
            }
                
            // osetreni prazdeho stavu - defaultne 1, nepochopitelna chyba, 
            // nenastavuje db automaticky defaultni hodnotu    
            if($this->data['NewClientWorkingHour']['stav'] == '')
                $this->data['NewClientWorkingHour']['stav'] = 1;
                
            //pr($this->data);    

			if($this->NewClientWorkingHour->save($this->data)){
			        //pr($this->data['ConnectionClientWorkingHourToMoneySetting']);
                    
                    /**
                     * Ulozeni nastaveni Srazek, Bonusu a priplatku do zvlastni tabulky connection
                     * ukladame pouze hodnotu a mesicni(zatim se nepouziva, pouze pro databazi)
                     */
                    if(isset($this->data['ConnectionClientWorkingHourToMoneySetting']) && !empty($this->data['ConnectionClientWorkingHourToMoneySetting'])){                    
                        $this->loadModel('ConnectionClientWorkingHourToMoneySetting');
                       
                        foreach($this->data['ConnectionClientWorkingHourToMoneySetting'] as $type_id =>$type_data){
                            /**
                             * najdeme jiz existuji ktere k teto dochazce mame, at je pouze prepisujeme
                             */
                            $exist_con = $this->ConnectionClientWorkingHourToMoneySetting->find('list',array(
                                'conditions'=>array(
                                    'new_client_working_hour_id'=>$this->NewClientWorkingHour->id,
                                    'type_id'=>$type_id
                                ),
                                'fields'=>array('connection_money_item_settings_id','id')
                            ));
                            
                            $this->ConnectionClientWorkingHourToMoneySetting->updateAll(
                                array('kos'=>1),
                                array('new_client_working_hour_id'=>$this->NewClientWorkingHour->id,'type_id'=>$type_id)
                            );
                                           
                            foreach($type_data as $con_id=>$con_data){
                                if($con_data['yn'] == 1 || $con_data['yn'] == 'on' || $con_data['yn'] == '1'){//pouze zaskrtnute
                                    $con_save_data = array(
                                        'new_client_working_hour_id'=>$this->NewClientWorkingHour->id,
                                        'type_id'=>$type_id,
                                        'connection_money_item_settings_id'=>$con_id,
                                        'val'=>$con_data['value'],
                                        'monthly'=>$con_data['monthly'],
                                        'kos'=>0
                                    );
                                    
                                    if(array_key_exists($con_id,$exist_con)){
                                        $this->ConnectionClientWorkingHourToMoneySetting->id = $exist_con[$con_id];
                                    }
                            
                                    $this->ConnectionClientWorkingHourToMoneySetting->save($con_save_data);
                                    $this->ConnectionClientWorkingHourToMoneySetting->id = null;
                                }
                            }
                        }
                    }
             
					die(json_encode(array('result'=>true, 'id'=>$this->NewClientWorkingHour->id, 'message' => 'Úspěšně uloženo do databáze.')));
			} else {
				die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB.')));
			}
		}
	}
	
    /**
     * @since 12.11.09
     * @author Sol
     * @abstract privatni funkce pro zpracovani preneseni nove zvolene company_money_item_id,company_work_position_id do connection 
     */
     private function company_money_item_id_to_connection($connection_id = null,$company_money_item_id = null,$company_work_position_id){
        if($connection_id == null || $company_money_item_id == null)
            die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB a tabulky connection.')));
        else{    
            $this->ConnectionClientRequirement->id = $connection_id;
            $this->ConnectionClientRequirement->saveField('company_money_item_id',$company_money_item_id);        
            $this->ConnectionClientRequirement->saveField('company_work_position_id',$company_work_position_id);        
        }    
     }
    
    
	function uzavrit_odpracovane_hodiny($client_id = null, $company_id = null, $client_working_hour_id = null, $month = null, $year = null){
		if ($company_id == null || $client_working_hour_id == null){
			LogError('Controller: employees_controller; Function: uzavrit_odpracovane_hodiny; Chyba! Nejsou známé všechny parametry.');
			die(json_encode(array('result'=>false,'message'=>'Chyba! Nejsou známé všechny parametry.')));
		}
		
		$this->loadModel('NewClientWorkingHour');
		$this->NewClientWorkingHour->id = $client_working_hour_id;
		$this->NewClientWorkingHour->saveField('stav',2);
		
		$this->ConnectionClientRequirement->bindModel(array(
			'belongsTo'	=>	array('Client','Company','RequirementsForRecruitment'),
			'hasOne'	=>	array('NewClientWorkingHour'=>array(
				'conditions' => array(
					'NewClientWorkingHour.year'=> $year,
					'NewClientWorkingHour.month'=> $month
				)
			))
		));
		
		$conditions	= array(
			"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '$year-$month'))",
			"((DATE_FORMAT(ConnectionClientRequirement.to,'%Y-%m') >= '$year-$month') OR (ConnectionClientRequirement.to = '0000-00-00 00:00'))",
			'ConnectionClientRequirement.type'=> 2,
			'ConnectionClientRequirement.new' => 1,
            'NewClientWorkingHour.stav' => 1,
			'NewClientWorkingHour.company_id' => $company_id
		);
		
		$count = $this->ConnectionClientRequirement->find('count', array('conditions'=>$conditions));
		
		if ($count == 0){
			$this->loadModel('Company');
			$company = $this->Company->read($this->Company->company_data_for_email_notification,$company_id);
			
			$termin = new DateTime();
			$termin->modify("+2 day");
			
			$task_to_save = array('CompanyTask'=>array(
				'cms_user_id' 	=> 	14, // Putorik Robert
				'name'			=> 	'Autorizace odpracovaných hodin',
				'termin'		=>	$termin->format("Y-m-d"),
				'company_id'	=>	$company_id,
				'setting_task_type_id' => 9,
				'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
				'text'			=> 'Autorizujte odpracované hodiny u firmy '.$company['Company']['name'].'.'
			));
			$this->loadModel('CompanyTask');
			$this->CompanyTask->save($task_to_save);

			//vytvoreni notifikacniho emailu pro CM,KOO,SM dane firmy a DIR = id->14 
			

			$replace_list = array(
					'##Company.name##' 	=>$company['Company']['name'],
					'##Rok##' 	=>$year,
					'##Mesic##' 	=>$month
			);
            $this->Email->set_company_data($company['Company']);
			$this->Email->send_from_template_new(10,array(),$replace_list);
			
			//vytoreni connection  firmy a jejiho stavu pro dany rok a mesic
			$conditions	= array(
                'ConnectionCompanyToAuthorization.new' => 1,
				'ConnectionCompanyToAuthorization.year' => $year,
				'ConnectionCompanyToAuthorization.month' => $month,
				'ConnectionCompanyToAuthorization.company_id' => $company_id
			);
			$this->loadModel('ConnectionCompanyToAuthorization');
			$finCon = $this->ConnectionCompanyToAuthorization->find('all', array(
				'conditions'=>$conditions,
				'fields'=>array('id')
			));
			if(!$finCon){ // jedna se o nove connection,pro dany mesic a rok
				$to_save = array('ConnectionCompanyToAuthorization'=>array(
					'company_id'	=>	$company_id,
                    'new'		=>	1,
					'year'			=> 	$year,
					'month'		=>	$month,
					'closed_user'	=>	$this->logged_user['CmsUser']['id'], // at vime kdo uzavrel odpracovane hodiny
					'closed' => date("Y-m-d H:i:s")
				));
				$this->ConnectionCompanyToAuthorization->save($to_save);
			}
			else { // connection je jiz vytvoreno, pouze obnov stav firmy, pro dany mesic a rok
				$this->ConnectionCompanyToAuthorization->id = $finCon[0]['ConnectionCompanyToAuthorization']['id'];
				$this->ConnectionCompanyToAuthorization->saveField('stav',1);
				$this->ConnectionCompanyToAuthorization->saveField('closed',date("Y-m-d H:i:s"));
				$this->ConnectionCompanyToAuthorization->saveField('closed_user',$this->logged_user['CmsUser']['id']);
			}

		}
		
		die(json_encode(array('result'=>true,'pocet'=>$count)));
	}
	
	/**
 	* Returns a JSON list of calculation for "profese"
 	*
	* @param $company_work_position_id (INT)
	* @param $year (DATE(Y))
	* @param $month DATE(m))
	* @param $json Boolean
 	* @return JSON
 	* @access public
	**/
	//moemntalne se nepouziva prootze byli zmeneny load profesi!
	public function load_calculation_for_prefese($company_work_position_id = '', $year = null, $month = null, $json = true){
		if ($company_work_position_id == null || $year == null || $month == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
		
		
		// load model of validity and calculation
		$this->loadModel('CompanyMoneyValidity');
		$this->CompanyMoneyValidity->bindModel(array('hasMany'=>array('NewMoneyItem'=>array(
            'fields'=>array(
                'id','name','vypocitana_celkova_cista_maximalni_mzda_na_hodinu',
                'cista_mzda_z_pracovni_smlouvy_na_hodinu','cista_mzda_faktura_zivnostnika_na_hodinu',
                'cista_mzda_dohoda_na_hodinu','cista_mzda_cash_na_hodinu','stravenka',
                'fakturacni_sazba_na_hodinu','normo_hodina','doprava','cena_ubytovani_na_mesic',
                'odmena_max_1','odmena_max_2','pausal','ps_pausal_hmm',
                'vlozeny_prispevek_na_stravne','vlozeny_prispevek_na_cestovne',
                'h1000','ps_hodina_hmm_monthly','ps_hodina_hmm',
                'ps_hodina_odmena_1','ps_hodina_max_1','ps_hodina_max_1p','ps_hodina_odmena_1_type',
                'ps_hodina_odmena_2','ps_hodina_max_2','ps_hodina_max_2p','ps_hodina_odmena_2_type'
            )
        ))));
		$money_items = $this->CompanyMoneyValidity->find(
				'all', 
				array(
					'conditions'=>array(
						'company_work_position_id' => $company_work_position_id,
                        "((DATE_FORMAT(CompanyMoneyValidity.platnost_od,'%Y-%m')<= '".$year."-".$month."'))",
						"((DATE_FORMAT(CompanyMoneyValidity.platnost_do,'%Y-%m')>= '".$year."-".$month."') OR (DATE_FORMAT(CompanyMoneyValidity.platnost_do,'%Y-%m') = '0000-00'))",
                           
                        //'MONTH(platnost_od) <=' => $month,
						//'YEAR(platnost_od) <=' => $year,
					//	array('AND'=> array('OR' => array(
                             //'MONTH(platnost_do) >=' => $month,
							//'MONTH(platnost_do)' => '00'
					//	))),
					//	array('AND'=> array('OR' => array(
					//		'YEAR(platnost_do) >=' => $year,
					//		'YEAR(platnost_do)' => '0000'
					//	)))
					)
				)
			);
		if ($money_items == false){
			if ($json == true)
				die(json_encode(array('result'=>false,'message'=>"Nebyla nalezena kalkulace s odpovídající platností pro $year/$month.")));
			else
				return false;
		} else {
			if (count($money_items) > 1){
				if ($json == true)
					die(json_encode(array('result'=>false,'message'=>"Pro tuto profesi byly naleznuty 2 nebo více odpovidajících platností pro $year/$month. Pravděpodobně se jedná o chybu v zadání nové platnosti fakturační sazby.")));
				else
					return false;
			} else {
				if ($json == true)
					die(json_encode(array('result'=>true,'data'=>$money_items[0]['NewMoneyItem'])));
				else
					return $money_items[0]['NewMoneyItem'];
			}
		}
		if ($json == true)
			die(json_encode(array('result'=>false,'message'=>"Chyba: Sem to vubec nemelo projit. Neco je spatne s podminkou.")));
		else
			return false;
	}
	
	
	public function load_calculation_for_prefese_second($company_work_position_id = null, $json = true){
		if ($company_work_position_id == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
	
		// load model of validity and calculation
		$this->loadModel('CompanyMoneyValidity');
		$this->CompanyMoneyValidity->bindModel(array('hasMany'=>array('NewMoneyItem'=>array('fields'=>array('id','name','vypocitana_celkova_cista_maximalni_mzda_na_hodinu','cista_mzda_z_pracovni_smlouvy_na_hodinu','cista_mzda_faktura_zivnostnika_na_hodinu','cista_mzda_dohoda_na_hodinu','cista_mzda_cash_na_hodinu','stravenka','fakturacni_sazba_na_hodinu','doprava','cena_ubytovani_na_mesic','odmena_max_1','odmena_max_2','normo_hodina')))));
		$money_items = $this->CompanyMoneyValidity->find(
				'all', 
				array(
					'conditions'=>array(
						'company_work_position_id' => $company_work_position_id,
						'platnost_od !='=>'0000-00-00', 
						'platnost_do '=>'0000-00-00'
					)
				)
			);
		if ($money_items == false){
			if ($json == true)
				die(json_encode(array('result'=>false,'message'=>"Nebyla nalezena kalkulace s odpovídající platností.")));
			else
				return false;
		} else {
			if (count($money_items) > 1){
				if ($json == true)
					die(json_encode(array('result'=>false,'message'=>"Pro tuto profesi byly naleznuty 2 nebo více odpovidajících platností pro $year/$month. Pravděpodobně se jedná o chybu v zadání nové platnosti fakturační sazby.")));
				else
					return false;
			} else {
				if ($json == true)
					die(json_encode(array('result'=>true,'data'=>$money_items[0]['NewMoneyItem'])));
				else
					return $money_items[0]['NewMoneyItem'];
			}
		}
		if ($json == true)
			die(json_encode(array('result'=>false,'message'=>"Chyba: Sem to vubec nemelo projit. Neco je spatne s podminkou.")));
		else
			return false;
	}
	
	
	
	function load_client($kvalifikace_id=null, $company_id = null, $requirement_id = null){
		if ($kvalifikace_id == null){
			die ('Prosím vyberte kvalifikaci');
		}
		$this->loadModel('ConnectionClientCareerItem');
		$this->ConnectionClientCareerItem->bindModel(array('belongsTo'=>array('Client')));
		$this->set('client_list',$this->ConnectionClientCareerItem->find('list',array('conditions'=>array('ConnectionClientCareerItem.setting_career_item_id'=>$kvalifikace_id),'fields'=>array('Client.id','Client.name'),'recursive'=>2,'order'=>'Client.name')));
		unset($this->ConnectionClientCareerItem);
		
		$this->set('requirement_id', $requirement_id);
		$this->set('company_id', $company_id);
		
		$this->render('nabor_pozice_client_list');
	}
	
    
    /**
     * @name ucetni dokumentace
     * @abstract na odpracovane hodiny id
     * @author Sol
     * @since 2.11.2009
     */
    function ucetni_dokumentace ($connection_client_requirement_id = null, $forma = null){
    //Configure::write('debug',1);
        if(empty($this->data)){
            if($connection_client_requirement_id == null)
                die('Chyba špatné spojení mezi klientem a náborem!!!');
            
            $this->loadModel('ClientUcetniDokumentace');
            $this->data = $this->ClientUcetniDokumentace->find('first',array(
                'conditions'=>array(
                    'connection_client_requirement_id'=>$connection_client_requirement_id,
                    'forma'=>$forma,
                    'kos'=>0
                )
            ));

   
            if(empty($this->data)){
               $this->data['ClientUcetniDokumentace'] = array(
                    'connection_client_requirement_id'=>$connection_client_requirement_id,
                    'forma'=>$forma,
               );
            }

            
           $this->set('forma',$forma);
            
            /**
             * nastavenií prav zobrazeni
             * nyni editovat pouze ucetni a admin
             */
             
             $only_show = false;
             if(!in_array($this->logged_user['CmsGroup']['id'],array(1,6,7)))
                $only_show = true;
                
             $this->set('only_show',$only_show);   
             
            //render
            $this->render('ucetni_dokumentace');
        }
        else {
            if($this->data['ClientUcetniDokumentace']['id'] == null)
                $this->data['ClientUcetniDokumentace']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
                
            $this->loadModel('ClientUcetniDokumentace');    
            $this->ClientUcetniDokumentace->save($this->data);    
            
        }

    }
	
	
	//zobrazeni karty klienta
	function client_info($id = null){
		echo $this->requestAction('clients/edit/'.$id.'/domwin/only_show');
		die();
	}
    
    /**
     * funkce ktera nam bude vracet nove mozne company_money_item_id
     * @author Sol
     * @since 11.11.09
     */
    public function load_new_posssible_money_item_id($client_connection_requirement = null){
		if ($client_connection_requirement == null) 
			die(json_encode(array('result'=>false,'message'=>'Nelze zvolit prázdnou profesi.')));
	
        $this->ConnectionClientRequirement->bindModel(array(
            'belongsTo'=>array(
                'NewMoneyItem'=>array(
                    'fields'=>array('id','name','doprava','cena_ubytovani_na_mesic')
                )
            )
        ));
	    $cmi_id = $this->ConnectionClientRequirement->read(
            array(
                'NewMoneyItem.id',
                'NewMoneyItem.name',
                'NewMoneyItem.doprava',
                'NewMoneyItem.pausal',
                'NewMoneyItem.cena_ubytovani_na_mesic',
                'company_money_item_id',
                'company_work_position_id'
            ),$client_connection_requirement);
        
        
        if(!$cmi_id)
            die(json_encode(array('result'=>false,'message'=>'V dochzáce nenalezena žádna forma odměny, kontaktujte administrátora.')));
        else{
            
            $forma_odmeny_name = $cmi_id['NewMoneyItem']['name'];
            $ubytovani = ($cmi_id['NewMoneyItem']['cena_ubytovani_na_mesic'] > 0 ? 1 : 0);
            $doprava = ($cmi_id['NewMoneyItem']['doprava'] > 0 ? 1 : 0);
            $pausal = (($cmi_id['NewMoneyItem']['pausal'] == 1) ? 1 : 0);
           
            $this->loadModel('NewMoneyItem');
            $search_unique = $this->NewMoneyItem->query('
                 SELECT id,name,IF(doprava > 0,1,0) as dop, IF(cena_ubytovani_na_mesic> 0,1,0) as ub 
                    FROM `wapis__company_money_items` AS `NewMoneyItem`
                 WHERE `company_work_position_id` = '.$cmi_id['ConnectionClientRequirement']['company_work_position_id'].'
                        AND `name` = '.$forma_odmeny_name.' and pausal='.$pausal.' AND `kos` = 0 AND id != '.$cmi_id['NewMoneyItem']['id'].'
                 HAVING dop = '.$doprava.' AND ub= '.$ubytovani.' ORDER BY id DESC LIMIT 1   
	       '); 
           
            if(count($search_unique) == 1)
               die(json_encode(array('result'=>true,'id'=>$search_unique[0]['NewMoneyItem']['id'])));
            else   
               die(json_encode(array('result'=>false,'message'=>'Nenalezena žádna možná nová forma odměny.')));
        }
		die(json_encode(array('result'=>false,'message'=>"Chyba: Sem to vubec nemelo projit. Neco je spatne s podminkou.")));
	}
    
    /**
     * funkce pro zobrazeni domwinu a posleze
     */
     function add_down_payments($type = 0){
         if(!isset($this->data))
            $this->data = $_POST; 
         
         if($this->data['save'] == 0){
            
             /**
              * Druha podminka pro vydani zaloh
              * maximalni limit pro zalohu je 3000 Kc nebo 100€ podle statu
              * s vyjimkou u ktere je zachovan soucasny maximalni limit: pocet hodin x hodinova sazba 
              * Tyto firmy jsou pridavany v Zadosti o zalohy
              * 
              * UPDATE 9.9.2010
              * limit se prenasi z nastaveni firmy
               */
              //$limit = ($this->data['stat_id']==1 ? 3000 : 100);
              
              $exception = false;
              $this->loadModel('CompanyExceptionPayment');
              $exception_company = $this->CompanyExceptionPayment->find('first',array(
                'conditions'=>array(
                    'kos'=>0,
                    'company_id'=>$this->data['company_id']
                )
              ));  
            
              if($exception_company){      
                $exception = true;  
              }
              $this->data['exception'] = $exception;
              
              
              /**
               * Limit berem z nastaveni firmy
               */
               $this->loadModel('Company');
               $comp = $this->Company->read(array('payment_cash_limit','payment_amount_limit'),$this->data['company_id']);
               $this->data['limit'] = ($type == 0 ? $comp['Company']['payment_amount_limit'] : $comp['Company']['payment_cash_limit']);  
               unset($this->Company); 
              //END druha podminka
            
              $this->data['ReportDownPayment'] = array(
                  'year' =>$this->data['year'],
                  'month' =>$this->data['month'],
                  'client_id' =>$this->data['client_id'],
                  'company_id' =>$this->data['company_id'],
                  'celkem_hodin' =>$this->data['celkem_hodin'],
                  'connection_client_requirement_id' =>$this->data['connection_client_requirement_id'],
                  'cms_user_id' =>$this->logged_user['CmsUser']['id']
              );
              
              $this->set('type',$type);
              
              //render              
              $this->render('add_down_payments');
         }
         else {
            $this->loadModel('ReportDownPayment');
            
            
            /**
             * Novinka od 17.3.10
             * pokud jsme do 15dne a klient ma v predchozim mesici dochazku
             * ulozime zalohu k predeslemu mesici
             */
             if(self::is_in_interval(date('d'),1,15)){
                $prev_month_date = new DateTime();
                $prev_month_date->setDate($this->data['ReportDownPayment']['year'], $this->data['ReportDownPayment']['month'], 01);
                $prev_month_date->modify("-1 month");

                $this->loadModel('NewClientWorkingHour');
                $previous_client_working = $this->NewClientWorkingHour->find('first',array(
                    'conditions'=>array(
                        'connection_client_requirement_id'=>$this->data['ReportDownPayment']['connection_client_requirement_id'],
                        'year'=> $prev_month_date->format('Y'),
                        'month'=> $prev_month_date->format('m')
                    )
                ));
                
                if($previous_client_working){
                    $this->data['ReportDownPayment']['year'] = $prev_month_date->format('Y');
                    $this->data['ReportDownPayment']['month'] = $prev_month_date->format('m');
                }
                
             } 
            
            
            /**
             * Podminka c.3
             * zalohy jednou za 4 dny
             * vyjimka opet tuto podminku rusi
             */
            $have_payment = $this->ReportDownPayment->find('first',array(
                'conditions'=>array(
                    'kos'=>0,
                    'client_id'=>$this->data['ReportDownPayment']['client_id'],
                    'company_id'=>$this->data['ReportDownPayment']['company_id'],
                    'connection_client_requirement_id'=>$this->data['ReportDownPayment']['connection_client_requirement_id'],
                    'DATE_FORMAT(created,"%Y-%m-%d") > DATE_SUB(DATE_FORMAT(NOW(),"%Y-%m-%d"), INTERVAL 4 DAY)'
                ),
                'order'=>'id DESC'
             ));
             //pr($have_payment);
             
             if($have_payment && $this->data['exception'] != 1){
                die(json_encode(array('result'=>false,'message'=>'Záloha muže být zadána pouze jednou za 4 dny.')));  
             }
             else{  
                 if($this->ReportDownPayment->save($this->data))
                	die(json_encode(array('result'=>true)));
    	         else
            		die(json_encode(array('result'=>false,'message'=>'Chyba během ukládání zálohy.')));  
             }   
         }
     }
     
     
     /**
      * funkce napsana pro zjisteni cisla zda spada do intervalu
      */
     private function is_in_interval($value, $start, $end){
        if($value >= $start && $value <= $end)
            return true;
        else
            return false;    
     }
     
     
    /**
     * nova moznost editace connection from / to datumu 
     */   
	function modify_from_date($connection_id = null,$client_id = null){
		if(empty($this->data)){
		    $this->set('client_id',$client_id);
          
		    $this->loadModel('ConnectionClientRequirement');  
            
            /**
             * nacteni seznamu connection, ostatnich zamestanni
             * aby pri editaci byl prehled kde a kdy pracoval..
             */
             $this->ConnectionClientRequirement->bindModel(array(
                'belongsTo'=>array('Company')
             )
             );
            $conection_list = $this->ConnectionClientRequirement->find('all',array(
                'conditions'=>array(
                    'client_id'=>$client_id,
                    'type'=>2,
                    'ConnectionClientRequirement.id !='=>$connection_id
                )
            ));
            $this->set('conection_list',$conection_list);
          
            
            $this->data = $this->ConnectionClientRequirement->read(array('id','from','to'),$connection_id);
            if(empty($this->data) || $this->data == null)
                die('Takové zaměstnání nebylo nalezeno');
		}
        else{
            if(empty($this->data['ConnectionClientRequirement']['from']) && $this->data['ConnectionClientRequirement']['id'] != '')
                die(json_encode(array('result'=>false)));  
            
            $this->loadModel('ConnectionClientRequirement');
            if($this->ConnectionClientRequirement->save($this->data)){  
                unset($this->ConnectionClientRequirement);
                die(json_encode(array('result'=>true)));
            }
            else
                die(json_encode(array('result'=>false)));    
        }
	}
    
    
    
}
?>