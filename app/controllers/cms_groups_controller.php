<?php
Configure::write('debug', 1);
class CmsGroupsController extends AppController
{
    var $name = 'CmsGroups';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex');
    var $components = array('ViewIndex', 'RequestHandler');
    var $uses = array('CmsGroup');
    var $renderSetting = array(
        'SQLfields' => array('id', 'name', 'updated', 'created', 'status', 'text',
            '(SELECT COUNT(id) FROM wapis__cms_users Where cms_group_id = CmsGroup.id and kos = 0) as pocet'
        ),
        'controller' => 'cms_groups',
        'page_caption' => 'Skupiny uživatelů systému',
        'sortBy' => 'CmsGroup.id.ASC',
        'top_action' => array(
            // caption|url|description|permission
            'add_item' => 'Přidat|edit|Pridat popis|add',
            'group' => 'Skupiny|groups|Nadřazené skupiny|groups',
        ),
        'filtration' => array(
            //'CmsGroup-name'		=>	'text|Název|'
        ),
        'items' => array(
            'id' => 'ID|CmsGroup|id|text|',
            'name' => 'Nazev|CmsGroup|name|text|',
            'text' => 'Popis|CmsGroup|text|text|orez#50',
            'pocet' => 'Počet uživatelů|0|pocet|text|',
            'updated' => 'Upraveno|CmsGroup|updated|datetime|',
            'created' => 'Vytvořeno|CmsGroup|created|datetime|'
        ),
        'posibility' => array(
            'status' => 'status|Změna stavu|status',
            'edit' => 'edit|Editace položky|edit',
            'klon' => 'klon|Vytvořit klon skupiny|klon',
            'trash' => 'trash|Do kosiku|trash'
        ),
        'domwin_setting' => array(
            'sizes' => '[1000,1000]',
            'scrollbars' => true,
            'languages' => true,
        )
    );

    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'Administrace' => '#', 'Skupiny uživatelů systému' => '#'));
        if ($this->RequestHandler->isAjax()) {
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
    }

    function edit($id = null)
    {
        $this->autoLayout = false;
        if (empty($this->data)) {
            $this->set('project_list', $this->get_list('AtProject'));

            $folder =& new Folder();
            $folder->cd(MODULS);
            $moduly = $folder->ls();
            $menu_items = $menu_out = $menu_top = array();
            foreach ($moduly[1] as $nm => $modul) {
                include('app/config/permission/' . $modul);
                if (isset($modul_menu) && count($modul_menu) > 0)
                    $menu_items[substr($modul, 4, -12)] = $modul_menu;
                $moduls[] = substr($modul, 4, -12);
            }
            foreach ($moduls as $modul) {
                $fk = array_search($menu_items[$modul]['name'], $menu_top);
                if ($fk !== false) {
                    if (is_array($menu_items[$modul]['child']))
                        foreach ($menu_items[$modul]['child'] as $submenu) {
                            $menu_out[$fk]['child'][] = $submenu;
                        }
                } else {
                    $menu_out[] = $menu_items[$modul];
                    $menu_top[] = $menu_items[$modul]['name'];
                }
            }

            /**
             * Pridani nastaveni usershare - pouze pro nastavnei permission nenastavuje se pro
             */
            $usershare_permission = array(
                'radio' => array(),
                'checkbox' => array(
                    'upload_file' => 'Nahrát soubor',
                    'delete_file' => 'Smazat soubor',
                    'download_file' => 'Stáhnout soubor',
                    'create_folder' => 'Vytvářet složku',
                    'delete_folder' => 'Smazat složku'
                )
            );
            $menu_out[] = array('name' => 'ftp', 'url' => '/ftp/', 'caption' => 'UserShare', 'child' => array());
            $this->permission_list['ftp'] = array('name' => 'UserShare', 'permission' => $usershare_permission);
            $this->set('permission_list', $this->permission_list);


            /**
             * slozky v usershare
             */

            $information['ftp_host'] = $this->logged_user['ftp_relace'];
            $information['ftp_user'] = $this->logged_user['ftp_login'];
            $information['ftp_heslo'] = $this->logged_user['ftp_password'];
            $this->ftp_connect = ftp_connect($information['ftp_host']);
            $login_result = ftp_login($this->ftp_connect, $information['ftp_user'], $information['ftp_heslo']);
            if ((!$this->ftp_connect) || (!$login_result)) die("Chyba připojení na FTP server! Špatné přihlašovací údaje");
            ftp_pasv($this->ftp_connect, true);

            // pokud je jen jedno FTP
            $this->logged_user['ftp_jedno'] = 'web/atep/uploaded/usershare';
            $directory = $this->logged_user['ftp_jedno'];
            $dirs = $this->rawlist_dump($directory);
            $this->set('usershare_folders', $dirs);
            //pr($this->permission_list);

            $this->set('menu_items', $menu_out);
            //echo '<pre>';
            //print_r($menu_out);
            //echo '</pre>';
            if ($id != null) {
                $this->data = $this->CmsGroup->read(null, $id);
            }

            $this->set('group_list', $this->get_list('CmsGroupSuperior'));
            $this->render('edit');
        } else {
            $this->CmsGroup->save($this->data);
            die();
        }
    }

    function rawlist_dump($directory = ".")
    {
        $ftp_rawlist = ftp_rawlist($this->ftp_connect, $directory);
        //echo $directory;
        //pr($ftp_rawlist);
        //die();
        $rawlist = array();
        foreach ($ftp_rawlist as $k => $v) {
            $info = array();
            $vinfo = preg_split("/[\s]+/", $v, 9);

            if (!in_array($vinfo[8], array('.', '..'))) {
                if ($vinfo[0] !== "total") {
                    $info['chmod'] = $vinfo[0];
                    $info['name'] = $vinfo[8];
                    $rawlist[$info['name']] = $info;
                }
            }
        }


        $dir = array();
        foreach ($rawlist as $k => $v) {
            if ($v['chmod']{0} == "d") {
                $dir[$k] = $v;
                if (!in_array($k, array('.', '..')))
                    $dir[$k]['sublist'] = $this->rawlist_dump($directory . '/' . $k);
            }
        }

        return $dir;
    }

    function klon($id = null)
    {
        if ($id != null) {
            $data = $this->CmsGroup->read(null, $id);
            $data['CmsGroup']['name'] = $data['CmsGroup']['name'] . '2';
            unset($data['CmsGroup']['id']);

            //pr($data);
            $this->CmsGroup->create();
            $this->CmsGroup->save($data);
            die(json_encode(array('result' => true)));
        }
        die(json_encode(array('result' => false, 'message' => 'Chyba: prázdné ID')));
    }

    /*
    * sprava jednotlivych skupinn
    */
    function groups()
    {
        $this->loadModel('CmsGroupSuperior');
        $this->set('group_list', $this->CmsGroupSuperior->find(
            'all',
            array(
                'conditions' => array(
                    'CmsGroupSuperior.kos' => 0
                ),
                //	'order'=>'CmsGroupSuperior.created AS'
            )
        ));

        // render
        $this->render('groups/index');

    }

    function group_edit($id = null)
    {
        $this->loadModel('CmsGroupSuperior');
        if (empty($this->data)) {
            if ($id != null) {
                $this->data = $this->CmsGroupSuperior->read(null, $id);
            }

            // render
            $this->render('groups/edit');
        } else {
            if ($this->CmsGroupSuperior->save($this->data))
                die(json_encode(array('result' => true)));
            else
                die(json_encode(array('result' => false, 'message' => 'Chyba behem ukladani nove skupiny')));
        }

    }

}

?>