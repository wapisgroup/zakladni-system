<?php
Configure::write('debug',1);
class ReportFuelConsumptionsController extends AppController {
	var $name = 'ReportFuelConsumptions';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('ReportFuelConsumption');
	var $renderSetting = array(
        'bindModel'=>array('belongsTo'=>array('CmsUser','AtProject','AtProjectCentre')),
		'SQLfields' => '*',
		'controller'=> 'report_fuel_consumptions',
		'page_caption'=>'Report - spotřeby vozidel',
		'sortBy'=>'ReportFuelConsumption.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
			//'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
			//'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			'ReportFuelConsumption-name|'				=>	'text|Název|',
			'ReportFuelConsumption-at_project_id'		=>	'select|Projekt|project_list',
            'ReportFuelConsumption-at_project_centre_id'		=>	'select|Středisko|centre_list'
		),
		'items' => array(
			'id'			=>	'ID|ReportFuelConsumption|id|text|',
			'name'			=>	'Název|ReportFuelConsumption|name|text|',
            'user'			=>	'Přidal|CmsUser|name|text|',
            'project'		=>	'Projekt|AtProject|name|text|',
            'centre'		=>	'Středisko|AtProjectCentre|name|text|',
            
			//'updated'		=>	'Změněno|Accommodation|updated|datetime|',
			'created'		=>	'Vytvořeno|ReportFuelConsumption|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'attach'	=>	'attachs|Přílohy|attach',
			'fuel_trash'	=>	'fuel_trash|Odstranit položku|fuel_trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#'));
		$this->set('project_list',$this->get_list('AtProject'));
		$this->set('centre_list',$this->get_list('AtProjectCentre'));
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			$cetre_list = array();
			$this->loadModel('SettingAccommodationType');
			// load accommodation_type_list_with_price_type
			$this->set('accommodationtype_list', $this->SettingAccommodationType->find('list', array('order'=>'name','conditions'=>array('SettingAccommodationType.kos'=>0))));
			$this->set('accommodationtype_title_list', $this->SettingAccommodationType->find('list', array('order'=>'name','conditions'=>array('SettingAccommodationType.kos'=>0),'fields'=>array('id','price_type'))));
			
			if ($id != null){
				$this->data = $this->ReportFuelConsumption->read(null,$id);
                $cetre_list = $this->get_list('AtProjectCentre',array('at_project_id'=>$this->data['ReportFuelConsumption']['at_project_id']));
			}
            
            
            $this->set('project_list',$this->get_list('AtProject'));
            $this->set('centre_list',$cetre_list);
			$this->render('edit');
		} else {
			$this->data['ReportFuelConsumption']['cms_user_id']=$this->logged_user['CmsUser']['id'];
			$this->ReportFuelConsumption->save($this->data);
            
            $project = self::load_for_email($this->data['ReportFuelConsumption']['at_project_id'],'AtProject',array('name','manager_id'));
		    $st = self::load_for_email($this->data['ReportFuelConsumption']['at_project_centre_id'],'AtProjectCentre',array('name','spravce_majetku_id','reditel_strediska_id'));             
            $replace_list = array(
				'##AtProject.name##' 	=>$project['AtProject']['name'],
                '##AtProjectCentre.name##' 	=>$st['AtProjectCentre']['name'],
                '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
			);
            foreach($this->data['ReportFuelConsumption'] as $item=>$value){
                $replace_list['##ReportFuelConsumption.'.$item.'##'] = $value;
            }
            
            if($this->data['ReportFuelConsumption']['id'] == ''){
                //do systemu byl vložen nový majetek	
                $replace_list['##akce##'] = 'do systemu byl vložen nový report spotřeby';
            }
            else{
                $replace_list['##akce##'] = 'byla provedena změna u reportu spotřeby';
            }
            $this->Email->send_from_template_new(51,array($this->logged_user['CmsUser']['email']),$replace_list);
            
			die();
		}
	}
    
    /**
     * Eliminaci zajistuje viewindex
     * zde pouze odesalni emailu
     */
    function fuel_trash($id = null){
        $this->data = $this->ReportFuelConsumption->read(null,$id);
        
        $project = self::load_for_email($this->data['ReportFuelConsumption']['at_project_id'],'AtProject',array('name','manager_id'));
	    $st = self::load_for_email($this->data['ReportFuelConsumption']['at_project_centre_id'],'AtProjectCentre',array('name','spravce_majetku_id','reditel_strediska_id'));             
        $replace_list = array(
			'##AtProject.name##' 	=>$project['AtProject']['name'],
            '##AtProjectCentre.name##' 	=>$st['AtProjectCentre']['name'],
            '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name'],
            '##akce##'=> 'byl smazán report spotřeby'
		);
        foreach($this->data['ReportFuelConsumption'] as $item=>$value){
            $replace_list['##ReportFuelConsumption.'.$item.'##'] = $value;
        }
        $this->Email->send_from_template_new(51,array($this->logged_user['CmsUser']['email']),$replace_list);
        
        $this->ViewIndex->render_trash($id);
        exit();
    }
    
    function load_for_email($id,$model,$fields = null){
        if(!isset($this->{$model}))
            $this->loadModel($model);
        
        return $this->{$model}->read($fields,$id);   
    }
	
	/**
 	* Seznam priloh
 	*
	* @param $ReportFuelConsumption_id
 	* @return view
 	* @access public
	**/
	function attachs($report_fuel_consumption_id){
		$this->autoLayout = false;
		$this->loadModel('ReportFuelConsumptionAttachment'); 
		$this->ReportFuelConsumptionAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->ReportFuelConsumptionAttachment->findAll(array('ReportFuelConsumptionAttachment.report_fuel_consumption_id'=>$report_fuel_consumption_id,'ReportFuelConsumptionAttachment.kos'=>0)));
		$this->set('report_fuel_consumption_id',$report_fuel_consumption_id);
		unset($this->ReportFuelConsumptionAttachment);
		$this->render('attachs/index');
	}
	
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($report_fuel_consumption_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('ReportFuelConsumptionAttachment'); 
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType'); $this->SettingAttachmentType =& new SettingAttachmentType();
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
			$this->data['ReportFuelConsumptionAttachment']['report_fuel_consumption_id'] = $report_fuel_consumption_id;
			if ($id != null){
				$this->data = $this->ReportFuelConsumptionAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->ReportFuelConsumptionAttachment->save($this->data);
			$this->attachs($this->data['ReportFuelConsumptionAttachment']['report_fuel_consumption_id']);
		}
		unset($this->ReportFuelConsumptionAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($report_fuel_consumption_id, $id){
		$this->loadModel('ReportFuelConsumptionAttachment');
		$this->ReportFuelConsumptionAttachment->save(array('ReportFuelConsumptionAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($report_fuel_consumption_id);
		unset($this->ReportFuelConsumptionAttachment);
	}
	
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('./uploaded/'.$file);
		$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}
}
?>