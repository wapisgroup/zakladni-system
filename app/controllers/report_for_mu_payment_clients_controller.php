<?php
Configure::write('debug', 1);

	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');

    define('CURRENT_YEAR', $_GET['current_year']);
    define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

define('fields','Company.*,ClientWorkingHour.*,AtCompany.name,Client.name,Client.id,Client.cislo_uctu,
    ConnectionClientRequirement.*,RequirementsForRecruitment.* ,CompanyWorkPosition.*,  
    CompanyMoneyItem.*,
    (IFNULL(ClientWorkingHour.drawback,0) + IF(drawback_opp_add = 1,IFNULL(drawback_opp,0),0)) as srazky_celkem,
    IFNULL((SELECT SUM(amount) FROM wapis__report_down_payments WHERE kos = 0 and connection_client_requirement_id = ConnectionClientRequirement.id AND ((year = "'.CURRENT_YEAR.'" AND month = "'.CURRENT_MONTH.'") OR ( year = 0 and month = 0 and (DATE_FORMAT(created,"%Y-%m"))= "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'" ) )),0) as zalohy,
    (
        ClientWorkingHour.salary_per_hour_p
        -
        IFNULL((SELECT SUM(amount) FROM wapis__report_down_payments WHERE kos = 0 and connection_client_requirement_id = ConnectionClientRequirement.id AND ((year = "'.CURRENT_YEAR.'" AND month = "'.CURRENT_MONTH.'") OR (year = 0 and month = 0 and  (DATE_FORMAT(created,"%Y-%m"))= "'.CURRENT_YEAR.'-'.CURRENT_MONTH.'" ) )),0)
    ) as  zbyva_k_vyplate
');
	//Configure::write('debug',1);
class ReportForMuPaymentClientsController extends AppController {
	var $name = 'ReportForMuPaymentClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Excel');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionClientRequirement');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','Company','RequirementsForRecruitment','CompanyWorkPosition'
            ),
			'hasOne'	=>	array(
				'ClientWorkingHour'=>array(
					'conditions' => array(
						'ClientWorkingHour.year'=> CURRENT_YEAR,
						'ClientWorkingHour.month'=> CURRENT_MONTH
					)
				)
			),
			'joinSpec'=>array(
				'CompanyMoneyItem'=>array(
					'foreignKey'=>'ClientWorkingHour.company_money_item_id',
					'primaryKey'=>'CompanyMoneyItem.id',
				),
				'AtCompany' => array(
                    'className' => 'SettingParentCompany',
					'primaryKey' => 'Company.parent_id',
					'foreignKey' => 'AtCompany.id',
					'conditions' => array(
						'AtCompany.kos' => 0
					)
				)
			)
		),
		'controller'	=>	'report_for_mu_payment_clients',
		'SQLfields' 	=>	array(fields),
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			'ConnectionClientRequirement.type'=> 2,
           // 'ConnectionClientRequirement.new'=> 0,
			'ClientWorkingHour.stav IN (3,4)',
			'OR'=>array(
				'CompanyMoneyItem.checkbox_pracovni_smlouva'=> 1,
				'CompanyMoneyItem.checkbox_dohoda'=> 1,
				'CompanyMoneyItem.pausal'=> 1,
                '(CompanyMoneyItem.checkbox_faktura = 1 and Company.stat_id = 1)'
            )
			
		),
		'page_caption'=>'Výplaty pro mzdovou účetní',
		'checkbox_setting' => array(
			'model'			=>	'ClientWorkingHour',
			'col'			=>	'stav',
			'col2'			=>	'stop_payment',
			'stav_array'	=>	array(3),
			'month_year'	=>	false
		),
		'sortBy'=>'Client.name.ASC',
		'top_action' => array(
			'multi_edit'		=>	'Vyplatit hromadně|money_all|Vyplatit hromadně|multi_edit',
			'export_excel' 		=> 	'Export Excel|export_excel|Export Excel|multi_edit',
		),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
			'Company-coordinator_id'					=>	'select|COO|koordinator_list',
			'Company-client_manager_id'					=>	'select|CM|client_manager_list',
			'GET-current_year'							=>	'select|Rok|actual_years_list',
			'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'				=>	'ID|ClientWorkingHour|id|hidden|',
			//'cc'				=>	'Client č.|ClientWorkingHour|client_number|text|',
			'company'			=>	'Společnost|Company|name|text|',
			'atcompany'			=>	'Nadřazená společnost|AtCompany|name|text|',
			'client'			=>	'Klient|Client|name|text|',	
            'cu'			    =>	'Č.účtu|Client|cislo_uctu|text|',	
            'forma_mzdy'		=>	'Forma|CompanyMoneyItem|name|text|',
			//'nastup'			=>	'Nástup|ConnectionClientRequirement|from|date|',
			//'ukonceni'			=>	'Ukončení|ConnectionClientRequirement|to|date|',
			'norhod'			=>	'FPD|ClientWorkingHour|standard_hours|text|',
			'svatky'			=>	'Sv|ClientWorkingHour|svatky|text|',
			'vikendy'			=>	'Ví|ClientWorkingHour|vikendy|text|',
			'pn'				=>	'PN|ClientWorkingHour|pracovni_neschopnost|text|',
			'dovolena'			=>	'Do|ClientWorkingHour|dovolena|text|',
			'prescasy'			=>	'Př|ClientWorkingHour|prescasy|text|',
            'paragraf'			=>	'PG|ClientWorkingHour|paragraf|text|',
			'hour_s1'			=>	'S1|ClientWorkingHour|hour_s1|text|',
			'hour_s2'			=>	'S2|ClientWorkingHour|hour_s2|text|',
			'hour_s3'			=>	'S3|ClientWorkingHour|hour_s3|text|',
			'celkem'			=>	'Celkem|ClientWorkingHour|celkem_hodin|text|',
			'ps'				=>	'PS|ClientWorkingHour|salary_part_1_p|text|',
			'd'					=>	'D|ClientWorkingHour|salary_part_3_p|text|',
			//'f'					=>	'F|ClientWorkingHour|salary_part_2_p|text|',
			//'c'					=>	'C|ClientWorkingHour|salary_part_4_p|text|',
			'total'				=>	'Total|ClientWorkingHour|salary_per_hour_p|text|',
            'srazka'			=>	'Srážka|0|srazky_celkem|text|',
            'fa_dpc_calculation'=>	'DPČ|ClientWorkingHour|fa_dpc_calculation|text|',
            'zalohy'			=>	'Zálohy,VVH|0|zalohy|text|',
            'zbyva_k_vyplate'	=>	'Zbýva k výplatě|0|zbyva_k_vyplate|text|',
			'odmena1'			=>	'Odměna 1|ClientWorkingHour|odmena_1|text|',
			//'odmena2'			=>	'Odměna 2|ClientWorkingHour|odmena_2|text|',
			'stravne'			=>	'Stravné|CompanyMoneyItem|vlozeny_prispevek_na_stravne|text|',
			'cestovne'			=>	'Cestovné|CompanyMoneyItem|vlozeny_prispevek_na_cestovne|text|',
			'stav'				=>	'Stav|ClientWorkingHour|stav|var|kalkulace_stav_list',
			'stop_payment'		=>	'Výp.|ClientWorkingHour|stop_payment|var|ano_ne'	
		),
        'items_hint'=>array(
            'client'=>array(
                'Nástup|ConnectionClientRequirement|from|date|',
			    'Ukončení|ConnectionClientRequirement|to|date|',
                'Koměntář k docházce|ClientWorkingHour|comment|text|',
            )
        ),
        /**
         * items_group nova funkce v renderu,
         * nastavujeme start a end colspanu(slouceni td) a count
         * zatim moznost jen jednoho colspanu
         * @var colspan_data => sloupec ktery se ma zobrazovat 
         * @var condition => podminka za ktere se ma group provádět, zatim pouze na items hodnotu a pouze jednu
         * @var css => nastaveni class pro kaskadu
         */
        'items_group'=>array(
            'condition'=>array(
                'model'=>'CompanyMoneyItem',
                'col'=>'pausal',
                'value'=>1
            ),
            'start'=>'ps',
            'count'=>3,
            'colspan_data'=> array(
                'model'=>'ClientWorkingHour',
                'col'=>'ps_pausal_hmm',
            ),
            'text'=>'Hrubá mzda: ',
            'css'=>'text_center color_red'
        ),
		'posibility' => array(
			'stop_status'			=>	'stop|Pozastavit výplatu|stop_status',
			'show'			=>	'odpracovane_hodiny|Karta docházky|show',
			'edit'			=>	'money|Vyplatit|edit'
		),
        'class_tr'=>array(
			'class'=>'color_blue',
			'model'=>'CompanyMoneyItem',
			'col'=>'name',
			'value'=>'0010'
		)
	);

    function beforeFilter(){
        parent::beforeFilter();
        if (isset($_GET['excel'])){
            $this->renderSetting['no_limit'] = true;
        }
    }

    
	function export_excel(){
	   $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  

         $fields_sql = array(
            'Client.name',
            'ConnectionClientRequirement.company_id',
            'Company.coordinator_id',
            'Company.client_manager_id',
            
            'Company.name',
            'ClientWorkingHour.*',
            'AtCompany.name',
            'CompanyMoneyItem.name',
            'ConnectionClientRequirement.from',
            'ConnectionClientRequirement.to',
            'ConnectionClientRequirement.company_money_item_id',
            'CompanyMoneyItem.vlozeny_prispevek_na_stravne',
            'CompanyMoneyItem.vlozeny_prispevek_na_cestovne',
            'CompanyMoneyItem.pausal',
            'CompanyWorkPosition.name',
            'CompanyWorkPosition.standard_hours',
            'Company.stat_id',
        );
         
               
        $criteria = $this->ViewIndex->filtration();    
        $render_condition = $this->ViewIndex->foreach_SQLcondition($this->renderSetting['SQLcondition']);    
          
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;     
        $this->ConnectionClientRequirement->bindModel($this->renderSetting['bindModel'],false);   
        $count = $this->ConnectionClientRequirement->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT ConnectionClientRequirement.id) as count"),
        		'conditions'=>am($criteria,$render_condition),
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];
        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->ConnectionClientRequirement->bindModel($this->renderSetting['bindModel'],false);
            foreach($this->ConnectionClientRequirement->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>am($criteria,$render_condition),
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1,
                    'order'=>'Client.name ASC'
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
         die();
	}
	
	function index(){
		//pr($this->viewVars['items']);
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Výplaty pro mzdovou účetní'=>'#'));
			
		$this->loadModel('CmsUser');
			$this->set('client_manager_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>3))));
			$this->set('koordinator_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>4))));
		$this->loadModel('Company');	
			$this->set('company_list',$this->Company->find('list',array('order'=>'name ASC')));
		
		$this->loadModel('CompanyMoneyItem');
		$cmi_list = $this->CompanyMoneyItem->find('list',array(
				'conditions'=>array()//'CompanyMoneyItem.kos'=>0)
		));
		$nhmi_list = $this->CompanyMoneyItem->find('list',array(
				'conditions'=>array(),//'CompanyMoneyItem.kos'=>0),
				'fields'=>array('id','normo_hodina')
		));
		$this->loadModel('SettingStatPd');
		$pd_list = $this->SettingStatPd->find('list',array(
				'conditions'=>array(
					'kos'=>0,
					'mesic'=>CURRENT_MONTH,
					'rok'=>CURRENT_YEAR
				),
				'fields'=>array('setting_stat_id','pocet')
		));

		foreach($this->viewVars['items'] as &$item){
			//nastaveni forem odmeny
			if(isset($item['CompanyMoneyItem'])){
				// pokud jsou rozdilna v ulozene dochazce nez v connection
				if($item['ClientWorkingHour']['company_money_item_id'] != $item['ConnectionClientRequirement']['company_money_item_id'] && $item['ClientWorkingHour']['company_money_item_id']<>0)
					$item['CompanyMoneyItem']['name'] = $cmi_list[$item['ClientWorkingHour']['company_money_item_id']];
			}
            
            //if($item['CompanyMoneyItem']['pausal'] == 1)
                //pr($this->viewVars);
			
			//#PRESCASY - START##
				$pd = 0;
			
				//#vyber pocet normo hodin 
				// bud berem hlavni normo hodiny z profese
				if($item['CompanyWorkPosition']['standard_hours']<>0){
					$nh = $item['CompanyWorkPosition']['standard_hours'];
				}
				else{ // nebo z nastavene formy odmeny
					$nh_money_item = 0;
					// pokud jsou rozdilna v ulozene dochazce nez v connection vezmi z dochazky
					if($item['ClientWorkingHour']['company_money_item_id'] != $item['ConnectionClientRequirement']['company_money_item_id'] && $item['ClientWorkingHour']['company_money_item_id']<>0)
						$nh_money_item = $nhmi_list[$item['ClientWorkingHour']['company_money_item_id']];
					else // jinak ber nastavnei formy odmeny z connection
						$nh_money_item = $nhmi_list[$item['ConnectionClientRequirement']['company_money_item_id']];
						
					$nh = ($nh_money_item <> 0 ? $nh_money_item : 0);
				}
				
				//pokud exituji nastaveny pracovni fond pro tento mesic a rok
				if(array_key_exists($item['Company']['stat_id'],$pd_list))
					$pd = $pd_list[$item['Company']['stat_id']];
				if($pd <> 0 && $nh<>0){
					$prescas = ($nh * $pd) - $item['ClientWorkingHour']['celkem_hodin'];
					$item['ClientWorkingHour']['prescasy'] = ($prescas < 0 ? ($prescas*-1) : 0);
				}
				else 
					$item['ClientWorkingHour']['prescasy'] = '---';
			//#PRESCASY - END##
		}
		
		if (isset($_GET['excel'])){
			$this->autoLayout = false;
			echo $this->render('../system/excel');
			die();
		}
	
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

	
	function money($id){
		$this->loadModel('ClientWorkingHour');
		$this->ClientWorkingHour->id = $id;
		$this->ClientWorkingHour->saveField('stav',4);
		unset($this->ClientWorkingHour);
		die();
	}

	function stop($id,$status){
		$this->loadModel('ClientWorkingHour');
		$this->ClientWorkingHour->id = $id;
		$this->ClientWorkingHour->saveField('stop_payment',$status==1 ? 0 :1);
		unset($this->ClientWorkingHour);
		die();
	}

	function odpracovane_hodiny($id = null, $year = null, $month = null){
		echo $this->requestAction('employees/odpracovane_hodiny/'.$id.'/'.$year.'/'.$month.'/');
		die();
	}

	function money_all(){
		$model = $this->renderSetting["checkbox_setting"]['model'];
		$this->loadModel($model);
		foreach($this->params['url']['data'][$model]['id'] as $key => $on){
			$this->$model->id = $key;
			$this->$model->saveField('stav',4);
		}
		unset($this->$model);	
		die();
	}

	
	
}
?>