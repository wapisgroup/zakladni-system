<?php
Configure::write('debug',1);
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 12.11.2009
 */

class HistorySetsController extends AppController {
	var $name = 'HistorySets';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','SmsClient');
	var $uses = array('HistorySet');
	var $renderSetting = array(
		'SQLfields' => array('*'),
		'controller'=> 'history_sets',
		'page_caption'=>'Historie - definice',
		'sortBy'=>'HistorySet.created.DESC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'	=>	'Přidat|edit|Přidání novou definici historie|add'
		),
		'filtration' => array(
			'HistorySet-cms_user_id'		=>	'select|Uživatel|cms_user_list',
		),
		'items' => array(
			'id'			=>	'ID|HistorySet|id|hidden|',
			'controller'	=>	'Kategorie|HistorySet|category_id|viewVars|history_category',
			'akce'			=>	'Akce|HistorySet|action_id|viewVars|history_type_list',
			'datum'			=>	'Datum|HistorySet|created|datetime|'
		),
		'posibility' => array(
            'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace definice historie|edit'		
		), 
        'domwin_setting' => array(
            'sizes' => '[1000,1000]', 
            'scrollbars' => true, 
            'languages' => true 
        )
	);
	
	/**
	 * 
	 * @return view
	 */
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/',$this->renderSetting['page_caption']=>'#'));
	
		$this->loadModel('CmsUser');
		$this->CmsUser->query("SET NAMES 'utf8'");
		$this->set('cms_user_list',array(-99=>'Nepřihlášen')+$this->CmsUser->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0,'status'=>1))));
		unset($this->CmsUser);
        
       	$this->loadModel('HistoryType');
		$this->set('history_type_list',$this->HistoryType->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0))));
		unset($this->HistoryType);
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	
	function edit($id = null){
		if (empty($this->data)){
            $this->loadModel('HistoryType');
		    $this->set('history_type_list',$this->HistoryType->find('list',array('order'=>'name ASC','conditions'=>array('kos'=>0))));
            unset($this->HistoryType);
        
			if ($id != null){
				$this->data = $this->HistorySet->read(null,$id);
			}	
		} else {
			if ($this->HistorySet->save($this->data))
				die(json_encode(array('result'=>true)));
			else
				die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani do DB')));
		}
	}
	
	
}
?>