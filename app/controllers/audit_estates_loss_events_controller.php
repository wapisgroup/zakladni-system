<?php
	Configure::write('debug',0);
	define('fields','AuditEstateLossEvent.*,Client.*, AuditEstate.*,AtCompany.*,AuditEstateLossEventCmsUser.name,
        ClientSpravceMajetku.name,ClientSpravceMajetku.mobil1,
        ( 
		  if(AuditEstateLossEvent.type = 1,
            AuditEstate.name,
            AuditEstateLossEvent.audit_estate_name
          )  
		) as majetek,
        ( 
		  if(AuditEstateLossEvent.type = 1,
            AuditEstate.imei_sn_vin,
            AuditEstateLossEvent.imei_sn_vin
          )  
		) as imei_sn_vin,	
        (
		  if(AuditEstateLossEvent.type = 1,
            AuditEstate.internal_number,
            AuditEstateLossEvent.internal_number
          )  
		) as internal_number,
		(
          if(AuditEstateLossEvent.type = 1,
            AtCompany.name,
            AuditEstateLossEvent.at_company_name
          )  
		) as firma,
        (
          if(AuditEstateLossEvent.type = 1,
            Client.name,
            AuditEstateLossEvent.osoba
          )  
		) as vlastnik_majetku,
        ( 
          CONCAT_WS(" ",IFNULL(AuditEstateLossEvent.expected_price,0),if(AuditEstateLossEvent.currency = 0,",- EUR",",- Kč"))  
		) as expected_price	,
        ( 
          CONCAT_WS(" ",IFNULL(AuditEstateLossEvent.fa_price,0),if(AuditEstateLossEvent.currency = 0,",- EUR",",- Kč"))  
		) as fa_price,	
        ( 
          SELECT COUNT(id) FROM wapis__audit_estate_loss_event_attachments WHERE audit_estate_loss_event_id = AuditEstateLossEvent.id and kos = 0
        ) as count_attachment
	');
class AuditEstatesLossEventsController extends AppController {
	var $name = 'AuditEstatesLossEvents';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Email','Upload');
	var $uses = array('AuditEstateLossEvent');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'=>array('AuditEstate','Client'),
			'joinSpec'=>array(
                'AtCompany'=>array(
    				'className'=>'AtCompany',
    				'primaryKey'=>'AtCompany.id',
    				'foreignKey'=>'AuditEstate.at_company_id'
			    ),
                'AtProjectCentre'=>array(
    				'className'=>'AtProjectCentre',
    				'primaryKey'=>'AtProjectCentre.id',
    				'foreignKey'=>'AuditEstate.at_project_centre_id'
			    ),
                'SpravceMajetku'=>array(
    				'className'=>'CmsUser',
    				'primaryKey'=>'SpravceMajetku.id',
    				'foreignKey'=>'AtProjectCentre.spravce_majetku_id'
			    ),
                'AuditEstateLossEventCmsUser'=>array(
                    'className'=>'CmsUser',
                    'primaryKey'=>'AuditEstateLossEventCmsUser.id',
    				'foreignKey'=>'AuditEstateLossEvent.cms_user_id'
                ),
                'ClientSpravceMajetku'=>array(
                    'className'=>'Client',
                    'primaryKey'=>'ClientSpravceMajetku.id',
    				'foreignKey'=>'SpravceMajetku.at_employee_id'
                )
			)
		),
		'SQLfields' =>array(fields),
		'SQLcondition' => array(
			//'AuditEstate.elimination_date IS NULL',
		),
		'controller'=> 'audit_estates_loss_events',
		'page_caption'=>'Škodové události',
		'sortBy'=>'AuditEstateLossEvent.id.DESC',
		//'group_by'=>'AuditEstate.id',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat událost|add',
            'export_excel' => 'Excel|export_excel|Export Excel|export_excel', 
		),
		'filtration' => array(
			'AuditEstate-name|AuditEstateLossEvent-audit_estate_name'			=>	'text|Název majetku|',
			'AuditEstate-imei_sn_vin|AuditEstateLossEvent-imei_sn_vin'		=>	'text|IMEI/SN/VIN|',
			'AuditEstateLossEvent-osoba|Client-name'	=>	'text|Osoba|',
			'AuditEstate-at_company_id'	    	=>	'select|Firma|company_list',
            'AuditEstateLossEvent-snp_stav'	    	=>	'select|Stav|snp_stav_list',	
            'AuditEstateLossEvent-currency'	    	=>	'select|Měna|currency_list'				
		),
		'items' => array(
			'id'		=>	'ID|AuditEstateLossEvent|id|text|',
			'name'		=>	'Název|AuditEstateLossEvent|audit_estate_name|text|',
			'imei'	      =>	'IMEI/SN/VIN|0|imei_sn_vin|text|',
			'int_numb'	      =>	'Int. číslo|0|internal_number|text|',
			'company'	=>	'V majetku firmy|0|firma|text|',
            'vlastnik_majetku'	=>	'Osoba v době škodové události|0|vlastnik_majetku|text|',
		    'date_event'	=>	'Datum škodové události|AuditEstateLossEvent|date_event|date|', 
            'date_to_repair'	=>	'Datum odeslání do opravy|AuditEstateLossEvent|date_to_repair|date|',   
            'date_from_repair'	=>	'Datum přijetí z opravy|AuditEstateLossEvent|date_from_repair|date|',   
        	'expected_price'	=>	'Předpokládáná cena|0|expected_price|text|', 
            'fa_price'	=>	'Fakturovaná cena|0|fa_price|text|', 
            'stav'	=>	'Stav|AuditEstateLossEvent|snp_stav|var|snp_stav_list', 
            //'count_attachment'	=>	'CA|0|count_attachment|text|', 
			//'created'	=>	'Vytvořeno|AuditEstate|created|datetime|',
			//'updated'	=>	'Změněno|AuditEstate|updated|datetime|'
		),
		'posibility' => array(
			'edit'		        =>	'edit|Editace položky|edit',
            'show'		        =>	'show|Detail položky|show',
            'attach'	        =>	'attachs|Přílohy|attach',
            'print'	         	=>	'form_template|Šablony pro tisk|print',	
            'trash'		        =>	'delete_estate_loss_event|Smazat položku|trash',
		),
        'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
			'defined_lang'	=> "['cz','en','de']",
		),  
        'only_his2'=>array(
            'select_connection'=>true,
            'where_model'=>'AuditEstate',
            'where_col'=>'at_project_id'
        ),
        'sum_variables_on_top'=>array(
            'path'=>'../audit_estates_loss_events/filtration_top'
        )      
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));
		$this->set('company_list',$this->get_list('AtCompany'));           
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox','wysiwyg_old/wswg'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox','../js/wysiwyg_old/wswg'));
		
        // celkova cena za polozky a pronajem
            $this->AuditEstateLossEvent->bindModel($this->renderSetting['bindModel']);
    		$rr = $this->AuditEstateLossEvent->find('all',array(
                'conditions'=>$this->ViewIndex->criteria,
                'fields'=>array('AuditEstateLossEvent.currency,type_of_event,SUM(fa_price) as cena'),
                'group'=>'type_of_event,AuditEstateLossEvent.currency'
            ));
            $this->set('filtration_sum_variables',$rr);    
 
        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
    function show($id = null){
        return self::edit($id,true);
    }
    
	function edit($id = null,$show = false){
		if (empty($this->data)){
		    $client_list = array(0=>'Zvolte datum události');
			if ($id != null){
			    $this->AuditEstateLossEvent->bindModel(array(
                    'belongsTo'=>array('AuditEstate','CmsUser'),
                    'joinSpec'=>array(
                        'AtCompany'=>array(
            				'className'=>'AtCompany',
            				'primaryKey'=>'AtCompany.id',
            				'foreignKey'=>'AuditEstate.at_company_id'
        			    ),
                        'AtProjectCentre'=>array(
            				'className'=>'AtProjectCentre',
            				'primaryKey'=>'AtProjectCentre.id',
            				'foreignKey'=>'AuditEstate.at_project_centre_id'
        			    )
        			)
                )); 
				$this->data = $this->AuditEstateLossEvent->read(array('AuditEstateLossEvent.*,AuditEstate.*,AtCompany.name,AtProjectCentre.name,CmsUser.name'),$id);
            
                $this->data['vyplnil'] = $this->data['CmsUser']['name'];
                
                if($this->data['AuditEstateLossEvent']['type'] == 1){
                    $this->data['old_send_on_stav'] = $this->data['AuditEstateLossEvent']['send_on_stav'];
                    $this->data['AuditEstateLossEvent']['audit_estate_name'] = $this->data['AuditEstate']['name'];
                    $this->data['AuditEstateLossEvent']['imei_sn_vin'] = $this->data['AuditEstate']['imei_sn_vin'];
                    $this->data['AuditEstateLossEvent']['internal_number'] = $this->data['AuditEstate']['internal_number'];
                    $this->data['AuditEstateLossEvent']['at_company_name'] = $this->data['AtCompany']['name'];
                    $this->data['AuditEstateLossEvent']['at_project_centre_name'] = $this->data['AtProjectCentre']['name'];
                    
                    //$client_list = self::find_audit_estated_connections($this->data['AuditEstateLossEvent']['date_event'],$this->data['AuditEstateLossEvent']['audit_estate_id'],false);
                    //$client_list = Set::combine($client_list,'/Client/id','/Client/name');
                }
            }
            
            //seznam in. zamestnancu
            $this->loadModel('ConnectionClientAtCompanyWorkPosition'); 
            $this->ConnectionClientAtCompanyWorkPosition->query('SET NAMES UTF8');
            $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
                'belongsTo'=>array('Client')
            ));
			$person_list = $this->ConnectionClientAtCompanyWorkPosition->find('all',array(							
					'conditions'=>array(
						//'ConnectionClientAtCompanyWorkPosition.datum_to'=>'0000-00-00',
                        'Client.kos'=>0,
                        'Client.name !='=>''
					)
                    ,'fields'=>array('DISTINCT Client.name','ConnectionClientAtCompanyWorkPosition.id','Client.id')
			        ,'order'=>'Client.name ASC'
                    ,'group'=>'Client.name' 
            ));
            $this->set('client_list',Set::combine($person_list,'{n}.Client.id','{n}.Client.name'));
            
            //$this->set('client_list',$client_list);
            $this->set('show',$show);
			$this->render('edit');
		} else {
		    if($this->data['AuditEstateLossEvent']['id'] == ''){
                  $this->data['AuditEstateLossEvent']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            }

		    $this->AuditEstateLossEvent->save($this->data['AuditEstateLossEvent']);
           
            /**
             * Zaskrtnute poslani do stavu majetku
             */
            if(
                $this->data['AuditEstateLossEvent']['type'] == 1 &&
                (
                    $this->data['old_send_on_stav'] == '' || 
                    ($this->data['old_send_on_stav'] != $this->data['AuditEstateLossEvent']['send_on_stav'])
                )
                &&
                    isset($this->data['AuditEstateLossEvent']['send_on_stav']) 
                && 
                (
                    $this->data['AuditEstateLossEvent']['send_on_stav'] == 1 ||
                    $this->data['AuditEstateLossEvent']['send_on_stav'] == 'on'
                )
            ){
                $this->loadModel('AuditEstateRepair');
                $this->loadModel('AuditEstate');
                $ae = $this->AuditEstate->read('currency',$this->data['AuditEstateLossEvent']['audit_estate_id']);
                
                $price = $this->data['AuditEstateLossEvent']['fa_price'];
                if($this->data['AuditEstateLossEvent']['currency'] != $ae['AuditEstate']['currency']){
                    $typ_convert = ($ae['AuditEstate']['currency'] == 0?1:2);
                    $price = self::convert_price($price,$typ_convert);
                }
                $save_data = array(
                    'audit_estate_id'=>$this->data['AuditEstateLossEvent']['audit_estate_id'],
                    'name'=>$this->data['AuditEstateLossEvent']['name'],
                    'price'=>$price,
                    'currency'=>$ae['AuditEstate']['currency'],
                    'date_repair'=>$this->data['AuditEstateLossEvent']['date_from_repair'],
                );
                $this->AuditEstateRepair->save($save_data);
            } 
            
            if($this->data['AuditEstateLossEvent']['client_id'] != '' && $this->data['AuditEstateLossEvent']['type'] == 1){
                $najomca = self::load_for_email($this->data['AuditEstateLossEvent']['client_id'],'Client',array('name','email'));

                $this->loadModel('ConnectionClientAtCompanyWorkPosition');
                $employee = $this->ConnectionClientAtCompanyWorkPosition->find('all',array('conditions'=>array(
                    'ConnectionClientAtCompanyWorkPosition.client_id'=>$this->data['AuditEstateLossEvent']['client_id'],
                    'or'=>array('ConnectionClientAtCompanyWorkPosition.datum_to'=>'0000-00-00', 'ConnectionClientAtCompanyWorkPosition.datum_to >='=>date('Y-m-d'))
                )));

                $najemce = $najomca['Client']['name'];

                if($employee && Count($employee) > 0 ){
                    $add_mail = $najomca['Client']['email'];
                }else{
                    $add_mail = null;
                }


            }
            else{
                $add_mail = null;
                $najemce = $this->data['AuditEstateLossEvent']['osoba'];
            }  
            
            
            if($this->data['AuditEstateLossEvent']['audit_estate_id'] != ''){              
                $this->loadModel('AuditEstate'); 
                $this->AuditEstate->bindModel(array('belongsTo'=>array('AtProjectCentre')));
                $at_centre = $this->AuditEstate->read(array('AtProjectCentre.reditel_strediska_id'),$this->data['AuditEstateLossEvent']['audit_estate_id']);
                unset($this->AuditEstate);
                    
                $this->Email->set_company_data(array(
                        'reditel_strediska_id'=>$at_centre['AtProjectCentre']['reditel_strediska_id']
                ));
            }
		   
             // Odeslani emailu - nova udalosti
			if ($this->data['AuditEstateLossEvent']['id'] == ''){
				$replace_list = array(
                    '##najemce##' 	=>$najemce,
					'##mena##' 	=>$this->currency_list[$this->data['AuditEstateLossEvent']['currency']],
                    '##typ_skod_udal##' 	=>$this->list_type_of_loss_events[$this->data['AuditEstateLossEvent']['type_of_event']],
                    '##datum_skod_udalosti##' 	=>self::mail_date($this->data['AuditEstateLossEvent']['date_event']),
                    '##stav_reseni##' 	=>$this->snp_stav_list[$this->data['AuditEstateLossEvent']['snp_stav']],
                    
                    '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
				);
                foreach($this->data['AuditEstateLossEvent'] as $item=>$value){
                    $replace_list['##AuditEstateLossEvent.'.$item.'##'] = $value;
                }
                
                $mails = array(0=>$this->logged_user['CmsUser']['email']);
                if($add_mail != null)
                    $mails[] = $add_mail;
                
				$this->Email->send_from_template_new(38,$mails,$replace_list);
			}
            else{//editace - email
                if($this->data['AuditEstateLossEvent']['cms_user_id'])
                    $vyplnil = self::load_for_email($this->data['AuditEstateLossEvent']['cms_user_id'],'CmsUser',array('name','email'));
                else
                    $vyplnil = null;  
                         
                $replace_list = array(
					'##najemce##' 	=>$najemce,
                    '##vyplnil##' 	=>($vyplnil != null?$vyplnil['CmsUser']['name']:''),
					'##mena##' 	=>$this->currency_list[$this->data['AuditEstateLossEvent']['currency']],
                    '##typ_skod_udal##' 	=>$this->list_type_of_loss_events[$this->data['AuditEstateLossEvent']['type_of_event']],
                    '##datum_skod_udalosti##' 	=>self::mail_date($this->data['AuditEstateLossEvent']['date_event']),
                    '##stav_reseni##' 	=>$this->snp_stav_list[$this->data['AuditEstateLossEvent']['snp_stav']],
                    
					'##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
				);
                foreach($this->data['AuditEstateLossEvent'] as $item=>$value){
                    $replace_list['##AuditEstateLossEvent.'.$item.'##'] = $value;
                }
                $mails = array(0=>$this->logged_user['CmsUser']['email']);
                if($vyplnil != null)
                    $mails[] = $vyplnil['CmsUser']['email'];
                if($add_mail != null)
                    $mails[] = $add_mail;    
                    
                $this->Email->send_from_template_new(39,$mails,$replace_list); 
			} 
            
			die();
		}
	}
    
    function delete_estate_loss_event($id){
         $this->AuditEstateLossEvent->bindModel(array(
                    'belongsTo'=>array('AuditEstate','CmsUser'),
                    'joinSpec'=>array(
                        'AtCompany'=>array(
            				'className'=>'AtCompany',
            				'primaryKey'=>'AtCompany.id',
            				'foreignKey'=>'AuditEstate.at_company_id'
        			    ),
                        'AtProjectCentre'=>array(
            				'className'=>'AtProjectCentre',
            				'primaryKey'=>'AtProjectCentre.id',
            				'foreignKey'=>'AuditEstate.at_project_centre_id'
        			    )
        			)
                )); 
		$this->data = $this->AuditEstateLossEvent->read(array('AuditEstateLossEvent.*,AuditEstate.*,AtCompany.name,AtProjectCentre.name,AtProjectCentre.reditel_strediska_id,CmsUser.name,CmsUser.email'),$id);
        if($this->data['AuditEstateLossEvent']['client_id'] != '' && $this->data['AuditEstateLossEvent']['type'] == 1){
            $najomca = self::load_for_email($this->data['AuditEstateLossEvent']['client_id'],'Client',array('name','email'));
            //Pokud je již dany klient propuštěn tak nezasílat email
            $this->loadModel('ConnectionClientAtCompanyWorkPosition');
            $employee = $this->ConnectionClientAtCompanyWorkPosition->find('all',array('conditions'=>array(
                'ConnectionClientAtCompanyWorkPosition.client_id'=>$this->data['AuditEstateLossEvent']['client_id'],
                'or'=>array('ConnectionClientAtCompanyWorkPosition.datum_to'=>'0000-00-00', 'ConnectionClientAtCompanyWorkPosition.datum_to >='=>date('Y-m-d'))
            )));
            $najemce = $najomca['Client']['name'];

            if($employee && Count($employee) > 0){
                $add_mail = $najomca['Client']['email'];
            }else{
                $add_mail = null;
            }
        }
        else{
            $add_mail = null;
            $najemce = $this->data['AuditEstateLossEvent']['osoba'];
        }         
        $replace_list = array(
            '##najemce##' 	=>$najemce,
            '##vyplnil##' 	=>$this->data['CmsUser']['name'],		
			'##mena##' 	=>$this->currency_list[$this->data['AuditEstateLossEvent']['currency']],
            '##typ_skod_udal##' 	=>$this->list_type_of_loss_events[$this->data['AuditEstateLossEvent']['type_of_event']],
            '##datum_skod_udalosti##' 	=>self::mail_date($this->data['AuditEstateLossEvent']['date_event']),
            '##stav_reseni##' 	=>$this->snp_stav_list[$this->data['AuditEstateLossEvent']['snp_stav']],
            
            '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
		);
        foreach($this->data['AuditEstateLossEvent'] as $item=>$value){
            $replace_list['##AuditEstateLossEvent.'.$item.'##'] = $value;
        }
        
        $mails = array(0=>$this->logged_user['CmsUser']['email']);
        if(isset($this->data['CmsUser']['email']) && $this->data['CmsUser']['email'] != null)
            $mails[] = $this->data['CmsUser']['email'];
        if($add_mail != null){
            $mails[] = $add_mail; 
        }
        
        if($this->data['AuditEstateLossEvent']['audit_estate_id'] != '')              
            $this->Email->set_company_data(array(
                    'reditel_strediska_id'=>$this->data['AtProjectCentre']['reditel_strediska_id']
            ));
            
		$this->Email->send_from_template_new(40,$mails,$replace_list);
        
        $this->ViewIndex->render_trash($id);
        exit();
    }
    
    function convert_price($price,$typ = 1){
        $kurz = (float)$this->zjistiKurz('EUR');//vraci hodnotu v korunach
        if($typ == 1){//CZK  na EUR
            return round((float)($price/$kurz),2);
        }
        else{//EUR na CZK
            return round((float)($price*$kurz),2);
        }
    }
    
    function load_for_email($id,$model,$fields = null){
        if(!isset($this->{$model}))
            $this->loadModel($model);
        
        return $this->{$model}->read($fields,$id);   
    }
    
    function mail_date($datum = null,$format = 'd.m.Y'){
        if($datum == null || $datum == '' || $datum == '0000-00-00')
            return false;
        else
            return  date($format,strtotime($datum));   
    }
    
    /**
     * vyhledavani majetku podle nazvu v DB
     * 
     */
    function find_audit_estates($name){
        if(strlen($name) > 2){
            $this->loadModel('AuditEstate');
            $this->AuditEstate->bindModel(array('belongsTo'=>array('AtCompany','AtProjectCentre')));
            $find = $this->AuditEstate->find('all',array(
                'conditions'=>array(
                    'AuditEstate.name LIKE "%'.$name.'%"',
                    'AuditEstate.kos'=>0,
                    'AuditEstate.elimination_date IS NULL'
                ),'fields'=>array(
                    'AuditEstate.id','AuditEstate.name','imei_sn_vin','internal_number',
                    'AtCompany.name','AtProjectCentre.name'
                )
            ));
            if($find){
               /// pr($find);
                die(json_encode(array('result'=>true,'finds'=>$find))); 
            }
            else
                die(json_encode(array('result'=>false)));
        }   
    }
    
    /**
     * Vyhledavani osoby ktera v danem datumu mela u sebe majetek
     * 22.8.2011 - vypnuto nacitame seznam vsech int. zamestnancu
     */
    function find_audit_estated_connections($date,$audit_estate_id,$die = true){
        if($date != null && $audit_estate_id != ''){
            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array('belongsTo'=>array('Client')));
            $find = $this->ConnectionAuditEstate->find('all',array(
                'conditions'=>array(
                    'ConnectionAuditEstate.audit_estate_id'=>$audit_estate_id,
                    "((ConnectionAuditEstate.created >= '".$date."'))",
		            '((ConnectionAuditEstate.to_date <= "'.$date.'") OR (ConnectionAuditEstate.to_date = "0000-00-00"))',
		             
                ),'fields'=>array('DISTINCT Client.id','Client.name')
            ));
            if($find){
                if($die)
                    die(json_encode(array('result'=>true,'finds'=>$find))); 
                else return $find;    
            }
            else{ //kdyz majetek neni prirazen ma ho spravce majetku
                $find = null;
                $this->loadModel('AuditEstate');
                $this->AuditEstate->bindModel(array(
                    'belongsTo'=>array('AtProjectCentre'),
                    'joinSpec'=>array(
                        'SpravceMajetku'=>array(
            				'className'=>'CmsUser',
            				'primaryKey'=>'SpravceMajetku.id',
            				'foreignKey'=>'AtProjectCentre.spravce_majetku_id'
        			    )
        			)
                ));
                $sp = $this->AuditEstate->find('first',array(
                    'conditions'=>array(
                        'AuditEstate.id'=>$audit_estate_id
                    ),'fields'=>array('SpravceMajetku.at_employee_id','SpravceMajetku.name')
                ));
                $find[] = array('Client'=>array('id'=>$sp['SpravceMajetku']['at_employee_id'],'name'=>$sp['SpravceMajetku']['name']));
                
                if($die)
                    die(json_encode(array('result'=>true,'finds'=>$find)));
                else return $find;
                
            }    
        }
        else
            die(json_encode(array('result'=>false)));
    }
    
    
   /**
 	* Seznam priloh
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	function attachs($audit_estates_loss_event_id){
		$this->autoLayout = false;
		$this->loadModel('AuditEstateLossEventAttachment'); 

		if($this->logged_user["CmsGroup"]["permission"]["audit_estates_loss_events"]["attach"]==2) 
			$pristup =  "AuditEstateLossEventAttachment.cms_user_id=".$this->logged_user['CmsUser']['id'];
		else 
			$pristup="";

		$this->AuditEstateLossEventAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->AuditEstateLossEventAttachment->findAll(array(
            'AuditEstateLossEventAttachment.audit_estate_loss_event_id'=>$audit_estates_loss_event_id,
            'AuditEstateLossEventAttachment.kos'=>0,
            $pristup
        )));
		$this->set('audit_estates_loss_event_id',$audit_estates_loss_event_id);
		unset($this->AuditEstateLossEventAttachment);
		$this->render('attachs/index');
		
	}
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($audit_estates_loss_event_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('AuditEstateLossEventAttachment'); 
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType'); 
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
			$this->data['AuditEstateLossEventAttachment']['audit_estate_loss_event_id'] = $audit_estates_loss_event_id;
			if ($id != null){
				$this->data = $this->AuditEstateLossEventAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->data["AuditEstateLossEventAttachment"]["cms_user_id"]=$this->logged_user['CmsUser']['id'];
			$this->AuditEstateLossEventAttachment->save($this->data);
			$this->attachs($this->data['AuditEstateLossEventAttachment']['audit_estate_loss_event_id']);
		}
		unset($this->AuditEstateLossEventAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($audit_estates_loss_event_id, $id){
		$this->loadModel('AuditEstateLossEventAttachment');
		$this->AuditEstateLossEventAttachment->save(array('AuditEstateLossEventAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($audit_estates_loss_event_id);
		unset($this->AuditEstateLossEventAttachment);
	}
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('../atep/uploaded/'.$file);
		$cesta = "http://".$_SERVER['HTTP_HOST']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}
     
      /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
        //Configure::write('debug',2);
        
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
        
        $fields_sql = array(fields);      
        $criteria = $this->ViewIndex->filtration();        
      
   
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
       /**
         * Celkovy pocet zaznamu
         */
        $limit = 100;   
        $page = 1;        
        $count = $this->AuditEstateLossEvent->find('first',
        	array(
        		'fields' => array("COUNT(DISTINCT AuditEstateLossEvent.id) as count"),
        		'conditions'=>$criteria,
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];
        
        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',','?'=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        for ($exported = 0; $exported < $count; $exported += $limit){
            $this->AuditEstateLossEvent->bindModel($this->renderSetting['bindModel'],false);  
            foreach($this->AuditEstateLossEvent->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>$criteria,
                    'group'=>'AuditEstateLossEvent.id',
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1,
                    'order'=>'AuditEstateLossEvent.id DESC'
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }
    
    
    /**
      * Zobrazeni dostupnych formularu pro skodove udalosti
      */
     function form_template($id = null){
            $this->set('id',$id);
            //$this->set('form_list',$this->get_list('FormTemplate',array('type'=>1)));
            $this->loadModel('FormTemplate');
            $this->FormTemplate->bindModel(array(
                //'belongsTo'=>array('FormTemplate')
            )); 
            //pr($connection);
            $forms = $this->FormTemplate->find('all',array('conditions'=>array(
               'FormTemplate.type'=>2,
               'FormTemplate.kos'=>0
            )));
            $this->set('form_list',$forms);
    
            $this->render('form_template/index');       
     }
     
     /**
      * Funkce pro generovani formulare
      * Tisk
      */
     function print_form($form_id,$loss_event_id){
    	$this->layout = 'print';
        
        $this->loadModel('AuditEstateLossEvent');
        $this->AuditEstateLossEvent->bindModel($this->renderSetting['bindModel']);
        $find = $this->AuditEstateLossEvent->find('first',array(
            'conditions'=>array(
                'AuditEstateLossEvent.id'=>$loss_event_id,
            ),
            'fields'=>$this->renderSetting['SQLfields']
        ));
        if(!$find){
            die('Chyba škodová událost nenelezena?!');
        }

        $data = $find;
        
        //stav majetku
        $this->loadModel('AuditEstateStav');
        $stav = $this->AuditEstateStav->find('first',array(
            'conditions'=>array('audit_estate_id'=>$find['AuditEstate']['id'],'kos'=>0),
            'order'=>'date_stav DESC',
            'fields'=>array('name')
        ));
        if(!$stav)
            $data = am($data,array('AuditEstateStav'=>array('name'=>null)));
        else
            $data = am($data,$stav);
        
        //pr($data);
                
        $this->loadModel('FormTemplate'); 
        $datas = $this->FormTemplate->read(null,$form_id);

		preg_match_all('@##([a-zA-z0-9\.\_\|]*)##@',$datas['FormTemplate']['text'],$text2);
		foreach($text2[1] as $key=>$item){
			$text2[1][$key] = $this->transfer_to_html($item,$data);
		}

		$data_ = str_replace($text2[0], $text2[1], $datas['FormTemplate']['text']);
		$this->set('data',$data_);

		//$this->render('generate');
	}

	private function transfer_to_html($value,$data){
	    if(strpos($value,'|') != false)
		  list($value,$arg) = explode('|',$value);
    
   
		if(strpos($value,'.') != false){
			list($model,$col) = explode('.',$value);
            //pr($data);
            if(isset($data[$model][$col]))
			   $value = $data[$model][$col];
            else
               $value = "";  	
		}
        
        if(isset($arg))
            switch($arg){
                case 'date': $value = date('d.m.Y',strtotime($value));  break;
                case 'today_date': $value = date('d.m.Y');  break;
                case 'mena':
                    $arr = array(0=>',- EUR',1=>',- CZK');
                    $value = (isset($arr[$value])?$arr[$value]:'');
                break;
                case 'typ_smlouvy': 
                    $arr = array(1=>'Smlouva na dobu neurčitou',2=>'Smlouva na dobu určitou');
                    $value = (isset($arr[$value])?$arr[$value]:$arr[1]);
                    
                    if($value == 2 && isset($data['ConnectionClientAtCompanyWorkPosition']['expirace_smlouvy'])){
                        $value .= ' ('.date('d.m.Y',strtotime($data['ConnectionClientAtCompanyWorkPosition']['expirace_smlouvy'])).')';
                    }
                break;
              }
              
              

	    return $value;	
	}
    
    function transfer_old(){
        $this->loadModel('AuditEstateLossEvent');
        $this->loadModel('AuditEstateRepair');
        
        $this->AuditEstateLossEvent->bindModel(array(
            'belongsTo'=>array('AuditEstate')
        ));
        $losts = $this->AuditEstateLossEvent->find('all',array(
            'conditions'=>array(
                'AuditEstateLossEvent.kos'=>0,
                'AuditEstateLossEvent.audit_estate_id != ""',
                'AuditEstateLossEvent.send_on_stav'=>1,
                'AuditEstateLossEvent.currency != AuditEstate.currency',
                'AuditEstateLossEvent.fa_price != ""'
            )
        ));
 
        foreach($losts as $lost){
            
            $repair = $this->AuditEstateRepair->find('first',array(
                'conditions'=>array(
                    'AuditEstateRepair.kos'=>0,
                    'AuditEstateRepair.audit_estate_id'=>$lost['AuditEstateLossEvent']['audit_estate_id'],
                    'AuditEstateRepair.price'=>$lost['AuditEstateLossEvent']['fa_price']
                ),
                //'fields'=>$this->renderSetting['SQLfields']
            ));
            if($repair){
                $this->AuditEstateRepair->id = $repair['AuditEstateRepair']['id'];
                //$this->AuditEstateRepair->saveField('price',self::convert_price($repair['AuditEstateRepair']['price'],$lost['AuditEstateLossEvent']['currency']));
            }
            
            echo $lost['AuditEstateLossEvent']['id'].' - ';
            echo $lost['AuditEstate']['imei_sn_vin'].' - ';
            echo $lost['AuditEstateLossEvent']['fa_price'].' - ';
            echo $lost['AuditEstateLossEvent']['currency'].' - ';
            echo $lost['AuditEstate']['currency'].'<br />';
        }
        die();
    }

}
?>