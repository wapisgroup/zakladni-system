<?php
Configure::write('debug',1);
class ReportClientOppsController extends AppController {
    var $name = 'ReportClientOpps';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('ConnectionClientOpp');
	var $renderSetting = array(
		'SQLfields' => '*,
           IF(vraceno = 0 AND srazka_ze_mzdy = 0,0,1) as stav,
           Client.name,Company.name,CmsUser.name,ConnectionClientOpp.*
        ',
		'bindModel' => array(
            'belongsTo' => array(
                'CmsUser'=>array('className'=>'CmsUser','foreignKey'=>'cms_user_id'),
                'Company',
                'Client'
            )
        ),
		'controller'=> 'report_client_opps',
		'page_caption'=>'Report rozdaných OPP - klienti',
		'sortBy'=>'ConnectionClientOpp.created.DESC',
		'top_action' => array(),
        'SQLcondition'=>array(),	
        'filtration' => array(
            'ConnectionClientOpp-cms_user_id' => 'select|Koordinátor|coo_list',
            'ConnectionClientOpp-typ' => 'select|Položka|type_list',
            'ConnectionClientOpp-kos' =>	'select|Stav|client_opp_stav_list',
            
            //'CompanyOrderItem-nabor' => 'select|Je nábor|select_ano',
            //'CompanyOrderItem-order_status' => 'select|Nábor status|select_order_status',
            //'Company-id' => 'select|Společnost|company_list',
        ),
		'items' => array(
			'id'				=>	'ID|ConnectionClientOpp|id|text|',
            'coo'		        =>	'Koordinator|CmsUser|name|text|',
            'client'		    =>	'Klient|Client|name|text|',
	        'typ'				=>	'Položka|ConnectionClientOpp|typ|text|',
            'name'				=>	'Název OPP|ConnectionClientOpp|name|text|',
            'size'				=>	'Velikost|ConnectionClientOpp|size|text|',	
            'price'				=>	'Cena|ConnectionClientOpp|price|text|',
			'datum'		        =>	'Datum přiřazení|ConnectionClientOpp|created|date|',
            'stav'		        =>	'Stav|ConnectionClientOpp|kos|var|client_opp_stav_list',
		),
		'posibility' => array(),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'		=> true,
			'languages'	=> 'false'
		)
	);
    
    function index(){
        $this->loadModel('OppItem');
		$this->set('type_list',$this->OppItem->find('list', array('group'=>'type','fields'=>array('type','type'))));        
        unset($this->OppItem);
        
        $this->set('client_opp_stav_list',array(0=>'Přiřazeno',1=>'Vraceno na sklad COO'));
        
        $this->loadModel('CmsUser');
		$this->set('coo_list',$cm_koo_list = $this->CmsUser->find('list',array(
			'conditions'=>array(
				'kos'=>0,
				'status'=>1,
				'cms_group_id IN(3,4)'
			)
		)));
        unset($this->CmsUser);
        
        if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {			
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
           
		}
	}
   
	
}
?>