<?php
//Configure::write('debug',1);
class AccommodationsController extends AppController {
	var $name = 'Accommodations';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload');
	var $uses = array('Accommodation');
	var $renderSetting = array(
		'SQLfields' => array('Accommodation.id','Accommodation.name','Accommodation.city','Accommodation.updated','Accommodation.created','Accommodation.account','Accommodation.day_of_payment'),
		'controller'=> 'accommodations',
		'page_caption'=>'Ubytování',
		'sortBy'=>'Accommodation.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
			//'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
			//'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			'Accommodation-name|'				=>	'text|Jméno|',
			'Accommodation-city|'				=>	'text|Město|'
		),
		'items' => array(
			'id'			=>	'ID|Accommodation|id|text|',
			'name'			=>	'Název|Accommodation|name|text|',
			'city'			=>	'Město|Accommodation|city|text|',
			//'contact_name'	=>	'Kontakt|Accommodation|contact_name|text|',
			//'contact_phone'	=>	'Telefon|Accommodation|contact_phone|text|',
			'account'		=>	'Účet|Accommodation|account|text|',
			'splatnost'		=>	'Splatnost|Accommodation|day_of_payment|text|',
			//'updated'		=>	'Změněno|Accommodation|updated|datetime|',
			'created'		=>	'Vytvořeno|Accommodation|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'kontakty'	=>	'kontakty|Kontakty|edit',
			'attach'	=>	'attachs|Přílohy|edit',
			'fakturace'	=>	'fakturacka|Vystavené faktury a úhrady|edit',
			'delete'	=>	'trash|Odstranit položku|trash'			
		),
        /**
         * nove zadavani jen sve, tzv. pristupy
         * nutne je zadat tabulku kde jsou pristupy ulozeny
         * a id pro jake se ma filtrovat prirazene zaznamy
         * group -> zda plati jen sve pro cms_group_id nebo pro cms_user_id
         */
        'only_his'=>array(
            'table'=>'wapis__connection_accommodation_users',
            'col'=>'accommodation_id',
            'group'=>false
        )
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti'=>'#'));
		
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			
			$this->loadModel('SettingAccommodationType');
			
			// load accommodation_type_list_with_price_type
			$this->set('accommodationtype_list', $this->SettingAccommodationType->find('list', array('order'=>'name','conditions'=>array('SettingAccommodationType.kos'=>0))));
			$this->set('accommodationtype_title_list', $this->SettingAccommodationType->find('list', array('order'=>'name','conditions'=>array('SettingAccommodationType.kos'=>0),'fields'=>array('id','price_type'))));
			
			if ($id != null){
				// nacteni useru
				$this->loadModel('ConnectionAccommodationUser');
				$this->ConnectionAccommodationUser->bindModel(array('belongsTo'=>array('CmsUser')));
				$this->set('users_list', $this->ConnectionAccommodationUser->find('all', array('conditions'=>array('ConnectionAccommodationUser.accommodation_id'=>$id))));
			
			
				$this->data = $this->Accommodation->read(null,$id);
			}
			$this->render('edit');
		} else {
			if (!isset($this->data['Accommodation']['imgs'])) $this->data['Accommodation']['imgs'] = array();
			
			$this->Accommodation->save($this->data);
            
            /**
             * vytvoření přístupu pro toho kdo to vytvořil
             */
            if($this->data['Accommodation']['id'] == ''){
			    $this->loadModel('ConnectionAccommodationUser');
                $save_acc = array(
                    'accommodation_id'=>$this->Accommodation->id,
                    'cms_user_id'=>$this->logged_user['CmsUser']['id']
                );
                $this->ConnectionAccommodationUser->id = null;
				$this->ConnectionAccommodationUser->save($save_acc);	
			}
            
			die();
		}
	}
	
	/**
 	* Seznam priloh
 	*
	* @param $Accommodation_id
 	* @return view
 	* @access public
	**/
	function attachs($accommodation_id){
		$this->autoLayout = false;
		$this->loadModel('AccommodationAttachment'); 
		$this->AccommodationAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->AccommodationAttachment->findAll(array('AccommodationAttachment.accommodation_id'=>$accommodation_id,'AccommodationAttachment.kos'=>0)));
		$this->set('accommodation_id',$accommodation_id);
		unset($this->AccommodationAttachment);
		$this->render('attachs/index');
	}
	
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($accommodation_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('AccommodationAttachment'); 
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType'); $this->SettingAttachmentType =& new SettingAttachmentType();
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
			$this->data['AccommodationAttachment']['accommodation_id'] = $accommodation_id;
			if ($id != null){
				$this->data = $this->AccommodationAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->AccommodationAttachment->save($this->data);
			$this->attachs($this->data['AccommodationAttachment']['accommodation_id']);
		}
		unset($this->AccommodationAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($accommodation_id, $id){
		$this->loadModel('AccommodationAttachment');
		$this->AccommodationAttachment->save(array('AccommodationAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($accommodation_id);
		unset($this->AccommodationAttachment);
	}
	
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('./uploaded/'.$file);
		$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}
	
	/**
 	* Zobrazeni kontaktu k dane firme
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	function kontakty($accommodation_id){
		$this->autoLayout = false;
		$this->loadModel('AccommodationContact'); $this->AccommodationContact =& new AccommodationContact();
		$this->set('contact_list', $this->AccommodationContact->findAll(array('AccommodationContact.accommodation_id'=>$accommodation_id,'AccommodationContact.kos'=>0)));
		$this->set('Accommodation_id',$accommodation_id);
		unset($this->AccommodationContact);
		$this->render('kontakty/index');
		
	}
	/**
 	* Editace kontaktu
 	*
	* @param $accommodation_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function kontakty_edit($accommodation_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('AccommodationContact'); $this->AccommodationContact =& new AccommodationContact();
		if (empty($this->data)){
			$this->data['AccommodationContact']['accommodation_id'] = $accommodation_id;
			if ($id != null){
				$this->data = $this->AccommodationContact->read(null,$id);
			}
			$this->render('kontakty/edit');
		} else {
			$this->AccommodationContact->save($this->data);
			$this->kontakty($this->data['AccommodationContact']['accommodation_id']);
		}
		unset($this->AccommodationContact);
	}
	/**
 	*Presun kontaktu do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function kontakty_trash($accommodation_id, $id){
		$this->loadModel('AccommodationContact'); $this->AccommodationContact =& new AccommodationContact();
		$this->AccommodationContact->save(array('AccommodationContact'=>array('kos'=>1,'id'=>$id)));
		$this->kontakty($accommodation_id);
		unset($this->AccommodationContact);
	}
	
	/**
	* Slouzi pro zobrazeni nadefinovanych fakturaci k dane firme
	*
	* @param $company_id
 	* @return view  list of company position
 	* @access public
 	*/
	
	function fakturacka($accommodation_id){
		// nacteni potrebnych DB modelu
		$this->loadModel('AccommodationInvoiceItem'); $this->AccommodationInvoiceItem =& new AccommodationInvoiceItem();
		// nacteni pracovnich pozic k dane spolecnosti
		$this->set('invoice_item_list', $pp =  $this->AccommodationInvoiceItem->findAll(array('AccommodationInvoiceItem.accommodation_id'=>$accommodation_id,'AccommodationInvoiceItem.kos'=>0)));
		
		// predani ID spolecnosti do View
		$this->set('accommodation_id',$accommodation_id);
		
		// uvolneni modelu z pameti
		unset($this->AccommodationInvoiceItem);
		
		// vyrendrovani seznamu pozic
		$this->render('invoice_item/index');
	}
	
	function invoice_item_edit($accommodation_id = null, $id = null) {
		$this->loadModel('AccommodationInvoiceItem'); 
		if (empty($this->data)){
			if ($id != null) {
				$this->data =  $this->AccommodationInvoiceItem->read(null, $id);
			} else
				$this->data['AccommodationInvoiceItem']['accommodation_id'] = $accommodation_id;
			$this->render('invoice_item/edit');
		} else {
			$this->AccommodationInvoiceItem->save($this->data);
			$this->fakturacka($this->data['AccommodationInvoiceItem']['accommodation_id']);
		}
	}
	
	/**
 	*Presun kontaktu do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function invoice_item_trash($accommodation_id, $id){
		$this->loadModel('AccommodationInvoiceItem'); 
		$this->AccommodationInvoiceItem->save(array('AccommodationInvoiceItem'=>array('kos'=>1,'id'=>$id)));
		$this->fakturacka($accommodation_id);
		unset($this->AccommodationInvoiceItem);
	}
	
	
	
	
	/**
 	*upload fotografie do ubytovani
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function upload_foto(){
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	}
	
	
	function access_add($accommodation_id = null){

		$this->autoLayout = false;
		//$client = $this->Client->find(array('Client.mobil'=>$client_mobil,'Client.kos'=>0));
		if ($accommodation_id!=null){
			$this->loadModel('CmsUser');
			if(empty($this->data))
			{
				$this->set('accommodation_id',$accommodation_id);
				//nacteni uzivatelu
				
				$cms_user_list2 = array('-2'=>'--- Pro všechny ---');
				
				$this->loadModel('ConnectionAccommodationUser');	
				$neco = $this->ConnectionAccommodationUser->find('list',array('conditions'=>array('ConnectionAccommodationUser.accommodation_id'=>$accommodation_id),'fields'=>array('id','cms_user_id')));
				if($neco)
					$cms_user_list = $this->CmsUser->find('list', array('conditions'=>array('CmsUser.kos'=>0,'CmsUser.id NOT IN ('.implode(',',$neco).')'),'order'=>'name ASC'));
				else
					$cms_user_list = $this->CmsUser->find('list', array('conditions'=>array('CmsUser.kos'=>0),'order'=>'name ASC'));
				
				if(!in_array(-2,$neco)){ //pokud uz nebyla pridana moznost Vsichni
					foreach($cms_user_list as $id=>$user)
						$cms_user_list2[$id] = $user;
				}
				else // jinak ji nezobrazuj
					$cms_user_list2 = $cms_user_list;
					
				$this->set('cms_user_list',$cms_user_list2);
			}
			else{
				$this->data["ConnectionAccommodationUser"]["accommodation_id"]=$accommodation_id;
				$this->loadModel('ConnectionAccommodationUser');
				$this->ConnectionAccommodationUser->save($this->data);	

				die(json_encode(($this->data["ConnectionAccommodationUser"]["cms_user_id"] != -2 ? $this->CmsUser->read(array('name'), $this->data["ConnectionAccommodationUser"]["cms_user_id"]) : 'Všichni')));
			}
		}

		$this->render('access/add');
	}
	
	function access_trash($access_id = null){
		if($access_id != null){
			$this->loadModel('ConnectionAccommodationUser');
			$this->ConnectionAccommodationUser->delete($access_id);
		}
		die();
	}
}
?>