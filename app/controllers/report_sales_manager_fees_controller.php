<?php
	//Configure::write('debug',1);
	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
	define('CURRENT_YEAR', $_GET['current_year']);
	define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);
	
class ReportSalesManagerFeesController extends AppController {
	var $name = 'ReportSalesManagerFees';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CompanyInvoiceItem');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('Company')),
		'permModel'=>'Company',
		'TypUserCompany'=>true,
		'permGroupId'=>true,
		'controller'=>'report_sales_manager_fees',
		'SQLfields' => array('CompanyInvoiceItem.id','Company.self_manager_id','Company.name', 'CompanyInvoiceItem.za_mesic', 'SUM(CompanyInvoiceItem.spolu_s_dph) as zaklad'),
		'SQLcondition'=>'DATE_FORMAT(CompanyInvoiceItem.za_mesic,"%Y-%m") = "#CURRENT_YEAR#-#CURRENT_MONTH#"',
		'page_caption'=>'Reporty - Odměny pro SalesManager',
		'sortBy'=>'CompanyInvoiceItem.za_mesic.DESC',
		'group_by' => 'Company.id, CompanyInvoiceItem.za_mesic',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Přidat novou fakturu|add',
		),
		'filtration' => array(
			'GET-current_year'					=>	'select|Rok|actual_years_list',
			'GET-current_month'					=>	'select|Měsíc|mesice_list',
			'Company-self_manager_id'			=>	'select|SalesManager|sm_list',
			'CompanyInvoiceItem-company_id'		=>	'select|Společnost|company_list',
		),
		'items' => array(
			'id'				=>	'ID|CompanyInvoiceItem|id|hidden|',
			'sm'				=>	'SalesManager|Company|self_manager_id|viewVars|sm_list',
			'company'			=>	'Firma|Company|name|text|',
			'za_mesic'			=>	'Za měsíc|CompanyInvoiceItem|za_mesic|date|Y-m',
			'zaklad'			=>	'Základ odměny|0|zaklad|money|',
			'procento'			=>	'Procento z FA|0|procento|procenta|',
			'odmena'			=>	'Odměna|0|odmena|money|',
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
		//	'edit'		=>	'edit|Editovat položku|edit',			
		)
	);
	
	function index(){
		//pr($this->viewVars['items']);
	
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Odměny pro SalesManager'=>'#'));
		
		$this->loadModel('CmsUser'); 	
		$this->loadModel('Company'); 	
		$this->set('sm_list',			$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>2),'order'=>'CmsUser.name ASC')));
		$this->set('company_list',		$this->Company->find('list',array('conditions'=>array('Company.kos'=>0),'order'=>array('Company.name ASC'))));
		unset($this->CmsUser);
		unset($this->Company);
		

		//nacteni nastaveni procenta a predani hodnot pro pocitani
		$this->loadModel('Setting'); 
		$ceny = array();
		$ceny = $this->Setting->read(null,5);

		$procento_pro_sm 	= $ceny['Setting']['value']['procento_z_fa'];

		foreach($this->viewVars['items'] as &$item){
			$item[0]['odmena'] = $item[0]['zaklad'] / 100 * $procento_pro_sm; 
			$item[0]['procento'] =  $procento_pro_sm; 
		}
		
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	
	
}
?>