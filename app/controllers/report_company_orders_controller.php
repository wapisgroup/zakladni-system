<?php
//Configure::write('debug',1);
class ReportCompanyOrdersController extends AppController {
    

    var $name = 'ReportCompanyOrders';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CompanyOrderItem');
	var $renderSetting = array(
		//'SQLfields' => array('id','Company.name','updated','created','status','SalesManager.name','ClientManager.name','Coordinator.name','mesto','SettingStav.namre'),
		'SQLfields' => '*,
            CONCAT_WS(" + ",CompanyOrderItem.count_of_free_position,CompanyOrderItem.count_of_substitute) as objednavka,
            CONCAT_WS(" / ",CompanyOrderItem.count_of_free_position,
                IF(
                 CompanyOrderItem.order_status = 1,
                    (SELECT 
                        COUNT(c.id) 
                     FROM wapis__connection_client_requirements as c
                     Where 
                        c.company_id = CompanyOrderItem.company_id AND 
                        c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                        type = 2
                     ),
                 CompanyOrderItem.statistika
                )
            ) as statistika,
            IF((CompanyOrderItem.start_datetime < NOW() && CompanyOrderItem.order_status IN (-1,1)), 1 , 0) as expire        
        ',
		'bindModel' => array(
            'belongsTo' => array(
                'Company'=>array('foreignKey'=>'company_id')
            )
        ),
        
		'controller'=> 'report_company_orders',
		'page_caption'=>'Objednávky v podnicích',
		'sortBy'=>'CompanyOrderItem.created.DESC',
		'top_action' => array(),
		
         /*
        'SQLcondition'=>array(
            'order_status'=>$order_status
        ),	
        */
        'filtration' => array(
            'CompanyOrderItem-name' => 'text|Objednávka|',
            'CompanyOrderItem-nabor' => 'select|Je nábor|select_ano',
            'CompanyOrderItem-order_status' => 'select|Nábor status|select_order_status',
            'Company-id' => 'select|Společnost|company_list',
        ),
		'items' => array(
			'id'				=>	'ID|CompanyOrderItem|id|text|',
	        'firma'				=>	'Firma|Company|name|text|',
			'order'				=>	'Profese|CompanyOrderItem|name|text|',	
			'order_date'		=>	'Datum objednávky|CompanyOrderItem|order_date|date|',
			'start_datetime'	=>	'Datum nástupu|CompanyOrderItem|start_datetime|date|',
			'objednavka'		=>	'Objednávka|0|objednavka|text|',
			'statistika'		=>	'Statistika|0|statistika|text|',
			'start_comment'		=>	'Komentář|CompanyOrderItem|start_comment|text|orez#30',
           	'status'			=>	'Status|CompanyOrderItem|order_status|var|order_stav_list',
            'created'			=>	'Vytvořeno|CompanyOrderItem|created|datetime|',
		),
        'class_tr'=>array(
            'class'=>'color_red',
            'model'=>0,
            'col'=>'expire',
            'value'=>1
        ),
		'posibility' => array(
			'show'		=>	'show|Detail|edit',
			'edit'		=>	'edit|Editovat WWW formulář|edit',
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'		=> true,
			'languages'	=> 'false'
		)
	);
    
    function index(){
		 include('select_config.php');
            $this->loadModel('Company');
            $company_conditions =  array('Company.kos'=>0);
			if (isset($this->filtration_company_condition))
				$company_conditions = am($company_conditions, $this->filtration_company_condition);
			$this->set('company_list',			$this->Company->find('list',array('conditions'=>$company_conditions,'order'=>'name ASC')));
            unset($this->Company);
        
        if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			// set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
			
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
           
		}
	}
    
    
    function show($order_id){
        $detail = $this->CompanyOrderItem->read('company_id',$order_id);
        echo $this->requestAction('companies/order_edit/'.$detail['CompanyOrderItem']['company_id'].'/'.$order_id.'/show');
		die();
    }
    
     /**
 	* Editace WWW objednavka
 	*
	* @param $order_id
 	* @return view
 	* @access public
	**/
    
    function edit($order_id=null){
       	$this->loadModel('CompanyOrderItem');
		if (empty($this->data)){
            if ($order_id != null)
				$this->data = $this->CompanyOrderItem->read(null, $order_id); 
            
            /**
             * Vyhledavame ve WWW inzerci zda uz je inzerat vytvoren pro tuto objednavku
             */
            $this->loadModel('WwwInsertion');
            $exist = $this->WwwInsertion->find('first',array('conditions'=>array(
                'company_order_item_id'=>$order_id,
                'kos'=>0
            )));
            if($exist){
                $ex_id = $exist['WwwInsertion']['id'];
            }
            else
                $ex_id = -1;
            
            $this->data['www_insertion_id'] = $ex_id;
           // $this->render('orders/www_objednavka');
        
        } else {
            if($this->data['CompanyOrderItem']['www_cms_user_id'] == '')
               $this->data['CompanyOrderItem']['www_cms_user_id'] = $this->logged_user['CmsUser']['id']; 
        
            $this->loadModel('WwwInsertion');
            if($this->data['www_insertion_id'] != -1){
                $this->WwwInsertion->id = $this->data['www_insertion_id'];
            }
            else{
                $this->WwwInsertion->id = null;
            }
            
            $stav = 0;
            if(isset($this->data['CompanyOrderItem']['www_show']) && ($this->data['CompanyOrderItem']['www_show'] == 1 || $this->data['CompanyOrderItem']['www_show'] == 'on'))
                $stav = 1;
                
            $save_arr = array(
                'company_order_item_id'=>$this->data['CompanyOrderItem']['id'],
                'name'=>$this->data['CompanyOrderItem']['www_name'],
                'city'=>$this->data['CompanyOrderItem']['start_place'],
                'napln'=>$this->data['CompanyOrderItem']['www_napln'],
                'pozadavky'=>$this->data['CompanyOrderItem']['www_pozadujeme'],
                'plat'=>$this->data['CompanyOrderItem']['www_mzda'],
                'dalsi_informace'=>$this->data['CompanyOrderItem']['www_dalsi_informace'],
                'setting_career_item_id'=>$this->data['CompanyOrderItem']['setting_career_item_id'],
                'cms_user_id'=>$this->data['CompanyOrderItem']['www_cms_user_id'],
                'status'=>$stav
            );
            $this->WwwInsertion->save($save_arr);
        
            if ($this->CompanyOrderItem->save($this->data)){
            	die(json_encode(array('result'=>true)));
			} else {
				die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani')));			
			}
        }
        
    }
    
	
}
?>