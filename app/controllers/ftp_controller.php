<?php
Configure::write('debug',1);
class FtpController extends AppController {
	var $name = 'Ftp';
	var $uses = array('CampaignEmail');
	var $helpers = array('htmlExt','Fastest','FileInput');
	var $layout = 'default';
	var $components = array('RequestHandler','Upload','Email');
    var $limit = 1024;//v MB
    var $renderSetting = array('controller'=>'ftp');//to je kvuli aut. natahnuti permission

	function upload_foto(){
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	}
    
    function user_share_size(){
        $output = shell_exec('du -b -s uploaded/usershare/');
        $output = explode(" ",trim($output));
        return $output[0];
    }
    
    function usershare(){
        $this->set('limit',$this->limit);//MB    
        $this->set('usage_size_mb',(self::user_share_size()/1024/1024));
        $this->set('usage_size',self::user_share_size());
        $this->set('scripts', array('uploader/uploader'));
		$this->set('styles', array('../js/uploader/uploader'));
        		
		//temp define
		$information['ftp_domena'] = $this->logged_user['domena_upload'].'usershare';

		$this->set('page_caption','Správa souborů');
		$this->set('path_to_domena',$information['ftp_domena']);

		$this->ftp_connect();

		$numargs = func_get_args();
		$directory = implode('/',$numargs);
		// pokud je jen jedno FTP
		$this->logged_user['ftp_jedno'] = 'web/atep/uploaded/usershare';
		if (!empty($this->logged_user['ftp_jedno']) && empty($numargs)){
			//ftp_chdir($this->ftp_connect, $this->logged_user['ftp_jedno']);
			$directory = $this->logged_user['ftp_jedno'].$directory;
		}
		
		//pr($directory);
		//pr($directory);
		$this->rawlist_dump($directory);
		$this->set('current_dir',$directory.'/');
        $this->set('ftp_jedno',$this->logged_user['ftp_jedno']);
		
		if (isset($_GET['input'])){
			$this->set('JSinput',$_GET['input']);
		}
		
        
        if (($this->RequestHandler->isAjax()) AND (!isset($_GET['win']))){
			$this->layout = false;
			$this->render('usershare_items');
		} else {
			$this->render('usershare_index');
		}
    }
	
	function index() {
		//$information = $this->Session->read('information');
		$this->set('scripts', array('uploader/uploader'));
		$this->set('styles', array('../js/uploader/uploader'));
		
		//temp define
		$information['ftp_domena'] = $this->logged_user['domena_upload'];

		$this->set('page_caption','Správa souborů');
		$this->set('path_to_domena',$information['ftp_domena']);
		$this->set('scripts',array('uploader/uploader'));
		$this->ftp_connect();

		$numargs = func_get_args();
		$directory = implode('/',$numargs);
		// pokud je jen jedno FTP
		 $this->logged_user['ftp_jedno'] = 'web/atep/uploaded/';
		if (!empty($this->logged_user['ftp_jedno']) && empty($numargs)){
			//ftp_chdir($this->ftp_connect, $this->logged_user['ftp_jedno']);
			$directory = $this->logged_user['ftp_jedno'].$directory;
		}
		
		//pr($directory);
		//pr($directory);
		$this->rawlist_dump($directory);
		$this->set('current_dir',$directory.'/');
        $this->set('ftp_jedno',$this->logged_user['ftp_jedno']);
		
		if (isset($_GET['input'])){
			$this->set('JSinput',$_GET['input']);
		}
		
		if (($this->RequestHandler->isAjax()) AND (!isset($_GET['win']))){
			$this->layout = false;
			$this->render('browse_items');
		} else {
			$this->render('browse_index');
		}
	}
	 
	function delete_file(){
		$this->ftp_connect();
		$numargs = func_get_args();
		$name = $numargs[count($numargs)-1];
		unset($numargs[count($numargs)-1]);
		$directory = implode('/',$numargs);
		ftp_chdir($this->ftp_connect, $directory);
		if (ftp_delete($this->ftp_connect, $name)) {
			echo "$name deleted successful\n";
		} else {
			echo "could not delete $name\n";
		}
		die();
	}
	
	function delete_directory(){
		$this->ftp_connect();
		$numargs = func_get_args();
		$name = $numargs[count($numargs)-1];
		unset($numargs[count($numargs)-1]);
		$directory = implode('/',$numargs);
        
		ftp_chdir($this->ftp_connect, $directory);
		if (count(ftp_nlist($this->ftp_connect, $name))>2){
			echo "Nelze smazat $name obdsahuje adresare nebo soubory \n";
		} else {
			if (ftp_rmdir($this->ftp_connect, $name)) {
				echo "Successfully deleted $name\n";
			} else {
				echo "There was a problem while deleting $name\n";
			}
		}
		die();
	}
	
	function create_directory(){
		$this->ftp_connect();
		$numargs = func_get_args();
		$name = $numargs[count($numargs)-1];
		unset($numargs[count($numargs)-1]);
		unset($numargs[0]);
		unset($numargs[1]);
		$directory = implode('/',$numargs);

        ftp_chdir($this->ftp_connect, $directory);	
		if (ftp_mkdir($this->ftp_connect, strtr($name,array(" "=>'-')))) {
			echo "successfully created $name\n";
		} else {
			echo "There was a problem while creating $name\n";
		}
		die();
	}
	 
	function ftp_connect() {
		//$information = $this->Session->read('information');
		
		// temp define 
		$information['ftp_host'] = $this->logged_user['ftp_relace'];
		$information['ftp_user'] = $this->logged_user['ftp_login'];
		$information['ftp_heslo'] = $this->logged_user['ftp_password'];
		
		
		//pr($information);
		$this->ftp_connect = ftp_connect($information['ftp_host']);
		$login_result = ftp_login ($this->ftp_connect, $information['ftp_user'], $information['ftp_heslo']);
		if ((!$this->ftp_connect) || (!$login_result)) die("Chyba připojení na FTP server! Špatné přihlašovací údaje");
		ftp_pasv ($this->ftp_connect, true) ;
		
	}
	
	function rawlist_dump($directory = ".") {
		$this->set('directory',$directory);
		$ftp_rawlist = ftp_rawlist($this->ftp_connect, $directory);
		$rawlist = array();
		// pokud neni nadrazeny dve tecky
		if (!in_array('..',$ftp_rawlist)){
			$ftp_rawlist[-1] =  'drwxr-xr-x    7 1001     ftpgroup     4096 Jul 20 11:44 ..'; 
			ksort($ftp_rawlist);
		}
		
		foreach ($ftp_rawlist as $v) {
			$info = array();
			$vinfo = preg_split("/[\s]+/", $v, 9);
            
			if ($vinfo[0] !== "total"){
				$info['chmod'] = $vinfo[0];
				$info['num'] = $vinfo[1];
				$info['owner'] = $vinfo[2];
				$info['group'] = $vinfo[3];
				$info['size'] = $vinfo[4];
				$info['month'] = $vinfo[5];
				$info['day'] = $vinfo[6];
				$info['time'] = $vinfo[7];
				$info['name'] = $vinfo[8];
				$rawlist[$info['name']] = $info;
			}
		}
        //pr($rawlist);
		$dir = array();
		$file = array();
		foreach ($rawlist as $k => $v) {
			if ($v['chmod']{0} == "d" && ($k == '..' || ($k != '..' && array_key_exists($k,$this->logged_user['CmsGroup']['usershare_folders']) && $this->logged_user['CmsGroup']['usershare_folders'][$k] == 1))) {
				$dir[$k] = $v;
			} elseif ($v['chmod']{0} == "-") {
				$file[$k] = $v;
			}	
		}
		//pr($dir);
		$this->set('dir',$dir);
		$this->set('file',$file);
		
	}
	
	################
	#         funkce pro upload       #
	################
	function read_dir($root){
		$root_dir = opendir($root);
		$time = null;
		while($file_file = readdir($root_dir)){
		if($file_file != '' && $file_file != '.' && $file_file != '..' && substr($file_file,0,3) == 'php')
			{
				
				$time_now = fileatime($root.'/'.$file_file);
				if ($time == null || $time_now > $time[0]['time']){
					$time[0]['time'] = $time_now;
					$time[0]['file'] = $file_file;
				}
			}
		}
		$time[0]['size'] = filesize($root.'/'.$time[0]['file']);
		return $time;
	}

	function sabsi($array, $index, $order='asc', $natsort=FALSE, $case_sensitive=FALSE)
	{
		
		if(is_array($array) && count($array)>0)
		{
			foreach(array_keys($array) as $key) @$temp[$key]=$array[$key][$index];
			if(!$natsort) ($order=='asc')? asort($temp) : arsort($temp);
			else
			{
				($case_sensitive)? natsort($temp) : natcasesort($temp);
				if($order!='asc') $temp=array_reverse($temp,TRUE);
			}
			foreach(array_keys($temp) as $key) (is_numeric($key))? $sorted[]=$array[$key] : $sorted[$key]=$array[$key];
			return $sorted;
		}
		return $array;
	}

	function get_status($file = null){
		$upload_tmp_dir = ini_get('upload_tmp_dir');

		//$upload_tmp_dir = '/var/www/wapis.cz/web/mark/tmp/';
		
		if ($file == null){
			$tmpdata = $this->read_dir($upload_tmp_dir);
			$tmpdata = $this->sabsi($tmpdata,'time');
			
			
			$stat = stat($upload_tmp_dir.'/'.@$tmpdata[0]['file']);
			$tmpdata[0]['size']= $stat['size'];
			$tmpdata[0]['block_size']= $stat['blksize'];
				
			$size = $tmpdata[0]['size'];
		} else {
			if (file_exists($upload_tmp_dir.'/'.$file)){
				$tmpdata[0]['file'] = $file;
				//$tmpdata[0]['size'] = filesize($upload_tmp_dir.'/'.$file);
				$stat = stat($upload_tmp_dir.'/'.$file);
				$tmpdata[0]['size']= $stat['size'];
				$tmpdata[0]['block_size']= $stat['blksize'];
				
				$size = $tmpdata[0]['size'];
				
				
			} else {
				$tmpdata[0]['file'] = '';//$file;
				$size = 'done';
			}
		}
		echo json_encode(array('upload_info'=>array('size'=>$size,'file'=>@$tmpdata[0]['file'],'path'=>$upload_tmp_dir)));
		die();
	}
	function load_image($file=null){
		$this->layout = false;
		$file = $this->logged_user['domena_upload'].Str_Replace('|','/',$file);
		echo '<img src="'.$file.'" id="file_preview_img">';
		$delka_domeny = strlen($this->logged_user['domena_upload']);
		$file=substr($file,$delka_domeny-1);
		echo "<input type='hidden' id='selected_file' value='".$file."' />";
		die();
		
	
	}
	function load_file($file=null){
		$this->layout = false;
		$file = $this->logged_user['domena_upload'].Str_Replace('|','/',$file);
		$file_name = strtolower(end(Explode("/", $file)));
			$pripona = strtolower(end(Explode(".", $file)));
			
			$ikony = array(
				'jpeg'=>'img',
				'png'=>'img',
				'gif'=>'img',
				'xls'=>'excel',
				'ods'=>'excel',
				'odt'=>'word',
				'doc'=>'word',
				'pdf'=>'pdf',
				'html'=>'html',
				'htm'=>'html',
				'php'=>'php',
				'css'=>'html',
				'ctp'=>'html',
				'ppt'=>'ppt',
				'ptc'=>'ppt',
				'zip'=>'zip',
				'rar'=>'zip',
				'avi'=>'video',
				'mpeg'=>'video',
				'mpg'=>'video',
				'jpg'=>'img',
				
			);	
			if (array_key_exists($pripona, $ikony)){
				$icon='<img src="../css/fastest/filebrowser/'.$ikony[$pripona].'.gif" class="relative top" />';
			} else
				$icon='<img src="../css/fastest/filebrowser/unknown.gif" />';
				
			//echo '<strong>Soubor: </strong><a href="'.$file.'" title="Zobrazit soubor" target="_blank">'.$file_name.'</a> '.$icon;
			$delka_domeny = strlen($this->logged_user['domena_upload']);
			$file=substr($file,$delka_domeny-1);
			
			//echo "<input type='hidden' id='selected_file' value='".$file."' />";
		die();
		
	
	}
	function upload_file(){
        $this->Upload->set('data_upload',$_FILES['upload_file']);
       
        $file_mb = round(($_FILES['upload_file']['size']/1024/1024),2);
        $usage_mb = round((self::user_share_size()/1024/1024),2);  
        if($usage_mb >= $this->limit){
        echo json_encode(array('result'=>false,'message'=>'Limit pro USERSHARE byl dosažen!'));
        }
        else if($usage_mb+$file_mb >= $this->limit){
            echo json_encode(array('result'=>false,'message'=>'Soubor je příliš velký, můžete nahrát soubor už jen o velikosti '.($this->limit-$usage_mb).'MB !'));
        }
        else{
            if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
                $replace_list = array(
    				'##filename##' 	=>$this->Upload->get('outputFilename'),
                    '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
    			);
                $this->Email->ftp_host = $this->logged_user['ftp_relace'];
        		$this->Email->ftp_user = $this->logged_user['ftp_login'];
        		$this->Email->ftp_heslo = $this->logged_user['ftp_password'];
                $this->Email->addAttachFromFtp($this->Upload->get('basicUploadPath').$this->Upload->get('outputFilename'),false);
                $this->Email->send_from_template_new(53,array('matus@wapis.cz'),$replace_list);
                
            	echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'result'=>true));
            } 
            else 
        	    echo json_encode(array('result'=>false,'message'=>$this->Upload->get('error_message')));		
        }
        die();
	}
    
    /**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('./uploaded/usershare/'.$file);
		$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/usershare/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'"');
		readfile($cesta);
		die();

	}	
}
?>