<?php
class BonusRecruiterZamController extends AppController {
	var $name = 'BonusRecruiterZam';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('BonusRecruiterView');
	var $renderSetting = array(
		'controller'=>'bonus_recruiter_zam',
		'SQLfields' => array(),
		'page_caption'=>'Provize recruiteru',
		'sortBy'=>'BonusRecruiterView.client_name.ASC',
		'SQLcondition'=> array(),
		'top_action' => array(		
		),
		'filtration' => array(
			'GET-recruiter'								=>	'select|Uživatel|user_list',
			'GET-current_year'							=>	'select|Rok|actual_years_list',
			'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'spec_id'		=>	'SpecID|BonusRecruiterView|spec_id|text|',
			'client_name'		=>	'Klient|BonusRecruiterView|client_name|text|',
			'company_name'		=>	'Firma|BonusRecruiterView|company_name|text|',
			
			'pozice_name'		=>	'Pozice|BonusRecruiterView|pozice_name|text|',
			'stav'		=>	'Stav|BonusRecruiterView|stav|var|kalkulace_stav_list',
			'month'		=>	'Měsíc|BonusRecruiterView|month|text|',
			'year'		=>	'Rok|BonusRecruiterView|year|text|',
			
			'celkem_hodin'		=>	'Počet odp. hod. celkem|BonusRecruiterView|celkem_hodin|text|',
			'vyplatit'		=>	'Odměna za odp. hodin|BonusRecruiterView|vyplatit|text|',
			'zbyva'		=>	'Zbyva|BonusRecruiterView|zbyva|text|',
			'odmena'	=>	'Odměna|BonusRecruiterView|odmena|text|',
			'Recruiter'		=>	'Recruiter|BonusRecruiterView|name|text|',
			'Typ'		=>	'Typ|BonusRecruiterView|typ|var|typ_odmeny_list',
			'mlm_group'		=>	'MLM|0|group_id|var|mlm_group_list',
			'procenta'	=>	'% z RDIN|BonusRecruiterView|procenta|text|',
			
		),
		'posibility' => array(
		)
	);
		function beforeFilter(){
			parent::beforeFilter();
			
			$_GET['recruiter']  = (isset($_GET['recruiter']) && !empty($_GET['recruiter']))?$_GET['recruiter']:$this->logged_user['CmsUser']['id'];
			$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
			$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:date('m');

			define('CUSTOM_ID',$_GET['recruiter']);	
			define('CURRENT_YEAR', $_GET['current_year']);
			define('CURRENT_MONTH', (strlen($_GET['current_month']) == 1)?'0'.$_GET['current_month']:$_GET['current_month']);
			define('sql_con2',
				(CUSTOM_ID == -1)?
					'year =#CURRENT_YEAR# AND MONTH =#CURRENT_MONTH# AND celkem_hodin <> 0  GROUP BY id'
				:
					'((id IN (
						SELECT cms_user_id 
						FROM wapis__connection_mlm_recruiters B
						WHERE ( cms_user_id=#CUSTOM_ID#|| parent_id =#CUSTOM_ID#)
							OR B.parent_id IN (  
									SELECT parent_id 
									FROM wapis__connection_mlm_recruiters as C
									WHERE C.parent_id IN (  
														SELECT DISTINCT X.cms_user_id
														FROM wapis__connection_mlm_recruiters as X
														WHERE X.parent_id = #CUSTOM_ID#)
									) 
					
					) 
					AND mlm_group_id IS NOT NULL) 
					OR (mlm_group_id IS NULL 
						AND id = #CUSTOM_ID# )
					) 
					AND year =#CURRENT_YEAR# AND MONTH =#CURRENT_MONTH# 
					AND celkem_hodin <> 0'
				);
			//define('sql_con', 'BonusRecruiterView.id = #CUSTOM_ID# OR BonusRecruiterView.parent_id = #CUSTOM_ID#');
			
			if (CUSTOM_ID == -1)
				define('fields','BonusRecruiterView.*, IF(mlm_group_id=0,umistovatel__group_id,mlm_group_id) as group_id, SUM(odmena) as odmena');
			else
				define('fields','BonusRecruiterView.*, (zbyva-vyplatit) as zbyva, IF(mlm_group_id=0,umistovatel__group_id,mlm_group_id) as group_id');
			
			$this->renderSetting['SQLfields'] = array(fields);	
			$this->renderSetting['SQLcondition'] = array(sql_con2);	
			
		}
	function index(){		
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Provize recruiteru'=>'#'));
		$this->set('typ_odmeny_list',array('Vlastní','Týmová','Týmová'));
		$this->loadModel('CmsUser');
		
		$podminky = '';
		if(!in_array($this->logged_user['CmsGroup']['id'],array(1,5))){
			$podminky = "id IN (
				SELECT cms_user_id 
				FROM wapis__connection_mlm_recruiters B
				WHERE ( cms_user_id=".$this->logged_user['CmsUser']['id']." || parent_id =".$this->logged_user['CmsUser']['id'].")
					OR B.parent_id IN (  
							SELECT parent_id 
							FROM wapis__connection_mlm_recruiters as C
							WHERE C.parent_id IN (  
												SELECT DISTINCT X.cms_user_id
												FROM wapis__connection_mlm_recruiters as X
												WHERE X.parent_id = ".$this->logged_user['CmsUser']['id'].")
							) 
			
			) ";
		}
		$this->set('user_list',$this->CmsUser->find('list',array(
				'order'=>'name asc',
				'conditions'=>array($podminky)
		)));
		
		if (CUSTOM_ID == -1){
			
			$this->viewVars['renderSetting']['items'] = array(
				'Recruiter'		=>	'Recruiter|BonusRecruiterView|name|text|',
				'month'		=>	'Měsíc|BonusRecruiterView|month|text|',
				'year'		=>	'Rok|BonusRecruiterView|year|text|',
				'odmena'	=>	'Odměna|BonusRecruiterView|odmena|text|'
			);
		}	
				
			
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
		
	}
	
}
?>