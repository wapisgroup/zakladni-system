<?php
Configure::write('debug',1);

$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);
class ReportCountWorkingHoursController extends AppController {
	var $name = 'ReportCountWorkingHours';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('ClientWorkingHour');
	var $renderSetting = array(
		'bindModel' => array(
			'belongsTo' => array(
				'Company',
			)
		),
		'SQLfields' => '*, count(ClientWorkingHour.id) as pocet',
		'SQLcondition' => array(
			'ClientWorkingHour.year' => CURRENT_YEAR,
			'ClientWorkingHour.month' => CURRENT_MONTH,
			'ClientWorkingHour.stav' => array(2,3,4)
		),
            
		
		'controller'=> 'report_count_working_hours',
		'page_caption'=>'Report - Počet odpracovaných hodin',
		'sortBy'=>'Company.name.ASC',
	
		'top_action' => array(
			// caption|url|description|permission
		),
		'filtration' => array(
			'ClientworkingHour-company_id'	=>	'select|Společnost|company_list',
			'GET-current_month'		=>	'select|Měsíc|mesice_list',
			'GET-current_year' 		=>	'select|Rok|actual_years_list',
		),
		'items' => array(
			'id'		=>	'ID|ClientWorkingHour|id|text|',
			'company'	=>	'Společnost|Company|name|text|',
			'rok'		=>	'Rok|ClientWorkingHour|year|text|',
			'mesic'		=>	'Měsíc|ClientWorkingHour|month|text|',
			'hodin'		=>	'Počet|0|pocet|text|'
		
		),
		'posibility' => array(
			
		),
		
		'group_by' => array(
			'ClientWorkingHour.company_id',
			//'ClientWorkingHour.month'
			//'ClientWorkingHour.year'
		),
        
		//'posibility_link' => array(
		//    'rozvazat_prac_pomer'	=>	'ClientView.id/ConnectionClientRequirement.id',
		//),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
		)
	);


    
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Klienti - interní nábor'=>'#'));	
				
		$this->set('company_list',$this->get_list('Company'));
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
}
?>