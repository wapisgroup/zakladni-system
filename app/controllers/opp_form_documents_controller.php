<?php
Configure::write('debug', 1);
class OppFormDocumentsController extends AppController {
	var $name = 'OppFormDocuments';
    var $helpers = array('htmlExt','Pagination','ViewIndex');
    var $components = array('ViewIndex','RequestHandler');
    var $uses = array('OppFormDocument');
	var $renderSetting = array(
		'controller'=>'opp_form_documents',
		'SQLfields' => array('id','name','parent_id','(SELECT name FROM wapis__opp_form_documents  WHERE id = `OppFormDocument`.`parent_id`) as parent'),
		'page_caption'=>'Klient - formuláře dokumenty',
		'sortBy'=>'OppFormDocument.id.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat|add',
			'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|OppFormDocument|id|text|',
            'name'		=>	'Název|OppFormDocument|name|text|',
			'parent'	=>	'Náleží k|0|parent|text|'
		),
		'posibility' => array(

			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'
		)
	);
	function index(){
        $this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
        if ($this->RequestHandler->isAjax()){
            $this->render('../system/items');
        } else {
            $this->render('../system/index');
        }
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
            if ($id != null){
                $this->data = $this->OppFormDocument->read(null,$id);
                $this->set('form_documents_list',  $this->OppFormDocument->find('list', array('conditions'=>array('OppFormDocument.kos'=>0, 'OppFormDocument.id !='=>$id))));
            }else{
                $this->set('form_documents_list',  $this->OppFormDocument->find('list', array('conditions'=>array('OppFormDocument.kos'=>0))));
            }
			$this->render('edit');
		} else {
            $this->OppFormDocument->save($this->data);
			die();
		}
	}

    function test(){
        $this->autoLayout = false;
        $this->loadModel('OppFormDocument');
        $firstLevel = $this->OppFormDocument->find('list', array('fields'=>array('id','name'), 'conditions'=>array('ISNULL(OppFormDocument.parent_id)', 'OppFormDocument.kos'=>0,'OppFormDocument.status'=>1)));
        if(Count($firstLevel) > 0){
            foreach($firstLevel as $id=>$name){
                $second = $this->OppFormDocument->find('list', array('fields'=>array('id','name'), 'conditions'=>array('OppFormDocument.parent_id' =>$id, 'OppFormDocument.kos'=>0,'OppFormDocument.status'=>1)));
                if(Count($second) > 0){
                    foreach($second as $id2=>$name2){
                        $third = $this->OppFormDocument->find('list', array('fields'=>array('id','name'), 'conditions'=>array('OppFormDocument.parent_id' =>$id2, 'OppFormDocument.kos'=>0,'OppFormDocument.status'=>1)));
                        if(Count($third) > 0){
                            $thirdLevel[$id2] = $third;
                        }
                    }
                    $secondLevel[$id] = $second;
                }
            }
        }
        $this->set('firstLevel', $firstLevel);
        $this->set('secondLevel', $secondLevel);
        $this->set('thirdLevel', $thirdLevel);
    }
    

    

}
?>