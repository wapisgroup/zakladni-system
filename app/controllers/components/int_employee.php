<?php
/**
 * @author Jakub Matuš
 * @created 13.7.2011
 * 
 * komponenta pro interni zamestnance
 */
class IntEmployeeComponent extends Object{
    
    
    function startup(&$controller) {
		$this->controller = &$controller;
	}
    
    /**
     * Funkce pro zjisteni zda dany zamestnanec jiz ma login
     */
    function client_have_cms_account($at_employee_id){
        if(!isset($this->controller->CmsUser))
            $this->controller->loadModel('CmsUser');
        
        $emp = $this->controller->CmsUser->find('first',array(
            'conditions'=>array(
                'at_employee_id'=>$at_employee_id
            ),
            'fields'=>array('id')
        ));
        
        if($emp){
            return $emp;
        }
        else
            return false;
    }
    
    /**
     * Vygenerujeme pristup do DANEPu pro interniho zamestnance, na zaklade emailu
     * A pošleme mu na mail vygenerovane heslo
     */
    function generate_int_zam_login($client,$cms_group_id){
        $heslo = substr(md5($client['Client']['id'].time()),0,6);
        
        $save_data['CmsUser'] = $client['Client'];
        $save_data['CmsUser']['cms_group_id'] = $cms_group_id;
        $save_data['CmsUser']['at_employee_id'] = $client['Client']['id'];
        $save_data['CmsUser']['heslo'] = $heslo;
        
        unset($save_data['CmsUser']['id']);
        if(!isset($this->controller->CmsUser))
            $this->controller->loadModel('CmsUser');
        $this->controller->CmsUser->id = null;
        $this->controller->CmsUser->save($save_data);
        
        return $heslo;          
    }
    
    /**
     * Funkce vyhleda vsechny zamestannce zamestnane na tuto profesi a 
     * zmeni jejich uzivateslka konta, na danou uziv. skupinu.
     * 
     * V pripade ze je prazdna uz. skupina je nutne ty pristupe zneaktivnit!
     */
    function change_cms_group_for_employee($data){
        if(!isset($this->controller->ConnectionClientAtCompanyWorkPosition))
            $this->controller->loadModel('ConnectionClientAtCompanyWorkPosition');
            
        $this->controller->ConnectionClientAtCompanyWorkPosition->bindModel(array(
            'joinSpec'=>array('CmsUser'=>array('primaryKey'=>'CmsUser.at_employee_id','foreignKey'=>'ConnectionClientAtCompanyWorkPosition.client_id'))
        ));
        
        $con = array(
            'ConnectionClientAtCompanyWorkPosition.at_project_centre_work_position_id'=>$data['id'],
            'ConnectionClientAtCompanyWorkPosition.datum_to'=>'0000-00-00',
            'ConnectionClientAtCompanyWorkPosition.kos'=>0
        ); 
        $users = $this->controller->ConnectionClientAtCompanyWorkPosition->find('all',array(
            'conditions'=>$con,
            'fields'=>array('CmsUser.id')
        )); 
        if($users){
            $this->controller->loadModel('CmsUser');
            foreach($users as $user){
                if($user['CmsUser']['id'] != ''){
                    $this->controller->CmsUser->id = $user['CmsUser']['id'];
                    if($data['cms_group_id'] != ''){//menime uzivatelske konta
                        $this->controller->CmsUser->saveField('cms_group_id',$data['cms_group_id']);
                    }
                    else{//je nutne je zrusit
                        $this->controller->CmsUser->saveField('status',0);
                    }
                
                }
            } 
        }    
               
    }
    
    /**
     * Funkce ktera zajisti zruseni uctu pres ktery dany zamestannec pristupoval do DANEPu
     */
    function delete_account($client_id){
        $acc = self::client_have_cms_account($client_id);
        if($acc){
            $this->controller->CmsUser->id = $acc['CmsUser']['id'];
            $this->controller->CmsUser->saveField('status',0);
        }
    }
    
    /**
     * Funkce ktera zajisti zmenu group id pri zmena_pp_int zamestancu
     */
    function change_int_zam_account($client_id,$new_group_id){
        $acc = self::client_have_cms_account($client_id);
        if($acc){
            $this->controller->CmsUser->id = $acc['CmsUser']['id'];
            $this->controller->CmsUser->saveField('cms_group_id',$new_group_id);
        }
    }

    
}
?>