<?php
/**
 * @author Jakub Matus
 * @created 16.3.2010
 * 
 * komponenta ktera bude zdruzovat funkce pro Clients Controller
 * tak abychom zpřehlednili controller a oddělili funkce od controlu a jeho použití samotného
 * a tedy i objektový přístup k funkcím
 * 
 */
// Configure::write('debug',1);
class ClientsComponent extends Object{
    private $request_data = array();
    private $duplicity_list = array();
    private $duplicity_domwin = false; // duplicita defaultne vypnuta
    private $duplicity_www = false; // www podobni klienti
    private $at_interni = false;
    
	function startup(&$controller) {
		$this->controller = &$controller;
	}
    
    public function set($var,$value){
        $this->$var = $value;
    }
    
    public function set_variables_for_view(){
        $this->controller->set('duplicity_domwin',$this->duplicity_domwin);
        
        //vyhledani duplicitnich klientu v atepu
        if($this->duplicity_domwin && !$this->duplicity_www)
            self::search_duplicity_list($this->controller->data['Client']['id'],$this->controller->data['Client']['name']);
            
        //vyhledani podobnych klientu v atepu tem z www    
        if($this->duplicity_www)
            self::search_www_duplicity_list($this->controller->data['ImportClient']['mobil1'],$this->controller->data['ImportClient']['datum_narozeni']);
           
        $this->controller->set('duplicity_list',$this->duplicity_list);
        $this->controller->set('at_interni',$this->at_interni);
    }
    
    /**
     * nastaveni request dat 
     * nepouziva se set kvuli specialnimu nastavnei duplicity
     */
    public function set_request_data($data){
        $this->request_data = $data;
        
        if(in_array('duplicity_domwin',$data))
            $this->duplicity_domwin = $data['duplicity_domwin'];    
    }
    
    
    /**
     * vyhledani duplicitnich klientu v atepu podle jmena
     */
    private function search_duplicity_list($id,$cl_name){
        if(!isset($this->controller->Client))
        $this->controller->loadModel('Client');
        $list = $this->controller->Client->find('all',array(
            'conditions'=>array(
                'name'=>$cl_name,
                'id != '=>$id,
                'kos'=>0
            )
        ));
        
        $this->duplicity_list = $list;
    }
    
    /**
     * vyhledani podobnych klientu v atepu podle tech z www
     * dle mobilu nebo datumu narozeni
     */
    private function search_www_duplicity_list($mobil,$datum_narozeni){
        if(!isset($this->controller->Client))
            $this->controller->loadModel('Client');
        
        $list = $this->controller->Client->find('all',array(
            'conditions'=>array(
                'AND'=>array( 
                    'OR'=>array(
                        'datum_narozeni'=>$datum_narozeni,
                        'mobil1'=>$mobil,
                        'mobil2'=>$mobil,
                        'mobil3'=>$mobil,
                    ),
                    'kos'=>0
                )
            )
        ));
        
        $this->duplicity_list = $list;
    }

	

}
?>