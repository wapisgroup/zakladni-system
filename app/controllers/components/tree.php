<?php
class TreeComponent extends Object{
	
	function startup(&$controller) {
		$this->controller = &$controller;
		switch($this->controller->action){
			case 'tree_menu':				
				$frst = $this->controller->params['pass'][0];
				switch ($frst){
					case 'change_menu_item':
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
							$controller->modelClass = $class;
							$controller->modelKey = Inflector::underscore($class);
						}
						$this->modelClass = $this->controller->modelClass;
						$this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						if (isset($this->ScaffoldModel->nonTranslation) && $this->ScaffoldModel->nonTranslation == true){
							$this->ScaffoldModel->save(array($class=>array('id'=>	$this->controller->params['pass'][1],'name'=>$this->controller->params['pass'][2])));
						} else {
							// load Translation Model
							$this->controller->Translation->query('UPDATE wapis__anslations SET history=1 WHERE col="name" AND parent_id='.$this->controller->params['pass'][1].' AND modul="'.$class.'"');
							
							$tran_data = array(
								'Translation' => array(
									'modul'	=> $class,
									'history'=>0,
									'parent_id'=>$this->controller->params['pass'][1],
									'col'=>'name',
									'lang'=>'cz',
									'value'=>$_POST['menu_name']
								)
							);

							$this->controller->Translation->save($tran_data);
						}
						die();
			
						break;
					case 'delete_item':
						$item_id = $this->controller->params['pass'][1];
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
				            $controller->modelClass = $class;
				            $controller->modelKey = Inflector::underscore($class);
				        }
						
				        $this->modelClass = $this->controller->modelClass;
				        $this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						
						if ($this->ScaffoldModel->delete($item_id)){
							// load Translation Model
							$this->controller->loadModel('Translation');
							$this->Translation =& new Translation();
							$this->Translation->query('UPDATE wapis__translations SET history=1 WHERE parent_id='.$item_id.' AND modul="'.$class.'"');
							echo json_encode(array('result'=>true));
						} else 
							echo json_encode(array('result'=>false));
						
						die();
						break;
					case 'status_item':
						$item_id = $this->controller->params['pass'][1];
						$status = $this->controller->params['pass'][2];
						
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
				            $controller->modelClass = $class;
				            $controller->modelKey = Inflector::underscore($class);
				        }
						
				        $this->modelClass = $this->controller->modelClass;
				        $this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						$table_name = $this->ScaffoldModel->tablePrefix.$this->ScaffoldModel->table;
						if ($status == 0){
							$menu = $this->ScaffoldModel->read(null,$item_id);
							
							if (($menu[$this->ScaffoldModel->name]['rght'] - $menu[$this->ScaffoldModel->name]['lft']) >1){
								echo json_encode(array('result'=>true,'typ'=>'Multi'));
								$this->ScaffoldModel->query('UPDATE '.$table_name.' SET status=0 WHERE lft >= '.$menu['MenuItem']['lft'].' AND rght <= '.$menu['MenuItem']['rght'].'');
							} else {
								$this->ScaffoldModel->query('UPDATE '.$table_name.' SET status=0 WHERE id='.$item_id.'');
								echo json_encode(array('result'=>true,'typ'=>'Non-Multi'));
							}
						} else {
							$this->ScaffoldModel->query('UPDATE '.$table_name.' SET status=1 WHERE id='.$item_id.'');
							echo json_encode(array('result'=>true));
						}
						die();
						break;
					case 'move_item_up':
						$item_id = $this->controller->params['pass'][1];					
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
				            $controller->modelClass = $class;
				            $controller->modelKey = Inflector::underscore($class);
				        }
						
				        $this->modelClass = $this->controller->modelClass;
				        $this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						$this->ScaffoldModel->moveUp($item_id);
						die();
						break;
					case 'move_item_down':
						$item_id = $this->controller->params['pass'][1];
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
				            $controller->modelClass = $class;
				            $controller->modelKey = Inflector::underscore($class);
				        }
						
				        $this->modelClass = $this->controller->modelClass;
				        $this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						$this->ScaffoldModel->moveDown($item_id);
						die();
						break;	
					case 'move_item_top':
						$item_id = $this->controller->params['pass'][1];
						$steps = $this->controller->params['pass'][2];
						
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
				            $controller->modelClass = $class;
				            $controller->modelKey = Inflector::underscore($class);
				        }
						
				        $this->modelClass = $this->controller->modelClass;
				        $this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						for ($i=0;$i<$steps;$i++)
							$this->ScaffoldModel->moveUp($item_id);
						die();
						break;		
					case 'move_item_bottom':
						$item_id = $this->controller->params['pass'][1];
						$steps = $this->controller->params['pass'][2];
						
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
				            $controller->modelClass = $class;
				            $controller->modelKey = Inflector::underscore($class);
				        }
						
				        $this->modelClass = $this->controller->modelClass;
				        $this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						for ($i=0;$i<$steps;$i++)
							$this->ScaffoldModel->moveDown($item_id);
						die();
						break;		
					case 'move_item':
						$item_id = $this->controller->params['pass'][1];
						$parent_id = $this->controller->params['pass'][2];
						$steps = $this->controller->params['pass'][3];
						
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
						    $controller->modelClass = $class;
						    $controller->modelKey = Inflector::underscore($class);
						}
						
						$this->modelClass = $this->controller->modelClass;
						$this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						
						$this->ScaffoldModel->id = $item_id;
						if ($parent_id == 'root')
							$this->ScaffoldModel->saveField('parent_id',null);
						else
							$this->ScaffoldModel->saveField('parent_id',$parent_id);
						for ($i=0; $i<$steps;$i++)
								$this->ScaffoldModel->moveUp($item_id);
						die();
						break;		
					case 'add_item':
						$parent_id = $this->controller->params['pass'][1];
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
							$controller->modelClass = $class;
							$controller->modelKey = Inflector::underscore($class);
						}
						
						$this->modelClass = $this->controller->modelClass;
						$this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						
						if (isset($this->ScaffoldModel->nonTranslation) && $this->ScaffoldModel->nonTranslation == true){
							$this->ScaffoldModel->save(array($class => array('parent_id'=>$parent_id,'name'=>'Nova polozka')));
						} else {
							$this->ScaffoldModel->save(array($class => array('parent_id'=>$parent_id)));
							
							$tran_data = array(
								'Translation' => array(
									'modul'	=> $class,
									'history'=>0,
									'parent_id'=>$this->ScaffoldModel->id
								)
							);
														// load Translation Model
							$this->controller->loadModel('Translation');
							$this->Translation =& new Translation();
							
							foreach ($this->controller->languages as $lang=>$caption){
								$tran_data['Translation']['col'] = 'name';
								$tran_data['Translation']['lang'] = $lang;
								$tran_data['Translation']['value'] = 'Nova polozka';
								$this->Translation->save($tran_data);
								$this->Translation->id = null;
							}
						}
						echo json_encode(array('id' => Inflector::underscore($class).$this->ScaffoldModel->id));
						exit();
						break;
					case 'add_root':
						$class = $this->controller->uses[0];
						if (!empty($this->controller->uses) && class_exists($class)) {
							$controller->modelClass = $class;
							$controller->modelKey = Inflector::underscore($class);
						}
						
						$this->modelClass = $this->controller->modelClass;
						$this->modelKey = $this->controller->modelKey;
						$this->ScaffoldModel =& $this->controller->{$this->modelClass};
						if (isset($this->ScaffoldModel->nonTranslation) && $this->ScaffoldModel->nonTranslation == true){
							$this->ScaffoldModel->save(array($class => array('status'=>1,'name'=>'Nova polozka')));
						} else {
							$this->ScaffoldModel->save(array($class => array('status'=>1)));
							$tran_data = array(
								'Translation' => array(
									'modul'	=> $class,
									'history'=>0,
									'parent_id'=>$this->ScaffoldModel->id
								)
							);
							foreach ($this->controller->languages as $lang=>$caption){
								$tran_data['Translation']['col'] = 'name';
								$tran_data['Translation']['lang'] = $lang;
								$tran_data['Translation']['value'] = 'Nova polozka';
								$this->controller->Translation->save($tran_data);
								$this->controller->Translation->id = null;
							}
						}
						echo json_encode(array('id' => Inflector::underscore($class).$this->ScaffoldModel->id));
						die();
						break;
					default:
						break;
				}
				die();
				break;
			default:
				break;
		}
	}	
}
?>