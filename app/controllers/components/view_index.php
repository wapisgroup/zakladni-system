<?php
class ViewIndexComponent extends Object{
	var $components = array('Pagination','Session');
	
	function startup(&$controller) {
		$this->controller = &$controller;
	
		switch($this->controller->action){
		//	case 'edit': $this->edit(); break;
			case 'index':
				if (!isset($this->controller->noComponentIndex)){
					$this->renderSetting = $this->controller->renderSetting;
					$this->render_index();
				}
				break;
			case 'trash':
				//$this->trash($this->controller->params['pass'][0]);
				$this->render_trash($this->controller->params['pass'][0]);
				break;
			case 'status':
				//$this->status($this->controller->params['pass'][0],$this->controller->params['pass'][1]);
				$this->render_status($this->controller->params['pass'][1],$this->controller->params['pass'][0]);
				break;
			case 'trash_more':
				$this->render_trash_more();
				break;
			case 'active_more':
				$this->render_active_more();
				break;
			case 'deactive_more':
				$this->render_deactive_more();
				break;
			default:
				break;
		}
	}
	
	function render_trash_more(){
		$class = $this->controller->uses[0];
		if (isset($this->controller->params['url']['data'][$class]['id'])){
			foreach($this->controller->params['url']['data'][$class]['id'] as $key => $on)
				$this->render_trash($key,true);
		}
		die('done');
	}
	
	function render_deactive_more(){
		$class = $this->controller->uses[0];
		if (isset($this->controller->params['url']['data'][$class]['id'])){
			if (!empty($this->controller->uses) && class_exists($class)) {
	            $controller->modelClass = $class;
	            $controller->modelKey = Inflector::underscore($class);
	        }
			
	        $this->modelClass = $this->controller->modelClass;
	        $this->modelKey = $this->controller->modelKey;
			$this->ScaffoldModel =& $this->controller->{$this->modelClass};
			$p = array();
			foreach($this->controller->params['url']['data'][$class]['id'] as $key => $on)
				$p[] = $key;
			
			$table_name = $this->ScaffoldModel->tablePrefix.$this->ScaffoldModel->table;
			$this->ScaffoldModel->query("UPDATE {$table_name} SET status=0 WHERE id IN (".implode(',',$p).")");
			echo json_encode($p);
		}
		die();
	}
	
	function render_active_more(){
		$class = $this->controller->uses[0];
		if (isset($this->controller->params['url']['data'][$class]['id'])){
			if (!empty($this->controller->uses) && class_exists($class)) {
	            $controller->modelClass = $class;
	            $controller->modelKey = Inflector::underscore($class);
	        }
			
	        $this->modelClass = $this->controller->modelClass;
	        $this->modelKey = $this->controller->modelKey;
			$this->ScaffoldModel =& $this->controller->{$this->modelClass};
			$p = array();
			foreach($this->controller->params['url']['data'][$class]['id'] as $key => $on)
				$p[] = $key;
			
			$table_name = $this->ScaffoldModel->tablePrefix.$this->ScaffoldModel->table;
			$this->ScaffoldModel->query("UPDATE {$table_name} SET status=1 WHERE id IN (".implode(',',$p).")");
			echo json_encode($p);
		}
		die();
	}
	
	function render_trash($id,$multi=false){
		if ($id == null) die();
		$model_class = $this->controller->uses[0];
		
		if (substr($model_class,-4) == 'View')
			$model_class = substr($model_class,0,-4);
		
		$this->ScaffoldModel = $this->controller->{$model_class};


		$this->ScaffoldModel->id = $id;
		$this->ScaffoldModel->saveField('kos',1);
		$this->ScaffoldModel->id = null;
		$this->ScaffoldModel->data = null;

		if ($multi == false) die();
	}
	
	function render_status($status,$id){
		$class = $this->controller->uses[0];
		if (!empty($this->controller->uses) && class_exists($class)) {
            $controller->modelClass = $class;
            $controller->modelKey = Inflector::underscore($class);
        }
		
        $this->modelClass = $this->controller->modelClass;
        $this->modelKey = $this->controller->modelKey;
		$this->ScaffoldModel =& $this->controller->{$this->modelClass};

		$this->ScaffoldModel->save(array(
									$class=>array(
										'id' => $id,
										'status'=>($status==0)?1:0
									)));


        $this->controller->loadModel('HistoryAction');
        $this->controller->HistoryAction->save(array('HistoryAction'=>array(
            'kos'=>0,
            'controller'=>'CmsUsers',
            'created'=>date('Y-m-d H:i:s'),
            'action'=>'status',
            'url'=> '/cms_users/status/'.$id.'/'.$status,
            'cms_user_id'=>$this->controller->logged_user['CmsUser']['id'],
        )));
        unset($this->controller->HistoryAction);
		die();
	}
    
    function get_bindmodels($data){
        $data2 = $data; 
        $first = array_keys(array_shift($data));
        $second = array_values(array_shift($data2));
        
        $r = array_unique(am($second,$first));
        
        foreach($r as $id=>$item){ 
            if(is_int($item))
                unset($r[$id]);
        }
        
        return $r;
    }
	
	function render_index(){
		$recursive = 1;
        $criteria = array();
		$model_class = $this->controller->uses[0];
        if(!isset($this->renderSetting['no_trash']))
            $criteria[$model_class.'.kos'] = 0;
     
		if (!isset($this->renderSetting['translation_model']))
			$this->renderSetting['translation_model'] = 'Translation';
		
		// START permission
		$logged_user = $this->Session->read('logged_user');
		$cntrl_perm = $logged_user['CmsGroup']['permission'][$this->renderSetting['controller']];
			
		/**
		 * JEN SVÉ
		 */
        // manazer realizace
        if ($logged_user['CmsGroup']['id'] == 18 && $this->controller->name == 'Companies'){
            $criteria[] = 'CompanyView.manazer_realizace_id = '.$logged_user['CmsUser']['id'].' or CompanyView.manazer_realizace_id IS NULL';
            //echo $this->controller->name;
        } else
		if (isset($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['index']) && $logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['index'] == 2){
			if ((isset($cntrl_perm['group_id']) && !empty($cntrl_perm['group_id'])) || isset($this->renderSetting['TypUserCompany'])){ // pro companyController
				
				if (isset($this->renderSetting['permModel'])){
                    //pr($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]);
					//$criteria[$this->renderSetting['permModel'].'.'.$logged_user['CmsGroup']['permission']['companies']['group_id']]= $logged_user['CmsUser']['id'];
					$criteria[$this->renderSetting['permModel'].'.'.$logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['group_id']]= $logged_user['CmsUser']['id'];
                    if ($this->renderSetting['controller'] == 'int_clients'){
                        $this->renderSetting['SQLcondition']['OR'] = null;
                        unset($this->controller->renderSetting['SQLcondition']['OR']);
                    }
                    //pr($criteria);
                } else if($cntrl_perm['group_id'] == 'coordinator_double'){
					//$criteria[$model_class.'.'.'coordinator_id']= $logged_user['CmsUser']['id'];
					$criteria[$model_class.'.'.'coordinator_id2']= $logged_user['CmsUser']['id'];
				}
				else
					$criteria[$model_class.'.'.$cntrl_perm['group_id']]= $logged_user['CmsUser']['id'];
			} 
            else if(isset($this->renderSetting['only_his']) && (!isset($this->renderSetting['only_his']['group_con']) || $this->renderSetting['only_his']['group_con'] == $logged_user['CmsGroup']['id'])){ 
                if(isset($this->renderSetting['only_his']['all_set']) && $this->renderSetting['only_his']['all_set'] === true){
                     $where = $this->renderSetting['only_his']['where_col'].' = '.$logged_user['CmsUser']['id'];
                     
                     //novinka, muzeme where podminkovat na vice poli, bud AND nebo OR
                     if(is_array($this->renderSetting['only_his']['where_col'])){
                        $where_array = array();
                        foreach($this->renderSetting['only_his']['where_col'] as $col){$where_array[] = $col.' = '.$logged_user['CmsUser']['id'];}
                        $where = join((isset($this->renderSetting['only_his']['where_join'])?' '.$this->renderSetting['only_his']['where_join'].' ':' AND '),$where_array);
                     }
                     $criteria[]= (isset($this->renderSetting['only_his']['in_table'])?$this->renderSetting['only_his']['in_table']:$model_class).'.'.(isset($this->renderSetting['only_his']['in_col'])?$this->renderSetting['only_his']['in_col']:'id').' IN(SELECT '.$this->renderSetting['only_his']['col'].' FROM '.$this->renderSetting['only_his']['table'].' Where '.$where.')';	   
                }
                else if(isset($this->renderSetting['only_his']['this_table']) && $this->renderSetting['only_his']['this_table'] = true){//WHERE podminka na stejnou tabulku
                    //novinka, muzeme where podminkovat na vice poli, bud AND nebo OR
                     if(is_array($this->renderSetting['only_his']['where_col'])){
                        $where_array = array();
                        foreach($this->renderSetting['only_his']['where_col'] as $col){$where_array[] = $col.' = '.$logged_user['CmsUser']['id'];}
                        $where = join((isset($this->renderSetting['only_his']['where_join'])?' '.$this->renderSetting['only_his']['where_join'].' ':' AND '),$where_array);
                     }
                     else
                        $where = $this->renderSetting['only_his']['where_col'].' = '.$logged_user['CmsUser']['id'];
                     
                     $criteria[]=$where;
                }
                else if(isset($this->renderSetting['only_his']['subQuery'])){//SUBQUERY
                     $criteria[]= (isset($this->renderSetting['only_his']['in_table'])?$this->renderSetting['only_his']['in_table']:$model_class).'.'.(isset($this->renderSetting['only_his']['in_col'])?$this->renderSetting['only_his']['in_col']:'id').' IN('.strtr($this->renderSetting['only_his']['subQuery'],array('#CmsUser.id#'=>$logged_user['CmsUser']['id'])).')';	   
                }    
                else if(!isset($this->renderSetting['only_his']['group']) || $this->renderSetting['only_his']['group'] === false) // jen sve cms_user_id
                    $criteria[]= $model_class.'.id IN(SELECT '.$this->renderSetting['only_his']['col'].' FROM '.$this->renderSetting['only_his']['table'].' Where cms_user_id = '.$logged_user['CmsUser']['id'].')';	   
                else    
                    $criteria[]= $model_class.'.id IN(SELECT '.$this->renderSetting['only_his']['col'].' FROM '.$this->renderSetting['only_his']['table'].' Where cms_group_id = '.$logged_user['CmsGroup']['id'].')';	                   
            }           
            else {
				if (isset($this->renderSetting['permModel']))
					$criteria[$this->renderSetting['permModel'].'.cms_user_id']= $logged_user['CmsUser']['id'];
				else
					$criteria[$model_class.'.cms_user_id']= $logged_user['CmsUser']['id'];
			}
		} 
        /**
         * VŠE
         */
        else if (isset($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['index']) && $logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['index'] == 3){
			if(isset($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['spec_permision']) && !Empty($logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['spec_permision']))
				$criteria[]=$logged_user['CmsGroup']['permission'][$this->renderSetting['controller']]['spec_permision'];
			else if ((isset($cntrl_perm['group_id']) && !empty($cntrl_perm['group_id'])) || isset($this->renderSetting['TypUserCompany'])){ // pro companyController
				$model_class_coo = (($model_class == 'CompanyView' || in_array('CompanyView',$this->get_bindmodels($this->renderSetting['bindModel']))) ? 'CompanyView' : 'Company' );
                      
                if($model_class == 'ClientView')
					$model_class_coo = 'ClientView';
                    
				if(isset($cntrl_perm['group_id']) && ($cntrl_perm['group_id'] == 'coordinator_double' || $cntrl_perm['group_id'] == 'client_manager_id')){
					/**
					 * @since 9.11.2009
                     * @author Sol
                     * @abstract slouceni pristupu COO a CM k reportum
					 */
                    
                    $criteria['or'] = array(
						$model_class_coo.'.'.'coordinator_id' => $logged_user['CmsUser']['id'],
						$model_class_coo.'.'.'coordinator_id2' => $logged_user['CmsUser']['id'],
						$model_class_coo.'.'.'client_manager_id' => $logged_user['CmsUser']['id']
					);
				}
                else if(isset($cntrl_perm['group_id']) && $cntrl_perm['group_id'] == 'coordinator_new'){
                    $criteria[] = $model_class_coo.'.id IN (SELECT company_id FROM wapis__mpany_coordinators WHERE cms_user_id = '.$logged_user['CmsUser']['id'].' AND kos = 0)';
                }    
				else if(isset($cntrl_perm['group_id']) && $cntrl_perm['group_id'] <> ''){
					$criteria[$model_class_coo.'.'.$cntrl_perm['group_id']]= $logged_user['CmsUser']['id'];
				}
			}else if(isset($this->renderSetting['group_condition'])){ 
    			 foreach($this->renderSetting['group_condition'] as $group_cond){
    			     if(isset($group_cond['id']) && isset($group_cond['condition'])){
    			          if(
                            (!is_array($group_cond['id']) && $group_cond['id'] == $logged_user['CmsGroup']['id'])
                            ||
                            (is_array($group_cond['id']) && in_array($logged_user['CmsGroup']['id'],$group_cond['id']))
                          ){  
                            $criteria[] =strtr($group_cond['condition'],array('#CmsUser.id#'=>$logged_user['CmsUser']['id']));
                          }
                     }
    			 }
			}
            /** 
             * @uses audit_estate_loss_events
             * @uses at_employees
             **/ 
            else if(
                    isset($this->renderSetting['only_his2']) 
                    && isset($this->renderSetting['only_his2']['select_connection']) 
                    && $this->renderSetting['only_his2']['select_connection'] == true
                    && isset($cntrl_perm[$this->renderSetting['only_his2']['where_col']])
                    && $cntrl_perm[$this->renderSetting['only_his2']['where_col']] != ''
            ){ 
                $where = (isset($this->renderSetting['only_his2']['where_model'])?$this->renderSetting['only_his2']['where_model']:$model_class).'.'.$this->renderSetting['only_his2']['where_col'].' = '.$cntrl_perm[$this->renderSetting['only_his2']['where_col']];
                $criteria[] = $where;
            }    
            
		} else {
			$this->controller->redirect('/');
		}
		
				
		// END permission

		$criteria = am($criteria,$this->filtration());
		if(isset($this->renderSetting['count_condition'])){
			$count_criteria = $criteria;
			$count_criteria[] = $this->renderSetting['count_condition'];	
		}
        //pr($criteria);
			
            
        /**
         * Nastavnei count sloupce, pokud je nastaven v rendersettingu 
         */    
        if(isset($this->renderSetting['count_col']))
            $this->Pagination->count_col = $this->renderSetting['count_col'];
        /**
         * Nastavnei count_distinct, pokud je nastaven v rendersettingu 
         */    
        if(isset($this->renderSetting['count_distinct']))
            $this->Pagination->count_distinct = $this->renderSetting['count_distinct'];    
	
		$this->ScaffoldModel = $this->controller->{$model_class};
		$this->Pagination->ajaxDivUpdate = 'items';
			
		$this->Pagination->show = isset($this->renderSetting['sortShow'])?$this->renderSetting['sortShow']:20;
		list($field,$model,$direction) = explode('.',isset($this->renderSetting['sortBy'])?$this->renderSetting['sortBy']:$model_class.'.name.ASC');
		

		$this->Pagination->recursive = 2;
		$this->Pagination->sortBy = $model;
		$this->Pagination->sortByClass = $field;
		$this->Pagination->direction = $direction;
		
		$this->Pagination->url = '/'.(isset($this->renderSetting['controller'])?$this->renderSetting['controller']:$this->controller->params['url']['url']).'/';
		$this->Pagination->controller = $this->controller;
		if (isset($this->renderSetting['bindModel']))
			$this->ScaffoldModel->bindModel($this->renderSetting['bindModel']);
		
		// START BIND TRANSLATION //	
		$bindTranslation = array();
		if (isset($this->renderSetting['bindTranslation'])){
			foreach($this->renderSetting['bindTranslation'] as $column){
				$translationClass = 'Translation'.ucfirst($model_class).ucfirst($column);
				$bindTranslation[$translationClass] = array(
					'className'		=> 	$this->renderSetting['translation_model'],
					'foreignKey'	=>	'parent_id',
					'conditions'	=>	array(
						"$translationClass.col"		=> 	$column,
						"$translationClass.history" => 	0,
						"$translationClass.lang"	=>	'cz',
						"$translationClass.modul"	=>	ucfirst($model_class)
					),
					'fields'	=> array("$translationClass.value")
				);
			}	
		}
		// END BIND TRANSLATION //
	
		if(!isset($this->renderSetting['bindModel']['hasOne'])) 	$this->renderSetting['bindModel']['hasOne'] = array();
		if(!isset($this->renderSetting['bindModel']['belongsTo'])) 	$this->renderSetting['bindModel']['belongsTo'] = array();
		if(!isset($this->renderSetting['bindModel']['hasMany'])) 	$this->renderSetting['bindModel']['hasMany'] = array();
		

		$pagination_bind = $this->renderSetting['bindModel'];
		$pagination_bind['hasOne']  = (am($pagination_bind['hasOne'], $bindTranslation));
		
		list($order,$limit,$page) = $this->Pagination->init((isset($count_criteria) ? $count_criteria : $criteria),null,array('modelClass'=>$this->ScaffoldModel->name)); 
		
		if (isset($this->renderSetting['SQLhaving'])){
			if (is_array($this->renderSetting['SQLhaving'])){
				$criteria[] = '1=1 Having '.implode(' AND ', $this->renderSetting['SQLhaving']);
			} else {
				$criteria[] = '1=1 Having '.$this->renderSetting['SQLhaving'];
			}
		}	

		if (isset($this->renderSetting['bindModel']))
			$this->controller->{$model_class}->bindModel($this->renderSetting['bindModel']);
		
		$this->ScaffoldModel->bindModel(array('hasOne'=>$bindTranslation));
			
		if(isset($this->renderSetting['no_limit']) )
			$limit = null;    
            
		$this->criteria = $criteria;
	    
      
    		$this->controller->set(
    			'items',
    			$this->controller->{$model_class}->find(
    				'all', 
    				array(
    					'conditions'		=> $criteria, 
    					'fields'		=> $this->renderSetting['SQLfields'], 
    					'order'			=> $order, 
    					'limit'			=> $limit, 
    					'page'			=> $page,
    					'recursive' 		=> 1,
    					'group'			=> (isset($this->renderSetting['group_by'])?$this->renderSetting['group_by']:null)
    				)
    			)
    		);
		
		if (!isset($this->renderSetting['modelClass']))
			$this->renderSetting['modelClass'] = $model_class;
		
		$this->controller->set('renderSetting',$this->renderSetting);	
		$this->controller->set('page_caption',$this->renderSetting['page_caption']);
	}
	
	
	
	function replace_fnc($string,$Session){
		$logged_user = $this->Session->read('logged_user');
		$string = strtr($string,array(
			'#CmsUserId#' => $logged_user['CmsUser']['id'],
			'#CmsGroupId#' => $logged_user['CmsGroup']['id'],
            '#CmsUserAtEmployeeId#' => (isset($logged_user['CmsUser']['at_employee_id'])?$logged_user['CmsUser']['at_employee_id']:null),
			'#CURRENT_YEAR#' => defined('CURRENT_YEAR')?CURRENT_YEAR:'',
			'#CURRENT_MONTH#' => defined('CURRENT_MONTH')?CURRENT_MONTH:'',
			'#CUSTOM_ID#' => defined('CUSTOM_ID')?CUSTOM_ID:'',
		));

		return $string;

		/*if (strstr($string,'SESSION::') != false) {
			$start = strpos($string,'SESSION::');
			$end = strpos($string,'::END');
			$insider = substr($string,$start+9,$end-($start+9));
			list($session_variable,$db_path) = explode(':',$insider);
			$db_path = explode('/',$db_path);
			if ($Session->check($session_variable)){
				$session_variable = $Session->read($session_variable);
				foreach ($db_path as $field){
					if (isset($session_variable[$field])){
						$session_variable = $session_variable[$field];
					} else {
						die ('Chyba!, Neexistujici prvek v Session');
					}
				}
				return substr($string,0,$start).$session_variable.substr($string,$end+5);
			} else {
				die('Chyba!, Neexistujici Session');
			}
		} if (strstr($string,'PARAMS::') != false) {
			$start = strpos($string,'PARAMS::');
			$end = strpos($string,'::END');
			$insider = substr($string,$start+8,$end-($start+8));
			return substr($string,0,$start).$this->controller->params[$insider].substr($string,$end+5);
		} if (strstr($string,'PASS::') != false) {
			$start = strpos($string,'PASS::');
			$end = strpos($string,'::END');
			$insider = substr($string,$start+6,$end-($start+6));
			return substr($string,0,$start).$this->controller->params['pass'][$insider].substr($string,$end+5);
		} else {
			return $string;
		}
		*/
	}
		
	function foreach_SQLcondition($session, $array = array()){
		if (is_array($array)){
			foreach($array as $key=>$item){
				$array[$key] = $this->foreach_SQLcondition($session,$item);
			}
		} else {
			$array = $this->replace_fnc($array,$session);
		}
		return $array;
	}	
	
	function filtration(){
		$filtr = array();
		
		if (isset($this->controller->renderSetting['SQLcondition'])):
			$plc = $this->foreach_SQLcondition($this->Session,$this->controller->renderSetting['SQLcondition']);
			$filtr = $plc;
		endif;
        
		if (isset($this->controller->params['url'])){
			foreach($this->controller->params['url'] as $key => $value){
				if (substr($key,0,11) == 'filtration_'){
			
                    if ($value == '')
						unset($this->controller->params['url'][$key]);
					else {
						if(substr($key,-3)=="mlm"){
							$filtr['AND']=array(
								'ConnectionMlmRecruiter.created <='=>$value,
								array('OR'=>array('ConnectionMlmRecruiter.closed >='=>$value,'ConnectionMlmRecruiter.closed'=>'0000-00-00'))
							);
						}
                       
						if(substr($key,-5)=="cmkoo"){
							$params = explode('-',substr($key,11));
							$model = $params[0];
							$cols = $params[1];
							$cons = explode('|',$cols);
							$str = array();
							foreach($cons as $k=>$item){
								if (!empty($item))
									$str[$model.'.'.$item] = $value;
							}
							if (count($str) > 1)
								$filtr['AND']['OR'] = $str;
							else 
								$filtr = am($filtr, $str);
						}
						else if(substr($key,-3)=="cce"){
							//nic nedelej
						}
                        else if(substr($key,-7)=="int_ccr" && isset($filtr['OR'])){
						   $filtr['OR'] = am($filtr['OR'],array('ConnectionClientRecruiter.cms_user_id = '.$value));
						}
                        else if(substr($key,-17)=="internal_employee"){
						   $filtr['OR'] = array(
                                'ConnectionAuditEstate.client_id' => $value,
                                'ConnectionAuditEstate.cms_user_id = (SELECT id FROM wapis__cms_users WHERE at_employee_id ='.$value.' and kos = 0 and status = 1)'
                            );
						}
                        else if(substr($key,-12)=="zero_or_more"){
                           if($value == 1){
                             $filtr[strtr(substr($key,11),array('-'=>'.','|zero_or_more'=>'')).' > '] = '0';
                           } 
                           else
                             $filtr[strtr(substr($key,11),array('-'=>'.','|zero_or_more'=>'')).' = '] = '0';
						}
                        else if($value == "NOTNULL"){
							$filtr[strtr(substr($key,11),array('-'=>'.')).' <> '] = '';
						}
                        else if(substr($value,0,8) == "SUBQUERY"){
                            if(isset($this->controller->subQuery[substr($value,9)]))
							   $filtr=$this->controller->subQuery[substr($value,9)];
						}
                        /**
                         * funkce pro checkbox
                         * pokud je on a neni nastaven dalsi parametr
                         * hleda se value 1
                         */
                        else if($value == "on"){
							$value = 1;
                            //pozadujeme nejaky sloupce jako null
                            if(substr($key,-7) == "NOTNULL"){
                                $key =  substr($key,0,-8);  
                                $filtr[strtr(substr($key,11),array('-'=>'.')).' <> '] = ''; 
                            }
                            else if(substr($key,-8) == "NOTNULL2"){
                                $key =  substr($key,0,-9);  
                                $filtr[strtr(substr($key,11),array('-'=>'.')).' <> '] = '0000-00-00'; 
                            }
                            else{ 
                                $filtr[strtr(substr($key,11),array('-'=>'.'))] = $value; 
                            }
						}
						else if(substr($key,-5)=="month"){
							$value = ($value < 10? '0'.$value : $value);
							$filtr['AND']=array('MONTH(SmsMessage.created)'=>$value);
						}
						else if(substr($key,-4)=="year"){
							$filtr['AND']=array('YEAR(SmsMessage.created)'=>$value);
						}
                        else if(substr($key,-5)=="osoba"){
							$filtr['AND']=array('((AuditEstate.stav = 1 AND Client.name LIKE "%'.$value.'%") OR (AuditEstate.stav = 0 AND SpravceMajetku.name LIKE "%'.$value.'%"))');
						}
                        else if (strrpos($key,"||")){
								$key = strtr(substr($key,11),array('-'=>'.'));
								$cons = explode('|',$key);
								$str = array();
								foreach($cons as $k=>$item){
									if (!empty($item))
										$str[$item] = $value;
								}
								if (count($str) > 1)
									$filtr['AND']['OR'] = $str;
								else 
									$filtr = am($filtr, $str);
						}
						else if (strrpos($key,"|")){
								$key = strtr(substr($key,11),array('-'=>'.'));
								$cons = explode('|',$key);
								$str = array();
								foreach($cons as $k=>$item){
									if (!empty($item))
										$str[$item . ' LIKE'] = '%'.$value.'%';
								}
								if (count($str) > 1)
									$filtr['AND']['OR'] = $str;
								else 
									$filtr = am($filtr, $str);
							} else {
								if (substr($key,-5) == 'month')
									$filtr['MONTH('.strtr(substr($key,11,-6),array('-'=>'.')).')'] = $value;
								elseif (substr($key,-4) == 'name')
									$filtr[strtr(substr($key,11),array('-'=>'.')).' LIKE'] = '%'.$value.'%';
								elseif (substr($key,-4) == 'from')
									$filtr[strtr(substr($key,11,-5),array('-'=>'.')).' >= '] = $value;
								elseif (substr($key,-3) == '-to')
									$filtr[strtr(substr($key,11,-3),array('-'=>'.')).' <= '] = $value;
								else { 
									$filtr[strtr(substr($key,11),array('-'=>'.'))] = (strpos($value,'|') === false)?$value:explode('|',$value);
								}
							}
					}
				}
			}
		}
		return $filtr;
	}
    
    
    
    function generate_td($item, $td, $viewVars = array()){

        App::import('Helper', 'Fastest');
        $loadHelper = 'FastestHelper';
        $this->Fastest = new $loadHelper();   
        
		$value =  @$item[$td['model']][$td['col']];
		switch($td['type']){
			case 'text':
				if (!empty($td['fnc'])){
					list($fnc, $params) = explode('#', $td['fnc']);
					return $this->Fastest->$fnc($value,$params);
				} else {
					return $value;
				}
				break;
			case 'procenta':
				return $value .(empty($value)?'':'%');
				break;
			case 'download':
				return ((($value!='')?'<a href="'.$td['fnc'].'/null/null/'.$value.'">Otevřít soubor</a>':''));
				break;
			
			case 'datetime':
				return ($value!='0000-00-00 00:00:00' && $value != null)?$this->Fastest->czechDateTime($value,$td['fnc']):'';
				break;
			case 'date':
				return ($value !='0000-00-00' && $value !='0000-00-00 00:00:00' && $value != null)?$this->Fastest->czechDate($value, $td['fnc']):'';
				break;
			case 'var':
				return @$viewVars[$td['fnc']][$value];
				break;
			case 'viewVars':
				return @$viewVars[$td['fnc']][$value];
				break;
			case 'hidden':
				return;
				break;
			case 'money':
				return number_format($value, 0, ',', ' ');;
				break;
			case 'pocet_dni_po_splatnosti':
				$po_splatnosti = (strtotime(date('Y-m-d')) - strtotime($item[$td['model']]['datum_splatnosti'])) / 3600 /24;
				if ($po_splatnosti > 0) {
					if ($item[$td['model']]['datum_uhrady'] != '0000-00-00')
						return 'Uhrazeno';
					else 
						return $po_splatnosti;
				} else
					return ;
				break;
		}
	}
}
?>