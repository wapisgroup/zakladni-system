<?php 
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 17.9.2009
 */
class SmsClientComponent{
    	var $controller;

    	/**
    	 * Inicializace komponenty
    	 * @param $controller
    	 * @return void
    	 */
    	function startup( &$controller ) {
			$this->controller = &$controller;
    	}
        
        /**
         * funkce pouze odstranuje diakritiku pomoci strtr
         * postupne se musi pridat veskere znaky
         * @author Sol
         * @created 29.12.09
         */
         private function removing_diacritical($text){
                 static $convertTable = array (
        
                'á' => 'a', 'Á' => 'A', 'ä' => 'a', 'Ä' => 'A', 'č' => 'c',
        
                'Č' => 'C', 'ď' => 'd', 'Ď' => 'D', 'é' => 'e', 'É' => 'E',
        
                'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'í' => 'i',
        
                'Í' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ľ' => 'l', 'Ľ' => 'L',
        
                'ĺ' => 'l', 'Ĺ' => 'L', 'ň' => 'n', 'Ň' => 'N', 'ń' => 'n',
        
                'Ń' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ö' => 'o', 'Ö' => 'O',
        
                'ř' => 'r', 'Ř' => 'R', 'ŕ' => 'r', 'Ŕ' => 'R', 'š' => 's',
        
                'Š' => 'S', 'ś' => 's', 'Ś' => 'S', 'ť' => 't', 'Ť' => 'T',
        
                'ú' => 'u', 'Ú' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ü' => 'u',
        
                'Ü' => 'U', 'ý' => 'y', 'Ý' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y',
        
                'ž' => 'z', 'Ž' => 'Z', 'ź' => 'z', 'Ź' => 'Z',
        
                );
                
                return strtr($text, $convertTable);
         }
        

    	/**
    	 * Odeslani SMS s danym nastavenim
    	 * @param $setting
    	 * @return Array
    	 */
    	function send($setting = array()){
            return null;
    		$template_id = null;
    		$multi = false;
    		$phone_number = '';
    		$phone_number_list = array();
    		$replace_list = array();
    		$message = 'SMS zprava';
    		$group_id = -1;
    		$client_name ='';
    		$client_id = 0;
    		$model = '';
    		$testing = false;
    		$testing_output = array(
    			'<?xml version="1.0"?><batch><status>200</status><message>Vse OK</message><smsid></smsid></batch>',
    			'<?xml version="1.0"?><batch><status>400</status><message>Invalid recipient address (MSISDN)</message><smsid></smsid></batch>',
    			'<?xml version="1.0"?><batch><status>333</status><message>Jina chyba</message><smsid></smsid></batch>'
    		);
    		extract($setting);
    		
            /**
             * prevaleni specalnich znaku na entity
             * napr & za &amp; 
             * a odstraneni diakritiky
             */
            $message = htmlspecialchars(self::removing_diacritical($message));   
            
    		if ($multi === false){
				$SMS = '<?xml version="1.0" encoding="utf-8" ?> <batch id=""> <request>textSMS</request>  <recipient>'.$phone_number.'</recipient> <content>'.$message.'</content>  <udh />  <delivery_report>0</delivery_report><custid>ATEP</custid>   </batch>'; 			
				$context = stream_context_create( array( 'http' => array( 'method'=>"POST", 'Host' => 'apiserver', 'header'=>"Content-Type: text/xml", 'content'=>$SMS ) ) );
				if ($testing === false){						
					$file = file_get_contents('http://directsmsapi.sluzba.cz/apifastest/receiver.asp', false, $context);
				} else {
					$file = $testing_output[array_rand($testing_output)];	
				}
				$doc = simplexml_load_string($file);
				switch ($doc->status){
					case '200':
						$output = array('result'=>true,'message'=>'', 'phone'=>$phone_number);
						break;
					case '400':
						$output = array('result'=>false,'message'=>'Neplatné telefonní číslo', 'phone'=>$phone_number);
						break;
					default:
						$output = array('result'=>false,'message'=>'STATUS: '.$doc->status.'. Neošetřená chyba při odesílání SMS', 'phone'=>$phone_number);
						break;
				}
			}
			
			$this->controller->loadModel('SmsMessage');
            $this->controller->SmsMessage->id = null;
			$this->controller->SmsMessage->save(array('SmsMessage'=>array(
				'phone' => $phone_number,
				'text' => $message,
				'group_id' => $group_id,
				'status_sms' => $doc->status,
				'error_message' => $output['message'],
				'client_name' => $client_name,
				'client_id' => $client_id,
				'model' => $model
			)));
            $this->controller->SmsMessage->id = null;
			unset($this->controller->SmsMessage);
			return $output;
	}
}
?>