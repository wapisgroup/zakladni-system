<?php
/**
 * @author Jakub Matus
 * @created 4.1.2011
 * 
 * komponenta ktera bude zdruzovat funkce pro Employees Controller - hlavne pro Odpracovane hodiny
 * tak abychom zpřehlednili controller a oddělili funkce od controlu a jeho použití samotného
 * 
 */
//Configure::write('debug',2);
class NewEmployeesComponent extends Object{
    private $connection_id;
    private $from;
    private $year; 
    private $month;
    private $this_data;
    private $dovolena = array();
    public $permission;
    public $logged_user;
    private $h1000 = false;
    private $odmeny = array( 
        1 => array(
            'label'=>'Odměna 1',
            'max'=>0.00
        ),
        2 => array(
            'label'=>'Odměna 2',
             'max'=>0.00
        )
    );

	function startup(&$controller) {
		$this->controller = &$controller;
	}
    
    public function set($var,$value){
        $this->$var = $value;
    }
    
    public function set_variables_for_view(){
        $this->controller->set('odmeny',$this->odmeny);
        $this->controller->set('permission_alow',self::permision_to_edit_mi_and_wp());
        $this->controller->set('zbyva_dovolene',self::zbyva_dovolene());
        $this->controller->set('dovolena',$this->dovolena);
        $this->controller->set('opp_list',self::drawback_for_opp());
        $this->controller->set('money_item_setting',self::get_money_item_setting());
    }
    
    /**
     * Funkce vytahuje nastavení
     * vytahujem nastaveni srazek, bonusu a priplatku
     * type_id : 0 - Bonusy
     *           1 - Srážky
     *           2 - Příplatky
     */
    private function get_money_item_setting(){
        //pr($this->this_data);
        $new_money_item_id = $this->this_data['ConnectionClientRequirement']['company_money_item_id'];
        
        $transfer_key = array('id','mzda_typ','zakladni_mzda',
            'normo_hodina','pocet_dni_dovolene_za_rok','stravenky','stravne'
        );
        $key_with_other_name = array(
            'normo_hodina'=>'standard_hours',
            'id'=>'new_money_item_id'
        );
        
        $this->controller->loadModel('NewMoneyItem');
        $new_money_item_setting = $this->controller->NewMoneyItem->read($transfer_key,$new_money_item_id);
        foreach($transfer_key as $key){
            if($key == 'zakladni_mzda')
                continue;
            
            $key_dochazka = $key;
            //Pokud je jiny nazev sloupce v DB pro dochazku nez v kalkulaci tak zmen nazev
            if(array_key_exists($key,$key_with_other_name)){$key_dochazka = $key_with_other_name[$key];}
                $this->controller->data['NewClientWorkingHour'][$key_dochazka] = $new_money_item_setting['NewMoneyItem'][$key];
        }
        
        //zakladni mzda se muze editovat, vpripade ze je uz uloznea, ber jej z dochazek
        if(!isset($this->controller->data['NewClientWorkingHour']['zakladni_mzda']))
            $this->controller->data['NewClientWorkingHour']['zakladni_mzda'] = $new_money_item_setting['NewMoneyItem']['zakladni_mzda'];

        $this->controller->loadModel('ConnectionMoneyItemSetting');
        $this->controller->ConnectionMoneyItemSetting->bindModel(array(
            'belongsTo'=>array('MoneyItemConnectionTool')
        ));
        $setting = $this->controller->ConnectionMoneyItemSetting->find('all',array(
            'conditions'=>array('ConnectionMoneyItemSetting.kos'=>0,'ConnectionMoneyItemSetting.new_money_item_id'=>$new_money_item_id)
        ));
        
        /**
         * Nahrani connection k dane dochazce
         */
        $connection_cwh_to_mis = array();    
        if($this->controller->data['NewClientWorkingHour']['id'] != ''){
            $this->controller->loadModel('ConnectionClientWorkingHourToMoneySetting');
            $exist_con = $this->controller->ConnectionClientWorkingHourToMoneySetting->find('all',array(
                'conditions'=>array(
                       'new_client_working_hour_id'=>$this->controller->data['NewClientWorkingHour']['id'],
                       'kos'=>0
                ),
                'fields'=>array('type_id','connection_money_item_settings_id','val','monthly')
            ));
            if($exist_con)
             $connection_cwh_to_mis = Set::combine($exist_con,'{n}.ConnectionClientWorkingHourToMoneySetting.connection_money_item_settings_id','{n}.ConnectionClientWorkingHourToMoneySetting','{n}.ConnectionClientWorkingHourToMoneySetting.type_id');
            
        }
        
        $this->controller->set('connection_cwh_to_mis',$connection_cwh_to_mis);
        return Set::combine($setting,'{n}.ConnectionMoneyItemSetting.id','{n}','{n}.ConnectionMoneyItemSetting.type_id');
    }
        
    
    /**
     * nastaveni pole odmen
     * @param data - obsha nastaveni pro odmeny
     * @param $spec - muzeme specifikovat dat pouze pro konkretni odmenu, tedy 1 nebo 2
     * @param $col - sloupec ktery chceme nastavit pro konkretni sloupec
     */
    public function set_new_odmeny($data, $spec = false, $col = false){
         if($spec && ($spec == 1 || $spec == 2)){
            if($col)
                $this->odmeny[$spec][$col] = $data;
            else    
                $this->odmeny[$spec] = $data;
         }
         else
            $this->odmeny = $data;  
    }
    


    /**
     * funkce ktera kontroluje zmenu udaju u ulozeni autorizovane dochazky
     * @return string text
     */
	public function check_changes_in_wh($data){
	    $text = '';
       
        if(!isset($this->controller->NewClientWorkingHour))
            $this->controller->loadModel('NewClientWorkingHour');
        
        /**
         * polozky ktere se maji hlidat, je nutne je vytahnout ktere jsou v db
         * a porovnat v tech ktere ukladame
         * nastavujeme 
         *  nazec atributu
         *  text ktery se ma zobrazit pri zmene
         */
        $whats_checks = array(
            'celkem_hodin' => 'Celkem hodin bylo změněno',
            'salary_per_hour' => 'Na hodinu celkem se změnilo',
            'stop_payment' => 'Pozastavení mzdy je změněno'
        ); 
        
        
        $is_saves_values = $this->controller->NewClientWorkingHour->read(array_keys($whats_checks),$data['NewClientWorkingHour']['id']);   
       
        /**
         * pokud jsou hodnoty rozdilne, tak vypis text zmeny
         */
	    foreach($whats_checks as $att=>$msg){
	       if(is_double($is_saves_values['NewClientWorkingHour'][$att]) && (round($is_saves_values['NewClientWorkingHour'][$att],3) != round($data['NewClientWorkingHour'][$att],3))){
	         if($att != 'stop_payment')
                $text .= $msg.' z '.$is_saves_values['NewClientWorkingHour'][$att].' na '.$data['NewClientWorkingHour'][$att].'<br/>';
             else   
                $text .= $msg.' z '.$this->controller->ano_ne_checkbox[$is_saves_values['NewClientWorkingHour'][$att]].' na '.$this->controller->ano_ne_checkbox[$data['NewClientWorkingHour'][$att]].'<br/>';
	       }
	       else if($is_saves_values['NewClientWorkingHour'][$att] != $data['NewClientWorkingHour'][$att]){  
             if($att != 'stop_payment')
                $text .= $msg.' z '.$is_saves_values['NewClientWorkingHour'][$att].' na '.$data['NewClientWorkingHour'][$att].'<br/>';
             else   
                $text .= $msg.' z '.$this->controller->ano_ne_checkbox[$is_saves_values['NewClientWorkingHour'][$att]].' na '.$this->controller->ano_ne_checkbox[$data['NewClientWorkingHour'][$att]].'<br/>';
           }
        }
        
        //pocatecni hlaska bloku
        if($text != '')
            $text = '<br />Byly zaznamenány následujicí změny:<br />'.$text;
        
        return $text;
    
    }
    
    
    /**
     * POKUD je nastaveno v permission povoleni pro editaci forem a profesi na dochazek
     * nastav skupinu uzivatelu ktere to je povoleno
     */
    function permision_to_edit_mi_and_wp(){
       if(isset($this->permission['editable_money_item_workposition']) && $this->permission['editable_money_item_workposition'] == 1){
        return array(0 => $this->logged_user['CmsGroup']['id']);     
       }
       else
        return array();
    }
    
    
    function zbyva_dovolene(){
        //Configure::write('debug',1);
        $koeficient = 0.0833;
        $pocet_dovolene_v_akutalni_dochazce = $this->this_data['NewClientWorkingHour']['dovolena'];
        $pocet_oh_v_akutalni_dochazce = $this->this_data['NewClientWorkingHour']['working_days_count'];
        
        if(!isset($this->controller->NewClientWorkingHour))
            $this->controller->loadModel('NewClientWorkingHour');
        
        $f = $this->controller->NewClientWorkingHour->find('first',array(
            'conditions'=>array(
                'connection_client_requirement_id'=>$this->connection_id
            ),
            'fields'=>array(
                'SUM(dovolena) as sum_dovolene, SUM(working_days_count) as sum_odpracovanych_dni'
            ),
            'group'=>'connection_client_requirement_id'
        ));
            
        $sum_dovolene = $sum_odpracovanych_dni = 0;        
        if(isset($f) && !empty($f)){
            $zbyva_dovolene = floor($f[0]['sum_odpracovanych_dni']*$koeficient);
            $sum_dovolene = $f[0]['sum_dovolene'];
            $sum_odpracovanych_dni = $f[0]['sum_odpracovanych_dni'];
            $zbyva_dovolene = $zbyva_dovolene - ($f[0]['sum_dovolene']); 
        }
        else
            $zbyva_dovolene = 0;
            
        //defaultni pocet vybrane dovolene    
        $this->dovolena = array(
            'zbyva_dovolene' => ($zbyva_dovolene + $pocet_dovolene_v_akutalni_dochazce),  
            'd_sum_odpracovanych_dni' => ($sum_odpracovanych_dni - $pocet_oh_v_akutalni_dochazce),  
            'koeficient'=>$koeficient
        );  
        
        return $zbyva_dovolene;
              
    }
    
    /**
     * Kontrolni funkce ktera ma za ukol,
     * zkontrolovat zda dana connection pro mesic a rok uz nema ulozenou dochazku.
     * Pokud ano vyhod chybu. Timto by meli byt osetreny duplicitni dochazky
     */
    public function if_exist_client_working_hour(){
        if(!isset($this->controller->NewClientWorkingHour))
            $this->controller->loadModel('NewClientWorkingHour');
        
        $f = $this->controller->NewClientWorkingHour->find('first',array(
            'conditions'=>array(
                'NewClientWorkingHour.connection_client_requirement_id'=>$this->connection_id,
                'NewClientWorkingHour.year'=>$this->year,
                'NewClientWorkingHour.month'=>$this->month
            ),
            'fields'=>array('id')
        ));
        
        if($f)
            return true;
        else
            return false;    
    }
    
    /**
     * Nacteni seznamu pridelenych OPP v danem mesici a roce
     * a nastaveni hodnoty srazky za tyto OPP ktere meli narok = ne
     */
    private function drawback_for_opp(){
        //Configure::write('debug',2);
         if(!isset($this->controller->ConnectionClientOpp))
            $this->controller->loadModel('ConnectionClientOpp');
        
        $this->controller->ConnectionClientOpp->bindModel(array(
            'belongsTo'=>array('ConnectionClientRequirement')
        ));
        $f = $this->controller->ConnectionClientOpp->find('all',array(
            'conditions'=>array(
					'ConnectionClientOpp.kos'=>0,
                    'ConnectionClientOpp.connection_client_requirement_id'=>$this->connection_id,
                    'OR'=>array(
                        array(
                            'ConnectionClientOpp.narok'=>'ne',
                			'(DATE_FORMAT(ConnectionClientOpp.created,"%Y-%m"))' => $this->year.'-'.$this->month,   
                        ),
                        array(
                            'ConnectionClientOpp.narok'=>'ano',
                            'ConnectionClientOpp.srazka_ze_mzdy'=>1,
                            '(DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m"))' => $this->year.'-'.$this->month,   
                        )
                    )    
                            
			),
            'fields'=>array('ConnectionClientOpp.id','ConnectionClientOpp.typ','ConnectionClientOpp.name','ConnectionClientOpp.size','ConnectionClientOpp.price')
        ));
        

        $this->controller->set('opp_sum',(isset($f) && !empty($f) ? array_sum(Set::extract($f,'/ConnectionClientOpp/price')) : 0));
        return $f; 
    }

}
?>