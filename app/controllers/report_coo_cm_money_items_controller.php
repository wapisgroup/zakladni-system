<?php
//Configure::write('debug',1);

class ReportCooCmMoneyItemsController extends AppController {
	var $name = 'ReportCooCmMoneyItems';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CompanyMoneyItem');
	var $renderSetting = array(
		'bindModel'	=> array(
            'belongsTo'=>array('Company','CompanyMoneyValidity','CompanyWorkPosition')
        ),
		'controller'=>'report_coo_cm_money_items',
		//'SQLfields' => array('CompanyMoneyItem.fakturacni_sazba_na_hodinu','CompanyMoneyItem.provinzni_marze','CompanyMoneyItem.vypocitana_celkova_cista_maximalni_mzda_na_hodinu','CompanyMoneyItem.cena_ubytovani_na_mesic','Company.name','Company.self_manager_id','Company.client_manager_id','Company.coordinator_id','CompanyWorkPosition.name','CompanyMoneyValidity.platnost_od','CompanyMoneyItem.id','CompanyMoneyItem.name','CompanyMoneyItem.created','Company.client_manager_id','CompanyMoneyItem.id'),
		'SQLfields' => array('*'),
        'page_caption'=>'Report - Kalkulace podniku CM/KOO',
		'sortBy'=>'CompanyMoneyItem.created.DESC',
		'SQLcondition'=>array(
			'CompanyMoneyValidity.platnost_do'=>'0000-00-00',
			'CompanyMoneyItem.schvaleno'=>0,
			'CompanyMoneyItem.kos'=>0,
			'CompanyWorkPosition.kos'=>0
		),
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'CompanyWorkPosition-name|'	=>	'text|Pozice|',
			'Company-self_manager_id'		=>	'select|SM|cms_user_list',
			'CompanyMoneyItem-company_id'		=>	'select|Firma|company_list'
		),
		'items' => array(
			'id'		=>	'ID|CompanyMoneyItem|id|hidden|',
			'self_manager_id' 		=>	'SalesManager|Company|self_manager_id|hidden|',
			'company'	=>	'Firma|Company|name|text|',
			'sm'		=>	'SM|Company|self_manager_id|viewVars|cms_user_list',
            'platnost'	=>	'Platnost od|CompanyMoneyValidity|platnost_od|date|',
			'pozice'	=>	'Profese|CompanyWorkPosition|name|text|',
            'typ'		=>	'Forma odměny|CompanyMoneyItem|name|text|',
           	'ps'		=>	'PS|CompanyMoneyItem|cista_mzda_z_pracovni_smlouvy_na_hodinu|text|',
			'd'			=>	'D|CompanyMoneyItem|cista_mzda_dohoda_na_hodinu|text|',
			'f'			=>	'F|CompanyMoneyItem|cista_mzda_faktura_zivnostnika_na_hodinu|text|',
			'c'			=>	'C|CompanyMoneyItem|cista_mzda_cash_na_hodinu|text|',
			'max'		=>	'Max|CompanyMoneyItem|vypocitana_celkova_cista_maximalni_mzda_na_hodinu|money|',
			'ubytovani'		=>	'Ubytovani|CompanyMoneyItem|cena_ubytovani_na_mesic|text|value_to_yes_no#long',
			'doprava'		=>	'Doprava|CompanyMoneyItem|doprava|text|value_to_yes_no#long',
			'stravenka'		=>	'Stravenky|CompanyMoneyItem|stravenka|text|value_to_yes_no#long',
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
		//	'edit'		=>	'edit|Editovat položku|edit',			
		), 
        'domwin_setting' => array(
            'sizes' => '[1000,1000]', 
            'scrollbars' => true, 
            'languages' => true 
        )
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Kalkulace firem CM/KOO'=>'#'));
		
			$this->loadModel('CmsUser'); 
			$this->loadModel('Company'); 
			$this->set('cms_user_list',		$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>array(2)))));
			$this->set('company_list',		$this->Company->find('list',array(
				'conditions'=>array(
					'Company.kos'=>0
				),
				'order'=>array('Company.name ASC')
			)));
			unset($this->Company);
			unset($this->CmsUser);
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id){
		echo $this->requestAction('companies/work_money_edit/-1/-1/' . $id);
		die();
	}
	
}
?>