<?php
//Configure::write('debug',1);
class MoneyItemBonusesController extends AppController {
	var $name = 'MoneyItemBonuses';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('MoneyItemBonus');
	var $renderSetting = array(
		'controller'=>'money_item_bonuses',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'Kalkulace - nastavení bonusů',
		'sortBy'=>'MoneyItemBonus.created.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat důvod|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingAccommodationType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingAccommodationType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|MoneyItemBonus|id|text|',
			'name'		=>	'Název|MoneyItemBonus|name|text|',
			'updated'	=>	'Upraveno|MoneyItemBonus|updated|datetime|',
			'created'	=>	'Vytvořeno|MoneyItemBonus|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->MoneyItemBonus->read(null,$id);
                
            $this->set('connection_tools',$this->get_list('MoneyItemConnectionTool'));    
			$this->render('edit');
		} else {
			$this->MoneyItemBonus->save($this->data);
			die();
		}
	}
}
?>