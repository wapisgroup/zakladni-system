<?php
Configure::write('debug',1);
class ClientEstorsController extends AppController
{
    var $name = 'ClientEstors';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex', 'FileInput');
    var $components = array('ViewIndex', 'RequestHandler', 'Upload', 'Email','Clients','IntEmployee');
    var $uses = array('Client');
    var $renderSetting = array(
        'bindModel' => array(
            
        ),
        'SQLcondition' => array(
            'Client.client_type' => 2
        ),
        'SQLfields' => '*',
        'controller' => 'client_estors',
        'page_caption' => 'Estor building – stavebny zamestnanci',
            //'count_group_by' => 'ClientView.id',
        'group_by' => 'Client.id',
        'sortBy' => 'Client.id.ASC',
        'top_action' =>array(
            // caption|url|description|permission
            'add_item' => 'Přidat|edit|Pridat popis|add',
            'add_express_item' => 'Přidat Express|edit_express|Pridat popis|add_express',
            'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel',
        ),
        'filtration' => array(
            'Client-name|' => 'text|Jméno|',
            'Client-stav' => 'select|Status|stav_client_list',
            //'ConnectionClientCareerItem-setting_career_item_id' => 'select|Profese|profese_list',
            //'ConnectionClientRecruiter-cms_user_id#admin_ctrl' => 'select|Recruiter|cms_user_list',
            //'ConnectionClientRequirement-company_id' => 'select|Firma|company_list',
            //'Client-next|'		=>	'select|Další|next_list',
            //'Client-mesto|' => 'text|Město|',
            //'Client-countries_id' => 'select|Okres|countries_list',

            //'ClientView-telefon1|'	=>	'text|Telefon|',
        ),
        'items' => array(
            //'client_manager_id' => 'cmid|ClientView|client_manager_id|hidden|',
            'id' => 'ID|Client|id|text|',
            'name' => 'Jméno|Client|name|text|',
            //'profese' => 'Profese|ClientView|Profese|text|',
            'mobil' => 'Telefon|Client|mobil|text|',
            'mesto' => 'Město|Client|mesto|text|',
            'datum_narozeni' => 'Datum narození|Client|datum_narozeni|date|',
            //'status' => 'Status|ClientView|stav|var|stav_client_list',
            //'datum_nastupu' => 'Datum nástupu|ConnectionClientRequirement|from|date|',
            //'firma' => 'Firma|ClientView|company|text|',
            //'cm' => 'CM|ClientView|client_manager|text|',
            //'coo' => 'COO|ClientView|coordinator|text|',
            //'coo2' => 'COO2|ClientView|coordinator2|text|',
            //'status_imp' => 'StatusImport|ClientView|import_stav|var|stav_importu_list',
            //'externi_nabor' => 'Typ|ClientView|externi_nabor|var|client_en_type',
            //	'updated'	=>	'Změněno|ClientView|updated|datetime|',
            'created' => 'Vytvořeno|ClientView|created|datetime|',
            'express' => '#|ClientView|express|text|status_to_ico#express'
        ),
        'posibility' => array(
            'edit' => 'edit|Editace položky|edit',
            //'attach' => 'attachs|Přílohy|attach',
            //'message' => 'messages|Zprávy|message',
            //'vyhodit' => 'rozvazat_prac_pomer|Rozvázat pracovní poměr|rozvazat_prac_pomer',
            //'zmena_pp' => 'zmena_pp|Změna pracovního poměru|zmena_pp',
            //'stats' => 'stats|Statistiky spojení|stats',
            //'domwin_add_activity'	=>	'domwin_add_activity|Přidání aktivity|domwin_add_activity',
            'delete' => 'trash|Odstranit položku|trash',

        ),
        'domwin_setting' => array(
            'sizes' => '[1000,1000]',
            'scrollbars' => true,
            'languages' => true
        )
    );
    
    function beforeFilter(){
        parent::beforeFilter();
        
        /**
         * povolit klienty i EN
         */
        //$this->ClientView->set_ignore_status(true);
               
        
        if(isset($_GET['filtration_ClientView-next|']) && !empty($_GET['filtration_ClientView-next|'])){
            list($col,$value) = explode('-',$_GET['filtration_ClientView-next|']);

            $this->params['url']['filtration_ClientView-'.$col] = $value;
            
            unset($this->params['url']['filtration_ClientView-next|']); 
            unset($col);
            unset($value);
        }

      
        //pr($_GET);    
    }

    function index()
    {
        $this->set('fastlinks', array('ATEP' => '/', 'Klienti' => '#'));

        $this->loadModel('CmsUser');
        $this->set('cms_user_list', $this->CmsUser->find('list', array('conditions' =>
            array('kos' => 0), 'order' => 'name ASC')));

        $this->set('cm_coo_list', $this->CmsUser->find('list', array('conditions' =>
            array('cms_group_id IN (3,4)'), 'fields' => array('name', 'name'), 'order' =>
            'name ASC')));

        $this->loadModel('Company');
        $company_conditions = array('Company.kos' => 0);

        if (isset($this->filtration_company_condition))
            $company_conditions = am($company_conditions, $this->
                filtration_company_condition);

        $this->set('company_list', $this->Company->find('list', array('conditions' => $company_conditions,
            'order' => array('Company.name ASC'))));
        unset($this->Company);


        /**
        * dalsi moznosit filtrace
        * moznsosti zadavame sloupec-stav => nazev moznosti
        */
        $this->set('next_list',array(
            'express-1'=> 'Expresní klienti',
            'import_adresa-NOTNULL'=> 'Neprázdna importní adresa',
            'email-NOTNULL'=> 'Neprázdna emailova adresa',
            'externi_nabor-0'=> 'Interní nábor',
            'externi_nabor-1'=> 'Externí nábor'
        ));

        $this->loadModel('SettingCareerItem');
        $this->set('profese_list', $this->SettingCareerItem->find('list', array('conditions' =>
            array('kos' => 0), 'order' => 'name ASC')));

        // load okresy list
            $this->loadModel('Countrie');
            //$this->set('client_countries_list',null);
            $this->set('countries_list', $this->Countrie->find('list', array(
                'conditions' =>array(
                    'Countrie.status' => 1, 
                    'Countrie.kos' => 0
                )
            )));
            unset($this->Countrie);
    
        /**
         * //změna pro umístění datum na created misto from
         *         foreach ($this->viewVars['items'] as & $item)
         *         {
         *             if ($item['ConnectionClientRequirement']['type'] == 1)
         *                 $item['ConnectionClientRequirement']['from'] = $item['ConnectionClientRequirement']['created'];
         *         }
         */


        if ($this->RequestHandler->isAjax())
        {
            $this->render('../system/items');
        } else
        {
            $this->set('scripts', array('uploader/uploader'));
            $this->set('styles', array('../js/uploader/uploader'));
            $this->render('../system/index');
        }
    }

    function edit($id = null, $domwin = null, $show = null)
    {

        //Configure::write('debug',1);
        $this->autoLayout = false;
        $parent_id = 0;
        
        if (empty($this->data))
        {
         
            /**
             * AT interni zamestnaci
             * - povinny udaj - cislo uctu
             *                - rodne cislo
             */
            if(isset($this->params['at_interni']) && $this->params['at_interni'] == true)
                $this->Clients->set('at_interni',true);
            
            $this->set('admin', $admin_group_id =  $this->logged_user['CmsGroup']['id']);
            $edit_ucet = true;
           
            

            // nacteni form sablon
            $this->loadModel('FormTemplate');
            $this->set('form_template_list', $this->FormTemplate->find('list', array('conditions' =>
                array('kos' => 0), 'order' => 'name')));
            unset($this->FormTemplate);


            // nacteni kvalifikace
            $this->loadModel('SettingCareerItem');
            $this->set('kvalifikace_list', $this->SettingCareerItem->find('list', array('order' =>
                'name')));
            unset($this->SettingCareerItem);

            // nacteni certifikatu
            $this->loadModel('SettingCertificate');
            $this->set('certifikaty_list', $this->SettingCertificate->find('list', array('order' =>
                'name')));
            unset($this->SettingCertificate);

            // nacteni vzdelani
            $this->loadModel('SettingEducation');
            $this->set('dosazene_vzdelani_list', $this->SettingEducation->find('list', array
                ('order' => 'name')));
            unset($this->SettingEducation);

            // load stat list
            $this->loadModel('SettingStat');
            $this->set('client_stat_list', $this->SettingStat->find('list', array('conditions' =>
                array('SettingStat.status' => 1, 'SettingStat.kos' => 0))));
            unset($this->SettingStat);
            
            // load pojistovna lisy
            $this->loadModel('SettingPojistovna');
            $this->set('pojistovna_list', $this->SettingPojistovna->find('list', array('conditions' =>
                array('SettingPojistovna.status' => 1, 'SettingPojistovna.kos' => 0))));
            unset($this->SettingPojistovna);


            $temp_stat_id = 1; //defaultně načti okresy z ČR

            // Natahnuti seznamu zakazek
            $this->set('zakazky_list',$this->get_list('EbCompany'));

            if ($id != null)
            {
                
                /**
                 * povol zobrazeni pro Client only show
                 */
                $this->Client->set_ignore_status(true);
                    
                $this->data = $this->Client->read(null, $id);
                
                
                /**
                 * pokud se jedna o duplicitni kartu, nastav pouze view mod
                 * id zmen za parent_id a nacteme data od parent_id
                 */
                if($this->data['Client']['parent_id'] != ''){
                    $parent_id = $this->data['Client']['parent_id'];
                    $this->data = $this->Client->read(null, $this->data['Client']['parent_id']);
                    $show = true;
                    $id = $this->data['Client']['id'];                
                }

                $temp_stat_id = $this->data['Client']['stat_id'];
                
                /**
                 * pokud je uz zaskrtnute u cisla uctu zkontrolovano
                 * zamezit editaci pro CM a COO, menit mohou pouze ADMIN, Ucetni
                 */
                if($this->data['Client']['cislo_uctu_control'] == 1 && !in_array($admin_group_id,array(1,6,7,56,57)))
                  $edit_ucet = false;
                    
            

                // nacteni modelu kvalifikace
                $this->loadModel('ConnectionClientCareerItem');
                $this->ConnectionClientCareerItem->bindModel(array('belongsTo' => array('SettingCareerItem')));
                // nacteni ulozenych pozic k danemu klientovi
                $this->set('kvalifikace_list_item', $kval = $this->ConnectionClientCareerItem->
                    find('all', array('conditions' => array('client_id' => $id), 'fields' => array('SettingCareerItem.id',
                    'SettingCareerItem.name', 'ConnectionClientCareerItem.name',
                    'ConnectionClientCareerItem.popis', 'ConnectionClientCareerItem.od',
                    'ConnectionClientCareerItem.do', 'ConnectionClientCareerItem.id'), 'recursive' =>
                    1)));
                // nacteni modelu kvalifikace
                $this->loadModel('ConnectionClientCertifikatyItem');
                $this->ConnectionClientCertifikatyItem->bindModel(array('belongsTo' => array('SettingCertificate')));
                // nacteni ulozenych pozic k danemu klientovi
                $this->set('certifikaty_list_item', $cert = $this->
                    ConnectionClientCertifikatyItem->find('all', array('conditions' => array('client_id' =>
                    $id), 'fields' => array('SettingCertificate.id', 'SettingCertificate.name',
                    'ConnectionClientCertifikatyItem.komentar',
                    'ConnectionClientCertifikatyItem.platnost', 'ConnectionClientCertifikatyItem.id'),
                    'recursive' => 1)));

                // nacteni recriteru
                $this->loadModel('ConnectionClientRecruiter');
                $this->ConnectionClientRecruiter->bindModel(array('belongsTo' => array('CmsUser')));
                $this->set('recruiter_list', $this->ConnectionClientRecruiter->find('all', array
                    ('conditions' => array('ConnectionClientRecruiter.client_id' => $id))));


                // nacteni formularu k danemu klientovi
                $this->loadModel('FormData');
                $this->FormData->bindModel(array('belongsTo'=>array('Company')));
                $this->set('client_form_list', $this->FormData->find('all', array(
                    'conditions' =>array(
                        'FormData.client_id' => $id, 
                        'FormData.kos' => 0
                    ),
                    'order'=>'FormData.history ASC'
                )));
                unset($this->FormData);


               
  
                 // nacteni novych aktivit
                $this->loadModel('HistoryItem');
                $this->HistoryItem->bindModel(array(
                    'belongsTo' => array(
                        'CmsUser' => array('fields' =>'name'),
                        'HistoryType' => array('fields' =>'name','foreignKey'=>'action_id')                    
                    ),
                    'hasMany'=>array(
                        'HistoryData' => array('foreignKey'=>'history_item_id')
                    )
                ));
                $this->set('history_item_list', $this->HistoryItem->find('all',array(
                    'conditions'=>array(
                        'HistoryItem.client_id' =>$id
                    ),
                    'order'=>'created DESC'
                )));
                unset($this->HistoryItem);
                
                
                /**
                 * pro historii nastaveni action id ktere pujdu do zalozky Activity - editace
                 * a nebudou se zobrazovat v zalozkach Activity
                 */
                 $this->set('edit_action_ids',array(5,17));
                 
                 
                  /**
                   * pokud jsou nastaveny request data
                   * nastav componentu
                   */
                  if(isset($this->params['request_data']))
                     $this->Clients->set_request_data($this->params['request_data']);
                     
                $this->loadModel('CmsUser');  
                $at_user = $this->CmsUser->findByAtEmployeeId($id);
                if($at_user){     
                    /**
                    * nacteni majetku ktere ma prirazen jako spravce majektu
                    * ty ktere jeste nikomu nepridelil
                    */    
                    $this->loadModel('AtProjectCentre');
                    $centre_list = $this->AtProjectCentre->find('list',array(
                        'conditions'=>array('spravce_majetku_id'=>$at_user['CmsUser']['id']),'fields'=>array('id','id')
                    ));
                    if(!empty($centre_list)){
                        $this->loadModel('AuditEstate');
                        $ae_manager_list = $this->AuditEstate->find('all',array(
                            'conditions'=>array('at_project_centre_id'=>array_keys($centre_list),'kos'=>0)
                        ));
                        $this->set('ae_manager_list',$ae_manager_list);
                    }
                }
               
                /**
                * nacteni majetku ktere ma prirazen klient
                */       
                $this->loadModel('ConnectionAuditEstate');
                $this->ConnectionAuditEstate->bindModel(array('belongsTo'=>array('AuditEstate')));
                $audit_estate_list = $this->ConnectionAuditEstate->find('all',array(
                    'conditions'=>array('ConnectionAuditEstate.kos'=>0,'ConnectionAuditEstate.client_id'=>$id,'AuditEstate.kos'=>0)
                ));
                $this->set('audit_estate_list',$audit_estate_list);
                
                /**
                 * firma ve ktere pracuje
                 */
                 $this->loadModel('ConnectionClientRequirement');
                 $conection = $this->ConnectionClientRequirement->find('first',array(
                    'conditions'=>array('client_id'=>$id,'type'=>2,'to'=>'0000-00-00'),
                    'fields'=>array('company_id')
                 ));
                 if($conection)
                    $this->data['Client']['st_company_id'] = $conection['ConnectionClientRequirement']['company_id'];
                 unset($this->ConnectionClientRequirement);
            }

            // load okresy list
            $this->loadModel('Countrie');
            //$this->set('client_countries_list',null);
            $this->Countrie->bindModel(array('belongsTo' => array('Province' => array('foreignKey' =>
                'province_id'))));
            $this->set('client_countries_list', $cc = $this->Countrie->find('list', array('conditions' =>
                array('Countrie.status' => 1, 'Countrie.kos' => 0, 'Province.stat_id' => $temp_stat_id),
                'recursive' => 1)));
            unset($this->Countrie);
            
            //seznam informaci o praci
            $this->set('info_about_job_list',$this->get_list('InfoAboutJob'));
                
            $this->set('leasing_company_list',$this->get_list('Company',array('status'=>1)));
            
            /**
             * nastaveni promene ktera povoluje editaci uctu
             */
            $this->set('edit_ucet', $edit_ucet);
            
            $this->set('only_show', $show);
            $this->set('parent_id', $parent_id);
            
            /**
             * nove nastaveni prommenych pro view z komponenty client
             */
            $this->Clients->set_variables_for_view();
            
            if(isset($this->params['at_interni']) && $this->params['at_interni'] == true)
                $this->data['old_mail'] = $this->data['Client']['email'];

            if ($id != null){
                // natahnuti existujicich zakazek
                $this->loadModel('ConnectionEbcomClient');
                $this->ConnectionEbcomClient->bindModel(array(
                    'belongsTo' => array(
                        'EbCompany',
                        'CmsUser'=>array(
                            'foreignKey' => 'start_who'
                        )
                    )
                ));
                $zaks = $this->ConnectionEbcomClient->find(
                    'all',
                    array(
                        'conditions' => array(
                            'ConnectionEbcomClient.client_id' => $id,
                            'ConnectionEbcomClient.stav' => 2,
                            'ConnectionEbcomClient.end' => '0000-00-00'
                        ),
                        'fields' => array(
                            'EbCompany.name',
                            'ConnectionEbcomClient.*',
                            'CmsUser.name'
                        )
                    )
                );

                $this->set('zak_list',$zaks);
            }

            $this->render('edit');
        } 
        else //SAVE
        {
            
            if (!$this->Client->save($this->data)){
                die('Neulozeno');
            }

            if (!in_array($this->logged_user['CmsGroup']['id'], array(8)))
                $save_cms_user_id = -1;
            else
                $save_cms_user_id = $this->logged_user['CmsUser']['id'];

            if (empty($this->data['Client']['id']) || $this->data['NewConnection'] == 1)
            {
                $this->loadModel('ConnectionClientRecruiter');
                $save_data = array('ConnectionClientRecruiter' => array('cms_user_id' => $save_cms_user_id,
                    'client_id' => $this->Client->id));
                $this->ConnectionClientRecruiter->save($save_data);

                // pokud se uklada -1, ulozeni pro Coo,Cm atd i kdo ho ulozil
                if ($save_cms_user_id == -1)
                {
                    $this->ConnectionClientRecruiter->id = null;
                    $save_data = array('ConnectionClientRecruiter' => array('cms_user_id' => $this->
                        logged_user['CmsUser']['id'], 'client_id' => $this->Client->id));
                    $this->ConnectionClientRecruiter->save($save_data);
                }
            }


            $this->loadModel('ConnectionClientCareerItem');

            // smazani starych propojeni
            $this->ConnectionClientCareerItem->deleteAll(array('client_id' => $this->Client->
                id));
            if (isset($this->data['ClientCarrerItems']))
            {
                foreach ($this->data['ClientCarrerItems'] as $career_name)
                {
                    $this->ConnectionClientCareerItem->save(array('ConnectionClientCareerItem' =>
                        array('name' => $career_name['name'], 'popis' => $career_name['popis'], 'od' =>
                        $career_name['od'], 'do' => $career_name['do'], 'client_id' => $this->Client->id,
                         'setting_career_item_id' => $career_name['setting_career_item_id'], )));
                    $this->ConnectionClientCareerItem->id = null;
                }
            }
            $this->loadModel('ConnectionClientCertifikatyItem');

            // smazani starych propojeni
            $this->ConnectionClientCertifikatyItem->deleteAll(array('client_id' => $this->Client->id));


            if (isset($this->data['CertifikatyItem']))
            {
                foreach ($this->data['CertifikatyItem'] as $cert_name)
                {
                    $this->ConnectionClientCertifikatyItem->save(array('ConnectionClientCertifikatyItem' =>
                        array('komentar' => $cert_name['komentar'], 'platnost' => $cert_name['platnost'],
                        'client_id' => $this->Client->id, 'setting_certificate_id' => $cert_name['list'], )));

                    //$papa[] = $cert_name['list'];
                    $this->ConnectionClientCertifikatyItem->id = null;
                    $this->ConnectionClientCertifikatyItem->data = null;
                }
            }
            //$papa[] = $this->Client->id;
            //die(json_encode(array('result'=>false,'message'=>join(';',$papa))));

            /**
             * nastala změna emailu, je nutné změnit login mail pokud má account
             */
            if($this->data['old_mail'] != $this->data['Client']['email']){
                $acc = $this->IntEmployee->client_have_cms_account($this->data['Client']['id']);
                
                if($acc){    
                    $this->loadModel('CmsUser');
                    $this->CmsUser->id = $acc['CmsUser']['id'];  
                    $this->CmsUser->save(array('email'=>$this->data['Client']['email']));
                }    
            }

            die();
        }
    }

    function remove_worker($id = null){
        if (empty($this->data)){
            $this->set('id',$id);
            $this->render('domwin/remove');
        } else {
            $this->loadModel('ConnectionEbcomClient');
            $this->data['ConnectionEbcomClient']['end_when'] = date('Y-m-d H:i:s');
            $this->data['ConnectionEbcomClient']['end_who'] = $this->logged_user['CmsUser']['id'];

            if($this->ConnectionEbcomClient->save($this->data)){
                die(json_encode(array('result'=>true)));
            } else {
                die(json_encode(array('result'=>false)));
            }
        }
    }

    function add_worker($client_id = null ){
        if (empty($this->data)){
            $this->set('eb_company_list',$this->get_list('EbCompany'));
            $this->set('client_id',$client_id);
            $this->render('domwin/add');
        } else {
            $this->loadModel('ConnectionEbcomClient');
            $this->data['ConnectionEbcomClient']['start_when'] = date('Y-m-d H:i:s');
            $this->data['ConnectionEbcomClient']['start_who'] = $this->logged_user['CmsUser']['id'];
            $this->data['ConnectionEbcomClient']['stav'] = 2;

            if($this->ConnectionEbcomClient->save($this->data)){
                die(json_encode(array('result'=>true)));
            } else {
                die(json_encode(array('result'=>false)));
            }
        }
    }

    /**
     * vytvoreni aktivity ke klientovi a jeji zaznamenani do aktivit
     * @param $from -> 1 - Cekaci listina(posilame pak mail)
     * @author Sol
     * @created 23.11.09
     */
    function add_activity($client_id = null,$from = false,$from_ccr_id = false){
        Configure::write('debug',1);
        $this->autoLayout = false;
        if (empty($this->data))
        {
            $cl = $this->Client->read(array('name','hodnoceni'),$client_id);
            $this->set('client_id',$client_id);
            $this->set('from_type',$from);
            $this->set('hodnoceni',$cl['Client']['hodnoceni']);
            $this->set('cl_name',$cl['Client']['name']);
            $this->set('from_ccr_id',$from_ccr_id);
            
            $this->loadModel('ConnectionClientRequirement');
            $ccr_list = array();
            $this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Company','CompanyOrderItem'=>array('foreignKey'=>'requirements_for_recruitment_id'))));
            $_ccr_list = $this->ConnectionClientRequirement->find('all',array(
                'conditions'=>array(
                    'client_id'=>$client_id,
                    'type'=>1,
                    'add_to_cl_datetime IS NOT NULL',
                    'cl_notification'=>array(0,1,2,3)
                )
            ));
            if($_ccr_list)
                foreach($_ccr_list as $item){
                    $ccr_list[$item['ConnectionClientRequirement']['id']] = $item['Company']['name'].' - '.$item['CompanyOrderItem']['name'];
                }
            $this->set('ccr_list',$ccr_list);
            
            if(!isset($_POST['from']) || (isset($_POST['from']) && $_POST['from'] != "objednavky"))
                $this->set('update','history_items2');
            
            $this->render('activity/add_activity');
        }
        else {
            /**
             * Ulozeni problemu
             */
            if($this->data['kind'] == 5){
                if($this->data['hodnoceni'] != 0){
                   echo $this->requestAction('/misconducts/save/',array('request_data'=>$this->data));
                }
            }
            else if($this->data['kind'] == 6){//blacklist history a ulozeni hodnoceni
                echo $this->requestAction('/black_list_clients/add_to_blacklist/'.$this->data['client_id'].'/',array('request_data'=>$this->data));
            }
            else if($this->data['kind'] == 7 && $this->data['ccr_id'] != ''){
                 $this->loadModel('ConnectionClientRequirement');
                 //$this->ConnectionClientRequirement->id = $this->data['ccr_id'];
                 $nl_stav = $this->ConnectionClientRequirement->read('nl_notification',$this->data['ccr_id']);
                 if(in_array($nl_stav['ConnectionClientRequirement']['nl_notification'],array(0,1))){
                    $this->ConnectionClientRequirement->save(array('nl_notification'=>2,'first_notification_datetime'=>date('Y-m-d H:i:s')));
                 }
                 else if(in_array($nl_stav['ConnectionClientRequirement']['nl_notification'],array(2,3)))
                    $this->ConnectionClientRequirement->save(array('nl_notification'=>4));
            } 
            
            if(isset($this->data['from_type']) && $this->data['from_type'] == 1){
                $this->loadModel('ConnectionClientRequirement');
                $this->ConnectionClientRequirement->bindModel(array('belongsTo'=>array('Company')));
                $ccr_detail = $this->ConnectionClientRequirement->read(array('Company.*','ConnectionClientRequirement.add_to_cl_cms_user_id','ConnectionClientRequirement.add_to_cl_datetime'),$this->data['from_ccr_id']);
                $recipient_list = array();
                if($ccr_detail['ConnectionClientRequirement']['add_to_cl_cms_user_id'] != ''){
                    $this->loadModel('CmsUser');
                    $user = $this->CmsUser->read(array('name','email'),$ccr_detail['ConnectionClientRequirement']['add_to_cl_cms_user_id']);
                    $recipient_list[] = $user['CmsUser']['email'];
                } 
                
                $replace_list = array(
                    '##Company.name##' 	=>$ccr_detail['Company']['name'],
					'##Client.name##' 	=>$this->data['cl_name'],
                    '##kind##'=>$this->kind_of_client_activity[$this->data['kind']],
                    '##zprava##'=>$this->data['text'],
                    '##prirazen_na_cl##' 	=>self::mail_date($ccr_detail['ConnectionClientRequirement']['add_to_cl_datetime']), 
                    '##priradil_na_cl##' 	=>$user['CmsUser']['name'],
                    '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
				);
                $this->Email->set_company_data($ccr_detail['Company']);
				$this->Email->send_from_template_new(43,$recipient_list,$replace_list);
            }
            
            //slepe ulozeni
            //uloznei zpracovaa history compnenta
            $this->load_client_activity($this->data['client_id']);
        }
    } 
    
    
    /**
     * Funkce pro pridani activity, nacita pouze zalozku z karty klienta 
     * Aktivity - editace a umoznuje pridani activity
     */
    function domwin_add_activity($client_id = null){
        //$this->autoLayout = false;
        if ($client_id != null)
        {
            $this->set('client_id',$client_id);
            $this->load_client_activity($client_id,null,null,true);
            $this->render('activity/index3');
        }
    } 
    
     /**
     * slepa funkce pro zaznamenani prideleni pomucek
     * @author Sol
     * @created 7.12.09
     * vypnuto 11.12.09
    
    function add_working_items(){
        $this->autoLayout = false;
        $this->load_client_activity($this->data['client_id']);
    } 
     */  
    
    /**
     * @author Sol
     * @created 20.11.09
     * nacteni jednotlivych akci z nove historie/activit klienta a jeho filtrovani podle $category_id
     */
    function load_client_activity($client_id,$category_id = null,$item = 'items',$only_data = false){
         // nacteni novych aktivit
         $this->loadModel('HistoryItem');
         $this->HistoryItem->bindModel(array(
              'belongsTo' => array(
                    'CmsUser' => array('fields' =>'name'),
                    'HistoryType' => array('fields' =>'name','foreignKey'=>'action_id')
              ),
              'hasMany'=>array(
                  'HistoryData' => array('foreignKey'=>'history_item_id')
              )  
         ));
         $this->set('history_item_list', $this->HistoryItem->find('all',array(
             'conditions'=>array(
                'HistoryItem.client_id' =>$client_id,
                ($category_id != '' ? 'HistoryItem.category_id = '.$category_id :'')
             ),
             'order'=>'created DESC'
         )));
         unset($this->HistoryItem);
         
            /**
             * pro historii nastaveni action id ktere pujdu do zalozky Activity - editace
             * a nebudou se zobrazovat v zalozkach Activity
             */
             $this->set('edit_action_ids',array(5,17));
         
         if($only_data == false){
             echo $this->render('activity/'.$item);
             die();
         }
    }
    
    
    /**
     * @author Sol
     * @created 20.11.09
     * zobrazeni detailu jednotlive polozky z historie klienta
     */
    function history_detail($history_item_id){
        
        $this->loadModel('HistoryItem');
        if ($history_item_id != null)
        {
            /**
             * nacteni dat prvku ktery chceme zobrazit
             */
            $this->HistoryItem->bindModel(array(
               // 'belongsTo' => array('CmsUser' => array('fields' =>'name'))
               'hasMany'=>array('HistoryData')
            ));
            $this->set('detail', $data = $this->HistoryItem->read(null,$history_item_id));
            
             /**
              * nacteni posledniho prvku teto akce k tomuto klientovi
              * pro porovnani a zobrazeni rozdilu, tedy zobrazeni zmenenych polozek
              * 
              * pokud najdeme alespoň jedny data k danému id
              */
              if(isset($data['HistoryData']) && count($data['HistoryData']) > 0){
                  $this->HistoryItem->bindModel(array(
                      'hasMany'=>array('HistoryData')
                  ));
                  $this->set('last_item', $this->HistoryItem->find(
                    'first',
                        array(
                        'conditions'=>array(
                            'HistoryItem.action_id' =>$data['HistoryItem']['action_id'],
        					'HistoryItem.category_id' => $data['HistoryItem']['category_id'],
        					'HistoryItem.client_id' => $data['HistoryItem']['client_id'],
                            'HistoryItem.id < ' => $history_item_id
                        ),
                        'order' => 'HistoryItem.id DESC'
                   )));
               }

             
             
            //render 
            $this->render('activity/detail');
        }
        unset($this->HistoryData);
   
    }
    
    //expresni pridani klienta
    function edit_express($id = null, $domwin = null, $show = null)
    {
        $this->autoLayout = false;
        if (empty($this->data))
        {
            // nacteni kvalifikace
            $this->loadModel('SettingCareerItem');
            $this->set('kvalifikace_list', $this->SettingCareerItem->find('list', array('order' =>
                'name')));
            unset($this->SettingCareerItem);
            
                   // nacteni vzdelani
            $this->loadModel('SettingEducation');
            $this->set('dosazene_vzdelani_list', $this->SettingEducation->find('list', array
                ('order' => 'name')));
            unset($this->SettingEducation);

            // load stat list
            $this->loadModel('SettingStat');
            $this->set('client_stat_list', $this->SettingStat->find('list', array('conditions' =>
                array('SettingStat.status' => 1, 'SettingStat.kos' => 0))));
            unset($this->SettingStat);

            $this->render('edit_express');
        }
        else{ 
            $this->data['Client']['express'] = 1;
            $this->Client->save($this->data);
            
            //ulozeni recruiteru
            if (!in_array($this->logged_user['CmsGroup']['id'], array(8)))
                $save_cms_user_id = -1;
            else
                $save_cms_user_id = $this->logged_user['CmsUser']['id'];

                $this->loadModel('ConnectionClientRecruiter');
                $save_data = array('ConnectionClientRecruiter' => array(
                    'cms_user_id' => $save_cms_user_id,
                    'client_id' => $this->Client->id
                ));
                $this->ConnectionClientRecruiter->save($save_data);

                // pokud se uklada -1, ulozeni pro Coo,Cm atd i kdo ho ulozil
                if ($save_cms_user_id == -1)
                {
                    $this->ConnectionClientRecruiter->id = null;
                    $save_data = array('ConnectionClientRecruiter' => array(
                        'cms_user_id' => $this->logged_user['CmsUser']['id'], 
                        'client_id' => $this->Client->id
                    ));
                    $this->ConnectionClientRecruiter->save($save_data);
                }
            
            
            //ulozeni profesi
            $this->loadModel('ConnectionClientCareerItem');
            if (isset($this->data['ClientCarrerItems']))
            {
                foreach ($this->data['ClientCarrerItems'] as $career_name)
                {
                    $this->ConnectionClientCareerItem->save(array('ConnectionClientCareerItem' =>
                        array('name' => $career_name['name'], 'popis' => $career_name['popis'], 'od' =>
                        $career_name['od'], 'do' => $career_name['do'], 'client_id' => $this->Client->
                        id, 'setting_career_item_id' => $career_name['setting_career_item_id'], )));
                    $this->ConnectionClientCareerItem->id = null;
                }
            }
        }
    }
    /**
     * recruiter add
     *
     * @param $client_id
     * @param $client_id
     * @return view
     * @access public
     **/
    function recruiter_add($client_id = null)
    {

        $this->autoLayout = false;
        //$client = $this->Client->find(array('Client.mobil'=>$client_mobil,'Client.kos'=>0));
        if ($client_id != null)
        {
            $this->loadModel('CmsUser');
            if (empty($this->data))
            {
                $this->set('client_id', $client_id);
                //nacteni  recruiteru
                $this->loadModel('ConnectionClientRecruiter');
                $neco = $this->ConnectionClientRecruiter->find('list', array('conditions' =>
                    array('ConnectionClientRecruiter.client_id' => $client_id), 'fields' => array('id',
                    'cms_user_id')));
                if ($neco) //$this->set('cms_user_list', $this->CmsUser->find('list', array('conditions'=>array('CmsUser.kos'=>0,'CmsUser.cms_group_id'=>8,'CmsUser.id NOT IN ('.implode(',',$neco).')'))));

                    $this->set('cms_user_list', $this->CmsUser->find('list', array('conditions' =>
                        array('CmsUser.kos' => 0, 'CmsUser.id NOT IN (' . implode(',', $neco) . ')'),
                        'order' => 'name ASC')));
                else //$this->set('cms_user_list', $this->CmsUser->find('list', array('conditions'=>array('CmsUser.kos'=>0,'CmsUser.cms_group_id'=>8))));

                    $this->set('cms_user_list', $this->CmsUser->find('list', array('conditions' =>
                        array('CmsUser.kos' => 0), 'order' => 'name ASC')));
            } else
            {
                $this->data["ConnectionClientRecruiter"]["client_id"] = $client_id;
                $this->loadModel('ConnectionClientRecruiter');
                $this->ConnectionClientRecruiter->save($this->data);

                die(json_encode($this->CmsUser->read(array('name'), $this->data["ConnectionClientRecruiter"]["cms_user_id"])));
            }
        }
    }

    /**
     * rating add
     *
     * @param $client_id
     * @return view
     * @access public
     **/
    function rating_add($client_id = null)
    {

        $this->autoLayout = false;
        //$client = $this->Client->find(array('Client.mobil'=>$client_mobil,'Client.kos'=>0));
        if ($client_id != null)
        {
            $this->loadModel('ClientRating');
            if (empty($this->data))
            {
                $this->set('client_id', $client_id);
                $this->set('logged_user_name', $this->logged_user["CmsUser"]["name"]);
                $this->render('hodnoceni');
            } else
            {
                $this->data["ClientRating"]["client_id"] = $client_id;
                $this->data["ClientRating"]["cms_user_id"] = $this->logged_user["CmsUser"]["id"];
                $this->ClientRating->save($this->data);

                die();
            }
        }
    }

    /**
     * Valid Clinet To Requiter
     *
     * @param $client_mobil
     * @return view
     * @access public
     **/
    function valid_requiter($client_mobil, $born = null)
    {
        $this->autoLayout = false;

        $conditions = array('Client.mobil' => $client_mobil, 'Client.kos' => 0);

        if ($born != null)
        {
            $conditions['datum_narozeni'] = $born;
        }

        $client = $this->Client->find($conditions);
        if ($client)
        {
            $this->loadModel('ConnectionClientRecruiter');

            $conditions = array('ConnectionClientRecruiter.client_id' => $client['Client']['id'],
                'ConnectionClientRecruiter.cms_user_id' => $this->logged_user['CmsUser']['id']);

            $client_own = $this->ConnectionClientRecruiter->find($conditions);
            if (!$client_own)
            {
                // nacteni modelu kvalifikace
                $this->loadModel('ConnectionClientCareerItem');
                $this->ConnectionClientCareerItem->bindModel(array('belongsTo' => array('SettingCareerItem')));
                // nacteni ulozenych pozic k danemu klientovi
                $kval = $this->ConnectionClientCareerItem->find('all', array('conditions' =>
                    array('client_id' => $client['Client']['id']), 'fields' => array('SettingCareerItem.id',
                    'SettingCareerItem.name', 'ConnectionClientCareerItem.name',
                    'ConnectionClientCareerItem.popis', 'ConnectionClientCareerItem.od',
                    'ConnectionClientCareerItem.do', 'ConnectionClientCareerItem.id'), 'recursive' =>
                    1));
                die(json_encode(array('result' => 1, 'client' => $client, 'kval' => $kval)));
            } else
            {
                die(json_encode(array('result' => -1, 'client' => $client)));
            }
        } else
            die(json_encode(array('result' => 0)));
    }


    /**
     * Valid Clinet To Requiter Edit
     *
     * @param $client_mobil
     * @return view
     * @access public
     **/
    function valid_requiter_edit($client_mobil)
    {
        $this->autoLayout = false;
        $client = $this->Client->find(array('Client.mobil' => $client_mobil,
            'Client.kos' => 0));
        if ($client)
        {
            die(json_encode(array('result' => 1, )));

        } else
        {
            die(json_encode(array('result' => 0, )));
        }
    }

    /**
     * Check Client Number
     * @author Jakub Matuš
     * @param $client_mobil
     * @return array client
     * @access public
     **/
    function check_number($client_mobil)
    {
        $this->autoLayout = false;
        $client = $this->Client->find('list', array('conditions' => array('OR' => array
            ('Client.mobil1' => $client_mobil, 'Client.mobil2' => $client_mobil,
            'Client.mobil3' => $client_mobil), 'Client.kos' => 0)));
        if ($client)
        {
            die(json_encode(array('result' => true, 'data' => $client)));

        } else
        {
            die(json_encode(array('result' => false)));
        }
    }

    /**
     * Check Client Surname
     * @author Jakub Matuš
     * @param $surname
     * @return array client
     * @access public
     **/
    function check_surname($surname)
    {
        $this->autoLayout = false;
        $client = $this->Client->find('superlist', array(
            'conditions' => array(
                'Client.prijmeni' => $surname,
                'Client.kos' => 0
            ),
            'fields'=>array('id','name','datum_narozeni'),
            'separator'=> ' - '
        ));
        
        if ($client)
            die(json_encode(array('result' => true, 'data' => $client)));
        else
            die(json_encode(array('result' => false)));
    }

    /**
     * Seznam priloh
     *
     * @param $client_id
     * @return view
     * @access public
     **/
    function attachs($client_id)
    {
        $this->autoLayout = false;
        $this->loadModel('ClientAttachment');

        if ($this->logged_user["CmsGroup"]["permission"]["clients"]["attach"] == 2)
            $pristup = "ClientAttachment.cms_user_id=" . $this->logged_user['CmsUser']['id'];
        else
            $pristup = "";

        $this->ClientAttachment->bindModel(array('belongsTo' => array('SettingAttachmentType')));
        $this->set('attachment_list', $this->ClientAttachment->findAll(array('ClientAttachment.client_id' =>
            $client_id, 'ClientAttachment.kos' => 0, $pristup)));
        $this->set('client_id', $client_id);
        unset($this->ClientAttachment);
        $this->render('attachs/index');
    }

    /**
     * Editace priloh
     *
     * @param $company_id
     * @param $id
     * @return view
     * @access public
     **/
    function attachs_edit($client_id = null, $id = null)
    {
        $this->autoLayout = false;
        $this->loadModel('ClientAttachment');
        if (empty($this->data))
        {
            $this->loadModel('SettingAttachmentType');
            $this->SettingAttachmentType = &new SettingAttachmentType();
            $this->set('setting_attachment_type_list', $this->SettingAttachmentType->find('list',
                array('conditions' => array('kos' => 0), 'order' => 'poradi ASC')));
            unset($this->SettingAttachmentType);
            $this->data['ClientAttachment']['client_id'] = $client_id;
            if ($id != null)
            {
                $this->data = $this->ClientAttachment->read(null, $id);
            }
            $this->render('attachs/edit');
        } else
        {
            $this->data["ClientAttachment"]["cms_user_id"] = $this->logged_user['CmsUser']['id'];
            $this->ClientAttachment->save($this->data);
            $this->attachs($this->data['ClientAttachment']['client_id']);
        }
        unset($this->ClientAttachment);
    }
    /**
     * Presun prilohy do kose
     *
     * @param $company_id
     * @param $id
     * @return view
     * @access public
     **/
    function attachs_trash($client_id, $id)
    {
        $this->loadModel('ClientAttachment');
        $this->ClientAttachment->save(array('ClientAttachment' => array('kos' => 1, 'id' =>
            $id)));
        $this->attachs($client_id);
        unset($this->ClientAttachment);
    }

    /**
     * Nahrani prilohy na ftp
     *
     * @return view
     * @access public
     **/
    function upload_attach()
    {
        $this->Upload->set('data_upload', $_FILES['upload_file']);
        if ($this->Upload->doit(json_decode($this->data['upload']['setting'], true)))
        {
            echo json_encode(array('upload_file' => array('name' => $this->Upload->get('outputFilename')),
                'return' => true));
        } else
            echo json_encode(array('return' => false, 'message' => $this->Upload->get('error_message')));
        die();
    }

    /**
     * Stazeni prilohy
     *
     * @param $file
     * @param $file_name
     * @return download file
     * @access public
     **/
    function attachs_download($file, $file_name)
    {
        $pripona = strtolower(end(Explode(".", $file)));
        $file = strtr($file, array("|" => "/"));
        $filesize = filesize('./uploaded/' . $file);
        $cesta = "http://" . $_SERVER['SERVER_NAME'] . "/uploaded/" . $file;

        header("Pragma: public"); // požadováno
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false); // požadováno u některých prohlížečů
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . $filesize);
        Header('Content-Type: application/octet-stream');
        Header('Content-Disposition: attachment; filename="' . $file_name . '.' . $pripona .
            '"');
        readfile($cesta);
        die();

    }


    /**
     * zpravy
     *
     * @param $client_id
     * @return view
     * @access public
     **/
    function messages($client_id, $render = 'messages/index')
    {
        $this->autoLayout = false;
        $this->loadModel('ClientMessage');
        $this->ClientMessage->bindModel(array('belongsTo' => array('CmsUser' => array('fields' =>
            'name'))));
        $this->set('message_list', $t = $this->ClientMessage->findAll(array('ClientMessage.client_id' =>
            $client_id, 'ClientMessage.kos' => 0)));
        $this->set('client_id', $client_id);
        unset($this->ClientMessage);
        $this->render($render);
    }

    /**
     * Editace zprav
     *
     * @param $company_id
     * @param $id
     * @return view
     * @access public
     * 	
     **/
    function messages_edit($client_id = null, $id = null)
    {
        $this->autoLayout = false;
        $this->loadModel('ClientMessage');
        if (empty($this->data))
        {
            $this->data['ClientMessage']['client_id'] = $client_id;

            if ($id != null)
            {
                $this->data = $this->ClientMessage->read(null, $id);
            }
            //	if ($domwin != null) $this->set('refresh_in_edit',true);
            $this->render('messages/edit');
        } else
        {

            $this->data['ClientMessage']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            $this->ClientMessage->save($this->data);
            $this->messages($this->data['ClientMessage']['client_id'], $_POST['render']);
        }
        unset($this->ClientMessage);
    }

    function messages_detail($message_id)
    {
        $this->loadModel('ClientMessage');
        if ($message_id != null)
        {
            $this->ClientMessage->bindModel(array('belongsTo' => array('CmsUser' => array('fields' =>
                'name'))));
            $this->set('detail', $this->ClientMessage->read(null, $message_id));
            $this->render('messages/detail');
        }
        unset($this->ClientMessage);
    }


    /**
     * Presun prilohy do kose
     *
     * @param $company_id
     * @param $id
     * @return view
     * @access public
     **/
    function messages_trash($client_id, $id)
    {
        $this->loadModel('ClientMessage');
        $this->ClientMessage->save(array('ClientMessage' => array('kos' => 1, 'id' => $id)));
        $this->messages($client_id);
        unset($this->ClientMessage);
    }

    /**
     * add message
     *
     * @param $client_id
     * @param $name
     * @param $text
     * @param $cms_user_id 	
     * @param $datum	
     * @return save date
     * @access public
     * 	
     **/
    function add_messages_edit($client_id, $name, $text, $cms_user_id, $datum = null)
    {
        $this->autoLayout = false;

        if (($client_id != null) && ($name != null) && ($text != null) && ($cms_user_id != null))
        {
            $this->loadModel('ClientMessage');
            if ($datum != null)
                $message_save['ClientMessage']['datum'] = $datum;
            $message_save['ClientMessage']['client_id'] = $client_id;
            $message_save['ClientMessage']['cms_user_id'] = $cms_user_id;
            $message_save['ClientMessage']['name'] = str_replace("_", " ", $name);
            $message_save['ClientMessage']['text'] = str_replace("_", " ", $text);
            $this->ClientMessage->save($message_save);
        }
        unset($this->ClientMessage);
        return;
    }

    function rozvazat_prac_pomer($client_id,$connection_id = null)
    {
        if(!isset($connection_id) || $connection_id == null)
            die('Špatně nastavené parametry spojení - connection_id!');
        
        if (empty($this->data))
        {
            
            // nacteni formularu k danemu klientovi
            $this->loadModel('SettingReasonAtSacking');
            $this->set('duvod_propusteni_list', $this->SettingReasonAtSacking->find('list', array(
                'conditions' => array(
                   'SettingReasonAtSacking.kos' => 0
                )
            )));
            unset($this->SettingReasonAtSacking);
            
            
            /**
             * Nacteni OPP k danemu connection a klientovi
             */
            $this->loadModel('ConnectionClientOpp');
            $this->set('opp_list',$this->ConnectionClientOpp->find('all', array(
                'conditions' => array(
                   'ConnectionClientOpp.kos' => 0,
                   'ConnectionClientOpp.client_id' => $client_id,
                   'ConnectionClientOpp.connection_client_requirement_id' => $connection_id,
                )
            )));
            unset($this->ConnectionClientOpp);
            
            $this->set('client_id', $client_id);
            $this->set('connection_id', $connection_id);
            $this->render('rozvazat_prac_pomer');
        } else
        {
             
            /**
             * Postupne prochazime vsechny OPP a jejich nastaveni
             * 
             */  
            $srazka_ze_mzdy = array('sum'=>0); 
            if(isset($this->data['Opp'])){  
                
                $datum_ukonceni = explode('-',$this->data['to']);
                $this->loadModel('ClientWorkingHour');
                $con = $this->ClientWorkingHour->find('first',array(
                          'conditions'=>array(
                                'client_id'=>$client_id,
                                'connection_client_requirement_id'=>$connection_id,
                                'year'=>$datum_ukonceni[0],
                                'month'=>$datum_ukonceni[1],
                                'kos'=>0
                          )
                ));
                          
                if($con){ //POKUD EXISTUJE DOCHAZKA
                    $this->loadModel('ConnectionClientOpp');
                    foreach($this->data['Opp'] as $connection_opp_id => $opp_item){
                        /**
                         * Srazka ze mzdy
                         */
                        if($opp_item['var'] == 1){
                            /**
                             * Pokud je opp v naroku a dava se srazka ze mzdy
                             * znamena to ze uchazec si OPP nechava a dela se dodatecna srazka z posledni stavajici dochazky.
                             * V pripade naroku = NE je to pouze inforamtivni, protoze v dochazce a mzde jiz jsou strzeny automaticky!
                             */
                            if($opp_item['narok'] == 'ano'){
                                $srazka_ze_mzdy['sum'] += $opp_item['price']; 
                                $this->ConnectionClientOpp->id = $connection_opp_id;
                                $this->ConnectionClientOpp->saveField('srazka_ze_mzdy',1);
                                $this->ConnectionClientOpp->saveField('cms_user_id2',$this->logged_user['CmsUser']['id']);                       
                            }
                        }   
                        /**
                         * Vraceni OPP
                         */
                        else if($opp_item['var'] == 2){
                            /**
                             * Pokud je narok ano, tak se polozka prirazuje pod sklad vracenych OPP
                             * Varianta s narokem ne nelze!
                             */
                            if($opp_item['narok'] == 'ano'){
                                  $this->ConnectionClientOpp->id = $connection_opp_id;
                                  $this->ConnectionClientOpp->saveField('vraceno',1); 
                                  $this->ConnectionClientOpp->saveField('cms_user_id2',$this->logged_user['CmsUser']['id']);                 
                            }
                        }
                     }   
                }
                else{
                    die(json_encode(array('result' => false, 'message' => 'Není vygenerovaná žádná docházka pro rok '.$datum_ukonceni[0].' a měsíc '.$datum_ukonceni[1].'. Tudíž nelze vytvořit srážku ze mzdy pro poslední docházku!')));
                }                
                
                
                
                /**
                 * V pripade ze je potreba odecist nejake srazky z posledni mzdy
                 */
                 if(!empty($srazka_ze_mzdy) && $srazka_ze_mzdy['sum'] > 0){
                         $this->ClientWorkingHour->id = $con['ClientWorkingHour']['id'];
                         $this->ClientWorkingHour->saveField('drawback_opp',$con['ClientWorkingHour']['drawback_opp'] + $srazka_ze_mzdy['sum']);
                         $this->ClientWorkingHour->saveField('salary_per_hour_p',$con['ClientWorkingHour']['salary_per_hour_p'] - $srazka_ze_mzdy['sum']);
                 }
            }  
           
                
            $this->loadModel('ConnectionClientRequirement');
            $conection = $this->ConnectionClientRequirement->find('first', array('conditions' =>
                array('client_id' => $client_id, 'type' => 2, 'to' => '0000-00-00')));
            if (!$conection)
                die(json_encode(array('result' => false, 'message' =>
                    'Chyba aplikace, nenalezeno spojeni mezi klientem a pracovní pozici')));

            if ($this->data['to'] < $conection['ConnectionClientRequirement']['from'])
                die(json_encode(array('result' => false, 'message' =>
                    'Vámi zadané datum propuštění je menší než dautm nástupu!!! Datum nástumu je ' .
                    $conection['ConnectionClientRequirement']['from'])));


            //company
            $this->loadModel('Company');
            $comp = $this->Company->read($this->Company->company_data_for_email_notification, $conection['ConnectionClientRequirement']['company_id']);
            unset($this->Company);


            //ulozeni data
            $this->ConnectionClientRequirement->id = $conection['ConnectionClientRequirement']['id'];
            $this->ConnectionClientRequirement->saveField('to', $this->data["to"]);
            $this->ConnectionClientRequirement->saveField('reason_at_sacking', $this->data["reason_at_sacking"]);

       
            //
            $this->loadModel('CompanyWorkPosition');
            $cwp = $this->CompanyWorkPosition->read('name', $conection['ConnectionClientRequirement']['company_work_position_id']);
            unset($this->CompanyWorkPosition);
         

            $this->loadModel('SettingReasonAtSacking');
            $sras = $this->SettingReasonAtSacking->read('name', $this->data['reason_at_sacking']);
            unset($this->SettingReasonAtSacking);

            //client
            $cl = $this->Client->read(null, $client_id);

            $replace_list = array(
                '##Company.name##' => $comp['Company']['name'],
                '##CompanyWorkPosition.name##' => $cwp["CompanyWorkPosition"]["name"],
                '##Client.name##' => $cl["Client"]["name"], 
                '##Client.mobil##' => (isset($cl['Client'][$cl['Client']['mobil_active']])?$cl['Client'][$cl['Client']['mobil_active']]:''), 
                '##CmsUser.name##' => $this->logged_user["CmsUser"]["name"], 
                '##ReasonAtSacking##' => $sras["SettingReasonAtSacking"]['name'], 
                '##Kvalita.prace##' => $this->hodnoceni_list2[$this->data['ClientRating']["kvalita_prace"]],
                '##Spolehlivost##' => $this->hodnoceni_list2[$this->data['ClientRating']["spolehlivost"]],
                '##Alkohol##' => $this->hodnoceni_list3[$this->data['ClientRating']["alkohol"]],
                '##Blacklist##' => $this->doporuceni_pro_nabor[$this->data['ClientRating']["blacklist"]],
                '##Zamestnan##' => $this->ano_ne_list[$this->data['ClientRating']["zamestnan"]],
                '##Message.text##' => $this->data['ClientRating']["text"]
            );
            $this->Email->set_company_data($comp['Company']);
            $this->Email->send_from_template_new(8, array(), $replace_list);

            // ulozeni stavu clienta
            $this->Client->saveField('stav', 0);
            
            if($cl['Client']['parent_id'] != 0)
                $this->Client->saveField('kos', 1);    
            
            unset($this->Client);
            
            //ulozeni hodnoceni
            $this->loadModel('ClientRating');
            $this->data['ClientRating']['client_id'] = $client_id;
            $this->data['ClientRating']['cms_user_id'] = $this->logged_user["CmsUser"]["id"];
            $this->ClientRating->save($this->data['ClientRating']);
            unset($this->ClientRating);
            
            /**
             * vsechny aktivni formulare presunout do archivovanych
             */
            $this->loadModel('FormData');
            $this->FormData->updateAll(
                array(
                    'FormData.history'=>1
                ), 
                array(
                    'FormData.client_id'=>$client_id,
                    'FormData.history'=>0
                )
            );
            

            die(json_encode(array('result' => true)));
        }


    }


    /**
     * Nova forma zmeny pracovniho procesu, kdy ukoncuje stare zamestnani a vytvari nove
     * Tedy ukoncuje jedno connection(povinny -> [to]) a vytvari nove s -> [from]
     * @author Sol
     * @created 23.11.09
     */
    function zmena_pp($client_id = null, $company_id = null, $company_work_position_id = null)
    {
        Configure::write('debug',1);
        if (empty($this->data))
        {
            $this->loadModel('CompanyWorkPosition');

            $detail = $this->CompanyWorkPosition->find('first',array(
            'fields'=>array('parent_id'),
            'conditions'=>array(
                'id'=> $company_work_position_id)
            ));
            $data_id[] = $company_work_position_id;
            if ($detail)
                $data_id[] = $detail['CompanyWorkPosition']['parent_id'];


            // load list of work position and calculations
            $this->set('company_work_position_list', $p = $this->CompanyWorkPosition->find('list',
                array('conditions' => array(
                    'CompanyWorkPosition.kos' => 0, 
                    'OR' => array('CompanyWorkPosition.id IN ('.implode(',', $data_id) . ')', 
                    'CompanyWorkPosition.parent_id IN (' . implode(',',
                $data_id) . ')'), 'CompanyWorkPosition.company_id' => $company_id,
                'CompanyWorkPosition.test = 0'), 'order' => 'CompanyWorkPosition.name ASC')));

            $this->loadModel('ConnectionClientRequirement');
            $this->ConnectionClientRequirement->bindModel(array(
              'belongsTo'=>array(
                  'CompanyWorkPosition'=>array('fields'=>array('name','id')),
                  'CompanyMoneyItem'=>array('fields'=>array('name','cena_ubytovani_na_mesic','doprava','stravenka'))
              )
            ));
            $ccr = $this->ConnectionClientRequirement->find('first', array(
                'conditions' => array(
                    'ConnectionClientRequirement.kos' => 0,
                    'ConnectionClientRequirement.company_id' => $company_id,
                    'ConnectionClientRequirement.client_id' => $client_id,
                    'ConnectionClientRequirement.type' => 2, 
                    'ConnectionClientRequirement.to' =>'0000-00-00'
                )
            ));
            if (!$ccr)
                die('Neexistuje takový pracovní poměr!');
            
            $this->loadModel('ClientWorkingHour');
            $cwh = $this->ClientWorkingHour->find('first', array(
                'conditions' => array(
                   'ClientWorkingHour.kos' => 0,
                   'ClientWorkingHour.client_id' => $client_id,
                   'ClientWorkingHour.connection_client_requirement_id' => $ccr['ConnectionClientRequirement']['id'],
                   'ClientWorkingHour.celkem_hodin > '=>0
                ),
                'fields'=>array('celkem_hodin')
            ));
            $dochazka = false;
            if($cwh)
               $dochazka = true;
            
            $this->set('dochazka',$dochazka);
                
                
            /**
             * Nacteni OPP k danemu connection a klientovi
             */
            $this->loadModel('ConnectionClientOpp');
            $this->set('opp_list',$this->ConnectionClientOpp->find('all', array(
                'conditions' => array(
                   'ConnectionClientOpp.kos' => 0,
                   'ConnectionClientOpp.client_id' => $client_id,
                   'ConnectionClientOpp.connection_client_requirement_id' => $ccr['ConnectionClientRequirement']['id'],
                )
            )));
            unset($this->ConnectionClientOpp);    

           

            $this->data['connection_client_requirements_id'] = $ccr['ConnectionClientRequirement']['id'];
            $this->data['from_old'] = $ccr['ConnectionClientRequirement']['from'];
            
            $this->set('detail', $ccr);
            $this->set('client_id', $client_id);
            $this->set('company_id', $company_id);

            //render
            $this->render('zmena_pp/zmena_pp_new');
        } else
        {//SAVE
            if ($this->data["company_work_position_id"] != null && $this->data["company_money_item_id"] != null)
            {
                
                if(isset($this->data['scenar']) && $this->data['scenar'] == 2){
                     $datum_ukonceni = explode('-',$this->data['do']);
                     $this->loadModel('ClientWorkingHour');
                     $con = $this->ClientWorkingHour->find('first',array(
                                  'conditions'=>array(
                                      'client_id'=>$client_id,
                                      'connection_client_requirement_id'=>$this->data['connection_client_requirements_id'],
                                      'year'=>$datum_ukonceni[0],
                                      'month'=>$datum_ukonceni[1],
                                      'kos'=>0
                                  )
                     ));
                     if(!$con)
                        die(json_encode(array('result' => false, 'message' => 'Není vygenerovaná žádná docházka pro rok '.$datum_ukonceni[0].' a měsíc '.$datum_ukonceni[1].'. Tudíž nelze vytvořit srážku ze mzdy pro poslední docházku!'))); 
                }
                
                $this->loadModel('ConnectionClientRequirement');
                $old = $this->ConnectionClientRequirement->read(
                    array(
                        'requirements_for_recruitment_id',
                        'client_id',
                        'company_id',
                        'objednavka'
                    ),
                    $this->data["connection_client_requirements_id"]
                );
               
                $this->ConnectionClientRequirement->id = $this->data["connection_client_requirements_id"];
                $this->ConnectionClientRequirement->saveField('to', $this->data["do"]);
                $this->ConnectionClientRequirement->id = null;
                
                $to_save['ConnectionClientRequirement'] = array(
                        'cms_user_id'=>$this->logged_user['CmsUser']['id'],
                        'from'=>$this->data["od"],
                        'type'=>2,
                        'company_work_position_id'=>$this->data["company_work_position_id"],
                        'company_money_item_id'=>$this->data["company_money_item_id"],
                        'requirements_for_recruitment_id'=>$old['ConnectionClientRequirement']["requirements_for_recruitment_id"],
                        'client_id'=>$old['ConnectionClientRequirement']["client_id"],
                        'company_id'=>$old['ConnectionClientRequirement']["company_id"],
                        'objednavka'=>$old['ConnectionClientRequirement']["objednavka"]
                );
                $this->ConnectionClientRequirement->save($to_save);
                
                
                
                /**
                 * Pokud je scenar vetsi nez jedna, klient mel nejake OPP
                 * 
                 * Scenare:
                 *  1. Prenesení klientove OPP
                 *  2. Klasicke vyrezeni OPP jako pri rozvazani pp
                 *  3. OPP se vraci na COO sklad
                 *      - connection kos = 1
                 */
                if(isset($this->data['scenar']) && $this->data['scenar'] > 0 && isset($this->data['Opp'])){
                    $this->loadModel('ConnectionClientOpp');
                    
                    switch($this->data['scenar']){
                        case 1:
                            foreach($this->data['Opp'] as $connection_opp_id => $opp_item){
                                //nacteni dat
                                $opp_detail = $this->ConnectionClientOpp->read(array('narok','DATE_FORMAT(created,"%Y-%m-%d") as created'),$connection_opp_id);
                                /**
                                 * Ty na ktere mel narok autoamticky presouvame do noveho pracovniho pomeru
                                 * ve starem nic neovlivni a co snimi pak chce uzivatel udelat se bude resit az pri 
                                 * rozvazani PP
                                 * 
                                 * Ty na ktere nemel narok musime budto prenest a nebo nechat
                                 * Pokud se jedna o OPP přiděleno před nebo v datum ukončení zůstává v starém connection
                                 * jinak se přenáší do nového pracovního poměru kde se v nadcházejicí dochazce vytvoří srážka!  
                                 */
                                if($opp_detail['ConnectionClientOpp']['narok'] == 'ano' || ($opp_detail['ConnectionClientOpp']['narok'] == 'ne' && ($opp_detail[0]['created'] > $this->data['do']) )){
                                    $this->ConnectionClientOpp->id = $connection_opp_id;
                                    $this->ConnectionClientOpp->saveField('connection_client_requirement_id',$this->ConnectionClientRequirement->id);                                    
                                }
                            }
                        break;
                        
                        case 2:
                            /**
                             * Postupne prochazime vsechny OPP a jejich nastaveni
                             * 
                             */  
                            $srazka_ze_mzdy = array('sum'=>0); 

                                    foreach($this->data['Opp'] as $connection_opp_id => $opp_item){
                                        /**
                                         * Srazka ze mzdy
                                         */
                                        if($opp_item['var'] == 1){
                                            /**
                                             * Pokud je opp v naroku a dava se srazka ze mzdy
                                             * znamena to ze uchazec si OPP nechava a dela se dodatecna srazka z posledni stavajici dochazky.
                                             * V pripade naroku = NE je to pouze inforamtivni, protoze v dochazce a mzde jiz jsou strzeny automaticky!
                                             */
                                            if($opp_item['narok'] == 'ano'){
                                                $srazka_ze_mzdy['sum'] += $opp_item['price']; 
                                                $this->ConnectionClientOpp->id = $connection_opp_id;
                                                $this->ConnectionClientOpp->saveField('srazka_ze_mzdy',1);
                                                $this->ConnectionClientOpp->saveField('cms_user_id2',$this->logged_user['CmsUser']['id']);                       
                                            }
                                        }   
                                        /**
                                         * Vraceni OPP
                                         */
                                        else if($opp_item['var'] == 2){
                                            /**
                                             * Pokud je narok ano, tak se polozka prirazuje pod sklad vracenych OPP
                                             * Varianta s narokem ne nelze!
                                             */
                                            if($opp_item['narok'] == 'ano'){
                                                  $this->ConnectionClientOpp->id = $connection_opp_id;
                                                  $this->ConnectionClientOpp->saveField('vraceno',1); 
                                                  $this->ConnectionClientOpp->saveField('cms_user_id2',$this->logged_user['CmsUser']['id']);                 
                                            }
                                        }
                                     }                 
                                
                                /**
                                 * V pripade ze je potreba odecist nejake srazky z posledni mzdy
                                 */
                                 if(!empty($srazka_ze_mzdy) && $srazka_ze_mzdy['sum'] > 0){
                                         $this->ClientWorkingHour->id = $con['ClientWorkingHour']['id'];
                                         $this->ClientWorkingHour->saveField('drawback_opp',$con['ClientWorkingHour']['drawback_opp'] + $srazka_ze_mzdy['sum']);
                                         $this->ClientWorkingHour->saveField('salary_per_hour_p',$con['ClientWorkingHour']['salary_per_hour_p'] - $srazka_ze_mzdy['sum']);
                                 }  
                        break; 
                        
                        case 3:
                            $this->loadModel('OppOrderItem');
                            foreach($this->data['Opp'] as $connection_opp_id => $opp_item){
                                //nacteni dat
                                $cco = $this->ConnectionClientOpp->read(null,$connection_opp_id);
                                //ulozeni stare connection,m priradime do kose
                                $this->ConnectionClientOpp->id = $connection_opp_id;
                                $this->ConnectionClientOpp->saveField('kos',1);
                                
                                
                                $opp = $this->OppOrderItem->find('first',array(
                                    'conditions'=>array(
                                        'cms_user_id'=>$cco['ConnectionClientOpp']['cms_user_id'],
                                        'type_id'=>$cco['ConnectionClientOpp']['typ'],
                                        'name'=>$cco['ConnectionClientOpp']['name'],
                                        'size'=>$cco['ConnectionClientOpp']['size'],
                                        'kos'=>0
                                    ),'fields'=>array('id','count')  
                                ));
                                $this->OppOrderItem->id = $opp['OppOrderItem']['id'];
                                $this->OppOrderItem->saveField('count',$opp['OppOrderItem']['count'] + 1);
                            }
                            unset($this->OppOrderItem);
                        break;  
                    }
                }
                
               
                unset($this->ConnectionClientRequirement);

                die(json_encode(array('result' => true,'message'=>'Pracovní poměr byl změněn.')));
            } else
                die(json_encode(array('result' => false,'message'=>'Chyba nebyly nastaveny profese a odměny!!!')));
        }
    }
    
    /**
     * @name statistiky spojeni
     * @abstract mezi klientem a jednotlivymi polozkami, dochazka, pozadavek, historie
     * @author Sol
     * @since 3.11.2009
     */
    function stats($client_id = null)
    {
        // pocet zprav od klienta
       $this->loadModel('ClientMessage');
       $this->set('pocet_zprav',$this->ClientMessage->find('count',array(
            'conditions'=>array(
                'kos'=>0,
                'client_id'=>$client_id
            )
       )));
       
       // pocet ulozenych dochazek
       $this->loadModel('ClientWorkingHour');
       $this->set('pocet_dochazek',$this->ClientWorkingHour->find('count',array(
            'conditions'=>array(
                'kos'=>0,
                'client_id'=>$client_id
            )
       )));
       
       
        // spojeni v dochazkach
       $this->loadModel('ConnectionClientRequirement');
       $this->set('pocet_pozadavku',$this->ConnectionClientRequirement->find('count',array(
            'conditions'=>array(
                'kos'=>0,
                'type'=>2,
                'to !='=>'0000-00-00',
                'client_id'=>$client_id
            ),'fields'=>'requirements_for_recruitment_id'
       )));
       
       
       //render
       $this->render('stats');  
    }

    // load okresy
    function load_ajax_okresy($stat_id)
    {
        $this->loadModel('Countrie');
        $this->Countrie->bindModel(array('belongsTo' => array('Province' => array('foreignKey' =>
            'province_id'))));
        die(json_encode($this->Countrie->find('list', array('conditions' => array('Countrie.status' =>
            1, 'Countrie.kos' => 0, 'Province.stat_id' => $stat_id), 'recursive' => 1,
            'order' => 'name ASC'))));
    }


     // private funkce vypis ukolovniku pro tisk 
    function ukolovnik_print()
    {
        Configure::write('debug',1);
        $this->layout = false;
        $this->loadModel('Todo');
        $todos = $this->Todo->find('all', array(
            'conditions' => array(
                'Todo.todo_stav_list' => 1
            ), 
            'order' => 'created ASC'
        ));
         //pr($todos);
        
        foreach($todos as $item){
            
            echo "<h2>".$item['Todo']['id']." - ".$item['Todo']['name']."</h2>";
            echo "<p>".$item['Todo']['text']."</p>";
        }
        die();
    }


     // predelani stare historie do nove
    function historie_to_aktivity()
    {
        die('vypnuto');
        Configure::write('debug',1);
        $this->layout = false;
        $this->loadModel('ClientMessage');
        $client_messages = $this->ClientMessage->find('all', array(
            'fields'=>array(
                'ClientMessage.*','IF(ClientMessage.text LIKE "%byl zaměstnán%",9,13) as action'
            ),
            'conditions' => array(
                'ClientMessage.client_id != 10',
                '(ClientMessage.text LIKE "%byl zaměstnán%" OR ClientMessage.text LIKE "%byl propuštěn%")'
            ), 
            'order' => 'created ASC'
        ));
         //pr($todos);
        
        
        $this->loadModel('HistoryItem');
        $this->loadModel('HistoryData');
        
        foreach($client_messages as $item){                   
            /**
             * vytvoření záznamu historie Item
             */
             $history_item = array(
                'client_id'=>$item['ClientMessage']['client_id'],
                'cms_user_id'=>$item['ClientMessage']['cms_user_id'],
                'created'=>$item['ClientMessage']['created'],
                'category_id'=>8,
                'action_id'=>$item[0]['action'],
                'import'=>1
             );
             $this->HistoryItem->id = null;
             $this->HistoryItem->save($history_item);
             
            /**
             * data pro dany History Item
             */
             $history_data = array(
                'history_item_id'=>$this->HistoryItem->id,
                'caption'=>'Text',
                'value'=>$item['ClientMessage']['text'],
                'cms_user_id'=>$item['ClientMessage']['cms_user_id'],
             );
             $this->HistoryData->id = null;
             $this->HistoryData->save($history_data);
             
            pr($history_item);
            pr($history_data);
        }
        
        unset($this->HistoryItem);
        unset($this->HistoryData);
        
        die();
    }
    
    
    //testovaci funkce
    function my_func(){
        die('vypnuto');
        Configure::write('debug',1);
        $this->layout = false;

       $this->loadModel('ClientWorkingHour');
       $cw = $this->ClientWorkingHour->find('all', array(
            'fields'=>array(
                'ClientWorkingHour.id',
                'ClientWorkingHour.days'
            ),
            'conditions' => array(
                'ClientWorkingHour.kos '=>0
            ),
            'limit'=>'8000,2000'
        ));
        
        echo count($cw).'<br/>';
        $ok = $false = 0;
        
     
        foreach($cw as $c){
             //echo $c['ClientWorkingHour']['id'].'<br />';
             $count_days = 0;
             $days = array();
             if(isset($c['ClientWorkingHour']['days']))
                 foreach($c['ClientWorkingHour']['days'] as $key=>$day){
                    list($null,$x,$y) = explode('_',$key);
                    $days[$y][$x] = $day;
                 }
             
             if(!empty($days)){
                 foreach($days as $d){
                    if($this->ClientWorkingHour->odpracoval($d))
                        $count_days++;
                 }
             }
             
             if($count_days > 0){
                $this->ClientWorkingHour->id = $c['ClientWorkingHour']['id'];
                $this->ClientWorkingHour->saveField('working_days_count',$count_days);
             }
        }      
        
        die("done");
    }
    

    
    /**
     * funkce pro vytvoreni exportu do excelu - CSV
     * podle filtrace vyber dane klienty a vygeneruj je do csv
     */
    function export_excel(){
        //Configure::write('debug',1);

         
       $start = microtime ();
       $start = explode ( " " , $start );
       $start = $start [ 1 ]+ $start [ 0 ];  
        
        $this->renderSetting['items'] = array(
            'id' => 'ID|ClientView2|id|text|',
            'jmeno' => 'Jméno|ClientView2|jmeno|text|',
            'prijmeni' => 'Příjmení|ClientView2|prijmeni|text|',
            'pohlavi_list' => 'Pohlaví|ClientView2|pohlavi_list|var|pohlavi_list',
            'datum_narozeni' => 'Datum narození|ClientView2|datum_narozeni|date|',
            'mobil1' => 'Tel.|ClientView2|mobil1|text|',
            'mobil2' => 'Tel.2|ClientView2|mobil2|text|',
            'mobil3' => 'Tel.3|ClientView2|mobil3|text|',
            'email' => 'Email|ClientView2|email|text|',
            'ulice' => 'Ulice|ClientView2|ulice|text|',
            'mesto' => 'Město|ClientView2|mesto|text|',
            'psc' => 'PSČ|ClientView2|psc|text|',
            'countries_id' => 'Okres|Countrie|name|text|',
            'stat_id' => 'Stát|Stat|name|text|',
            'agree_contact' => 'Souhlasím|ClientView2|agree_contact|text|value_to_yes_no#long',
            'profese1' => 'Profese1|0|Profese|text|parse_string#0',
            'profese2' => 'Profese2|0|Profese|text|parse_string#1',
            'profese3' => 'Profese3|0|Profese|text|parse_string#2',
            'certifikat1' => 'Certifikát1|0|Certifikat|text|parse_string#0',
            'certifikat2' => 'Certifikát2|0|Certifikat|text|parse_string#1',            
            'vzdelani' => 'Vzdělání|SettingEducation|name|text|',
            'obor_vzdelani' => 'Obor vzdělání|Vzdelani|name|text|',
            'poznamka' => 'Poznámka|ClientView2|poznamka|text|',
            'status_imp' => 'StatusImport|ClientView2|import_stav|var|stav_importu_list',
            'status' => 'Status|ClientView2|stav|var|stav_client_list', 
            'firma' => 'Firma|Company|name|text|', 
            'datum_nastupu' => 'Datum nástupu|ConnectionClientRequirement|from|date|', 
            'cislo_uctu' => 'Číslo účtu|ClientView2|cislo_uctu|text|',
            'externi_nabor' => 'Nábor EXT/INT|ClientView2|externi_nabor|var|client_en_type',
            'Recruiter1' => 'Recruiter1|0|Recruiters|text|parse_string#0',
            'Recruiter2' => 'Recruiter2|0|Recruiters|text|parse_string#1',
            'Recruiter3' => 'Recruiter3|0|Recruiters|text|parse_string#2'
        );
         
         $fields_sql = array(
            "ClientView2.id",
            "ClientView2.jmeno",
            "ClientView2.prijmeni",
            "ClientView2.pohlavi_list",
            "ClientView2.datum_narozeni",
            "ClientView2.mobil1",
            "ClientView2.mobil2",
            "ClientView2.mobil3",
            "ClientView2.email",
            "ClientView2.ulice",
            "ClientView2.mesto",
            "ClientView2.psc",
            "ClientView2.agree_contact",
            "ClientView2.poznamka",
            "ClientView2.import_stav",
            "ClientView2.stav",
            "ClientView2.cislo_uctu",
            "ClientView2.externi_nabor",
            "Countrie.name",
            "Stat.name",
            "GROUP_CONCAT(DISTINCT SettingCareerItem.name SEPARATOR '<br/>') as Profese",
            "GROUP_CONCAT(DISTINCT SettingCertificate.name SEPARATOR '<br/>') as Certifikat",
            "GROUP_CONCAT(DISTINCT Recruiter.name SEPARATOR '<br/>') as Recruiters",            
            "Vzdelani.name",
            "SettingEducation.name",
            "Company.name",
            "ConnectionClientRequirement.from",
         );
               
        $criteria = $this->ViewIndex->filtration();
        foreach($criteria as $key => $val){
            if (strpos($key,'ClientView.') !== false){
                $new_key = substr($key,0,strpos($key,'ClientView.')).'ClientView2.'.substr($key,strpos($key,'ClientView.')+11);
                unset($criteria[$key]);
                $criteria[$new_key] = $val;
            }   
        }
        
        
       
        $this->loadModel('ClientView2');
      
     
         header("Pragma: public"); // požadováno
         header("Expires: 0");
         header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
         header("Cache-Control: private",false); // požadováno u některých prohlížečů
         header("Content-Transfer-Encoding: binary");
         Header('Content-Type: application/octet-stream');
         Header('Content-Disposition: attachment; filename="'.date('Ymd_His').'.csv"');
     
      
        /*
         * Celkovy pocet zaznamu
         */
         
        $limit = 100;   
        $page = 1;        
        $count = $this->ClientView2->find('first',
        	array(
        		'fields' =>  array("COUNT(DISTINCT ClientView2.id) as count"),
        		'conditions'=>am($criteria,array('ClientView2.kos'=>0)),
        		'recursive'	=>1
        	)
        );    
        $count = $count[0]['count'];
        
        // hlavicka
        foreach($this->renderSetting['items'] as &$item_setting){
            list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
        	$item_setting = compact(array("caption", "model","col","type","fnc"));	
            if($type != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',$caption).'";'; 
        }
        echo "\n";   
        unset($item_setting, $caption, $model, $col, $type, $fnc);
        
        $str_array=array("<br/>"=>', ',':'=>',',';'=>',',''=>'', '#'=>' ');
        /*
         * Cyklicky vypis dat po $limit zaznamu
         */
        for ($exported = 0; $exported < $count; $exported += $limit){
            foreach($this->ClientView2->find('all',array(
                   'fields'=>$fields_sql,
                    'conditions'=>am($criteria,array('ClientView2.kos'=>0)),
                    'group'=>'ClientView2.id',
                    'limit'=>$limit,
                    'page'=>$page,
                    'recursive'=>1
            )) as $item){
                foreach($this->renderSetting['items'] as $key => $td){
                    if($td['type'] != 'hidden') echo '"'.iconv('UTF-8','Windows-1250',strtr($this->ViewIndex->generate_td($item,$td,$this->viewVars),$str_array)).'";';     
                }
                echo "\n";  
            }
            $page++;
        }
       
        //time
         $end = microtime ();
         $end = explode ( " " , $end );
         $end = $end [ 1 ]+ $end [ 0 ]; 
         echo 'Generate in '.($end - $start).'s';
         echo "\n"; 
        
        die();
    }
    
    
    
    /**
     * tisk karty
     */
    function print_client_card($id = null, $hidden = false){
        $this->layout = 'print';
        
        if($id != null){
            $this->set('admin', $admin_group_id =  $this->logged_user['CmsGroup']['id']);
            $edit_ucet = true;
            $this->set('only_show', $show);
            $this->set('print', true);
            
            if($hidden)
                $this->set('hidden',true);
            

            // nacteni form sablon
            $this->loadModel('FormTemplate');
            $this->set('form_template_list', $this->FormTemplate->find('list', array('conditions' =>
                array('kos' => 0), 'order' => 'name')));
            unset($this->FormTemplate);


            // nacteni kvalifikace
            $this->loadModel('SettingCareerItem');
            $this->set('kvalifikace_list', $this->SettingCareerItem->find('list', array('order' =>
                'name')));
            unset($this->SettingCareerItem);

            // nacteni certifikatu
            $this->loadModel('SettingCertificate');
            $this->set('certifikaty_list', $this->SettingCertificate->find('list', array('order' =>
                'name')));
            unset($this->SettingCertificate);

            // nacteni vzdelani
            $this->loadModel('SettingEducation');
            $this->set('dosazene_vzdelani_list', $this->SettingEducation->find('list', array
                ('order' => 'name')));
            unset($this->SettingEducation);

            // load stat list
            $this->loadModel('SettingStat');
            $this->set('client_stat_list', $this->SettingStat->find('list', array('conditions' =>
                array('SettingStat.status' => 1, 'SettingStat.kos' => 0))));
            unset($this->SettingStat);


            $temp_stat_id = 1; //defaultně načti okresy z ČR

            if ($id != null)
            {
                
                /**
                 * povol zobrazeni pro Client only show
                 */
               
                $this->Client->set_ignore_status(true);
                    
                $this->data = $this->Client->read(null, $id);
                

                $temp_stat_id = $this->data['Client']['stat_id'];
                
                /**
                 * pokud je uz zaskrtnute u cisla uctu zkontrolovano
                 * zamezit editaci pro CM a COO, menit mohou pouze ADMIN, Ucetni
                 */
                if($this->data['Client']['cislo_uctu_control'] == 1 && !in_array($admin_group_id,array(1,6,7)))
                  $edit_ucet = false;
                    
            

                // nacteni modelu kvalifikace
                $this->loadModel('ConnectionClientCareerItem');
                $this->ConnectionClientCareerItem->bindModel(array('belongsTo' => array('SettingCareerItem')));
                // nacteni ulozenych pozic k danemu klientovi
                $this->set('kvalifikace_list_item', $kval = $this->ConnectionClientCareerItem->
                    find('all', array('conditions' => array('client_id' => $id), 'fields' => array('SettingCareerItem.id',
                    'SettingCareerItem.name', 'ConnectionClientCareerItem.name',
                    'ConnectionClientCareerItem.popis', 'ConnectionClientCareerItem.od',
                    'ConnectionClientCareerItem.do', 'ConnectionClientCareerItem.id'), 'recursive' =>
                    1)));
                // nacteni modelu kvalifikace
                $this->loadModel('ConnectionClientCertifikatyItem');
                $this->ConnectionClientCertifikatyItem->bindModel(array('belongsTo' => array('SettingCertificate')));
                // nacteni ulozenych pozic k danemu klientovi
                $this->set('certifikaty_list_item', $cert = $this->
                    ConnectionClientCertifikatyItem->find('all', array('conditions' => array('client_id' =>
                    $id), 'fields' => array('SettingCertificate.id', 'SettingCertificate.name',
                    'ConnectionClientCertifikatyItem.komentar',
                    'ConnectionClientCertifikatyItem.platnost', 'ConnectionClientCertifikatyItem.id'),
                    'recursive' => 1)));

                // nacteni recriteru
                $this->loadModel('ConnectionClientRecruiter');
                $this->ConnectionClientRecruiter->bindModel(array('belongsTo' => array('CmsUser')));
                $this->set('recruiter_list', $this->ConnectionClientRecruiter->find('all', array
                    ('conditions' => array('ConnectionClientRecruiter.client_id' => $id))));


                // nacteni formularu k danemu klientovi
                $this->loadModel('FormData');
                $this->FormData->bindModel(array('belongsTo'=>array('Company')));
                $this->set('client_form_list', $this->FormData->find('all', array(
                    'conditions' =>array(
                        'FormData.client_id' => $id, 
                        'FormData.kos' => 0
                    ),
                    'order'=>'FormData.history ASC'
                )));
                unset($this->FormData);


               
  
                 // nacteni novych aktivit
                $this->loadModel('HistoryItem');
                $this->HistoryItem->bindModel(array(
                    'belongsTo' => array(
                        'CmsUser' => array('fields' =>'name'),
                        'HistoryType' => array('fields' =>'name','foreignKey'=>'action_id')                    
                    ),
                    'hasMany'=>array(
                        'HistoryData' => array('foreignKey'=>'history_item_id')
                    )
                ));
                $this->set('history_item_list', $this->HistoryItem->find('all',array(
                    'conditions'=>array(
                        'HistoryItem.client_id' =>$id
                    ),
                    'order'=>'created DESC'
                )));
                unset($this->HistoryItem);
                
                /**
                 * pro historii nastaveni action id ktere pujdu do zalozky Activity - editace
                 * a nebudou se zobrazovat v zalozkach Activity
                 */
                 $this->set('edit_action_ids',array(5,17));
            }

            // load okresy list
            $this->loadModel('Countrie');
            //$this->set('client_countries_list',null);
            $this->Countrie->bindModel(array('belongsTo' => array('Province' => array('foreignKey' =>
                'province_id'))));
            $this->set('client_countries_list', $cc = $this->Countrie->find('list', array('conditions' =>
                array('Countrie.status' => 1, 'Countrie.kos' => 0, 'Province.stat_id' => $temp_stat_id),
                'recursive' => 1)));
            unset($this->Countrie);
            
            
            /**
             * nastaveni promene ktera povoluje editaci uctu
             */
            $this->set('edit_ucet', $edit_ucet);
            
            //render
            $this->render('edit');
        }
        else        
            die('Bez id!!!');

    }

    /**
     * pridani do seznam inforamci o praci
     */
    function add_info_about_job($name = null){
        if($name != null){
            $this->loadModel('InfoAboutJob');
            if($this->InfoAboutJob->save(array('name'=>$name)))
                die(json_encode(array('result'=> true,'id'=>$this->InfoAboutJob->id)));
            else
               die(json_encode(array('result'=> false)));    
        }
        else 
            die(json_encode(array('result'=> false)));   
    }

    
}

?>