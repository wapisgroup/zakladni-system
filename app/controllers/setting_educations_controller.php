<?php
class SettingEducationsController extends AppController {
	var $name = 'SettingEducations';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingEducation');
	var $renderSetting = array(
		'controller'=>'setting_educations',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'Nastavení vzdělání',
		'sortBy'=>'SettingEducation.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingEducation-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingEducation-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingEducation|id|text|',
			'name'		=>	'Název|SettingEducation|name|text|',
			'updated'	=>	'Upraveno|SettingEducation|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingEducation|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|delete'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[550,900]',
			'scrollbars'	=> true,
			'languages'		=> false
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení vzdělání'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingEducation->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingEducation->save($this->data);
			die();
		}
	}
}
?>