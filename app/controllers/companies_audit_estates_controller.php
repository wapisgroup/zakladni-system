<?php
class CompaniesAuditEstatesController extends AppController {
	var $name = 'CompaniesAuditEstates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('CompanyAuditEstate');
	var $renderSetting = array(
		//'bindModel'	=> array('belongsTo'=>array()),
		'SQLfields' => '*',
		'controller'=> 'companies_audit_estates',
		'page_caption'=>'Firmy',
		'sortBy'=>'CompanyAuditEstate.name.ASC',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat firmu|add',
		),
		'filtration' => array(
			'CompanyAuditEstate-name'				=>	'text|Název|',
			//'CompanyAuditEstate-cms_group_id'		=>	'select|Skupina|cms_group_list',
		 	
		),
		'items' => array(
			'id'		=>	'ID|CompanyAuditEstate|id|text|',
			'name'		=>	'Název|CompanyAuditEstate|name|text|',
			'created'	=>	'Vytvořeno|CompanyAuditEstate|created|datetime|',
			'updated'	=>	'Změněno|CompanyAuditEstate|updated|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'person'	=>	'person|Osoby firmy|person',
            'centre'	=>	'centre|Střediska|centre',
			'trash'	    =>	'trash|Odstranit položku|trash'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->CompanyAuditEstate->read(null,$id);
			}

			$this->render('edit');
		} else {
			$this->CompanyAuditEstate->save($this->data['CompanyAuditEstate']);
			die();
		}
	}
    
    /**
     * sekce osoby k jednotlivym firmam
     */
    function person($company_id = null){
        $this->set('company_id',$company_id);
        
        $this->loadModel('CompanyPerson');
        $person_list = $this->CompanyPerson->find('all',array(
            'conditions'=>array(
                'kos'=>0,
                'company_id'=>$company_id
            )
        ));       
        
        $this->set('person_list',$person_list);
        
        //render
        $this->render('persons/index');
    }
    
        //EDIT
        function person_edit($company_id = null,$id = null){
            if(empty($this->data)){
                if($id != null){
                    $this->loadModel('CompanyPerson');
                    $this->data = $this->CompanyPerson->read(null,$id);
                }
                else                
                    $this->data['CompanyPerson']['company_id'] = $company_id;
                
                //render
                $this->render('persons/edit');
            }
            else{
                $this->loadModel('CompanyPerson');
                $this->CompanyPerson->save($this->data);
            }
        }
        
        //TRASH
        function person_trash($id){
            $this->loadModel('CompanyPerson');
            $this->CompanyPerson->id = $id;
            $this->CompanyPerson->saveField('kos',1);
            unset($this->CompanyPerson);
            die();
        }
        
   /**
     * sekce strediska k jednotlivym firmam
     */
    function centre($company_id = null){
        $this->set('company_id',$company_id);
        
        $this->loadModel('CompanyCentre');
        $centre_list = $this->CompanyCentre->find('all',array(
            'conditions'=>array(
                'kos'=>0,
                'company_id'=>$company_id
            )
        ));       
        
        $this->set('centre_list',$centre_list);
        
        //render
        $this->render('centres/index');
    }
    
        //EDIT
        function centre_edit($company_id = null,$id = null){
            if(empty($this->data)){
                if($id != null){
                    $this->loadModel('CompanyCentre');
                    $this->data = $this->CompanyCentre->read(null,$id);
                }
                else                
                    $this->data['CompanyCentre']['company_id'] = $company_id;
                
                //render
                $this->render('centres/edit');
            }
            else{
                $this->loadModel('CompanyCentre');
                $this->CompanyCentre->save($this->data);
            }
        }
        
        //TRASH
        function centre_trash($id){
            $this->loadModel('CompanyCentre');
            $this->CompanyCentre->id = $id;
            $this->CompanyCentre->saveField('kos',1);
            unset($this->CompanyCentre);
            die();
        }   
}
?>