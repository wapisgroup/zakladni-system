<?php	
Configure::write('debug',1);

	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
	define('CURRENT_YEAR', $_GET['current_year']);
	define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);
	
class ReportBonusCashsController extends AppController {
	var $name = 'ReportBonusCashs';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionCompanyToAuthorization');
	var $renderSetting = array(
		'bindModel' => array(
            'belongsTo' => array(
				'CompanyView' => array('className' => 'CompanyView', 'foreignKey' => 'company_id')
            )
        ),
		'controller'=>'report_bonus_cashs',
		'SQLfields' => array(
            'ConnectionCompanyToAuthorization.id',
            'ConnectionCompanyToAuthorization.month',
            'ConnectionCompanyToAuthorization.year',
            'ConnectionCompanyToAuthorization.closed',
            'ConnectionCompanyToAuthorization.stav',
            'ConnectionCompanyToAuthorization.company_id',
            'CompanyView.client_manager',
            'CompanyView.coordinator',
            'CompanyView.coordinator2',
            'CompanyView.name,
                (SELECT SUM(salary_part_4_p) as dop
                FROM wapis__client_working_hours as w
                WHERE w.company_id = ConnectionCompanyToAuthorization.company_id AND w.year = ConnectionCompanyToAuthorization.year and w.month = ConnectionCompanyToAuthorization.month
             ) as doplatky'
        ),
		'SQLcondition'=> array(
            'ConnectionCompanyToAuthorization.stav' => 2,
            'ConnectionCompanyToAuthorization.month' => CURRENT_MONTH,
            'ConnectionCompanyToAuthorization.year' => CURRENT_YEAR,
        ),
		'page_caption'=>'Reporty - Doplatky Abc',
		'sortBy'=>'NULL.doplatky.DESC',
        'info_box'=>'data_info_box',
		'top_action' => array(
			//'add_item'		=>	'Přidat|edit|Přidat novou fakturu|add',
		),
		'filtration' => array(	
			'GET-current_month'					=>	'select|Měsíc|mesice_list',
            'GET-current_year'					=>	'select|Rok|actual_years_list',
			// 'Company-self_manager_id'			=>	'select|SalesManager|sm_list',
			// 'CompanyInvoiceItem-company_id'		=>	'select|Společnost|company_list',
		),
		'items' => array(
			'id'				=>	'ID|ConnectionCompanyToAuthorization|id|hidden|',
			'company'			=>	'Firma|CompanyView|name|text|',
			'cm_id'				=>	'ClientManager|CompanyView|client_manager|text|',
			'coordinator'		=>	'Koordinátor|CompanyView|coordinator|text|',
			'coordinator2'		=>	'Koordinátor2|CompanyView|coordinator2|text|',
			'mesic'				=>	'Za měsíc|ConnectionCompanyToAuthorization|month|var|mesice_list|',
			'rok'				=>	'Za rok|ConnectionCompanyToAuthorization|year|text|',
			'stav'				=>	'Stav|ConnectionCompanyToAuthorization|stav|var|stav_company_auth_list|',
            'suma'				=>	'Suma Abc|0|doplatky|text|'
		),
		'posibility' => array(
			'show'		=>	'show|Detail|show',	
			'print'		=>	'print_detail|Tisk|print'	
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> 'true',
			'languages'		=> 'false'
		)
	);
    
	function index(){
	//	pr($this->viewVars['items']);
	
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Doplatky Abc'=>'#'));
        
        $this->loadModel('ClientWorkingHour');
        $this->ClientWorkingHour->bindModel(array('belongsTo'=>array('Company')));
        $info_box_sum = $this->ClientWorkingHour->find('all',array(
            'conditions'=>array(
                'ClientWorkingHour.year'=>CURRENT_YEAR,
                'ClientWorkingHour.month'=>CURRENT_MONTH,
                'ClientWorkingHour.kos'=>0,
                'ClientWorkingHour.company_id IN (SELECT company_id FROM wapis__connection_company_to_authorizations as cc WHERE cc.kos = 0 and cc.year = "'.CURRENT_YEAR.'" and cc.month = "'.CURRENT_MONTH.'")'
            ),
            'fields'=>array(
                'SUM(IF(Company.stat_id = 1,salary_part_4_p,0)) as cz',
                'SUM(IF(Company.stat_id = 2,salary_part_4_p,0)) as eu'
            ),
            'group'=>'ClientWorkingHour.year,ClientWorkingHour.month'
        ));
        
        if(!$info_box_sum){
           $info_box_sum = array('cz'=>0,'eu'=>0); 
        }
        else{
           $info_box_sum = $info_box_sum[0][0];
        }
        
        $info_box = '<span>Celkem za: CZ: '.$info_box_sum['cz'].' CZK</span><br /><span style="margin-left:55px;">SK: '.$info_box_sum['eu'].' EUR</span>';
		$this->set('data_info_box',$info_box);
        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function show($id){
		if ($id != null){
			$this->set('id',$id);
			$detail = $this->ConnectionCompanyToAuthorization->read(array('company_id','year','month'),$id);
			
			$this->loadModel('ClientWorkingHour');
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array('Client','CompanyWorkPosition','CompanyMoneyItem','ConnectionClientRequirement','Company')
		    ));
	
			$this->set('zamestnanci_list',$z = $this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.company_id'=>$detail['ConnectionCompanyToAuthorization']['company_id'],
					'ClientWorkingHour.year'=>$detail['ConnectionCompanyToAuthorization']['year'],
					'ClientWorkingHour.month'=>$detail['ConnectionCompanyToAuthorization']['month'],
					'ConnectionClientRequirement.type'=>2
				),
				'fields'=>array(
                    'ClientWorkingHour.id',
                    'ConnectionClientRequirement.from','ConnectionClientRequirement.to',
					'ClientWorkingHour.connection_client_requirement_id',
					'ClientWorkingHour.year','ClientWorkingHour.month',
					'CompanyMoneyItem.name','ClientWorkingHour.id',
					'ClientWorkingHour.stav','Client.name',
					'celkem_hodin','CompanyWorkPosition.name',
					'drawback','odmena_1','odmena_2',
					'salary_per_hour','salary_per_hour_p',
					'salary_part_1_p','salary_part_2_p','salary_part_3_p','salary_part_4_p',		
					'Company.stat_id',
					'company_money_item_id','stop_payment',
                    'CompanyMoneyItem.cena_ubytovani_na_mesic','CompanyMoneyItem.doprava','CompanyMoneyItem.stravenka'                    
				),
				'order'=>array('Client.name ASC','salary_part_4_p DESC')
			)));

            //pr($z);

			unset($this->ClientWorkingHour);

			//render
			$this->render('show');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}
    
    function print_detail($id){
        $this->layout = 'print';
        
        if ($id != null){
            $this->ConnectionCompanyToAuthorization->bindModel(array(
			 'belongsTo'=>array('Company')
		    ));
			$this->set('detail',$detail = $this->ConnectionCompanyToAuthorization->read(
                array('company_id','year','month','Company.stat_id','Company.name',
                '(SELECT SUM(salary_part_4_p) as dop
                    FROM wapis__client_working_hours as w
                    WHERE w.company_id = ConnectionCompanyToAuthorization.company_id AND w.year = ConnectionCompanyToAuthorization.year and w.month = ConnectionCompanyToAuthorization.month
                 ) as doplatky'
                ),$id));
			
			$this->loadModel('ClientWorkingHour');
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array('Client','CompanyWorkPosition','CompanyMoneyItem','ConnectionClientRequirement','Company')
		    ));
	
			$this->set('zamestnanci_list',$this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.company_id'=>$detail['ConnectionCompanyToAuthorization']['company_id'],
					'ClientWorkingHour.year'=>$detail['ConnectionCompanyToAuthorization']['year'],
					'ClientWorkingHour.month'=>$detail['ConnectionCompanyToAuthorization']['month'],
					'ConnectionClientRequirement.type'=>2,
					'ClientWorkingHour.salary_part_4_p >'=>0
				),
				'fields'=>array(
                    'ConnectionClientRequirement.id',
                    'ConnectionClientRequirement.from','ConnectionClientRequirement.to',
					'ClientWorkingHour.connection_client_requirement_id',
					'ClientWorkingHour.year','ClientWorkingHour.month',
					'CompanyMoneyItem.name','ClientWorkingHour.id',
					'ClientWorkingHour.stav','Client.name',
					'celkem_hodin','CompanyWorkPosition.name',
					'drawback','odmena_1','odmena_2',
					'salary_per_hour','salary_per_hour_p',
					'salary_part_1_p','salary_part_2_p','salary_part_3_p','salary_part_4_p',		
					'Company.stat_id',
					'company_money_item_id','stop_payment',
                    'CompanyMoneyItem.cena_ubytovani_na_mesic','CompanyMoneyItem.doprava','CompanyMoneyItem.stravenka',
                    '(SELECT IFNULL(SUM(amount),0) FROM wapis__report_down_payments WHERE connection_client_requirement_id = ConnectionClientRequirement.id and year="'.$detail['ConnectionCompanyToAuthorization']['year'].'" and month = "'.$detail['ConnectionCompanyToAuthorization']['month'].'") as zalohy'
				),
				'order'=>array('Client.name ASC','salary_part_4_p DESC')
			)));

			unset($this->ClientWorkingHour);

			//render
			$this->render('print');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}    
	
	
}
?>