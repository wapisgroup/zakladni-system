<?php
Configure::write('debug',1);
class AuditEstateWithdrawalTypesController extends AppController {
	var $name = 'AuditEstateWithdrawalTypes';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('AuditEstateWithdrawalType');
	var $renderSetting = array(
        'bindModel'	=> array(
			//'belongsTo'	=>	array('AuditEstateWithdrawalTypeGroup')
		),
		'SQLfields' => array('AuditEstateWithdrawalType.id','AuditEstateWithdrawalType.name','AuditEstateWithdrawalType.updated',
            'AuditEstateWithdrawalType.created'
        ),
		'controller'=> 'audit_estate_withdrawal_types',
		'page_caption'=>'Kategorie vyřazení majetku',
		'sortBy'=>'AuditEstateWithdrawalType.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
		    //'AuditEstateWithdrawalType-form_template_group_id'		=>	'select|Skupina|group_list'
		),
		'items' => array(
			'id'		=>	'ID|AuditEstateWithdrawalType|id|text|',
			//'group'		=>	'Skupina|AuditEstateWithdrawalType|form_template_group_id|viewVars|group_list',
			'name'		=>	'Nazev|AuditEstateWithdrawalType|name|text|',
			'updated'	=>	'Upraveno|AuditEstateWithdrawalType|updated|datetime|',
			'created'	=>	'Vytvořeno|AuditEstateWithdrawalType|created|datetime|'
		),
		'posibility' => array(
			'status'	                => 	'status|Změna stavu|status',
			'edit'		                =>	'edit|Editace položky|edit',
			//'export_to_companies'		=>	'export_to_companies|Exportovat do firem|export_to_companies',
			'trash'	                    =>	'trash|Do kosiku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> 'true',
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Šablony formulářů'=>'#'));
        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){

		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null){
				$this->data = $this->AuditEstateWithdrawalType->read(null,$id);
			}
            
			$this->render('edit');
		} else {
			$this->AuditEstateWithdrawalType->save($this->data);
            die();
		}
	}

}
?>