<?php
//Configure::write('debug',0);
class CmsUsersController extends AppController {
	var $name = 'CmsUsers';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CmsUser');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('CmsGroup')),
		'SQLfields' => array('CmsUser.id','CmsUser.name','CmsUser.email','CmsUser.last_log','CmsUser.updated','CmsUser.created','CmsUser.status','CmsGroup.name','CmsUser.mobil', 'CmsUser.telefon'),
		'controller'=> 'cms_users',
		'page_caption'=>'Uživatelé systému',
		'sortBy'=>'CmsUser.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add'		=>	'Pridat|edit|Pridat|add',
			//'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
			//'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			'CmsUser-name'				=>	'text|Jm√©no|',
			'CmsUser-cms_group_id'		=>	'select|Skupina|cms_group_list',
		 	
		),
		'items' => array(
			'id'		=>	'ID|CmsUser|id|text|',
			'name'		=>	'Název|CmsUser|name|text|',
			'mobil'		=>	'Mobil|CmsUser|mobil|text|',
			'tel'		=>	'telefon|CmsUser|telefon|text|',
			'email'		=>	'Email|CmsUser|email|text|',
			'group'		=>	'Skupina|CmsGroup|name|text|',
			'last_log'	=>	'Last Login|CmsUser|last_log|datetime|',
			'updated'	=>	'Změněno|CmsUser|updated|datetime|'
		),
		'posibility' => array(
			'status_new'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Odstranit položku|trash'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Uživatelé systému'=>'#'));
		$this->loadModel('CmsGroup'); $this->CmsGroup =& new CmsGroup();
		$this->set('cms_group_list',$this->CmsGroup->find('list',array('order'=>'name')));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			// load cms group list
			$this->loadModel('CmsGroup'); 
			$this->set('cms_group_list',$this->CmsGroup->find('list',array('conditions'=>array('CmsGroup.kos'=>0,'CmsGroup.status'=>1))));


            $this->loadModel('ConnectionClientAtCompanyWorkPosition'); 
            $this->ConnectionClientAtCompanyWorkPosition->query('SET NAMES UTF8');
            $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
                'belongsTo'=>array('Client')
            ));
			$person_list = $this->ConnectionClientAtCompanyWorkPosition->find('all',array(							
					'conditions'=>array(
						'ConnectionClientAtCompanyWorkPosition.datum_to'=>'0000-00-00',
                        'Client.kos'=>0,
                        'Client.name !='=>''
					),'fields'=>array('Client.id','Client.name'),'order'=>'Client.name'
			));
            $this->set('at_employees_list',Set::combine($person_list,'{n}.Client.id','{n}.Client.name'));
			//$this->set('at_employees_list',$person_list);
            
			if ($id != null){
				$this->data = $this->CmsUser->read(null,$id);
			}

			$this->render('edit');
		} else {
			//print_r($this->data);
			$this->CmsUser->save($this->data['CmsUser']);
			
			if($this->data['ConnectionMlmRecruiter']['mlm_group_id'] != ''){
				$this->loadModel('ConnectionMlmRecruiter');
				$this->data['ConnectionMlmRecruiter']['cms_user_id']=$this->CmsUser->id;
				$this->data['ConnectionMlmRecruiter']['rok_od']=date('Y');
				$this->data['ConnectionMlmRecruiter']['mesic_od']=date('m');
				$this->ConnectionMlmRecruiter->save($this->data['ConnectionMlmRecruiter']);
			}
			die();
		}
	}
	
	function logout(){
		$this->Session->delete('logged_user');
		$this->redirect('/login/');
	}
	
	function login(){
		$this->layout = 'login';
		if (!empty($this->data)){
			$condition = array(
				'CmsUser.email' => 	$this->data['CmsUser']['email'],
				'CmsUser.heslo'		=> 	md5($this->data['CmsUser']['heslo']),
				'CmsUser.kos'		=>	0,
				'CmsUser.status'	=>	1,
				'CmsGroup.status'	=>	1,
				'CmsGroup.kos'		=>	0
			);

			$this->CmsUser->bindModel(array('belongsTo'=>array('CmsGroup')));
			$user = $this->CmsUser->find($condition,array('CmsUser.id','CmsUser.email','CmsUser.at_employee_id','CmsUser.last_log','CmsUser.name','CmsGroup.name','CmsGroup.id','CmsGroup.permission','CmsGroup.usershare_folders','CmsGroup.cms_group_superior_id','CmsUser.last_log'));

            /**
             * O≈°et≈ôen√≠ browseru!!!
             *
            if(!in_array($user['CmsGroup']['id'],array(1,56,57)) && !in_array($user['CmsUser']['id'],array(14))){
               if(strpos($_SERVER['HTTP_USER_AGENT'],'Prism') === false && $_SERVER['REMOTE_ADDR'] != '90.176.43.89'){
                  die(json_encode(array('result'=>false,'message'=>'CHYBA: Přístup do ATEPU je POUZE a jen z aplikace ATEP. Přístup přes webový prohlížeč je zakázan!')));
               }       
            } */
            
            if ($user){
				$user['domena'] = 'wapis.cz';
				$user['ftp_jedno'] = '';
				$user['domena_upload'] = DOMENA_UPLOAD;
				$user['ftp_relace'] = FTP_RELACE;
				$user['ftp_login'] = FTP_LOGIN;
				$user['ftp_password'] = FTP_PASSWORD;
				$this->Session->write('logged_user',$user);
				$this->CmsUser->save(array('CmsUser'=>array('id'=>$user['CmsUser']['id'], 'last_log'=>date("Y-m-d H:i:s"))));

				//logace poslednich prihlaseni
				$this->loadModel('LastLogin'); 
				$pole["ip"] = $_SERVER["REMOTE_ADDR"];
				$pole["cms_user_id"] = $user['CmsUser']['id'];
				$this->LastLogin->save($pole);

				die(json_encode(array('result'=>true)));
			} else {
				die(json_encode(array('result'=>false,'message'=>'Byly zadány špatné přihlašocaví údaje, nebo byl Váš účet deaktivovaný.')));
			}
		}
	}
	
	function upload_foto() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	}
    
    function change_password(){
		if (empty($this->data)){
            $id = $this->logged_user['CmsUser']['id'];
			if ($id != null){
				$this->data = $this->CmsUser->read(null,$id);
			}
            else { die(json_encode(array('result'=>false))); }

			$this->render('change_password');
		}else {
			$this->CmsUser->save($this->data['CmsUser']);
			die(json_encode(array('result'=>true)));
		}
	}
    
	
    /**
     * Priradime uzivatelum atepu, id internich zamestanncu pokud najdeme takove jmeno
     */
	function add_at_employee_id(){
	   $users = $this->CmsUser->find('all',array('conditions'=>array('kos'=>0,'at_employee_id IS NULL')));
       
       $this->loadModel('ConnectionClientAtCompanyWorkPosition'); 
            $this->ConnectionClientAtCompanyWorkPosition->query('SET NAMES UTF8');
            $this->ConnectionClientAtCompanyWorkPosition->bindModel(array(
                'belongsTo'=>array('Client')
            ));
			$person_list = $this->ConnectionClientAtCompanyWorkPosition->find('all',array(							
					'conditions'=>array(
						'ConnectionClientAtCompanyWorkPosition.datum_to'=>'0000-00-00',
                        'Client.kos'=>0,
                        'Client.name !='=>''
					),'fields'=>array('Client.id','Client.name'),'order'=>'Client.name'
			));
            $at_employees_list = Set::combine($person_list,'{n}.Client.name','{n}.Client.id');
       $suxes = 0;     
       foreach($users as $user){
            if(array_key_exists($user['CmsUser']['name'],$at_employees_list)){
                $suxes++;
                $this->CmsUser->id = $user['CmsUser']['id'];
                $this->CmsUser->saveField('at_employee_id',$at_employees_list[$user['CmsUser']['name']]);
            }
       }
       echo($suxes);
       die();
	}
    
    
    function generate_new_password_for_group($cms_group_id = 4){
        die('vypnuto');
        $this->loadModel('CmsUser'); 
        $users = $this->CmsUser->find('all',array(							
				'conditions'=>array(
                    'CmsUser.kos'=>0,
                    'CmsUser.status'=>1,
                    //'CmsUser.id'=>1,
                    //'CmsUser.cms_group_id'=>$cms_group_id
				)
		));
        echo count($users);
        foreach($users as $user){
            $heslo = substr(md5($user['CmsUser']['id'].time()),0,6);
            
            $this->CmsUser->id = $user['CmsUser']['id'];
            $this->CmsUser->saveField('heslo',$heslo);
            
            $replace_list = array(
            	'##Client.email##'=>$user['CmsUser']['email'],
                '##heslo##'=> $heslo,
            );
            $this->Email->send_from_template_new(33,array($user['CmsUser']['email']),$replace_list);
       }
       die();
    }
}
?>