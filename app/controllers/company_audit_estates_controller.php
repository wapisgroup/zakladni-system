<?php
die('nepouziva se');
Configure::write('debug',1);
class CompanyAuditEstatesController extends AppController {
	var $name = 'CompanyAuditEstates';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('CompanyAuditEstate');
	var $renderSetting = array(
		//'bindModel'	=> array('belongsTo'=>array('Company')),
		'SQLfields' => '*,
                       (
                            SELECT COUNT(id) 
                            FROM wapis__connection_audit_estates
                            Where company_id=CompanyAuditEstate.id and kos=0 and to_date = "0000-00-00"
                       ) as count_pronajem,
                       (
                            SELECT COUNT(id) 
                            FROM wapis__audit_estates
                            Where company_id=CompanyAuditEstate.id and kos=0
                       ) as count_evidence,
                       (
                            SELECT SUM(a.hire_price) 
                            FROM wapis__connection_audit_estates as c
                            LEFT JOIN wapis__audit_estates as a ON(a.id=c.audit_estate_id)
                            Where c.company_id=CompanyAuditEstate.id and c.kos=0 and c.to_date = "0000-00-00"
                       ) as sum_hire_price
        ',
        'SQLcondition'=>array(
            '(
                (
                  SELECT COUNT(id) 
                    FROM wapis__connection_audit_estates
                  Where company_id=CompanyAuditEstate.id and kos=0 and to_date = "0000-00-00"
                ) > 0 
              OR
                (SELECT COUNT(id) FROM wapis__audit_estates Where company_id=CompanyAuditEstate.id and kos=0) > 0
             )   
              '
        ),
		'controller'=> 'person_audit_estates',
		'page_caption'=>'Majetek podle firem',
		'sortBy'=>'CompanyAuditEstate.name.ASC',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat majetek|add',
		),
		'filtration' => array(
			'Company-name'				=>	'text|Jméno|',		 	
		),
		'items' => array(
			'id'		=>	'ID|CompanyAuditEstate|id|text|',
			'name'		=>	'Jméno|CompanyAuditEstate|name|text|',
			'count_evidence'=>	'v evidenci|0|count_evidence|text|',
			'count_pronajem'=>	'v uzivani|0|count_pronajem|text|',
			'sum_hire_price'=>	'Mesíční pronájem|0|sum_hire_price|money2|'
		),
		'posibility' => array(
			'show'		        =>	'show|Detail položky|show'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Evidence majetku'=>'#',$this->renderSetting['page_caption']=>'#'));

		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
    
    /**
     * detail firem a vypis jejiho majetku
     */
	function show($company_id = null){
            
            $this->loadModel('ConnectionAuditEstate');
            $this->ConnectionAuditEstate->bindModel(array('belongsTo'=>array('AuditEstate')));
            $estate_list = $this->ConnectionAuditEstate->find('all',array(
                'conditions'=>array(
                    'ConnectionAuditEstate.kos'=>0,
                    'ConnectionAuditEstate.company_id'=>$company_id
                )
            ));
            $this->set('estate_list',$estate_list);
            
            $this->loadModel('AuditEstate');
            $audit_estate_list = $this->AuditEstate->find('all',array(
                'conditions'=>array(
                    'kos'=>0,
                    'company_id'=>$company_id
                )
            ));
            $this->set('audit_estate_list',$audit_estate_list);
	}
    
    
    /**
     * funkce pro odevzdani majetku zpet firme
     */
    function give_off($connection_audit_estate_id = null){
        if(empty($this->data)){
            $this->data['ConnectionAuditEstate']['id'] = $connection_audit_estate_id;
            
            //render
            $this->render('give_off');
        }        
        else{            
            $this->loadModel('ConnectionAuditEstate');
            if($this->ConnectionAuditEstate->save($this->data))
                die(json_encode(array('result'=>true)));
            else
                die(json_encode(array('result'=>false)));
         }
    }
    
    
   
}
?>