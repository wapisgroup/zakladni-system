<?php
class ChangelogsController extends AppController {
	var $name = 'Changelogs';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('Changelog');
	var $renderSetting = array(
		'controller'=>'changelogs',
		'SQLfields' => array('Changelog.id','Changelog.text','Changelog.updated','Changelog.created','Changelog.datum'),
		'page_caption'=>'Changelog',
		'sortBy'=>'Changelog.datum.DESC',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(),
		'items' => array(
			'id'		=>	'ID|Changelog|id|text|',
			
			'datum'		=>	'Datum|Changelog|datum|date|',
            'text'		=>	'Text|Changelog|text|text|orez#100',
			'updated'	=>	'Upraveno|Changelog|updated|datetime|',
			'created'	=>	'Vytvořeno|Changelog|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|delete'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[550,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Chnagelog'=>'#'));
	
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->Changelog->read(null,$id);
			$this->render('edit');
		} else {
			$this->Changelog->save($this->data);
			die();
		}
	}
}
?>