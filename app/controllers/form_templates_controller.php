<?php
Configure::write('debug',1);
class FormTemplatesController extends AppController {
	var $name = 'FormTemplates';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Wysiwyg');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('FormTemplate');
	var $renderSetting = array(
        'bindModel'	=> array(
			'belongsTo'	=>	array('FormTemplateGroup')
		),
		'SQLfields' => array('FormTemplate.id','FormTemplate.name','FormTemplate.updated',
            'FormTemplate.created','FormTemplate.text','FormTemplate.form_template_group_id',
            'FormTemplateGroup.name'
        ),
        'SQLcondition' => array(
            'FormTemplate.type'=>0
        ),
		'controller'=> 'form_templates',
		'page_caption'=>'Šablony formulářů',
		'sortBy'=>'FormTemplate.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
            
			'group'		=>	'Skupiny|groups|Skupiny šablon|groups',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		     'FormTemplate-form_template_group_id'		=>	'select|Skupina|group_list'
		),
		'items' => array(
			'id'		=>	'ID|FormTemplate|id|text|',
			'group'		=>	'Skupina|FormTemplate|form_template_group_id|viewVars|group_list',
			'name'		=>	'Nazev|FormTemplate|name|text|',
			'text'		=>	'Popis|FormTemplate|text|text|orez#50',
			'updated'	=>	'Upraveno|FormTemplate|updated|datetime|',
			'created'	=>	'Vytvořeno|FormTemplate|created|datetime|'
		),
		'posibility' => array(
			'status'	                => 	'status|Změna stavu|status',
			'edit'		                =>	'edit|Editace položky|edit',
			'export_to_companies'		=>	'export_to_companies|Exportovat do firem|export_to_companies',
			'trash'	                    =>	'trash|Do kosiku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> 'true',
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('scripts', array('wysiwyg_old/wswg')); 
		$this->set('styles', array('../js/wysiwyg_old/wswg'));
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Šablony formulářů'=>'#'));
        
        $this->loadModel('FormTemplateGroup');
        	$this->set('group_list', $this->FormTemplateGroup->find(
        		'list',
        		array(
        			'conditions' => array(
        				'FormTemplateGroup.kos' => 0
        			)
        		)
        	));
        
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){

		$this->autoLayout = false;
		if (empty($this->data)){
		      
            /**
             * nacteni skupin sablon 
             */  
            $this->loadModel('FormTemplateGroup');
        	$this->set('group_list', $this->FormTemplateGroup->find(
        		'list',
        		array(
        			'conditions' => array(
        				'FormTemplateGroup.kos' => 0
        			)
        		)
        	));
          
			if ($id != null){
				$this->data = $this->FormTemplate->read(null,$id);
			}
			$this->render('edit');
		} else {
			$this->FormTemplate->save($this->data);
			
            /**
             * hlidani parent sablon u firem
             * musime projit vsechny CompanyFormTemplate kde je toto id parent a 
             * upozrnit tak na aktualizaci
             */
            $this->loadModel('CompanyFormTemplate');
            $this->CompanyFormTemplate->updateAll(
                array(
                    'CompanyFormTemplate.change'=>1
                ), 
                array(
                    'CompanyFormTemplate.parent_id'=>$this->data['FormTemplate']['id']
                )
            );
                  
            
            die();
		}
	}


	function generate_form($id = null,$client_id, $data_id = null, $company_table = false){
		if($id == null && $data_id == null)
			die('Chyba, špatná kombinace id!!!');


		$this->autoLayout = false;
		$this->loadModel('Client');
		$this->client = $this->Client->read(null,$client_id);

        /**
         * defaultne tahano z globalnich sablon 
         */
        $data_table = 'CompanyFormTemplate'; 
        $this->loadModel($data_table); 
        $datas = $this->$data_table->read(null,$id);

	
		preg_match_all('@##(.*)##@',$datas[$data_table]['text'],$text2);

		foreach($text2[1] as $key=>$item){
			$temp = explode('|',$item);
            
            //pokud neni nastaven value
            if(!isset($temp[1]))
				$temp[1] = null;
            //pokud neni nastaven set
			if(!isset($temp[2]))
				$temp[2] = null;
                
			list($type, $value, $set) = $temp;
			//echo $value;
			$text2[1][$key] = $this->transfer_to_html($type,$value,$set);
		}

		$data = str_replace($text2[0], $text2[1], $datas[$data_table]['text']);
		$this->set('data',$data);
		
		$this->data['FormData']['client_id']=$client_id;
		$this->data['FormData']['name']=$datas[$data_table]['name'];
	
        if($company_table){
            /**
             * rovnou se uklada k danemu klientovi
             */
            $this->loadModel('FormData');
            $form_data_to_save = $this->data;
            $form_data_to_save['FormData']['company_id'] = $datas[$data_table]['company_id'];
            $form_data_to_save['FormData']['html_code'] = $data;
                        
            $this->FormData->id = null; 
            $this->FormData->save($form_data_to_save); 
        }
        else //pro globalni sablony se zobrazuje
		  $this->render('generate/index');
          
         
	}
    
    
    function generate_form_new($id = null,$client_id){
		if($id == null)
			die('Chyba, špatná kombinace id!!!');


		$this->autoLayout = false;
		$this->loadModel('Client');
		$this->client = $this->Client->read(null,$client_id);
        
        $this->loadModel('ConnectionClientRequirement');
        $conn = $this->ConnectionClientRequirement->find('first',array(
            'conditions'=>array(
                'type'=>2,
                'client_id'=>$client_id,
            ),
            'order'=>'id DESC'
        ));
        
        if($conn)
            $this->client['ConnectionClientRequirement'] = $conn['ConnectionClientRequirement'];
        /**
         * defaultne tahano z globalnich sablon 
         */
        $data_table = 'CompanyFormTemplate'; 
        $this->loadModel($data_table); 
        $datas = $this->$data_table->read(null,$id);

	
		preg_match_all('@##(.*)##@',$datas[$data_table]['text'],$text2);

		foreach($text2[1] as $key=>$item){
			$temp = explode('|',$item);
            
            //pokud neni nastaven value
            if(!isset($temp[1]))
				$temp[1] = null;
            //pokud neni nastaven set
			if(!isset($temp[2]))
				$temp[2] = null;
                
			list($type, $value, $set) = $temp;
			//echo $value;
			$text2[1][$key] = $this->transfer_to_html($type,$value,$set);
		}

		$data = str_replace($text2[0], $text2[1], $datas[$data_table]['text']);
		$this->set('data',$data);
		
		$this->data['FormData']['client_id']=$client_id;
		$this->data['FormData']['name']=$datas[$data_table]['name'];
	
		$this->render('generate/index_new');
	}

	private function transfer_to_html($type,$value,$set){
		$r = null;
		$value = substr($value,1,-1);
		
		if(strpos($value,'.') != false){
			list($model,$col) = explode('.',$value);
			$value = $this->client[$model][$col];	
		}

		if($type != ''){
			switch($type){
				case 'INPUT' :
					$r = '<input type="text" name="data[FormData][values][]" value="'.$value.'" />';
				break;

				case 'CHECKBOX' :
					$r = '<input type="checkbox" name="data[FormData][values][]" '.($value==1 ? 'checked="checked"' : "").' />';
				break;

				case 'TEXTAREA' :
					$r = '<textarea rows="5" cols="35"  name="data[FormData][values][]">'.$value.'</textarea>';
				break;

				case 'SELECT' :
				case 'SELECTN' :
					$s_typ = substr($set,0,1);

					if($set != null){
						$r = '<select name="data[FormData][values][]" />';
						//$r .= '<option value="">lol</option>';
						if($s_typ == '_'){
							$select = substr($set,1,-1);
							if ($type == 'SELECTN')
								$r .= '<option value=""></option>';
							foreach($this->viewVars[$select] as $val=>$name){
								$r .= '<option value="'.$val.'" '.($value==$val ? 'selected="selected"' : "").' >'.$name.'</option>';
							}
						}
						else if($s_typ == '{'){
							$select = split(',',substr($set,1,-1));

							foreach($select as $val=>$name){
								$r .= '<option value="'.$val.'" '.($value==$val ? 'selected="selected"' : "").' >'.$name.'</option>';
							}
						}
		
						$r .= '</select>';
					}
				break;
			}
		}
		
		return $r;
	
	}


	function save_client_form(){

		if(!empty($this->data)){
			//$html_code = $this->FormTemplate->read(array('text'),$this->data['FormData']['template_id']);
		//	$this->data['FormData']['html_code'] = $html_code['FormTemplate']['text'];
			
			$this->loadModel('FormData');
			$this->FormData->save($this->data);
			$save_id = $this->FormData->id;
			unset($this->FormData);
			
			die(json_encode(array('result'=>true, 'message' => 'All it`s ok :-)', 'id'=> $save_id)));
		}
		else 
			die(json_encode(array('result'=>false, 'message' => 'Chyba: Nepodařilo se uložit do DB.')));
	}

	
	function delete_client_form($id){
		$this->loadModel('FormData');
		$this->FormData->id = $id;
		$this->FormData->saveField('kos',1);
		unset($this->FormData);		
		die();
	}

	
	function edit_client_form($id){
		$this->loadModel('FormData');
		$this->data = $this->FormData->read(null,$id);
		unset($this->FormData);		

		// render
		$this->render('generate/edit');
	}

	function print_client_form($id){
		$this->layout = 'print';

		$this->loadModel('FormData');
		$this->data = $this->FormData->read(null,$id);
		unset($this->FormData);		

		// render
		$this->render('generate/print');
	}
    
    
     /*
     * sprava jednotlivych skupinn
     */
    function groups(){
    	$this->loadModel('FormTemplateGroup');
    	$this->set('group_list', $this->FormTemplateGroup->find(
    		'all',
    		array(
    			'conditions' => array(
    				'FormTemplateGroup.kos' => 0
    			),
    			'order'=>'FormTemplateGroup.created DESC'
    		)
    	));
        
        // render
		$this->render('groups/index');
    	
    }
    
    function group_edit($id = null){
    	$this->loadModel('FormTemplateGroup');
    	if (empty($this->data)){
    		if ($id != null){
    			$this->data = $this->FormTemplateGroup->read(null, $id); 
    		}
                
            // render
    		$this->render('groups/edit');
    	} else {
    		if ($this->FormTemplateGroup->save($this->data))
    			die(json_encode(array('result'=>true)));
    		else
    			die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani nove skupiny')));
    	}
    	
    }
    
    
    function export_to_companies($id){
        $detail_template = $this->FormTemplate->read(null,$id);
        
        $checkbox = array(
            1=>'checkbox_pracovni_smlouva',
            2=>'checkbox_dohoda',
            3=>'checkbox_faktura',
            4=>'checkbox_cash',
        );

        $this->loadModel('CompanyMoneyItem');
        $this->CompanyMoneyItem->bindModel(array(
            'belongsTo'=>array(
                'Company'=>array('fields'=>array('id','name'),'conditions'=>array('Company.kos'=>0))
            )
        ));
        $find = $this->CompanyMoneyItem->find('all',array(
             'fields'=>array('Company.id','Company.name'),
             'conditions'=>array(
                  'Company.id <>'=>0,
                  'Company.id NOT IN(SELECT company_id FROM wapis__company_form_templates where parent_id = '.$id.' and history=0 and kos=0)',
                  'CompanyMoneyItem.kos'=>0,
                  'company_money_validity_id <>'=>-1,
                  $checkbox[$detail_template['FormTemplate']['form_template_group_id']]=>1
              ),
              'group'=>'CompanyMoneyItem.company_id',
              'order'=>'Company.name ASC'
        ));
        unset($this->CompanyMoneyItem);
        
        if($find){
            /**
             * ulozeni teto sablony ke vsem firmam
             * ktere vyhovuji podmince, tedy maji nejakou takovou kalkulaci
             */
            $this->loadModel('CompanyFormTemplate'); 
            foreach($find as $item){
                $this->CompanyFormTemplate->id = null;
                $to_save = array(
                    'company_id'=>$item['Company']['id'],
                    'parent_id'=>$id,
                    'form_template_group_id'=>$detail_template['FormTemplate']['form_template_group_id'],
                    'name'=>$detail_template['FormTemplate']['name'],
                    'text'=>$detail_template['FormTemplate']['text']
                );
                $this->CompanyFormTemplate->save($to_save);
            }
            unset($this->CompanyFormTemplate);
            
            die(json_encode(array('result'=>true,'pocet'=>count($find))));
        }
        else
            die(json_encode(array('result'=>false,'message'=>'Nenalezeny žádne firmy k exportu.')));
    }


   
}
?>