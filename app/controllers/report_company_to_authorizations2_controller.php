<?php	
	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
	$_GET['current_month'] = '06';
    define('CURRENT_YEAR', $_GET['current_year']);
	define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

define('fields','CompanyView.id,ConnectionCompanyToAuthorization.new,
                ConnectionCompanyToAuthorization.id,ConnectionCompanyToAuthorization.auth_date,
                ConnectionCompanyToAuthorization.month,ConnectionCompanyToAuthorization.year,
                ConnectionCompanyToAuthorization.closed,ConnectionCompanyToAuthorization.stav,
                CompanyView.sales_manager,CompanyView.client_manager,CompanyView.coordinator,
                CompanyView.coordinator2,CompanyView.name,CompanyView.company_manager,
    IF(DAY(closed) > 6,(SELECT value FROM wapis__settings as setting Where id = 9 LIMIT 1),0) as srazka_coo,
    IF(DAY(auth_date) > 8,(SELECT value FROM wapis__settings as setting Where id = 10 LIMIT 1),0) as srazka_mr
');

Configure::write('debug',1);	
class ReportCompanyToAuthorizations2Controller extends AppController {
	var $name = 'ReportCompanyToAuthorizations2';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionCompanyToAuthorization');
	var $renderSetting = array(
		'bindModel' => array('belongsTo' => array(
				'CompanyView' => array('className' => 'CompanyView', 'foreignKey' => 'company_id')
		)),
		'controller'=>'report_company_to_authorizations',
		'SQLfields' => array(fields),
		'SQLcondition'=>array(
            'ConnectionCompanyToAuthorization.year'=> CURRENT_YEAR,
			'ConnectionCompanyToAuthorization.month'=> CURRENT_MONTH
        ),
		'page_caption'=>'Reporty - Docházka k autorizaci',
		'sortBy'=>'ConnectionCompanyToAuthorization.closed.DESC',
		'top_action' => array(
			//'add_item'		=>	'Přidat|edit|Přidat novou fakturu|add',
		),
		'filtration' => array(
			'GET-current_year'					=>	'select|Rok|actual_years_list',
			'GET-current_month'					=>	'select|Měsíc|mesice_list',
			'CompanyView-client_manager_id|coordinator_id|coordinator_id2-cmkoo'=>	'select|Koordinátor|cm_koo_list',
            'CompanyView-manazer_realizace_id'	=>	'select|Manažer real.|manager_realizace_list',		 	

			// 'CompanyInvoiceItem-company_id'		=>	'select|Společnost|company_list',
		),
		'items' => array(
			'id'				=>	'ID|ConnectionCompanyToAuthorization|id|hidden|',
			'company'			=>	'Firma|CompanyView|name|text|',
            'cam_id' 		    =>	'Manažér realizace|CompanyView|company_manager|text|',
			'sm_id' 			=>	'SalesManager|CompanyView|sales_manager|text|',
			'cm_id'				=>	'Koordinátor|CompanyView|client_manager|text|',
			'coordinator'		=>	'Koordinátor2|CompanyView|coordinator|text|',
			'coordinator2'		=>	'Koordinátor3|CompanyView|coordinator2|text|',
			'mesic'				=>	'Za měsíc|ConnectionCompanyToAuthorization|month|var|mesice_list|',
			'rok'				=>	'Za rok|ConnectionCompanyToAuthorization|year|text|',
			'stav'				=>	'Stav|ConnectionCompanyToAuthorization|stav|var|stav_company_auth_list|',
			 'new'	=>	'#|ConnectionCompanyToAuthorization|new|text|status_to_ico2#new-Nový typ docházky-1',
            'closed'			=>	'Datum uz. docházky|ConnectionCompanyToAuthorization|closed|datetime|',
			'auth_date'			=>	'Datum autorizace. docházky|ConnectionCompanyToAuthorization|auth_date|datetime|',
            'srazka_coo'		=>	'Srážka Koo|0|srazka_coo|text|',
            'srazka_mr'		    =>	'Srážka MR|0|srazka_mr|text|'
        ),
		'posibility' => array(
			'show'		=>	'show|Detail|show',			
			'autorizace'		=>	'autorizace|Autorizovat|autorizace',			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'	=> 'false',
			'languages'		=> 'false'
		),
        'checkbox_setting' => array(
			'model'			=>	'CompanyView',
            'col'			=>	'id',
            'checked_on_start'=>true
		),
        'sum_variables_on_top'=>array(
            'path'=>'../report_company_to_authorizations/filtration_top'
        )
	);
	
	function index(){      
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Docházka k autorizaci'=>'#'));
		$this->loadModel('CmsUser');
			$this->set('manager_realizace_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>18))));
			
    		$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
    			'conditions'=>array(
    				'kos'=>0,
    				'status'=>1,
    				'cms_group_id IN(3,4)'
    			)
    		)));
            
            
        $_ignored_items = array();
        if(isset($this->params['url']['chck'])){
            $_ignored_items = explode('|',rtrim($this->params['url']['chck'],'|'));
        }      
        $this->set('_ignored_items',$_ignored_items);
        
        //ty odškrtlé řádky vynulujeme
		foreach($this->viewVars['items'] as &$item){
            if(in_array($item['CompanyView']['id'],$_ignored_items)){
                $item[0]['srazka_coo'] = $item[0]['srazka_mr'] = '0 ';
            }   
		}
        
        // celkova cena za srazky
            $this->ConnectionCompanyToAuthorization->bindModel($this->renderSetting['bindModel']);
    		$rr = $this->ConnectionCompanyToAuthorization->find('all',array(
                'conditions'=>$this->ViewIndex->criteria,
                'fields'=>array('CompanyView.id,IF(DAY(closed) > 6,(SELECT value FROM wapis__settings as setting Where id = 9 LIMIT 1),0) as srazka_coo,
                                 IF(DAY(auth_date) > 8,(SELECT value FROM wapis__settings as setting Where id = 10 LIMIT 1),0) as srazka_mr'
                ),
            ));
            $this->set('filtration_sum_variables2',$rr);    
            
            
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function show($id){
		if ($id != null){
			$this->set('id',$id);
			$detail = $this->ConnectionCompanyToAuthorization->read(array('new','company_id','year','month'),$id);
			
            /**
             * zobrazeni novych dochazek, v nove funkci
             */
            if($detail['ConnectionCompanyToAuthorization']['new'] == 1){
                self::show_new($id,$detail);
                return false;
            }
            
            
            $month = $detail['ConnectionCompanyToAuthorization']['month'];
            $month = ($month<10) ? '0'.$month : $month;
            
            $dochazka_model = array(0=>'ClientWorkingHour',1=>'NewClientWorkingHour');
			$this->loadModel('ClientWorkingHour');
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array(
                'Client','CompanyWorkPosition','CompanyMoneyItem',
                'ConnectionClientRequirement'=>array(
                        'conditions'=>array(
                            "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".$detail['ConnectionCompanyToAuthorization']['year']."-".$month."'))",
		                  	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.$detail['ConnectionCompanyToAuthorization']['year'].'-'.$month.'") OR (ConnectionClientRequirement.to = "0000-00-00"))',
		                  	'type'=> 2
                        ),
                        'fields'=>array(
                            'ConnectionClientRequirement.id'
                        )
                ),
                'Company')
		    ));
	
			$this->set('zamestnanci_list',$this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.company_id'=>$detail['ConnectionCompanyToAuthorization']['company_id'],
					'ClientWorkingHour.year'=>$detail['ConnectionCompanyToAuthorization']['year'],
					'ClientWorkingHour.month'=>$month,
					'ConnectionClientRequirement.id <>'=>0
				),
				'fields'=>array(
					'ClientWorkingHour.connection_client_requirement_id',
					'ClientWorkingHour.year','ClientWorkingHour.month',
					'CompanyMoneyItem.name','ClientWorkingHour.id',
					'ClientWorkingHour.stav','Client.name',
					'celkem_hodin','CompanyWorkPosition.name',
					'drawback','odmena_1','odmena_2',
					'salary_per_hour','salary_per_hour_p',
					'salary_part_1','salary_part_2','salary_part_3','salary_part_4',	
					'salary_part_1_max','salary_part_2_max','salary_part_3_max','salary_part_4_max',	
					'Company.stat_id',
					'company_money_item_id','ClientWorkingHour.director_read'
				),
				'order'=>'Client.name ASC'
			)));

			unset($this->ClientWorkingHour);

			//render
			$this->render('show');	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}	
    
    function show_new($id,$detail){

            $month = $detail['ConnectionCompanyToAuthorization']['month'];
            $month = ($month<10) ? '0'.$month : $month;
            
			$this->loadModel('NewClientWorkingHour');
			$this->NewClientWorkingHour->bindModel(array(
			'belongsTo'=>array(
                'Client','CompanyWorkPosition','NewMoneyItem',
                'ConnectionClientRequirement'=>array(
                        'conditions'=>array(
                            "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".$detail['ConnectionCompanyToAuthorization']['year']."-".$month."'))",
		                  	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.$detail['ConnectionCompanyToAuthorization']['year'].'-'.$month.'") OR (ConnectionClientRequirement.to = "0000-00-00"))',
		                  	'type'=> 2,
                            'ConnectionClientRequirement.new'=> 1
                        ),
                        'fields'=>array(
                            'ConnectionClientRequirement.id'
                        )
                ),
                'Company')
		    ));
	
			$this->set('zamestnanci_list',$this->NewClientWorkingHour->find('all',array(
				'conditions'=>array(
					'NewClientWorkingHour.company_id'=>$detail['ConnectionCompanyToAuthorization']['company_id'],
					'NewClientWorkingHour.year'=>$detail['ConnectionCompanyToAuthorization']['year'],
					'NewClientWorkingHour.month'=>$month,
					'ConnectionClientRequirement.id <>'=>0
				),
				'fields'=>array(
					'NewClientWorkingHour.connection_client_requirement_id',
					'NewClientWorkingHour.year','NewClientWorkingHour.month',
					'NewClientWorkingHour.id',
					'NewClientWorkingHour.stav','Client.name',
					'celkem_hodin','CompanyWorkPosition.name',
					'salary_start','salary_per_hour_p',
                    'sum_bonus','sum_drawback','sum_extra_pay',
					//'salary_part_1','salary_part_2','salary_part_3','salary_part_4',	
					//'salary_part_1_max','salary_part_2_max','salary_part_3_max','salary_part_4_max',	
					'Company.stat_id',
					'NewClientWorkingHour.director_read'
				),
				'order'=>'Client.name ASC'
			)));

			unset($this->NewClientWorkingHour);

			//render
			$this->render('show_new');	
    }

	function autorizace($id){

		if ($id != null){
			$this->ConnectionCompanyToAuthorization->bindModel(array(
			'belongsTo'=>array('Company')
		    ));
			$detail = $this->ConnectionCompanyToAuthorization->read(array('ConnectionCompanyToAuthorization.new','Company.name','company_id','year','month'),$id);

			$this->ConnectionCompanyToAuthorization->id = $id;
			$this->ConnectionCompanyToAuthorization->saveField('stav',2);
			$this->ConnectionCompanyToAuthorization->saveField('auth_date',date("Y-m-d H:i:s"));
			$this->ConnectionCompanyToAuthorization->saveField('auth_user',$this->logged_user['CmsUser']['id']);
			
            if($detail['ConnectionCompanyToAuthorization']['new'] == 1){
                self::autorizace_new($id,$detail);
                return false;
            }
            
			$this->loadModel('ClientWorkingHour');
			$this->ClientWorkingHour->updateAll(
			    array('ClientWorkingHour.stav' => 3),
			    array('ClientWorkingHour.company_id' => $detail['ConnectionCompanyToAuthorization']['company_id'],
			    'ClientWorkingHour.month' => $detail['ConnectionCompanyToAuthorization']['month'],
			    'ClientWorkingHour.year' => $detail['ConnectionCompanyToAuthorization']['year']
				)
			);
			$this->ClientWorkingHour->bindModel(array(
			'belongsTo'=>array('CompanyMoneyItem','Client')
		    ));
			$clients = $this->ClientWorkingHour->find('all',array(
				'fields'=>array('Client.id','Client.name','CompanyMoneyItem.checkbox_pracovni_smlouva','CompanyMoneyItem.checkbox_dohoda','CompanyMoneyItem.checkbox_faktura','CompanyMoneyItem.checkbox_cash'),
				'conditions'=> array('ClientWorkingHour.company_id' => $detail['ConnectionCompanyToAuthorization']['company_id'],
									'ClientWorkingHour.month' => $detail['ConnectionCompanyToAuthorization']['month'],
									'ClientWorkingHour.year' => $detail['ConnectionCompanyToAuthorization']['year']
				)));
			$this->loadModel('CmsUser');
			$fu_list = $this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>array(6)),'order'=>'CmsUser.name ASC'));
			$mu_list = $this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>array(7)),'order'=>'CmsUser.name ASC'));
			unset($this->CmsUser);


			$this->loadModel('CompanyTask');
			foreach($clients as $item){
                //data pro historii
                $data = array(
                    'company_id'=>$detail['ConnectionCompanyToAuthorization']['company_id'],
                    'month'=>$detail['ConnectionCompanyToAuthorization']['month'],
                    'year'=>$detail['ConnectionCompanyToAuthorization']['year']
                );
           
                //odeslani akce pro zachyceni historie
                $this->requestAction('report_company_to_authorizations/record_autorization_client/'.$item['Client']['id'],array('data'=>$data));
             
             
				//financni ucetni
				if($item['CompanyMoneyItem']['checkbox_faktura']==1 || $item['CompanyMoneyItem']['checkbox_cash']==1)
				{
					foreach($fu_list as $fu=>$key){
						//vytvoreni ukolu
						$termin = new DateTime();
						$termin->modify("+2 day");					
						$task_to_save = array('CompanyTask'=>array(
							'cms_user_id' 	=> 	$fu, 
							'name'			=> 	'Autorizovaná výplata',
							'termin'		=>	$termin->format("Y-m-d"),
							'company_id'	=>	$detail['ConnectionCompanyToAuthorization']['company_id'],
							'setting_task_type_id' => 9,
							'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
							'text'			=> 'Byla autorizovaná výplata ve společnosti <strong>'.$detail['Company']['name'].'</strong> u klienta <strong>'.$item['Client']['name'].'</strong>.'
						));
						$this->CompanyTask->id = null;
						$this->CompanyTask->save($task_to_save);
						
					}
				}
				// mzdova ucetni
				if($item['CompanyMoneyItem']['checkbox_pracovni_smlouva']==1 || $item['CompanyMoneyItem']['checkbox_dohoda']==1)
				{
					foreach($mu_list as $mu=>$key){
						//vytvoreni ukolu
						$termin = new DateTime();
						$termin->modify("+2 day");					
						$task_to_save = array('CompanyTask'=>array(
							'cms_user_id' 	=> 	$mu, 
							'name'			=> 	'Autorizovaná výplata',
							'termin'		=>	$termin->format("Y-m-d"),
							'company_id'	=>	$detail['ConnectionCompanyToAuthorization']['company_id'],
							'setting_task_type_id' => 9,
							'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
							'text'			=> 'Byla autorizovaná výplata ve společnosti <strong>'.$detail['Company']['name'].'</strong> u klienta <strong>'.$item['Client']['name'].'</strong>.'
						));
						$this->CompanyTask->id = null;
						$this->CompanyTask->save($task_to_save);
						
					}
				}
			}
			unset($this->CompanyTask);
			unset($this->ClientWorkingHour);

			die();	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}
    
    function autorizace_new($id,$detail){
		if ($id != null){
	
			$this->loadModel('NewClientWorkingHour');
			$this->NewClientWorkingHour->updateAll(
			    array('NewClientWorkingHour.stav' => 3),
			    array('NewClientWorkingHour.company_id' => $detail['ConnectionCompanyToAuthorization']['company_id'],
			    'NewClientWorkingHour.month' => $detail['ConnectionCompanyToAuthorization']['month'],
			    'NewClientWorkingHour.year' => $detail['ConnectionCompanyToAuthorization']['year']
				)
			);
			$this->NewClientWorkingHour->bindModel(array(
			'belongsTo'=>array('Client')
		    ));
			$clients = $this->NewClientWorkingHour->find('all',array(
				'fields'=>array('Client.id','Client.name'),
				'conditions'=> array('ClientWorkingHour.company_id' => $detail['ConnectionCompanyToAuthorization']['company_id'],
									'ClientWorkingHour.month' => $detail['ConnectionCompanyToAuthorization']['month'],
									'ClientWorkingHour.year' => $detail['ConnectionCompanyToAuthorization']['year']
				)));
			$this->loadModel('CmsUser');
			$fu_list = $this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>array(6)),'order'=>'CmsUser.name ASC'));
			$mu_list = $this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>array(7)),'order'=>'CmsUser.name ASC'));
			unset($this->CmsUser);


			$this->loadModel('CompanyTask');
			foreach($clients as $item){
                //data pro historii
                $data = array(
                    'company_id'=>$detail['ConnectionCompanyToAuthorization']['company_id'],
                    'month'=>$detail['ConnectionCompanyToAuthorization']['month'],
                    'year'=>$detail['ConnectionCompanyToAuthorization']['year']
                );
           
                //odeslani akce pro zachyceni historie
                $this->requestAction('report_company_to_authorizations/record_autorization_client/'.$item['Client']['id'],array('data'=>$data));
             

					foreach($fu_list as $fu=>$key){
						//vytvoreni ukolu
						$termin = new DateTime();
						$termin->modify("+2 day");					
						$task_to_save = array('CompanyTask'=>array(
							'cms_user_id' 	=> 	$fu, 
							'name'			=> 	'Autorizovaná výplata',
							'termin'		=>	$termin->format("Y-m-d"),
							'company_id'	=>	$detail['ConnectionCompanyToAuthorization']['company_id'],
							'setting_task_type_id' => 9,
							'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
							'text'			=> 'Byla autorizovaná výplata ve společnosti <strong>'.$detail['Company']['name'].'</strong> u klienta <strong>'.$item['Client']['name'].'</strong>.'
						));
						$this->CompanyTask->id = null;
						$this->CompanyTask->save($task_to_save);
						
					}
				

					foreach($mu_list as $mu=>$key){
						//vytvoreni ukolu
						$termin = new DateTime();
						$termin->modify("+2 day");					
						$task_to_save = array('CompanyTask'=>array(
							'cms_user_id' 	=> 	$mu, 
							'name'			=> 	'Autorizovaná výplata',
							'termin'		=>	$termin->format("Y-m-d"),
							'company_id'	=>	$detail['ConnectionCompanyToAuthorization']['company_id'],
							'setting_task_type_id' => 9,
							'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
							'text'			=> 'Byla autorizovaná výplata ve společnosti <strong>'.$detail['Company']['name'].'</strong> u klienta <strong>'.$item['Client']['name'].'</strong>.'
						));
						$this->CompanyTask->id = null;
						$this->CompanyTask->save($task_to_save);
						
					}
			}
			unset($this->CompanyTask);
			unset($this->NewClientWorkingHour);

			die();	
	    }
		else 
			die('Bez id ? :-)');
		
		
	}
	
	function repair($id,$working_hour_id, $new = 0){
	    $models = array(0=>'ClientWorkingHour',1=>'NewClientWorkingHour');
		if (empty($this->data))
	  	{
			
			// promenne
	        $this->set('id',$id);
            $this->set('new',$new);
            
            $this->loadModel($models[$new]);
            $d = $this->$models[$new]->read(array('client_id'), $working_hour_id);
	        $this->set('client_id',$d[$models[$new]]['client_id']);
            
	        $this->set('working_hour_id',$working_hour_id);
	       
			//render
	        $this->render('repair');
	  	}
	  	else {
			include('select_config.php');
			// ulozeni stavu klienta / working hour
			$this->loadModel($models[$new]);
			$this->$models[$new]->id = $working_hour_id;
			$this->$models[$new]->saveField('stav',1);
			

			// ulozeni stavu firmy
			$this->ConnectionCompanyToAuthorization->id = $id;
			$this->ConnectionCompanyToAuthorization->saveField('stav',0);

			//vytvoreni ukolu
			$this->ConnectionCompanyToAuthorization->bindModel(array(
			'belongsTo'=>array('Company')
		    ));
			$detail = $this->ConnectionCompanyToAuthorization->read(array('company_id','year','month','Company.*'),$id);
			$this->$models[$new]->bindModel(array(
			'belongsTo'=>array('Client')
		    ));
			$client = $this->$models[$new]->read(array('Client.name'),$working_hour_id);


			$termin = new DateTime();
			$termin->modify("+2 day");

			$cms_user_id[] = $detail['Company']['client_manager_id'];
			$cms_user_id[] = $detail['Company']['coordinator_id'];
			$cms_user_id[] = $detail['Company']['coordinator_id2'];
			
			
			//vytvoreni ukolu
			$this->loadModel('CompanyTask');
			foreach($cms_user_id as $user ){
				$task_to_save = array('CompanyTask'=>array(
					'cms_user_id' 	=> 	$user, 
					'name'			=> 	'Odpracované hodiny k opravě',
					'termin'		=>	$termin->format("Y-m-d"),
					'company_id'	=>	$detail['ConnectionCompanyToAuthorization']['company_id'],
					'setting_task_type_id' => 9,
					'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
					'text'			=> 'Odpracované hodiny k opravě ve společnosti <strong>'.$detail['Company']['name'].'</strong> u klienta <strong>'.$client['Client']['name'].'</strong> za měsíc <strong>'.$this->mesice_list[$detail['ConnectionCompanyToAuthorization']['month']].'</strong> a rok <strong>'.$detail['ConnectionCompanyToAuthorization']['year'].'</strong><br /><br /> Komentář :<br />'.$this->data['text'].' .'
				));
				$this->CompanyTask->id = null;
				$this->CompanyTask->save($task_to_save);
			}
			
	

			$replace_list = array(
				'##CompanyTask.name##' 			=>$task_to_save['CompanyTask']['text'],
				'##CompanyTask.text##' 	=>$task_to_save['CompanyTask']['name']
			);
			$this->Email->set_company_data($detail['Company']);	
			$this->Email->send_from_template_new(1,array(),$replace_list);
			
			
			
			unset($this->CompanyTask);
			unset($this->ClientWorkingHour);
			die();
	    }		
	}
    
    
    // $year,$month,$company_name
    function record_autorization_client($client_id){
       return true;                
    }
    
    
    /**
     * funkce ktera meni stav precteno/neprecteno dane dochazky pro directora
     * @created 7.12.09
     */
    function change_status_read($client_working_hour_id = null,$stav,$new = 0){
        if($client_working_hour_id != null){
            $models = array(0=>'ClientWorkingHour',1=>'NewClientWorkingHour');
            $this->loadModel($models[$new]);
            $this->$models[$new]->id = $client_working_hour_id;
            
            if($this->$models[$new]->saveField('director_read',$stav))
                die(json_encode(array('result'=>true)));
            else
                die(json_encode(array('result'=>false)));
        }
    }
	
	
	
}
?>