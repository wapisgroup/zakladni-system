<?php
class CompanyForeignsController extends AppController {
	var $name = 'CompanyForeigns';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('CompanyForeign');
	var $renderSetting = array(
		'controller'=>'company_foreigns',
		'SQLfields' => array('id','name','updated','created'),
		'page_caption'=>'Soukromé firmy',
		'sortBy'=>'CompanyForeign.name.ASC',
		'top_action' => array(
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(),
		'items' => array(
			'id'		=>	'ID|CompanyForeign|id|text|',
			'name'		=>	'Název|CompanyForeign|name|text|',
			'updated'	=>	'Upraveno|CompanyForeign|updated|datetime|',
			'created'	=>	'Vytvořeno|CompanyForeign|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
			'trash'	=>	'trash|Do košiku|trash'			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[550,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->CompanyForeign->read(null,$id);
                
             // load stat list
			$this->set('company_stat_list',$this->get_list('SettingStat'));    
			$this->render('edit');
		} else {
			$this->CompanyForeign->save($this->data);
			die();
		}
	}
}
?>