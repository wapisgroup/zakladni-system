<?php
//Configure::write('debug',2);
require('orders_cast_posts_controller.php');
// http://www.facebook.com/login.php?api_key=117861415038761&connect_display=popup&v=1.0&next=http://www.facebook.com/connect/login_success.html&cancel_url=http://www.facebook.com/connect/login_failure.html&fbconnect=true&return_session=true&req_perms=read_stream,publish_stream,offline_access
// http://www.facebook.com/login.php?api_key=117861415038761&connect_display=popup&v=1.0&next=http://www.facebook.com/connect/login_success.html&cancel_url=http://www.facebook.com/connect/login_failure.html&fbconnect=true&return_session=true&session_key_only=true&req_perms=read_stream,publish_stream,offline_access
define('APPID', '117861415038761');
define('APPSECRET', '71e1e8aa254ea894652ffa69ee955865');

App::import('Vendor', 'facebook', array('file' => 'facebook2' . DS . 'facebook.php'));

class NaborOrdersController extends OrdersCastPostsController
{
    var $name = 'NaborOrders';
    var $helpers = array('htmlExt', 'Pagination', 'ViewIndex', 'FileInput');
    var $components = array('ViewIndex', 'RequestHandler', 'Upload', 'Email');
    var $uses = array('CompanyOrderItem');
    var $renderSetting = array(
        //'SQLfields' => array('id','Company.name','updated','created','status','SalesManager.name','ClientManager.name','Coordinator.name','mesto','SettingStav.namre'),
        'SQLfields' => '*,
            CONCAT_WS(" + ",CompanyOrderItem.count_of_free_position,CompanyOrderItem.count_of_substitute) as objednavka,
             CONCAT_WS(" / ",CompanyOrderItem.count_of_free_position,
                (SELECT 
                        COUNT(c.id) 
                     FROM wapis__connection_client_requirements as c
                     Where 
                        c.company_id = CompanyOrderItem.company_id AND 
                        c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                        type = 1 AND
                        objednavka = 1
                     ),
                IF(
                 CompanyOrderItem.order_status = 1,
                    (SELECT 
                        COUNT(c.id) 
                     FROM wapis__connection_client_requirements as c
                     Where 
                        c.company_id = CompanyOrderItem.company_id AND 
                        c.requirements_for_recruitment_id = CompanyOrderItem.id AND
                        type = 2 AND
                        objednavka = 1
                     ),
                 CompanyOrderItem.statistika
                )
            ) as statistika,
            IF((CompanyOrderItem.start_datetime < NOW() && CompanyOrderItem.order_status IN (-1,1)), 1 , 0) as expire                
        ',
        'SQLcondition' => array(
            'order_status' => array(-1, 1),
            'nabor' => 1
        ),
        'bindModel' => array('belongsTo' => array(
            'Company' => array('foreignKey' => 'company_id')
        )),
        'controller' => 'nabor_orders',
        'page_caption' => 'Obsazeni objednávek - Nábor',
        'sortBy' => 'CompanyOrderItem.order_date.DESC',
        'top_action' => array(),
        'filtration' => array(
            'CompanyOrderItem-company_id' => 'select|Společnost|company_list',
        ),
        'items' => array(
            'id' => 'ID|CompanyOrderItem|id|text|',
            'firma' => 'Firma|Company|name|text|',
            'order' => 'Profese|CompanyOrderItem|name|text|',
            'order_date' => 'Datum objednávky|CompanyOrderItem|order_date|date|',
            'start_datetime' => 'Datum nástupu|CompanyOrderItem|start_datetime|date|',
            'objednavka' => 'Objednávka|0|objednavka|text|',
            'statistika' => 'Statistika|0|statistika|text|',
            'start_comment' => 'Komentář|CompanyOrderItem|start_comment|text|orez#30',
            'status' => 'Status|CompanyOrderItem|order_status|var|order_stav_list',
            'created' => 'Vytvořeno|CompanyOrderItem|created|datetime|',
        ),
        'class_tr' => array(
            'class' => 'color_red',
            'model' => 0,
            'col' => 'expire',
            'value' => 1
        ),
        'posibility' => array(
            'pozice' => 'nabor_pozice|Obsazení pracovních pozic|pozice',
            //'show'		    =>	'show|Detail šablony|show'
            'edit' => 'edit|Editovat WWW formulář|edit',

        ),
        'domwin_setting' => array(
            'sizes' => '[1000,1000]',
            'scrollbars' => true,
            'languages' => 'false'
        )
    );

    function beforeRender()
    {
        parent::beforeRender();

        parent::set_path('orders_cast_posts');
    }

    /**
     * Funkce, zda je dany klient na cerne listine
     * @param INT $client_id, ID klienta
     */
    function is_blacklist($client_id, $return_array = false)
    {
        $this->loadModel('ClientRating');
        $this->ClientRating->bindModel(array('belongsTo' => array('Client')));
        if (isset($this->data['clients']))
            $client_id = $this->data['clients'];

        $clients = $this->ClientRating->find(
            'all',
            array(
                'conditions' => array(
                    'blacklist' => array(2, 3),
                    'client_id' => $client_id
                ), 'fields' => array('Client.id', 'Client.name'),
                'group' => 'Client.id'
            )
        );
        $wrong_client = null;
        $count_client = 0;
        if ($clients)
            foreach ($clients as $client) {
                if ($return_array !== false)
                    $wrong_client .= '#' . $client['Client']['id'] . ' - ' . $client['Client']['name'] . "\n";

                $count_client++;
            }

        if ($count_client > 0) {
            if ($return_array === false)
                die(json_encode(array('result' => false)));
            else
                die(json_encode(array('result' => false, 'wrong_clients' => $wrong_client)));
        } else {
            die(json_encode(array('result' => true)));
        }
    }

    /**
     * Funkce, zda je dany klient na cekaci listine uz nekde
     * @param INT $client_id, ID klienta
     */
    function is_allready_on_cl($client_id)
    {
        $this->loadModel('ConnectionClientRequirement');
        $client = $this->ConnectionClientRequirement->find(
            'count',
            array(
                'conditions' => array(
                    'kos' => 0,
                    'type' => 1,
                    'client_id' => $client_id
                )
            )
        );
        //var_dump($client);
        if ($client > 0) {
            die(json_encode(array('result' => false)));
        } else {
            die(json_encode(array('result' => true)));
        }
    }


    /**
     * Editace WWW objednavka
     *
     * @param $order_id
     * @return view
     * @access public
     **/

    function edit($order_id = null)
    {
        $this->loadModel('CompanyOrderItem');
        if (empty($this->data)) {
            if ($order_id != null)
                $this->data = $this->CompanyOrderItem->read(null, $order_id);

            $this->render('../report_company_orders/edit');

        } else {
            if ($this->CompanyOrderItem->save($this->data)) {
                die(json_encode(array('result' => true)));
            } else {
                die(json_encode(array('result' => false, 'message' => 'Chyba behem ukladani')));
            }
        }

    }

    function fcb_add()
    {
        //Configure::write('debug',1);
        $this->loadModel('CmsUser');
        $user = $this->CmsUser->read(array('id', 'fcb_token', 'fcb_session_key'), 1);

        if (empty($user['CmsUser']['fcb_token'])) {
            die(json_encode(array('result' => false, 'message' => "U Vašeho účtu není nastaven Token pro Facebook přístup!\n Token získáte na adrese https://login.facebook.com/code_gen.php?api_key=" . APPID . "&v=1.0")));
        } else {
            if (empty($user['CmsUser']['fcb_session_key'])) {
                $fb = new FacebookRestClient(APPID, APPSECRET);
                $result = $fb->call_method('facebook.auth.getSession', array('auth_token' => $user['CmsUser']['fcb_token'], 'generate_session_secret' => true));
                //$result = $fb->call_method('facebook.auth.getSession', array('auth_token' => 'PDPDL0', 'generate_session_secret' => true,'host_url'=>'atep.jobspartners.cz'));
                $this->CmsUser->save(
                    array(
                        'id' => 1,
                        'fcb_session_key' => $result['session_key']
                    )
                );
                $user['CmsUser']['fcb_session_key'] = $result['session_key'];
            }
        }

        //pr($user);


        try {
            $facebook = new Facebook(APPID, APPSECRET);
            $facebook->api_client->session_key = $user['CmsUser']['fcb_session_key'];
            $message = $this->data['Fcb']['text'];
            if ($facebook->api_client->stream_publish($message, null, null, 142896262417205))
                die(json_encode(array('result' => true)));
            else {
                die(json_encode(array('result' => false, 'message' => "Chyba: Příspěvek nemohl být přidán")));
            }
        } catch (Exception $e) {
            die(json_encode(array('result' => false, 'message' => "Chyba: " . $e)));
        }
    }

    function fcb()
    {
        $okres = $this->get_list('Countrie');
        $data = $this->params['data']['CompanyOrderItem'];

        $this->loadModel('CmsUser');
        $kontakt = $this->CmsUser->read(array('name', 'telefon'), $data['contact_id']);

        $string = array();
        if (!empty($data['name'])) $string[] = 'Pozice: ' . $data['name'];
       // if (!empty($data['start_place'])) $string[] = 'Pracoviště: ' . $data['start_place'];
        //if (!empty($data['education'])) $string[] = 'Požadované vzdělání: ' . $data['education'];
        if (!empty($data['certification'])) $string[] = 'Certifikát: ' . $data['certification'];
        if (!empty($data['okres_id'])) $string[] = 'Okres: ' . $okres[$data['okres_id']];

        $koo_contact = false;
        if (!empty($data['cont1_name'])){
            $koo = 'Koordinátor: ' . $data['cont1_name'] . ' ' . $this->data['CompanyOrderTemplate']['cont1_tel'];
            if(trim($this->data['CompanyOrderTemplate']['cont1_tel']) != ''){
                $koo_contact = true;
            }
        }
        if (!empty($data['contact_id']) && isset($kontakt)){
            $koo = 'Koordinátor: ' . $kontakt['CmsUser']['name'] . ' ' . $kontakt['CmsUser']['telefon'];
            if(trim($kontakt['CmsUser']['telefon']) != ''){
                $koo_contact = true;
            }
        }

        if (isset($koo)){
            $string[] = $koo;
        }
        $this->set('koo_contact', $koo_contact);
        $this->set('fcb_text', implode("\n", $string));
        $this->render('../nabor_orders/fcb');
    }

    function getAppId()
    {

        return $this->FcbAppId;
    }

    function fcb_logout()
    {
        Configure::write('debug', 1);
        $this->Facebook->start(array(
            'appId' => APPID,
            'secret' => APPSECRET,
        ));

        $this->FcbAppId = $this->Facebook->getAppId();

        setcookie('fbs_' . $this->getAppId(), '', time() - 100, '/', $_SERVER["SERVER_NAME"]);
        unset($_SESSION['fb_' . $this->getAppId() . '_code']);
        unset($_SESSION['fb_' . $this->getAppId() . '_access_token']);
        unset($_SESSION['fb_' . $this->getAppId() . '_user_id']);
        unset($_SESSION['fb_' . $this->getAppId() . '_state']);
        pr($_SESSION);
        die();
    }

    function fcb_add2()
    {

        Configure::write('debug', 1);

        $this->Facebook->start(array(
            'appId' => APPID,
            'secret' => APPSECRET,
        ));

        // Get User ID
        $user = $this->Facebook->getUser();

        if ($user) {
            $this->set('logout_url', $this->Facebook->getLogoutUrl());
        } else {
            $loginUrl = $this->Facebook->getLoginUrl(array('scope' => 'publish_stream'));
        }

        pr($user);

        if ($user) {
            $know = false;
            if ($know == false) {
                $url = "https://graph.facebook.com/oauth/authorize?client_id=117861415038761&scope=offline_access,publish_stream,manage_pages&redirect_uri=http://atep.jobspartners.cz/nabor_orders/fcb_add/";

                echo $code_content = file_get_contents($url);
                die('sss');
                //header("Location: " . $url);
            } else if (isset($this->params['url']['code'])) {
                $url = "https://graph.facebook.com/oauth/access_token?client_id=" . APPID . "&redirect_uri=http://atep.jobspartners.cz/nabor_orders/fcb_add/&client_secret=" . APPSECRET . "&code=" . $this->params['url']['code'];
            }

            if ($know == true) {
                $accounts =

                $msg_body = array(
                    'message' => $this->data['Fcb']['text']
                );

                $post_url = '/142896262417205/feed';
                try {
                    $postResult = $this->Facebook->api($post_url, 'post', $msg_body);
                } catch (FacebookApiException $e) {
                    echo $e->getMessage();
                }


                pr($postResult);

                die();
            }
        }
        die('render');
    }

    function fcb_addd()
    {
        Configure::write('debug', 1);
        pr($this->params);
        die();
    }
}

?>