<?php
Configure::write('debug',1);
class EbCompaniesController extends AppController {
	var $name = 'EbCompanies';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Email','Upload');
	var $uses = array('EbCompany');
	var $renderSetting = array(
		'SQLfields' =>'*',
		 'bindModel' => array(
            'belongsTo'=>array(
                'Nakupce'=>array(
                    'className'=>'CmsUser',
                    'foreignKey'=>'buyer_id'
                ),
                'Vedouci'=>array(
                    'className'=>'CmsUser',
                    'foreignKey'=>'master_id'
                ),
                'Rozpoctar'=>array(
                    'className'=>'CmsUser',
                    'foreignKey'=>'rozpoctar_id'
                ),
                'Obchodnik'=>array(
                    'className'=>'CmsUser',
                    'foreignKey'=>'obchodnik_id'
                ),
                'Stavbyvedouci'=>array(
                    'className'=>'CmsUser',
                    'foreignKey'=>'stavbyvedouci_id'
                ),
                'Mistr'=>array(
                    'className'=>'CmsUser',
                    'foreignKey'=>'mistr_id'
                )
            )
            //'joinSpec' => array(
            //     'AtProject' => array('className'=>'AtProject','foreignKey' => 'at_project_id')
		    //)
         ),
		'controller'=> 'eb_companies',
		'page_caption'=>'Seznam zakázek - estor building',
		'sortBy'=>'EbCompany.name.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add'
		),
		'filtration' => array(
            //'EbCompany-project_id'	=>	'select|Projekt|project_list',		 	
		),
		'items' => array(
			'id'					=>	'ID|EbCompany|id|text|',
			'ico'					=>	'P.Č|EbCompany|pc|text|',
            'name'					=>	'Jméno investora|EbCompany|name|text|',
            'order_name'			=>	'Název zakázky|EbCompany|order_name|text|',
            'mesto'					=>	'Místo zakázky|EbCompany|mesto|text|',
            'pr_financni_objem'		=>	'Objem|EbCompany|pr_financni_objem|text|',
            'pr_pred_termin_ukonceni'	=>	'Předp.termín dokončení|EbCompany|pr_pred_termin_ukonceni|date|',
            'nakupce'	=>	'Nákupce|Nakupce|name|text|',
            'vedouci'	=>	'Vedoucí výroby|Vedouci|name|text|',
            'rozpoctar'	=>	'Rozpočtář|Rozpoctar|name|text|',
            'obchodnik'	=>	'Obchodník|Obchodnik|name|text|',
            'stavbyvedouci'	=>	'Stavbyvedoucí|Stavbyvedouci|name|text|',
            'mistr'	=>	'Mistr|Mistr|name|text|',
            'stav'		=>	'Stav|EbCompany|stav_id|var|eb_stav_list',
			//'manager'	   	        =>	'Správce|Manager|name|text|', 
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',	
            'reports'	=>	'aktivity|Aktivity|aktivity',			
			'rozpocet'	=>	'rozpocet|Nákladový rozpočet|rozpocet',			
			'slep_roz'	=>	'slepy_rozpocet|Hlášení stavbyvedoucího|slepy_rozpocet',			
			'vynosovy_rozpocet'	=>	'vynosovy_rozpocet|Výnosový rozpočet|vynosovy_rozpocet',
            'delete'	=>	'trash|Odstranit položku|trash',
		),
		'domwin_setting' => array(
			'sizes' 		=> '[800,900]',
			'scrollbars'	=> true,
			'languages'		=> true,
			'defined_lang'	=> "['cz','en','de']",
		),
        'only_his'=>array(
            'this_table'=>true,
            'where_col'=>array('buyer_id','master_id','rozpoctar_id','obchodnik_id','stavbyvedouci_id','mistr_id'),    
            'where_join'=>'OR',
        ),
        'only_his_posibility'=>array('buyer_id','master_id','rozpoctar_id','obchodnik_id','stavbyvedouci_id','mistr_id')
	);
	



	/**
 	* Returns a view, if not AJAX, load data for basic view. 
 	*
	* @param none
 	* @return view
 	* @access public
	**/
	function index(){
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'#','Nastavení firem'=>'#'));

            // set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox','wysiwyg_old/wswg'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox','../js/wysiwyg_old/wswg'));
			
			
			$this->render('../system/index');
		}
	}

    function remove_worker($id = null){
        if (empty($this->data)){
            $this->set('id',$id);
            $this->render('domwin/remove');
        } else {
            $this->loadModel('ConnectionEbcomClient');
            $this->data['ConnectionEbcomClient']['end_when'] = date('Y-m-d H:i:s');
            $this->data['ConnectionEbcomClient']['end_who'] = $this->logged_user['CmsUser']['id'];

            if($this->ConnectionEbcomClient->save($this->data)){
                die(json_encode(array('result'=>true)));
            } else {
                die(json_encode(array('result'=>false)));
            }
        }
    }

    function add_worker($eb_company_id = null, $client_id = null ){
        if (empty($this->data)){
            $this->set('eb_company_id',$eb_company_id);
            $this->set('client_id',$client_id);
            $this->render('domwin/add');
        } else {
            $this->loadModel('ConnectionEbcomClient');
            $this->data['ConnectionEbcomClient']['start_when'] = date('Y-m-d H:i:s');
            $this->data['ConnectionEbcomClient']['start_who'] = $this->logged_user['CmsUser']['id'];
            $this->data['ConnectionEbcomClient']['stav'] = 2;

            if($this->ConnectionEbcomClient->save($this->data)){
                die(json_encode(array('result'=>true)));
            } else {
                die(json_encode(array('result'=>false)));
            }
        }
    }

	/**
 	* Returns a view if empty this->data, ales JSON result of save
 	*
	* @param $id The number of ID column
 	* @return view / JSON
 	* @access public
	**/
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
		    //tab setting z permision
            $permision = $this->logged_user['CmsGroup']['permission'][$this->renderSetting['controller']];
			$tab = array();
            foreach($permision as $key=>$val){
                if(strpos($key,'tab_') !== false){
                    $_key = substr($key,4);
                    $tab[$_key] = $val;
                }
            }
            $this->set('tab_setting',$tab);
            $this->set('allow_go_to_realization',$r = (isset($permision['go_to_realization'])?$permision['go_to_realization']:0));
		    if ($id != null){
				$this->data = $this->EbCompany->read(null,$id);
				
				// load province list
				$this->loadModel('Province'); 
				$this->set('province_list',$this->Province->find('list',array('conditions'=>array('stat_id'=>$this->data['EbCompany']['stat_id']))));
				unset($this->Province );
                
               	// load country list
                if (isset($this->data['EbCompany']['province_id']) && !empty($this->data['EbCompany']['province_id'])){
				    $this->loadModel('Countrie'); 
				    $this->set('country_list',$this->Countrie->find('list',array('conditions'=>array('province_id'=>$this->data['EbCompany']['province_id']))));
				    unset($this->Countrie);
                }
            
                $this->set('company_id',$id);

                

                // natahnuti existujicich zamestnancu
                $this->loadModel('ConnectionEbcomClient');
                $this->ConnectionEbcomClient->bindModel(array(
                    'belongsTo' => array(
                        'Client',
                        'CmsUser'=>array(
                            'foreignKey' => 'start_who'
                        )
                    )
                ));
                $zams = $this->ConnectionEbcomClient->find(
                    'all',
                    array(
                        'conditions' => array(
                            'ConnectionEbcomClient.eb_company_id' => $id,
                            'ConnectionEbcomClient.stav' => 2,
                            'ConnectionEbcomClient.end' => '0000-00-00'
                        ),
                        'fields' => array(
                            'Client.name',
                            'ConnectionEbcomClient.*',
                            'CmsUser.name'
                        )
                    )
                );
                
                $_conditions = array('client_type'=>2);
                $exist_zam = Set::extract($zams,'/ConnectionEbcomClient/client_id');
                if(!empty($exist_zam))
                    $_conditions += array('id NOT IN('.join(',',$exist_zam).')');
                // nacteni klientu pro zamestnani
                $this->set('clients',$this->get_list('Client',$_conditions));
                $this->set('zam_list',$zams);
                
                //popis zakazky
                $this->loadModel('EbCompanyOrderItem');
                $order_items = $this->EbCompanyOrderItem->find('all',array(
                        'conditions' => array(
                            'EbCompanyOrderItem.eb_company_id' => $id,
                            'EbCompanyOrderItem.kos' => 0
                        )
                ));
                $this->set('order_items',$order_items);
                
                //vyhodnoceni
                $this->loadModel('EbCompanyEvaluation');
                $evaluation_items = $this->EbCompanyEvaluation->find('all',array(
                        'conditions' => array(
                            'EbCompanyEvaluation.eb_company_id' => $id,
                            'EbCompanyEvaluation.kos' => 0
                        )
                ));
                $this->set('evaluation_items',$evaluation_items);

			} else {
				$this->loadModel('Province');
				$this->set('province_list',$this->Province->find('list',array('conditions'=>array('stat_id'=>1))));		
                unset($this->Province );	
			} 
            // load stat list
			$this->set('company_stat_list',$this->get_list('SettingStat'));

            $this->set('nakupce_list',$this->get_list('CmsUser',array('cms_group_id'=>66)));
            $this->set('vedouci_list',$this->get_list('CmsUser',array('cms_group_id'=>21)));
            $this->set('rozpoctar_list',$this->get_list('CmsUser',array('cms_group_id'=>65)));
            $this->set('obchodnik_list',$this->get_list('CmsUser'));
            $this->set('stavbyvedouci_list',$this->get_list('CmsUser',array('cms_group_id'=>67)));
            $this->set('mistr_list',$this->get_list('CmsUser',array('cms_group_id'=>34)));
            
			$this->render('edit');
		} else {
		    if($this->data['EbCompany']['id'] != '')
		      $old_data = $this->EbCompany->read(null,$this->data['EbCompany']['id']);
            
            if($this->data['EbCompany']['name'] == '')
                die(json_encode(array('result'=>false,'message'=>'CHYBA!!!')));
            
			$this->EbCompany->save($this->data);
            
            // load province list
			$this->loadModel('Province'); 
			$province_list = $this->Province->find('list',array('conditions'=>array('stat_id'=>$this->data['EbCompany']['stat_id'])));
			unset($this->Province);
            
           	// load country list
            if (isset($this->data['EbCompany']['province_id']) && !empty($this->data['EbCompany']['province_id'])){
			    $this->loadModel('Countrie'); 
			    $country_list = $this->Countrie->find('list',array('conditions'=>array('province_id'=>$this->data['EbCompany']['province_id'])));
			    unset($this->Countrie);
            }
            
            $company_stat_list = $this->get_list('SettingStat');
            
            $replace_list = array(
				'##stat##' 	=>$company_stat_list[$this->data['EbCompany']['stat_id']],
                '##kraj##' 	=>$province_list[$this->data['EbCompany']['province_id']],
                '##okres##' 	=>$country_list[$this->data['EbCompany']['country_id']],
                '##CmsUser.name##' 	=>$this->logged_user['CmsUser']['name']
			);
            foreach($this->data['EbCompany'] as $item=>$value){
                $replace_list['##EbCompany.'.$item.'##'] = $value;
            }
            
            //$mails = array(0=>$this->logged_user['CmsUser']['email']);
            $mails = array();
            $this->loadModel('CmsUser');
            $cms_mails = $this->CmsUser->find('list',array('fields'=>array('id','email')));
            $check_columns = array('buyer_id','master_id','rozpoctar_id','obchodnik_id','stavbyvedouci_id','mistr_id');
            if(isset($old_data) && !empty($old_data))
                foreach($check_columns as $column){
                    if($old_data['EbCompany'][$column] != $this->data['EbCompany'][$column] && $this->data['EbCompany'][$column] != '')
                        $mails[] = $cms_mails[$this->data['EbCompany'][$column]];
                }
            
            if($this->data['EbCompany']['id'] == '')//nova zakazka
			  $this->Email->send_from_template_new(48,array(),$replace_list);
              
            if(!empty($mails))//odeslani info za byl nekdo prirazen k zakazce
              $this->Email->send_from_template_new(47,$mails,$replace_list);
            
			die();
		}
	}
    
   
   	/**
 	* Seznam priloh
 	*
	* @param $company_id
 	* @return view
 	* @access public
	**/
	function attachs($type,$company_id){
		$this->autoLayout = false;
		$this->loadModel('EbCompanyAttachment'); 

		
		$this->EbCompanyAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->EbCompanyAttachment->findAll(array(
            'EbCompanyAttachment.eb_company_id'=>$company_id,
            'EbCompanyAttachment.kos'=>0,
            'type'=>$type
        )));
		$this->set('link',$type.'/'.$company_id);
        $this->set('type',$type);
		unset($this->EbCompanyAttachment);
		$this->render('attachs/index');
		
	}
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($type = null,$company_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('EbCompanyAttachment');
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType');
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
			$this->data['EbCompanyAttachment']['eb_company_id'] = $company_id;
            $this->data['EbCompanyAttachment']['type'] = $type;
			if ($id != null){
				$this->data = $this->EbCompanyAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->data["EbCompanyAttachment"]["cms_user_id"]=$this->logged_user['CmsUser']['id'];
			$this->EbCompanyAttachment->save($this->data);
			$this->attachs($this->data['EbCompanyAttachment']['type'],$this->data['EbCompanyAttachment']['eb_company_id']);
		}
		unset($this->EbCompanyAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($type,$company_id, $id){
		$this->loadModel('EbCompanyAttachment');
		$this->EbCompanyAttachment->save(array('EbCompanyAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($type,$company_id);
		unset($this->EbCompanyAttachment);
	}
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('../atep/uploaded/'.$file);
		$cesta = "http://".$_SERVER['HTTP_HOST']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}
    
    
     /**
     * Zobrazeni polozek rozpoctu pro danou zakazku
     * @param int $rozpocet_id 
	 * @return VIEW
     */
    function rozpocet($eb_company_id){
        $this->loadModel('RozpocetItem');
        $this->RozpocetItem->bindModel(array(
            'joinSpec'=>array('RozpocetItemCategory'=>array(
                'className'=>'RozpocetItemCategory','foreignKey'=>'RozpocetItem.rozpocet_item_category_id',
                'primaryKey'=>'RozpocetItemCategory.parent_id'
            ))
        ));
        $this->set(
            'items',
            $this->RozpocetItem->find(
                'all',
                array(
                    'fields'=>array(
                        'RozpocetItemCategory.name',
                        'RozpocetItem.code',
                        'RozpocetItem.rozpocet_item_category_id',
                        'RozpocetItem.code_item',
                        'RozpocetItem.name',
                        'RozpocetItem.mn',
                        'RozpocetItem.mj',
                        'RozpocetItem.jc',
                        'RozpocetItem.cena',
                        'RozpocetItem.id',
                        'ROUND((SELECT SUM(mnozstvi) FROM wapis__rozpocet_blind_items WHERE rozpocet_item_id = RozpocetItem.id),2) as splnene_hodnota',
                        '(ROUND((SELECT SUM(mnozstvi) FROM wapis__rozpocet_blind_items WHERE rozpocet_item_id = RozpocetItem.id)/mn*100,2)) as procenta'
                    ),
                    'conditions'=>array(
                        'RozpocetItem.kos'=>0, 
                        'RozpocetItem.eb_company_id'=>$eb_company_id
                    )
                )
            )
        );
        $this->set('eb_company_id',$eb_company_id);
        $this->render('rozpocet/index');
    }
    
    
    function rozpocet_delete($item_id = null) {
        if($item_id == null)
            die(json_encode(array('result'=>false, 'message'=>'Prazdne ID!!!')));
        
           $this->loadModel('RozpocetItem');
           $this->RozpocetItem->id = $item_id;
        if($this->RozpocetItem->saveField('kos',1))
            die(json_encode(array('result'=>true)));
        else
           die(json_encode(array('result'=>false, 'message'=>"Nepovedlo se smazat zaznam s id:".$item_id." z DB")));
            
           
    }    
    
    /**
     * Editace polozek rozpoctu pro danou zakazku
     * @param int $order_id [optimal] 
     * @param int $item_id [optimal] 
	 * @return VIEW|JSON
     */
    function rozpocet_edit($eb_company_id = null, $item_id = null) {
        $this->loadModel('RozpocetItem');
        if (empty($this->data)){
            if ($item_id != null){
                $this->data = $this->RozpocetItem->read(null, $item_id);
            } else {
                $this->data['RozpocetItem']['eb_company_id'] = $eb_company_id;
                $this->data['RozpocetItem']['kurz'] = '30.12';  
            }
            
            $this->loadModel('RozpocetItemCategory');
            $this->set('category_list',$this->RozpocetItemCategory->find('list',array('conditions'=>array('kos'=>0),'fields'=>array('parent_id','name'))));
            unset($this->RozpocetItemCategory);
            
            $this->render('rozpocet/edit');
        } else {
            if (empty($this->data['RozpocetItem']['id'])){
                $this->data['RozpocetItem']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            }
            if($this->RozpocetItem->save($this->data))
                die(json_encode(array('result'=>true)));
            else
                die(json_encode(array('result'=>false, 'message'=>"Nepovedlo se ulozit zaznam do DB")));
        }
	}

     /**
     * Zobrazeni slepych polozek rozpoctu pro danou zakazku
     * @param int $rozpocet_id 
	 * @return VIEW
     */
    function slepy_rozpocet($eb_company_id) {
        $this->loadModel('RozpocetItem');
        $this->RozpocetItem->bindModel(array(
            'hasOne'=>array(
                'RozpocetBlindItem' => array(
                    'conditions'=>array(
                         'mesic'=>date("m"), 
                         'rok'=>date("Y")
                    )
                )
            ),
            'joinSpec'=>array(
                'ConnectionEmployeeUkol'=>array(
                    'className'=>'ConnectionEmployeeUkol',
                    'primaryKey'=>'ConnectionEmployeeUkol.rozpocet_item_id',
                    'foreignKey'=>'RozpocetItem.id',
                    'conditions'=>array('ConnectionEmployeeUkol.rok'=>date("Y"),'ConnectionEmployeeUkol.mesic'=>date("m"))
                )
            )
        ));
        
        $this->set('items',$i = $this->RozpocetItem->find('all',array(
            'conditions'=>array('RozpocetItem.kos'=>0, 'RozpocetItem.eb_company_id'=>$eb_company_id),
            'fields'=>array('RozpocetItem.*','RozpocetBlindItem.*','SUM(ConnectionEmployeeUkol.ks) as mj_dochazka'),
            'group'=>'RozpocetItem.id'
        )));

        $this->set('eb_company_id',$eb_company_id);
        $this->set('mesic',date("m"));
        $this->set('rok',date("Y"));
        $this->render('slepy_rozpocet/index');
	}

     /**
     * Zobrazeni slepeho rozpoctu pro dany mesic a rok s moznosti editace mnozstvi pro jednotlive polozky
     * @param int $order_id 
     * @param int $month 
     * @param int $year 
	 * @return VIEW 
     */
    function slepy_rozpocet_items($eb_company_id, $month, $year){
        $this->loadModel('RozpocetItem');
        $this->RozpocetItem->bindModel(array(
            'hasOne'=>array(
                'RozpocetBlindItem' => array(
                    'conditions'=>array(
                         'mesic'=>$month, 
                         'rok'=>$year
                    )
                )
            ),
            'joinSpec'=>array(
                'ConnectionEmployeeUkol'=>array(
                    'className'=>'ConnectionEmployeeUkol',
                    'primaryKey'=>'ConnectionEmployeeUkol.rozpocet_item_id',
                    'foreignKey'=>'RozpocetItem.id',
                    'conditions'=>array('ConnectionEmployeeUkol.rok'=>$year,'ConnectionEmployeeUkol.mesic'=>$month)
   
                )
            )
        ));
        
        $this->set('items',$i = $this->RozpocetItem->find('all',array(
            'conditions'=>array('RozpocetItem.kos'=>0, 'RozpocetItem.eb_company_id'=>$eb_company_id),
            'fields'=>array('RozpocetItem.*','RozpocetBlindItem.*','SUM(ConnectionEmployeeUkol.ks) as mj_dochazka'),
            'group'=>'RozpocetItem.id'
        )));
        
        $this->set('eb_company_id',$eb_company_id);
        $this->set('mesic',$month);
        $this->set('rok',$year);
        
        $this->render('slepy_rozpocet/items');
    }
    
    /**
     * Ulozeni dat slepehno rozpoctu k danemu mesici a roku 
     * @return JSON 
     */
    function save_blind_data(){
        $this->loadModel('RozpocetBlindItem');
        foreach($this->data['RozpocetBlindItem'] as $row){
            $this->RozpocetBlindItem->save($row);
            $this->RozpocetBlindItem->id = null;
        }
        die(json_encode(array('result'=>true)));
    }
    
    /**
     * Tisk slepeho rozpoctu pro dany mesic a rok
     * @param int $order_id 
     * @param int $month 
     * @param int $year 
	 * @return VIEW 
     */
    function blind_rozpocet_print($eb_company_id, $month, $year){
        //$this->CompanyOrder->bindModel(array('belongsTo'=>array('Company')));
        $this->set('order_info', $this->EbCompany->read(null,$eb_company_id));
        $this->loadModel('RozpocetItem');
        $this->RozpocetItem->bindModel(array('hasOne'=>array(
            'RozpocetBlindItem' => array(
                'conditions'=>array(
                     'mesic'=>$month, 
                     'rok'=>$year
                )
            )
        )));
        
        $this->set('items',$this->RozpocetItem->find('all',array('conditions'=>array('RozpocetItem.kos'=>0, 'RozpocetItem.eb_company_id'=>$eb_company_id))));
        $this->set('eb_company_id',$eb_company_id);
        $this->set('mesic',$month);
        $this->set('rok',$year);
        
        $this->render('slepy_rozpocet/print','print');
    }
    
    
    /**
     * Zobrazeni polozek vynosovy_rozpocet pro danou zakazku
     * @param int $rozpocet_id 
	 * @return VIEW
     */
    function vynosovy_rozpocet($eb_company_id){
        $this->loadModel('RozpocetVynosovyItem');
        $this->RozpocetVynosovyItem->bindModel(array(
            'joinSpec'=>array('RozpocetItemCategory'=>array(
                'className'=>'RozpocetItemCategory','foreignKey'=>'RozpocetVynosovyItem.rozpocet_item_category_id',
                'primaryKey'=>'RozpocetItemCategory.parent_id'
            ))
        ));
        $this->set(
            'items',
            $this->RozpocetVynosovyItem->find(
                'all',
                array(
                    'fields'=>array(
                        'RozpocetItemCategory.name',
                        'RozpocetVynosovyItem.code',
                        'RozpocetVynosovyItem.rozpocet_item_category_id',
                        'RozpocetVynosovyItem.code_item',
                        'RozpocetVynosovyItem.name',
                        'RozpocetVynosovyItem.mn',
                        'RozpocetVynosovyItem.mj',
                        'RozpocetVynosovyItem.jc',
                        'RozpocetVynosovyItem.cena',
                        'RozpocetVynosovyItem.id'
                    ),
                    'conditions'=>array(
                        'RozpocetVynosovyItem.kos'=>0, 
                        'RozpocetVynosovyItem.eb_company_id'=>$eb_company_id
                    )
                )
            )
        );
        $this->set('eb_company_id',$eb_company_id);
        $this->render('vynosovy_rozpocet/index');
    }
    
    
    function vynosovy_rozpocet_delete($item_id = null) {
        if($item_id == null)
            die(json_encode(array('result'=>false, 'message'=>'Prazdne ID!!!')));
        
           $this->loadModel('RozpocetVynosovyItem');
           $this->RozpocetVynosovyItem->id = $item_id;
        if($this->RozpocetVynosovyItem->saveField('kos',1))
            die(json_encode(array('result'=>true)));
        else
           die(json_encode(array('result'=>false, 'message'=>"Nepovedlo se smazat zaznam s id:".$item_id." z DB")));
            
           
    }    
    
    /**
     * Editace polozek rozpoctu pro danou zakazku
     * @param int $order_id [optimal] 
     * @param int $item_id [optimal] 
	 * @return VIEW|JSON
     */
    function vynosovy_rozpocet_edit($eb_company_id = null, $item_id = null) {
        $this->loadModel('RozpocetVynosovyItem');
        if (empty($this->data)){
            if ($item_id != null){
                $this->data = $this->RozpocetVynosovyItem->read(null, $item_id);
            } else {
                $this->data['RozpocetVynosovyItem']['eb_company_id'] = $eb_company_id;
                $this->data['RozpocetVynosovyItem']['kurz'] = '30.12';  
            }
            
            $this->loadModel('RozpocetItemCategory');
            $this->set('category_list',$this->RozpocetItemCategory->find('list',array('conditions'=>array('kos'=>0),'fields'=>array('parent_id','name'))));
            unset($this->RozpocetItemCategory);
            
            $this->render('vynosovy_rozpocet/edit');
        } else {
            if (empty($this->data['RozpocetVynosovyItem']['id'])){
                $this->data['RozpocetVynosovyItem']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            }
            if($this->RozpocetVynosovyItem->save($this->data))
                die(json_encode(array('result'=>true)));
            else
                die(json_encode(array('result'=>false, 'message'=>"Nepovedlo se ulozit zaznam do DB")));
        }
	}
    
    
      	/**
 	* Zobrazi seznam aktivit
 	*
	* @param int $order_id
 	* @return view
 	* @access public
	**/
	
	function aktivity($eb_company_id){
		$this->autoLayout = false;
		$this->loadModel('EbCompanyActivity'); 
        $this->EbCompanyActivity->bindModel(array('belongsTo'=>array(
            'CmsUser',
            'EbCompany'
        )));
		$this->set('activity_list', $this->EbCompanyActivity->find('all',array(
            'conditions'=>array(
                'EbCompanyActivity.eb_company_id'=>$eb_company_id,
                'EbCompanyActivity.kos'=>0
            ), 
            'order'=>'EbCompanyActivity.id DESC'
        )));
		$this->set('eb_company_id',$eb_company_id);
		unset($this->EbCompanyActivity);
		$this->render('aktivity/index');
	}
	
	/**
 	* Editace aktivit
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function aktivity_edit($eb_company_id = null, $id = null){
		$this->autoLayout = false;
		// START nacteni  zakladnich modelu pro editaci
		$this->loadModel('EbCompanyActivity');
		// END nacteni  zakladnich modelu pro editaci
		if (empty($this->data)){			
		
			if ($id != null){
				$this->data = $this->EbCompanyActivity->read(null,$id);
			} else 
                	$this->data['EbCompanyActivity']['eb_company_id'] = $eb_company_id;
			$this->render('aktivity/edit');
		} else {
			// START pridani cms_user_id pokud se vytvari aktivita
			if (empty($this->data['EbCompanyActivity']['id'])) 	$this->data['EbCompanyActivity']['cms_user_id'] 	= $this->logged_user['CmsUser']['id'];
			// END pridani cms_user_id pokud se vytvari aktivita 
			// ulozeni aktivity
			$this->EbCompanyActivity->save($this->data);				
			// reload seznamu aktivit
			$this->aktivity($this->data['EbCompanyActivity']['eb_company_id']);
		}
		unset($this->EbCompanyActivity);
	}
	
    
   /**
 	* Prenos aktivity do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function aktivity_trash($eb_company_id, $id){
		$this->loadModel('EbCompanyActivity'); 
		$this->EbCompanyActivity->save(array('EbCompanyActivity'=>array('kos'=>1,'id'=>$id)));
		$this->aktivity($eb_company_id);
		unset($this->EbCompanyActivity);
	}
    
    
    function import($eb_company_id, $model = null) {
	    if($model == null){
	       $model = 'RozpocetItem';
	    }
        
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
            $path = './uploaded/'.$this->Upload->get('outputFilename');
            //$path = './uploaded/karta-projektu-ostrava_upraveny.csv';
            $this->loadModel($model);
            $row = 0;
            $imported = 0;
            if (($handle = fopen($path, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    if ($row > 1 && $data[0] != ''){ // dva radky skip
                        $to_save = array(
                           'rozpocet_item_category_id'       => $data[0],  	
                           'eb_company_id'          => $eb_company_id,  	
    	                   'cms_user_id'       => $this->logged_user['CmsUser']['id'],	
    	                   'code'              => $data[2],
                           'code_item'         => $data[3],  	
    	                   'name'              => iconv('windows-1250','utf-8',$data[4]), 	
    	                   'mj'                => $data[5],
    	                   'mn'                => strtr($data[6],array(','=>'.')),
    	                   'jc'                => strtr($data[7],array(','=>'.')),
    	                   'cena'              => strtr($data[8],array(','=>'.'))
                        );   
                        
                        try{
                           if (@$this->{$model}->save($to_save)){                        
                                $this->{$model}->id = null;
                                $imported ++;
                           } 
                        } catch (Exception $e) {
                           // echo 'Caught exception: ',  $e->getMessage(), "\n";
                        }
                    }

                 $row++;
                   
                }
                fclose($handle);
                die(json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true)));
            }
		} else 
			die(json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message'))));		
	
	} 
    
    
    function edit_order_item($eb_company_id = null, $id = null,$render = 'edit'){
        $this->loadModel('EbCompanyOrderItem');  
        if(empty($this->data)){
            if($id != null){
                $this->data = $this->EbCompanyOrderItem->read(null,$id);
            }
            else{
                if($eb_company_id == null)
                    die('Položky lze přidávat pouze po uložení karty!');
                
                $this->data['EbCompanyOrderItem']['eb_company_id'] = $eb_company_id;
            }
            
            $this->render('tabs/order_item/'.$render);
        }else{//save
            if($this->data['EbCompanyOrderItem']['id'] == '')
              $this->data['EbCompanyOrderItem']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            
            $this->EbCompanyOrderItem->save($this->data);
            
            //pocet nezaskrtnutych polozek
            $counts = $this->EbCompanyOrderItem->find('count',array(
                'conditions'=>array(
                    'eb_company_id'=>$this->data['EbCompanyOrderItem']['eb_company_id'],
                    'kos'=>0,
                    'stav'=>0
                )
            ));
            $goto = 3;
            /**
             * Pokud byli všechny položky zaškrtnuty jako relizovane, zakazka se uklada do stavu 4
             * a po refreshi se ihned přechází na domtab Vyhodnoceni
             */
            if($counts == 0){
                $goto = 4;
                $this->EbCompany->id = $this->data['EbCompanyOrderItem']['eb_company_id'];
                $this->EbCompany->saveField('stav_id',4);
            }
            
            die(json_encode(array('goto'=>$goto)));
        }
    }
    
    
    function edit_evaluation($type = null,$eb_company_id = null, $id = null){
        $this->loadModel('EbCompanyEvaluation');  
        if(empty($this->data)){
            if($id != null){
                $this->data = $this->EbCompanyEvaluation->read(null,$id);
            }
            else{
                if($eb_company_id == null)
                    die('Položky lze přidávat pouze po uložení karty!');
                
                $this->data['EbCompanyEvaluation']['type'] = $type;
                $this->data['EbCompanyEvaluation']['eb_company_id'] = $eb_company_id;
            }
            
            $this->render('tabs/evaluatins/edit');
        }else{//save
            if($this->data['EbCompanyEvaluation']['id'] == '')
              $this->data['EbCompanyEvaluation']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            
            $this->EbCompanyEvaluation->save($this->data);
        }
    }
       
}
?>