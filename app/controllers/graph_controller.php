<?php
Configure::write('debug',1);
class GraphController extends AppController {
	var $name = 'Graph';
	var $helpers = array('htmlExt');
	var $components = array('RequestHandler');
	var $uses = array('ClientWorkingHour');
    //var $modul_load_view = false; //defaultne modul nenacita
	 
	function index($manager_realizace_id = null){
	   
        $mr_conditions = null;
        if($manager_realizace_id != null)
            $mr_conditions = 'Company.manazer_realizace_id = '.$manager_realizace_id.' AND ';
		
        $this->ClientWorkingHour->bindModel(array('belongsTo'=>array('Company')),false);
        $this->set('graph_data_working_hour',$this->ClientWorkingHour->find(
            'all',
            array(
                'fields' => array(
                    'sum(celkem_hodin) as pocet',
                    'year', 
                    'month'
                ),
                'conditions'=>$mr_conditions.' year is not null and (year > 2009 || (year = 2009 and month >= 09 )) and (year < '.date('Y').' || (year = '.date('Y').' AND month <= '.date('m').'))',
                'order'=>array('year','month'),
                'group'=>array('year','month')
            )
        ));
 

        $this->set('graph_data_working_clients',$this->ClientWorkingHour->find(
            'all',
            array(
                'fields' => array(
                    'count(DISTINCT client_id) as pocet',
                    'year', 
                    'month'
                ),
                'conditions'=>$mr_conditions.' year is not null and (year > 2009 || (year = 2009 and month >= 09 )) and (year < '.date('Y').' || (year = '.date('Y').' AND month <= '.date('m').'))',
                'order'=>array('year','month'),
                'group'=>array('year','month')
            )
        ));
        
        if ($this->RequestHandler->isAjax()){
			$this->render('items');
		}else{
		    $this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Graphs'=>'#'));

            $this->loadModel('CmsUser');
            $this->set('manager_realizace_list', $this->CmsUser->find('list', array('conditions'=>array('cms_group_id'=>18,'kos'=>0))));
    		  
		    $this->render('index');
		}
                
	}
}
?>