<?php
Configure::write('debug',1);
class ReportInvoiceItemsController extends AppController {
	var $name = 'ReportInvoiceItems';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Excel');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CompanyInvoiceItem');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('Company')),
		'permModel'=>'Company',
		'TypUserCompany'=>true,
		'permGroupId'=>true,
		'controller'=>'report_invoice_items',
		'SQLfields' =>'CompanyInvoiceItem.*,Company.*,
            (
                SELECT
                 SUM(salary_per_hour_p)
                FROM wapis__client_working_hours as ClientWorkingHour
                WHERE ClientWorkingHour.year = 2010 AND ClientWorkingHour.company_id = CompanyInvoiceItem.company_id  
                GROUP BY ClientWorkingHour.company_id,ClientWorkingHour.year
              ) as total_year,
              ROUND((pocet_hodin_firma - pocet_hodin_atep),2) as rozdil_hodin,
              IF(CompanyInvoiceItem.currency_id = 0,IF(Company.stat_id = 1,1,2),CompanyInvoiceItem.currency_id) as mena',
		'page_caption'=>'Fakturace podniku',
		'sortBy'=>'CompanyInvoiceItem.za_mesic.DESC',
		//'SQLcondition'=>array('CompanyMoneyValidity.platnost_do'=>'0000-00-00','CompanyInvoiceItem.schvaleno'=>0,'CompanyInvoiceItem.kos'=>0),
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Přidat novou fakturu|add',
			'excel'			=>	'Export|export_excel|Vyexportovat do excelu|add',
		),
		'filtration' => array(
        	'CompanyInvoiceItem-company_id'				=>	'select|Firma|company_list',
			'Company-self_manager_id'			=>	'select|SM|sm_list',
			'CompanyInvoiceItem-za_mesic'			=>	'date_month|Za měsíc|',
			'CompanyInvoiceItem-datum_uhrady-NOTNULL2'	=>	'checkbox|Uhrazeno|',
			
		),
		'items' => array(
			'id'				=>	'ID|CompanyInvoiceItem|id|text|',
			'company'			=>	'Firma|Company|name|text|',
		//	'ico'				=>	'IČO|Company|ico|text|',
			'za_mesic'			=>	'Za měsíc|CompanyInvoiceItem|za_mesic|date|Y-m',			
			'cena'	            		=>	'Částka ATEP|CompanyInvoiceItem|cena|money2|',
			'pocet_hodin_atep'		=>	'Hodiny ATEP|CompanyInvoiceItem|pocet_hodin_atep|text|',
			'pocet_hodin_firma'		=>	'Hodiny firma|CompanyInvoiceItem|pocet_hodin_firma|text|',
			'rozdil_hodin   '		=>	'Rozdil hodiny|0|rozdil_hodin|text|',
			'total_year'	    		=>	'Celkem hodin v roce 2010|0|total_year|text|',
			'bez_dph'			=>	'Bez DPH|CompanyInvoiceItem|cena_bez_dph|money2|',
			's_dph'				=>	'S DPH|CompanyInvoiceItem|spolu_s_dph|money2|',
			'mena'	         		=>	'Měna|0|mena|var|currency_list2',
			'datum_splatnosti'		=>	'Splatnost|CompanyInvoiceItem|datum_splatnosti|date|',
			'datum_uhrady'			=>	'Uhrazeno|CompanyInvoiceItem|datum_uhrady|date|',
			'po_splatnosti'			=>	'Po splatnosti|CompanyInvoiceItem|pocet_dni_po_splatnosti|pocet_dni_po_splatnosti|',
		
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
			'edit'		=>	'edit|Editovat položku|edit',	
            'trash'     =>	'trash|Smazat položku|trash'	
		)
	);
	
	function beforeFilter(){
		parent::beforeFilter();
		if (isset($_GET['excel'])){
			$this->renderSetting['sortShow'] = 99999;
		}
	}
	
	function export_excel(){
		$link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&excel=true';
		$this->redirect($link);
		exit;
	}
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Firmy'=>'#',$this->renderSetting['page_caption']=>'#'));
		
		$this->loadModel('CmsUser'); 	
		$this->loadModel('Company'); 	
		
		$this->set('sm_list',			$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0,'CmsUser.cms_group_id'=>2))));
		$this->set('company_list',		$this->Company->company_list());

		unset($this->CmsUser);
		unset($this->Company);
		
		if (isset($_GET['excel'])){
			$this->autoLayout = false;
			echo $this->render('../system/excel');
			die();
		}
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		echo $this->requestAction('companies/invoice_item_edit/-1/' . $id);
		die();
	}
	
	function load_company_information($company_id){
		$this->loadModel('Company'); $this->Company =& new Company();
		$this->Company->bindModel(array('belongsTo' => array(
			'SalesManager' 	=> array('className' => 'CmsUser', 'foreignKey' => 'self_manager_id')
		)));
		die(json_encode($this->Company->read(array('Company.ico','Company.stat_id','SalesManager.name'),$company_id)));
		
	}
    
    function load_company_atep_detail($company_id,$date){
        list($year,$month,$day) = explode('-',$date);

        
        $this->loadModel('ClientWorkingHour');
		$this->ClientWorkingHour->bindModel(array(
			   'belongsTo'=>array(
                    'ConnectionClientRequirement'=>array(
                        'conditions'=>array(
                            "((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '".$year."-".$month."'))",
		                  	'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "'.$year.'-'.$month.'") OR (ConnectionClientRequirement.to = "0000-00-00"))',
		                  	'type'=> 2
                        ),
                        'fields'=>array(
                            'ConnectionClientRequirement.id'
                        )
                    )
               )
        ));
	
		$item = $this->ClientWorkingHour->find('all',array(
				'conditions'=>array(
					'ClientWorkingHour.company_id'=>$company_id,
					'ClientWorkingHour.year'=>$year,
					'ClientWorkingHour.month'=>$month,
                    'ConnectionClientRequirement.id <>'=>0
				),
				'fields'=>array(
					'SUM(salary_per_hour_p) as total',
                    'ROUND(SUM(celkem_hodin),1) as sum_celkem_hodin',
				),
				'group' => 'ClientWorkingHour.company_id,ClientWorkingHour.year, ClientWorkingHour.month'
		));
        
        $item = array_shift($item);
        
        if($item)
		  die(json_encode(array('result'=>true,'amount'=>$item[0]['total'],'pocet_hodin'=>$item[0]['sum_celkem_hodin'])));
		else  
          die(json_encode(array('result'=>false)));
		
	}
	
}
?>