<?php
//Configure::write('debug',1);
class CampaignItemsTippersController extends AppController {
	var $name = 'CampaignItemsTippers';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput','Wysiwyg');
	var $components = array('ViewIndex','RequestHandler','Upload','Email');
	var $uses = array('CampaignItem');
	var $renderSetting = array(
		'SQLfields' => array('CampaignItem.id','CampaignItem.name','CampaignGroup.name','CampaignItem.stav','CampaignItem.pozice','CampaignItem.mail','CampaignItem.mobil','CampaignItem.telefon','Company.name','CampaignItem.created'),
		'controller'=> 'campaign_items_tippers',
		'bindModel'=>array('belongsTo'=>array('Company','CampaignGroup')),
		'page_caption'=>'Kampaně - Tipeři',
		'sortBy'=>'CampaignItem.name.ASC',
		'SQLcondition'=>array('CampaignItem.stav'=>1),
		'top_action' => array(
			// caption|url|description|permission
			'add_item'	=>	'Přidat|edit|Přidání nové kampaně|add',
			'group'		=>	'Kampaně|groups|Přidat kampaň|groups',
			'email'		=> 	'Odeslání Emailu|email_send|Odeslání hromadného emailu kampaně|email',
			'template'	=> 	'Šablony|template|Šablony emailu|email',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			'CampaignItem-name|'				=>	'text|Jméno|',
			'CampaignItem-city|'				=>	'text|Město|'
		),
		'items' => array(
			'id'			=>	'ID|CampaignItem|id|text|',
			'kampan'		=>	'Kampaň|CampaignGroup|name|text|',
			'company'		=>	'Společnost|Company|name|text|',
			'pozice'		=>	'Pozice|CampaignItem|pozice|text|',
			'mail'			=>	'Email|CampaignItem|mail|text|',
			'mobil'			=>	'Mobil|CampaignItem|mobil|text|',
			'telefon'		=>	'Telefon|CampaignItem|telefon|text|',
			'stav'			=>	'Stav|CampaignItem|stav|viewVars|campaign_item_stavs',
		
			//'updated'		=>	'Změněno|CampaignItem|updated|datetime|',
			'created'		=>	'Vytvořeno|CampaignItem|created|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
		//	'stav'		=>	'campaign_stav|Přidat aktivitu|message'	
		), 
        'domwin_setting' => array(
            'sizes' => '[900,600]', 
            'scrollbars' => true, 
            'languages' => 'false' 
        )
	);
	
	function beforeRender(){
		parent::beforeRender();
		$this->set('kampane_stav',array(
			0 => 'Založeno',
			1 => 'Odesláno'
		));		
	}
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/',$this->renderSetting['page_caption']=>'#'));
				
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			
			$this->set('scripts',array('wysiwyg_old/wswg','uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/wysiwyg_old/wswg','../js/uploader/uploader','../js/clearbox/clearbox'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		
		// nacteni kampani
		$this->loadModel('CampaignGroup');
		$this->set('campaign_group_list',$this->CampaignGroup->find('list',array('conditions'=>array('CampaignGroup.kos'=>0,'CampaignGroup.stav'=>0),'order'=>'CampaignGroup.name ASC')));
		unset($this->CampaignGroup);
		
		// nacteni spolecnosti
		$this->loadModel('Company');
		$this->set('company_list',$this->Company->find('list',array('conditions'=>array('Company.kos'=>0),'order'=>'Company.name ASC')));
		unset($this->Company);
		
		if (empty($this->data)){
			if ($id != null){		
				$this->CampaignItem->bindModel(array('belongsTo'=>array('CampaignGroup')));
				$this->data = $this->CampaignItem->read(null,$id);
				
				/*
				 * nacteni kontaktu k dane firme
				 */				
				$this->loadModel('CompanyContact');
				$this->set('company_contact_list',$this->CompanyContact->find('list',array('conditions'=>array('CompanyContact.kos'=>0,'CompanyContact.company_id'=>$this->data['CampaignItem']['company_id']),'order'=>'CompanyContact.name ASC')));
				unset($this->CompanyContact);
				
				
				
			} else {
				$this->set('company_contact_list',array());
			}
			$this->render('edit');
		} else {
			/*
			 * Ulozeni zaznamu
			 */
			if ($this->CampaignItem->save($this->data))
				die(json_encode(array('result'=>true)));
			else 
				die(json_encode(array('result'=>false, 'message'=>'Chyba behem ukladani zaznamu do DB')));
		}
	}
	
	function campaign_stav($id = null, $die = true) {
		$this->loadModel('CampaignItemHistory');
		
		if (empty($this->data)){
			$this->CampaignItemHistory->bindModel(array('belongsTo'=>array('CmsUser')));
			$this->set('history_list',$this->CampaignItemHistory->find('all', array('conditions'=>array('campaign_item_id'=>$id))));
			$this->data['CampaignItem']['id'] = $id;
			$this->render();
		} else {
            if($this->data['CampaignItem']['stav'] == 4)
                $this->data['CampaignItem']['datum_tel_kontaktu'] = date('Y-m-d'); 
            else if($this->data['CampaignItem']['stav'] == 5)
                $this->create_tasks_for_sm();
                                
			$this->CampaignItem->save($this->data);
			
			$this->data['CampaignItemHistory'] = array(
				'stav' 	=> $this->data['CampaignItem']['stav'],
				'text'	=> $this->data['CampaignItemHistory']['text'],
				'campaign_item_id' => $this->data['CampaignItem']['id'],
				'cms_user_id'=>$this->logged_user['CmsUser']['id']
			);
			
			
			
			$this->CampaignItemHistory->save($this->data);
			$this->CampaignItemHistory->id = null;
			if ($die === true)
				die(json_encode(array('result'=>true)));
		}
	}
	
	function add_company(){
		$this->loadModel('Company');
		if (empty($this->data)){
			
			// load stat list
			$this->loadModel('SettingStat'); 
			$this->set('company_stat_list',$this->SettingStat->find('list',array('conditions'=>array('SettingStat.status'=>1,'SettingStat.kos'=>0))));
			unset($this->SettingStat);
			
			// load province for CR
			$this->loadModel('Province');
			$this->set('province_list',$this->Province->find('list',array('conditions'=>array('stat_id'=>1))));
			unset($this->Province);
			
		} else {
			
			$this->Company->save($this->data);
			die(json_encode(array(
				'result' => true,
				'data'	=> $this->Company->find('list',array('conditions'=>array('Company.kos'=>0),'order'=>'Company.name ASC')),
				'company_id' => $this->Company->id
			)));
			
		}
		
	}
    
    /*
     * sprava jednotlivych kampani
     */
    function groups(){
    	$this->loadModel('CampaignGroup');
    	$this->set('group_list', $this->CampaignGroup->find(
    		'all',
    		array(
    			'conditions' => array(
    				'CampaignGroup.kos' => 0
    			),
    			'order'=>'CampaignGroup.created DESC'
    		)
    	));
    	
    }
    
    function group_edit($id = null){
    	$this->loadModel('CampaignGroup');
    	if (empty($this->data)){
    		if ($id != null){
    			$this->data = $this->CampaignGroup->read(null, $id); 
    		}
    	} else {
    		if ($this->CampaignGroup->save($this->data))
    			die(json_encode(array('result'=>true)));
    		else
    			die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani nove kampane')));
    	}
    	
    }

    /*
     * Pridani noveho kontaktu ve firme
     */
    function add_company_contact($company_id = null){
    	/*
    	 * Initializace modelu
    	 */
		$this->loadModel('CompanyContact');
		
		if (empty($this->data)){
			// jedna se o view
			if ($company_id == null)
    			die(json_encode(array('result'=>false,'message'=>'Chyba, neznámé ID společnosti')));
			
			$this->data['CompanyContact']['company_id'] = $company_id;
		} else {
			// jedna se o ulozeni
			if ($this->CompanyContact->save($this->data))
				die(json_encode(array(
					'result'	=> true,
					'select_id' => $this->CompanyContact->id,
					'data'		=> $this->CompanyContact->find('list',array('conditions'=>array('CompanyContact.kos'=>0,'CompanyContact.company_id'=>$this->data['CompanyContact']['company_id']),'order'=>'CompanyContact.name ASC')),
					'contact' 	=> $this->data 
				)));
			else 
				die(json_encode(array('result'=>false, 'message'=>'Chyba během ukládání záznamu do DB')));
		}
    }
    
    /*
     * Natahnuti kontaktu k dane firme
     */
    function load_company_contacts($company_id = null){
    	if ($company_id == null)
    		die(json_encode(array('result'=>false, 'message'=>'Nebylo specifikovano ID spolecnosti')));
		
    	$this->loadModel('CompanyContact');
    	die(json_encode(array('result'=>true, 'data'=>$this->CompanyContact->find('list',array('conditions'=>array('CompanyContact.kos'=>0,'CompanyContact.company_id'=>$company_id),'order'=>'CompanyContact.name ASC')))));
    }
    
    /*
     * Natahnuti informaci o danem kontaktu
     */
    function load_company_contact_info($company_contact_id = null){
    	if ($company_contact_id == null)
    		die(json_encode(array('result'=>false, 'message'=>'Nebylo specifikovano ID kontaktni osoby')));
    	
    	$this->loadModel('CompanyContact');
    	die(json_encode(array('result'=>true, 'data'=>$this->CompanyContact->read(array('telefon1','telefon2','email'),$company_contact_id))));	
    }
    
     /**
     * Vytvoření úkolu pro SM - Klimparová
     * @since 9.11.2009
     */
    private function create_tasks_for_sm(){
    	$defaul_sm = 1; // pote zmenit na klimparovou
    	
    	$this->CampaignItem->bindModel(array('belongsTo'=>array('Company')));
    	$d = $this->CampaignItem->read(array('Company.name','Company.self_manager_id','Company.id'),$this->data['CampaignItem']['id']);
    	
    	$for_sm = (!empty($d['Company']['self_manager_id'])?$d['Company']['self_manager_id']:$default_sm); 
    	
        $for_sm = $defaul_sm; //test 
        
        //vytvoreni ukolu
		$termin = new DateTime();
        $termin->modify("+2 day");					
	    $task_to_save = array('CompanyTask'=>array(
		    'cms_user_id' 	=> 	$for_sm, 
		    'name'			=> 	'Kampane - Call Centrum',
            'termin'		=>	$termin->format("Y-m-d"),
			'company_id'	=>	$d['Company']['id'],
			'setting_task_type_id' => 10,
			'creator_id'	=> 	$this->logged_user['CmsUser']['id'],
			'text'			=> 'Zpracování požadavku z Call Centra <strong>'.$d['Company']['name'].'</strong>.'
		));
        $this->loadModel('CompanyTask');
		$this->CompanyTask->save($task_to_save);
        unset($this->CompanyTask);
        
        
        //odeslani emailu
		$this->loadModel('CmsUser'); 
		$email_list = $this->CmsUser->find('list',array(
            'fields'=>array('id','email'), 
			'conditions'=>array(
						'id'=>$for_sm
			)
        ));
		unset($this->CmsUser);


		$replace_list = array(
			'##CompanyTask.name##' 			=>$task_to_save['CompanyTask']['text'],
			'##CompanyTask.text##' 	=>$task_to_save['CompanyTask']['name']
		);
				
		$this->Email->send_from_template_new(1,$email_list,$replace_list);
    }   
	
    function load_capaign_odberatele($group_id = null){
    	//odberatele
    	$this->CampaignItem->bindModel(array('belongsTo'=>array('Company', 'CompanyContact')));
		$this->set('odberatele_list', $this->CampaignItem->find('all',array(
        	'fields'=>array(
            	'CampaignItem.id',
                'CampaignItem.mail',
                'CampaignItem.pozice',
                'Company.name',
                'CompanyContact.name'
          	),
            'conditions'=>array(
            	'stav' => 1,
                'mail !=' => '',
          		'campaign_group_id'=>$group_id 
          	)
     	)));
     	$this->render('email_send_odberatele');
    }
    
    function email_send($id = null){
		$this->autoLayout = false;

        if (empty($this->data)){
        	// nacteni seznamu sablon
			$this->loadModel('CampaignTemplate');
			$this->set('campaign_template_list',$this->CampaignTemplate->find('list',array('conditions'=>array('CampaignTemplate.kos'=>0),'order'=>'CampaignTemplate.name ASC')));
			unset($this->CampaignTemplate);
			
        	// nacteni kampani
			$this->loadModel('CampaignGroup');
			$this->set('campaign_group_list',$this->CampaignGroup->find('list',array('conditions'=>array('CampaignGroup.kos'=>0,'CampaignGroup.stav'=>0),'order'=>'CampaignGroup.name ASC')));
			unset($this->CampaignGroup);
            
            //nacteni emailu z nadrazenych spolecnosti pro odesilatele
            $this->loadModel('SettingParentCompany');
			$this->set('parent_companies',$this->SettingParentCompany->find('list',array('fields'=>array('id','name'))));
			
            //nacteni typu priloh
            $this->loadModel('SettingAttachmentType');
            $this->SettingAttachmentType = &new SettingAttachmentType();
            $this->set('setting_attachment_type_list', $this->SettingAttachmentType->find('list',
                array('conditions' => array('kos' => 0), 'order' => 'poradi ASC')));
            unset($this->SettingAttachmentType);
            
            //render
			$this->render('email_send');
		} else {       
		  
            $attach_list = array();
            if(isset($this->data['prilohy']) && count($this->data['prilohy']) > 0)
                $attach_list = $this->data['prilohy'];
      
            foreach($this->data['Email']['to'] as $id=>$mail){
            	$data = null;
            	$this->CampaignItem->bindModel(array('belongsTo'=>array('CompanyContact')));
            	$data = $this->CampaignItem->read(array('CompanyContact.name','CampaignItem.datum_inzeratu','CampaignItem.inzerat_z'),$id);
                $this->loadModel('SettingParentCompany');
                $spc = $this->SettingParentCompany->read(array('email','name'),$this->data['Email']['from']);
                                
        		$replace_list = array(
        			'##JMENO##' 			=> $data['CompanyContact']['name'],
        			'##DATUM.INZERATU##' 	=> date('d.m.Y', strtotime($data['CampaignItem']['datum_inzeratu'])),
        			'##ODKUD##' 			=> $data['CampaignItem']['inzerat_z']
        		);
                
                //pr($this->data['prilohy']);
                
                $options = array(
        			'from' 				=> 	$spc['SettingParentCompany']['email'],
        			'from_name' 	=> $spc['SettingParentCompany']['name'],
        			'subject' 	=> $this->data['Email']['subject'],
        			'text' 	=> $this->data['Email']['text'],
        			'attach_path' 	=> '/uploaded/campaign_items_tippers/attachment/'
        		);
        	    //odeslani emailu	
        		$this->Email->send_without_template($options,array($mail),$replace_list,$attach_list);
                
				
                $email_list = array();
					
                // zmena stavu kamapne group
                $this->loadModel('CampaignGroup');
                $this->CampaignGroup->id = $this->data['Email']['campaign_group_id'];
                $this->CampaignGroup->saveField('stav',1);
                unset($this->CampaignGroup);
                
                //zmena stavu a zapsani historie
                $this->data = am($this->data, array(
                	'CampaignItem' => array(
                		'id' => $id,
                		'stav' =>2
                	),
                	'CampaignItemHistory'=>array(
                		'text' => 'Byl odeslan Email'
                	) 
                ));
                self::campaign_stav(null, false);
                $this->CampaignItem->id = $id;
                $this->CampaignItem->saveField('stav',2);
                
                
            }
			die(json_encode(array('result'=>true)));
		}
	}
    
	
	function template(){
    	$this->loadModel('CampaignTemplate');
    	$this->set('template_list', $this->CampaignTemplate->find(
    		'all',
    		array(
    			'conditions' => array(
    				'CampaignTemplate.kos' => 0
    			),
    			'order'=>'CampaignTemplate.created DESC'
    		)
    	));
    	
    }
    
    function template_edit($id = null){
    	$this->loadModel('CampaignTemplate');
    	if (empty($this->data)){
    		if ($id != null){
    			$this->data = $this->CampaignTemplate->read(null, $id); 
                
                $this->loadModel('CampaignTemplatesAttachment');
                $this->CampaignTemplatesAttachment->bindModel(array('belongsTo' => array('SettingAttachmentType')));
                $this->set('attachment_list', $this->CampaignTemplatesAttachment->findAll(array('CampaignTemplatesAttachment.campaign_template_id' =>
                    $id, 'CampaignTemplatesAttachment.kos' => 0)));
                unset($this->CampaignTemplatesAttachment);
    		}
            
            $this->set('id',$id);
    	} else {
    		if ($this->CampaignTemplate->save($this->data))
    			die(json_encode(array('result'=>true)));
    		else
    			die(json_encode(array('result'=>false,'message'=>'Chyba behem ukladani sablony')));
    	}
    	
    }
	
	function load_template($id){
		$this->loadModel('CampaignTemplate');
        $this->CampaignTemplate->bindModel(array(
            'hasMany'=>array(
                'CampaignTemplatesAttachment'=>array(
                    'foreingKey'=>'campaign_template_id'
                )
            )
        ));
		$data = $this->CampaignTemplate->read(null, $id);
        //pr($data);
		if ($data){
			die(json_encode(array('result'=>true,'data'=>array(
                'text'=>$data['CampaignTemplate']['text'],
                'name'=>$data['CampaignTemplate']['name'],
                'prilohy'=>$data['CampaignTemplatesAttachment']
            ))));
		} else {
			die(json_encode(array('result'=>false,'message'=>'Nepodarilo se nacist sablonu')));
		}
	}
    
    
     /**
     * Seznam priloh
     *
     * @param $campaign_template_id
     * @return view
     * @access public
     **/
    function attachs($campaign_template_id)
    {
        $this->autoLayout = false;
        $this->loadModel('CampaignTemplatesAttachment');

        if ($this->logged_user["CmsGroup"]["permission"]["clients"]["attach"] == 2)
            $pristup = "CampaignTemplatesAttachment.cms_user_id=" . $this->logged_user['CmsUser']['id'];
        else
            $pristup = "";

        $this->CampaignTemplatesAttachment->bindModel(array('belongsTo' => array('SettingAttachmentType')));
        $this->set('attachment_list', $this->CampaignTemplatesAttachment->findAll(array('CampaignTemplatesAttachment.campaign_template_id' =>
            $campaign_template_id, 'CampaignTemplatesAttachment.kos' => 0, $pristup)));
        $this->set('id', $campaign_template_id);
        unset($this->CampaignTemplatesAttachment);
        $this->render('template/attachs');
    }

    /**
     * Editace priloh
     *
     * @param $campaign_template_id
     * @param $id
     * @return view
     * @access public
     **/
    function attachs_edit($campaign_template_id = null, $id = null)
    {
        $this->autoLayout = false;
        $this->loadModel('CampaignTemplatesAttachment');
        if (empty($this->data))
        {
            $this->loadModel('SettingAttachmentType');
            $this->SettingAttachmentType = &new SettingAttachmentType();
            $this->set('setting_attachment_type_list', $this->SettingAttachmentType->find('list',
                array('conditions' => array('kos' => 0), 'order' => 'poradi ASC')));
            unset($this->SettingAttachmentType);
            $this->data['CampaignTemplatesAttachment']['campaign_template_id'] = $campaign_template_id;
            if ($id != null)
            {
                $this->data = $this->CampaignTemplatesAttachment->read(null, $id);
            }
            
            $this->render('template/attachs_edit');
        } else
        {
            $this->data["CampaignTemplatesAttachment"]["cms_user_id"] = $this->logged_user['CmsUser']['id'];
            $this->CampaignTemplatesAttachment->save($this->data);
            $this->attachs($this->data['CampaignTemplatesAttachment']['campaign_template_id']);
        }
        unset($this->CampaignTemplatesAttachment);
    }
    /**
     * Presun prilohy do kose
     *
     * @param $company_id
     * @param $id
     * @return view
     * @access public
     **/
    function attachs_trash($template_id, $id)
    {
        $this->loadModel('CampaignTemplatesAttachment');
        $this->CampaignTemplatesAttachment->save(array('CampaignTemplatesAttachment' => array('kos' => 1,'id' =>$id)));
        $this->attachs($template_id);
        unset($this->CampaignTemplatesAttachment);
    }

    /**
     * Nahrani prilohy na ftp
     *
     * @return view
     * @access public
     **/
    function upload_attach()
    {
        $this->Upload->set('data_upload', $_FILES['upload_file']);
        if ($this->Upload->doit(json_decode($this->data['upload']['setting'], true)))
        {
            echo json_encode(array('upload_file' => array('name' => $this->Upload->get('outputFilename')),
                'return' => true));
        } else
            echo json_encode(array('return' => false, 'message' => $this->Upload->get('error_message')));
        die();
    }

    /**
     * Stazeni prilohy
     *
     * @param $file
     * @param $file_name
     * @return download file
     * @access public
     **/
    function attachs_download($file, $file_name)
    {
        $pripona = strtolower(end(Explode(".", $file)));
        $file = strtr($file, array("|" => "/"));
        $filesize = filesize('./uploaded/' . $file);
        $cesta = "http://" . $_SERVER['SERVER_NAME'] . "/uploaded/" . $file;

        header("Pragma: public"); // požadováno
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false); // požadováno u některých prohlížečů
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . $filesize);
        Header('Content-Type: application/octet-stream');
        Header('Content-Disposition: attachment; filename="' . $file_name . '.' . $pripona .
            '"');
        readfile($cesta);
        die();

    }

}
?>