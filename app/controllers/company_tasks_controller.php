<?php

class CompanyTasksController extends AppController {
	var $name = 'CompanyTasks';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('CompanyTask');
	var $renderSetting = array(
		//'bindModel'	=> array('belongsTo'=>array('Company','CmsUserFor'=>array('ClassName'=>'CmsUser','foreignKey'=>'cms_user_id'),'CmsUserTo'=>array('ClassName'=>'CmsUser','foreignKey'=>'creator_id'))),
		'bindModel'	=> array('belongsTo'=>array('Company','CmsUserFrom'=>array('className'=>'CmsUser','foreignKey'=>'creator_id'),'CmsUserTo'=>array('className'=>'CmsUser','foreignKey'=>'cms_user_id'))),
		'controller'=>'company_tasks',
		'SQLfields' => array('Company.name','CompanyTask.id','CompanyTask.name','CompanyTask.termin','CompanyTask.text','CompanyTask.complate','CompanyTask.complate_date','CompanyTask.updated','CompanyTask.created','CompanyTask.company_id','CmsUserFrom.name','CmsUserTo.name'),
		'page_caption'=>'Reporty - Úkoly v podnicích',
		'sortBy'=>'CompanyTask.created.DESC',
		'top_action' => array(
			// caption|url|description|permission
			//'add_item'		=>	'Přidat|edit|Pridat popis|add',
		),
		'filtration' => array(
			'CompanyTask-name'			=>	'text|Název úkolu|',
			'CompanyTask-complate'		=>	'select|Hotovo|ya_list',
			'CompanyTask-cms_user_id'	=>	'select|Pro|cms_user_list',
			'CompanyTask-creator_id'	=>	'select|Vytvořil|cms_user_list',
			'CompanyTask-termin'		=>	'date_f_t|Termín|',
			
		),
		'items' => array(
			'id'			=>	'ID|CompanyTask|id|hidden|',
			'company'		=>	'Firma|Company|name|text|',
			'name'			=>	'Název|CompanyTask|name|text|orez#40',
			'cms_user_from'	=>	'Zadal|CmsUserFrom|name|text|',
			'cms_user_to'	=>	'Pro|CmsUserTo|name|text|',
			'termin'		=>	'Termín|CompanyTask|termin|date|',
			'complate'		=>	'Splněno|CompanyTask|complate|viewVars|ya_list',
			'complate_date'	=>	'Termín splnění|CompanyTask|complate_date|date|',
			'created'		=>	'Datum vytvoření|CompanyTask|created|datetime|'
		),
		'posibility' => array(
			//'show'		=>	'show|Zobrazit úkol|show',			
			'edit'		=>	'edit|Editovat úkol|edit'
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Úkoly v podnicích'=>'#'));
		$this->loadModel('CmsUser'); $this->CmsUser =& new CmsUser();
		$this->loadModel('Company'); $this->Company =& new Company();
		
		$this->set('cms_user_list',$this->CmsUser->find('list',array('conditions'=>array('CmsUser.status'=>1,'CmsUser.kos'=>0),'order'=>'CmsUser.name ASC')));
		$this->set('company_list',$this->Company->find('list',array('conditions'=>array('Company.status'=>1,'Company.kos'=>0))));
		$this->set('ya_list',array('Ne','Ano'));
		unset($this->CmsUser);
		unset($this->Company);
		
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function show($id = null){
		$this->autoLayout = false;
		$this->CompanyTask->bindModel(array('belongsTo'=>array('CompanyContact','Company')));
		$this->data = $this->CompanyTask->read(null,$id);
		$this->render('show');
	}
	
	function edit($id = null){		
		echo $this->requestAction('companies/tasks_edit/-1/' . $id);
		die();
	}
	
	
	function send_email(){
		$this->Email->send_from_template(1,array('strnad@fastest.cz'),array('##CmsUser.name##'=>'Zbynek Strnad'));
		die();
	}
}
?>