<?php
//Configure::write('debug',2);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

define('fields','Company.name,SUM(count) as sum_count,SUM(price_cz*count) as sum_price_cz,SUM(price_eu*count) as sum_price_eu');
        
class ReportCompanyOppsController extends AppController {
    var $name = 'ReportCompanyOpps';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('OppOrderItem');
	var $renderSetting = array(
		'SQLfields' =>array(fields),
		'bindModel' => array(
            'belongsTo' => array(
                'Company'=>array('className'=>'Company','foreignKey'=>'company_id'),
                'OppOrder'=>array('className'=>'OppOrder','foreignKey'=>'opp_order_id'),
            )
        ),
		'controller'=> 'report_company_opps',
		'page_caption'=>'Report vydaných OPP za podniky',
		'sortBy'=>'OppOrderItem.created.DESC',
		'top_action' => array(),
        'SQLcondition'=>array(
            'OppOrderItem.kos'=>0,
            'OppOrderItem.count >'=>0,
            'OppOrder.kos'=>0,
            'OppOrder.status'=>1,
            "DATE_FORMAT(OppOrder.datum_potvrzeni,'%Y-%m') = '#CURRENT_YEAR#-#CURRENT_MONTH#'",
        ),	
        'count_col'=>'OppOrderItem.company_id',
        'group_by'=>'OppOrderItem.company_id',
        'filtration' => array(
            'GET-current_month'		  =>	'select|Měsíc|mesice_list',
            'GET-current_year'		  =>	'select|Rok|actual_years_list',
        ),
		'items' => array(
			'id'				=>	'ID|OppOrderItem|id|hidden|',
	        'company'			=>	'Podnik|Company|name|text|',
            'stav'		        =>	'Celkem kusů|0|sum_count|text|',
            'price_cz'			=>	'Cena CZ|0|sum_price_cz|money2|',
            'price_eu'			=>	'Cena EU|0|sum_price_eu|money2|',
		),
		'posibility' => array(),
		'domwin_setting' => array(
			'sizes' 		=> '[1000,1000]',
			'scrollbars'		=> true,
			'languages'	=> 'false'
		)
	);
    
    function index(){
        
        if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {			
			// set FastLinks
			$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#',$this->renderSetting['page_caption']=>'#'));
			$this->render('../system/index');
           
		}
	}
   
	
}
?>