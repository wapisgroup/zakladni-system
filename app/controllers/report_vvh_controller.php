<?php
//Configure::write('debug',2);
$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
@define('CURRENT_YEAR', $_GET['current_year']); 
@define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class ReportVvhController extends AppController {
	var $name = 'ReportVvh';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Excel');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ReportDownPayment');
	var $renderSetting = array(
		'bindModel'	=> array(
            'belongsTo'=>array('Company','CmsUser','Client'),
            'joinSpec'=>array(
                'SettingParentCompany'=>array(
                    'className'=>'SettingParentCompany',
                    'foreignKey'=>'Company.parent_id',
                    'primaryKey'=>'SettingParentCompany.id'
                )
            )
        ),
		'controller'=>'report_vvh',
		'SQLfields' => '*',
		'page_caption'=>'Seznam požadavků na výplaty v hotovosti',
		'sortBy'=>'ReportDownPayment.created.DESC',
		'SQLcondition'=>array('ReportDownPayment.type'=>1,'ReportDownPayment.kos'=>0,"((DATE_FORMAT(ReportDownPayment.created,'%Y-%m') = '#CURRENT_YEAR#-#CURRENT_MONTH#'))"),
		'top_action' => array(
			// caption|url|description|permission
			'excel'		        =>	'Excel|export_excel|Exportovat do excelu|index',
            'multi_edit'		=>	'Uhradit hromadně|paid_all|Uhradit hromadně|multi_edit',

		),
		'filtration' => array(
			'ReportDownPayment-cms_user_id'		=>	'select|CM/KOO|cm_list',
			'ReportDownPayment-company_id'		=>	'select|Společnost|company_list',
			'Client-name'		=>	'text|Zaměstnanec|',
			'ReportDownPayment-date_of_paid'	=>	'select|Neuhrazeno|filtr_uhrazeno',
			//'ReportDownPayment-type'	=>	'select|Typ|down_payments_type',
            
            'GET-current_year'							=>	'select|Rok|actual_years_list',
            'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'				=>	'ID|ReportDownPayment|id|hidden|',
			'cms_user_id'		=>	'CM/KOO|CmsUser|name|text|',
			'company'			=>	'Firma|Company|name|text|',
            'nad_spol'			=>	'Nadřazená spol.|SettingParentCompany|name|text|',
			'zamestnanec'		=>	'Zaměstnanec|Client|name|text|',
			//'cislo_uctu'		=>	'Č. účtu|0|cislo_uctu|cislo_uctu|',
			'castka'			=>	'Částka|ReportDownPayment|amount|text|',
			'date_of_paid'		=>	'Uhrazeno|ReportDownPayment|date_of_paid|date|',
			'type'		        =>	'Typ|ReportDownPayment|type|var|down_payments_type|',
			'created'			=>	'Vytvořeno|ReportDownPayment|created|date|'
		),
		'posibility' => array(
		//	'show'		=>	'show|Zobrazit aktivitu|show',			
			//'edit'		=>	'edit|Editovat položku|edit',
            'uhrazeno'	=>	'uhrazeno|Uhrazeno|uhrazeno',			
			'trash'		=>	'trash|Smazat položku|trash',			
		),
		'domwin_setting' => array(
			'sizes' 		=> '[900,900]',
			'scrollbars'	=> true,
			'languages'		=> 'false',
			'defined_lang'	=> 'false'
		),
        'checkbox_setting' => array(
			'model'			=>	'ReportDownPayment',
			'col'			=>	'date_of_paid',
			'stav_array'	=>	array('0000-00-00'),
			'month_year'	=>	false
		),
        //'no_limit' => true
	);

    function beforeFilter(){
        parent::beforeFilter();
        if (isset($_GET['excel'])){
			$this->renderSetting['no_limit'] = true;
        }
    }
	
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Vystavené zálohy o VVH'=>'#'));
		
		/*
		 * Spolecnost List pro filtraci
		 */
		$this->loadModel('Company'); 
		$company_conditions =  array('Company.kos'=>0);
		if (isset($this->filtration_company_condition))
			$company_conditions = am($company_conditions, $this->filtration_company_condition);
		$this->set('company_list',		$this->Company->find('list',array('conditions'=>$company_conditions,'order'=>'Company.name ASC')));
		unset($this->Company);
		
		/*
		 * Seznam CM a KOO List pro filtraci
		 */
		$this->loadModel('CmsUser');
		$this->set('cm_list',$this->CmsUser->find('list',array('conditions'=>array('CmsUser.cms_group_id'=>array(3,4),'CmsUser.kos'=>0,'CmsUser.status'=>1))));
		unset($this->CmsUser);

        if (isset($_GET['excel'])){
			$this->autoLayout = false;
			echo $this->render('../system/excel');
			die();
		}

		/*
		 * Start Render
		 */
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

    function export_excel(){
		$link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&excel=true';
		$this->redirect($link);
		exit;
	}
	
	    
    /**
     * Funkce ktera vyplnuje dle aktualniho datumu nebo zvoleneho sloupce uhrazeno
     * 
     * @param $id 
     * @author Sol
     * @created 7.12.2009
     */
    function uhrazeno($id){
   	    echo $this->requestAction('report_down_payments/uhrazeno/'.$id.'/');
		die();
	}
    
    
    /**
     * funkce dava datum pro vsechny zaskrtnute
     */
    function paid_all(){ 		
        echo $this->requestAction('report_down_payments/paid_all/');
		die();
	}
    
    /**
     * slepa funkce pro historii
     * @param $client_id
     * @data $date_of_paid
     * @data $company_id
     * @data $amount
     */
    function record_paid_client($client_id){
       return true;      
    }
    
    
    
    /**
     * funkce pro nacteni zamestnancu podle nadrazene spolecnosti
     */
    public function load_parent_company_employees($parent_company_id){
        echo $this->requestAction('report_down_payments/load_parent_company_employees/'.$parent_company_id.'/');
		die();
    }
    
    
    function transfer_to_new_connection(){
       // die('nefunkcni');
        Configure::write('debug',1);
        $this->ReportDownPayment->bindModel(array('belongsTo'=>array('Client')));
        $rdp = $this->ReportDownPayment->find('all',array(
            'conditions'=>array(
                'company_id'=>2388,
                'ReportDownPayment.kos'=>0,
                'year'=>'2011',
                'month'=>'7',
                'type'=>1
            ),
            'group'=>'client_id',
            'fields'=>array('client_id','SUM(amount) vvh','connection_client_requirement_id','Client.name')
        ));
       // pr($rdp);
       echo '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';
        echo count($rdp).'<br />';
        $this->loadModel('ClientWorkingHour');
        foreach($rdp as $item){
            $working_hour = $this->ClientWorkingHour->find('first',array(
                'conditions'=>array(
                    'company_id'=>'2388',
                    'client_id'=>$item['ReportDownPayment']['client_id'],
                    'connection_client_requirement_id'=>$item['ReportDownPayment']['connection_client_requirement_id'],
                    'year'=>'2011',
                    'month'=>'07'
                )
            ));
            if($working_hour){
                echo $item['ReportDownPayment']['client_id'].' | ';
                echo $item['Client']['name'].' | ';
                echo 'Mzda:'.$working_hour['ClientWorkingHour']['salary_per_hour_p'].' | ';
                echo 'VVH:'.$item['0']['vvh'].' | ';
                echo (($working_hour['ClientWorkingHour']['salary_per_hour_p']-$item['0']['vvh'])<0?'Prenos':'OK');
                echo '<br />';
            }
            
            /*
            $this->ReportDownPayment->id = $item['ReportDownPayment']['id'];
            $this->ReportDownPayment->save(array(
                'connection_client_requirement_id'=>$connection_new['ConnectionClientRequirement']['id'],
                'company_id'=>'2388'
            ));
            */
        }    
        die();
    }
}
?>