<?php
//Configure::write('debug',1);
class InternalRegulationsController extends AppController {
	var $name = 'InternalRegulations';
	var $helpers = array('htmlExt','Pagination','ViewIndex','FileInput');
	var $components = array('ViewIndex','RequestHandler','Upload');
	var $uses = array('InternalRegulation');
	var $renderSetting = array(
		'bindModel'	=> array('belongsTo'=>array('InternalRegulationGroup')),
		'SQLfields' => '*',
        'controller'=> 'internal_regulations',
		'page_caption'=>'Interní předpisy',
		'sortBy'=>'InternalRegulation.id.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
			'group'		    =>	'Kategorie|group|Kategorie|group'
			//'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
			//'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
			//'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
			'InternalRegulation-question'			=>	'text|Otázka|',
			'InternalRegulation-internal_regulation_group_id'		=>	'select|Kategorie|internal_regulation_group_list'	
		),
		'items' => array(
			'id'		=>	'ID|InternalRegulation|id|text|',
            'group'		=>	'Kategorie|InternalRegulationGroup|name|text|',
			'question'	=>	'Otázka|InternalRegulation|question|text|orez#100',
			'answer'	=>	'Odpověď|InternalRegulation|answer|text|orez#100',
			'contact'	=>	'Kontaktná osoba|InternalRegulation|contact|text|orez#100',
			//'created'	=>	'Vytvořeno|InternalRegulation|created|datetime|',
			//'updated'	=>	'Změněno|InternalRegulation|updated|datetime|'
		),
		'posibility' => array(
			'edit'		=>	'edit|Editace položky|edit',
            //'access'	=>	'access|Přístupy|access',
            'attach'	=>	'attachs|Přílohy|edit',	
			'delete'	=>	'trash|Odstranit položku|delete'			
		),
        /**
         * nove zadavani jen sve, tzv. pristupy
         * nutne je zadat tabulku kde jsou pristupy ulozeny
         * a id pro jake se ma filtrovat prirazene zaznamy
         * group -> zda plati jen sve pro cms_group_id nebo pro cms_user_id
         
        'only_his'=>array(
            'table'=>'wapis__InternalRegulation_access',
            'col'=>'InternalRegulation_id',
            'group'=>true
        )*/
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#',$this->renderSetting['page_caption']=>'#'));
		$this->loadModel('InternalRegulationGroup'); 
		$this->set('internal_regulation_group_list',$this->InternalRegulationGroup->find('list',array(
            'conditions'=>array('kos'=>0),
            'group'=>'name')
        ));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->set('scripts',array('uploader/uploader'));
			$this->set('styles',array('../js/uploader/uploader'));
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			// load InternalRegulation group list
			$this->loadModel('InternalRegulationGroup');
			$this->set('group_list',$this->InternalRegulationGroup->find('list',array('conditions'=>array('InternalRegulationGroup.kos'=>0))));

			
			if ($id != null){
				$this->data = $this->InternalRegulation->read(null,$id);
			}

			$this->render('edit');
		} else {
			$this->InternalRegulation->save($this->data['InternalRegulation']);
			die();
		}
	}
    
    
    /**
     * sekce skupiny
     */
    function group(){
        
        $this->loadModel('InternalRegulationGroup');
        $group_list = $this->InternalRegulationGroup->find('all',array(
            'conditions'=>array(
                'kos'=>0
            )
        ));       
        
        $this->set('group_list',$group_list);
        
        //render
        $this->render('group/index');
    }
    
        //EDIT
        function group_edit($id = null){
            if(empty($this->data)){
                if($id != null){
                    $this->loadModel('InternalRegulationGroup');
                    $this->data = $this->InternalRegulationGroup->read(null,$id);
                }
                
                //render
                $this->render('group/edit');
            }
            else{
                $this->loadModel('InternalRegulationGroup');
                $this->InternalRegulationGroup->save($this->data);
            }
        }
        
        //TRASH
        function group_trash($id){
            $this->loadModel('InternalRegulationGroup');
            $this->InternalRegulationGroup->id = $id;
            $this->InternalRegulationGroup->saveField('kos',1);
            unset($this->InternalRegulationGroup);
            die();
        }



    /**
     * přístupy k danému InternalRegulation 
     * CMSGroup
     */
    function access($InternalRegulation_id = null){
        $this->set('InternalRegulation_id',$InternalRegulation_id);
        
        $this->loadModel('InternalRegulationAccess');
        $this->InternalRegulationAccess->bindModel(array('belongsTo'=>array('CmsGroup')));
        $access_list = $this->InternalRegulationAccess->find('all',array(
            'conditions'=>array(
                'InternalRegulationAccess.kos'=>0,
                'InternalRegulationAccess.InternalRegulation_id'=>$InternalRegulation_id
            )
        ));       
        
        $this->set('access_list',$access_list);
        
        //render
        $this->render('access/index');
    }
    
        //EDIT
        function access_edit($InternalRegulation_id = null,$id = null){
            if(empty($this->data)){
                if($id != null){
                    $this->loadModel('InternalRegulationAccess');
                    $this->data = $this->InternalRegulationAccess->read(null,$id);
                }
                else                
                    $this->data['InternalRegulationAccess']['InternalRegulation_id'] = $InternalRegulation_id;
                $this->loadModel('CmsGroup');
                $this->set('cms_group_list',$this->CmsGroup->find('list',array('conditions'=>array('kos'=>0,'status'=>1,'id NOT IN(SELECT cms_group_id FROM wapis__InternalRegulation_access Where kos = 0 and InternalRegulation_id = '.$InternalRegulation_id.')'))));
                
                //render
                $this->render('access/edit');
            }
            else{
                $this->loadModel('InternalRegulationAccess');
                $this->InternalRegulationAccess->save($this->data);
            }
        }
        
        //TRASH
        function access_trash($id){
            $this->loadModel('InternalRegulationAccess');
            $this->InternalRegulationAccess->id = $id;
            $this->InternalRegulationAccess->saveField('kos',1);
            unset($this->InternalRegulationAccess);
            die();
        } 
        
   
   /**
 	* Seznam priloh
 	*
	* @param $InternalRegulation_id
 	* @return view
 	* @access public
	**/
	function attachs($InternalRegulation_id){
		$this->autoLayout = false;
		$this->loadModel('InternalRegulationAttachment'); 
		$this->InternalRegulationAttachment->bindModel(array('belongsTo'=>array('SettingAttachmentType')));
		$this->set('attachment_list', $this->InternalRegulationAttachment->findAll(
                array('InternalRegulationAttachment.internal_regulation_id'=>$InternalRegulation_id,
                        'InternalRegulationAttachment.kos'=>0
                )
        ));
		$this->set('InternalRegulation_id',$InternalRegulation_id);
		unset($this->InternalRegulationAttachment);
		$this->render('attachs/index');
	}
	
	/**
 	* Editace priloh
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_edit($InternalRegulation_id = null, $id = null){
		$this->autoLayout = false;
		$this->loadModel('InternalRegulationAttachment'); 
		if (empty($this->data)){
			$this->loadModel('SettingAttachmentType');
			$this->set('setting_attachment_type_list',$this->SettingAttachmentType->find('list',array('conditions'=>array('kos'=>0),'order'=>'poradi ASC')));
			unset($this->SettingAttachmentType);
            
			$this->data['InternalRegulationAttachment']['internal_regulation_id'] = $InternalRegulation_id;
			if ($id != null){
				$this->data = $this->InternalRegulationAttachment->read(null,$id);
			}
			$this->render('attachs/edit');
		} else {
			$this->InternalRegulationAttachment->save($this->data);
			$this->attachs($this->data['InternalRegulationAttachment']['InternalRegulation_id']);
		}
		unset($this->InternalRegulationAttachment);
	}
	/**
 	* Presun prilohy do kose
 	*
	* @param $company_id
	* @param $id
 	* @return view
 	* @access public
	**/
	function attachs_trash($InternalRegulation_id, $id){
		$this->loadModel('InternalRegulationAttachment');
		$this->InternalRegulationAttachment->save(array('InternalRegulationAttachment'=>array('kos'=>1,'id'=>$id)));
		$this->attachs($InternalRegulation_id);
		unset($this->InternalRegulationAttachment);
	}
	
	/**
 	* Nahrani prilohy na ftp
 	*
 	* @return view
 	* @access public
	**/
	function upload_attach() {
		$this->Upload->set('data_upload',$_FILES['upload_file']);
		if ($this->Upload->doit(json_decode($this->data['upload']['setting'],true))){
			echo json_encode(array('upload_file'=>array('name'=>$this->Upload->get('outputFilename')),'return'=>true));
		} else 
			echo json_encode(array('return'=>false,'message'=>$this->Upload->get('error_message')));		
		die();
	} 
	
	/**
 	* Stazeni prilohy
 	*
	* @param $file
	* @param $file_name
 	* @return download file
 	* @access public
	**/
	function  attachs_download($file,$file_name){
		$pripona = strtolower(end(Explode(".", $file)));
		$file = strtr($file,array("|"=>"/"));
		$filesize = filesize('./uploaded/'.$file);
		$cesta = "http://".$_SERVER['SERVER_NAME']."/uploaded/".$file;
				 
		header("Pragma: public"); // požadováno
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: private",false); // požadováno u některých prohlížečů
	    header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $filesize);
		Header('Content-Type: application/octet-stream');
		Header('Content-Disposition: attachment; filename="'.$file_name.'.'.$pripona.'"');
		readfile($cesta);
		die();

	}     
    
    
    
    function import(){
        $file = './uploaded/internal_regulations/InternalRegulation.csv';
        
        
        $handle = @fopen($file, "r");
        if ($handle) {
            $i = 0;
            while (!feof($handle)) {
                $buffer = fgets($handle, 4096);
                $data = explode(';',$buffer);

                if($i > 0  && $data[0] != ''){ //prvni radek je hlavicka
                    $to_save = array(
                        'question'=>iconv('CP1250','UTF8',$data[0]),
                        'answer'=>iconv('CP1250','UTF8',$data[1]),
                        'contact'=>iconv('CP1250','UTF8',$data[2]),
                    );
                
                    $this->InternalRegulation->id = null;
                    $this->InternalRegulation->save($to_save);
                }
            
                $i++;
                
            }
            fclose($handle);
        }

       die('done'); 
    }
    
}
?>