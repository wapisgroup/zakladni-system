<?php
class SettingActivityTypesController extends AppController {
	var $name = 'SettingActivityTypes';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('ViewIndex','RequestHandler');
	var $uses = array('SettingActivityType');
	var $renderSetting = array(
		'controller'=>'setting_activity_types',
		'SQLfields' => array('id','name','updated','created','status','poradi'),
		'page_caption'=>'Nastavení typů aktivit',
		'sortBy'=>'SettingActivityType.poradi.ASC',
		'top_action' => array(
			// caption|url|description|permission
			'add_item'		=>	'Přidat|edit|Pridat popis|add',
		//	'delete_item'	=> 	'Smazat|trash_more|Smazat multi popis|delete',
		//	'active_item'	=>	'Aktivovat|active_more|Aktivovat multi popis|status',
		//	'deactive_item'	=>	'Deaktivovat|deactive_more|Deaktivovat multi popis|status'
		),
		'filtration' => array(
		//	'SettingActivityType-status'	=>	'select|Stav|select_stav_zadosti',
		//	'SettingActivityType-name'		=>	'text|jmeno|'
		),
		'items' => array(
			'id'		=>	'ID|SettingActivityType|id|text|',
			'poradi'	=>	'Pořadí|SettingActivityType|poradi|text|',
			'name'		=>	'Název|SettingActivityType|name|text|',
			'updated'	=>	'Upraveno|SettingActivityType|updated|datetime|',
			'created'	=>	'Vytvořeno|SettingActivityType|created|datetime|'
		),
		'posibility' => array(
			'status'	=> 	'status|Změna stavu|status',
			'edit'		=>	'edit|Editace položky|edit',
			'delete'	=>	'trash|Do košiku|delete'			
		)
	);
	function index(){
		$this->set('fastlinks',array('ATEP'=>'/','Administrace'=>'#','Nastavení stavů firem'=>'#'));
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}
	
	function edit($id = null){
		$this->autoLayout = false;
		if (empty($this->data)){
			if ($id != null)
				$this->data = $this->SettingActivityType->read(null,$id);
			$this->render('edit');
		} else {
			$this->SettingActivityType->save($this->data);
			die();
		}
	}
}
?>