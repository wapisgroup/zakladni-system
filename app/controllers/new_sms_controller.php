<?php 
Configure::write('debug',1);
class NewSmsController extends AppController
{
    var $name = 'NewSms';
	var $helpers = array('htmlExt','Pagination','ViewIndex');
	var $components = array('RequestHandler','NewOrder','Pagination','SmsClient');
	var $uses = array('CompanyOrderItem','ClientView');    
    var $renderSetting = array(
       'controller'=> 'new_sms',
       'clients'=>array(
            'filtration_url'=>'/new_order/set_items/clients/1/',
            'filtration_update'=>'items_clients',
            'filtration_button_br'=>true,
            'filtration_next_button'=>array(
                0 =>array(
                    'label'=>'',
                    'options'=>array('id'=>'move_client_to_sms','class'=>'ta status_new1')
                )
            ),
            'tr_id'=>array(
                'show'=>true,
                'model'=>'ClientView',
                'col'=>'id'
            ),
            'modelClass'=>'ClientView',
    		'permModel' => 'ConnectionClientRecruiter',
            'bindModel' => array(
                'hasOne' => array(
                    'ConnectionClientCareerItem' => array('foreignKey' =>       'client_id'), 
                    'ConnectionClientRecruiter' => array('className' => 'ConnectionClientRecruiter', 'foreignKey' => 'client_id'),
                    'ConnectionClientRequirement' => array('className' =>     
                        'ConnectionClientRequirement', 'foreignKey' => 'client_id',
                         'conditions' =>        array(
                            'ConnectionClientRequirement.kos' => 0,        	'ConnectionClientRequirement.to'=>'0000-00-00',
                             '(ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1)'
                         )
                    )
                )
            ),
            'SQLfields' => '*', 
            'controller' => 'new_order', 
            'page_caption' => 'Klienti',
            'group_by' => 'ClientView.id', 
            'sortBy' => 'ClientView.id.ASC', 
            'top_action' =>array( 
                // caption|url|description|permission
                'add_item' => 'Přidat|edit|Pridat popis|add', 
                'add_express_item' => 'Přidat Express|edit_express|Pridat popis|add_express', 
                'export_excel' => 'Export Excel|export_excel|Export Excel|export_excel', 
            ), 
            'filtration' => array(
                'ClientView-name|' => 'text|Jméno|',
    	        'ClientView-pohlavi_list' => 'select|Pohlaví|pohlavi_list',  
    	        'ClientView-datum_narozeni|year' => 'year|Rok narození|',  
                'ClientView-stav' => 'select|Status|stav_client_list',
                'ConnectionClientCareerItem-setting_career_item_id' => 'select|Profese|profese_list',
                'ConnectionClientRecruiter-cms_user_id#admin_ctrl' => 'select|Recruiter|cms_user_list', 
                'ConnectionClientRequirement-company_id' => 'select|Firma|company_list',
                'ClientView-next|'		=>	'select|Další|next_list',
                'ClientView-mesto|' => 'text|Město|', 
                'ClientView-countries_id' => 'select|Okres|countries_list',  
            ), 
            'items' => array(
                'client_manager_id' => 'cmid|ClientView|client_manager_id|hidden|', 
                'id' => 'ID|ClientView|id|text|',
                'name' => 'Jméno|ClientView|name|text|', 
                'profese' => 'Profese|ClientView|Profese|text|',
                'mobil' => 'Telefon|ClientView|mobil|text|',
                'status' => 'Status|ClientView|stav|var|stav_client_list', 
            ),     
            'posibility' => array(
                'edit' => 'edit|Editace položky|edit',                  
            ), 
            'domwin_setting' => array(
                'sizes' => '[1000,1000]', 
                'scrollbars' => true, 
                'languages' => true 
            )
       )      
	);
	 
     function beforeFilter(){
        parent::beforeFilter();
        
        /**
         * povolit klienty i EN
         */
        $this->ClientView->set_ignore_status(true); 
        if(isset($_GET['filtration_ClientView-next|']) && !empty($_GET['filtration_ClientView-next|'])){
            list($col,$value) = explode('-',$_GET['filtration_ClientView-next|']);

            $this->params['url']['filtration_ClientView-'.$col] = $value;
            
            unset($this->params['url']['filtration_ClientView-next|']); 
            unset($col);
            unset($value);
        }  
    } 
    
    function index(){
        $this->layout = 'order';
        $this->set('template_list',$this->get_list('SmsTemplate'));
        $this->set('company_list',$this->get_list('Company'));
        
        //list obci
        $this->set('countries_list',$this->get_list('Countrie'));
        //profese
        $this->set('profese_list',$this->get_list('SettingCareerItem'));
        
        $this->set('cms_user_list',$this->get_list('CmsUser'));
       /** 
        * dalsi moznosit filtrace
        * moznsosti zadavame sloupec-stav => nazev moznosti
        */
        $this->set('next_list',array(
            'express-1'=> 'Expresní klienti',
            'import_adresa-NOTNULL'=> 'Neprázdna importní adresa',
            'email-NOTNULL'=> 'Neprázdna emailova adresa',
            'externi_nabor-0'=> 'Interní nábor',
            'externi_nabor-1'=> 'Externí nábor'
        ));
        
        // set JS and Style
			$this->set('scripts',array('uploader/uploader','clearbox/clearbox'));
			$this->set('styles',array('../js/uploader/uploader','../js/clearbox/clearbox'));
        
        self::set_items('clients');
        
        self::refresh_waiting_list();
    }
 
    /**
     * Nacitani obsahu klientu
     */
    function set_items($name = null,$render = false){
		if($name == null)
            die('chybny parametr');
        
        $model_class = ($name == 'orders') ? $this->uses[0] : $this->uses[1];
        $renderSetting = $this->renderSetting[$name];
        

        $criteria = $this->NewOrder->filtration($name);
        
        if(!isset($renderSetting['no_trash']))
            $criteria[$model_class.'.kos'] = 0;
		if(isset($renderSetting['count_condition'])){
			$count_criteria = $criteria;
			$count_criteria[] = $renderSetting['count_condition'];
			
		}
			
            
        /**
         * Nastavnei count sloupce, pokud je nastaven v rendersettingu 
         */    
        if(isset($renderSetting['count_col']))
            $this->Pagination->count_col = $renderSetting['count_col'];
	
		$this->ScaffoldModel = $this->{$model_class};
        
        if($render === false)
		  $this->Pagination->paging_name = 'paging_'.$name;
		
        $this->Pagination->ajaxDivUpdate = 'items_'.$name;
			
		$this->Pagination->show = isset($renderSetting['sortShow'])?$renderSetting['sortShow']:20;
		list($field,$model,$direction) = explode('.',isset($renderSetting['sortBy'])?$renderSetting['sortBy']:$model_class.'.name.ASC');
		

		$this->Pagination->recursive = 2;
		$this->Pagination->sortBy = $model;
		$this->Pagination->sortByClass = $field;
		$this->Pagination->direction = $direction;
		
		$this->Pagination->url = '/'.(isset($renderSetting['controller'])?$renderSetting['controller']:$this->controller->params['url']['url']).'/set_items/'.$name.'/1/';
		$this->Pagination->controller = $this;
		if (isset($renderSetting['bindModel']))
			$this->ScaffoldModel->bindModel($renderSetting['bindModel']);
		
		
		if(!isset($renderSetting['bindModel']['hasOne'])) 	$renderSetting['bindModel']['hasOne'] = array();
		if(!isset($renderSetting['bindModel']['belongsTo'])) 	$renderSetting['bindModel']['belongsTo'] = array();
		if(!isset($renderSetting['bindModel']['hasMany'])) 	$renderSetting['bindModel']['hasMany'] = array();
		

		$pagination_bind = $renderSetting['bindModel'];
		
		list($order,$limit,$page) = $this->Pagination->init((isset($count_criteria) ? $count_criteria : $criteria),null,array('modelClass'=>$this->ScaffoldModel->name)); 
		
		if (isset($renderSetting['SQLhaving'])){
			if (is_array($renderSetting['SQLhaving'])){
				$criteria[] = '1=1 Having '.implode(' AND ', $renderSetting['SQLhaving']);
			} else {
				$criteria[] = '1=1 Having '.$renderSetting['SQLhaving'];
			}
		}	

		if (isset($renderSetting['bindModel']))
			$this->{$model_class}->bindModel($renderSetting['bindModel']);
		
			
        if(isset($renderSetting['no_limit']))
            $limit = null;    
            
		$items = $this->{$model_class}->find(
				'all', 
				array(
					'conditions'	=> $criteria, 
					'fields'		=> $renderSetting['SQLfields'], 
					'order'			=> $order, 
					'limit'			=> $limit, 
					'page'			=> $page,
					'recursive' 	=> 1,
					'group'			=> (isset($renderSetting['group_by'])?$renderSetting['group_by']:null)
				)
			);
			
	
        if($render == true){
            $this->set('items',$items);
            $this->set('renderSetting',$renderSetting);
            
            $this->render('items');
        }
        else{
            $this->set('items_'.$name,$items);
        }
	}
 
    /**
     * Editacni karta klienta
     */
    function edit($id = null,$domwin = null, $show = null){
		echo $this->requestAction('clients/edit/'.$id.'/'.$domwin.'/'.$show);
		die();
	}
    
    /**
     * hromadne pridani klientu na waiting list
     */
    public function multi_add_to_wl(){               
            $wrong_client = false;
            if(!empty($this->data['clients'])){
                foreach($this->data['clients'] as $client_id){
                    $return = self::single_add_to_wl($client_id);
                    if($return['result'] == false && isset($return['wrong'])){
                        if($wrong_client == '')
                            $wrong_client = "\nTito klienti jiz jsou umisteni na teto objednavce:\n\n";
                        
                        $wrong_client .= $return['message']."\n";
                    }
                }
                
                die(json_encode(array('result'=>true,'wrong_clients'=>$wrong_client)));
            }
            else{
                die(json_encode(array('result'=>false,'message'=>'Nebyli zvoleni zadni klienti!!')));
            }

    }
    
    /**
     * Pridani jednotliveho klienta
     */
    public function single_add_to_wl($client_id = null){
            if($client_id == null)
                return array('result'=>false);
            
			$this->loadModel('Client');
			$data = $this->Client->read(array('name','id'), $client_id);
			unset($this->Client);
			
			$this->loadModel('SmsWaitingList');
				
				$try_find_con = $this->SmsWaitingList->find('first',array(
					'conditions'=>array(
						'client_id' => $client_id,
					)
				));
				if($try_find_con){
					return array('result'=>false,'wrong'=>true,'message'=>$data['Client']['id'].' - '.$data['Client']['name']);
				}
				

				$connection_to_save = array('SmsWaitingList'=>array(
					'client_id' => $client_id,
					'cms_user_id' => $this->logged_user['CmsUser']['id']
				));
				

				/*
				 * Vlastni ulozeni Connection
				 */
                $this->SmsWaitingList->id = null; 
				if ($this->SmsWaitingList->save($connection_to_save)){
					return array('result'=>true);
				} else {
				    return array('result'=>false);
				}
    }
    
    /**
     * funkce nam obnovuje waiting list
     */
    public function refresh_waiting_list($render = false){
        $this->loadModel('SmsWaitingList');
        $this->SmsWaitingList->bindModel(array('belongsTo'=>array('CmsUser','Client')));
        $this->set('sms_waiting_list',$this->SmsWaitingList->find('all',array(
            'fields'=>array(
              'SmsWaitingList.id','SmsWaitingList.created',
              'Client.id','Client.name','Client.mobil1',
    		  'Client.mobil2','Client.mobil3','Client.mobil_active',
              'CmsUser.name'
            )
        )));
        
        if($render == true){
            $this->render('sms_waiting_list');
        }
    }
    
    public function remove_client_from_wl($connection_id = null){
        if($connection_id == null)
            die(json_encode(array('result'=>false)));
        
        $this->loadModel('SmsWaitingList');
        $this->SmsWaitingList->delete($connection_id);   
        die(json_encode(array('result'=>true)));
    }
    
    
    /**
     * ODESLANI SMS
     */
    public function send(){
        if(!empty($this->data)){
            //ulozeni sms
            $this->data['Sms']['cms_user_id'] = $this->logged_user['CmsUser']['id'];
            $this->loadModel('Sms');
            $this->Sms->save($this->data);
            $group_id = $this->Sms->id;
            unset($this->Sms);     
            
            $this->loadModel('SmsWaitingList');
            $this->SmsWaitingList->bindModel(array('belongsTo'=>array('Client')));
            $sms_waiting_list = $this->SmsWaitingList->find('all',array('fields'=>array(
              'SmsWaitingList.id','SmsWaitingList.created',
              'Client.id','Client.name','Client.mobil1',
    		  'Client.mobil2','Client.mobil3','Client.mobil_active'
            )));
            
            $sms_true = $sms_false = 0;
            foreach($sms_waiting_list as $item_sms){
                $result = self::send_item(array(
        			'message' => $this->data['Sms']['text'],
        			'phone_number' => $item_sms['Client'][$item_sms['Client']['mobil_active']],
        			'group_id' => $group_id,
        			'client_id' =>  $item_sms['Client']['id'],
        			'client_name' => $item_sms['Client']['name'],
        			'model'	=> 'Client'
        		));
                
                if($result['result'] == true)
                    $sms_true++;
                else{
                    $sms_false++;
                }    
                $this->SmsWaitingList->delete($item_sms['SmsWaitingList']['id']);
            }
            
            die(json_encode(array('result'=>true,'sms_true'=>$sms_true,'sms_false'=>$sms_false)));
        }
        else
            die(json_encode(array('result'=>false)));
    }
    
    /**
	 * Odeslani jednotlive SMS
	 * @return JSON
	 */
	function send_item($data){
        return $this->SmsClient->send($data);
	}
}
?>