<?php

//    Configure::write('debug',1);

	$_GET['current_year'] 	= (isset($_GET['current_year']) && !empty($_GET['current_year']))?$_GET['current_year']:date('Y');
	$_GET['current_month'] 	= (isset($_GET['current_month']) && !empty($_GET['current_month']))?$_GET['current_month']:(int)date('m');
	define('CURRENT_YEAR', $_GET['current_year']);
	define('CURRENT_MONTH', ($_GET['current_month'] < 10)?'0'.$_GET['current_month']:$_GET['current_month']);

class ReportForFuPaymentClientsController extends AppController {
	var $name = 'ReportForMuPaymentClients';
	var $helpers = array('htmlExt','Pagination','ViewIndex','Excel');
	var $components = array('ViewIndex','RequestHandler','Email');
	var $uses = array('ConnectionClientRequirement');
	var $renderSetting = array(
		'bindModel'	=> array(
			'belongsTo'	=>	array('Client','Company','RequirementsForRecruitment','CompanyMoneyItem'),
			'hasOne'	=>	array(
				'ClientWorkingHour'=>array(
					'conditions' => array(
						'ClientWorkingHour.year'=> CURRENT_YEAR,
						'ClientWorkingHour.month'=> CURRENT_MONTH
					)
				)
			),
			'joinSpec'	=> array(
				'AtCompany' => array(
					'primaryKey' => 'Company.parent_id',
					'foreignKey' => 'AtCompany.id',
					'conditions' => array(
						'AtCompany.kos' => 0
					)
				)
			)
		),
		'controller'	=>	'report_for_fu_payment_clients',
		'SQLfields' 	=>	'*',
		'SQLcondition'	=>  array(
			"((DATE_FORMAT(ConnectionClientRequirement.from,'%Y-%m')<= '#CURRENT_YEAR#-#CURRENT_MONTH#'))",
			'((DATE_FORMAT(ConnectionClientRequirement.to,"%Y-%m") >= "#CURRENT_YEAR#-#CURRENT_MONTH#") OR (ConnectionClientRequirement.to = "0000-00-00 00:00"))',
			'ConnectionClientRequirement.type'=> 2,
            'ConnectionClientRequirement.new'=> 0,
			'ClientWorkingHour.stav IN (3,4)',
			//'CompanyMoneyItem.checkbox_faktura'=> 1
			
		),
		'page_caption'=>'Výplaty pro finanční účetní',
		'checkbox_setting' => array(
			'model'			=>	'ClientWorkingHour',
			'col'			=>	'stav',
			'col2'			=>	'stop_payment',
			'stav_array'	=>	array(3),
			'month_year'	=>	false
		),
		'sortBy'=>'Client.name.DESC',
		'top_action' => array(
			'multi_edit'		=>	'Vyplatit hromadně|money_all|Vyplatit hromadně|multi_edit',
			'excel'			=>	'Export|export_excel|Vyexportovat do excelu|multi_edit',
		),
		'filtration' => array(
            'Client-name'		=>	'text|Klient|',
			'ConnectionClientRequirement-company_id'	=>	'select|Společnost|company_list',
			'Company-client_manager_id|coordinator_id|coordinator_id2-cmkoo'		=>	'select|CM/KOO|cm_koo_list',
            'GET-current_year'							=>	'select|Rok|actual_years_list',
			'GET-current_month'							=>	'select|Měsíc|mesice_list',
		),
		'items' => array(
			'id'				=>	'ID|ClientWorkingHour|id|hidden|',
			//'cc'				=>	'Client č.|ClientWorkingHour|client_number|text|',
			'company'			=>	'Společnost|Company|name|text|',
			'atcompany'			=>	'Nadřazená společnost|AtCompany|name|text|',
			'client'			=>	'Klient|Client|name|text|',	
			'cislo_uctu'		=>	'Č. účtu|Client|cislo_uctu|text|',	
            'forma_mzdy'		=>	'Forma|CompanyMoneyItem|name|text|',
			'nastup'			=>	'Nástup|ConnectionClientRequirement|from|date|',
			'ukonceni'			=>	'Ukončení|ConnectionClientRequirement|to|date|',
		//	'norhod'			=>	'FPD|ClientWorkingHour|standard_hours|text|',
		//	'svatky'			=>	'Sv|ClientWorkingHour|svatky|text|',
		//	'vikendy'			=>	'Ví|ClientWorkingHour|vikendy|text|',
		//	'pn'				=>	'PN|ClientWorkingHour|pracovni_neschopnost|text|',
		//	'dovolena'			=>	'Do|ClientWorkingHour|dovolena|text|',
		//	'prescasem'			=>	'Př|ClientWorkingHour|prescasy|text|',
			'celkem'			=>	'Celkem|ClientWorkingHour|celkem_hodin|text|',
			'ps'				=>	'PS|ClientWorkingHour|salary_part_1_p|text|',
			'd'					=>	'D|ClientWorkingHour|salary_part_2_p|text|',
			'f'					=>	'F|ClientWorkingHour|salary_part_3_p|text|',
			'c'					=>	'C|ClientWorkingHour|salary_part_4_p|text|',
			'total'				=>	'Total|ClientWorkingHour|salary_per_hour_p|text|',
			'srazka'			=>	'Srážka|ClientWorkingHour|drawback|text|',
			'odmena1'				=>	'Odměna 1|ClientWorkingHour|odmena_1|text|',
			'odmena2'				=>	'Odměna 2|ClientWorkingHour|odmena_2|text|',
	       	'stav'				=>	'Stav|ClientWorkingHour|stav|var|kalkulace_stav_list',
			'stop_payment'		=>	'Výp.|ClientWorkingHour|stop_payment|var|ano_ne'		
		),
		'posibility' => array(
			'stop_status'		=>	'stop|Pozastavit výplatu|stop_status',
			'show'			=>	'odpracovane_hodiny|Karta docházky|show',
			'edit'			=>	'money|Vyplatit|edit'
			
		)
	);

    function beforeFilter(){
        parent::beforeFilter();
        if (isset($_GET['excel'])){
            $this->renderSetting['no_limit'] = true;
        }
    }
	
	function export_excel(){
		$link = '/'.$this->renderSetting['controller'].'/?';
		$sub = array();
		foreach($this->params['url'] as $key=>$get){
			if ($key != 'url'){
				$sub[] = "{$key}={$get}";
			}
		}
		$link .= implode('&',$sub) . '&excel=true';
		$this->redirect($link);
		exit;
	}
	
	function index(){
		//pr($this->viewVars['items']);

		$this->set('fastlinks',array('ATEP'=>'/','Reporty'=>'#','Výplaty pro finanční účetní'=>'#'));
			
		$this->loadModel('CmsUser');
        	$this->set('cm_koo_list',$cm_koo_list = $this->CmsUser->find('list',array(
    			'conditions'=>array(
    				'kos'=>0,
    				'status'=>1,
    				'cms_group_id IN(3,4)'
    			)
    		)));	$this->loadModel('Company');	
		
        $this->set('company_list',$this->Company->find('list',array('order'=>'name ASC')));

        /**
         * Natahnuti celkove sumace za vsechny (total)
         */
        $this->ConnectionClientRequirement->bindModel($this->renderSetting['bindModel']);
        $total = $this->ConnectionClientRequirement->find('first',array(
                'fields' => array(
                    'SUM(salary_per_hour_p) as total'
                ),
                'conditions' => $this->ViewIndex->criteria
        ));
        list($total) = array_values($total);

        $total = empty($total['total'])?0:$total['total'];
        $this->set('list_of_sums',array('Suma za všechny podniky'=>$total));


		if (isset($_GET['excel'])){
			$this->autoLayout = false;
			echo $this->render('../system/excel');
			die();
		}
	
		if ($this->RequestHandler->isAjax()){
			$this->render('../system/items');
		} else {
			$this->render('../system/index');
		}
	}

	function money($id){
		$this->loadModel('ClientWorkingHour');
		$this->ClientWorkingHour->id = $id;
		$this->ClientWorkingHour->saveField('stav',4);
		unset($this->ClientWorkingHour);
		die();
	}

	function stop($id,$status){
		$this->loadModel('ClientWorkingHour');
		$this->ClientWorkingHour->id = $id;
		$this->ClientWorkingHour->saveField('stop_payment',$status==1 ? 0 :1);
		unset($this->ClientWorkingHour);
		die();
	}

	function odpracovane_hodiny($id = null, $year = null, $month = null){
		echo $this->requestAction('employees/odpracovane_hodiny/'.$id.'/'.$year.'/'.$month.'/');
		die();
	}

	function money_all(){
		$model = $this->renderSetting["checkbox_setting"]['model'];
		$this->loadModel($model);
		foreach($this->params['url']['data'][$model]['id'] as $key => $on){
			$this->$model->id = $key;
			$this->$model->saveField('stav',4);
		}
		unset($this->$model);	
		die();
	}
	
	
}
?>