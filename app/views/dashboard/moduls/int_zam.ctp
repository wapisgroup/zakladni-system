<h2>Int. zaměstnanci:</h2>
<table class='table'>
	<tr>
		<td>Měsíc</td>
		<td><?php echo $htmlExt->input('int_zam_mesic',array('value'=>$date,'class'=>'months'));?></td>
	</tr>
	
    <?php
    
        foreach($int_zam_data as $key=>$item){
            echo '<tr>';
            echo '<td>'.$item['label'].'</td>';
            echo '<td>';
                if(isset($item['type']) && $item['type'] == 'money')
                    echo $fastest->price($item['value'],',-');
                else
                    echo $item['value'];    
            echo '</td>';
            echo '</tr>';
        }
    
    ?>
    
</table>