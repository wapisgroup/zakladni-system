<h2>Firmy:</h2>
<table class='table'>
	<tr>
		<td>Datum</td>
		<td colspan="2"><?php echo $htmlExt->inputDate('firmy_datum',array('value'=>$date,'class'=>''));?></td>
	</tr>
	<tr>
		<td></td>
		<td><strong>ČR</strong></td>
		<td><strong>SR</strong></td>
	</tr>
    <?php
    
        foreach($firmy_data as $key=>$item){
            echo '<tr>';
            echo '<td>'.$item['label'].'</td>';
            echo '<td>';
            if(!is_array($item['value'])){
            
                if(isset($item['type']) && $item['type'] == 'money')
                    echo $fastest->price($item['value'],',-');
                else
                    echo $item['value'];   
            }
            else{
                foreach($item['value'] as $id=>$val){
                    if(isset($item['type']) && $item['type'] == 'money')
                        echo $fastest->price($val,',-');
                    else
                        echo $val;  
                        
                    if(isset($item['value'][$id+1]))
                        echo '</td><td>';     
                }
                
            }        
                     
            echo '</td>';
            echo '</tr>';
        }
    
    ?>
    
</table>