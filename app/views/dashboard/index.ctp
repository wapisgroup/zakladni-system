<h1>Dashboard</h1>
<div id="dashboard">
    <div class="sll3">
        <div id="fakturace">
            <?php echo $this->renderElement('../dashboard/moduls/fakturace');?>
        </div>
        <div id="dochazky">
            <?php echo $this->renderElement('../dashboard/moduls/dochazky');?>
        </div>
        <div id="firmy">
            <?php echo $this->renderElement('../dashboard/moduls/firmy');?>
        </div>
        <div id="int_zam">
            <?php echo $this->renderElement('../dashboard/moduls/int_zam');?>
        </div>
    </div>
    <div class="sll66">
        <div id="objednavky">
            <?php echo $this->renderElement('../dashboard/moduls/objednavky');?>
        </div>
        <br />
        <div class="sll3" id="oddeleni_naboru">
            <?php echo $this->renderElement('../dashboard/moduls/oddeleni_naboru');?>
        </div>
        <div class="sll3" id="oddeleni_realizace">
            <?php echo $this->renderElement('../dashboard/moduls/oddeleni_realizace');?>
        </div>
        <div class="sll3" id="oddeleni_obchodu">
             <?php echo $this->renderElement('../dashboard/moduls/oddeleni_obchodu');?>
        </div>
        <br />
    </div>
</div>
<br />
<script>

//funkce pro mesice ktera vyvolava MonthCal
function check_months(){ 
    $each($('dashboard').getElements('.months'),function(item){
        var id = item.getParent('div').id;
        var div_id = id;
        var coo_id = '';
        id = ucfirst(id.camelCase2());
      
        window.addEvent('domready',function(){
            new MonthCal($(id+'Mesic'),{onComplete: function (){
                if($(div_id).hasChild('CoordinatorId')){
                    coo_id = ($('CoordinatorId').value == null ? 0 : $('CoordinatorId').value);
                }    
                
                if(coo_id == '')
                    ajax_load_module(div_id,$(id+'Mesic').value);
                else
                    ajax_load_module(div_id,$(id+'Mesic').value,coo_id);
           	}})
        });
        
    
    });
}    
check_months();

//load jednotlivych modulu
function ajax_load_module(id,date,coo_id){
        button_preloader($(id));
        if(typeof(coo_id) == "undefined")
            coo_id = '';
        
		new Request.HTML({
			url: '/dashboard/ajax_load_module/'+id+'/'+date + '/'+coo_id,
            update: id,
			onComplete: (function(){
			     button_preloader_disable($(id));
                 check_months();
                 
                 if(id == 'firmy')
                    $('calbut_FirmyDatum').addEvent('change',function(e){
                      ajax_load_module('firmy',this.value);
                    });
                    
                 else if(id == 'objednavky')
                    $('calbut_ObjednavkyDatum').addEvent('change',function(e){
                      ajax_load_module('objednavky',this.value);
                    });   
			})
		}).send();
}


$('CoordinatorId').addEvent('change',coo_event);
//specialni por koordinatory select cv dochazkovem modulu
function coo_event(e){
    e.stop();
    var my_url = '/dashboard/ajax_load_module/dochazky/'+$('DochazkyMesic').value;   
    
    if(this.value != '')
       my_url += '/'+this.value;
                
       button_preloader($('dochazky'));
        	new Request.HTML({
    			url: my_url,
                update: 'dochazky',
    			onComplete: (function(){
    			     button_preloader_disable($('dochazky'));
                     check_months();
                     $('CoordinatorId').addEvent('change',coo_event);
    			})
    		}).send();   
            
}

//datum ve firmach
$('calbut_FirmyDatum').addEvent('change',function(e){
   ajax_load_module('firmy',this.value);
});

//datum ve objednavky
$('calbut_ObjednavkyDatum').addEvent('change',function(e){
   ajax_load_module('objednavky',this.value);
});

</script>