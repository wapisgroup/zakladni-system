<?php
if (count($renderSetting['filtration'])>6)
	$sirka_filtrace = 'filtration_big';
else	
	$sirka_filtrace = 'filtration_small';
?>

<div id="filtration" class="<?php echo $sirka_filtrace;?>"><?php echo $viewIndex->filtration($renderSetting,$this->viewVars, $logged_user);?></div>
<div class="lista_add" id='lista_add'><?php echo $viewIndex->top_action($renderSetting, $logged_user);?></div>
<h1><?php echo $renderSetting['page_caption'];?></h1>
<form action='#' method='post' id='items_form'>
	<div id='items'><?php echo $this->renderElement('../system/items');?></div>
</form>
<div id="print_datum"><?= date('d.m.Y H:i:s');?></div>
<?
if(isset($renderSetting['thunderbird'])){
    echo '<div id="ThunderBirdBox"></div>';
}
?>
<div id='legenda'>
	<h3>Legenda</h3>
	<?php echo $viewIndex->legend_render($renderSetting);?>
</div>
<?php //pr($items);?>
<script>
$('filtr_button').addEvent('click', function(e){
	new Event(e).stop();
	new Request.HTML({
		update: 'items',
		url: $('filtration_view_index').action
	}).get($('filtration_view_index'));
});

if ($('filtr_button_cancel')){
	$('filtr_button_cancel').addEvent('click', function(e){
		new Event(e).stop();
		$('filtration_view_index').getElements('.fltr_input, .fltr_select').each(function(item){item.value = ''})
        $('filtration_view_index').getElements('.fltr_checkbox').each(function(item){item.checked = false;})
		//preloader(true);
		new Request.HTML({
			update: 'items',
			url: $('filtration_view_index').getProperty('action'),
			onComplete: function(){
			//	preloader(false);
			}
		}).get($('filtration_view_index'));
	});
}


if ($$('.filtr_more')){
$$('.filtr_more').addEvent('click', function(){
    var filtr = this.getParent('.filtration_big');
    var pole = filtr.getElement('div[id="pole"]');
	if (filtr.hasClass('big_filtr')){
		filtr.removeClass('big_filtr');
		filtr.removeClass('up');
		filtr.tween('height', 44);
        pole.tween('height', 44);
	} else {
		var pocet = this.getProperty('rel');
		
		var vyska_filtrace = Math.ceil(pocet/3)*23;
		filtr.addClass('big_filtr');
		filtr.addClass('up');
		filtr.tween('height', vyska_filtrace);
        pole.tween('height', vyska_filtrace);
	}
	return false;
});
}

if ($('lista_add').getElement('.groups,.group,.items')){
	$('lista_add').getElement('.groups,.group,.items').addEvent('click', function(e){
		
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_groups',
			sizes		: [600,450],
			scrollbars	: 'true',
			defined_lang: [<?php echo implode(',',array_map(create_function('$item', 'return  \'"\'.$item.\'"\';'),array_keys($languages))); ?>],
			languages	: false,
			title		: this.title,
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
}

if ($('lista_add').getElement('.template')){
	$('lista_add').getElement('.template').addEvent('click', function(e){
		
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_groups',
			sizes		: [600,450],
			scrollbars	: 'true',
			languages	: false,
			title		: this.title,
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});
}


if ($('lista_add').getElement('.transport_card')){
	$('lista_add').getElement('.transport_card').addEvent('click', function(e){
	   
       var ids = '';
       $each($('tabulka_item').getElement('tbody').getElements('input[type="checkbox"]'),function(item){
            if(item.checked){
                ids += item.id.substr(9)+'|';
            }
       })
       //console.log(ids);
		
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_cards',
			sizes		: [600,450],
			scrollbars	: 'true',
			languages	: false,
			title		: this.title,
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true,
            post_data : $('filtration_view_index').toQueryString()+'&id='+ids.substr(0,ids.length-1)
		}); 
	});
}

if ($('lista_add').getElements('.edit,.convert_users,.type_works,.newsletter,.blacklist,.setting, .edit_express, .email_send, .kb, .company_exception,.import_csv,.money_item')){
	$('lista_add').getElements('.edit,.convert_users,.type_works,.newsletter,.blacklist,.setting,  .edit_express, .email_send, .kb, .company_exception,.import_csv,.money_item').addEvent('click', function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin',
			sizes		: <?php echo (isset($renderSetting['domwin_setting']['sizes']))?$renderSetting['domwin_setting']['sizes']:'[600,450]'?>,
			scrollbars	: <?php echo (isset($renderSetting['domwin_setting']['scrollbars']))?$renderSetting['domwin_setting']['scrollbars']:'true'?>,
			languages	: <?php echo (isset($renderSetting['domwin_setting']['languages']))?$renderSetting['domwin_setting']['languages']:'false'?>,
			defined_lang: [<?php echo implode(',',array_map(create_function('$item', 'return  \'"\'.$item.\'"\';'),array_keys($languages))); ?>],
			title		: this.title,
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
	});
}

if ($('lista_add').getElements('.money_all')){
	$('lista_add').getElements('.money_all').addEvent('click', function(e){
		new Event(e).stop();
			if (confirm('Opravdu si přejete hromadně vyplatit?')){
				new Request.JSON({
					url:this.href,
					onComplete: function(json){
						click_refresh();
						//preloader(false);
						if (json){
							json.each(function(id){
								ch = $('checkbox_'+id);
								if (ch){
									but = ch.getParent('tr').getElement('.ta money');
									if (but)
										but.addClass('none');
								}
							});
						}
					}
				}).get($('items_form'));
			}	
	});
}


if ($('lista_add').getElements('.export_excel')){
	$('lista_add').getElements('.export_excel').addEvent('click', function(e){
		new Event(e).stop();
			
        window.location = this.href+'/?'+$('filtration_view_index').toQueryString();

	});
}


if ($('lista_add').getElements('.paid_all')){
	$('lista_add').getElements('.paid_all').addEvent('click', function(e){
		new Event(e).stop();
			if (confirm('Opravdu si přejete hromadně uhradit?')){
				new Event(e).stop();
        		domwin.newWindow({
        			id			: 'domwin_uhrazeno',
        			sizes		: <?php echo '[400,250]';?>,
        			scrollbars	: <?php echo (isset($renderSetting['domwin_setting']['scrollbars']))?$renderSetting['domwin_setting']['scrollbars']:'true'?>,
        			languages	: <?php echo (isset($renderSetting['domwin_setting']['languages']))?$renderSetting['domwin_setting']['languages']:'false'?>,
        			defined_lang: [<?php echo implode(',',array_map(create_function('$item', 'return  \'"\'.$item.\'"\';'),array_keys($languages))); ?>],
        			title		: this.title,
        			ajax_url	: this.href,
                    post_data   : $('items_form'),
        			closeConfirm: true,
        			max_minBtn	: false,
        			modal_close	: false
        		}); 
			}	
	});
}

// pro evidenci majetku
if ($('filtr_AuditEstate-at_project_id') && $('filtr_AuditEstate-at_project_centre_id')){
	$('filtr_AuditEstate-at_project_id').ajaxLoad('/audit_estates/get_stredisko/',['filtr_AuditEstate-at_project_centre_id']);
}
if ($('filtr_ClientZam-at_project_id') && $('filtr_ClientZam-at_project_centre_id')){
	$('filtr_ClientZam-at_project_id').ajaxLoad('/audit_estates/get_stredisko/',['filtr_ClientZam-at_project_centre_id']);
}
if ($('filtr_ReportContract-at_project_id') && $('filtr_ReportContract-at_project_centre_id')){
	$('filtr_ReportContract-at_project_id').ajaxLoad('/audit_estates/get_stredisko/',['filtr_ReportContract-at_project_centre_id']);
}
if ($('filtr_ReportFuelConsumption-at_project_id') && $('filtr_ReportFuelConsumption-at_project_centre_id')){
	$('filtr_ReportFuelConsumption-at_project_id').ajaxLoad('/audit_estates/get_stredisko/',['filtr_ReportFuelConsumption-at_project_centre_id']);
}

</script>