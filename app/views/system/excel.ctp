<?php
    $output = array();
    $headers  = array();
    $k = 0;
    foreach($items as $it){
        $row = array();
        foreach($renderSetting['items'] as $data){
            list($caption, $model, $column, $type,$prop) = explode('|',$data);
            switch ($type){
                case 'var':
                    $var = $$prop;
                    $row["$model.$column"] = @$var[$it[$model][$column]];
                    break;
                case 'pocet_dni_po_splatnosti':
    				$po_splatnosti = (strtotime(date('Y-m-d')) - strtotime($it[$model]['datum_splatnosti'])) / 3600 /24;
    				if ($po_splatnosti > 0) {
    					if ($it[$model]['datum_uhrady'] != '0000-00-00')
    						$row["$model.$column"] = 'Uhrazeno';
    					else 
    						$row["$model.$column"] =  $po_splatnosti;
    				} else
    					$row["$model.$column"] = null ;
				break;    
                default:
                    $row["$model.$column"] = @$it[$model][$column];
            }
            
            if ($k == 0)
                $headers["$model.$column"] = $caption;
        }
        $output[] = $row;
        $k = 0;
    }
    //pr($headers);
    
//   die();
    
    if (isset($excel_render)){
        $excel->$excel_render($items, $souhrn); 
    }
    else
        $excel->generate($output, $page_caption, $headers); 
?>