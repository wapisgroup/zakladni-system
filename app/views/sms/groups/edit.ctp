<form id='add_edit_group_formular' action='/sms/group_edit/' method='post'>
	<?php echo $htmlExt->hidden('SmsTemplate/id');?>
	<fieldset>
		<legend>Základní</legend>
		<?php echo $htmlExt->input('SmsTemplate/name',array('label_class'=>'long','class'=>'long','label'=>'Název skupiny'));?><br />
	   <?php echo $htmlExt->textarea('SmsTemplate/text',array('tabindex'=>8,'label'=>'Text sms','class'=>'long','label_class'=>'long','MAXLENGTH' => 160 ));?> <br class="clear">
<label class='long'>Počet možných znaků:</label><var class='long' id='sms_len'>160</var><br />
	
    </fieldset>	
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditGroupSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditGroupClose'/>
	</div>
</form>
<script>
$('sms_len').setHTML(160 - $('SmsTemplateText').value.length);
	
	$('SmsTemplateText').addEvents({
		'keyup': function(){
		
			$('sms_len').setHTML(160 - this.value.length);
			if (this.value.length >= 160){
				this.value = this.value.substring(0,160);
                $('sms_len').setHTML(0);
            }    
		},
		'keydown': function(e){
			var event = new Event(e);
			//alert(event.code);
			//zamezeni vlozeni &
			if(event.code == 55)
				return false;
		}
	});

$('AddEditGroupClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_group_edit');});
$('AddEditGroupSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('add_edit_group_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('add_edit_group_formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
							domwin.loadContent('domwin_groups');
							domwin.closeWindow('domwin_group_edit');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
					
				}
			}).post($('add_edit_group_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	
	validation.define('add_edit_group_formular',{
		'SmsTemplateName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název šablony'}
		},
        'SmsTemplateText': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit text šablony'}
		}		
	});
	validation.generate('add_edit_group_formular',<?php echo (isset($this->data['SmsTemplate']['id']))?'true':'false';?>);

</script>