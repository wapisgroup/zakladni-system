<form action='/at_companies/sharing_company/' method='post' id='setting_project_centre_formular'>
	<?php echo $htmlExt->hidden('AtProjectCompany/at_company_id');?>
	<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom2">
		<div class="domtabs field">
			<fieldset>
				<legend>Projekty</legend>
				<?php 
                foreach($project_list as $p_id=>$project){
                    echo $htmlExt->checkbox('AtProjectCompany/projects/'.$p_id,null,array('tabindex'=>1,'label'=>$project, 'class'=>'','label_class'=>'long')).'<br />';
                }
                ?>
			</fieldset>
	    </div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'ssave_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'cclose','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">

    var domtab = new DomTabs({'className':'admin_dom2'}); 
	$('ssave_close').addEvent('click',function(e){
		new Event(e).stop();
			new Request.JSON({
				url:$('setting_project_centre_formular').action,		
				onComplete:function(){
				    click_refresh($('AtProjectCompanyAtCompanyId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('setting_project_centre_formular'));
	});
	
	$('cclose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>