  <?php if(isset($filtration_sum_variables2)){
    $pagination->ignored_params = array();
    $sum_coo = $sum_mr = 0;
    foreach($filtration_sum_variables2 as $sum_vars){
        if(in_array($sum_vars['CompanyView']['id'],$_ignored_items)){continue;}
        
        $sum_coo += $sum_vars['0']['srazka_coo'];
        $sum_mr += $sum_vars['0']['srazka_mr'];
    }
    
    ?>
    <div id="filtration_variables2">
        <div class="sll">
            Celkem Koo: <strong><?= $fastest->price($sum_coo);?></strong><br />
        </div>
        <div class="slr">
            Celkem MR: <strong><?= $fastest->price($sum_mr);?></strong><br />    
        </div>
    </div>
    <?php }