<div class="domtabs admin_dom_links">
	<ul class="zalozky">
		<li class="ousko"><a href="#krok1">Aktivita</a></li>
		<li class="ousko"><a href="#krok2">Přiřazený úkol</a></li>
	</ul>
</div>
<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Aktivita</legend>
				<div class='sll'>
					<label>Název aktivity:</label><var><?php echo $this->data['CompanyActivity']['name'] ?></var><br />
					<label>Město:</label><var><?php echo $this->data['CompanyActivity']['mesto'] ?></var><br />
					<label>Vytvořil:</label><var><?php echo $this->data['CmsUser']['name'] ?></var><br />
				</div>
				<div class='slr'>
					<label>Datum aktivity:</label><var><?php echo $fastest->czechDateTime($this->data['CompanyActivity']['activity_datetime']) ?></var><br />
					<label>Společnost:</label><var><?php echo $this->data['Company']['name'] ?></var><br />
					<label>Kontatní osoba:</label><var><?php echo $this->data['CompanyContact']['name'] ?></var><br />
				</div>
			</fieldset>
			<fieldset>
				<legend>Popis aktivity</legend>
				<p style='min-height:110px'><?php echo nl2br($this->data['CompanyActivity']['text']) ?></p>
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Přidružený úkol</legend>
				<?php echo $htmlExt->var_text('CompanyTask/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php echo $htmlExt->var_text('CompanyTask/termin',array('tabindex'=>1,'label'=>'Termín'));?> <br class="clear">
					<?php echo $htmlExt->var_text('CmsUser/name',array('tabindex'=>2,'label'=>'Pro'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->var_text('CompanyTask/SettingTaskType/name',array('tabindex'=>2,'label'=>'Typ aktivity'));?> <br class="clear">
					<?php echo $htmlExt->var_text('Company/name',array('tabindex'=>2,'label'=>'Firma'));?> <br class="clear">
				</div>
				<br class='clear' />
			</fieldset>
			<fieldset>
				<legend>Popis úkolu</legend>
				<p style='min-height:110px'><?php echo nl2br($this->data['CompanyTask']['text']) ?></p>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='AddEditCompanyActivityClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditCompanyActivityClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_show');
	});
</script>
