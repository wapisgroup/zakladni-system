
<h1>Klienti v mém vlastnictví</h1>

<table id="tabulka_item" class="table">
<thead>
    <tr>
        <th>Pracovník</th>
        <th>Způsob</th>
        <th>Vytvořen</th>
        <th>Zaplacen</th>
        <th>Možnosti</th>
    </tr>
</thead>
<tbody>

<? foreach($own_clients as $own): ?>
     <tr><td><?= $own['Client']['name']; ?></td>
         <td><?= (isset($own['ClientBuy']['id'])?'Obj. '.$own['ClientBuy']['id']:'Vytvořen'); ?></td>
         <td><?= (isset($own['ClientBuy']['id'])?date("j. n. Y", strtotime($own['ClientBuy']['from_date'])):date("j. n. Y", strtotime($own['Client']['created']))); ?></td>
         <td><?= (isset($own['ClientBuy']['id'])?(isset($own['ClientBuy']['pay_date'])?'ano':'ne'):'') ?></td>
         <td><a href="/int_clients/client_buy/<?= $own['Client']['id']; ?>">Detail</a></td>
     </tr>
<? endforeach; ?>
</tbody>
</table>