
<h1>Klienti v mém vlastnictví</h1>

<table id="tabulka_item" class="table">
<thead>
    <tr>
        <th>Pracovník</th>
        <th>Způsob</th>
        <th>Vytvořen</th>
        <th>Zaplacen</th>
        <!--<th>Možnosti</th>-->
    </tr>
</thead>
<tbody>

<? foreach($own_clients as $own): ?>
     <tr><td><?= $own['MyClientsView']['client_name']; ?></td>
         <td><?= (isset($own['MyClientsView']['id'])?'Obj.č. '.$own['MyClientsView']['id']:'Vytvořen'); ?></td>
         <td><?= (isset($own['MyClientsView']['id'])?date("j. n. Y", strtotime($own['MyClientsView']['from_date'])):date("j. n. Y", strtotime($own['MyClientsView']['created']))); ?></td>
         <td><?= (isset($own['MyClientsView']['id'])?(isset($own['MyClientsView']['pay_date'])?'ano':'ne'):'') ?></td>
     </tr>
<? endforeach; ?>
</tbody>
</table>