
<div class="domtabs noprint">
<ul class="zalozky">
    <li class="ousko"><a href="#krok1">Objednávka klienta</a></li>
</ul>
</div>

<div class="domtabs client_buy">
 <div class="domtabs field">

            <div class="tab_content">
<? if($owner['CmsUser']['name'] == -1): ?>
    Tohoto klienta nelze objednat, chybi vlastnik.
<? else: ?>
  <? if($mine != true): ?>
    <? if(isset($id)): ?>
                <label>Klient:</label> <?= $client['Client']['name']; ?><br />
                <label>Vlastník:</label> <?= $owner['CmsUser']['name']; ?><br />

            <form id="client-buy" action="/int_clients/client_buy/<?= $id; ?>">
                <?php echo $htmlExt->hidden('ClientBuy/from_cms_user_id', array('tabindex' => 1, 'value'=>$owner['CmsUser']['id'])); ?>
                <?php echo $htmlExt->hidden('ClientBuy/to_cms_user_id', array('tabindex' => 1, 'value'=>$cms_user_id,null, true)); ?>
                <?php echo $htmlExt->hidden('ClientBuy/id', array('tabindex' => 1, value=>$owner['ClientBuy']['id'])); ?>

                <?php echo $htmlExt->selectTag('ClientBuy/setting_career_item_id', $work_list ,( $owner['ClientBuy']['setting_career_item_id'] ?  $owner['ClientBuy']['setting_career_item_id'] : $request['ClientBuy']['setting_career_item_id']), array('label'=>'Pozice', 'class'=>''),null, false); ?><br />
                <? if($owner['ClientBuy']['from_date']): ?>
                    <label>Zakoupen:</label> <?= $owner['ClientBuy']['from_date']; ?><br />
                <? endif; ?>
                <? if(!isset($request)): ?>
                       <label>Stav:</label> Dostupný k pronajmutí <br />
                       <?php echo $htmlExt->hidden('Action', array('tabindex' => 1, value=>'buy')); ?>
                       <input type="button" name="submit-buy" id="submit-buy" value="Zakoupit" /><br />

                <? else: ?>
                   <?php echo $htmlExt->hidden('Request/id', array('tabindex' => 1, value=>$request['ClientBuy']['id'])); ?>

                   <? if($request['ClientBuy']['to_cms_user_id'] != $cms_user_id ): ?>
                       <label>Stav:</label> Žádost o převod klientem <?= $request['CmsUser']['name']; ?><br />
                       <?php echo $htmlExt->hidden('Request/to_cms_user_id', array('tabindex' => 1, value=>$request['ClientBuy']['to_cms_user_id'])); ?>
                       <?php echo $htmlExt->hidden('Action', array('tabindex' => 1, value=>'request_to_free')); ?>
                       <?php echo $htmlExt->textArea('RequestText'); ?><br />
                       <input type="button" name="submit-buy" id="submit-buy" value="Požádat o uvolnění" /><br />

                   <? else: ?>
                       <label>Stav:</label> Neuhrazen ( provedl jste objednávku ) <br />
                       <?php echo $htmlExt->hidden('Action', array('tabindex' => 1, value=>'make_free')); ?>
                       <input type="button" name="submit-buy" id="submit-buy" value="Uvolnit" /><br />

                   <? endif; ?>
                <? endif; ?>

        <? else: ?>

           <? if(!isset($request)): ?>
                       <?php echo $htmlExt->hidden('Action', array('tabindex' => 1, value=>'buy')); ?>
                       <input type="button" name="submit-buy" id="submit-buy" value="Zakoupit" /><br />
                       <br />

           <? else: ?>
             <?php echo $htmlExt->hidden('Request/id', array('tabindex' => 1, value=>$request['ClientBuy']['id'])); ?>

             <? if($request['ClientBuy']['to_cms_user_id'] != $cms_user_id ): ?>
                    <label>Stav:</label> Žádost o převod klientem <?= $request['CmsUser']['name']; ?><br />
                    <?php echo $htmlExt->hidden('Request/to_cms_user_id', array('tabindex' => 1, value=>$request['ClientBuy']['to_cms_user_id'])); ?>
                    <?php echo $htmlExt->hidden('Action', array('tabindex' => 1, value=>'request_to_free')); ?>
                    <?php echo $htmlExt->textArea('RequestText'); ?><br />
                    <input type="button" name="submit-buy" id="submit-buy" value="Požádat o uvolnění" /><br />

             <? else: ?>
                    <label>Stav:</label> Neuhrazen ( provedl jste objednávku ) <br />
                    <?php echo $htmlExt->hidden('Action', array('tabindex' => 1, value=>'make_free')); ?>
                    <input type="button" name="submit-buy" id="submit-buy" value="Uvolnit" /><br />

             <? endif; ?>
           <? endif; ?>

        <? endif; ?>
            </form>
    <? else: ?>
     <fieldset>
       <div class="tab_content">

               <label>Klient: </label><?= $client['Client']['name']; ?><br />
               <label>Vlastník:</label> <?= $owner['CmsUser']['name']; ?><br />
               <?php echo $htmlExt->hidden('ClientBuy/from_cms_user_id', array('tabindex' => 1, 'value'=>$owner['CmsUser']['id'])); ?>
               <?php echo $htmlExt->hidden('ClientBuy/to_cms_user_id', array('tabindex' => 1, 'value'=>$cms_user_id,null, true)); ?>
               <?php echo $htmlExt->hidden('ClientBuy/id', array('tabindex' => 1, value=>$owner['ClientBuy']['id'])); ?>
               <label>Pozice:</label> <?php echo $htmlExt->selectTag('ClientBuy/setting_career_item_id', $work_list ,( $owner['ClientBuy']['setting_career_item_id'] ?  $owner['ClientBuy']['setting_career_item_id'] : $request['ClientBuy']['setting_career_item_id']), array('disabled'=>'disabled', 'class'=>''),null, false); ?><br />
               <label>Zakoupen:</label> <?= $owner['ClientBuy']['from_date']; ?><br />
               <label>Cena:</label> <?= $owner['ClientBuy']['price']; ?><br />

               <? if($request): ?>
                    <div id="request">
                      <fieldset>
                         Uživatel <b><?= $request['CmsUser']['name']; ?></b> má o tohoto pracovníka zájem<br />
                         Pro jeho převedení je nutné jej vyfakturovat
                      </fieldset>
                    </div>
               <? endif; ?>
        </div>
        </fieldset>
    <? endif; ?>
<? endif; ?>
        </div>
 </div>
</div>
<style>
.tab_content{
     padding: 15px;
     font-size: 13px;
     vertical-align: center;
     line-height:20px;
}
.tab_content form, .tab_content select{
    font-size: 11px;
}
.tab_content select{
    width: 30%;
}
.tab_content input{
    margin-top: 15px;
}
.tab_content label{
    width: 12%;
    font-size: 11px;
    margin: 0px;
    padding: 0px;
    line-height:20px;
    vertical-align: center;
    font-weight: bold;
}
.client_buy{
    margin: auto;
}
.tab_content #request{
    font-size: 12px;
    margin-top: 12px;
    text-align:center;
}
</style>
<script language="javascript" type="text/javascript">


 $('submit-buy').addEvent('click',function(e){
		send_data( $('submit-buy'));
 });

     function send_data(obj){
         var form = obj.getParent('form');
         new Request.JSON({
             url:form.getProperty('action'),
             onComplete:function (JSON) {
                 alert(JSON.msg);
                 domwin.closeWindow('domwin');
             }
         }).send(form)
     }
</script>