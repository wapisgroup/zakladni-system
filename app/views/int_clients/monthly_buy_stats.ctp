
<h1>Měsíční fakturace klientů</h1>

<table id="tabulka_item" class="table">
<thead>
    <tr>
        <th>Poskytovatel</th>
        <th>Středisko</th>
        <th>Odberatel</th>
        <th>Pronajem</th>
        <th></th>
    </tr>
</thead>
<tbody>

<? foreach($orders as $own): ?>
     <tr>
        <td><?= $project_list[$own['Poskytovatel']['at_project_id']]; ?></td>
        <td><?= $center_list[$own['Poskytovatel']['at_project_centre_id']]; ?></td>
        <td><?= $project_list[$own['Odberatel']['at_project_id']]; ?></td>
        <td><?= $own[0]['price_sum']; ?></td>
        <td> <a href="/int_clients/monthly_detail/<?= $own['Poskytovatel']['at_project_id'].'/'.$own['Odberatel']['at_project_id'].'/'.$own['Poskytovatel']['at_project_centre_id']; ?>/">Detail</a> </td>
     </tr>
<? endforeach; ?>
</tbody>
</table>