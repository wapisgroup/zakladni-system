<form action='/cms_users/change_password/' method='post' id='cms_user_edit_formular'>
	<?php echo $htmlExt->hidden('CmsUser/id');?>
	<div>
			<fieldset>
				<legend>Přístupové heslo do sytému systém</legend>
				<div class="sll">  
					<?php echo $htmlExt->password('CmsUser/heslo',array('tabindex'=>3,'label'=>'Heslo','value'=>''));?> <br class="clear"/>
				</div>
				<div class="slr">  
					<?php echo $htmlExt->password('CmsUser/heslo2',array('tabindex'=>4,'label'=>'Potvrzení hesla'));?> <br class="clear"/>
				</div>
			</fieldset>

	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Změnit heslo',array('id'=>'save_close','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">


	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
        
        if($('CmsUserHeslo').value == $('CmsUserHeslo2').value){
    		new Request.JSON({
    			url:$('cms_user_edit_formular').action,		
    			onComplete:function(){
    				//click_refresh($('CmsUserId').value);
                    alert('Heslo bylo změněno.');
    				domwin.closeWindow('domwin_change_password');
    
    			}
    		}).post($('cms_user_edit_formular'));
        }
        else{
            alert('Hesla nejsou stejná!!!');
        }
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_change_password');});
</script>