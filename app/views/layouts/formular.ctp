<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang='cs' xml:lang='cs' xmlns='http://www.w3.org/1999/xhtml'>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv='Content-language' content='cs' />
	<meta name='description' content='CMS Fastest Admin'/>
	<meta name='keywords' content='CMS'/>
	<meta name="robots" content="all,noindex,nofollow" />
	<meta name='Author' content='Fastest Solution, url: http://www.fastest.cz' />
	<meta name='Copyright' content='Fastest Solution, All Rights Reserved url: http://www.fastest.cz' />
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="pragma" content="no-cache"/>
	<link href='/css/fastest/icons/favicon.ico' rel='shortcut icon' />
	<link href='/css/reset.css' rel='stylesheet' type='text/css' media='screen' />
	<!--<link href='/css/print.css' rel='stylesheet' type='text/css' media='print' />-->
	<?php if (isset($_GET['fastest'])): ?>
		<link href='/css/fastest_own/menu.css' rel='stylesheet' type='text/css' media='screen' />
		<link href='/css/fastest_own/style.css' rel='stylesheet' type='text/css' media='screen' />
		<link href='/js/domwin/domwin_own.css' rel='stylesheet' type='text/css' media='screen' />
		<link href='/js/domtabs/domtabs_own.css' rel='stylesheet' type='text/css' media='screen' />
	
	<?php else: ?>
	<link href='/css/fastest/style.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/css/fastest/menu.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/js/domwin/domwin.css' rel='stylesheet' type='text/css' media='screen' />
	<link href='/js/domtabs/domtabs.css' rel='stylesheet' type='text/css' media='screen' />
	
	<?php endif; ?>
	<!--<link href='/js/domtabs/domtabs.css' rel='stylesheet' type='text/css' media='screen' />-->
	<link href='/js/calendar/calendar.css' rel='stylesheet' type='text/css' media='screen' />
	
	<script type="text/javascript" src="/js/calendar/calendar.js"></script>
	<script type="text/javascript" src="/js/mootools1.2.js"></script>
	<script type="text/javascript" src="/js/mootools.other.js"></script>
	<!--<script type="text/javascript" src="/js/domtabs/domtabs.js"></script>-->
	<script type="text/javascript" src="/js/domtabs/domtabs.js"></script>
	<script type="text/javascript" src="/js/domwin/domwin.js"></script>
	<script type="text/javascript" src="/js/validation/validation.js"></script>
<?php
	if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $javascript->link($link);}} else {echo $javascript->link($scripts);}}
	if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $html->css($style);} } else {echo $html->css($styles);}} 
?>
	<title>systém</title>
</head>
<body>
<div class="obal" id="obal">
	<div class="layout"><?php echo $content_for_layout; ?></div>
</div>
<div id="addon"></div>

</body>
</html>