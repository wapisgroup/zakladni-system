<form id='add_edit_todo_formular' action='/todos/edit/' method='post'>
	<?php echo $htmlExt->hidden('Todo/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->input('Todo/name',array('label'=>'Název úkolu', 'class'=>'long','label_class'=>'long'));?><br />
			<?php echo $htmlExt->selectTag('Todo/type_id',$todo_type_list,null,array('label'=>'Kategorie', 'class'=>'long','label_class'=>'long'),null,false);?>
			
            <div class='sll'>	
				<?php echo $htmlExt->selectTag('Todo/priorita_list',$priorita_list,null,array('label'=>'Priorita'),null,false);?>
				<?php echo $htmlExt->selectTag('Todo/todo_stav_list',$todo_stav_list,null,array('label'=>'Stav'),null,false);?>
				<?php echo $htmlExt->input('Todo/cena',	array('label'=>'Cena'));?><br />
			</div>
			<div class="slr" >  
				<?php echo $htmlExt->input('Todo/cms_user',	array('label'=>'Zadavatel'));?><br />
				<?php echo $htmlExt->inputDateTime('Todo/termin',	array('label'=>'Termín'));?><br />	
				<?php echo $htmlExt->checkbox('Todo/fakturovano',null,array('label'=>'Fakturováno'));?><br />	
			</div>
			<br />
				<?php echo $htmlExt->textarea('Todo/text',array('label'=>'Text úkolu', 'class'=>'long','label_class'=>'long','style'=>'height:300px'));?><br />
				<?php echo $htmlExt->textarea('Todo/poznamka',array('label'=>'Poznámka řešitele', 'class'=>'long','label_class'=>'long'));?><br />
		
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditTodoSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditTodoClose'/>
	</div>
</form>
<script>
	if ($('TodoCmsUser').value==''){
		$('TodoCmsUser').value='<?php echo $logged_user['CmsUser']['name'];?>';
	}
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditTodoClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
	
	
	$('AddEditTodoSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_todo_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('add_edit_todo_formular').action,		
				onComplete:function(){
					click_refresh($('TodoId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_todo_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_todo_formular',{
		'TodoName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název úkolu'}
		}		
	});
	validation.generate('add_edit_todo_formular',<?php echo (isset($this->data['Todo']['id']))?'true':'false';?>);
	
</script>
