<form action='/at_employees/edit_prac_pomer/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('ConnectionClientAtCompanyWorkPosition/id');?>
    <?php echo $htmlExt->hidden('ConnectionClientAtCompanyWorkPosition/client_id');?>
    <?php echo $htmlExt->hidden('Client/email');?>
    <?php echo $htmlExt->hidden('generate_login',array('value'=>0));?>
    <?php echo $htmlExt->hidden('group_id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zaměstnání</legend>
                <?php echo '<label>Firma:</label><var>'.$this->data['AtCompany']['name'].'</var>'?><br />
                <?php echo '<label>Projekt:</label><var>'.$this->data['AtProject']['name'].'</var>'?><br />
                <?php echo $htmlExt->inputDate('datum',array('tabindex'=>1,'label'=>'Datum', 'class'=>'','label_class'=>'long'));?> <br class="clear">
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_centre_id',$centre_list,null,array('label'=>'Sředisko','label_class'=>'long'));?><br />
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_cinnost_id',$cinnost_list,null,array('label'=>'Činnost','label_class'=>'long'),null,false);?><br />
            	<?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_project_centre_work_position_id',$work_position_list,null,array('label'=>'Profese','label_class'=>'long'));?><br />
				<?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/at_company_money_item_id',$money_item_list,null,array('label'=>'Forma zaměstnání','label_class'=>'long'));?><br />
		
                <?php echo $htmlExt->selectTag('ConnectionClientAtCompanyWorkPosition/typ_smlouvy',array(1=>'Smlouva na dobu neurčitou',2=>'Smlouva na dobu určitou'),null,array('label'=>'Typ smlouvy','label_class'=>'long'));?><br />
                <div id="expirace" class="<?echo($this->data['ConnectionClientAtCompanyWorkPosition']['typ_smlouvy'] == 1 ? 'none' :'')?>">
                    <?php echo $htmlExt->inputDate('ConnectionClientAtCompanyWorkPosition/expirace_smlouvy',array('tabindex'=>1,'label'=>'Expirace smlouvy', 'class'=>'','label_class'=>'long'));?> <br class="clear"/>
                </div>
        	</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Editovat zaměstnání',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    var exist_email = false;
    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreId').ajaxLoad('/int_clients/ajax_load_at_company_work_position_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId']);
    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreId').ajaxLoad('/int_clients/ajax_load_at_company_cinnost_list/', ['ConnectionClientAtCompanyWorkPositionAtProjectCinnostId']);

     $('ConnectionClientAtCompanyWorkPositionTypSmlouvy').addEvent('click',function(e){
        e.stop();
        if(this.value == 2){
            $('expirace').removeClass('none');
        }
        else{
            $('expirace').addClass('none');
        }
    }) 
    
    function load_info_position(id){
        new Request.JSON({
			url:'/int_clients/need_genereate_login/'+id,		
			onComplete:function(json){
				if(json){
				    if(json.result == true){
				        $('GroupId').value = json.group_id;
				        $('GenerateLogin').value = json.need;
				    }
                    else{
                        alert(json.message);
                    }
				}
                else
                    alert('Chyba aplikace, obnovte akci');
			}
	    }).send();
    }
    
    if($('ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId').value != '')
        load_info_position($('ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId').value);
    
    $('ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId').addEvent('change',function(e){
        e.stop();
        load_info_position(this.value);
    })   
    
     function validate_mail(email){
        new Request.JSON({
			url:'/int_clients/check_mail_in_int_zam/'+email+'/'+$('ConnectionClientAtCompanyWorkPositionId').value,		
			onComplete:function(json){
				if(json){
				    if(json.result == true){
                        exist_email = 'Tento email už má interní zaměstnanec, jedná se o zaměstnance: '+json.zam+"\nEmaily musí být unikátní pro int. zaměstance.";
				    }
				}
			}
	    }).send();
    } 
    
    if($('ClientEmail').value != ''){
       validate_mail($('ClientEmail').value);
    }
         
     
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
        valid_result = validation.valideForm('setting_career_edit_formular');
        
        if( $('calbut_Datum').value == '') 
			if(valid_result == true)
				valid_result = Array("Musite zvolit datum");
			else
				valid_result[valid_result.length]="Musite zvolit datum";
        if( $('ConnectionClientAtCompanyWorkPositionAtProjectCentreWorkPositionId').value == '') 
			if(valid_result == true)
				valid_result = Array("Musite zvolit profesi");
			else
				valid_result[valid_result.length]="Musite zvolit profesi";        
        
        if (valid_result == true){   
            if($('GenerateLogin').value == 0 || ($('GenerateLogin').value == 1 && $('ClientEmail').value != '')){
                if($('GenerateLogin').value == 0 || ($('GenerateLogin').value == 1 && exist_email == false)){
        			new Request.JSON({
        				url:$('setting_career_edit_formular').action,		
        				onComplete:function(){
        					domwin.closeWindow('domwin_zamestnanci_hodiny');
                            click_refresh($('ConnectionClientAtCompanyWorkPositionId').value); 
        				}
        			}).post($('setting_career_edit_formular'));
                }
                else{ alert(exist_email); }
            }
            else{
                alert("Profesi kterou jste zvolili, vyžaduje vygenerování přístupu do systému.\nEmail tedy musí být u klienta vyplněn - slouží jako přístupové údaje!!!");
            }	
        } 
        else {
		  var error_message = new MyAlert();
		  error_message.show(valid_result);
	    }         
	});
    
    validation.define('formular',{	
        'ConnectionClientAtCompanyWorkPositionAtProjectCentreId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit středisko'},
		},
        'ConnectionClientAtCompanyWorkPositionAtCompanyMoneyItemId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit formu zaměstnání'},
		},
        'ConnectionClientAtCompanyWorkPositionTypSmlouvy': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit typ smlouvy'},
		}
	});
	validation.generate('formular',true);
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_zamestnanci_hodiny');});
</script>