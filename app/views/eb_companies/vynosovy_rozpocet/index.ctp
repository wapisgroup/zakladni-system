<div class="sll">
<script>
	function upload_file_complete(){
         domwin.loadContent('domwin_vynosovy_rozpocet');
         alert('Import proběhl úspěšně');
	}
</script>
<a class="button rozpocet_edit" href="/eb_companies/vynosovy_rozpocet_edit/<?php echo $eb_company_id;?>/">Pridať položku</a><br />
</div>
<div class="slr">
<?php $fileinput_setting = array(
						'label'			=> 'Importovat položky',
						'upload_type'	=> 'php',
                        'filename_type' => 'alias',
                        'file_ext'      => array('csv'),
                        'onComplete'    => 'upload_file_complete',
						'paths' 		=> array(
							'path_to_file'	=> 'uploaded/',
							'upload_script'	=> '/eb_companies/import/'.$eb_company_id.'/RozpocetVynosovyItem/',
							'status_path' 	=> '/get_status.php',
						)
);?>
<?php echo $fileInput->render('Import/file',$fileinput_setting);?>
</div>
<br />
<table class="table">
    <tr>
        <th>#</th>
        <th>Kategorie</th>
        <th>Kód cenníka</th>
        <th>Kód položky</th>
        <th>Popis</th>
        <th>Množstvo</th>
        <th>Jednotková cena</th>
        <th>Cena celkem EUR</th>
        <th>Možnosti</th>
    </tr>
    <?php if (isset($items) && count($items)>0):?>
    <?php foreach($items as $key=>$item):?>
    <tr>
        <td><?php echo $key+1;?></td>
        <td><?php echo $item['RozpocetItemCategory']['name'];?></td>
        <td><?php echo $item['RozpocetVynosovyItem']['code'];?></td>
        <td><?php echo $item['RozpocetVynosovyItem']['code_item'];?></td>
        <td><?php echo $item['RozpocetVynosovyItem']['name'];?></td>
        <td><?php echo $item['RozpocetVynosovyItem']['mn'];?> <?php echo $item['RozpocetVynosovyItem']['mj'];?></td>
        <td><?php echo $item['RozpocetVynosovyItem']['jc'];?></td>
        <td><?php echo $item['RozpocetVynosovyItem']['cena'];?></td>
        <td>
            <a href="/eb_companies/vynosovy_rozpocet_edit/<?php echo $eb_company_id;?>/<?php echo $item['RozpocetVynosovyItem']['id'];?>/" title="Úprava položky rozpočtu" class="ta edit rozpocet_edit"></a>
            <a href="/eb_companies/vynosovy_rozpocet_delete/<?php echo $item['RozpocetVynosovyItem']['id'];?>/" title="Zmazanie položky rozpočtu" class="ta trash rozpocet_delete"></a>
        </td>
    </tr>
    <?php endforeach;?>
    <?php else:?>
    <tr>
        <td colspan="8" align="center">Nebola nájdená žiadna položka ...</td>
    </tr>
    <?php endif;?>
</table>
<div style="text-align: center; margin-top:15px">
    <input type="button" value="Zavriť" id="close_domwin" /> 
</div>

<script type="text/javascript" language="javascript">
    $('close_domwin').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_vynosovy_rozpocet');});
    $$('.rozpocet_edit').addEvent('click', function(e){
        e.stop();
		domwin.newWindow({
			id			: 'domwin_rozpocet_edit',
			sizes		: [600,500],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: false,
            remove_scroll:false
		}); 
    });
    
    $$('.rozpocet_delete').addEvent('click', function(e){
        e.stop();
		new Request.JSON({
            url: this.href,
            onComplete: (function(json){
                if (json){
                	if (json.result === true){
                		this.getParent('tr').dispose();        
                	} else {
                		alert(json.message);
                	}
                } else {
                	alert('Chyba aplikace')
                }
            }).bind(this)
        }).send();
    });
</script>