<form id='add_edit_company_formular' action='/eb_companies/edit/' method='post'>
	<?php echo $htmlExt->hidden('EbCompany/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1" rel="0,1">Základní informace</a></li>
            <li class="ousko"><a href="#krok2" rel="2">Příprava a rozpočet</a></li>
            <li class="ousko"><a href="#krok3" rel="2">Popis zakázky</a></li>
            <li class="ousko"><a href="#krok4" rel="3">Realizace</a></li>
            <li class="ousko"><a href="#krok5" rel="4">Vyhodnocení</a></li>
            <li class="ousko"><a href="#krok6" rel="5">Archiv</a></li>
            <?php if (isset($this->data['EbCompany']['id'])):?>
            <li class="ousko"><a href="#krok7">Zaměstnanci</a></li>
            <?php endif;?>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->input('EbCompany/name',array('label'=>'Jméno investora', 'class'=>'long','label_class'=>'long'));?><br />
			<div class='sll'>	
				<?php echo $htmlExt->selectTag('EbCompany/stat_id',$company_stat_list,null,array('label'=>'Stát'),null,false);?><br />
				<?php echo $htmlExt->selectTag('EbCompany/province_id',$province_list,null,array('label'=>'Kraj'));?><br />
				<?php echo $htmlExt->selectTag('EbCompany/country_id',(isset($country_list)?$country_list:array()),null,array('label'=>'Okres'));?><br />
				
				<?php echo $htmlExt->input('EbCompany/ulice',	array('label'=>'Ulice'));?><br />
				<?php echo $htmlExt->input('EbCompany/mesto',	array('label'=>'Město'));?><br />
                <?php echo $htmlExt->input('EbCompany/psc',	array('label'=>'PSČ'));?><br />
				<?php echo $htmlExt->input('EbCompany/www',	array('label'=>'WWW'));?><br />	
				<?php echo $htmlExt->input('EbCompany/ico',	array('label'=>'IČO'));?><br />	
				<?php echo $htmlExt->input('EbCompany/dic',	array('label'=>'DIČ'));?><br />
                <?php echo $htmlExt->inputDate('EbCompany/datum_podpisu_smlouvy',	array('label'=>'Datum podpisu smlouvy s frimou'));?><br />
                <?php echo $htmlExt->input('EbCompany/pc',	array('label'=>'P.Č.'));?><br />
			</div>
			<div class='slr'>
               	<?php echo $htmlExt->input('EbCompany/order_name',	array('label'=>'Název zakázky'));?><br />
				
				<?php echo $htmlExt->selectTag('EbCompany/buyer_id',$nakupce_list,null,array('label'=>'Nákupce'));?><br />
                <?php echo $htmlExt->selectTag('EbCompany/master_id',$vedouci_list,null,array('label'=>'Vedoucí výroby'));?><br />		
			    <?php echo $htmlExt->selectTag('EbCompany/rozpoctar_id',$rozpoctar_list,null,array('label'=>'Rozpočtář'));?><br />		
                <?php echo $htmlExt->selectTag('EbCompany/obchodnik_id',$obchodnik_list,null,array('label'=>'Obchodník'));?><br />		
                <?php echo $htmlExt->selectTag('EbCompany/stavbyvedouci_id',$stavbyvedouci_list,null,array('label'=>'Stavbyvedoucí'));?><br />		
                <?php echo $htmlExt->selectTag('EbCompany/mistr_id',$mistr_list,null,array('label'=>'Mistr'));?><br />		
                <?php echo $htmlExt->selectTag('EbCompany/stav_id',$eb_stav_list,null,array('disabled'=>'disabled','class'=>'read_info','label'=>'Stav zakázky'),null,false);?><br />		

            </div>
		    <br />
    	</div>
        <div class="domtabs field noprint">
            <?php echo $this->renderElement('../eb_companies/tabs/priprava_a_rozpocet'); ?>
        </div>
        <div class="domtabs field noprint">
            <?php echo $this->renderElement('../eb_companies/tabs/popis_zakazky'); ?>
        </div>
        <div class="domtabs field noprint">
            <?php echo $this->renderElement('../eb_companies/tabs/realizace'); ?>
        </div>
        <div class="domtabs field noprint">
            <?php echo $this->renderElement('../eb_companies/tabs/vyhodnoceni'); ?>
        </div>
        <div class="domtabs field"></div>
        <?php if (isset($this->data['EbCompany']['id'])):?>
        <div class="domtabs field">
            <div style='height:300px; width:150px; overflow: hidden; border: 1px solid #CCC; float:left'>
                <table id='client_list_cm' class="table">
                    <tr><th colspan='2'>Jméno</th></tr>
                    <?php foreach($clients as $client_id => $client_name):?>
                    <tr><td><?= $client_name;?></td><td><a title="Umístění na zakázku" href="/eb_companies/add_worker/<?= $this->data['EbCompany']['id'];?>/<?= $client_id;?>/" rel="<?= $client_id;?>" onclick="return false;" class="ta add add_worker">Add</a></td></tr>
                    <?php endforeach;?>
                </table>
            </div>
            <div style='float: left; width: 615px; height:300px; border: 1px solid #ccc; margin-left: 5px'>
                <table class='table' id='worker_items'>
                    <thead>
                        <tr>
                            <th>Zaměstnanec</th>
                            <th>Přiřadil</th>
                            <th>Od</th>
                            <th>Možnosti</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($zam_list as $zl):?>
                    <tr>
                        <td><?= $zl['Client']['name'];?></td>
                        <td><?= $zl['CmsUser']['name'];?></td>
                        <td><?= date('d.m.Y H:i',strtotime($zl['ConnectionEbcomClient']['start']));?></td>
                        <td>
                            <a title="Propuštění" class="ta delete remove_worker" href="/eb_companies/remove_worker/<?= $zl['ConnectionEbcomClient']['id'];?>">Odstranit</a>
                        </td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <br class="clear"/>
        </div>
        <?php endif;?>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditEbCompanySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditEbCompanyClose'/>
	</div>
</form>
<script>
    $$('.remove_worker').addEvent('click', function(e){
        e.stop();
		domwin.newWindow({
			id			: 'domwin_worker_remove',
			sizes		: [400,170],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		});
    })

    $$('.add_worker').addEvent('click', function(e){
        e.stop();
		domwin.newWindow({
			id			: 'domwin_worker_add',
			sizes		: [400,170],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		});
    })
	var stav_select = $('EbCompanyRozpoctarId').value;
	var domtab = new DomTabs({
	   'className':'admin_dom',
       'permission':'<?= json_encode($tab_setting);?>',
       'stav_permission':$('EbCompanyStavId').value,
       'stav_strictly':false
    }); 
    
	$('AddEditEbCompanyClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
	
    $('EbCompanyRozpoctarId').addEvent('change',function(e){
        if(stav_select == ''){
            $('EbCompanyStavId').value = 2;
        }
    })
	$('EbCompanyStatId').ajaxLoad('/companies/load_province/', ['EbCompanyProvinceId']);
	$('EbCompanyProvinceId').ajaxLoad('/companies/load_country/', ['EbCompanyCountryId']);
	
	$('AddEditEbCompanySaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_company_formular');
		if (valid_result == true){
			button_preloader($('AddEditEbCompanySaveAndClose'));
            $('EbCompanyStavId').removeProperty('disabled');
			new Request.JSON({
				url:$('add_edit_company_formular').action,		
				onComplete:function(){
					button_preloader_disable($('AddEditEbCompanySaveAndClose'));
					click_refresh($('EbCompanyId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_company_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	validation.setStav($('EbCompanyStavId').value);
	validation.define('add_edit_company_formular',{
		'EbCompanyName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit nazev firmy'}
		},
        'EbCompanyStatId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit stát'}
		},
        'EbCompanyProvinceId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kraj'}
		},
        'EbCompanyCountryId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit okres'}
		},
        'EbCompanyUlice': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ulici'}
		},
        'EbCompanyMesto': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit město'}
		},
        'EbCompanyPsc': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit PSČ'}
		},
        'EbCompanyPc': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit P.Č.'}
		},
        'calbut_EbCompanyPrZadani': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit termín zadání','stav':2}
		},
        'calbut_EbCompanyPrOdevzdani': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit termín odevzdání','stav':2}
		},
        'calbut_EbCompanyPrVybKonani': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit termín výběrového konání','stav':2}
		},
        'calbut_EbCompanyPrPredTerminUkonceni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit předp. termín ukončení','stav':2}
		},
        'EbCompanyPrFinancniObjem': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit finanční objem','stav':2}
		},
        'EbCompanyPrInvestor': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit ivestora','stav':2}
		}
	});
	validation.generate('add_edit_company_formular',<?php echo (isset($this->data['EbCompany']['id']))?'true':'false';?>);
	validation.check_form('add_edit_company_formular');
</script>
