<div class="sll"><?php echo $htmlExt->var_text('EbCompany/order_name',	array('label'=>'Zakázka'));?></div>
<div class="slr"><?php echo $htmlExt->checkbox('go_to_realization',	null,array('label'=>'Posunout zakázku k realizaci','disabled'=>($allow_go_to_realization == 0?'disabled':null)));?></div>
<br />
<table class='table' id='zakazky_items'>
    <thead>
        <tr>
            <th>Pořadové číslo</th>
            <th>Popis činnosti</th>
            <th>Datum</th>
            <th>Předpokládaný počet dní</th>
            <th>Možnosti</th>
        </tr>
    </thead>
    <tbody>
        <tr><td colspan="5">
            <a href='/eb_companies/edit_order_item/<?= $this->data['EbCompany']['id']?>' class="ta add" id='add_from_list_bonus' title='Přidat bod'>Přidat bod</a>
            &nbsp; Přidat další bod
            </td>
        </tr>
    <?php 
    $oi = 1;
    if(isset($order_items))
        foreach($order_items as $item):?>
        <tr>
            <td><?= $oi;?>.</td>
            <td><?= $item['EbCompanyOrderItem']['name'];?></td>
            <td><?= $fastest->czechDate($item['EbCompanyOrderItem']['supposed_date']);?></td>
            <td><?= $item['EbCompanyOrderItem']['supposed_days'];?></td>
            <td>
                <a title="Editovat bod" class="ta edit" href="/eb_companies/edit_order_item/<?= $this->data['EbCompany']['id']?>/<?= $item['EbCompanyOrderItem']['id'];?>">Editovat</a>
            </td>
        </tr>
        <?php 
        $oi++;
        endforeach;?>
    </tbody>
</table>
<script>

if($('GoToRealization'))
    $('GoToRealization').addEvent('click', function(e){
        if($('EbCompanyStavId').value < 3){  $('EbCompanyStavId').value = 3; } 
    }) 
    
if($('EbCompanyStavId') && $('EbCompanyStavId').value >= 3){
    $('zakazky_items').getElement('a.add').getParent('tr').dispose();
}       
        
$('zakazky_items').getElements('a.edit,a.add').addEvent('click', function(e){
        e.stop();
		domwin.newWindow({
			id			: 'domwin_order_item_edit',
			sizes		: [500,400],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		});
})

</script>