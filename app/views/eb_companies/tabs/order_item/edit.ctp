<form action='/eb_companies/edit_order_item/' method='post' id='order_activity_edit_formular'>
	<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom2">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('EbCompanyOrderItem/id');?>
			<?php echo $htmlExt->hidden('EbCompanyOrderItem/eb_company_id');?>
			<fieldset>
				<legend>Základní informace</legend>
				<?php echo $htmlExt->input('EbCompanyOrderItem/name',array('tabindex'=>1,'label'=>'Název','class'=>'','label_class'=>''));?> <br class="clear">
				<?php echo $htmlExt->inputDate('EbCompanyOrderItem/supposed_date',array('tabindex'=>2,'label'=>'Datum','class'=>'','label_class'=>''));?> <br class="clear">
				<?php echo $htmlExt->input('EbCompanyOrderItem/supposed_days',array('tabindex'=>3,'label'=>'Předpokládaný počet dní','class'=>'','label_class'=>''));?> <br class="clear">
				
                </fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložiť' id='AddEditEbCompanyOrderItemSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditEbCompanyOrderItemClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom2'}); 
	
	$('AddEditEbCompanyOrderItemClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_order_item_edit');});
	
    function load_content_param(){ domtab.goTo(2); }
	$('AddEditEbCompanyOrderItemSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('order_activity_edit_formular');
		if (valid_result == true){
            button_preloader($('AddEditEbCompanyOrderItemSaveAndClose'));
			new Request.JSON({
				url:$('order_activity_edit_formular').action,		
				onComplete:function(){
				    domwin.loadContent('domwin',{'onComplete': 'load_content_param'}); 
					domwin.closeWindow('domwin_order_item_edit');
                    button_preloader_disable($('AddEditEbCompanyOrderItemSaveAndClose'));
				}
			}).post($('order_activity_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('order_activity_edit_formular',{
		'EbCompanyOrderItemName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('order_activity_edit_formular',<?php echo (isset($this->data['EbCompanyOrderItem']['id']))?'true':'false';?>);
</script>