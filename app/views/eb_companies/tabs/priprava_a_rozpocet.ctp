<div class='sll'>	
	<?php echo $htmlExt->inputDate('EbCompany/pr_zadani',	array('label'=>'Termín zadání'));?><br />
	<?php echo $htmlExt->inputDate('EbCompany/pr_odevzdani',	array('label'=>'Termín odevzdání'));?><br />
    <?php echo $htmlExt->inputDate('EbCompany/pr_vyb_konani',	array('label'=>'Termín výběrového konání'));?><br />
	<?php echo $htmlExt->inputDate('EbCompany/pr_pred_termin_ukonceni',	array('label'=>'Předp.Termín ukončení'));?><br />	
	<?php echo $htmlExt->input('EbCompany/pr_financni_objem',	array('label'=>'Finanční objem'));?><br />	
	<?php echo $htmlExt->input('EbCompany/pr_investor',	array('label'=>'Investor'));?><br />
</div>
<div class='slr'>
   	<label>Projektová dokumentace</label><a class="ta attachs" href="/eb_companies/attachs/1" title="Přílohy">attachs</a><br />
    <label>Výkaz výměr</label><a class="ta attachs" href="/eb_companies/attachs/2" title="Přílohy">attachs</a><br />
    <label>Vstupné údaje od odd.nakupu</label><a class="ta attachs" href="/eb_companies/attachs/3" title="Přílohy">attachs</a><br />
    <label>Nákladový rozpočet</label>
        <a class="ta attachs" href="/eb_companies/attachs/4" title="Přílohy">attachs</a>
        <?php if($this->data['EbCompany']['id'] != ''){?>
        <a class="ta rozpocet" href="/eb_companies/rozpocet/<?= $this->data['EbCompany']['id'];?>/" title="Nákladový rozpočet">rozpocet</a>
        <?php } ?>
    <br />
    <label>Výnosový rozpočet</label>
        <a class="ta attachs" href="/eb_companies/attachs/5" title="Přílohy">attachs</a>
        <?php if($this->data['EbCompany']['id'] != ''){?>
        <a class="ta vynosovy_rozpocet" href="/eb_companies/vynosovy_rozpocet/<?= $this->data['EbCompany']['id'];?>/" title="Výnosový rozpočet">rozpocet</a>
        <?php } ?>
    <br />
    <label>Karta projektu</label><a class="ta attachs" href="/eb_companies/attachs/6" title="Přílohy">attachs</a><br />
       	
</div>
<br />
<?php echo $htmlExt->textarea('EbCompany/pr_note',	array('label'=>'Poznámka','class'=>'long','label_class'=>'long'));?><br />

<script>


$('add_edit_company_formular').getElements('.attachs').addEvent('click',function(e){
    e.stop();
    if($('EbCompanyId').value != ''){
    new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_attach',
			sizes		: [650,600],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href+'/'+$('EbCompanyId').value,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
	}); 
    }
    else
        alert('Uložte první kartu, nahrávat přílohy jde až po uložení.');
})

if($('add_edit_company_formular').getElements('.rozpocet'))
$('add_edit_company_formular').getElements('.rozpocet').addEvent('click',function(e){
    e.stop();
    new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_rozpocet',
			sizes		: [800,800],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
	}); 
})

if($('add_edit_company_formular').getElements('.vynosovy_rozpocet'))
$('add_edit_company_formular').getElements('.vynosovy_rozpocet').addEvent('click',function(e){
    e.stop();
    new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_vynosovy_rozpocet',
			sizes		: [800,800],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
	}); 
})



</script> 