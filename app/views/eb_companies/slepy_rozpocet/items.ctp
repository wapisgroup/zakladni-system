<form action="/eb_companies/save_blind_data/" method="post" id="save_blind_data_form">
<p>Slepý rozpočet pro měsíc <strong><?php echo $mesice_list2[$mesic];?></strong> a rok <strong><?php echo $actual_years_list[$rok]?></strong>.</p><br />
<table class="table">
    <tr>
        <th>#</th>
        <th>Kód cenníka</th>
        <th>Kód položky</th>
        <th>Popis</th>
        <th>Množstvo</th>
        <th>Množstvo v docházce</th>
        <th>Jednotková cena</th>
        <th>Cena celkem EUR</th>
    </tr>
    <?php if (isset($items) && count($items)>0):?>
    <?php foreach($items as $key=>$item):?>
    <tr>
        <td>
            <?php echo $key+1;?>
            <input type="hidden" value="<?php echo $item['RozpocetItem']['id'];?>" name="data[RozpocetBlindItem][<?php echo $key;?>][rozpocet_item_id]"/>  
            <input type="hidden" value="<?php echo $item['RozpocetBlindItem']['id'];?>" name="data[RozpocetBlindItem][<?php echo $key;?>][id]"/>  
            <input type="hidden" value="<?php echo $mesic;?>"      name="data[RozpocetBlindItem][<?php echo $key;?>][mesic]"/>  
            <input type="hidden" value="<?php echo $rok;?>"        name="data[RozpocetBlindItem][<?php echo $key;?>][rok]"/>  
            <input type="hidden" value="<?php echo $eb_company_id;?>"   name="data[RozpocetBlindItem][<?php echo $key;?>][eb_company_id]"/>  
        </td>
        <td><?php echo $item['RozpocetItem']['code'];?></td>
        <td><?php echo $item['RozpocetItem']['code_item'];?></td>
        <td><?php echo $item['RozpocetItem']['name'];?></td>
        <td><input type="text" value="<?php echo $item['RozpocetBlindItem']['mnozstvi'];?>" name="data[RozpocetBlindItem][<?php echo $key;?>][mnozstvi]"/> <?php echo $item['RozpocetItem']['mj'];?></td>
        <td><?php echo $item[0]['mj_dochazka'];?></td>
        <td><?php echo $item['RozpocetItem']['jc'];?></td>
        <td><?php echo $item['RozpocetItem']['cena'];?></td>
    </tr>
    <?php endforeach;?>
    <?php else:?>
    <tr>
        <td colspan="8" align="center">Nebola nájdená žiadna položka ...</td>
    </tr>
    <?php endif;?>
</table>
<div align="center"><br />
    <input type="button" value="Uložit" id="save_fields" />
    <input type="button" value="Tisk" id="print_fields" />
</div>
</form>
<script>
    $('print_fields').addEvent('click',function(e){
       e.stop();
       var mesic = $('Mesic').value;
       var rok = $('Rok').value;
       window.open('/eb_companies/blind_rozpocet_print/<?php echo $eb_company_id;?>/' + mesic + '/' + rok + '/') ;
    });

    $('save_fields').addEvent('click', function(e){
       e.stop();
	   new Request.JSON({
	       url:$('save_blind_data_form').getProperty('action'),
           onComplete:function(json){
    	     if (json){
            	if (json.result === true){
                    load_my();
            		alert('Data byla uložena.');        
            	} else {
            		alert(json.message);
            	}
            } else {
            	alert('Chyba aplikace')
            }
          }
	   }).send($('save_blind_data_form'));
    
    });
</script>