<div class="sll">
    <?php echo $htmlExt->selectTag('mesic',$mesice_list2,$mesic,array('label'=>'Mesiac','class'=>'change_datum'));?>
</div>
<div class="slr">
    <?php echo $htmlExt->selectTag('rok',$actual_years_list,$rok,array('label'=>'Rok','class'=>'change_datum'));?>
</div>
<fieldset>
    <legend>Zoznam položiek rozpočtu</legend>
    <div id="blind_items" style="padding: 10px;"><?php echo $this->renderElement('../eb_companies/slepy_rozpocet/items');?></div>
</fieldset>

<div style="text-align: center; margin-top:15px">
    <input type="button" value="Zavriť" id="close_domwin" /> 
</div>

<script type="text/javascript" language="javascript">
    $('close_domwin').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin');});

    $$('.change_datum').addEvent('change',load_my);
    
    function load_my(){
        var mesic = $('Mesic').value;
        var rok = $('Rok').value;
        
        if (mesic == '' || rok == '') {
             $('blind_items').setHTML('Vyberte prosím rok a mesiac ....');
        } else {
            new Request.HTML({
                update: $('blind_items'),
                url: '/eb_companies/slepy_rozpocet_items/<?php echo $eb_company_id;?>/' + mesic + '/' + rok + '/'
            }).send();
        }
        
    }
</script>