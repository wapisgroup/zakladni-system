<form action='/car_monthly_statements/edit/' method='post' id='setting_stav_edit_formular'>
	<?php echo $htmlExt->hidden('CarMonthlyStatement/id');?>
    <?php echo $htmlExt->hidden('CarMonthlyStatement/audit_estate_id');?>
	<?php echo $htmlExt->hidden('CarMonthlyStatement/rok');?>
    <?php echo $htmlExt->hidden('CarMonthlyStatement/mesic');?>        
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
                <?php echo $htmlExt->var_text('AuditEstate/name',array('label'=>'Majetek','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->var_text('0/osoba',array('label'=>'Nájemce','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->var_text('0/stredisko',array('label'=>'Středisko','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
                <?php echo $htmlExt->var_text('0/hire_price',array('label'=>'Měsíční pronájem','class'=>'long50','label_class'=>'long50'));?> <br class="clear">
               <fieldset>
                <legend>Výnosy</legend>
                    <?php echo $htmlExt->input('CarMonthlyStatement/fakt_najem',array('tabindex'=>1,'label'=>'Fakturační nájem','class'=>'yields long50 read_info','readonly'=>'readonly','label_class'=>'long50'));?> <br class="clear">
				    <?php echo $htmlExt->input('CarMonthlyStatement/pojistne_plneni',array('tabindex'=>2,'label'=>'Pojistné plnění','class'=>'yields long50 read_info','readonly'=>'readonly','label_class'=>'long50'));?> <br class="clear">
               </fieldset>
               <fieldset>
                <legend>Náklady</legend>
                    <?php echo $htmlExt->input('CarMonthlyStatement/leasing',array('tabindex'=>3,'label'=>'Leasing','class'=>'costs integer long50','label_class'=>'long50'));?> <br class="clear">
    				<?php echo $htmlExt->input('CarMonthlyStatement/insurance',array('tabindex'=>4,'label'=>'Pojištení','class'=>'costs integer long50','label_class'=>'long50'));?> <br class="clear">
    			    <?php echo $htmlExt->input('CarMonthlyStatement/cest_dan',array('tabindex'=>5,'label'=>'Cestovní daň','class'=>'costs integer long50','label_class'=>'long50'));?> <br class="clear">
                    <?php echo $htmlExt->input('CarMonthlyStatement/gps_data',array('tabindex'=>6,'class'=>'costs integer long50','label'=>'GPS/data','label_class'=>'long50'));?> <br class="clear">
    			   	<?php echo $htmlExt->input('CarMonthlyStatement/repairs',array('tabindex'=>7,'class'=>'costs integer long50','label'=>'Opravy','label_class'=>'long50'));?> <br class="clear">
                    <?php echo $htmlExt->input('CarMonthlyStatement/gears_km',array('tabindex'=>8,'class'=>'costs integer long50','label'=>'Pneu/km','label_class'=>'long50'));?> <br class="clear">
    		        <?php echo $htmlExt->input('CarMonthlyStatement/other_costs',array('tabindex'=>9,'class'=>'costs integer long50','label'=>'Ostatní výdaje','label_class'=>'long50'));?> <br class="clear">  
               </fieldset> 
               
               <?php echo $htmlExt->input('CarMonthlyStatement/yields_costs',array('tabindex'=>10,'class'=>'read_info integer long50','readonly'=>'readonly','label'=>'Výnosy/Náklady','label_class'=>'long50'));?> <br class="clear">  
               <br />
               <?php echo $htmlExt->input('CarMonthlyStatement/km_monthly',array('tabindex'=>11,'class'=>'long50 integer','label'=>'Km/měsíc','label_class'=>'long50'));?> <br class="clear">  
               <?php echo $htmlExt->input('CarMonthlyStatement/phm_monthly',array('tabindex'=>12,'class'=>'long50 integer','label'=>'PHM/L/Měs','label_class'=>'long50'));?> <br class="clear">  
               <?php echo $htmlExt->input('CarMonthlyStatement/average_consumption',array('tabindex'=>13,'class'=>'long50 consumption integer','label'=>'Průměrná spotřeba','label_class'=>'long50'));?> <br class="clear">  
               <?php echo $htmlExt->input('CarMonthlyStatement/normal_consumption',array('tabindex'=>14,'class'=>'long50 consumption integer','label'=>'Nor. spotřeba','label_class'=>'long50'));?> <br class="clear">  
               <?php echo $htmlExt->input('CarMonthlyStatement/difference',array('tabindex'=>15,'class'=>'read_info long50 integer','readonly'=>'readonly','label'=>'Rozdíl/L','label_class'=>'long50'));?> <br class="clear">  
              
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>8));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>9));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    
    $('setting_stav_edit_formular').getElements('.integer').inputLimit();
    $each($$('.integer'),function(item){if(item.value == ''){item.value = 0;}})
    
    $$('.yields').addEvent('change',recount_yields_costs);
    $$('.costs').addEvent('change',recount_yields_costs);
    function recount_yields_costs(){
        var vynosy = naklady = 0;
        $each($$('.yields'),function(item){vynosy += item.value.toFloat();})
        $each($$('.costs'),function(item){naklady += item.value.toFloat();})
    
        $('CarMonthlyStatementYieldsCosts').value = (vynosy-naklady).toFloat();
    }
    if($('CarMonthlyStatementId').value == ''){recount_yields_costs();}
    
    $$('.consumption').addEvent('change',recount_difference_consumption);
    function recount_difference_consumption(){
        prum = $('CarMonthlyStatementAverageConsumption').value.toFloat();
        norm = $('CarMonthlyStatementNormalConsumption').value.toFloat();
        $('CarMonthlyStatementDifference').value = (prum-norm).toFloat();   
    }
     
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_stav_edit_formular');
		if (valid_result == true){
            button_preloader($('save_close'));
			new Request.JSON({
				url:$('setting_stav_edit_formular').action,		
				onComplete:function(){
                    button_preloader_disable($('save_close'));
					click_refresh($('CarMonthlyStatementId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('setting_stav_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_stav_edit_formular',{
	});
	validation.generate('setting_stav_edit_formular',<?php echo (isset($this->data['CarMonthlyStatement']['id']))?'true':'false';?>);
</script>