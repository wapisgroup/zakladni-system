<form action='/accommodations/kontakty_edit/' method='post' id='Accommodation_contact_edit_formular'>
	<?php echo $htmlExt->hidden('AccommodationContact/id');?>
	<?php echo $htmlExt->hidden('AccommodationContact/accommodation_id');?>
	<fieldset>
		<legend>Vyplňte následující údaje</legend>
		<div class='sll'>
			<?php echo $htmlExt->input('AccommodationContact/jmeno',array('tabindex'=>1,'label'=>'Jméno'));?> <br class="clear">
			<?php echo $htmlExt->input('AccommodationContact/telefon1',array('tabindex'=>3,'label'=>'Telefon'));?> <br class="clear">
			<?php echo $htmlExt->input('AccommodationContact/email',array('tabindex'=>5,'label'=>'Email'));?> <br class="clear">
					
		</div>
		<div class='slr'>
			<?php echo $htmlExt->input('AccommodationContact/prijmeni',array('tabindex'=>2,'label'=>'Příjmení'));?> <br class="clear">
			<?php echo $htmlExt->input('AccommodationContact/telefon2',array('tabindex'=>4,'label'=>'Telefon'));?> <br class="clear">
			<?php echo $htmlExt->input('AccommodationContact/position',array('tabindex'=>6,'label'=>'Pozice'));?> <br class="clear">		
		</div>
		<br/>
		<?php echo $htmlExt->textarea('AccommodationContact/comment',array('tabindex'=>7,'label'=>'Komentář','class'=>'long','label_class'=>'long'));?> <br class="clear">
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditAccommodationContactSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditAccommodationContactClose'/>
	</div>
</form>
<script>
	$('AddEditAccommodationContactClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_kontakty_add');});
	
	$('AddEditAccommodationContactSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('Accommodation_contact_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('Accommodation_contact_edit_formular').action,		
				update: $('domwin_kontakty').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_kontakty_add');
				}
			}).post($('Accommodation_contact_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('Accommodation_contact_edit_formular',{
		'AccommodationContactJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'}
		},
		'AccommodationContactPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'}
		}
	});
	validation.generate('Accommodation_contact_edit_formular',<?php echo (isset($this->data['AccommodationContact']['id']))?'true':'false';?>);
	
</script>