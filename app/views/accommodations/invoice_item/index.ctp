<div class="win_fce top">
<a href='/accommodations/invoice_item_edit/<?php echo $accommodation_id;?>' class='button edit_invoice_item' id='Accommodation_invoice_item_add'>Přidat Fakturu</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>Měsíc</th>
		<th>Datum vystavení</th>
		<th>Datum splatnosti</th>
		<th>Částka</th>
		<th>Bez DPH</th>
		<th>S DPH</th>
		<th>Datum úhrady</th>
		<th>Po splatnosti</th>
		<th>Komentář</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($invoice_item_list) && count($invoice_item_list) >0):?>
	<?php foreach($invoice_item_list as $invoice_item):?>
	<tr>
		<td><?php echo date('Y-m',strtotime($invoice_item['AccommodationInvoiceItem']['za_mesic']));?></td>
		<td><?php echo $fastest->czechDate($invoice_item['AccommodationInvoiceItem']['datum_vystaveni']);?></td>
		<td><?php echo $fastest->czechDate($invoice_item['AccommodationInvoiceItem']['datum_splatnosti']);?></td>
		<td><?php echo $invoice_item['AccommodationInvoiceItem']['cena'];?></td>
		<td><?php echo $invoice_item['AccommodationInvoiceItem']['cena_bez_dph'];?></td>
		<td><?php echo $invoice_item['AccommodationInvoiceItem']['spolu_s_dph'];?></td>
		<td><?php echo $fastest->czechDate($invoice_item['AccommodationInvoiceItem']['datum_uhrady']);?></td>
		<td><?php 
			$po_splatnosti = (strtotime(date('Y-m-d')) - strtotime($invoice_item['AccommodationInvoiceItem']['datum_splatnosti'])) / 3600 /24;
			
			if ($po_splatnosti > 0) {
				if ($invoice_item['AccommodationInvoiceItem']['datum_uhrady'] != '0000-00-00')
					echo 'Uhrazeno';
				else 
					echo $po_splatnosti;
			} else
				echo '';
		?></td>
		<td><?php echo $invoice_item['AccommodationInvoiceItem']['comment'];?></td>
		
		<td>
			<a title='Editace položky' 	class='ta edit edit_invoice_item' href='/accommodations/invoice_item_edit/<?php echo $accommodation_id;?>/<?php echo $invoice_item['AccommodationInvoiceItem']['id'];?>'/>edit</a>
		<?php if ($logged_user['CmsGroup']['id'] == 1):?>
			<a title='Do kosiku' 		class='ta trash' href='/accommodations/invoice_item_trash/<?php echo $accommodation_id;?>/<?php echo $invoice_item['AccommodationInvoiceItem']['id'];?>'/>trash</a>
		<?php endif;?>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K tomuto ubytování nebyla nadefinována faktura</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($contact_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListAccommodationInvoiceItemClose' class='button'/>
	</div>
<script>
	$('ListAccommodationInvoiceItemClose').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_invoice_item');
	});


	
	$('domwin_invoice_item').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto fakturu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_invoice_item').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_invoice_item').getElements('.edit_invoice_item').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_invoice_item_add',
			sizes		: [580,420],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>