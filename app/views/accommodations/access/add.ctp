<form action='/accommodations/access_add/<?php echo $accommodation_id;?>' method='post' id='client_attachs_edit_formular'>
	<fieldset>
			<legend>Uživatel</legend>
					<?php echo $htmlExt->selectTag('ConnectionAccommodationUser/cms_user_id', $cms_user_list, null, array('tabindex'=>23,'label'=>'Uživatel'));?> <br class="clear">
			</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
´</form>
<script>

	
	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_recruiter_add');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_attachs_edit_formular');
		
		if (valid_result == true){
			new Request.JSON({
				url:$('client_attachs_edit_formular').action,		
				//update: $("domwin"),
				onComplete:function(json){
					try{
						  if(json.CmsUser.name) user = json.CmsUser.name;
					}catch(e){
						  user = json;
					}
					
					tr = new Element('tr').inject($('recruiter_table'));
					new Element('td').inject(tr).setHTML(user);
					new Element('td').inject(tr).setHTML('<?php echo date('d.m.Y');?>');
					domwin.closeWindow('domwin_recruiter_add');
				}
			}).post($('client_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
</script>