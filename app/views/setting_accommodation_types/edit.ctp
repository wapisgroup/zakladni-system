<form action='/setting_accommodation_types/edit/' method='post' id='setting_task_type_edit_formular'>
	<?php echo $htmlExt->hidden('SettingAccommodationType/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('SettingAccommodationType/name',array('tabindex'=>1,'label'=>'Název'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('SettingAccommodationType/price_type',$price_for_list, null, array('tabindex'=>2,'label'=>'Typ ceny'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_task_type_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_task_type_edit_formular').action,		
				onComplete:function(){
					click_refresh($('SettingAccommodationTypeId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_task_type_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_task_type_edit_formular',{
		'SettingAccommodationTypeName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
			'isUnique':{'condition':{'model':'SettingAccommodationType','field':'name'},'err_message':'Tento stav je již použit'}
		}
	});
	validation.generate('setting_task_type_edit_formular',<?php echo (isset($this->data['SettingAccommodationType']['id']))?'true':'false';?>);
</script>