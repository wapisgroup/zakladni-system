<div class="win_fce top">
<a href='/companies_audit_estates/centre_edit/<?php echo $company_id;?>' class='button edit' id='company_centre_add_new' title='Přidání nové středisko'>Přidat středisko</a>
</div>
<table class='table' id='table_centres'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($centre_list) && count($centre_list) >0):?>
    	<?php foreach($centre_list as $centre_item):?>
    	<tr>
    		<td><?php echo $centre_item['CompanyCentre']['id'];?></td>
    		<td><?php echo $centre_item['CompanyCentre']['name'];?></td>
    		<td>
    			<a title='Editace střediska "<?php echo $centre_item['CompanyCentre']['name'];?>"' 	class='ta edit' href='/companies_audit_estates/centre_edit/<?php echo $company_id;?>/<?php echo $centre_item['CompanyCentre']['id'];?>'/>edit</a>
                <?php
                if($logged_user['CmsGroup']['id'] == 1){
                ?>
                	<a title='Odstranit středisko 'class='ta trash' href='/companies_audit_estates/centre_trash/<?php echo $centre_item['CompanyCentre']['id'];?>'/>trash</a>
        		<?php }?>
            
            </td>
    	</tr>
    	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této firmě nebyla nadefinována střediska</td>
	</tr>
	<?php endif;?>
</table>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyTemplateClose' class='button'/>
	</div>
<script>
	$('ListCompanyTemplateClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin');});

	$('table_centres').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat toto středisko?')){
			new Request.HTML({
				url:this.href,
				onComplete: function(){
				    domwin.loadContent('domwin');
				}
			}).send();
		}
	});
	
	$('domwin').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_edit',
			sizes		: [500,290],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
    
</script>