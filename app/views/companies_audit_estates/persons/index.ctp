<div class="win_fce top">
<a href='/companies_audit_estates/person_edit/<?php echo $company_id;?>' class='button edit' id='company_person_add_new' title='Přidání novou osobu'>Přidat osobu</a>
</div>
<table class='table' id='table_persons'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($person_list) && count($person_list) >0):?>
    	<?php foreach($person_list as $person_item):?>
    	<tr>
    		<td><?php echo $person_item['CompanyPerson']['id'];?></td>
    		<td><?php echo $person_item['CompanyPerson']['name'];?></td>
    		<td>
    			<a title='Editace osoby "<?php echo $person_item['CompanyPerson']['name'];?>"' 	class='ta edit' href='/companies_audit_estates/person_edit/<?php echo $company_id;?>/<?php echo $person_item['CompanyPerson']['id'];?>'/>edit</a>
                <?php
                if($logged_user['CmsGroup']['id'] == 1){
                ?>
                	<a title='Odstranit osobu 'class='ta trash' href='/companies_audit_estates/person_trash/<?php echo $person_item['CompanyPerson']['id'];?>'/>trash</a>
        		<?php }?>
            
            </td>
    	</tr>
    	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této firmě nebyla nadefinována osoba</td>
	</tr>
	<?php endif;?>
</table>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyTemplateClose' class='button'/>
	</div>
<script>
	$('ListCompanyTemplateClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin');});

	$('table_persons').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto osobu?')){
			new Request.HTML({
				url:this.href,
				onComplete: function(){
				    domwin.loadContent('domwin');
				}
			}).send();
		}
	});
	
	$('domwin').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_edit',
			sizes		: [880,590],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
    
</script>