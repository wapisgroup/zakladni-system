<?php 

function recursive_folder($folders,$htmlExt){
    if(empty($folders)) 
        return true;
    
    echo '<ul style="margin-left:20px;">';
    foreach($folders as $folder_name=>$folder){
        echo '<li>';
            echo $htmlExt->checkbox('CmsGroup/usershare_folders/'.$folder_name);
            echo ' '.$folder_name.'<br />';
            recursive_folder($folder['sublist'],$htmlExt);
            //echo '<br />';
        echo '</li>';   
    }
    echo '</ul>';
}
?>
<form action='/cms_groups/edit/' method='post' id='cms_group_edit_formular'>
	<?php echo $htmlExt->hidden('CmsGroup/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<?php 
				$i=2;
				foreach($menu_items as $key=>$value){
					echo '<li class="ousko"><a href="#krok'.$i.'">'.$value['caption'].'</a></li>';
					$i++;
				}
			?>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní informace</legend>
				<?php echo $htmlExt->input('CmsGroup/name',array('tabindex'=>1,'label'=>'Název'));?> <br class="clear">
				<?php echo $htmlExt->selectTag('CmsGroup/cms_group_superior_id',$group_list,null,array('label'=>'Nadřazená skupina','class'=>'','label_class'=>''));?><br />
				
                <?php echo $htmlExt->textarea('CmsGroup/text',array('tabindex'=>3,'label'=>'Popis'));?> <br class="clear">
			</fieldset>
			<div class="krok">
				<a href='#krok2' class='admin_dom_move next'>Další krok</a>
			</div>
		</div>
		<?php 
		$type_permission = array(1=>'Nepřístupno',2=>'Jen své',3=>'Všechny');
		
			foreach($menu_items as $key=>$value){
				echo '
					<div class="domtabs field">
							<fieldset>
								<legend>Permission - '.$value['caption'].' </legend>
							
				';
                echo '<fieldset><legend>Zvolit vše</legend>';
                echo $htmlExt->radio('ChooseAll',$type_permission,null,array('class'=>'radio choose_all'));
                echo '</fieldset>';
				// inicializace pole kde ukladam aliasy jiz vykreslenych pro foreach nove pridanych modulu
				$vykresleni = Array();
				
				//vykresli hlavni sekci pokud neni v childech take
				//pr($value['child']);
				if(!array_key_exists($value['name'],$value['child']) && array_key_exists($value['name'],$permission_list)){
					$modul_alias = $value['name'];
					$modul = $permission_list[$value['name']];
					?>
							<fieldset>
								<legend><?php echo $modul['name'];?></legend>
								<?php foreach($modul['permission']['radio'] as $col=> $caption):?>
									<label><?php echo $caption;?>:</label>
									<?php echo $htmlExt->radio('CmsGroup/permission/'.$modul_alias.'/'.$col,$type_permission,null,array('class'=>'radio'));?>
									<br class='clear'/>
								<?php endforeach;?>
								<?php if (isset($modul['permission']['select']) && count($modul['permission']['select'])>0):?>
									<?php foreach($modul['permission']['select'] as $col => $data):
                                        $_select_data = array();
                                        if(isset($data['data'])){$_select_data = $data['data'];}
                                        else if(isset(${$data['data_list']})){ $_select_data = ${$data['data_list']};}
                                    ?>
										<label><?php echo $data['caption'];?></label>
										<?php echo $htmlExt->selectTag('CmsGroup/permission/'.$modul_alias.'/'.$col,$_select_data);?>
										<br class='clear'/>
									<?php endforeach;?>
								<?php endif;?>
								<?php if (isset($modul['permission']['checkbox']) && count($modul['permission']['checkbox'])>0):?>
									<?php foreach($modul['permission']['checkbox'] as $col => $caption):?>
										<label><?php echo $caption;?></label>
										<?php echo $htmlExt->checkbox('CmsGroup/permission/'.$modul_alias.'/'.$col);?>
										<br class='clear'/>
									<?php endforeach;?>
								<?php endif;?>
							</fieldset>
					<?php
					$vykresleni[] = $modul_alias;	
				}
				
				//vykresli childy z menu
				foreach($value['child'] as $child_key=>$child_value){
					foreach($permission_list as $modul_alias => $modul){
					//echo $modul.'<br />';
						if($modul_alias == $child_value['name'] ){
							?>
							<fieldset>
								<legend><?php echo $modul['name'];?></legend>
								<?php foreach($modul['permission']['radio'] as $col=> $caption):?>
									<label><?php echo $caption;?>:</label>
									<?php echo $htmlExt->radio('CmsGroup/permission/'.$modul_alias.'/'.$col,$type_permission,null,array('class'=>'radio'));?>
									<br class='clear'/>
								<?php endforeach;?>
								<?php if (isset($modul['permission']['select']) && count($modul['permission']['select'])>0):?>
									<?php foreach($modul['permission']['select'] as $col => $data):
                                        $_select_data = array();
                                        if(isset($data['data'])){$_select_data = $data['data'];}
                                        else if(isset(${$data['data_list']})){ $_select_data = ${$data['data_list']};}
                                    ?>
										<label><?php echo $data['caption'];?></label>
										<?php echo $htmlExt->selectTag('CmsGroup/permission/'.$modul_alias.'/'.$col,$_select_data);?>
										<br class='clear'/>
									<?php endforeach;?>
								<?php endif;?>
								<?php if (isset($modul['permission']['checkbox']) && count($modul['permission']['checkbox'])>0):?>
									<?php foreach($modul['permission']['checkbox'] as $col => $caption):?>
										<label><?php echo $caption;?></label>
										<?php echo $htmlExt->checkbox('CmsGroup/permission/'.$modul_alias.'/'.$col);?>
										<br class='clear'/>
									<?php endforeach;?>
								<?php endif;?>
							</fieldset>
							<?php
							$vykresleni[] = $modul_alias;	
						}
						
					}
				}
				/*
				//vykresli novych modulu ktere byly prave vytvoreny a nejsou v menu ani v childech.
				foreach($permission_list as $modul_alias => $modul){
					if(($modul['menu']['name'] == $value['name']) && !in_array($modul_alias,$vykresleni) && $modul_alias !='clients'){
							?>
							<fieldset>
								<legend><?php echo $modul['name'];?></legend>
								<?php foreach($modul['permission']['radio'] as $col=> $caption):?>
									<label><?php echo $caption;?>:</label>
									<?php echo $htmlExt->radio('CmsGroup/permission/'.$modul_alias.'/'.$col,$type_permission,null,array('class'=>'radio'));?>
									<br class='clear'/>
								<?php endforeach;?>
								<?php if (isset($modul['permission']['select']) && count($modul['permission']['select'])>0):?>
									<?php foreach($modul['permission']['select'] as $col => $data):?>
										<label><?php echo $data['caption'];?></label>
										<?php echo $htmlExt->selectTag('CmsGroup/permission/'.$modul_alias.'/'.$col,$data['data']);?>
										<br class='clear'/>
									<?php endforeach;?>
								<?php endif;?>
								<?php if (isset($modul['permission']['checkbox']) && count($modul['permission']['checkbox'])>0):?>
									<?php foreach($modul['permission']['checkbox'] as $col => $caption):?>
										<label><?php echo $caption;?></label>
										<?php echo $htmlExt->checkbox('CmsGroup/permission/'.$modul_alias.'/'.$col);?>
										<br class='clear'/>
									<?php endforeach;?>
								<?php endif;?>
							</fieldset>
							<?php
					}
				}
				*/
				echo '</fieldset>';
                
                if($value['caption'] == 'UserShare' && isset($usershare_folders)){
                    echo '<fieldset><legend>Přístup ke složkám</legend>';
                    recursive_folder($usershare_folders,$htmlExt);
                    echo '</fieldset>';
                }
 
                echo '</div>';
			}
		?>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
    $$('.choose_all').addEvent('click', function(e){
        var val = this.get('value');
        console.log(val);
        var parDiv = this.getParent('div.field');
        var radios = parDiv.getElements('input[type="radio"][value="'+val+'"]').set('checked', true);
        console.log(radios);
    });
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('cms_group_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('cms_group_edit_formular').action,		
				onComplete:function(){
					click_refresh($('CmsGroupId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('cms_group_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('cms_group_edit_formular',{
		'CmsGroupName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('cms_group_edit_formular',<?php echo (isset($this->data['CmsGroup']['id']))?'true':'false';?>);
</script>