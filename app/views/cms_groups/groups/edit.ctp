<form id='add_edit_group_formular' action='/cms_groups/group_edit/' method='post'>
	<?php echo $htmlExt->hidden('CmsGroupSuperior/id');?>
	<fieldset>
		<legend>Základní</legend>
		<?php echo $htmlExt->input('CmsGroupSuperior/name',array('label_class'=>'long','class'=>'long','label'=>'Název skupiny'));?><br />
	</fieldset>	
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditGroupSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditGroupClose'/>
	</div>
</form>
<script>
$('AddEditGroupClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_group_edit');});
$('AddEditGroupSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('add_edit_group_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('add_edit_group_formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
							domwin.loadContent('domwin_groups');
							domwin.closeWindow('domwin_group_edit');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
					
				}
			}).post($('add_edit_group_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	
	validation.define('add_edit_group_formular',{
		'CmsGroupSuperiorName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název skupiny'}
		}		
	});
	validation.generate('add_edit_group_formular',<?php echo (isset($this->data['CmsGroupSuperior']['id']))?'true':'false';?>);

</script>