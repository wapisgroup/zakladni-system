<table class='table' id='rating_table'>
	<tr>
    	<th>ID</th>
    	<th>Pozice</th>
    	<th>Společnost</th>
    	<th>Kontakt</th>
        <th>Email</th>
    </tr>
<?php if (isset($odberatele_list) && count($odberatele_list)>0):?>
	<?php foreach($odberatele_list as $odberatel):?>
		<tr>
			<td><?php echo $htmlExt->hidden('Email/to/'.$odberatel['CampaignItem']['id'],array('value'=>$odberatel['CampaignItem']['mail']));?><?php echo $odberatel['CampaignItem']['id'];?></td>
			<td><?php echo $odberatel['CampaignItem']['pozice'];?></td>
			<td><?php echo $odberatel['Company']['name'];?></td>
			<td><?php echo $odberatel['CompanyContact']['name'];?></td>
			<td><?php echo $odberatel['CampaignItem']['mail'];?></td>
		</tr>
	<?php endforeach?>
<?php else:?>
	<tr>
		<t colspan='2'><em>Nejsou zde žádní odběratelé emailu</em></td>
	</tr>
<?php endif;?>
</table>
<script>
	$('EmailPocet').value = '<?php echo (isset($odberatele_list)?count($odberatele_list):'0'); ?>';
	if($('EmailPocet').value != 0)
		$('EmailPocet').removeClass('require').addClass('valid');
	else 
		$('EmailPocet').removeClass('valid').addClass('require');
</script>