<div id="attachs_box">
    <div class="win_fce top">
    <a href='/campaign_items_tippers/attachs_edit/<?php echo $id;?>' class='button edit' id='company_attach_add_new' title='Přidání nové přílohy'>Přidat přílohu</a>
    </div>
    <table class='table' id='table_attachs'>
    	<tr>
    		<th>#ID</th>
    		<th>Název</th>
    		<th>Typ</th>
    		<th>Vytvořeno</th>
    		<th>Možnosti</th>
    	</tr>
    	<?php if (isset($attachment_list) && count($attachment_list) >0):?>
    	<?php foreach($attachment_list as $attachment_item):?>
    	<tr>
    		<td><?php echo $attachment_item['CampaignTemplatesAttachment']['id'];?></td>
    		<td><?php echo $attachment_item['CampaignTemplatesAttachment']['name'];?></td>
    		<td><?php echo $attachment_item['SettingAttachmentType']['name'];?></td>
    		<td><?php echo $fastest->czechDateTime($attachment_item['CampaignTemplatesAttachment']['created']);?></td>
    		<td>
    			<a title='Editace přílohy "<?php echo $attachment_item['CampaignTemplatesAttachment']['name'];?>"' 	class='ta edit' href='/campaign_items_tippers/attachs_edit/<?php echo $id;?>/<?php echo $attachment_item['CampaignTemplatesAttachment']['id'];?>'/>edit</a>
    			<a title='Odstranit přílohu'class='ta trash' href='/campaign_items_tippers/attachs_trash/<?php echo $id;?>/<?php echo $attachment_item['CampaignTemplatesAttachment']['id'];?>'/>trash</a>
    			<a title='Stáhnout přílohu'class='ta download' href='/campaign_items_tippers/attachs_download/campaign_items_tippers|attachment|<?php echo $attachment_item['CampaignTemplatesAttachment']['file'];?>/<?php echo $attachment_item['CampaignTemplatesAttachment']['alias_'];?>/'/>download</a>
    		</td>
    	</tr>
    	<?php endforeach;?>
    	<?php else:?>
    	<tr>
    		<td colspan='5'>K této šabloně nebyla nadefinována příloha</td>
    	</tr>
    	<?php endif;?>
    </table>
</div>
<script>
	$('table_attachs').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto přílohu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_group_edit').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('attachs_box').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_attach_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>