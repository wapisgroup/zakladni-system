 <form action='/campaign_items_tippers/edit/' method='post' id='kampan_edit_formular'>
	<?php echo $htmlExt->hidden('CampaignItem/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<?php if (empty($this->data['CampaignItem']['id']) || $this->data['CampaignItem']['stav'] == 1):?>
				<legend>Zvolte kampaň</legend>
				<div class="sll">  
					<?php echo $htmlExt->selectTag('CampaignItem/campaign_group_id',$campaign_group_list,null,array('tabindex'=>1,'label'=>'Kampaň'));?>
				</div>
				<?php else:?>
				<legend>Kampaň</legend>
				<div class="sll">  
					<?php echo $htmlExt->var_text('CampaignGroup/name',array('tabindex'=>1,'label'=>'Kampaň'));?>
				</div>
				<?php endif;?>
				<div class='slr'>
				</div>
			</fieldset>
			<fieldset>
				<legend>Zadejte údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->selectTag('CampaignItem/company_id',$company_list,null,array('tabindex'=>2,'label'=>'Společnost','style'=>'width:53.5%'));?>
					<img src='/css/fastest/icons/add.png' title='Přidat společnost' style='cursor:pointer;' id='add_company'/>
					 <br class="clear">
					 
					<?php echo $htmlExt->selectTag('CampaignItem/company_contact_id',$company_contact_list,null,array('tabindex'=>4,'label'=>'Kontaktní osoba','style'=>'width:53.5%'));?> 
					<img src='/css/fastest/icons/add.png' title='Přidat kontakt' style='cursor:pointer;' id='add_company_contact'/>
					<br class="clear">
					
					<?php echo $htmlExt->input('CampaignItem/telefon',array('tabindex'=>6,'label'=>'Telefon'));?> <br class="clear">
					<?php echo $htmlExt->inputDate('CampaignItem/datum_inzeratu',array('tabindex'=>8,'label'=>'Datum inzerátu'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CampaignItem/pozice',array('tabindex'=>3,'label'=>'Požadovaná pozice'));?> <br class="clear">
					<?php echo $htmlExt->input('CampaignItem/mail',array('tabindex'=>5,'label'=>'Email'));?> <br class="clear">
					<?php echo $htmlExt->input('CampaignItem/mobil',array('tabindex'=>7,'label'=>'Mobil'));?> <br class="clear">
					<?php echo $htmlExt->input('CampaignItem/inzerat_z',array('tabindex'=>9,'label'=>'Umístění inzerátu'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button','tabindex'=>10));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button','tabindex'=>11));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});

	/*
	* Nastavit automaticky focus na select
	*/
    if($('CampaignItemCampaignGroupId'))
	   $('CampaignItemCampaignGroupId').focus();
	
	/*
	* Pridani nove spolecnosti, zavola se domwin pro zadani nove spolecnosti
	* refreshne se select a vybere se automaticky dana spolecnost
	*/
	$('add_company').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_add_company',
			sizes		: [800,500],
			scrollbars	: false,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: '/campaign_items_tippers/add_company/',
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true
		}); 
	});


	/*
	* Pridani noveho kontaktu pro spolecnost, zavola se domwin pro zadani nove kontaktu spolecnosti.
	* Pokud neni zvolena spolecnost, hod mu alert, ale ji musi nejprve zvolit
	*/
	$('add_company_contact').addEvent('click',function(e){
		new Event(e).stop();

		var spolecnost_id = $('CampaignItemCompanyId').value;

		if (spolecnost_id == ''){
			alert('Pro zadání nového kontaktu, musíte zvolit nejdříve společnost!!!');
		} else {
			domwin.newWindow({
				id			: 'domwin_add_company_contact',
				sizes		: [800,500],
				scrollbars	: false,
				title		: this.title,
				languages	: false,
				type		: 'AJAX',
				ajax_url	: '/campaign_items_tippers/add_company_contact/' + spolecnost_id,
				closeConfirm: false,
				max_minBtn	: false,
				modal_close	: true
			}); 
		}
	});

	/*
	* Natahnuti kontaktu k dane firme po zmene selectu seznamu firem, vse pres JS Request.JSON
	* Pokud zvolim prazdnou hodnotu, vymaze se mi select z kontakty
	*/
	$('CampaignItemCompanyId').addEvent('change', function(){
		if (this.value == ''){
			$('CampaignItemCompanyContactId').empty();
		} else {
			new Request.JSON({
				url: '/campaign_items_tippers/load_company_contacts/' + this.value,
				onComplete: function(json){
					if (json){	
						if (json.result === true){
							$('CampaignItemCompanyContactId').empty();
							new Element('option',{html:'', value:'', title:''}).inject($('CampaignItemCompanyContactId'));
							$each(json.data, function(caption, id){
								new Element('option',{html:caption, value:id, title:caption}).inject($('CampaignItemCompanyContactId'));
							})
						} else {
							alert(result.message);
						}
					} else {
						alert('Chyba aplikace');
					}
				}
			}).send();
		}
	});

	/*
	* Nacteni informaci o kontaktu po vyberu kontaktu ze selectu, vlozi se email, telefon, mobil.
	* Mobil se bere z hodnoty CompanyContact.telefon2
	*/
	$('CampaignItemCompanyContactId').addEvent('change', function(){
		if (this.value == ''){
			$('CampaignItemTelefon').value ='';
			$('CampaignItemMail').value ='';
			$('CampaignItemMobil').value ='';
		} else {
			new Request.JSON({
				url: '/campaign_items_tippers/load_company_contact_info/' + this.value,
				onComplete: function(json){
					if (json){	
						if (json.result === true){
							$('CampaignItemTelefon').value 	= json.data.CompanyContact.telefon1;
							$('CampaignItemMail').value 	= json.data.CompanyContact.email;
							$('CampaignItemMobil').value 	= json.data.CompanyContact.telefon2;
						} else {
							alert(result.message);
						}
					} else {
						alert('Chyba aplikace');
					}
				}
			}).send();
		}
	})
	
	/*
	* Vlastni ulozeni daneho pozadavku
	* dojde ke kontrole validity, kde nutne zaznamy jsou id kampane, id spolecnosti, id kontaktni osoby
	* datum inzeratu a "inzerat z"
	*/
	
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		/*
		* Nahrada za event na kalendar
		*/
		if ($('calbut_CampaignItemDatumInzeratu').value != ''){
			$('calbut_CampaignItemDatumInzeratu').removeClass('require').addClass('valid');
		}
		valid_result = validation.valideForm('kampan_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('kampan_edit_formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
							click_refresh($('CampaignItemId').value);
							domwin.closeWindow('domwin');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
					
				}
			}).post($('kampan_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	
	validation.define('kampan_edit_formular',{
		'CampaignItemCampaignGroupId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zvolit kampaň'},
		},
		'CampaignItemCompanyId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zvolit společnost'},
		},
		'CampaignItemCompanyContactId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zvolit kontakt společnosti'},
		},
		'CampaignItemPozice': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit požadovanou pozici'},
		},
		'calbut_CampaignItemDatumInzeratu': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum inzerátu'},
		},
		'CampaignItemInzeratZ': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zvolit umístění inzerátu'},
		}
	});
	validation.generate('kampan_edit_formular',<?php echo (isset($this->data['CampaignItem']['id']))?'true':'false';?>);
	
</script>