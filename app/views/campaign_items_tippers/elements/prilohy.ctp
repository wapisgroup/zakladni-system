<fieldset >
	<legend >Přílohy Emailu</legend>
	<div>
		<div class="sll" >  
			<?php echo $htmlExt->input('Newsletter/attach_name',array('tabindex'=>2,'label'=>'Popis přílohy'));?> <br />
			<?php echo $htmlExt->input('Newsletter/attach_file',array('class'=>'file_browse','tabindex'=>2,'label'=>'Příloha'));?> 
			<img src="/css/fastest/upload/browse.png" alt="Vybrat soubor" title="Vybrat soubor" id="browse_attach" class="icon top" /><br />
			
			 <script language="JavaScript" type="text/javascript">
						$('browse_attach').addEvent('click',function(e){
							new Event(e).stop();
							domwin.newWindow({
								id			: 'filemanager',
								sizes		: [1000,420],
								scrollbars	: false,
								languages	: false,
								title		: this.title,
								ajax_url	: '/ftp/?win=true&input=NewsletterAttachFile',
								closeConfirm: false,
								max_minBtn	: false,
								modal_close	: false
							}); 
							
						});
			</script>
			
			<label>&nbsp;</label><input type='button' value='Nahrát přílohu' id='add_attachment_button'/>
		</div>
		<div class="slr" id='prilohy_list'>  
			<?php echo $this->renderElement('../newsletters/elements/prilohy_list');?>
		</div>
	<br />
	</div>
</fieldset>
<script>
	$('add_attachment_button').addEvent('click', function(e){
		new Event(e).stop();
		
		
		
		if ($('NewsletterAttachName').value == '' || $('NewsletterAttachFile').value == ''){
			var invalid = [];
			if ($('NewsletterAttachName').value == '') invalid[invalid.length] = 'Musíte zadat popis přílohy.'
			if ($('NewsletterAttachFile').value == '') invalid[invalid.length] = 'Musíte zadat soubor přílohy.'
			var prilohy_add_err = new MyAlert(invalid);
			//prilohy_add_err.show(invalid)
		} else {
			$$('no_attachment').addClass('none');
			var tmp_file = $('NewsletterAttachFile').value;
			var table = $('prilohy_list').getElement('table');
			var tr = new Element('tr').inject(table);
			new Element('td').setHTML($('NewsletterAttachName').value).inject(tr);
			//new Element('td').setHTML($('NewsletterAttachFile').value).inject(tr);
			var el_name = $('NewsletterAttachFile').value.split('/');
			var name = el_name[el_name.length-1];
			new Element('td').setHTML(name).inject(tr);
			
			new Element('img',{'class':'icon', alt:'Smazat', title: 'Smazat', src:'/css/fastest/table/ta_trash_all.png'})
				.inject(new Element('td').inject(tr))
				.addEvent('click', function(){
					if (confirm('Opravdu si přejete smazat přílohu?')) 
						tr.dispose();
				})
			new Element('input',{type:'hidden', name:'data[Newsletter][attachments][]', value:$('NewsletterAttachName').value + '|' + tmp_file}).inject(tr);
			
			$('NewsletterAttachName').value = '';
			$('NewsletterAttachFile').value = '';
		} 
	})
</script>