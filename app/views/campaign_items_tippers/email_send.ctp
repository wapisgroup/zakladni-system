 <form action='/campaign_items_tippers/email_send/' method='post' id='kampan_akt_edit_formular'>

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<li class="ousko"><a href="#krok2">Odběratelé</a></li>
			<li class="ousko"><a href="#krok2">Přílohy</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zadejte údaje</legend>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('Email/campaign_group_id',$campaign_group_list,null,array('tabindex'=>1,'label'=>'Kampaň'));?> <br class="clear">
					<?php echo $htmlExt->input('Email/subject',array('tabindex'=>3,'label'=>'Předmět'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('Email/campaign_template_id',$campaign_template_list,null,array('tabindex'=>5,'label'=>'Šablona'));?> <br class="clear">			
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('Email/from',$parent_companies,null,array('tabindex'=>2,'label'=>'Od'));?> <br class="clear">
					<?php echo $htmlExt->input('Email/pocet',array('tabindex'=>4,'label'=>'Počet odběratelů','value'=>0,'readonly'=>'readonly'));?> <br class="clear">
				</div>
				<br />
				
                <label class="long">Text:</label>
				<?php echo $wysiwyg->render('Email/text',array('tabindex'=>5));?> <br class="clear"/>
            </fieldset>
		</div>
		<div class="domtabs field">     
			<fieldset>
				<legend>Odběratelé</legend>
				<div id='odberatele_table'>
					<?php echo $this->renderElement('../campaign_items_tippers/email_send_odberatele');?>
				</div>            
            </fieldset>
		</div>
		<div class="domtabs field">     
			<fieldset>
				<legend>Přílohy</legend>
				<?php echo $this->renderElement('../campaign_items_tippers/email_send/attachs');?>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','class'=>'button'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','class'=>'button'));?>
	</div>
</form>
<script language="javascript" type="text/javascript">

	if($('EmailCampaignGroupId'))
		$('EmailCampaignGroupId').addEvent('change', function(){
			if (this.value == ''){
				
			} else {
				new Request.HTML({
					url: '/campaign_items_tippers/load_capaign_odberatele/' + this.value,
					update: 'odberatele_table'
				}).send();
			}
		});

    $$('.wysiwyg').makeWswg();
	var domtab = new DomTabs({'className':'admin_dom'});
    
	$('EmailCampaignTemplateId').addEvent('change', function(e){
		e.stop();
		if (this.value == '') {
			$$('.wysiwyg').killWswg();
			$('EmailText').value = '<p></p>';
			//$('EmailSubject').value = '<p></p>';
			$$('.wysiwyg').makeWswg();
		} else {
			new Request.JSON({
				url: '/campaign_items_tippers/load_template/' + this.value,
				onComplete: function(json){
					if (json) {
						if (json.result === true) {
							$$('.wysiwyg').killWswg();
							$('EmailText').value = json.data.text;
							$('EmailSubject').value = json.data.name;
							$$('.wysiwyg').makeWswg();
                            
                            //zmen tridu aby byla validacka spravne pokud vyplnujeme name
                            if(json.data.name != '' && !$('EmailSubject').hasClass('valid')){
                                $('EmailSubject').removeClass('invalid');
                                $('EmailSubject').removeClass('require');
                                $('EmailSubject').addClass('valid');
                            }
                            
                            
                            //smazani priloh z predeslych templates ktere mozna byli nacteny
                            var tbody = $('table_attachs');
                                tbody.getElements('tr').each(function(tr){
                                    if(tr.hasClass('temp'))
                                        tr.dispose();
                            });
                            
                            
                            //nacteni priloh
                            json.data.prilohy.each(function(item){
                                uid = uniqid();
                        		var td_html = '<td>' + item.name + '</td>';
                        		   td_html += '<td>' + $('CampaignTemplatesAttachmentSettingAttachmentTypeId').options[item.setting_attachment_type_id].title + '</td>';
                        		   td_html += '<td>'+item.created+'</td>';   	
                        		   td_html += '<td><a href="#" class="ta trash" title="Smazat">Smazat</a></td>';
                        		   td_html += '<input name="data[prilohy]['+uid+'][asfile]" type="hidden" value="'+item.name+'" />';
                        		   td_html += '<input name="data[prilohy]['+uid+'][filename]" type="hidden" value="'+item.file+'" />';
            
                        		var tr = new Element('tr',{class:'temp'}).inject(tbody).setHTML(td_html);
                        		tr.getElement('.trash').addEvent('click',delete_attach.bindWithEvent(this));  
                                
                            });
						}
						else {
							alert(json.message);
						}
					}
					else {
						alert('Chyba aplikace');
					}
				}
			}).send();
		}
		
	})
		

	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		$$('.wysiwyg').killWswg();
		if ($$('.disabled_save').length != 0)
			alert('Je aktivní HTML mód editoru. Přepněte jej prosím do normální módu.');
		else {
			valid_result = validation.valideForm('kampan_akt_edit_formular');
			if (valid_result == true){
				new Request.JSON({
					url:$('kampan_akt_edit_formular').action,		
					onComplete:function(json){
						if (json){
							if (json.result === true){
								alert('Emaily odeslány!');
								domwin.closeWindow('domwin');
								click_refresh();
							} else {
								alert('json.message');
							}
						} else {
							alert('Chyba aplikace');
						}
					    
	
					}
				}).post($('kampan_akt_edit_formular'));
			} else {
				var error_message = new MyAlert();
				error_message.show(valid_result)
			}
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); $$('.wysiwyg').killWswg();  domwin.closeWindow('domwin');});

	validation.define('kampan_akt_edit_formular',{
		'EmailCampaignGroupId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zvolit kampaň.'}
		},
		'EmailFrom': {
			'testReq': {'condition':'not_empty','err_message':'Musíte zvolit odesilatele.'}
		},
		'EmailSubject': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit předmět.'}
		},
		'EmailPocet': {
			'not_equal': {'condition':'0','err_message':'Počet odběratelů musí být větší než nula.'}
		}		
	});
	
	validation.generate('kampan_akt_edit_formular',false);
	
</script>