<br/>
<a href='/campaign_items_tippers/group_edit/' class='button add_new_group' title='Přidat novou kampaň'>Přidat novou kampaň</a>
<br /><br />
<table class='table' id='groups_table'>
	<tr>
		<th>ID</th>
		<th>Název</th>
		<th>Stav</th>
		<th>Možnosti</th>
	</tr>
<?php if (isset($group_list) && count($group_list)>0): ?>
	<?php foreach($group_list as $group):?>
	<tr>
		<td><?php echo $group['CampaignGroup']['id'];?></td>
		<td><?php echo $group['CampaignGroup']['name'];?></td>
		<td><?php echo $kampane_stav[$group['CampaignGroup']['stav']];?></td>
		<td>
			<?php if ($group['CampaignGroup']['stav'] == 0):?>
			<a href='/campaign_items_tippers/group_edit/<?php echo $group['CampaignGroup']['id'];?>' class='ta edit' title='Editace kmapaně: <?php echo $group['CampaignGroup']['name'];?>'>Edit</a>
			<?php endif;?>
			<!-- a href='/campaign_items/groups/delete/<?php echo $group['CampaignGroup']['id'];?>' class='ta trash' title='Smazání kmapaně: <?php echo $group['CampaignGroup']['name'];?>'>Trash</a-->
		</td>
	</tr>
	<?php endforeach;?>
<?php else: ?>
	<tr>
		<td colspan='3'>Nenalezeno</td>
	</tr>
<?php endif; ?>
</table>
<div class="win_save">
	<?php echo $htmlExt->button('Zavřít',array('id'=>'DomwinGroupsClose','class'=>'button','tabindex'=>2));?>
</div>
<script>
$('DomwinGroupsClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_groups');});
$('domwin_groups').getElement('.add_new_group').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_group_edit',
		sizes		: [550,180],
		scrollbars	: false,
		languages	: false,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: true,
		max_minBtn	: false,
		modal_close	: false,
		remove_scroll: false
	}); 
});

$('groups_table').getElements('.edit').addEvent('click', function(e){
	new Event(e).stop();
	domwin.newWindow({
		id			: 'domwin_group_edit',
		sizes		: [550,180],
		scrollbars	: false,
		languages	: false,
		title		: this.title,
		ajax_url	: this.href,
		closeConfirm: true,
		max_minBtn	: false,
		modal_close	: false,
		remove_scroll: false
	}); 
});

$('groups_table').getElements('.trash').addEvent('click', function(e){
	new Event(e).stop();
	if (confirm('Opravdu si přejete odstranit tuto položku?')){
		new Request.HTML({
			url: this.href,
			update: $('domwin_groups').getElement('.CB_ImgContainer')
		}).send();
	}
});
</script>
