<form action='/report_contact_companies/edit/' method='post' id='company_contact_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyContact/id');?>
	<fieldset>
		<legend>Firma</legend>
		<?php echo $htmlExt->selectTag('CompanyContact/company_id',$company_list,null,array('label'=>'Firma'),null,false);?><br />

	</fieldset>
	<fieldset>
		<legend>Vyplňte následující údaje</legend>
		<div class='sll'>
			<?php echo $htmlExt->input('CompanyContact/jmeno',array('tabindex'=>1,'label'=>'Jméno'));?> <br class="clear">
			<?php echo $htmlExt->input('CompanyContact/telefon1',array('tabindex'=>3,'label'=>'Telefon'));?> <br class="clear">
			<?php echo $htmlExt->input('CompanyContact/email',array('tabindex'=>5,'label'=>'Email'));?> <br class="clear">
					
		</div>
		<div class='slr'>
			<?php echo $htmlExt->input('CompanyContact/prijmeni',array('tabindex'=>2,'label'=>'Příjmení'));?> <br class="clear">
			<?php echo $htmlExt->input('CompanyContact/telefon2',array('tabindex'=>4,'label'=>'Telefon'));?> <br class="clear">
			<?php echo $htmlExt->input('CompanyContact/position',array('tabindex'=>6,'label'=>'Pozice'));?> <br class="clear">		
		</div>
		<br/>
		<?php echo $htmlExt->textarea('CompanyContact/comment',array('tabindex'=>7,'label'=>'Komentář','class'=>'long','label_class'=>'long'));?> <br class="clear">
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyContactSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyContactClose'/>
	</div>
</form>
<script>
	$('AddEditCompanyContactClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	
	$('AddEditCompanyContactSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_contact_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('company_contact_edit_formular').action,		
				onComplete:function(){
					click_refresh($('CompanyContactId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('company_contact_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_contact_edit_formular',{
		'CompanyContactJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'}
		},
		'CompanyContactPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'}
		}
	});
	validation.generate('company_contact_edit_formular',<?php echo (isset($this->data['CompanyContact']['id']))?'true':'false';?>);
	
</script>