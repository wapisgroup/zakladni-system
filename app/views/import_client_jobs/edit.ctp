<div id="client_edit_full">
<form action='/import_client_jobs/edit/' method='post' id='jobspartners_form'>
	<?php echo $htmlExt->hidden('ImportClientJob/id'); ?>
	<input type="hidden" name="data[NewConnection]" id="new_connection" value="0" />
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>			
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Osobní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('ImportClientJob/jmeno', array('tabindex' => 1, 'label' =>'Jméno')); ?><br />
					<?php echo $htmlExt->input('ImportClientJob/prijmeni', array('tabindex' => 3,'label' => 'Příjmení')); ?><br />
				</div>
				<div class="slr">  
					<?php echo $htmlExt->inputDate('ImportClientJob/datum_narozeni', array('tabindex' =>4, 'label' => 'Datum narození')); ?><br />
				</div>
			</fieldset>	
			<fieldset>
				<legend>Kontaktní údaje</legend>
				<div class="sll">  
                    <p>Telefony uvádějte ve formátu (00420123456789)</p>
					<?php echo $htmlExt->input('ImportClientJob/mobil1', array('tabindex' => 5, 'label' =>'Telefon', 'class' => 'mobile integer', 'MAXLENGTH' => 14)); ?><br />		
			        <br />
                    <?php echo $htmlExt->input('ImportClientJob/email', array('tabindex' =>8, 'label' => 'Email')); ?><br />
			
				</div>
			    <div class="slr" > 
                    <p>&nbsp;</p>
                    <?php //pr($this->data['ImportClientJob']);?>
					<?php echo $htmlExt->input('ImportClientJob/ulice', array('tabindex' => 9, 'label' =>'Ulice')); ?><br />
					<?php echo $htmlExt->input('ImportClientJob/mesto', array('tabindex' => 10, 'label' =>'Město')); ?><br />		
                    <?php echo $htmlExt->input('ImportClientJob/psc', array('tabindex' => 11, 'label' =>'PSČ')); ?><br />
                    <?php echo $htmlExt->selectTag('ImportClientJob/countries_id', $client_countries_list, null,array('tabindex' => 12, 'label' => 'Okres'), null, false); ?><br />
                    <?php echo $htmlExt->selectTag('ImportClientJob/kraj_id', $client_kraj_list, null,array('tabindex' => 12, 'label' => 'Kraj'), null, false); ?><br />
					<?php echo $htmlExt->selectTag('ImportClientJob/stat_id', $client_stat_list, null,array('tabindex' => 12, 'label' => 'Stát')); ?><br />
					
				</div>
			</fieldset>
            <fieldset>
                <legend>Hledám</legend>
                    <div class="sll">
                        <?php echo $htmlExt->selectTag('ImportClientJob/hledam', $form_www_hledam, null,array( 'label' => 'Hledám')); ?><br />
                        <?php echo $htmlExt->selectTag('ImportClientJob/work_stat',array(0=>'Celý stát')+$client_stat_list, null,array( 'label' => 'Stát')); ?><br />
                    
                    </div>
                    <div class="slr">
                        <?php echo $htmlExt->selectTag('ImportClientJob/profese', $form_www_profese, null,array( 'label' => 'Profese')); ?><br />
                        <?php echo $htmlExt->selectTag('ImportClientJob/work_kraj',array(0=>'Celý stát')+$client_kraj_list, null,array( 'label' => 'Kraj')); ?><br />
                    
                    </div>
            </fieldset>
			<fieldset>
                <legend>Životopis</legend>
                    <?php 
                    if (isset($this->data['ImportClientJob']['files']) && !empty($this->data['ImportClientJob']['files'])){ 
                        foreach ($this->data['ImportClientJob']['files'] AS $file)
                            echo '<var>'.$html->link('Stáhnout životopis','http://www.jobspartners.sk/uploaded/form_zivotopis/'.$file,array('target'=>'_blank'),null,false).'</var>';
                    } else {
                        echo '<p>Nejsou připojeny žádné životopisy.</p>';
                        
                    }?>
            </fieldset>
			
	
		
			
			<fieldset>
				<legend>Poznamka</legend>
					<?php echo $htmlExt->textarea('ImportClientJob/poznamka', array('label' =>'Poznamka', 'class' => 'long', 'label_class' => 'long')); ?><br/>
			</fieldset>
		</div>
		       
		
	</div>
	<div class="win_save">
	    <?php echo $htmlExt->button('Uložit', array('id' => 'save_close', 'class' => 'button')); ?>
		<?php echo $htmlExt->button('Zavřít', array('id' => 'close', 'class' =>'button')); ?>
	</div>
</form>
</div>
 <script language="javascript" type="text/javascript">
    var form_id = 'jobspartners_form';
 
	var domtab = new DomTabs({'className':'admin_dom'}); 
    $(form_id).getElements('.float, .integer').inputLimit();
     
  	 

    $('save_close').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('jobspartners_form');
		if (valid_result == true){
			new Request.JSON({
				url:$('jobspartners_form').action,		
				onComplete:function(){
					click_refresh($('ImportClientJobId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('jobspartners_form'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
    
    $('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	
    validation.define(form_id,{
		'ImportClientJobJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		},
		'ImportClientJobMobil1': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit mobil'},
			'length': {'condition':{'min':14,'max':14}, 'err_message':'Mobil musí mít 14 znaků'},		
		},
		'ImportClientJobPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'},
		}
	});
	validation.generate(form_id,<?php echo (isset($this->data['ImportClientJob']['id'])) ?
'true' : 'false'; ?>);
	
	
</script>