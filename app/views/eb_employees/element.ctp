<input type='hidden' value='0' id='made_change_in_form' />
<form action='/eb_employees/odpracovane_hodiny/' method='post' id='EbClientWorkingHour_edit_formular'>
	<!-- Basic Hidden Fields -->
	
	<?php //echo $htmlExt->hidden('EbClientWorkingHour/id');?>
	<?php echo $htmlExt->hidden('EbClientWorkingHour/company_id');?>
	<?php echo $htmlExt->hidden('EbClientWorkingHour/client_id');?>
	<?php //echo $htmlExt->hidden('EbClientWorkingHour/company_order_id');?>
	<?php //echo $htmlExt->hidden('EbClientWorkingHour/connection_client_order_id');?>
	<?php echo $htmlExt->hidden('EbClientWorkingHour/rok',array('value'=>$rok));?>
	<?php echo $htmlExt->hidden('EbClientWorkingHour/mesic',array('value'=>$mesic));?>

<!-- END -->

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
            <?php 
             foreach($this->data['connections'] as $id=>$con){
                echo '<li class="ousko"><a href="#krok'.$id.'">'.$con['EbCompany']['name'].' - '.$con['EbCompany']['order_name'].'</a></li>';
             }?>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		
			<?php 
            //pr($this->data['connections'][12]['EbClientWorkingHour']);
             foreach($this->data['connections'] as $id=>$con){
                echo '<div class="domtabs field" rel="'.$id.'" >';
			    echo $this->renderElement('../eb_employees/tab_item',array('connection'=>$con,'month'=>$mesic, 'year' =>$rok));
                echo '</div>';
             }?>
        
        <div id="TogetherForOneEmployee">
            <br />
			<fieldset>
				<legend>Informace o docházce</legend>
				<div class='slr3'>
					<?php echo $htmlExt->input('EbClientWorkingHour/vikendy',array('readonly'=>'readonly','label'=>'Víkendy','class'=>'read_info'));?>
					<?php echo $htmlExt->input('EbClientWorkingHour/ocestrovani_clena_rodiny',array('readonly'=>'readonly','label'=>'OČR','class'=>'read_info'));?>
					<?php echo $htmlExt->input('EbClientWorkingHour/nahradni_volno',array('readonly'=>'readonly','label'=>'V','class'=>'read_info'));?>
					<?php echo $htmlExt->input('EbClientWorkingHour/celkem_hodin',array('readonly'=>'readonly','label'=>'Celkem hodin','class'=>'read_info'));?>
				</div>
				<div class='slr3'>
					<?php echo $htmlExt->input('EbClientWorkingHour/prescasy',array('readonly'=>'readonly','label'=>'Přesčasy','class'=>'read_info'));?>	
					<?php echo $htmlExt->input('EbClientWorkingHour/neplacene_volno',array('readonly'=>'readonly','label'=>'NV','class'=>'read_info'));?>
					<?php echo $htmlExt->input('EbClientWorkingHour/pracovni_neschopnost',array('readonly'=>'readonly','label'=>'PN','class'=>'read_info'));?>
				</div>
				<div class='slr3'>
					<?php echo $htmlExt->input('EbClientWorkingHour/svatky',array('readonly'=>'readonly','label'=>'Svátky','class'=>'read_info'));?>
					<?php echo $htmlExt->input('EbClientWorkingHour/dovolena',array('readonly'=>'readonly','label'=>'Dovolenka','class'=>'read_info'));?>
					<?php echo $htmlExt->input('EbClientWorkingHour/neomluvena_absence',array('readonly'=>'readonly','label'=>'A','class'=>'read_info'));?>
					<?php echo $htmlExt->input('EbClientWorkingHour/paragraf',array('readonly'=>'readonly','label'=>'PG','class'=>'read_info'));?>
				</div>
			</fieldset>
			<fieldset>
				<legend>Zadání mzdy pro aktuální měsíc</legend>
				<div class='slr3'>
					<?php echo $htmlExt->input('EbClientWorkingHour/salary_per_hour_p',array('label'=>'Mzda za hodiny', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
				</div>
				<div class='slr3'>
					
                    <?php echo $htmlExt->input('EbClientWorkingHour/salary_per_hour',array('label'=>'Na hodinu celkem','class'=>'float  for_null_past_money_item_change'));?><br />
				</div>
				<div class='slr3'>
					
                    <?php echo $htmlExt->selectTag('EbClientWorkingHour/salary_zaco',$salary_zaco, null, array('label'=>'Typ práce'));?><br />
				</div>	
				<br />
			</fieldset>
            <fieldset>
				<legend>Celková výplata</legend>
				<div class='slr3'>
					<label>Pozdržet výplatu:</label><?php echo $htmlExt->checkbox('EbClientWorkingHour/stop_payment');?><br/>
				</div>
				<div class='slr3'>
					<?php echo $htmlExt->input('EbClientWorkingHour/money_sum',array('label'=>'Celkem k vyplacení netto','class'=>'read_info for_null_past_money_item_change'));?><br />
				</div>
				<div class='slr3'>
					<?php echo $htmlExt->input('EbClientWorkingHour/drawback',array('label'=>'Srážka','class'=>'float for_null_past_money_item_change'));?><br />
				</div>
				<label style='width:11.5%; padding-left:10px'>Komentář k docházce:</label><?php echo $htmlExt->textarea('EbClientWorkingHour/comment',array('class'=>'textarea_spec','style'=>'width:67%'));?>				
			</fieldset>
        </div>
	</div>
	<div style='text-align:center'>
	    <?php if ($allow_ulozit == true){ ?>	
        <input type='button' value='Uložit' id='only_save'/>
		<?php }?>
 	    <?php if ($allow_uzavrit == true){ ?>
            <input type='button' value='Uzavřít docházku' id='uzavrit'/>
        <?php }?>
		<input type='button' value='Zavřít' id='only_close'/>
	</div>
</form>

<div id='dblclick_input' class='none'>
<label>Typ:</label>
	<select class='choose_input'>
		<option value="1" 	title="Cas">Cas</option>
		<option value="PN" 	title="Pracovní neschopnost">Pracovní neschopnost</option>
		<option value="D" 	title="Dovolená">Dovolená</option>
		<option value="NV" 	title="Neplacené volno">Neplacené volno</option>
		<option value="OČR" title="Ošetřování člena rodiny">Ošetřování člena rodiny</option>
		<option value="A" 	title="Neomluvená absence">Neomluvená absence</option>
		<option value="V" 	title="Nahradní volno">Nahradní volno</option>
		<option value="PG" 	title="Paragraf">Paragraf</option>
	</select>
	<br/>
	<div class='choose_do_div'>
		<label>Od:</label>
		<select class='choose_od_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_od_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<div class='choose_od_div'>
		<label>Do:</label>
		<select class='choose_do_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_do_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<p>
		<input type='button' value='Vložte' class='choose_do_it'/>
	</p>
</div>

<script language="javascript" type="text/javascript">

	var jsonval = '<?php echo $salary_item_list;?>';
	var txt = jsonval.replace(/(\u000A)+|(\n)+|\u000A|(\u000D)+|\u000D|\u000A\u000D|\n\r/g, ', ');
    var salary_item_list = JSON.decode(txt);
    //var salary_item_list = JSON.decode('[]');

    var logged_group = <?php echo $logged_user['CmsGroup']['id'];?>;
    var logged_group_superior = <?php echo $logged_user['CmsGroup']['cms_group_superior_id'];?>;
    var logged_user = <?php echo $logged_user['CmsUser']['id'];?>;

    var inspector_list = <?= json_encode(Set::combine($this->data['connections'],'{n}.ConnectionClientOrder.id','{n}.CompanyOrder.building_inspector_id'));?>;


    $$('input.cena_ks, input.ks').addEvent('change',recount_task_row);
    $$('.prace_select_items').addEvent('change', function(e){
          this.getParent('td').getNext('td').getElement('input').value = this.options[this.selectedIndex].getProperty('title');
          recount_task_row(e);
    })
   
	/*
	 * initializace domtab a input limit 
	 */ 
	var domtab = new DomTabs({'className':'admin_dom'});
	$('EbClientWorkingHour_edit_formular').getElements('.integer, .float').inputLimit();
	
   /*
    * zmena selectu typu prace a propsani ceny na hodinu
    */
    if($('EbClientWorkingHourSalaryZaco'))
        $('EbClientWorkingHourSalaryZaco').addEvent('change', function(){
            var ar = this.getOptionText().split(' :: ');
            if (ar[1])
                $('EbClientWorkingHourSalaryPerHour').value = ar[1];
            else 
                $('EbClientWorkingHourSalaryPerHour').value = '0';
            recount_hours();
        });
    
    
    
	/*
	 * DblClick na jednotlive dny v mesici, respektive pouze na ty,
	 * ktere nejsou disabled. Dojde k DOM natahnuti elementu s ID dblclick_input a 
	 * naslednemu zobrazeni v domwinu. Pokud zvoli cas od do, tak se mu vlozi rozdil casu,
	 * pokud zvoli jinou moznost, vlozi se prislusny kod do inputu
	 */
	$('EbClientWorkingHour_edit_formular').getElements('.hodiny:enabled').addEvent('dblclick', function(){
		// call
		var obj = this;
		domwin.newWindow({
			id			: 'domwin_hodiny_od_do',
			sizes		: [300,170],
			scrollbars	: true, 
			title		: 'Vložte čas práce',
			languages	: false,
			type		: 'DOM',
			dom_id		: $('dblclick_input'),
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true,
			remove_scroll: false,
			dom_onRender: function(){
				var choose_select = $('domwin_hodiny_od_do').getElement('.choose_input');
				var choose_do_it  = $('domwin_hodiny_od_do').getElement('.choose_do_it');
				var choose_do_div = $('domwin_hodiny_od_do').getElement('.choose_do_div');
				var choose_od_div = $('domwin_hodiny_od_do').getElement('.choose_od_div');
				choose_select.addEvent('change', function(){
					if (this.value == 1){
						choose_do_div.removeClass('none');
						choose_od_div.removeClass('none');
					} else {
						choose_do_div.addClass('none');
						choose_od_div.addClass('none');
					}
				});
				
				choose_do_it.addEvent('click', function(e){
					new Event(e).stop();
					var choose_od = $('domwin_hodiny_od_do').getElement('.choose_od_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_od_m').value;
					var choose_do = $('domwin_hodiny_od_do').getElement('.choose_do_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_do_m').value;
					if (choose_select.value == 1)
						obj.value = compare_times(choose_od,choose_do);					
					else {
						obj.value = choose_select.value;
					}
					domwin.closeWindow('domwin_hodiny_od_do');
					// zavolani funkce pro prepocitani hodin a vyplaty
					recount_hours();
				});
			}
		});
	});
	
	/*
	 * Funkce na porovnani casu v ramci vice dnu
	 */
	function compare_times(time1, time2){
		var s1 = time1.split(':'), s2 = time2.split(':'), td;
		t1 = new Date(1970, 0, 0, s1[0] ? s1[0] : 0, s1[1] ? s1[1] : 0, s1[2] ? s1[2] : 0);
		t2 = new Date(1970, 0, ((s2[0] < s1[0]) || (s2[0]==s1[0] && s2[1] < s1[1]))?1:0, s2[0] ? s2[0] : 0, s2[1] ? s2[1] : 0, s2[2] ? s2[2] : 0);
		td = (t2 - t1)/60/1000;
		zbytek = (td % 60);
		zbytek = (1 / (60 / zbytek));
		output = (td / 60).toInt() + zbytek;
		return output;
	}
	
	/*
	 * Funkce pro prepocet hodin a vyplat, tzn. projde dny a spocita hodiny, pracovni volna, etc.
	 * Posleze dojde k prepoctu cena za hodinu * pocet odpracovanych hodin
	 */
	function recount_hours(){
		//var NH = $('EbClientWorkingHourStandardHours').value.toFloat();
		/*
		 * datovy typ pro pocitani poctu hodin, vikendu, neplacenych voln, etc.
		 */
		data = {'CelkemHodin' : 0,'Svatky' : 0,'Vikendy' : 0,'Dovolena' : 0,'NeplaceneVolno' : 0,'OcestrovaniClenaRodiny' : 0,'NeomluvenaAbsence' : 0,'PracovniNeschopnost' : 0,'NahradniVolno' : 0,'Paragraf' : 0}
		/*
		 * Prochazi jednotlive hodiny a zapisuje hodnoty do datoveho typu
		 */
		$$('.hodiny').each(function(item){
			switch(item.value){
				case 'PN':		data.PracovniNeschopnost ++;	break;
				case 'OČR':		data.OcestrovaniClenaRodiny ++;	break;
				case 'D':		data.Dovolena ++;				break;
				case 'NV':		data.NeplaceneVolno ++;			break;
				case 'A':		data.NeomluvenaAbsence ++;		break;
				case 'V':		data.NahradniVolno ++;			break;
				case 'PG':		data.Paragraf ++;				break;
				default:
					if (item.value == '') item.value = 0;
					data.CelkemHodin += item.value.toFloat();
					if (item.hasClass('vikend')) data.Vikendy 	+= item.value.toFloat();
					if (item.hasClass('svatky')) data.Svatky 	+= item.value.toFloat();
					break;
			}
		});
		/*
		 * Propise obsah datoveho typu do inputu
		 */
		$each(data, function(value,obj) {$('EbClientWorkingHour' + obj).value = value;});
		count_salary();
	}
	
	/*
	 * initializace funkce recound pro zmenu na inputech danych dnu
	 */
	$$('.hodiny').addEvent('blur', recount_hours);
	
	/*
	 * funkce na spocitani vyplat, rozpocitani jednotlivych polozek
	 * soucet celkoveho platu
	 */
	function count_salary(){

		/*
		 * mzda za hodiny
		 */
		var cena_za_hodinu 	= ($('EbClientWorkingHourSalaryPerHour').value=='')?'0':$('EbClientWorkingHourSalaryPerHour').value.toFloat();
		var celkem_hodin 	= ($('EbClientWorkingHourCelkemHodin').value=='')?'0':$('EbClientWorkingHourCelkemHodin').value.toFloat();
		var celkem_za_hodiny = $('EbClientWorkingHourSalaryPerHourP').value = cena_za_hodinu * celkem_hodin;
		
		/*
		 * celkova vyplata, tj. celkem za hodiny + celkem za ukoly - srazka
		 */
		var srazka = ($('EbClientWorkingHourDrawback').value=='')?'0':$('EbClientWorkingHourDrawback').value.toFloat();
        
        celkem_za_ukoly = 0;
        $each($$('.task_sum'),function(item){ celkem_za_ukoly += (item.value=='')?0:item.value.toFloat();})
        
		//var celkem_za_ukoly = ($('EbClientWorkingHourSalaryPerTask').value=='')?'0':$('EbClientWorkingHourSalaryPerTask').value.toFloat();
		$('EbClientWorkingHourMoneySum').value = celkem_za_hodiny + celkem_za_ukoly - srazka; 
		
	}
	/*
	 * initializace funkce count_salary na zmenu castky na hodinu a na zmenu srazky
	 */
	$('EbClientWorkingHourSalaryPerHour').addEvent('blur',count_salary);
	$('EbClientWorkingHourDrawback').addEvent('blur',count_salary);
	
	/*
	 * Pridani noveho radku do tabulky ukolovky
	 * funkce je initializovana primo na click event na tlacitko "Pridat ukolovku"
	 */
	$$('.add_task').addEvent('click', function(e){
		e.stop();
		
		var unique = uniqid();
        var rel = this.getProperty('rel').split('|');
		var conection_id = rel[0];
        var order_id = rel[1];
		var new_tr = new Element('tr').inject($('task_list'+conection_id));
        
        if(typeof (salary_item_list[order_id]) == 'undefined')
            return false;
        
        var s = new Element('select',{name:'data[connections]['+conection_id+'][ukols][' + unique + '][name]'})
            .inject(new Element('td').inject(new_tr))
            .addEvent('change', function(e){
                this.getParent('td').getNext('td').getElement('input').value = this.options[this.selectedIndex].getProperty('title');
                recount_task_row(e);
            })
        new Element('option',{title:'0', html:'', value:'0',}).inject(s);
            
        $each(salary_item_list[order_id],function(item){
          new Element('option',{title:item.pay_max, html:item.name, value:item.id,}).inject(s);  
        })
		
		new Element('input',{type:'text',name:'data[connections]['+conection_id+'][ukols][' + unique + '][cena_ks]'}).addEvent('blur',recount_task_row).inject(new Element('td').inject(new_tr));
		new Element('input',{type:'text',name:'data[connections]['+conection_id+'][ukols][' + unique + '][ks]'}).addEvent('blur',recount_task_row).inject(new Element('td').inject(new_tr));
		new Element('input',{type:'text',name:'data[connections]['+conection_id+'][ukols][' + unique + '][cena]','class':'cena_row',readonly:'readonly'}).addClass('read_info').inject(new Element('td').inject(new_tr));
		new Element('a',{href:'#','class':'ta delete',html:'Odstranit'}).addEvent('click', delete_ukolovka_row.bindWithEvent()).inject(new Element('td').inject(new_tr));
		
		recount_task_salary(conection_id);
	});
    
    /*
     * Funkce pro odstraneni radku ukolovky po kliknuti na smazat
     */
    function delete_ukolovka_row(e){
        e.stop(); 
        if(confirm('Opravdu si přejete odstranit tento úkol?')){
            i = e.target.getParent('tbody').getProperty('id').replace(/task_list/,"");
            e.target.getParent('tr').dispose();
            recount_task_salary(i);
        }
    }
	
    /*
     * initializace funkce smazani radku ukolovky pro nactene radky z DB
     */
     $$('.delete_ukolovka_row').addEvent('click',delete_ukolovka_row.bindWithEvent()); 
    
	/*
	 * funkce pro vypocet celkove ceny za ukol, aplikovana bude na inputy ks a ceny za ks na udalost onblur
	 * nasledne zavolana funkce na prepocet platu za ukolovky (recount_task_salary)
	 */
	function recount_task_row(e){
        e.stop();
		var inputs = e.target.getParent('tr').getElements('input[type=text]');
		inputs[2].value = ((inputs[0].value == '')?0:inputs[0].value.toFloat()) * ((inputs[1].value == '')?0:inputs[1].value.toFloat());
        var i = e.target.getParent('tbody').getProperty('id');
		recount_task_salary(i.replace(/task_list/,""));
	}
	
	/*
	 * Funkce pro prepocet ceny za ukoly, na konci zavola funkci pro prepocet celkove ceny 
	 */
	function recount_task_salary(connection_id){
       if(connection_id == null){
         alert('Chyba v přepočtech, špatně nastavené ID connectio');
         return false;
       }
       
		var task_cena = 0;
		$('task_list'+connection_id).getElements('.cena_row').each(function(input){task_cena += (input.value == '')?0:input.value.toFloat()});
		$('Connections'+connection_id+'EbClientWorkingHourSalaryPerTask').value = task_cena;
		count_salary();
	}
	
	/*
	 * Udalost na funkci ulozit
	 */
    if($('only_save')) 
	$('only_save').addEvent('click', function(e){
		new Event(e).stop();
		
        if($('EbClientWorkingHourSalaryZaco').value != ''){
            button_preloader($('only_save'));
    		new Request.JSON({
    			url: $('EbClientWorkingHour_edit_formular').getProperty('action'),
    			onComplete: function(json){
    				if (json){
    					if (json.result === true){
    						click_refresh($('EbClientWorkingHourClientId').value);
    						alert(json.message);
    					}
    				} else {
    					alert('Chyba aplikace!');
    				}
                    
                    button_preloader_disable($('only_save'));
    			}
    		}).send($('EbClientWorkingHour_edit_formular'));
        }
        else{
            alert('Musíte vyplnit typ práce!');
        }
	});
    
    
    /*
	 * Udalost na funkci ulozit
	 */
    if($('uzavrit')) 
	$('uzavrit').addEvent('click', function(e){
		new Event(e).stop();
        if(confirm('Opravdu chcete uzavřít docházku tohoto klienta, za tento měsíc?!')){
    		button_preloader($('uzavrit'));
    		new Request.JSON({
    			url: '/eb_employees/uzavrit_odpracovane_hodiny/'+$('EbClientWorkingHourClientId').value+'/'+$('EbClientWorkingHourMesic').value+'/'+$('EbClientWorkingHourRok').value+'/',
    			onComplete: function(json){
    				if (json){
    					if (json.result === true){
    						click_refresh($('EbClientWorkingHourClientId').value);
    						domwin.closeWindow('domwin_zamestnanci_hodiny');
    					}
                        else {
        					alert(son.message);
        				}
    				} else {
    					alert('Chyba aplikace!');
    				}
                    
                    button_preloader_disable($('uzavrit'));
    			}
    		}).send();
        }
	});

    /*
     * zavreni domwinu bez ulozeni po kliknuti na tlacitko zavrit
     */
    $('only_close').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_zamestnanci_hodiny');
	});


     /**
     * Funkce rozhoduje zda ma povoleni
     * mohou pozue admini, a nebo lide kteri jsou stavbyvdouci
     */
    function test_is_protected(connection_id){
        //podminka pro stavbyvedouci - zrusena
        // (typeof inspector_list[connection_id] != 'undefined'  && inspector_list[connection_id] == logged_user)
        if(logged_group_superior == 1 || logged_group_superior == 5)
            return true;
        else 
            return false;    
    }

    /**
     * Prochazeni tabu a vsechny inputy nastavit na readonly a schovat mazajici tl u ukolu a pridani
     */
    function make_readonly_tab(tab){
        tab.getElement('table.odpracovane_hodiny').getElements('input').setProperty('readonly','readonly').addClass('read_info').removeEvents('dblclick');
        tab.getElement('.add_task').dispose();
 
        tab.getElement('tbody.ukony').getElements('input').setProperty('readonly','readonly').addClass('read_info').removeEvents('click');
        $each(tab.getElement('tbody.ukony').getElements('select'),function(sel){
            sel.setProperty('readonly','readonly').addClass('read_info');
            sel.getElements('option').setProperty('disabled','disabled'); 
        })
        tab.getElement('tbody.ukony').getElements('a.delete').dispose();
    }
    
    /**
     * Prochazeni TABU a kontrola zda ma moznost editovat, 
     * pokud ne, vse v danem tabu, nastavit na readonly a zakazat smazani ukonu!
     */
    $each($$('.domtabs .field'),function(tab){
        var connection = tab.getProperty('rel');
        
        if(test_is_protected(connection) == false){
            make_readonly_tab(tab);
        }
        
    });



</script>