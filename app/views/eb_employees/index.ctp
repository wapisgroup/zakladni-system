<div>
	<?php //echo $htmlExt->hidden('connection_client_order_id',array('value' => $connection_id));?>
	<div class="sll3" >  
		<?php echo $htmlExt->var_text('Client/jmeno',array('label'=>'Jméno','class'=>'read_info'));?> <br/>
		<?php echo $htmlExt->var_text('Client/prijmeni',array('label'=>'Příjmení','class'=>'read_info'));?> <br/>
	</div>
	<div class='sll3'>
		<?php echo $htmlExt->selectTag('month2',$mesice_list2,$mesic,array('label'=>'Měsíc', 'class'=>'odpracovane_hodiny_change_date'));?> <br/>
		<?php echo $htmlExt->selectTag('year2', $actual_years_list,$rok,array('label'=>'Rok', 'class'=>'odpracovane_hodiny_change_date'),null,true);?>
	</div>
</div>
<br />
<div id="odpracovane_hodiny_element">
	<?php echo $this->renderElement('../eb_employees/element',array('month'=>$mesic, 'year' =>$rok));?>
</div> 
<script language="javascript" type="text/javascript">

	// init event for onChange year and month
	$$('.odpracovane_hodiny_change_date').addEvents({
		'change': function(e){
			this.dont_change_focus = true;
			$('odpracovane_hodiny_element').fade(0);
			if (!$('made_change_in_form') || $('made_change_in_form').value == 0 || confirm('Byly provedeny změny, po provedeni tohoto kroku nebudou změněné udaje uloženy. Pokud je chcete uložit, klikněte nejprve na tlačitko Uložit. Chcete pokračovat?')){
				new Request.HTML({
					url: '/eb_employees/odpracovane_hodiny/' + $('ClientWorkingHourClientId').value + '/' + $('Year2').value + '/' + $('Month2').value + '/true',
					update: $('odpracovane_hodiny_element'),
					onComplete: function(){
						$('odpracovane_hodiny_element').fade(1);
					}
				}).send();
			} else {
				new Event(e).stop();
				this.options[this.currentSelection].selected = 'selected';
				$('odpracovane_hodiny_element').fade(1);
			}
			this.dont_change_focus = false;
		},
		'focus': function(){
			if (!this.dont_change_focus || this.dont_change_focus == false){
				this.currentSelection = this.selectedIndex;
			}
		}
	});

</script>