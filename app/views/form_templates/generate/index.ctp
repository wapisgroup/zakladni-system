﻿<form id='add_edit_client_formular' action='/form_templates/save_client_form/' method='post'>
<?php 
echo $htmlExt->hidden('FormData/id');
echo $htmlExt->hidden('FormData/name');
echo $htmlExt->hidden('FormData/client_id');
echo $htmlExt->hidden('FormData/html_code');


echo '<div id="html_code">';
//pr($data);
echo $data;

echo '</div>';
?>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditTodoSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditTodoClose'/>
	</div>
</form>
<script>

	$('AddEditTodoClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_forms');
	});

	$('AddEditTodoSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();


		// value transfer to html value
		$('html_code').getElements('input[type=text]').each(function(item){
			item.setAttribute('value',item.value);
		});
		$('html_code').getElements('input[type=checkbox]').each(function(item){
			if (item.checked){
				item.setAttribute('checked','checked');
			} else {
				item.removeAttribute('checked');
			}
		});
		$('html_code').getElements('select').each(function(item){
			item.getElements('option').each(function(op){
				if (item.value == op.value)
					op.setAttribute('selected','selected');
				else
					op.removeAttribute('selected');
			});
		});
	

		$('FormDataHtmlCode').value = $('html_code').getHTML();
		// end transfer

		new Request.JSON({
			url:$('add_edit_client_formular').action,
			onComplete:function(json){
				//click_refresh($('TodoId').value);
				if(json.result == true){
					tr = new Element('tr').inject($('form_list'));
					new Element('td').inject(tr).setHTML($('FormDataName').value);
					new Element('td').inject(tr).setHTML('<?php echo date('d.m.Y');?>');
					var posibility = new Element('td').inject(tr);
					new Element('a', {'href':'/form_templates/edit_client_form/' + json.id,'html':'Editovat'}).inject(posibility);
					new Element ('span').inject(posibility).setHTML(' | ');
					new Element('a', {'href':'/form_templates/delete_client_form/' + json.id,'html':'Smazat'}).inject(posibility);
					new Element ('span').inject(posibility).setHTML(' | ');
					new Element('a', {'href':'/form_templates/print_client_form/' + json.id,'target':'_blank','html':'Tisknout'}).inject(posibility);

					if($('NoneSearch')!=null) 
						$('NoneSearch').addClass('none');
					
					domwin.closeWindow('domwin_forms');
					alert(json.message);
				}
				else 
					alert(json.message);

			}
		}).post($('add_edit_client_formular'));

	});

</script>