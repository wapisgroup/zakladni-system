<form id='formular_add' action='/client_estors/add_worker/'>
    <?= $htmlExt->hidden('ConnectionEbcomClient/client_id',array('value'=>$client_id));?>
    <fieldset>
        <legend>Informace o nástupu</legend>
        <?= $htmlExt->selectTag('ConnectionEbcomClient/eb_company_id',$eb_company_list,array(),array('label'=>'Zakazka'));?><br />
        <?= $htmlExt->inputDate('ConnectionEbcomClient/start',array('label'=>'Datum nástupu'));?><br />
        <div class="win_save">
            <input type="submit" value='Uložit' id='win_save' onclick="return false;" />
            <input type="button" value='Zrušit' id='win_close' onclick="return false;"/>
        </div>
    </fieldset>
</form>
<script type="text/javascript">
    function goto(){
        domtab.goTo(5);
    }
    $('win_save').addEvent('click', function(e){
        e.stop();
        if ($('calbut_ConnectionEbcomClientStart').value == ''){
            alert('Musíte vyplnit datum nástupu');
        } else {
            new Request.JSON({
                url: $('formular_add').getProperty('action'),
                onComplete: function(json){
                    if (json && json.result == true){
                        alert('Zaměstnanec byl přiřazen.');
                        domwin.closeWindow('domwin_worker_add');
                        domwin.loadContent('domwin', {goto: 9});
                    } else {
                        alert('Nastala chyba během ukladání, opakujte akci prosím později');
                    }
                }
            }).send($('formular_add'));
        }
    })

    $('win_close').addEvent('click', function(e){
        e.stop();
        domwin.closeWindow('domwin_worker_add');
    })
</script>