<form action="/opp_items/edit" method="post" id='form_opp'>
	<?php echo $htmlExt->hidden('OppItem/id');?>
	<fieldset>
		<legend>Popis OOP</legend>
		<div class="sll">
			<?php echo $htmlExt->selectTag('OppItem/type',$type_list,null,array('label'=>'Druh'),null,false);?><br/>
			<?php echo $htmlExt->input('OppItem/code',array('label'=>'Kód'));?><br/>
            <?php echo $htmlExt->input('OppItem/size',array('label'=>'Velikost'));?><br/>
		</div>
		<div class="slr">
			<?php echo $htmlExt->input('OppItem/name',array('label'=>'Název'));?><br/>
			<?php echo $htmlExt->input('OppItem/count',array('label'=>'Počet','class'=>'integer'));?><br/>
		</div>
		<br />
	</fieldset>
	<fieldset>
		<legend>Nastavení cen</legend>
		<div class="sll">
			<?php echo $htmlExt->input('OppItem/price_cz',array('label'=>'Cena CZK', 'class'=>'float'));?><br/>
			<?php echo $htmlExt->input('OppItem/price_euro',array('label'=>'Cena EURO', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
		</div>
		<div class="slr">
			<?php echo $htmlExt->input('OppItem/rate',array('label'=>'Kurz', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
		</div>
		<br />
	</fieldset>
	<div class="win_save">
		<input type="button" value="Uložit" id='save_form'  class="button"/>
		<input type="button" value="Zavřít" id='close_form' class="button"/>
	</div>
</form>
<script>
	$$('.float, .integer').inputLimit();

	$('save_form').addEvent('click', function(e){
		e.stop();
        valid_result = validation.valideForm('form_opp');
		if (valid_result == true){
    		new Request.JSON({
    			url: $('form_opp').getProperty('action'),
    			onComplete: function(json){
    				if (json){
    					if (json.result === true){
    						click_refresh($('OppItemId').value);
    						domwin.closeWindow('domwin');
    					} else {
    						alert(json.message);
    					}
    				} else {
    					alert('Chyba aplikace');
    				}
    			}
    		}).send($('form_opp'));
        } else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	})
    
    validation.define('form_opp',{
		'OppItemCode': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit kód'}
		},
        'OppItemName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		},
        'OppItemCount': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit počet'}
		},
        'OppItemSize': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit velikost'}
		},
        'OppItemPriceCz': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit cenu'}
		}
	});
	validation.generate('form_opp',<?php echo (isset($this->data['OppItem']['id']))?'true':'false';?>);

	
	$('close_form').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin');
	})
	
	$('OppItemPriceCz').addEvent('change', function(){
		if(this.value == '') this.value = 0;
		var kurz = $('OppItemRate').value, euro = $('OppItemPriceEuro');
		euro.value = Math.round(this.value / kurz * 100) / 100;
		
	}).focus();
</script>