<div class="win_fce top">
<a href='/setting_stats/pocet_dni_edit/<?php echo $stat_id;?>' class='button edit' id='company_attach_add_new' title='Přidání'>Přidat</a>
</div>
<table class='table' id='table_attachs'>
	<tr>
		<th>Rok</th>
		<th>Měsíc</th>
		<th>Počet</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($pocet_dni_list) && count($pocet_dni_list) >0):?>
	<?php foreach($pocet_dni_list as $item):?>
	<tr>
		<td><?php echo $item['SettingStatPd']['rok'];?></td>
		<td><?php echo $item['SettingStatPd']['mesic'];?></td>
		<td><?php echo $item['SettingStatPd']['pocet'];?></td>
		<td><?php echo $fastest->czechDateTime($item['SettingStatPd']['created']);?></td>
		<td>
			<a title='Editace' 	class='ta edit' href='/setting_stats/pocet_dni_edit/<?php echo $stat_id;?>/<?php echo $item['SettingStatPd']['id'];?>'/>edit</a>
			<a title='Odstranit'class='ta trash' href='/setting_stats/pocet_dni_trash/<?php echo $stat_id;?>/<?php echo $item['SettingStatPd']['id'];?>'/>trash</a>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K tomuto státu nebyly nadefinované žádne počty dnů v měsíci</td>
	</tr>
	<?php endif;?>
</table>
<?php  //pr($pocet_dni_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListClientAttachmentClose' class='button'/>
	</div>
<script>
	$('ListClientAttachmentClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_svatky');});

	$('table_attachs').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tento záznam?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_svatky').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_svatky').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_attach_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>