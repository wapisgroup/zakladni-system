<form action='/setting_stats/pocet_dni_edit/' method='post' id='client_attachs_edit_formular'>
	<?php echo $htmlExt->hidden('SettingStatPd/id');?>
	<?php echo $htmlExt->hidden('SettingStatPd/setting_stat_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->selectTag('SettingStatPd/rok', array('2008'=>'2008','2009'=>'2009','2010'=>'2010','2011'=>'2011','2012'=>'2012','2013'=>'2013'),null,array('label'=>'Rok','class'=>'half'),null,true);?>
				<?php echo $htmlExt->selectTag('SettingStatPd/mesic',$mesice_list,null,array('label'=>'Měsíc','class'=>'half'),null,true);?>
				<?php echo $htmlExt->input('SettingStatPd/pocet',array('tabindex'=>1,'label'=>'Počet dní','class'=>'long','label_class'=>'long'));?> <br class="clear">

			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientAttachmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientAttachmentClose'/>
	</div>
</form>
<script>
	
	$('AddEditClientAttachmentClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach_add');});
	
	$('AddEditClientAttachmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();

			new Request.HTML({
				url:$('client_attachs_edit_formular').action,		
				update: $('domwin_svatky').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_attach_add');
				}
			}).post($('client_attachs_edit_formular'));

	});
	
</script>