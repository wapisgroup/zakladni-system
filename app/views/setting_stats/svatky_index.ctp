<form method='post' id='calendar_formular'>
	<div>
		<?php echo $htmlExt->hidden('SettingStatSvatek/setting_stat_id');?>
		<?php echo $htmlExt->selectTag('SettingStatSvatek/year', array('2008'=>'2008','2009'=>'2009','2010'=>'2010','2011'=>'2011','2012'=>'2012','2013'=>'2013'),null,array('class'=>'half'),null,true);?>
		<?php echo $htmlExt->selectTag('SettingStatSvatek/month', array(	1	=> 'Leden', 2	=> 'Únor', 3	=> 'Březen', 4	=> 'Duben', 5	=> 'Květen', 6	=> 'Červen', 7	=> 'Červenec', 8	=> 'Srpen', 9	=> 'Zaří', 10	=>'Říjen', 11	=>'Listopad', 12	=>'Prosinec'),null,array('class'=>'half'));?> <br/>
		<?php 
			$thismonth = ( int ) date( "m" );
		    $thisyear = ( int ) date( "Y" );
		?>
		<br />
		<em>Zvolte si prosím státní svátky. Státní svátek zvolíte zaškrtnutím daného políčka u daného datumu.</em>
		<div id='calendar_div'><span class="info">Zvolte platné datum</span><?php // echo $this->renderElement('../setting_stats/svatky',array('thismonth'=>$thismonth,'thisyear'=>$thisyear)); ?></div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
<script>
$('close').addEvent('click', function(e){
	new Event(e).stop();
	domwin.closeWindow('domwin_svatky');
});

$('save_close').addEvent('click', function(e){
	new Event(e).stop();
	new Request.JSON({
		url: '/setting_stats/svatky_save/',
		onComplete: function(json){
			if (json.result == true)
				alert('Data byla uložena');
			else
				alert(json.message);
		}
	}).post($('calendar_formular'))
});

$('SettingStatSvatekYear').addEvent('change', function(){
	new Request.HTML({
		url: '/setting_stats/svatky_refresh/' + $('SettingStatSvatekSettingStatId').value + '/' + $('SettingStatSvatekYear').value +'/' + $('SettingStatSvatekMonth').value,
		update:'calendar_div'
	}).send();
});

$('SettingStatSvatekMonth').addEvent('change', function(){
	new Request.HTML({
		url: '/setting_stats/svatky_refresh/' + $('SettingStatSvatekSettingStatId').value + '/' + $('SettingStatSvatekYear').value +'/' + $('SettingStatSvatekMonth').value,
		update:'calendar_div'
	}).send();
});

</script>