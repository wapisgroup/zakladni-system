<?php

	$start 	= $this->data['ConnectionClientRequirement']['from'];
	$end 	= ($this->data['ConnectionClientRequirement']['to']== '0000-00-00')? /*date("Y-m-d")*/ '2014-03-18' :$this->data['ConnectionClientRequirement']['to'];
	
	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );
	
	// render list of disabled fields
	$not_to_disabled = array();
	for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
		$curr_date = $year.'-'.$month.'-'.((strlen($i) == 1)?'0'.$i:$i);
		if (strtotime($start) <= strtotime($curr_date) && strtotime($end) >= strtotime($curr_date))
			$not_to_disabled[] = $i;
	}
    if($_SERVER['REMOTE_ADDR'] == '46.151.56.229'){
        //var_dump($not_to_disabled);
    }
?>
<input type='hidden' value='0' id='made_change_in_form' />
<form action='/employees/odpracovane_hodiny/' method='post' id='ClientWorkingHour_edit_formular'>
	<!-- Basic Hidden Fields -->
	
	<?php echo $htmlExt->hidden('ClientWorkingHour/id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/company_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/client_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/fakturace');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/requirements_for_recruitment_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/connection_client_requirement_id');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/year',array('value'=>$year));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/month',array('value'=>$month));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/typ_cena_za_ubytovani');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/max_hodin_v_mesici');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/change_max_salary');?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/working_days_count');?>
	<?php echo $htmlExt->hidden('Dovolena/zbyva_dovolene',array('value'=>$dovolena['zbyva_dovolene']));?>
    <?php echo $htmlExt->hidden('cena_ubytovani_na_mesic',array('value'=>(isset($cena_ubytovani_na_mesic)?$cena_ubytovani_na_mesic:0)));?>
    <?php echo $htmlExt->hidden('pocet_dnu_v_mesici',array('value'=>(isset($pocet_dnu_v_mesici)?$pocet_dnu_v_mesici:0)));?>
	<?php echo $htmlExt->hidden('ClientWorkingHour/stav');?>
    <?php echo $htmlExt->hidden('money_item_name');?>
	<?php echo $htmlExt->hidden('transfer_money_item_id',array('value'=>0));?>
	<?php echo $htmlExt->hidden('stop_payment_now',array('value'=>0));?>
	<?php echo $htmlExt->hidden('change_max_salary_now',array('value'=>0));?>

    <!-- Up zobrazeni pod elementem -->
	<div class='sll3'>
		<?php echo $htmlExt->selectTag('ClientWorkingHour/company_work_position_id',$company_work_position_list,null,array('label'=>'Profese','disabled'=>'disabled','class'=> 'read_info'));?><br/>
		<?php 
			$text =  $max_salary_hours['cena_ubytovani_na_mesic']<>0 ? 'Ano' : 'Ne';
			echo $htmlExt->var_text('varUbytovani', array('label'=>'Využívá ubytování','class'=>'read_info','value'=>$text));?><br />	
	   	    <?php
					$accommodation_list = array(0=>'Nezvoleno') + $accommodation_list;

					if($ubytovani_select)
						echo $htmlExt->selectTag('ClientWorkingHour/accommodation_id',$accommodation_list,null,array('label'=>'Ubytování'),array('title'=>(array('0|0') + $accommodation_title_list)),false);
					else 
						echo $htmlExt->hidden('ClientWorkingHour/accommodation_id').'<br />';
		?><br/>
    </div>
	<div class="sll3" >  
		<?php echo $htmlExt->selectTag('ClientWorkingHour/company_money_item_id',$money_item_list,null,array('label'=>'Forma odměny','disabled'=>'disabled','class'=> 'read_info'),array('title'=>$money_item_title));?><br/>
		<?php 
			$text =  $max_salary_hours['doprava']<>0 ? 'Ano' : 'Ne';
			echo $htmlExt->var_text('varDoprava', array('label'=>'Využívá dopravu','class'=>'read_info','value'=>$text)); 
			echo $htmlExt->input('ClientWorkingHour/zbyva_dovolene',array('readonly'=>'readonly','label'=>'Zbývá dovolené','class'=>'read_info','value'=>$zbyva_dovolene)); ?>
    </div>
	<div class="sll3" >  
	<?php echo $htmlExt->input('ClientWorkingHour/max_salary',array('readonly'=>'readonly','label'=>'Max. mzda','class'=>'read_info'));?>
	<?php 
		$text =  $max_salary_hours['stravenka']<>0 ? 'Ano' : 'Ne';
		echo $htmlExt->var_text('varStravenka', array('label'=>'Využívá stravenky','class'=>'read_info','value'=>$text)).'<br />';
        echo $htmlExt->selectTag('ClientWorkingHour/company_cw_template_id',$company_cw_template_list,null,array('label'=>'Šablona'),null,false).'<br />';
        echo $htmlExt->var_text('Client/st_centre',array('label'=>'Středisko','class'=>'read_info'));
	
  ?><br />
		
	</div> <br /><br />
<!-- END -->

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
            <li class="ousko"><a href="#krok1">Docházka</a></li>
			<li class="ousko"><a href="#krok2">Nastavení</a></li>
			<li class="ousko"><a href="#krok3">Provozní marže</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field" >
			<table class='table odpracovane_hodiny'>	
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
				<?php 
					$smena_captions = array(
						1	=> 'Ranní',
						2	=> 'Odpolední',
						3	=> 'Noční'
					);
				?>
				<?php for($k = 1; $k <= 3; $k++):?>
				<tr class='tr_smennost_<?php echo $k;?> <?php 
						if (($k == 2 && $this->data['ClientWorkingHour']['setting_shift_working_id'] == 1) || 
							($k == 3 && in_array($this->data['ClientWorkingHour']['setting_shift_working_id'],array(1,2)))
							|| (empty($this->data['ClientWorkingHour']['setting_shift_working_id']))
							) { echo 'none'; }?>'>	
					<td><?php echo $smena_captions[$k];?></td>
					<?php $current_day = $last_day;?>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
						<?php 
							if (in_array(($i+1),$svatky)){ 
								$class =  " svatky";
							} else if (in_array($current_day,array(6,7))){
								$class = " vikend";
							} else {
								$class = "";
							}
						?>
						<?php echo $htmlExt->input('ClientWorkingHour/days/'. '_' .$k. '_' .($i+1),array('class'=>'hodiny integer s'.$k.' '.$class,'disabled'=>(!in_array(($i+1),$not_to_disabled) || (isset($this->data['ClientWorkingHour']['days']['_' .$k. '_' .($i+1)]) && $this->data['ClientWorkingHour']['days']['_' .$k. '_' .($i+1)] == 'A') && !in_array($logged_group,array(1)) )?'disabled':null));?></td>
						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
					<?php endfor;?>
				</tr>
				<?php endfor;?>
				<tr class='tr_food_ticket <?php if (!isset($this->data['ClientWorkingHour']['stravenka']) || $this->data['ClientWorkingHour']['stravenka'] == 0) echo 'none';?>'>
					<td>Stravenky - <a href="#" id="food_days_all">vše</a></td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('ClientWorkingHour/food_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'food_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
				<tr class='tr_accommodation <?php if (!isset($this->data['ClientWorkingHour']['accommodation_id']) || $this->data['ClientWorkingHour']['accommodation_id'] == 0) echo 'none';?>'>
					<td>Ubytování <a href="#" id="accommodation_days_all">vše</a></td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('ClientWorkingHour/accommodation_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'accommodation_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
			</table>
			<br />
            <?php
                if($odmeny_typ == 3){
                    echo "<div class='text_center color_red'>Pracujete se mzdou která je zadávané v hrubém!</div>";
                }
            ?>
			<fieldset>
				<legend>Zadání mzdy pro aktuální měsíc</legend>
				<div class='slr3'>
					<label></label><strong style='margin-left:3px'>Informace o docházce</strong><br />
					<?php echo $htmlExt->input('ClientWorkingHour/celkem_hodin',array('readonly'=>'readonly','label'=>'Celkem hodin','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/svatky',array('readonly'=>'readonly','label'=>'Svátky','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/prescasy',array('readonly'=>'readonly','label'=>'Přesčasy','class'=>'read_info'));?>				
					<?php echo $htmlExt->input('ClientWorkingHour/vikendy',array('readonly'=>'readonly','label'=>'Víkendy','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/dovolena',array('readonly'=>'readonly','label'=>'Dovolenka','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/neplacene_volno',array('readonly'=>'readonly','label'=>'NV','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/ocestrovani_clena_rodiny',array('readonly'=>'readonly','label'=>'OČR','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/neomluvena_absence',array('readonly'=>'readonly','label'=>'A','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/pracovni_neschopnost',array('readonly'=>'readonly','label'=>'PN','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/nahradni_volno',array('readonly'=>'readonly','label'=>'V','class'=>'read_info'));?>
					<?php echo $htmlExt->input('ClientWorkingHour/paragraf',array('readonly'=>'readonly','label'=>'PG','class'=>'read_info'));?>
					<?php for($k = 1; $k <= 3; $k++):?>
					<?php echo $htmlExt->input('ClientWorkingHour/hour_s'.$k,array('readonly'=>'readonly','label'=>'S'.$k,'class'=>'read_info '.((($k == 2 && $this->data['ClientWorkingHour']['setting_shift_working_id'] == 1) || ($k == 3 && in_array($this->data['ClientWorkingHour']['setting_shift_working_id'],array(1,2))))?'none':''),'label_class'=>((($k == 2 && $this->data['ClientWorkingHour']['setting_shift_working_id'] == 1) || ($k == 3 && in_array($this->data['ClientWorkingHour']['setting_shift_working_id'],array(1,2))))?'none':'')));?>
					<?php endfor;?>
                       <?php if(!$sub_salary_disabled[1]){?>
                            <?php 	echo $htmlExt->var_text('varPrispevekStravne', array('label'=>'Příspěvek na stravné','class'=>'read_info','value'=>$prispevky_z_kalkulace['stravne']));?><br />
                            <?php 	echo $htmlExt->var_text('varPrispevekCestovne', array('label'=>'Příspěvek na cestovné','class'=>'read_info','value'=>$prispevky_z_kalkulace['cestovne']));?><br />
                        <?php }?>
				</div>
				
				<div class='sll3'>
					<label></label><strong style='margin-left:3px'>Hodinové sazby</strong><br />
<!-- Nové zobrazneni maximalek -->
<div class="odmeny_typ_1">
					<div class='<?php echo ($sub_salary_disabled[1])?"none":"";?>'><?php echo $htmlExt->input('ClientWorkingHour/salary_part_1',array('label'=>'Na hodinu PS','class'=>'short float salary','disabled'=>$sub_salary_disabled[1]))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHour/salary_part_1_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled'))."<br />";?></div>
					<div class='<?php echo ($sub_salary_disabled[2])?"none":"";?>'><?php echo $htmlExt->input('ClientWorkingHour/salary_part_2',array('label'=>'Na hodinu D','class'=>'short float salary','disabled'=>$sub_salary_disabled[2]))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHour/salary_part_2_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled'))."<br />";?></div>
					<div class='<?php echo ($sub_salary_disabled[3])?"none":"";?>'><?php echo $htmlExt->input('ClientWorkingHour/salary_part_3',array('label'=>'Na hodinu ŽL','class'=>'short float salary','disabled'=>$sub_salary_disabled[3]))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHour/salary_part_3_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled'))."<br />";?></div>
					<div class='<?php echo ($sub_salary_disabled[4])?"none":"";?>'><?php echo $htmlExt->input('ClientWorkingHour/salary_part_4',array('label'=>'Na hodinu C','class'=>'short float salary','disabled'=>$sub_salary_disabled[4]))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHour/salary_part_4_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled'))."<br />";?></div>
				

					<?php echo $htmlExt->input('ClientWorkingHour/salary_per_hour',array('readonly'=>'readonly','label'=>'Na hodinu celkem','class'=>'short read_info for_null_past_money_item_change'));?><br />
 
                    <?php if($stat_id == 1){ ?>
                    <div id="dpc_box">
                        <div class="sll">
                        	<?php echo $htmlExt->input('ClientWorkingHour/fa_dpc',array('readonly'=>'readonly','label'=>'DPČ','class'=>'short read_info'));?><br />
                        </div>
                        <div class="slr">
                            <?php echo $htmlExt->input('ClientWorkingHour/fa_dpc_calculation',array('readonly'=>'readonly','label'=>'výpočet','class'=>'short read_info'));?>
                            <?php if(in_array($logged_group,array(1,5))){ ?>
                            <span id="edit_dpc_value">[*]</span>
                            <span id='reset_dpc' style='cursor:pointer' title="resetovat DPČ">R</span>
                            <?php }?>
                            <br />
                        </div>
                        <br />
                    </div>
                    <?php } ?>
 </div>
					<label>Pozdržet výplatu:</label><?php echo $htmlExt->checkbox('ClientWorkingHour/stop_payment');?><br/>
					<?php echo $htmlExt->input('ClientWorkingHour/drawback',array('label'=>'Srážka','class'=>'short float for_null_past_money_item_change'));?><br />
                    <?php echo $htmlExt->input('ClientWorkingHour/drawback_opp',array('label'=>'Srážka za OPP','class'=>'short float','value'=>(isset($this->data['ClientWorkingHour']['drawback_opp']) && $this->data['ClientWorkingHour']['drawback_opp'] != ''? $this->data['ClientWorkingHour']['drawback_opp']:(isset($opp_sum) ? $opp_sum : 0))));
                          echo $htmlExt->checkbox('ClientWorkingHour/drawback_opp_add');?><br />
                     <?php 
                    if(isset($cena_ubytovani_na_mesic)){
                        echo $htmlExt->input('ClientWorkingHour/drawback_accommodation',array('readonly'=>'readonly','label'=>'Srážka za ubytování','class'=>'short read_info'));
                        echo $htmlExt->checkbox('ClientWorkingHour/drawback_accommodation_add');
                    }?><br class="clear_left" /> 
                     <div class="odmeny_typ_2 none">
                     	<?php echo $htmlExt->input('ClientWorkingHour/ps_pausal_hmm',array('label'=>'Hrubá mzda','readonly'=>'readonly','class'=>'read_info short float for_null_past_money_item_change'));?><br />
                     </div>
                     <div class="odmeny_typ_3 none">
                     	<?php echo $htmlExt->input('ClientWorkingHour/ps_hodina_hmm',array('label'=>'Základní hodinová mzda','readonly'=>'readonly','class'=>'read_info short float for_null_past_money_item_change'));?><br />
                     </div>
                     
					<?php echo $htmlExt->input('ClientWorkingHour/odmena_1',array('label'=>$odmeny[1]['label'],'class'=>'short float salary'))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHour/odmena_1_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled','value'=>($this->data['ClientWorkingHour']['odmena_1_max'] != 0)?$this->data['ClientWorkingHour']['odmena_1_max']:$odmeny[1]['max']))."<br />";?>
					<?php echo $htmlExt->input('ClientWorkingHour/odmena_2',array('label'=>$odmeny[2]['label'],'class'=>'short float salary','disabled'=>($odmeny[2]['max'] > 0 ? '' : 'disabled')))."<label class='label_short'>/</label>".$htmlExt->input('ClientWorkingHour/odmena_2_max',array('class'=>'short float for_null_past_money_item_change maximalky','disabled'=>'disabled','value'=>($this->data['ClientWorkingHour']['odmena_2_max'] != 0)?$this->data['ClientWorkingHour']['odmena_2_max']:$odmeny[2]['max']))."<br />";?>
					
				</div>
				<div class='sll3'>
					<label></label><strong style='margin-left:3px'>Mzda celkem</strong><br />
                    <div class="odmeny_typ_1">
    					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_1_p',array('label'=>'Pracovní smlouva','readonly'=>'readonly','class'=>'read_info'));?> <br/>
    					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_2_p',array('label'=>'Dohoda','readonly'=>'readonly','class'=>'read_info'));?><br/>
    					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_3_p',array('label'=>'ŽL','readonly'=>'readonly','class'=>'read_info'));?><br/>
    					<?php echo $htmlExt->input('ClientWorkingHour/salary_part_4_p',array('label'=>'Abc','readonly'=>'readonly','class'=>'read_info'));?><br/>
    					<?php echo $htmlExt->input('ClientWorkingHour/salary_per_hour_p',array('label'=>'Celkem mzda', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
    					<?php echo $htmlExt->input('change_max_zalohy_now',array('label'=>'Zbývá k výplatě', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
				    </div>
                    <div id="transfer_dohoda_to_cash_box" class="<?= ($allow_transfer_dohoda_to_cash == true ?'':'none');?>">
                        <?php echo $htmlExt->input('ClientWorkingHour/transfer_dohoda_to_cash_value',array('label'=>'Přenést částku z dohody do abc','class'=>'short float'));?>
                    
                        <?php echo $htmlExt->checkbox('ClientWorkingHour/transfer_dohoda_to_cash',null,array()); ?><br />
                    </div>
                    <div id="ignore_dpc_box" class="<?= ($allow_ignore_dpc == true ?'':'none');?>">
                        <?php echo $htmlExt->checkbox('ClientWorkingHour/ignore_dpc',null,array('label'=>'Ignorovat DPČ')); ?><br />
                    </div>
                    <div class="odmeny_typ_3 none">
                        <div style="height: 49px;">&nbsp;</div>
                        
                        <?php echo $htmlExt->input('ClientWorkingHour/ps_hodina_hmm_monthly',array('label'=>'Základní měsíční mzda','readonly'=>'readonly','class'=>'read_info short float for_null_past_money_item_change'));?><br />
               
                        <?php echo $htmlExt->input('ClientWorkingHour/salary_per_hour_h1000',array('label'=>'Celkem mzda', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
                    </div>
                </div>
					<br class="clear_left" />
					<?php //echo $htmlExt->textarea('ClientWorkingHour/drawback_comment',array('label'=>'Komentář ke srážkám', 'class'=>'textarea_spec','label_class'=>'small'));?>			
					<?php echo $htmlExt->textarea('ClientWorkingHour/comment',array('label'=>'Komentář k docházce', 'class'=>'textarea_spec','label_class'=>'small'));?>				
				<br class="clear_left" />
                <?php 
                if(isset($cena_ubytovani_na_mesic)){
                    echo $htmlExt->input('ClientWorkingHour/real_accommodation_price',array('label'=>'Reálná cena za ubyt.','class'=>'short integer','label_class'=>'small'));
                  }?><br class="clear_left" />
                
                <div id="payment_list_box">
                    <div id="payment_list_box_items">
                    <?php 
                    echo "&nbsp;Zálohy:<br class='clear_left' />";
                    foreach($payment_list as $item){
                        $out = null;
                        $out .= '<strong>'.$fastest->czechDate($item['ReportDownPayment']['created']).'</strong>';
                        $out .= '&nbsp;&nbsp;&nbsp;'.$item['CmsUser']['name'];
                        $out .= '&nbsp;&nbsp;&nbsp;částka - '.$item['ReportDownPayment']['amount'];
                        $out .= '&nbsp;&nbsp;&nbsp;hodin - '.$item['ReportDownPayment']['celkem_hodin'];
                        $out .= '&nbsp;&nbsp;&nbsp; uhrazeno - '.$fastest->czechDate($item['ReportDownPayment']['date_of_paid']);
                        
                        $payment_sum += $item['ReportDownPayment']['amount'];
                        echo $out.'<br class="clear_left" />';
                    }
                    ?>
                    </div>
                    <br class="clear_left" /> 
                    <?php 
                    echo "&nbsp;Srážka za OPP:<br class='clear_left' />";
                    foreach($opp_list as $item){
                        $out = null;
                        $out .= '<strong>Položka:</strong> '.$item['ConnectionClientOpp']['typ'];
                        $out .= ', <strong>Velikost</strong> '.$item['ConnectionClientOpp']['size'];
                        $out .= ', <strong>Cena</strong> '.$fastest->price($item['ConnectionClientOpp']['price'],'');
                        echo $out.'<br class="clear_left" />';
                    }
                    ?>
                </div>
			</fieldset>
		</div>
		<div class="domtabs field" >
			<fieldset>
				<legend>Základní nastavení <?php if (isset($transfer_from_last_month)):?>(Přeneseno z minulého měsíce)<?php endif;?></legend>
				<div class='sll3'><?php //echo($this-data['ClientWorkingHour']['accommodation_id']);?>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost'),null,false);?> <br /> 
					<br /><br />
					<?php echo $htmlExt->selectTag('ClientWorkingHour/including_suit',array('Ne','Ano'),null,array('label'=>'Dostává prac. oděv'),null,false);?><br/>
					<?php echo $htmlExt->input('ClientWorkingHour/client_number',array('label'=>'Klientské číslo'));?><br/>
					<?php echo $htmlExt->selectTag('ClientWorkingHour/subdodavka',array('Ne','Ano'),null,array('label'=>'Subdodávka'),null,false);?><br/>	
				</div>
				<div class='sll3'>
					<?php echo $htmlExt->input('ClientWorkingHour/standard_hours',array('readonly'=>'readonly','value'=>$this->data['ClientWorkingHour']['standard_hours'],'label'=>'Norma hod.','class'=>'read_info'));?><br />
					<?php echo $htmlExt->input('ClientWorkingHour/cena_za_ubytovani',array('readonly'=>'readonly','label'=>'Cena ubytování','class'=>'read_info'));?><br/>
					<div id="stravenka" class="<?php echo $max_salary_hours['stravenka']==0 ? 'hidden' : ""; ?>"><?php echo $htmlExt->input('ClientWorkingHour/stravenka',array('readonly'=>'readonly','label'=>'Cena stravenky','class'=> 'read_info','label_class'=> 'read_info'));?><br/></div>
					<div id="doprava" class="<?php echo $max_salary_hours['doprava']==0 ? 'hidden' : ""; ?>"><?php echo $htmlExt->var_text('ClientWorkingHour/doprava', array('label'=>'Cena dopravy','class'=> 'read_info','label_class'=> 'read_info','value'=>$max_salary_hours['doprava']));?><br/></div>
						<div style='height:38px'></div><br/>
					<div id="subdodavka" class=""><?php echo $htmlExt->selectTag('ClientWorkingHour/subdodavka_id',$subdodavka_list,null,array('label'=>'Subdodavatel'),array('title'=>'Subdodávka'));?><br/></div>
					
				</div>
				<div class='sll3'>
					<br/>			
				</div>
			</fieldset>
         
		</div>
		<div class="domtabs field" >
			<div class='sll'>
				<?php echo $htmlExt->input('ClientWorkingHour/salary_user_cost',array('label'=>'Osobní náklady'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/salary_cost',array('label'=>'Mzdové náklady','class'=>'read_info','readonly'=>'readonly'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/accommodation_cost',array('label'=>'Náklady na ubytování','value'=>(($this->data['ClientWorkingHour']['including_of_accommodation']==0)?'n/a':$this->data['ClientWorkingHour']['accommodation_cost']),'class'=>'read_info','readonly'=>'readonly'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/recruitment_cost',array('label'=>'Náklady na nábor','class'=>'read_info','readonly'=>'readonly'));?>
				<?php echo $htmlExt->input('ClientWorkingHour/clothes_cost',array('label'=>'Náklady na oděv','value'=>(($this->data['ClientWorkingHour']['including_suit']==0)?'n/a':$this->data['ClientWorkingHour']['clothes_cost'])));?>
				<?php echo $htmlExt->input('ClientWorkingHour/transfer_cost',array('label'=>'Náklady na dopravu','value'=>(($this->data['ClientWorkingHour']['including_of_transport']==0)?'n/a':$this->data['ClientWorkingHour']['transfer_cost'])));?>
				<?php echo $htmlExt->input('ClientWorkingHour/food_cost',array('label'=>'Náklady na stravenky','value'=>(($this->data['ClientWorkingHour']['stravenka']==0)?'n/a':$this->data['ClientWorkingHour']['stravenka']),'class'=>'read_info','readonly'=>'readonly'));?>
			
			</div>
			<div class='slr'>
				<?php echo $htmlExt->input('ClientWorkingHour/marze',array('label'=>'Vypočítaná marže','class'=>'read_info','readonly'=>'readonly'));?>
				<label>Vygenerování marže:</label><input type='button' id='generate_marze' value='Vygeneruj' style='width:57.2%; margin-left:3px;'/><br/>
			</div>
			<br />
		</div>
      
	</div>
	<div style='text-align:center'>
		<?php if ((empty($this->data['ClientWorkingHour']['stav']) || in_array($this->data['ClientWorkingHour']['stav'],array(1))) || $company_detail['Company']['manazer_realizace_id'] == $logged_user || in_array($logged_user,array(209,236,237)) || in_array($logged_group,$permission_alow) || in_array($logged_group,array(1,5)) || in_array($logged_group,array(6,7)) && in_array($this->data['ClientWorkingHour']['stav'],array(1,2))):?>
		<input type='button' value='Uložit' id='only_save'/>
		<?php endif;?>
        
        <?php if ((($year.'-'.$month) == date('Y-m')) && $allow_payment_0){?>
		      <input type='button' value='Požadavek na zálohu' rel='0' class='add_down_payments'/>
        <?php }?>
        
        <?php if ((($year.'-'.$month) == date('Y-m')) && $allow_payment_1){?>
		      <input type='button' value='Požadavek na VVH' rel='1' class='add_down_payments'/>
		<?php }?>
         
        <input type='button' value='Přidat negativní hodnocení' id='add_misconducts'/>
		<input type='button' value='Zavřít' id='only_close'/>

        <?php if ($logged_group == 1 && !empty($this->data['ClientWorkingHour']['stav']) &&  $this->data['ClientWorkingHour']['stav'] != 1):?>
        <input type='button' value='Zrušení uzavření dochazky' id='cancel_auth'/>
        <?php endif;?>

	</div>
</form>

<div id='dblclick_input' class='none'>
<label>Typ:</label>
	<select class='choose_input'>
		<option value="1" 	title="Cas">Cas</option>
		<option value="PN" 	title="Pracovní neschopnost">Pracovní neschopnost</option>
		<option value="D" 	title="Dovolená">Dovolená</option>
		<option value="NV" 	title="Neplacené volno">Neplacené volno</option>
		<option value="OČR" title="Ošetřování člena rodiny">Ošetřování člena rodiny</option>
		<!--
        <option value="A" 	title="Neomluvená absence">Neomluvená absence</option>
		-->
        <option value="V" 	title="Nahradní volno">Nahradní volno</option>
		<option value="PG" 	title="Paragraf">Paragraf</option>
	</select>
	<br/>
    <div class="dovolena_info none">
        <?php 	echo $htmlExt->var_text('Client/zbyva_dovolene', array('label'=>'Nárok na dovolenou','class'=>'read_info','value'=>$zbyva_dovolene)); ?>
    </div>
	<div class='choose_do_div'>
		<label>Od:</label>
		<select class='choose_od_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_od_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<div class='choose_od_div'>
		<label>Do:</label>
		<select class='choose_do_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_do_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<p>
		<input type='button' value='Vložte' class='choose_do_it'/>
	</p>
</div>


<script language="javascript" type="text/javascript">

    if ($('cancel_auth')){
        $('cancel_auth').addEvent('click', function(e){
            e.stop();
            if (confirm('Opravdu si přejete zrušit uzavření docházky?')){
                button_preloader($('cancel_auth'));
                new Request.JSON({
                    url: '/employees/cancel_auth/' + $('ClientWorkingHourId').value,
                    onComplete: function(json){
                        if (json){
                            if (json.result === true){
                                new Request.HTML({
                                    url: '/employees/odpracovane_hodiny/' + $('ConnectionClientRequirementId').value + '/' + $('Year2').value + '/' + $('Month2').value + '/true',
                                    update: $('odpracovane_hodiny_element'),
                                    onComplete: function(){
                                        $('odpracovane_hodiny_element').fade(1);
                                        click_refresh($('ConnectionClientRequirementId').value);
                                    }
                                }).send();
                            } else {
                                alert(json.message);
                            }
                        } else {
                            alert('Chyba aplikace, opakujte akci pozdeji');
                        }
                        button_preloader_disable($('cancel_auth'));
                    }
                }).send();
            }
        });
    }

	// domtabs init
	var domtab = new DomTabs({'className':'admin_dom'});
	var admin_save = false;
    var odmeny_typ = <?php echo $odmeny_typ;?>;
	var manazar_realizace_id = '<?php echo $company_detail['Company']['manazer_realizace_id'];?>';
    var stat_id = <?php echo $stat_id;?>;
    var max_hodin_na_smennost = 8;
    var permission_to_editation_max = '<?php echo $permission_to_editation_max;?>';
    var warning_about_cw_tempalte_miss = '<?php echo $warning_about_cw_tempalte_miss;?>';
    var dohoda_limit = '<?= (isset($dohoda_limit)?$dohoda_limit:false);?>';

	// editace maximalek
	<?php if(isset($logged_group)){	?> 
		var logged_group = <?php echo $logged_group;?>;
		var logged_user = <?php echo $logged_user;?>;
		// admini a directori muzou editovat maximalky
		var allow_groups = [1, 5];
		var allow_users = [209, 184, 236,237];
		var permission_alow = <?php echo '['.join(',',$permission_alow).']';?>;
		if(logged_group == 1 || allow_users.contains(logged_user) || permission_alow.contains(logged_group) || manazar_realizace_id == logged_user){
			$('ClientWorkingHourCompanyWorkPositionId').removeProperty('disabled');
			$('ClientWorkingHourCompanyWorkPositionId').removeClass('read_info');
			$('ClientWorkingHourCompanyMoneyItemId').removeProperty('disabled');
			$('ClientWorkingHourCompanyMoneyItemId').removeClass('read_info');
		}
	<?php }	?>
    //nova volba nastavnei editace maximalek!
    if(permission_to_editation_max == true){
        $('odpracovane_hodiny_element').getElements('input.maximalky').removeProperty('disabled');
    }


	<?php if(isset($cista_mzda_1001)){	?>
	// osetreni formy odmeny 1001 a nastaveni default hodnot z db
	if($('ClientWorkingHourCompanyMoneyItemId').value != null){
		title = $('ClientWorkingHourCompanyMoneyItemId').options[$('ClientWorkingHourCompanyMoneyItemId').selectedIndex].title.split('|');
		forma = title[0];
		
		if(forma == '1001'){
		    var hodnota = (<?php echo $cista_mzda_1001;?>).toFloat();
            if(hodnota > $('ClientWorkingHourSalaryPart1Max').value){ hodnota = $('ClientWorkingHourSalaryPart1Max').value;}
			$('ClientWorkingHourSalaryPart1').value = hodnota;
			$('ClientWorkingHourSalaryPart1').setProperty('disabled','disabled');
			calculate_salary_cost();	
		}
			
	}
	<?php }	?>

	// prazdne inputy vloz 0
	$('ClientWorkingHour_edit_formular').getElements('.float').each(function(item){
		if (item.value == '') item.value = '0';
	});
    //pri zmene float input(dojde k smazani hodnoty) zmen na nulu
    //jinak součty pak házejí NaN
    $('ClientWorkingHour_edit_formular').getElements('.float').addEvent('change',function(e){if(this.value == ''){this.value = 0;}})

	// calculate sumary salary per hour
	$$('.salary').addEvent('change', function(){
		var max = ((this.id)+'Max');

		if(this.value.toFloat() > $(max).value.toFloat()){
			alert('Překročili jste maximální hodnotu! Bude nastavena maximální hodnota.');
			this.value=$(max).value.toFloat();
		}
			var sumary = 0;
			$$('.salary').each(function(el){
				if (el.value != '' && el.id !='ClientWorkingHourOdmena2' && el.id !='ClientWorkingHourOdmena1')
					sumary += el.value.toFloat();
			});

			$('ClientWorkingHourSalaryPerHour').value = sumary;
			if (sumary.toFloat() > $('ClientWorkingHourMaxSalary').value.toFloat() )
				$('ClientWorkingHourSalaryPerHour').addClass('invalid');
			else
				$('ClientWorkingHourSalaryPerHour').removeClass('invalid');
			
			calculate_salary_cost(true);	
	});


	// condition for readonly for use_accommodation
	$('ClientWorkingHourAccommodationId').addEvent('change', function(){
		if (this.value != 0){
			$('ClientWorkingHour_edit_formular').getElements('.accommodation_day:enabled').setProperty('checked','checked');
		
			title = this.options[this.selectedIndex].title.split('|');
			$('ClientWorkingHourCenaZaUbytovani').value = title[1];
			$('ClientWorkingHourTypCenaZaUbytovani').value = title[0];
            
            rozdil_v_cene_ubytovani();
		} else {
			$('ClientWorkingHour_edit_formular').getElements('.accommodation_day:enabled').removeProperty('checked');
		
			$('ClientWorkingHourCenaZaUbytovani').value = $('ClientWorkingHourRozdilVUbytovani').value = 0;
			$('ClientWorkingHourTypCenaZaUbytovani').value = -1;
		}
		set_accommodation(this.value);
		//calculate_accommodation_money();	
	});
    
    if($('ClientWorkingHourRealAccommodationPrice'))
        $('ClientWorkingHourRealAccommodationPrice').addEvent('change', rozdil_v_cene_ubytovani);  
    
    if($('ClientWorkingHourDrawbackAccommodationAdd'))
        $('ClientWorkingHourDrawbackAccommodationAdd').addEvent('click', function(){calculate_salary_cost(true)});
    if($('ClientWorkingHourDrawbackOppAdd'))
        $('ClientWorkingHourDrawbackOppAdd').addEvent('click', function(){calculate_salary_cost(true)});    
    
    if($('ClientWorkingHourTransferDohodaToCash'))
        $('ClientWorkingHourTransferDohodaToCash').addEvent('click', function(){calculate_salary_cost(true)});  
    
    if($('ClientWorkingHourTransferDohodaToCashValue'))
        $('ClientWorkingHourTransferDohodaToCashValue').addEvent('change', function(){calculate_salary_cost(true)});  
    
    if($('ClientWorkingHourIgnoreDpc'))
        $('ClientWorkingHourIgnoreDpc').addEvent('click', function(){calculate_salary_cost()});  
    
    
    /**
     * Jedna se o prepocet rozdilu ubytovani
     * (suma na ubytovanie podla kalkulacie) minus (pocet dni zaskrknutych checkboxoch * cena ubytovani v karte dochadzka v karte „nastaveni“ )
     */
    function rozdil_v_cene_ubytovani(){
        if($('ClientWorkingHourDrawbackAccommodation')){
            prispevke_na_ubytovani = $('CenaUbytovaniNaMesic').value.toFloat();
            pocet_dnu_v_mesici = $('PocetDnuVMesici').value.toFloat();
            pocet_zaskrtnutych_dni = 0;
            $each($('ClientWorkingHour_edit_formular').getElements('.accommodation_day:enabled'),function(item){
                if(item.checked){pocet_zaskrtnutych_dni++;}
            })
            
            realna_cena_za_ubytovani = $('ClientWorkingHourRealAccommodationPrice').value.toFloat();
            cena_ubytovani_za_den = realna_cena_za_ubytovani / pocet_dnu_v_mesici;
            $('ClientWorkingHourDrawbackAccommodation').value = Math.round((prispevke_na_ubytovani - (pocet_zaskrtnutych_dni*cena_ubytovani_za_den))*100)/100;
            calculate_salary_cost(true);
        }
    }

	// subdodavatel change and hide select
	if($('ClientWorkingHourSubdodavkaId').value == "")
		$('subdodavka').addClass('hidden');

	$('ClientWorkingHourSubdodavka').addEvent('change', function(){
		if (this.value != 0){			
				$('subdodavka').removeClass('hidden');
		} else {
				$('subdodavka').addClass('hidden');
		}
	});

	// vygenerovani marze
	$('generate_marze').addEvent('click', function(e){
		new Event(e).stop();
		
		plat = ($('ClientWorkingHourSalaryCost').value == '' || $('ClientWorkingHourSalaryCost').value == 'n/a')?0:$('ClientWorkingHourSalaryCost').value.toFloat();
		ubyt = ($('ClientWorkingHourAccommodationCost').value == '' || $('ClientWorkingHourAccommodationCost').value == 'n/a')?0:$('ClientWorkingHourAccommodationCost').value.toFloat();
		recr = ($('ClientWorkingHourRecruitmentCost').value == '' || $('ClientWorkingHourRecruitmentCost').value == 'n/a')?0:$('ClientWorkingHourRecruitmentCost').value.toFloat();
		clot = ($('ClientWorkingHourClothesCost').value == '' || $('ClientWorkingHourClothesCost').value == 'n/a')?0:$('ClientWorkingHourClothesCost').value.toFloat();
		tran = ($('ClientWorkingHourTransferCost').value == '' || $('ClientWorkingHourTransferCost').value == 'n/a')?0:$('ClientWorkingHourTransferCost').value.toFloat();
		food = ($('ClientWorkingHourFoodCost').value == '' || $('ClientWorkingHourFoodCost').value == 'n/a')?0:$('ClientWorkingHourFoodCost').value.toFloat();
		
		var naklady = plat + ubyt + recr + clot + tran + food;
		var vynosy = $('ClientWorkingHourFakturace').value.toFloat() * $('ClientWorkingHourCelkemHodin').value.toFloat();
		$('ClientWorkingHourMarze').value = Math.round((vynosy - naklady) / vynosy * 100 * 100) / 100
	});

	$('ClientWorkingHourSalaryUserCost').addEvent('change',  function(){calculate_salary_cost(true)});
	$('ClientWorkingHourDrawback').addEvent('change',  function(){calculate_salary_cost(true)});
    $('ClientWorkingHourDrawbackOpp').addEvent('change', function(){calculate_salary_cost(true)});

	$('only_close').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_zamestnanci_hodiny')
	});

	// change use accommodation
	function set_accommodation(e){
		if (e == 0){
			$('ClientWorkingHour_edit_formular').getElements('.accommodation_day').removeProperty('checked');
			$('ClientWorkingHour_edit_formular').getElement('.tr_accommodation').addClass('none');
		} else {
			$('ClientWorkingHour_edit_formular').getElement('.tr_accommodation').removeClass('none');
		}
		calculate_accommodation_money();
	};

	// change use food_ticket
	function set_food_ticket(e){
		if (e == 0){
			$('ClientWorkingHour_edit_formular').getElements('.food_days').removeProperty('checked');
			$('ClientWorkingHour_edit_formular').getElement('.tr_food_ticket').addClass('none');
		} else {
			$('ClientWorkingHour_edit_formular').getElement('.tr_food_ticket').removeClass('none');
		}
		calculate_accommodation_money();
	};
	
	// event to calculate  && declare function for calculate caccommodation
	function calculate_accommodation_money(){ 
		switch ($('ClientWorkingHourTypCenaZaUbytovani').value){
			case '-1':  $('ClientWorkingHourAccommodationCost').value = 'n/a'; break;
			case '1':
				$('ClientWorkingHourAccommodationCost').value = Math.round(($('ClientWorkingHourCenaZaUbytovani').value * $$('.accommodation_day').map(function(el){ if(el.checked) return el; else return null; }).clean().length) * 100)/100;
				break;
			case '2': $('ClientWorkingHourAccommodationCost').value = $('ClientWorkingHourCenaZaUbytovani').value; break;
			default:
				break;
		}
	}
	$$('.accommodation_day').addEvent('click', calculate_accommodation_money);
	
	//hromadne zaskrtnuti
	$('accommodation_days_all').addEvent('click', my_select_all.bindWithEvent(this,['accommodation_day']));
	$('food_days_all').addEvent('click', my_select_all.bindWithEvent(this,['food_day']));
	

	// fce na hromadne zaskrtnuti
	function my_select_all(e,className){
		var event = new Event(e);
		event.stop();
		obj = event.target;

		if (!obj.zaskrkly){
			$('ClientWorkingHour_edit_formular').getElements('.'+className+':enabled').setProperty('checked','checked');
			obj.zaskrkly = true;
		} else {
			$('ClientWorkingHour_edit_formular').getElements('.'+className).removeProperty('checked');
			try {
				delete obj.zaskrkly;
			} catch (e){
				obj.zaskrkly = null;
			}
		}
	}
    
    
	
	function calculate_salary_cost(change){



	    if(typeof change == "undefined"){
	       change = false;   
        }
        
        var money_item_title = $('ClientWorkingHourCompanyMoneyItemId').options[$('ClientWorkingHourCompanyMoneyItemId').selectedIndex].getProperty('title').split('|');
        var money_item_selected = money_item_title[0];
	    if((money_item_selected == "0100" || money_item_selected == "0101") && dohoda_limit.toInt() > 0 && $('ClientWorkingHourYear').value.toInt() >= 2012){
            calculate_salary_cost_dohoda(change);
            return true;
	    }

        if (money_item_selected == "0111" ){
            var celkem_na_hod = $('ClientWorkingHourSalaryPerHour').value;
            var celkem_hodin = $('ClientWorkingHourCelkemHodin').value;

            if ((celkem_na_hod * celkem_hodin) > 2000){
                pocet_hodin = (2000 - (2000 % celkem_na_hod)) / celkem_na_hod;
                castka = pocet_hodin * celkem_na_hod;
                castka = (celkem_na_hod * celkem_hodin) - castka;
                $('ClientWorkingHourFaDpcCalculation').value = castka;
            } else {
                $('ClientWorkingHourFaDpcCalculation').value = 0;
            }
        }
        if(money_item_selected == "0010" && $('ClientWorkingHourFaDpcCalculation')){
             if($('ClientWorkingHourFaDpcCalculation').value == '' || $('ClientWorkingHourFaDpcCalculation').value == 0){
                $('ClientWorkingHourFaDpcCalculation').value = Math.floor((Math.random()*250) + 2250);
             }
        }

		var pocet_hodin = $('ClientWorkingHourCelkemHodin').value.toFloat();
		var osobni_naklady = $('ClientWorkingHourSalaryUserCost').value.toFloat();
		
		var salary_part_1 = $('ClientWorkingHourSalaryPart1').value.toFloat();
		var salary_part_2 = $('ClientWorkingHourSalaryPart2').value.toFloat();
		var salary_part_3 = $('ClientWorkingHourSalaryPart3').value.toFloat();
		var salary_part_4 = $('ClientWorkingHourSalaryPart4').value.toFloat();
		
		var drawback = $('ClientWorkingHourDrawback').value.toFloat();
        //var drawback_opp = $('ClientWorkingHourDrawbackOpp').value.toFloat();
        
        if($('ClientWorkingHourDrawbackAccommodationAdd')){
            var drawback_accommodation_add = $('ClientWorkingHourDrawbackAccommodationAdd').checked; 
            var drawback_accommodation = (drawback_accommodation_add == true && $('ClientWorkingHourDrawbackAccommodation').value.toFloat() < 0 ? $('ClientWorkingHourDrawbackAccommodation').value.toFloat() : 0);
        }
        else{
            drawback_accommodation_add = drawback_accommodation = 0;
        }
        
        if($('ClientWorkingHourDrawbackOppAdd')){
            var drawback_opp_add = $('ClientWorkingHourDrawbackOppAdd').checked; 
            var drawback_opp = (drawback_opp_add == true ? $('ClientWorkingHourDrawbackOpp').value.toFloat() : 0);
        }
        else{
            drawback_opp_add = drawback_opp = 0;
        }
        
		var odmena_1 = $('ClientWorkingHourOdmena1').value.toFloat();
		var odmena_2 = $('ClientWorkingHourOdmena2').value.toFloat();

		var salary_to_count = salary_part_2 + salary_part_4;

        if (money_item_selected == "0111" && $('ClientWorkingHourFaDpcCalculation').value != 0){

                $('ClientWorkingHourSalaryPart1P').value = s1 = pocet_hodin * salary_part_1;
                $('ClientWorkingHourSalaryPart2P').value = s2 =  pocet_hodin * salary_part_2 - $('ClientWorkingHourFaDpcCalculation').value.toFloat() ;
                $('ClientWorkingHourSalaryPart3P').value = s3 = pocet_hodin * salary_part_3;
                $('ClientWorkingHourSalaryPart4P').value = s4 =  pocet_hodin * salary_part_4 + $('ClientWorkingHourFaDpcCalculation').value.toFloat() ;
        } else {
            $('ClientWorkingHourSalaryPart1P').value = s1 = pocet_hodin * salary_part_1;
            $('ClientWorkingHourSalaryPart2P').value = s2 =  pocet_hodin * salary_part_2;
            $('ClientWorkingHourSalaryPart3P').value = s3 = pocet_hodin * salary_part_3;
            $('ClientWorkingHourSalaryPart4P').value = s4 =  pocet_hodin * salary_part_4;
        }
        
        var ignore_dpc = false;
        if($('ClientWorkingHourIgnoreDpc') && $('ClientWorkingHourIgnoreDpc').checked){ignore_dpc = true;}
        
        if(ignore_dpc === true){
            $('ClientWorkingHourFaDpcCalculation').value = 0;
        }

        /**
         * DPC odecet od ZL
         */
        if(stat_id == 1 && ignore_dpc === false && $('ClientWorkingHourFaDpcCalculation') && $('ClientWorkingHourFaDpcCalculation').value > 0 && s3 > 0){
            s3 = s3 - $('ClientWorkingHourFaDpcCalculation').value.toFloat();
            $('ClientWorkingHourSalaryPart3P').value = s3;
        }


        var celkem_mzda = ((s1 + s2 + s3 + s4)-drawback-drawback_opp+drawback_accommodation+odmena_1+odmena_2);

		$('ClientWorkingHourSalaryPerHourP').value = (stat_id == 1 ?  Math.round(celkem_mzda) : celkem_mzda);
		
		if (salary_part_1 == '0' && salary_part_3 == '0'){
			$('ClientWorkingHourSalaryCost').value = salary_to_count * pocet_hodin ;
			$('ClientWorkingHourSalaryUserCost').value = 'n/a';
		} else {
			if ($('ClientWorkingHourSalaryUserCost').value == 'n/a')
				$('ClientWorkingHourSalaryUserCost').value = 0;
			$('ClientWorkingHourSalaryCost').value = salary_to_count * pocet_hodin + $('ClientWorkingHourSalaryUserCost').value.toFloat();
			
		}
        
        if(odmeny_typ == 3){
            var salary_part_h1000_na_hodinu = $('ClientWorkingHourPsHodinaHmm').value.toFloat();
            var celkem_mzda2 = ((pocet_hodin * salary_part_h1000_na_hodinu)-drawback-drawback_opp+odmena_1+odmena_2)
            $('ClientWorkingHourSalaryPerHourH1000').value = (stat_id == 1 ?  Math.floor(celkem_mzda2) : celkem_mzda2);
	
        }	
	};

    if($('reset_dpc') && $('ClientWorkingHourFaDpcCalculation')){
        $('reset_dpc').addEvent('click', function(e){
            e.stop();
            $('ClientWorkingHourFaDpcCalculation').value = Math.floor((Math.random()*250) + 2250);
            calculate_salary_cost();
        })
    }
    
    /**
     * nove pocitani mzdy pro dohodu
     * naprosto nesmyslne zadani...
     */
    function calculate_salary_cost_dohoda(change){
        var transfer = $('ClientWorkingHourTransferDohodaToCash').checked;
        var transfer_value = $('ClientWorkingHourTransferDohodaToCashValue').value.toFloat();
        
        var pocet_hodin = $('ClientWorkingHourCelkemHodin').value.toFloat();
		var osobni_naklady = $('ClientWorkingHourSalaryUserCost').value.toFloat();
		
		var salary_part_1 = $('ClientWorkingHourSalaryPart1').value.toFloat();
		var salary_part_2 = $('ClientWorkingHourSalaryPart2').value.toFloat();
		var salary_part_3 = $('ClientWorkingHourSalaryPart3').value.toFloat();
		var salary_part_4 = $('ClientWorkingHourSalaryPart4').value.toFloat();
		
		var drawback = $('ClientWorkingHourDrawback').value.toFloat();

        if($('ClientWorkingHourDrawbackAccommodationAdd')){
            var drawback_accommodation_add = $('ClientWorkingHourDrawbackAccommodationAdd').checked; 
            var drawback_accommodation = (drawback_accommodation_add == true && $('ClientWorkingHourDrawbackAccommodation').value.toFloat() < 0 ? $('ClientWorkingHourDrawbackAccommodation').value.toFloat() : 0);
        }
        else{
            drawback_accommodation_add = drawback_accommodation = 0;
        }
        
        if($('ClientWorkingHourDrawbackOppAdd')){
            var drawback_opp_add = $('ClientWorkingHourDrawbackOppAdd').checked; 
            var drawback_opp = (drawback_opp_add == true ? $('ClientWorkingHourDrawbackOpp').value.toFloat() : 0);
        }
        else{
            drawback_opp_add = drawback_opp = 0;
        }
        
		var odmena_1 = $('ClientWorkingHourOdmena1').value.toFloat();
		var odmena_2 = $('ClientWorkingHourOdmena2').value.toFloat();

		var odmeny = odmena_1 + odmena_2;
        var srazky = 0-drawback-drawback_opp+drawback_accommodation;

        s1 = pocet_hodin * salary_part_1;
        s2 =  pocet_hodin * salary_part_2;
        s3 = pocet_hodin * salary_part_3;
    	s4 =  pocet_hodin * salary_part_4; 
        
        if(change == true || (change == false && pocet_hodin == 0)){
    		$('ClientWorkingHourSalaryPart1P').value = s1;
    		$('ClientWorkingHourSalaryPart2P').value = s2;
    		$('ClientWorkingHourSalaryPart3P').value = s3;
    		$('ClientWorkingHourSalaryPart4P').value = s4; 
        }
        
        var celkem_mzda = ((s1 + s2 + s3 + s4)+srazky+odmeny);
        celkem_mzda = (stat_id == 1 ?  Math.round(celkem_mzda) : Math.round(celkem_mzda,2));
        if(celkem_mzda <= dohoda_limit){
            /**
             * abc se vyplaci do limitu pouze ve forme dohody
             * vse nad limit pak jde do abc
             */
            if(transfer == true){ 
                if(transfer_value > 0){
                    $('ClientWorkingHourSalaryPart4P').value = transfer_value;
                    $('ClientWorkingHourSalaryPart2P').value = celkem_mzda-transfer_value;
                }
                else{
                    $('ClientWorkingHourSalaryPart4P').value = celkem_mzda;
                    $('ClientWorkingHourSalaryPart2P').value = 0;
                }
            }
            else{
                $('ClientWorkingHourSalaryPart2P').value = celkem_mzda;
                $('ClientWorkingHourSalaryPart4P').value = 0;
            }
        }
        else{
            if(transfer == true && transfer_value > 0){ 
                $('ClientWorkingHourSalaryPart4P').value = transfer_value;
                $('ClientWorkingHourSalaryPart2P').value = celkem_mzda-transfer_value;
            } 
            else{       
                var five_percent = (dohoda_limit/100)*5;
                //5% castka a ted randomem od 1-5% odečtem e z dohody a hodime to do abc at neni dohoda porad 9500
                var random_number = Math.random()*five_percent;
                random_number = (stat_id == 1 ?  Math.round(random_number) : Math.round(random_number,2));
                if(change == true && pocet_hodin > 0){
                    var val4 = (celkem_mzda) - dohoda_limit+random_number;
                    var val2 = dohoda_limit-random_number;
                    $('ClientWorkingHourSalaryPart4P').value = (stat_id == 1 ?  Math.round(val4) : Math.round(val4,2));
                    $('ClientWorkingHourSalaryPart2P').value = (stat_id == 1 ?  Math.round(val2) : Math.round(val2,2));
                }
            }
        }
        
        
		$('ClientWorkingHourSalaryPerHourP').value = (stat_id == 1 ?  Math.round(celkem_mzda) : celkem_mzda);
		
    }

	// input limit
	$('ClientWorkingHour_edit_formular').getElements('.integer, .float').inputLimit();
	 
	
	// save button
    if($('only_save'))
    	$('only_save').addEvent('click', function(e){
    		new Event(e).stop();
    
    		$('ClientWorkingHourCompanyWorkPositionId').removeProperty('disabled');
    		$('ClientWorkingHourCompanyMoneyItemId').removeProperty('disabled');
    		// odstraneni disabled u maximalek aby jse ulozili
    		$('ClientWorkingHourSalaryPart1').removeProperty('disabled');
    		$('ClientWorkingHourSalaryPart1Max').removeProperty('disabled');
    		$('ClientWorkingHourSalaryPart2Max').removeProperty('disabled');
    		$('ClientWorkingHourSalaryPart3Max').removeProperty('disabled');
    		$('ClientWorkingHourSalaryPart4Max').removeProperty('disabled');
            
            $each($$('.odpracovane_hodiny').getElements('input'),function(item){
                item.removeProperty('disabled');
            })
    
            //prepocteni hodin pred save
            recount_hours();
    
            button_preloader($('only_save'));
    		new Request.JSON({
    			url: $('ClientWorkingHour_edit_formular').getProperty('action'),
    			onComplete: function(json){
    				if (json){
    					if (json.result === true){
    						if(admin_save == true){
    							new Request.HTML({
    								url: '/employees/odpracovane_hodiny/' + $('ConnectionClientRequirementId').value + '/' + $('Year2').value + '/' + $('Month2').value + '/true',
    								update: $('odpracovane_hodiny_element'),
    								onComplete: function(){
    									$('odpracovane_hodiny_element').fade(1);
    								}
    							}).send();
    						}
    						else {
				      	         /*
                                $('ClientWorkingHourCompanyWorkPositionId').setProperty('disabled','disabled');
    							$('ClientWorkingHourCompanyMoneyItemId').setProperty('disabled','disabled');
    							if(!allow_groups.contains(logged_group)){
    								$('ClientWorkingHourSalaryPart1').setProperty('disabled','disabled');
    								$('ClientWorkingHourSalaryPart1Max').setProperty('disabled','disabled');
    								$('ClientWorkingHourSalaryPart2Max').setProperty('disabled','disabled');
    								$('ClientWorkingHourSalaryPart3Max').setProperty('disabled','disabled');
    								$('ClientWorkingHourSalaryPart4Max').setProperty('disabled','disabled');
    							}
    							$('ClientWorkingHourId').value = json.id;
    							$('made_change_in_form').value = 0;
    						    */
                                
                                click_refresh($('ConnectionClientRequirementId').value);
                                domwin.closeWindow('domwin_zamestnanci_hodiny');
    						}
                            
    					} else {
    						alert(json.message);
    					}
    				} else {
    					alert('Chyba aplikace!');
    				}
                    
                    button_preloader_disable($('only_save'));
    			}
    		}).send($('ClientWorkingHour_edit_formular'));
    	});

		
	
	// function for reacound day/hours
	$$('.hodiny').addEvent('change', recount_hours);
	function recount_hours(){
	    //if(this.value.toFloat() > max_hodin_na_smennost.toFloat()){
	    //   alert("Zadaný počet hodin je větší než nastavená maximálka ("+max_hodin_na_smennost+"), hodnota bude změněna.\nPokud potřebujete zadat větší počet hodin, rozdělte tyto hodin do více směn!")
	    //   this.value = max_hodin_na_smennost;
        //}
        
        
		var NH = $('ClientWorkingHourStandardHours').value.toFloat();
		var count = 0;
		var vikendy = 0;
		var svatky = 0;
		//var prescas = 0;
		var dovolena = 0;
		var neplacene_volno = 0;
		var ocr = 0;
		var neomluvena_absence = 0;
		var pracovni_neschopnost = 0;
		var nahradni_volno = 0;
		var paragraf = 0;
		
		var s1 = 0, s2 = 0, s3 = 0;
		
		$$('.hodiny').each(function(item){
			switch(item.value){
				case 'PN':
						pracovni_neschopnost ++;
					break;
				case 'OČR':
						ocr ++;
					break;
				case 'D':
						dovolena ++;
					break;
				case 'NV':
						neplacene_volno ++;
					break;
				case 'A':
						neomluvena_absence ++;
					break;
				case 'V':
						nahradni_volno ++;
					break;
				case 'PG':
						paragraf ++;
					break;
				default:
                    var value = item.value.replace(/\s+/g, '');
					if (value == ''){
                        item.value = 0;
                        value = 0;
                    }
					
					if (item.hasClass('s1')) s1 += value.toFloat();
					if (item.hasClass('s2')) s2 += value.toFloat();
					if (item.hasClass('s3')) s3 += value.toFloat();
					
					// celkem
					count += value.toFloat();

					// vikend
					if (item.hasClass('vikend')) vikendy += value.toFloat();
					// svatky
					if (item.hasClass('svatky')) svatky += value.toFloat();
					// prescas
					//if (item.value.toFloat() > NH) prescas += (item.value.toFloat() - NH);
					break;
			}
            
            //kontrola zda jsme nesmazali dovolenou
            $('ClientWorkingHourZbyvaDovolene').value = $('DovolenaZbyvaDovolene').value - dovolena;
		});

		
		$('ClientWorkingHourCelkemHodin').value = count;
		$('ClientWorkingHourSvatky').value = svatky;
		//$('ClientWorkingHourPrescasy').value = prescas;
		$('ClientWorkingHourVikendy').value = vikendy;
		$('ClientWorkingHourDovolena').value = dovolena;
		$('ClientWorkingHourNeplaceneVolno').value = neplacene_volno;
		$('ClientWorkingHourOcestrovaniClenaRodiny').value = ocr;
		$('ClientWorkingHourNeomluvenaAbsence').value =  neomluvena_absence;
		$('ClientWorkingHourPracovniNeschopnost').value =  pracovni_neschopnost;
		$('ClientWorkingHourNahradniVolno').value =  nahradni_volno;
		$('ClientWorkingHourParagraf').value =  paragraf;
    		
		$('ClientWorkingHourHourS1').value = s1;
		$('ClientWorkingHourHourS2').value = s2;
		$('ClientWorkingHourHourS3').value = s3;
		
		calculate_salary_cost(true);
		calculate_prescasy();
	};

	// zmena formy hodiny a nacteni jejiho nastaveni
	$('ClientWorkingHourCompanyMoneyItemId').addEvent('change', function(){
		/*
		title = this.options[this.selectedIndex].title.split('|');
		var ar = this.options[this.selectedIndex].getHTML().split(' - ');
		max_salary = 0;
		if (ar[1]){
			ar = ar[1].split('/');	
			for(i=0;i<ar.length;max_salary+=ar[i++].toFloat());
		}
		type = title[0];
		stravenka = title[1];
		normo_hodina = title[2];
		doprava = title[3];
		fakturace = title[4];
		for (i = 0; i<4; i++){
			if (type[i] == 0){ 
				$('ClientWorkingHourSalaryPart'+(i+1)).setProperty('disabled','disabled').setValue('0');
				$('ClientWorkingHourSalaryPart'+(i+1)+'Max').value = 0;
				//$('ClientWorkingHourSalaryPart'+(i+1)).getNext('label').setHTML('/0.00');
			}
			else{ 
				$('ClientWorkingHourSalaryPart'+(i+1)).removeProperty('disabled');
				$('ClientWorkingHourSalaryPart'+(i+1)+'Max').value = ar[i];
				//$('ClientWorkingHourSalaryPart'+(i+1)).getNext('label').setHTML('/'+ar[i]);
			}
		}

		$('ClientWorkingHourMaxSalary').value = max_salary;

		if(stravenka == 0){
			$('stravenka').addClass('hidden');
			$('VarStravenka').setHTML('Ne');
			
		}
		else {
			$('stravenka').removeClass('hidden');
			$('VarStravenka').setHTML('Ano');
		}
		set_food_ticket(stravenka);
		$('ClientWorkingHourStravenka').value = (stravenka) ? stravenka : 0;


		if(doprava==0){
			$('doprava').addClass('hidden');
			$('VarDoprava').setHTML('Ne');
		}
		else {
			$('doprava').removeClass('hidden');
			$('VarDoprava').setHTML('Ano');
		}

		$('ClientWorkingHourDoprava').setHTML((doprava) ? doprava : 0);
		$('ClientWorkingHourStandardHours').value = (normo_hodina) ? normo_hodina : 0;
		$('ClientWorkingHourFakturace').value = (fakturace) ? fakturace : 0;
		
		calculate_salary_cost();
		calculate_food_money();
		*/
		
        change_money_item_id(this);
        
		admin_save = true;  // jiny save - dela se save a po nem hned reload
		$('only_save').value = 'Uložit a obnovit';
		//alert('Pro změnu musíte uložit docházku. Docházka se znova sama nahraje, až po té provádět nové úpravy!');
	});
	
	
	
		
	// event to calculate  && declare function for calcultion food
	function calculate_food_money(){ 
		$('ClientWorkingHourFoodCost').value = $('ClientWorkingHourStravenka').value.toFloat() * $$('.food_day').map(function(el){ if(el.checked) return el; else return null; }).clean().length
	}
	$$('.food_day').addEvent('click', calculate_food_money);
	/*
	// change use food ticket
	$('ClientWorkingHourIncludingOfMealsTicket').addEvent('change', function(){
		if (this.value == 0){
			$('ClientWorkingHour_edit_formular').getElements('.food_day').removeProperty('checked');
			$('ClientWorkingHour_edit_formular').getElement('.tr_food_ticket').addClass('none');
			$('ClientWorkingHourFoodCost').value = 'n/a';
		} else {
			$('ClientWorkingHour_edit_formular').getElement('.tr_food_ticket').removeClass('none');
		}
	})
*/

	// change work shift
	$('ClientWorkingHourSettingShiftWorkingId').addEvents({
		'change': function(e){
			this.dont_change_focus = true;
			if (confirm('Opravdu si přejete změnit typ směn, může dojít ke ztrátě nadefinovaných počtů hodin?')){
				// define new selectedIndex
				this.currentSelection = this.selectedIndex;
				var count_shift = this.value;
				switch (count_shift){
					case '1':
						$('ClientWorkingHour_edit_formular').getElements('.tr_smennost_2, .tr_smennost_3').addClass('none');
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_2').getElements('input').setValue('');
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_3').getElements('input').setValue('');
						
						$('ClientWorkingHourHourS2').addClass('none');
						$('ClientWorkingHourHourS3').addClass('none');
						
						$('ClientWorkingHourHourS2').getPrevious('label').addClass('none');
						$('ClientWorkingHourHourS3').getPrevious('label').addClass('none');
						break;
					case '2':
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_2').removeClass('none');
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_3').addClass('none');
						$('ClientWorkingHour_edit_formular').getElement('.tr_smennost_3').getElements('input').setValue('');
						
						$('ClientWorkingHourHourS2').removeClass('none');
						$('ClientWorkingHourHourS3').addClass('none');
						
						$('ClientWorkingHourHourS2').getPrevious('label').removeClass('none');
						$('ClientWorkingHourHourS3').getPrevious('label').addClass('none');
						break;
					case '3':
						$('ClientWorkingHour_edit_formular').getElements('.tr_smennost_2, .tr_smennost_3').removeClass('none');
						
						$('ClientWorkingHourHourS2').removeClass('none');
						$('ClientWorkingHourHourS3').removeClass('none');
						
						$('ClientWorkingHourHourS2').getPrevious('label').removeClass('none');
						$('ClientWorkingHourHourS3').getPrevious('label').removeClass('none');
						break;
					default:
						alert('Neznamy typ smeny');
						break;
				}
				recount_hours();
			} else {
				new Event(e).stop();
				this.options[this.currentSelection].selected = 'selected';
			}
			this.dont_change_focus = false;
		},
		'focus': function(e){
			// add hack for cancel confirm, its select default selectedIndex
			if (!this.dont_change_focus || this.dont_change_focus == false){
				this.currentSelection = this.selectedIndex;
			}
		}
	});
	
	// add event for change Profese and load list for calculations
	// - vypla dlouhodobe, zmenu nyni v dochazce provadi pouze admina  musi preulozit a znova otevrit dochazku
	$('ClientWorkingHourCompanyWorkPositionId').addEvents({
		'change': function(){
			new Request.JSON({
				url: '/employees/load_calculation_for_prefese/' + this.value + '/' + $('Year2').value + '/' + $('Month2').value,
				onComplete: (function (json){
					if (json){
						if (json.result === true){
							var sub_select = $('ClientWorkingHourCompanyMoneyItemId');
							sub_select.empty();
							new Element('option').setHTML('').inject(sub_select);
							$each(json.data, function(item){
								doprava = 'Doprava '+(item.doprava != 0 ? 'Ano' : 'Ne');
								ubytovani = 'Ubytování '+(item.cena_ubytovani_na_mesic != 0 ? 'Ano' : 'Ne');
	
								new Element('option',{title:item.name + '|' + item.stravenka + '|' + item.fakturacni_sazba_na_hodinu,value:item.id})
									.setHTML(item.name + ' - ' + item.cista_mzda_z_pracovni_smlouvy_na_hodinu + '/' + item.cista_mzda_dohoda_na_hodinu + '/' + item.cista_mzda_faktura_zivnostnika_na_hodinu + '/' + item.cista_mzda_cash_na_hodinu
										+ ' - ' + ubytovani +' / '+ doprava)
									.inject(sub_select);
							})
							this.currentSelection = this.selectedIndex;
							calculate_food_money();
							alert('Změnili jste hlavní nastavení docházky, použijte tl. uložit, docházka se vám znova načte.');
	
						} else {
							this.options[this.currentSelection].selected = 'selected';
							alert(json.message);
						}
					} else {
						alert('Systemova chyba!!!');
					}
				}).bind(this)
			}).send();
		},
		'focus': function(){
			this.currentSelection = this.selectedIndex;
		}
	});
	//

	
	// add event dblclick on textinput
	$('ClientWorkingHour_edit_formular').getElements('.hodiny:enabled').addEvent('dblclick', function(){
		// call
		var obj = this;
		domwin.newWindow({
			id			: 'domwin_hodiny_od_do',
			sizes		: [300,170],
			scrollbars	: true, 
			title		: 'Vložte čas práce',
			languages	: false,
			type		: 'DOM',
			dom_id		: $('dblclick_input'),
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true,
			remove_scroll: false,
			dom_onRender: function(){
				var choose_select = $('domwin_hodiny_od_do').getElement('.choose_input');
				var choose_do_it  = $('domwin_hodiny_od_do').getElement('.choose_do_it');
				
				var choose_do_div = $('domwin_hodiny_od_do').getElement('.choose_do_div');
				var choose_od_div = $('domwin_hodiny_od_do').getElement('.choose_od_div');
				
			
				
				choose_select.addEvent('change', function(){
					if (this.value == 1){
						choose_do_div.removeClass('none');
						choose_od_div.removeClass('none');
					} 
                    else {
						choose_do_div.addClass('none');
						choose_od_div.addClass('none');
                        
					}
                    
                    if (this.value == 'D'){
						$('domwin_hodiny_od_do').getElement('.dovolena_info').removeClass('none');
                        $('domwin_hodiny_od_do').getElement('.dovolena_info').getChildren('var').setHTML($('ClientWorkingHourZbyvaDovolene').value);
                    }    
                    else    
                        $('domwin_hodiny_od_do').getElement('.dovolena_info').addClass('none');
				});
				
				choose_do_it.addEvent('click', function(e){
					new Event(e).stop();
					
					var choose_od = $('domwin_hodiny_od_do').getElement('.choose_od_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_od_m').value;
					var choose_do = $('domwin_hodiny_od_do').getElement('.choose_do_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_do_m').value;
				
					if (choose_select.value == 1)
						obj.value = compare_times(choose_od,choose_do);					
					else {
						obj.value = choose_select.value;

                        if(obj.value == 'D'){
                            var pocet = $('domwin_hodiny_od_do').getElement('.dovolena_info').getChildren('var').getHTML();
                            
                            if(pocet == 0 || pocet < 0)
                                alert('Nárok na dovolenou jde do záporných čísel!');
                            
                            pocet--;
                            $('ClientWorkingHourZbyvaDovolene').value = pocet; 
                        }
					}
					domwin.closeWindow('domwin_hodiny_od_do');
					recount_hours();
				});
			}
		});
	});
	
	function compare_times(time1, time2){
		var s1 = time1.split(':'), s2 = time2.split(':'), td;
		t1 = new Date(1970, 0, 0, s1[0] ? s1[0] : 0, s1[1] ? s1[1] : 0, s1[2] ? s1[2] : 0);
		t2 = new Date(1970, 0, ((s2[0] < s1[0]) || (s2[0]==s1[0] && s2[1] < s1[1]))?1:0, s2[0] ? s2[0] : 0, s2[1] ? s2[1] : 0, s2[2] ? s2[2] : 0);
		td = (t2 - t1)/60/1000;
		//zbytek = ((td % 60) < 10)?'0'+(td % 100):
		zbytek = (td % 60);
		zbytek = (1 / (60 / zbytek));
		output = (td / 60).toInt() + zbytek;
		return output;
	}
	
	$('odpracovane_hodiny_element').getElements('input, select').addEvent('change', function(){
		$('made_change_in_form').value = 1;
	});


	// dynamické přepočítávání přesčasů
	function calculate_prescasy(){
	    /**
	     * vypnuto 6.3.2012
	    
		var tmp = 0;
		var max_dni_v_mesici = $('ClientWorkingHourMaxHodinVMesici').value.toFloat();
		var celkem_hodin = $('ClientWorkingHourCelkemHodin').value.toFloat();
		

		if(max_dni_v_mesici != 0){
			tmp = max_dni_v_mesici - celkem_hodin;
			$('ClientWorkingHourPrescasy').value = ( tmp < 0 ? tmp*-1 : 0);
		}
		else
			$('ClientWorkingHourPrescasy').value = 'Chybné nastavení';
         */
	
	};
	
	// při načtení ihned přepočti
	calculate_prescasy();
	
	
	
	// dynamické přepočítávání přesšasů
	function zjisti_zmenu_maximalky(){
		var result = false;
		var max_mzda = 0;
		maximalky = $('ClientWorkingHourCompanyMoneyItemId').options[$('ClientWorkingHourCompanyMoneyItemId').selectedIndex].getHTML().split('-');
		maximalky = maximalky[1].trim().split('/'); // dane maximalky z formy odmeny
		
		var nastavene_maximalky = new Array( // zvolene maximalky
			$('ClientWorkingHourSalaryPart1Max').value.toFloat(),
			$('ClientWorkingHourSalaryPart2Max').value.toFloat(),
			$('ClientWorkingHourSalaryPart3Max').value.toFloat(),
			$('ClientWorkingHourSalaryPart4Max').value.toFloat()
		);
		
		for (i=0;i<=3;i++){
			if(maximalky[i] < nastavene_maximalky[i])
				result = true;
				
			max_mzda += nastavene_maximalky[i];
		}

		if(result == true){
            if($('ClientWorkingHourChangeMaxSalary').value != 1)
                $('ChangeMaxSalaryNow').value = 1;
          
			$('ClientWorkingHourChangeMaxSalary').value = 1;
        }else{
			$('ClientWorkingHourChangeMaxSalary').value = 0;
            $('ChangeMaxSalaryNow').value = 0;
        }    
			
		//nastaveni max mzdy po prepoctu
		$('ClientWorkingHourMaxSalary').value = max_mzda;
	};
    
	
    /**
     * pausalni view
     * od 5.11.09
     * by Sol
     */
	if(odmeny_typ == 2){
	   $$('.odmeny_typ_1').addClass('none');
	   $$('.odmeny_typ_2').removeClass('none');
	}
    /**
     * view pro 1000H formu odmeny
     * 1.3.10
     */
    else if(odmeny_typ == 3){
       $$('.odmeny_typ_1').addClass('none');
	   $$('.odmeny_typ_3').removeClass('none');
    }
	
	
    /**
     * Nove hlidani change money item
     * od 11.11.09
     * by Sol
     */
     function change_money_item_id(item){
         if(item.value != ''){
            if(confirm('Přenést tuto změnu i pro následující měsíce?')){
                $('TransferMoneyItemId').value = 1;
            }
            
            alert('Změny se projeví až po uložení a znovu načtení karty docházky.');
            $('only_save').removeClass('hidden');
            
            $('ClientWorkingHour_edit_formular').getElements('.salary,.for_null_past_money_item_change').each(function(item){
                item.value = 0;
    		});
    		calculate_salary_cost(true);
    		calculate_food_money();
         }
         else
             $('TransferMoneyItemId').value = 0;
     }   
    
    
    
    /**
     * Provadi osetreni chyby formy odmeny / je null
     * od 11.11.09
     * by Sol
     */
     function money_item_null_guard(){
        if($('ClientWorkingHourCompanyMoneyItemId').value == ''){
            $('only_save').addClass('hidden');
            //
            
            //request ktery zjistí z novych forem, moznou nahradu
            	new Request.JSON({
    				url: '/employees/load_new_posssible_money_item_id/' + $('ClientWorkingHourConnectionClientRequirementId').value + '/',
    				onComplete: function (json){
    					if (json){
    						if (json.result === true){
    							alert('Forma odměny je prázdna, docházka nyní nelze uložit. Prosím zvolte příslušnou formu odměny.\n\nVětšinou se jedná o změnu platnosti u dané kalkulace a je třeba zvolit k ní novou.');
                                if(json.id){
                               	     $('ClientWorkingHourCompanyMoneyItemId').getElements('option').each(function(item){
                               	       if(item.value != '' && item.value != json.id){
                                            item.dispose();
                                       }
                                       else if(item.value == '')
                                            item.setHTML('Vyberte novou formu odměny');    
                                    });
                                  
                                    //odstreni starych events
                                    $('ClientWorkingHourCompanyMoneyItemId').removeEvents('change');
                                    
                                    $('ClientWorkingHourCompanyMoneyItemId').removeClass('read_info');
                                    $('ClientWorkingHourCompanyMoneyItemId').removeAttribute('disabled');
                                               
                                    //prirazeni novych funkci
                                    $('ClientWorkingHourCompanyMoneyItemId').addEvent('change', function(){change_money_item_id(this);});
                                }
                                else
                                    alert('Chyba nepodařilo se předat formu odměny, zkuste to prosím znova.');
    						} else {
    							alert(json.message);
    						}
    					} else {
    						alert('Systemova chyba!!!');
    					}
    				}
		      	}).send();
            	        
        }
        else {
           zjisti_zmenu_maximalky();	
	       $$('.maximalky').addEvent('change', zjisti_zmenu_maximalky);
        }
        
     }
     //vyvolani udalosti guard money item
     money_item_null_guard(); 
     
     $('ClientWorkingHourStopPayment').addEvent('change', function(e){
        if(this.checked)
            $('StopPaymentNow').value = 1;
        else
            $('StopPaymentNow').value = 0;    
     });
    
    /**
     * jiz proplaceno
     */
    var payment_sum = <?php echo $payment_sum;?>;
    title = $('ClientWorkingHourCompanyMoneyItemId').options[$('ClientWorkingHourCompanyMoneyItemId').selectedIndex].title.split('|');
    forma = title[0];
    if (forma == '1001'){
        var zbyva_k_vyplate_ = $('ClientWorkingHourSalaryPart1P').value - $('ClientWorkingHourDrawback').value;
    } else {
        var zbyva_k_vyplate_ = $('ClientWorkingHourSalaryPerHourP').value - payment_sum;
    }

    $('ChangeMaxZalohyNow').value = (stat_id == 1 ? Math.floor(zbyva_k_vyplate_) : zbyva_k_vyplate_);
    
    /**
     * zavolani domwinu pro vytvoreni pozadavku pro zalohu
     */
     if($$('.add_down_payments')){
    	$$('.add_down_payments').addEvent('click', function(e){
    		new Event(e).stop();
            
            var type = this.getProperty('rel');
            
            /**
             * limit k propalceni
             */
            var payment_limit =  $('ChangeMaxZalohyNow').value;
            
            	domwin.newWindow({
        			id			: 'domwin_down_payments',
        			sizes		: [350,185],
        			scrollbars	: false, 
        			title		: (type == 0 ? 'Vložte požadavek na zálohu' : 'Vložte požadavek na VVH'),
        			languages	: false,
        			type		: 'AJAX',
                    ajax_url	: '/employees/add_down_payments/'+type+'/',
                    post_data	: {
                            save:0,
                            limit:payment_limit,
                            celkem_hodin:$('ClientWorkingHourCelkemHodin').value,
                            company_id:$('ClientWorkingHourCompanyId').value,
                            connection_client_requirement_id:$('ClientWorkingHourConnectionClientRequirementId').value,
                            year:$('ClientWorkingHourYear').value,
                            month:$('ClientWorkingHourMonth').value,
                            client_id:$('ClientWorkingHourClientId').value,
                            stat_id:stat_id
                    },
        			closeConfirm: false,
        			max_minBtn	: false,
        			modal_close	: true,
        			remove_scroll: false
                });
        });     
    }
    
    
    /**
     * kontrola mzdy celkem
     * pokud jedna z mezd celkem ps-d-zl-c je vetsi jak nula
     * a mzda celkem je nula aktivuj prepocet znova
     */
     function check_salary_sum(){
        var part_1 = $('ClientWorkingHourSalaryPart1P').value.toFloat();
		var part_2 = $('ClientWorkingHourSalaryPart2P').value.toFloat();
		var part_3 = $('ClientWorkingHourSalaryPart3P').value.toFloat();
		var part_4 = $('ClientWorkingHourSalaryPart4P').value.toFloat();
		var sum = $('ClientWorkingHourSalaryPerHourP').value.toFloat();
        
        /**
         * pokud je jedna z castek vyssi jak nula a celkova je nula byl chybny prepocet
         * prepocet vyvolame znova
         */
        if((part_1 > 0 || part_2 > 0 || part_3 > 0 || part_4 > 0) && sum == 0){
            alert('Chybně uložená docházka! Právě byl proveden přepočet mzdy celkem. Prosím uložte docházku.');
            calculate_salary_cost(true);
        }
     }
     //start check
     check_salary_sum(); 
     
     
     /**
      * kontrola maximalek
      * kdyby doslo k preneseni chybne hodinovky a byla vyssi nez maximalka aktualni
      */   
      function check_salary_max(){
        var sumary = 0;
        
            $$('.salary').each(function(el){
				if (el.value.toFloat() != 0){
				    var max = ((el.id)+'Max');

            		if(el.value.toFloat() > $(max).value.toFloat()){
            			alert('Překročili jste maximální hodnotu! Bude nastavena maximální hodnota.');
            			el.value=$(max).value.toFloat();
                        
                        
            		}
                    
                    if (el.value != '' && el.id !='ClientWorkingHourOdmena2' && el.id !='ClientWorkingHourOdmena1')
                        sumary += el.value.toFloat();
				}
			});     

			$('ClientWorkingHourSalaryPerHour').value = sumary;
            
            calculate_salary_cost();
      }
      //start check
      check_salary_max();
    
    
    
    /**
     * zavolani domwinu pro pridani problemu
     */
     if($('add_misconducts')){
    	$('add_misconducts').addEvent('click', function(e){
    		new Event(e).stop();
                        
            	domwin.newWindow({
        			id			: 'domwin_add_misconducts',
        			sizes		: [600,320],
        			scrollbars	: false, 
        			title		: 'Vložte negativní hodnocení',
        			languages	: false,
        			type		: 'AJAX',
                    ajax_url	: '/misconducts/add/',
                    post_data	: {
                            client_id:$('ClientWorkingHourClientId').value,
                            year:$('ClientWorkingHourYear').value,
                            month:$('ClientWorkingHourMonth').value
                    },
        			closeConfirm: false,
        			max_minBtn	: false,
        			modal_close	: true,
        			remove_scroll: false
                });
        });     
    }
    
    function load_details_for_cw_template(id){
        var smennost_setting = $('ClientWorkingHourSettingShiftWorkingId').value;
        
        /**
         * Je treba hlidat datum ukonceni, aby sablona nevyplnovala uz dny kdy uz nebude pracovat!
         */
        var datum_propusteni = $('ConnectionClientRequirementTo').getHTML();
        datum_propusteni = datum_propusteni.split('-');
        var check_day = this_month = false;
        if(datum_propusteni[0].toInt() == $('ClientWorkingHourYear').value.toInt() && datum_propusteni[1].toInt() == $('ClientWorkingHourMonth').value.toInt())
            check_day = true;
            
        today = new Date();    
        if(today.getMonth()+1 == $('ClientWorkingHourMonth').value.toInt()){
            check_day = this_month = true;
        }
           
        $each($$('input.hodiny'),function(hod){hod.value = 0;});//reset hodin
        if(id > 0){
            new Request.JSON({
				url: '/employees/load_cw_template_item/' + id + '/',
				onComplete: function (json){
					if (json){
						if (json.result === true){
						
							$each(json.data, function(val,key){
								var smennost = key.substr(1,1);
                                var den = key.substr(3,2).toInt();
                                if(smennost <= smennost_setting && 
                                    (
                                        (check_day == false)
                                        ||
                                        (check_day == true && this_month == true && den <= today.getDate())    
                                        || 
                                        (check_day == true && this_month == false && den <= datum_propusteni[2].toInt())
                                    )
                                ){
                                    $('ClientWorkingHourDays'+smennost+''+den).value = val;
                                }
	
							})
							recount_hours();
						} else {
							alert(json.message);
						}
					} else {
						alert('Systemova chyba!!!');
					}
				}
			}).send();
        }    
    }
    
    if($('ClientWorkingHourCompanyCwTemplateId')){
        $('ClientWorkingHourCompanyCwTemplateId').addEvent('change',function(e){
            new Event(e).stop();
            if(confirm('Vyplňené hodnoty v tabulce hodin, budou nahrazeny hodinami ze šablony, chcete pokračovat??'))
                load_details_for_cw_template(this.value);
        })
      
        if($('ClientWorkingHourId').value == ''){
            if($('ClientWorkingHourCompanyCwTemplateId').value != '')
                load_details_for_cw_template($('ClientWorkingHourCompanyCwTemplateId').value);
            else if(warning_about_cw_tempalte_miss == true){
                alert('Minulý měsíc jste používali šablonu pro docházku, pro tento měsíc jí nemáte nadefinovanou a proto nemůže být šablona použita.');
            }    
        }  
     }   
    

    if($('ClientWorkingHourFaDpcCalculation')){
        var money_item_title = $('ClientWorkingHourCompanyMoneyItemId').options[$('ClientWorkingHourCompanyMoneyItemId').selectedIndex].getProperty('title').split('|');
        var money_item_selected = money_item_title[0];
        var ignore_dpc = false;
        if($('ClientWorkingHourIgnoreDpc') && $('ClientWorkingHourIgnoreDpc').checked){ignore_dpc = true;}

        if(ignore_dpc === false && (["0010","0111"].indexOf(money_item_selected) != -1) && $('ClientWorkingHourYear').value.toInt() >= 2012 && ($('ClientWorkingHourYear').value.toInt() == 2012 && $('ClientWorkingHourMonth').value.toInt() >= 2) &&
            ($('ClientWorkingHourId').value == '' ||
                ($('ClientWorkingHourFaDpcCalculation').value == ''
                || $('ClientWorkingHourFaDpcCalculation').value == 0
                || $('ClientWorkingHourFaDpcCalculation').value <= 0
                )
            )
        ){
            if($('ClientWorkingHourFaDpcCalculation').value <= 0){
                alert('DPČ výpočet byl chybně uložen s mínusovou hodnotou '+$('ClientWorkingHourFaDpcCalculation').value+'! Výpočet proběhne znovu, následně na uplatnění změny, musíte opětovně uložit docházku!')
            }

            if (money_item_selected == '0010'){
                var rozmezi = 250;
                var max = 2500;
                var rozmezi_number = (Math.random()*rozmezi+1);
                if(rozmezi_number < 0){rozmezi_number = 100;}
                /**
                 * muze nabyvat rozmezi 2250 - 2500
                 */
                rozmezi_number = Math.round(rozmezi_number);
                value = ($('ClientWorkingHourFaDpc').value * 15);
                if(value > max){ value = max;}//limit
                if(value == 0 || value < 0){ value = 2400;}

                value = value - rozmezi_number;
            } else if (money_item_selected == '0111'){
                value = 1;
            }

            $('ClientWorkingHourFaDpcCalculation').value = value;


            calculate_salary_cost();
        } else if (
                ignore_dpc === false &&
                (["0010","0111"].indexOf(money_item_selected) != -1) &&
                $('ClientWorkingHourYear').value.toInt() >= 2012 &&
                $('ClientWorkingHourYear').value.toInt() == 2012 &&
                $('ClientWorkingHourMonth').value.toInt() >= 2 &&
                $('ClientWorkingHourCelkemHodin').value == 0)
            {
                /**
                 * Nastaveni DPC pokud je pocet 0 dochazky na 0;
                 */
                if ($('ClientWorkingHourFaDpcCalculation').value != 0){
                    alert('Klient má 0 odpracovaných hodin, DPČ bude nastaven na 0, následně na uplatnění změny, musíte opětovně uložit docházku!');
                    $('ClientWorkingHourFaDpcCalculation').value = 0;
                    calculate_salary_cost();
                }
        }
        else if((["0010","0111"].indexOf(money_item_selected) == -1)){
            $('dpc_box').addClass('none');
        }
    }
    
    if($('edit_dpc_value'))
        $('edit_dpc_value').addEvent('click',function(e){
            e.stop();
            if ($('ClientWorkingHourCelkemHodin').value == 0 || $('ClientWorkingHourCelkemHodin').value == ''){
                alert('Není možné nastavit DPČ, klient nemá odpracovaná žádné hodiny!!');
                $('ClientWorkingHourFaDpcCalculation').value = 0;
                calculate_salary_cost();
            } else {
                var value=prompt("Zadejte novou hodnotu DPČ výpočtu",$('ClientWorkingHourFaDpcCalculation').value.toFloat());
                if(value > 0){
                    $('ClientWorkingHourFaDpcCalculation').value = value;
                    calculate_salary_cost();
                }
                else{
                    alert('DPČ nemuže být nižšší než nula!');
                }
            }
        })
    
</script>