<script language="javascript" type="text/javascript">
	// domtabs init
	var domtab = new DomTabs({'className':'admin_dom'});
    var stat_id = <?php echo $stat_id;?>;
    
    // input limit
	$('NewClientWorkingHour_edit_formular').getElements('.integer, .float').inputLimit();
        
    //vypocet zakladni mzdy
    function zakladni_mzda(){
        var typ = ($('NewClientWorkingHourMzdaTyp').value == ''?0:$('NewClientWorkingHourMzdaTyp').value);
        var mzda = ($('NewClientWorkingHourZakladniMzda').value == ''?0:$('NewClientWorkingHourZakladniMzda').value);
        var pocet_hodin = ($('NewClientWorkingHourCelkemHodin').value == ''?0:$('NewClientWorkingHourCelkemHodin').value.toFloat());

        $('NewClientWorkingHourSalaryStart').value = myround(typ == 1 ? mzda * pocet_hodin : mzda);
    }
    zakladni_mzda();//na zacatku inicializujeme
    
    //prepocet celkem mzdy
    function calculate_salary_cost(){
		var pocet_hodin = $('NewClientWorkingHourCelkemHodin').value.toFloat();
        var zakladni_mzda = $('NewClientWorkingHourSalaryStart').value.toFloat();
        var celkem_mzda = bonusy = priplatky = srazky = 0;
        
        if($('NewClientWorkingHourSumBonus') && $('NewClientWorkingHourSumBonus').value != '')    
            bonusy = $('NewClientWorkingHourSumBonus').value.toFloat();
        if($('NewClientWorkingHourSumExtraPay') && $('NewClientWorkingHourSumExtraPay').value != '')    
            priplatky = $('NewClientWorkingHourSumExtraPay').value.toFloat();
        if($('NewClientWorkingHourSumDrawback') && $('NewClientWorkingHourSumDrawback').value != '')    
            srazky = $('NewClientWorkingHourSumDrawback').value.toFloat();
       
        celkem_mzda = myround((zakladni_mzda + bonusy + priplatky)-srazky);
		$('NewClientWorkingHourSalaryPerHourP').value = (stat_id == 1 ?  Math.floor(celkem_mzda) : celkem_mzda);
	};
    
    // function for reacound day/hours
	$$('.hodiny').addEvent('change', recount_hours);
	function recount_hours(){
		var NH = $('NewClientWorkingHourStandardHours').value.toFloat();
		var count = 0;
		var vikendy = 0;
        var sobota = 0;
        var nedele = 0;
		var svatky = 0;
        var prescas = 0;
        var prescas_vikendy = 0;
		var dovolena = 0;
        var dovolena_hour = 0;
		var neplacene_volno = 0;
        var neplacene_volno_hour = 0;
		var ocr = 0;
        var ocr_hour = 0;
		var neomluvena_absence = 0;
        var neomluvena_absence_hour = 0;
		var pracovni_neschopnost = 0;
        var pracovni_neschopnost_hour = 0;
		var nahradni_volno = 0;
        var nahradni_volno_hour = 0;
		var paragraf = 0;
        var paragraf_hour = 0;
        var dvanactky = 0;
		
		var s1 = 0, s2 = 0, s3 = 0;
		
		$$('.hodiny').each(function(item){
		    var hodnota = item.value;
		    if(hodnota.test('[A-ZČ]{1,3}[0-9]{1,2}(\.[0-9])?')){
              matches = hodnota.match('([A-ZČ]{1,3})([0-9]{1,2}\.?[0-9]?)?');
              hodnota = matches[1];
              action_hour = matches[2].toFloat();
		    }else{
		      action_hour = NH;
		    }
          
			switch(hodnota){
				case 'PN':
						pracovni_neschopnost ++;
                        pracovni_neschopnost_hour += action_hour;
					break;
				case 'OČR':
						ocr ++;
                        ocr_hour += action_hour;
					break;
				case 'D':
						dovolena ++;
                        dovolena_hour += action_hour;
					break;
				case 'NV':
						neplacene_volno ++;
                        neplacene_volno_hour += action_hour;
					break;
				case 'A':
						neomluvena_absence ++;
                        neomluvena_absence_hour += action_hour;
					break;
				case 'V':
						nahradni_volno ++;
                        nahradni_volno_hour += action_hour;
					break;
				case 'PG':
						paragraf ++;
                        paragraf_hour += action_hour;
					break;
				default:
					if (item.value == '') item.value = 0;
					
					if (item.hasClass('s1')) s1 += item.value.toFloat();
					if (item.hasClass('s2')) s2 += item.value.toFloat();
					if (item.hasClass('s3')) s3 += item.value.toFloat();
					
					// celkem
					count += item.value.toFloat();

					// vikend
					if (item.hasClass('vikend')){ vikendy += item.value.toFloat();}   
                    // sobota
					if (item.hasClass('sobota')) sobota += item.value.toFloat();
                    // nedele
					if (item.hasClass('nedele')) nedele += item.value.toFloat();
					// svatky
					if (item.hasClass('svatky')) svatky += item.value.toFloat();
                    
					// prescasy rozdeleny zvlast
					if (item.value.toFloat() > NH){
					   if (item.hasClass('vikend')) 
					       prescas_vikendy += (item.value.toFloat() - NH);
                       else    
					       prescas += (item.value.toFloat() - NH);
                    }
                    
                    // dvanactky
					if (item.value.toFloat() >= 12){ 
					   dvanactky += item.value.toFloat();
                    }
                    
					break;
			}
		});
	
        $('NewClientWorkingHourCelkemHodin').value = count;
		$('NewClientWorkingHourSvatky').value = svatky;
		$('NewClientWorkingHourPrescasy').value = prescas;
        $('NewClientWorkingHourPrescasyVikend').value = prescas_vikendy;
		$('NewClientWorkingHourVikendy').value = vikendy;
        $('NewClientWorkingHourSobota').value = sobota;
        $('NewClientWorkingHourNedele').value = nedele;
		$('NewClientWorkingHourDovolena').value = dovolena;
        $('NewClientWorkingHourDovolenaHour').value = dovolena_hour;
		$('NewClientWorkingHourNeplaceneVolno').value = neplacene_volno;
        $('NewClientWorkingHourNeplaceneVolnoHour').value = neplacene_volno_hour;
		$('NewClientWorkingHourOcestrovaniClenaRodiny').value = ocr;
        $('NewClientWorkingHourOcestrovaniClenaRodinyHour').value = ocr_hour;
		$('NewClientWorkingHourNeomluvenaAbsence').value =  neomluvena_absence;
        $('NewClientWorkingHourNeomluvenaAbsenceHour').value =  neomluvena_absence_hour;
		$('NewClientWorkingHourPracovniNeschopnost').value =  pracovni_neschopnost;
        $('NewClientWorkingHourPracovniNeschopnostHour').value =  pracovni_neschopnost_hour;
		$('NewClientWorkingHourNahradniVolno').value =  nahradni_volno;
        $('NewClientWorkingHourNahradniVolnoHour').value =  nahradni_volno_hour;
		$('NewClientWorkingHourParagraf').value =  paragraf;
        $('NewClientWorkingHourParagrafHour').value =  paragraf_hour;
        $('NewClientWorkingHourDvanactky').value = dvanactky;
    		
		$('NewClientWorkingHourHourS1').value = s1;
		$('NewClientWorkingHourHourS2').value = s2;
		$('NewClientWorkingHourHourS3').value = s3;
        
		zakladni_mzda();		
        count_money_item_block();
	};
    
    
    /**
     * Funkce prepocitava blok - bonusus, priplatku a srazek
     * vypocitava jednotlive hodnoty, definuje castky prepoctene za mesic, a sumu z ajednotlivy blok
     * ktera se pak bude pricitat nebo odecitat do vyplatni pasky
     */
    function count_money_item_block(){
        var main_block = $('money_item_setting_box');
        var normo_hodina = $('NewClientWorkingHourStandardHours').value.toFloat();
        var celkem_hodin = $('NewClientWorkingHourCelkemHodin').value.toFloat();
        var zhm = $('NewClientWorkingHourSalaryStart').value.toFloat();
        var typ_sum_id = ['SumBonus','SumExtraPay','SumDrawback'];
        var phm = $('NewClientWorkingHourAverageHourSalary').value.toFloat();
        
        //Prochazime jednotlive bloky(tabulky)
        $each(main_block.getElements('table'),function(table){
             var table_typ = table.getProperty('rel');
             //list typu !!!POZOR!!! nastavuje se v select_configu!
             //table_typ[0,2]   1=>'Měsíčně',2=>'Hodinovka',3=>'% z ZHM',4=>'maximálka'
             //table_typ[1]   1=>'% z ZHM',2=>'Hodinovka'
            
            /** 
              * V dane tabulce vyhledame vsechny prvky - one_item
              * jedna se o prvky ktere reprezentuji urcitou hodnotu,
              * a k nim pak nalezneme, typ, a mesicni castku
              */
              var celkova_castka_bloku = 0;
              
              $each(table.getElements('input.one_item'),function(item){
                   var procenta = false;
                   var item_value = item.value;
                   if(item.value.substr(-1) == '%'){
                      item_value = item.value.replace("%","");
                      procenta = true;
                   }
                
                   var hodnota = item_value.toFloat();
                   item.id = item.id.replace(new RegExp("[Value]+$", "g"), "");
                   var name = $(item.id+'Name').value;
                   var typ = $(item.id+'Typ').value;
                   var phm_in_result = $(item.id+'Phm').value;
                   var max = $(item.id+'Max').value.toFloat();
                   var yn = ($(item.id+'Yn').checked ? 1 : 0);
                   var mesicne = 0;
           
                   //connections
                   var con_col = $(item.id+'ConCol').value;
                   var con_typ = $(item.id+'ConTyp').value;
           
                   if(yn == 1){//pocitame pouze ty zaskrtnute
                      if(table_typ == 1){//Priplatky
                         switch(typ){
                            case '1':  mesicne = (zhm/100)*hodnota;  break;
                            case '2':  
                                if(con_col != '' && con_typ == 1){
                                    if($('NewClientWorkingHour'+(con_col.camelCase2().capitalize())))
                                        celkem_hodin = $('NewClientWorkingHour'+(con_col.camelCase2().capitalize())).value.toFloat();
                                }
                              
                                mesicne = hodnota*celkem_hodin;  
                            break;
                            case '5':  
                                if($('NewClientWorkingHour'+(con_col.camelCase2().capitalize())) && $('NewClientWorkingHour'+(con_col.camelCase2().capitalize())).value != '')
                                   udalost_count = $('NewClientWorkingHour'+(con_col.camelCase2().capitalize())).value.toFloat();
                                else
                                   udalost_count = 0;
                                   
                                if(con_col != '' && con_typ == 2){
                                     mesicne = hodnota*udalost_count;
                                }
                                else{
                                     mesicne = udalost_count*(phm_in_result == true ? phm : 1);
                                     if(procenta == true){ mesicne = (mesicne * hodnota)/100} //procento z nejakeho priplatku
                                     else if(hodnota > 0){mesicne = mesicne * hodnota};
                                }        
                            break;
                         }   
                      }
                      else{//Srazky, Bonusy
                         switch(typ){
                            case '1':  mesicne = hodnota;  break;
                            case '2':  
                                if(con_col != '' && con_typ == 1){
                                    if($('NewClientWorkingHour'+(con_col.camelCase2().capitalize())))
                                        celkem_hodin = $('NewClientWorkingHour'+(con_col.camelCase2().capitalize())).value.toFloat();
                                }
                                
                                mesicne = hodnota*celkem_hodin;  
                            break;
                            case '3':  mesicne = (zhm/100)*hodnota;  break;
                            case '4':  
                                if(hodnota > max){item.value = hodnota = max; alert(name+ ' je jako typ maximálka, překročily jste nastavené maximum('+max+'), vaše hodnota byla přepsána tímto maximem.');}  
                                mesicne = hodnota;
                            break;
                            case '5':  
                                if($('NewClientWorkingHour'+(con_col.camelCase2().capitalize())) && $('NewClientWorkingHour'+(con_col.camelCase2().capitalize())).value != '')
                                   udalost_count = $('NewClientWorkingHour'+(con_col.camelCase2().capitalize())).value.toFloat();
                                else
                                   udalost_count = 0;
                                    
                                if(con_col != '' && con_typ == 2){
                                     mesicne = hodnota*udalost_count;
                                }
                                else{
                                     mesicne = udalost_count*(phm_in_result == true ? phm : 1);
                                     if(procenta == true){ mesicne = (mesicne * hodnota)/100} //procento z nejakeho priplatku
                                     else if(hodnota > 0){mesicne = mesicne * hodnota};
                                }           
                            break;
                            
                         }
                      }
                   }
                   
                   celkova_castka_bloku += mesicne.toFloat();;
                   $(item.id+'Monthly').value = myround(mesicne);
              });

              $('NewClientWorkingHour'+typ_sum_id[table_typ]).value = myround(celkova_castka_bloku); 
              table.getElement('span.sum_of_all').setHTML(myround(celkova_castka_bloku));
        });
        		
		calculate_salary_cost();
    }
    count_money_item_block();

    //prirazeni event, checkboxu ve srazkach, bonusech a priplatcich, a provest cely prepocet
    $$('.yn_setting').addEvent('click',function(e){
        var el = this.getParent('tr').getElement('input.one_item');
        if(this.checked){
            el.addEvent('change',count_money_item_block);
            el.removeClass('read_info');
        }
        else{
            el.removeEvent('change');
            el.addClass('read_info');
        }
        count_money_item_block();
    })
    //ty co jsou zakrtnute po najeti, prirad jim change funkci
    $each($('money_item_setting_box').getElements('input.one_item'),function(input){
        if(!input.hasClass('read_info')){input.addEvent('change',count_money_item_block);}
    })
   
    //hromadne zaskrtnuti
	$('accommodation_days_all').addEvent('click', my_select_all.bindWithEvent(this,['accommodation_day']));
	$('food_days_all').addEvent('click', my_select_all.bindWithEvent(this,['food_day']));
	
    // condition for readonly for use_accommodation
	$('NewClientWorkingHourAccommodationId').addEvent('change', function(){
		if (this.value != 0){
			$('NewClientWorkingHour_edit_formular').getElements('.accommodation_day:enabled').setProperty('checked','checked');

		} else {
			$('NewClientWorkingHour_edit_formular').getElements('.accommodation_day:enabled').removeProperty('checked');
		}
		set_accommodation(this.value);
        recount_checked_accomondation();
	});
    
    // change use accommodation
	function set_accommodation(e){
		if (e == 0){
			$('NewClientWorkingHour_edit_formular').getElements('.accommodation_day').removeProperty('checked');
			$('NewClientWorkingHour_edit_formular').getElement('.tr_accommodation').addClass('none');
		} else {
			$('NewClientWorkingHour_edit_formular').getElement('.tr_accommodation').removeClass('none');
		}
	};

	// fce na hromadne zaskrtnuti
	function my_select_all(e,className){
		var event = new Event(e);
		event.stop();
		obj = event.target;

		if (!obj.zaskrkly){
			$('NewClientWorkingHour_edit_formular').getElements('.'+className+':enabled').setProperty('checked','checked');
			obj.zaskrkly = true;
		} else {
			$('NewClientWorkingHour_edit_formular').getElements('.'+className).removeProperty('checked');
			try {
				delete obj.zaskrkly;
			} catch (e){
				obj.zaskrkly = null;
			}
		}
        
        recount_checked_accomondation();
	}
    
    
    $('NewClientWorkingHour_edit_formular').getElements('.accommodation_day:enabled').addEvent('change',recount_checked_accomondation);
    function recount_checked_accomondation(){
            var acc_days = 0;
        	$each($('NewClientWorkingHour_edit_formular').getElements('.accommodation_day:enabled'),function(item){
        	     if(item.checked){acc_days++;}
        	});

            $('NewClientWorkingHourAccommodationDaysCount').value = acc_days;
            count_money_item_block();
    }
    
    
    	// save button
    if($('only_save'))
    	$('only_save').addEvent('click', function(e){
    		new Event(e).stop();

            $each($$('.odpracovane_hodiny').getElements('input'),function(item){
                item.removeProperty('disabled');
            })
    
            //prepocteni hodin pred save
            recount_hours();
    
            button_preloader($('only_save'));
    		new Request.JSON({
    			url: $('NewClientWorkingHour_edit_formular').getProperty('action'),
    			onComplete: function(json){
    				if (json){
    					if (json.result === true){
                                click_refresh($('ConnectionClientRequirementId').value);
                                domwin.closeWindow('domwin_zamestnanci_hodiny');
    					} else {
    						alert(json.message);
    					}
    				} else {
    					alert('Chyba aplikace!');
    				}
                    
                    button_preloader_disable($('only_save'));
    			}
    		}).send($('NewClientWorkingHour_edit_formular'));
    });
    
    $('only_close').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_zamestnanci_hodiny');
	});    

    // add event dblclick on textinput
	$('NewClientWorkingHour_edit_formular').getElements('.hodiny:enabled').addEvent('dblclick', function(){
		// call
		var obj = this;
		domwin.newWindow({
			id			: 'domwin_hodiny_od_do',
			sizes		: [300,170],
			scrollbars	: true, 
			title		: 'Vložte čas práce',
			languages	: false,
			type		: 'DOM',
			dom_id		: $('dblclick_input'),
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true,
			remove_scroll: false,
			dom_onRender: function(){
			    var NH = $('NewClientWorkingHourStandardHours').value.toFloat();
				var choose_select = $('domwin_hodiny_od_do').getElement('.choose_input');
                var hour_info = $('domwin_hodiny_od_do').getElement('.hour_info');
                  var action_hour = $('domwin_hodiny_od_do').getElement('.action_hour');
				
                var choose_do_it  = $('domwin_hodiny_od_do').getElement('.choose_do_it');
				var choose_do_div = $('domwin_hodiny_od_do').getElement('.choose_do_div');
				var choose_od_div = $('domwin_hodiny_od_do').getElement('.choose_od_div');
				
				choose_select.addEvent('change', function(){
					if (this.value == 1){
						choose_do_div.removeClass('none');
						choose_od_div.removeClass('none');
                        hour_info.addClass('none');
					} 
                    else {
						choose_do_div.addClass('none');
						choose_od_div.addClass('none');
                        hour_info.removeClass('none');
                        action_hour.value = NH;
					}
                    
                    if (this.value == 'D'){
						$('domwin_hodiny_od_do').getElement('.dovolena_info').removeClass('none');
                        $('domwin_hodiny_od_do').getElement('.dovolena_info').getChildren('var').setHTML($('NewClientWorkingHourZbyvaDovolene').value);
                    }    
                    else    
                        $('domwin_hodiny_od_do').getElement('.dovolena_info').addClass('none');
				});
				
				choose_do_it.addEvent('click', function(e){
					new Event(e).stop();
					
					var choose_od = $('domwin_hodiny_od_do').getElement('.choose_od_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_od_m').value;
					var choose_do = $('domwin_hodiny_od_do').getElement('.choose_do_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_do_m').value;
				
					if (choose_select.value == 1)
						obj.value = compare_times(choose_od,choose_do);					
					else {
					    var action_hours = '';
                        if(action_hour.value != NH)
                            action_hours = action_hour.value;
                        
						obj.value = choose_select.value+action_hours;

                        if(obj.value == 'D'){
                            var pocet = $('domwin_hodiny_od_do').getElement('.dovolena_info').getChildren('var').getHTML();
                            
                            if(pocet == 0 || pocet < 0)
                                alert('Nárok na dovolenou jde do záporných čísel!');
                            
                            pocet--;
                            $('NewClientWorkingHourZbyvaDovolene').value = pocet; 
                        }
					}
					domwin.closeWindow('domwin_hodiny_od_do');
					recount_hours();
				});
			}
		});
	});
	
	function compare_times(time1, time2){
		var s1 = time1.split(':'), s2 = time2.split(':'), td;
		t1 = new Date(1970, 0, 0, s1[0] ? s1[0] : 0, s1[1] ? s1[1] : 0, s1[2] ? s1[2] : 0);
		t2 = new Date(1970, 0, ((s2[0] < s1[0]) || (s2[0]==s1[0] && s2[1] < s1[1]))?1:0, s2[0] ? s2[0] : 0, s2[1] ? s2[1] : 0, s2[2] ? s2[2] : 0);
		td = (t2 - t1)/60/1000;
		//zbytek = ((td % 60) < 10)?'0'+(td % 100):
		zbytek = (td % 60);
		zbytek = (1 / (60 / zbytek));
		output = (td / 60).toInt() + zbytek;
		return output;
	}
    
    function myround(num){return Math.round(num * 100) / 100;}

    // change work shift
	$('NewClientWorkingHourSettingShiftWorkingId').addEvents({
		'change': function(e){
			this.dont_change_focus = true;
			if (confirm('Opravdu si přejete změnit typ směn, může dojít ke ztrátě nadefinovaných počtů hodin?')){
				// define new selectedIndex
				this.currentSelection = this.selectedIndex;
				var count_shift = this.value;
				switch (count_shift){
					case '1':
						$('NewClientWorkingHour_edit_formular').getElements('.tr_smennost_2, .tr_smennost_3').addClass('none');
						$('NewClientWorkingHour_edit_formular').getElement('.tr_smennost_2').getElements('input').setValue('');
						$('NewClientWorkingHour_edit_formular').getElement('.tr_smennost_3').getElements('input').setValue('');
						break;
					case '2':
						$('NewClientWorkingHour_edit_formular').getElement('.tr_smennost_2').removeClass('none');
						$('NewClientWorkingHour_edit_formular').getElement('.tr_smennost_3').addClass('none');
						$('NewClientWorkingHour_edit_formular').getElement('.tr_smennost_3').getElements('input').setValue('');
						break;
					case '3':
						$('NewClientWorkingHour_edit_formular').getElements('.tr_smennost_2, .tr_smennost_3').removeClass('none');
						break;
					default:
						alert('Neznamy typ smeny');
						break;
				}
				recount_hours();
			} else {
				new Event(e).stop();
				this.options[this.currentSelection].selected = 'selected';
			}
			this.dont_change_focus = false;
		},
		'focus': function(e){
			// add hack for cancel confirm, its select default selectedIndex
			if (!this.dont_change_focus || this.dont_change_focus == false){
				this.currentSelection = this.selectedIndex;
			}
		}
	});
    
    /**
     * jiz proplaceno
     */
    var payment_sum = <?php echo $payment_sum;?>;
    var zbyva_k_vyplate_ = $('NewClientWorkingHourSalaryPerHourP').value - payment_sum;
    
    $('ChangeMaxZalohyNow').value = (stat_id == 1 ? Math.floor(zbyva_k_vyplate_) : zbyva_k_vyplate_);
    
    /**
     * zavolani domwinu pro vytvoreni pozadavku pro zalohu
     */
     if($$('.add_down_payments')){
    	$$('.add_down_payments').addEvent('click', function(e){
    		new Event(e).stop();
            
            var type = this.getProperty('rel');
            
            /**
             * limit k propalceni
             */
            var payment_limit =  $('ChangeMaxZalohyNow').value;
            
            	domwin.newWindow({
        			id			: 'domwin_down_payments',
        			sizes		: [350,185],
        			scrollbars	: false, 
        			title		: (type == 0 ? 'Vložte požadavek na zálohu' : 'Vložte požadavek na VVH'),
        			languages	: false,
        			type		: 'AJAX',
                    ajax_url	: '/new_employees/add_down_payments/'+type+'/',
                    post_data	: {
                            save:0,
                            limit:payment_limit,
                            celkem_hodin:$('NewClientWorkingHourCelkemHodin').value,
                            company_id:$('NewClientWorkingHourCompanyId').value,
                            connection_client_requirement_id:$('NewClientWorkingHourConnectionClientRequirementId').value,
                            year:$('NewClientWorkingHourYear').value,
                            month:$('NewClientWorkingHourMonth').value,
                            client_id:$('NewClientWorkingHourClientId').value,
                            stat_id:stat_id
                    },
        			closeConfirm: false,
        			max_minBtn	: false,
        			modal_close	: true,
        			remove_scroll: false
                });
        });     
    }
</script>