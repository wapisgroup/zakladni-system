<?php 
		
	$start 	= $this->data['ConnectionClientRequirement']['from'];
	$end 	= ($this->data['ConnectionClientRequirement']['to']== '0000-00-00')?date("Y-m-d"):$this->data['ConnectionClientRequirement']['to'];	
	
	$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
	$current_day = $last_day;
	$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );
	
	// render list of disabled fields
	$not_to_disabled = array();
	for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
		$curr_date = $year.'-'.$month.'-'.((strlen($i) == 1)?'0'.$i:$i);
		if (strtotime($start) <= strtotime($curr_date) && strtotime($end) >= strtotime($curr_date))
			$not_to_disabled[] = $i;
	}

?>
<input type='hidden' value='0' id='made_change_in_form' />
<form action='/new_employees/odpracovane_hodiny/' method='post' id='NewClientWorkingHour_edit_formular'>
	<!-- Basic Hidden Fields -->
	
	<?php echo $htmlExt->hidden('NewClientWorkingHour/id');?>
	<?php echo $htmlExt->hidden('NewClientWorkingHour/company_id');?>
	<?php echo $htmlExt->hidden('NewClientWorkingHour/client_id');?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/new_money_item_id');?>
	<?php echo $htmlExt->hidden('NewClientWorkingHour/fakturace');?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/mzda_typ');?>
	<?php echo $htmlExt->hidden('NewClientWorkingHour/requirements_for_recruitment_id');?>
	<?php echo $htmlExt->hidden('NewClientWorkingHour/connection_client_requirement_id');?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/company_work_position_id');?>
	<?php echo $htmlExt->hidden('NewClientWorkingHour/year',array('value'=>$year));?>
	<?php echo $htmlExt->hidden('NewClientWorkingHour/month',array('value'=>$month));?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/stav');?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/zbyva_dovolene',array('value'=>$zbyva_dovolene)); ?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/sum_bonus');?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/sum_extra_pay');?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/sum_drawback');?>
    <?php echo $htmlExt->hidden('NewClientWorkingHour/accommodation_days_count');?>

    <div class="sll3">
     		<?php 
    					$accommodation_list = array(0=>'Nezvoleno') + $accommodation_list;
    
    					if($ubytovani_select)
    						echo $htmlExt->selectTag('NewClientWorkingHour/accommodation_id',$accommodation_list,null,array('label'=>'Ubytování'),false);//,'title'=>(array('0|0|0') + $accommodation_title_list)),false);
    					else 
    						echo $htmlExt->hidden('NewClientWorkingHour/accommodation_id').'<br />';
    		?><br/>
    </div>
    <div class="sll3">
        <?php echo $htmlExt->selectTag('NewClientWorkingHour/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost'),null,false);?> <br /> 
				
    </div>

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
            <li class="ousko"><a href="#krok1">Docházka</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field" >
			<table class='table odpracovane_hodiny'>	
				<tr>
					<th>Den</th>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<th <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend'";
						?>>
							<?php echo $dny[$current_day];?>
							<br />
							<?php echo ($i+1);?>
							<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
							
						</th>
					<?php endfor;?>
				</tr>
				<?php 
					$smena_captions = array(
						1	=> 'Ranní',
						2	=> 'Odpolední',
						3	=> 'Noční'
					);
				?>
				<?php for($k = 1; $k <= 3; $k++):?>
				<tr class='tr_smennost_<?php echo $k;?> <?php 
						if (($k == 2 && $this->data['NewClientWorkingHour']['setting_shift_working_id'] == 1) || 
							($k == 3 && in_array($this->data['NewClientWorkingHour']['setting_shift_working_id'],array(1,2)))
							|| (empty($this->data['NewClientWorkingHour']['setting_shift_working_id']))
							) { echo 'none'; }?>'>	
					<td><?php echo $smena_captions[$k];?></td>
					<?php $current_day = $last_day;?>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td <?php 
							if (in_array(($i+1),$svatky)) 
								echo "class='svatky'";
							else if (in_array($current_day,array(6,7))) 
								echo "class='vikend ".($current_day == 6?"sobota":"nedele")."'";
						?>>
						<?php 
							if (in_array(($i+1),$svatky)){ 
								$class =  " svatky";
							} else if (in_array($current_day,array(6,7))){
								$class = " vikend ".($current_day == 6?"sobota":"nedele");
							} else {
								$class = "";
							}
                            
						?>
						<?php echo $htmlExt->input('NewClientWorkingHour/days/'. '_' .$k. '_' .($i+1),array('class'=>'hodiny integer s'.$k.' '.$class,'disabled'=>(!in_array(($i+1),$not_to_disabled) || (isset($this->data['NewClientWorkingHour']['days']['_' .$k. '_' .($i+1)]) && $this->data['NewClientWorkingHour']['days']['_' .$k. '_' .($i+1)] == 'A') )?'disabled':null));?></td>
						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
					<?php endfor;?>
				</tr>
				<?php endfor;?>
				<tr class='tr_food_ticket <?php if (!isset($this->data['NewClientWorkingHour']['stravenka']) || $this->data['NewClientWorkingHour']['stravenka'] == 0) echo 'none';?>'>
					<td>Stravenky - <a href="#" id="food_days_all">vše</a></td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('NewClientWorkingHour/food_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'food_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
				<tr class='tr_accommodation <?php if (!isset($this->data['NewClientWorkingHour']['accommodation_id']) || $this->data['NewClientWorkingHour']['accommodation_id'] == 0) echo 'none';?>'>
					<td>Ubytování <a href="#" id="accommodation_days_all">vše</a></td>
					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
						<td><?php echo $htmlExt->checkbox('NewClientWorkingHour/accommodation_days/'. '_' .$k. '_' .($i+1),null,array('class'=>'accommodation_day','disabled'=>(!in_array(($i+1),$not_to_disabled))?'disabled':null));?></td> 
					<?php endfor;?>
				</tr>
			</table>
			<br />
            <div class="sll">
                <fieldset>
    				<legend>Docházka - součtové řádky</legend>
                        <?php echo $htmlExt->input('NewClientWorkingHour/celkem_hodin',array('readonly'=>'readonly','label'=>'Celkem hodin','class'=>'read_info'));?>
    					<?php for($k = 1; $k <= 3; $k++):?>
    					   <?php echo $htmlExt->input('NewClientWorkingHour/hour_s'.$k,array('readonly'=>'readonly','label'=>$smena_captions[$k],'class'=>'read_info '.((($k == 2 && $this->data['NewClientWorkingHour']['setting_shift_working_id'] == 1) || ($k == 3 && in_array($this->data['NewClientWorkingHour']['setting_shift_working_id'],array(1,2))))?'none':''),'label_class'=>((($k == 2 && $this->data['NewClientWorkingHour']['setting_shift_working_id'] == 1) || ($k == 3 && in_array($this->data['NewClientWorkingHour']['setting_shift_working_id'],array(1,2))))?'none':'')));?>
    					<?php endfor;?>
                        <?php echo $htmlExt->input('NewClientWorkingHour/dovolena',array('readonly'=>'readonly','label'=>'Dovolena','class'=>'read_info short'));?>
                        <?php echo $htmlExt->input('NewClientWorkingHour/dovolena_hour',array('readonly'=>'readonly','class'=>'read_info short'));?>hod<br class="clear" />
                        
    				   	<?php echo $htmlExt->input('NewClientWorkingHour/neplacene_volno',array('readonly'=>'readonly','label'=>'Neplacené volno','class'=>'read_info short'));?>
                       	<?php echo $htmlExt->input('NewClientWorkingHour/neplacene_volno_hour',array('readonly'=>'readonly','class'=>'read_info short'));?>hod<br class="clear" />
                        
                        <?php echo $htmlExt->input('NewClientWorkingHour/neomluvena_absence',array('readonly'=>'readonly','label'=>'Absence','class'=>'read_info short'));?>
    				    <?php echo $htmlExt->input('NewClientWorkingHour/neomluvena_absence_hour',array('readonly'=>'readonly','class'=>'read_info short'));?>hod<br class="clear" />
                        
                        <?php echo $htmlExt->input('NewClientWorkingHour/prescasy',array('readonly'=>'readonly','label'=>'Přesčasy','class'=>'read_info'));?>				
                        <?php echo $htmlExt->input('NewClientWorkingHour/prescasy_vikend',array('readonly'=>'readonly','label'=>'Přesčasy víkend','class'=>'read_info'));?>
                        <?php echo $htmlExt->input('NewClientWorkingHour/svatky',array('readonly'=>'readonly','label'=>'Svátky','class'=>'read_info'));?>
    					<?php echo $htmlExt->input('NewClientWorkingHour/vikendy',array('readonly'=>'readonly','label'=>'Víkendy','class'=>'read_info'));?>
 				        <?php echo $htmlExt->input('NewClientWorkingHour/sobota',array('readonly'=>'readonly','label'=>'Sobota','class'=>'read_info'));?>
 				        <?php echo $htmlExt->input('NewClientWorkingHour/nedele',array('readonly'=>'readonly','label'=>'Neděle','class'=>'read_info'));?>
 				        <?php echo $htmlExt->input('NewClientWorkingHour/dvanactky',array('readonly'=>'readonly','label'=>'12.hod nad čas','class'=>'read_info'));?>
 				       
                        
                        <?php echo $htmlExt->input('NewClientWorkingHour/nahradni_volno',array('readonly'=>'readonly','label'=>'Náhradní volno','class'=>'read_info short'));?>
                    	<?php echo $htmlExt->input('NewClientWorkingHour/nahradni_volno_hour',array('readonly'=>'readonly','class'=>'read_info short'));?>hod<br class="clear" />
                        
                        <?php echo $htmlExt->input('NewClientWorkingHour/ocestrovani_clena_rodiny',array('readonly'=>'readonly','label'=>'OČR','class'=>'read_info short'));?>
    					<?php echo $htmlExt->input('NewClientWorkingHour/ocestrovani_clena_rodiny_hour',array('readonly'=>'readonly','class'=>'read_info short'));?>hod<br class="clear" />
                        
                        <?php echo $htmlExt->input('NewClientWorkingHour/pracovni_neschopnost',array('readonly'=>'readonly','label'=>'Pracovní neschopnost','class'=>'read_info short'));?>
    					<?php echo $htmlExt->input('NewClientWorkingHour/pracovni_neschopnost_hour',array('readonly'=>'readonly','class'=>'read_info short'));?>hod<br class="clear" />
                        
                        <?php echo $htmlExt->input('NewClientWorkingHour/paragraf',array('readonly'=>'readonly','label'=>'Paragraf','class'=>'read_info short'));?>
                        <?php echo $htmlExt->input('NewClientWorkingHour/paragraf_hour',array('readonly'=>'readonly','class'=>'read_info short'));?>hod<br class="clear" />
                        
                </fieldset>
                <fieldset>
    				<legend>Nastaveni</legend>
                    <?php echo $htmlExt->input('NewClientWorkingHour/standard_hours',array('readonly'=>'readonly','label'=>'Denní pracovní fond','class'=>'read_info'));?><br />
				    <?php echo $htmlExt->input('NewClientWorkingHour/pocet_dni_dovolene_za_rok',array('readonly'=>'readonly','label'=>'Počet dní dovolené za rok','class'=>'read_info'));?><br />
			        <?php echo $htmlExt->input('NewClientWorkingHour/stravenky',array('readonly'=>'readonly','label'=>'Stravenky','class'=>'read_info'));?><br />
                    <?php echo $htmlExt->input('NewClientWorkingHour/stravne',array('readonly'=>'readonly','label'=>'Stravné na měsíc','class'=>'read_info'));?><br />
	                <?php echo $htmlExt->input('NewClientWorkingHour/average_hour_salary',array('label'=>'Průměrná hod. mzda','class'=>''));?><br />
				    
                </fieldset>    
                
                
                  	<?php echo $htmlExt->input('NewClientWorkingHour/salary_per_hour_p',array('label'=>'Celkem mzda', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
                    <?php echo $htmlExt->input('change_max_zalohy_now',array('label'=>'Zbývá k výplatě', 'readonly'=>'readonly','class'=>'read_info'));?><br/>
		
                <br />
                <div id="payment_list_box">
                    <div id="payment_list_box_items">
                    <?php 
                    echo "&nbsp;Zálohy:<br class='clear_left' />";
                    foreach($payment_list as $item){
                        $out = null;
                        $out .= '<strong>'.$fastest->czechDate($item['ReportDownPayment']['created']).'</strong>';
                        $out .= '&nbsp;&nbsp;&nbsp;'.$item['CmsUser']['name'];
                        $out .= '&nbsp;&nbsp;&nbsp;částka - '.$item['ReportDownPayment']['amount'];
                        $out .= '&nbsp;&nbsp;&nbsp;hodin - '.$item['ReportDownPayment']['celkem_hodin'];
                        $out .= '&nbsp;&nbsp;&nbsp; uhrazeno - '.$fastest->czechDate($item['ReportDownPayment']['date_of_paid']);
                        
                        $payment_sum += $item['ReportDownPayment']['amount'];
                        echo $out.'<br class="clear_left" />';
                    }
                    ?>
                    </div>
                </div>
        	</div>	
            <div id="money_item_setting_box" class="slr">	
 
            <fieldset>
    				<legend>Základní hrubá mzda</legend>
                    <?php echo $htmlExt->input('NewClientWorkingHour/zakladni_mzda',array('label'=>'Kalkulace ZHM - '.$list_mzda_typ[$this->data['NewClientWorkingHour']['mzda_typ']],'class'=>''));?><br />
	                <?php echo $htmlExt->input('NewClientWorkingHour/salary_start',array('label'=>'Celkem za měsíc', 'readonly'=>'readonly','class'=>'read_info'));?><br/>

                </fieldset>
          
                    <table rel="0" class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Bonusy</th>
                                <th>A/N</th>
                                <th>Součet</th>
                                <th style="text-align:right;"><span class="sum_of_all"></span> Kč</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(isset($money_item_setting) && isset($money_item_setting[0])){
                               echo $fastest->make_money_item_setting_in_employee(0,$money_item_setting[0],$list_type_of_drawback_and_bonus,$connection_cwh_to_mis);
                            }
                            ?>
                        </tbody>
                    </table>  
                    
                    <br />
                    <table rel="2" class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Srážky</th>
                                <th>A/N</th>
                                <th>Součet</th>
                                <th style="text-align:right;"><span class="sum_of_all"></span> Kč</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(isset($money_item_setting) && isset($money_item_setting[2])){
                               echo $fastest->make_money_item_setting_in_employee(2,$money_item_setting[2],$list_type_of_drawback_and_bonus,$connection_cwh_to_mis);
                            }
                            ?>
                        </tbody>
                    </table>  
                    
                    <br />  
                    <table rel="1" class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Příplatky</th>
                                <th>A/N</th>
                                <th>Součet</th>
                                <th style="text-align:right;"><span class="sum_of_all"></span> Kč</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if(isset($money_item_setting) && isset($money_item_setting[1])){
                               echo $fastest->make_money_item_setting_in_employee(1,$money_item_setting[1],$list_type_of_extra_pay,$connection_cwh_to_mis);
                            }
                            ?>
                        </tbody>
                    </table>  
                    
                  					
            </div>
            <br />
		</div>
	</div>
	<div style='text-align:center'>
		<?php if ((empty($this->data['NewClientWorkingHour']['stav']) || in_array($this->data['NewClientWorkingHour']['stav'],array(1))) || $company_detail['Company']['manazer_realizace_id'] == $logged_user['CmsUser']['id'] || in_array($logged_user['CmsUser']['id'],array(209,236,237)) || in_array($logged_user['CmsGroup']['id'],$permission_alow) || in_array($logged_user['CmsGroup']['id'],array(1,5)) || in_array($logged_user['CmsGroup']['id'],array(6,7)) && in_array($this->data['NewClientWorkingHour']['stav'],array(1,2))):?>
		<input type='button' value='Uložit' id='only_save'/>
		<?php endif;?>
        
        <?php if ((($year.'-'.$month) == date('Y-m')) && $allow_payment_0){?>
		      <input type='button' value='Požadavek na zálohu' rel='0' class='add_down_payments'/>
        <?php }?>
        
        <?php if ((($year.'-'.$month) == date('Y-m')) && $allow_payment_1){?>
		      <input type='button' value='Požadavek na VVH' rel='1' class='add_down_payments'/>
		<?php }?>
        
		<input type='button' value='Zavřít' id='only_close'/>
	</div>
</form>

<div id='dblclick_input' class='none'>
<label>Typ:</label>
	<select class='choose_input'>
		<option value="1" 	title="Cas">Cas</option>
		<option value="PN" 	rel="pracovni_neschopnost" title="Pracovní neschopnost">Pracovní neschopnost</option>
		<option value="D" 	rel="dovolena" title="Dovolená">Dovolená</option>
		<option value="NV" 	rel="neplacene_volno" title="Neplacené volno">Neplacené volno</option>
		<option value="OČR" rel="ocestrovani_clena_rodiny" title="Ošetřování člena rodiny">Ošetřování člena rodiny</option>
        <option value="A" 	rel="neomluvena_absence" title="Neomluvená absence">Neomluvená absence</option>
        <option value="V" 	rel="nahradni_volno" title="Nahradní volno">Nahradní volno</option>
		<option value="PG" 	rel="paragraf" title="Paragraf">Paragraf</option>
	</select>
	<br/>
    <div class="dovolena_info none">
        <?php 	echo $htmlExt->var_text('Client/zbyva_dovolene', array('label'=>'Nárok na dovolenou','class'=>'read_info','value'=>$zbyva_dovolene)); ?><br />
    </div>
    <div class="hour_info none">
        <?php 	echo $htmlExt->input('action_hour', array('label'=>'Hodin','class'=>'action_hour')); ?>
    </div>
	<div class='choose_do_div'>
		<label>Od:</label>
		<select class='choose_od_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_od_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<div class='choose_od_div'>
		<label>Do:</label>
		<select class='choose_do_h' style='width:40px; float:left;'>
			<?php for($i = 0; $i<24; $i++):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select><span style='display:block; width:5px; float:left; line-height:20px'>:</span>
		<select class='choose_do_m' style='width:40px'>
			<?php for($i = 0; $i<60; $i += 5):?>
				<option value="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>" 	title="<?php echo ((strlen($i)==1)?'0'.$i:$i);?>"><?php echo ((strlen($i)==1)?'0'.$i:$i);?></option>
			<?php endfor;?>
		</select>
		<br/>
	</div>
	<p>
		<input type='button' value='Vložte' class='choose_do_it'/>
	</p>
</div>
<?php echo $this->renderElement('../new_employees/js',array('payment_sum'=>$payment_sum));?>