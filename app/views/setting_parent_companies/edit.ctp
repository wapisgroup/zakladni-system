<form action='/setting_parent_companies/edit/' method='post' id='parent_company_edit_formular'>
	<?php echo $htmlExt->hidden('SettingParentCompany/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('SettingParentCompany/name',array('tabindex'=>1,'label'=>'Název'));?> <br class="clear">
					<?php echo $htmlExt->input('SettingParentCompany/email',array('tabindex'=>2,'label'=>'Email'));?> <br class="clear">
					<?php echo $htmlExt->input('SettingParentCompany/account',array('tabindex'=>3,'label'=>'Číslo účtu'));?> <br class="clear">
					<?php echo $htmlExt->input('SettingParentCompany/code_bank',array('tabindex'=>3,'label'=>'Kód banky','class'=>'integer','MAXLENGTH'=>4));?> <br class="clear">
				</div>
				<div class="slr">  
				</div>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
    
    $('parent_company_edit_formular').getElements('.integer').inputLimit();
     
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('parent_company_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('parent_company_edit_formular').action,		
				onComplete:function(){
					click_refresh($('SettingParentCompanyId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('parent_company_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('parent_company_edit_formular',{
		'SettingParentCompanyName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
			'isUnique':{'condition':{'model':'SettingParentCompany','field':'name'},'err_message':'Tento stav je již použit'}
		}
	});
	validation.generate('parent_company_edit_formular',<?php echo (isset($this->data['SettingParentCompany']['id']))?'true':'false';?>);
</script>