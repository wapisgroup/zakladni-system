<?php
    $output = $headers =  $new_items = array();
    $index = $k = 0;
    
    $pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
    $last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
    $current_day = $last_day;
    $dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );

    $headers[] = 'Den';
    for($i=0; $i<$pocet_dnu_v_mesici; $i++):
       $headers[] = $dny[$current_day].' '.($i+1);
       if(in_array($current_day,array(6,7))) 
          $excel->vikends[] = ($i+1);
       $current_day = ($current_day == 7)?$current_day=1:$current_day+1;
    endfor;

	if (isset($items) && count($items)>0)
		foreach ($items as $num => $item):
				foreach($renderSetting['items'] as $key => $td){
				    list($caption, $model, $col, $type, $fnc) = explode('|',$td);
                    $td = compact(array("caption", "model","col","type","fnc"));
					$caption_td = null;
                    if($type != 'hidden')
                        $caption_td = $caption.' - ';
                        
                    if(isset($new_items[$num]))    
                        $new_items[$num] .= ' '.$caption_td.$viewIndex->generate_td($item,$td,$this->viewVars);  
				    else
                        $new_items[$num] = $caption_td.$viewIndex->generate_td($item,$td,$this->viewVars); 
                }
		endforeach;
	$_setting = array(
        'title'=>$spec_h1,
        'pocet_dnu_v_mesici'=>$pocet_dnu_v_mesici,
        'last_day'=>$last_day,
        'current_day'=>$current_day,
        'dny'=>$dny,
        'year'=>$year,
        'month'=>$month,
        'svatky'=>$svatky
    );
    $excel->render_print_employees($items,$new_items,$headers,$_setting);
?>