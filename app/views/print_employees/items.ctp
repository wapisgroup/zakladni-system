<table id='tabulka_item' class='table odpracovane_hodiny2'>
<?php 
$pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
$last_day = date('N', mktime(0, 0, 0, $month, 1, $year)); 
$current_day = $last_day;
$dny = array( 1=>'Po', 2=>'Út', 3=>'St', 4=>'Čt', 5=>'Pá', 6=>'So', 7=>'Ne' );

	echo '<thead><tr>';?>
           <th>Den</th>
            <?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
            <th <?php 
                    if (in_array(($i+1),$svatky)) 
            		  echo "class='svatky'";
            		else if (in_array($current_day,array(6,7))) 
            		  echo "class='vikend'";
            	?>>
            	<?php echo $dny[$current_day];?>
            <br />
          	<?php echo ($i+1);?>
        	<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
          						
            </th>
    <?php endfor;?>
    <?php
	echo '</tr></thead>';
	echo'<tbody>';
	$index = 0;
	$class_tr = '';
	if (isset($items) && count($items)>0)
		foreach ($items as $num => $item):
            //echo $item;
            /**
             * klasika z items vypsani radku
             * klient, company atd
             */    
			echo '<tr class="od"><td colspan=100>'; 
				foreach($renderSetting['items'] as $key => $td){
				    list($caption, $model, $col, $type, $fnc) = explode('|',$td);
                    $td = compact(array("caption", "model","col","type","fnc"));
					$caption_td = null;
                    if($type != 'hidden')
                        $caption_td = '<strong>'.$caption.'</strong> - ';
                    
                    echo $caption_td.$viewIndex->generate_td($item,$td,$this->viewVars);
                    
                    if($type != 'hidden')
                        echo '<span style="background-color:silver; margin-left:2%;"></span>';
				}
			echo '</td></tr>';
            
            /**
             * zde vyrendrovani radku s vyplnennyma hodinama
             * po jednotlivych smenach
             */
             $this->data = $item;
                                       
            	$start 	= $this->data['ConnectionClientRequirement']['from'];
            	$end 	= ($this->data['ConnectionClientRequirement']['to']== '0000-00-00')?date("Y-m-d"):$this->data['ConnectionClientRequirement']['to'];	
            	
            		
            	// render list of disabled fields
            	$not_to_disabled = array();
            	for ($i=1; $i <= $pocet_dnu_v_mesici; $i++){
            		$curr_date = $year.'-'.$month.'-'.((strlen($i) == 1)?'0'.$i:$i);
            		if (strtotime($start) <= strtotime($curr_date) && strtotime($end) >= strtotime($curr_date))
            			$not_to_disabled[] = $i;
            	} 
            					$smena_captions = array(
            						1	=> 'Ranní',
            						2	=> 'Odpolední',
            						3	=> 'Noční'
            					);
            				?>
            				<?php for($k = 1; $k <= 3; $k++):?>
            				<tr class='tr_smennost_<?php echo $k;?> <?php 
            						if (($k == 2 && $this->data['ClientWorkingHour']['setting_shift_working_id'] == 1) || 
            							($k == 3 && in_array($this->data['ClientWorkingHour']['setting_shift_working_id'],array(1,2)))
            							|| (empty($this->data['ClientWorkingHour']['setting_shift_working_id']))
            							) { echo 'none'; }?>'>	
            					<td><?php echo $smena_captions[$k];?></td>
            					<?php $current_day = $last_day;?>
            					<?php for($i=0; $i<$pocet_dnu_v_mesici; $i++):?>
            						<td <?php 
            							if (in_array(($i+1),$svatky)) 
            								echo "class='svatky'";
            							else if (in_array($current_day,array(6,7))) 
            								echo "class='vikend'";
            						?>>
            						<?php 
            							if (in_array(($i+1),$svatky)){ 
            								$class =  " svatky";
            							} else if (in_array($current_day,array(6,7))){
            								$class = " vikend";
            							} else {
            								$class = "";
            							}
            						?>
                                    <var><?php echo (isset($this->data['ClientWorkingHour']['days']['_' .$k. '_' .($i+1)]) ? $this->data['ClientWorkingHour']['days']['_' .$k. '_' .($i+1)] : ' ');?></var>
            						<?php $current_day = ($current_day == 7)?$current_day=1:$current_day+1;?>
            					<?php endfor;?>
            				</tr>
            				<?php endfor;?>
            		
            <?php
			//echo '</td></tr>';
            echo '<tr><td colspan=100>&nbsp;</td></tr>';

		endforeach;
	else
		echo '<tr><td colspan="100" style="text-align:center">Nic nebylo nenalezeno ...</td></tr>';
?>
</tbody></table>
<br class="clear" />


<script>

	
	/* **************************** */
	/* END  js for Company Activities*/
	/* **************************** */
	
	
	<?php if (isset($spec_h1)):?>
		$('obal').getElement('h1').setHTML('<?php echo $spec_h1;?>');
	<?php endif;?>

	<?php if(isset($change_list_js)):?>
		var get_data =  JSON.decode('<?php echo json_encode($_GET);?>');

		var data_change = JSON.decode('<?php echo $change_list_js;?>');
		$each(data_change, function(items, elementId){
			var element = $(elementId);
			if (element){
				element.empty();
				new Element('option',{title:'-- všechny --', value:''}).setHTML('-- všechny --').inject(element);
				$each(items, function(caption, id){
					neco = (get_data['filtration_ConnectionClientRequirement-company_id']==id ? 'selected' : '');
					new Element('option',{title:caption, value:id, selected:neco}).setHTML(caption).inject(element);
				})
			}
		});
	<?php endif;?>
</script>
