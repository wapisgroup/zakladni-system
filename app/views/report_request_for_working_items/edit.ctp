<form action='/report_request_for_working_items/edit/' method='post' id='report_down_payments_edit_formular'>
	<?php echo $htmlExt->hidden('RequestForWorkingItem/id');?>
	<?php echo $htmlExt->hidden('RequestForWorkingItem/cms_user_id');

?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
    </div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní</legend>
				<div class="sll">  
					<?php if (isset($this->data['RequestForWorkingItem']['date_of_pickup']) && $this->data['RequestForWorkingItem']['date_of_pickup'] != '0000-00-00' && $logged_user['CmsGroup']['id'] != 1):?>
						<?php echo $htmlExt->var_text('Company/name',array('tabindex'=>1,'label'=>'Firma'));?> <br class="clear">
						<?php echo $htmlExt->var_text('Client/name',array('tabindex'=>2,'label'=>'Zaměstnanec'));?> <br class="clear">
						<?php echo $htmlExt->var_text('RequestForWorkingItem/working_item_id',array('tabindex'=>2,'label'=>'Pomůcka'));?> <br class="clear">
						<?php echo $htmlExt->var_text('RequestForWorkingItem/amount',array('tabindex'=>4,'label'=>'Částka'));?> <br class="clear">
					    <?php echo $htmlExt->var_text('RequestForWorkingItem/date_of_pickup',array('tabindex'=>5,'label'=>'Zaplaceno '));?> <br class="clear">(vyplňuje finanční účetní)
				
                    <?php else:?>
						<?php echo $htmlExt->selectTag('RequestForWorkingItem/company_id',$company_list,null,array('tabindex'=>1,'label'=>'Firma'));?> <br class="clear">
						<?php echo $htmlExt->selectTag('RequestForWorkingItem/client_id',$client_list,null,array('tabindex'=>2,'label'=>'Zaměstnanec'));?> <br class="clear">
						<?php echo $htmlExt->selectTag('RequestForWorkingItem/working_item_id',$activity_pomucky_list,null,array('tabindex'=>2,'label'=>'Pomůcka'));?> <br class="clear">
					<?php endif;?>
				</div>
			</fieldset>
		</div>
	</div>
	
	<div class="win_save">
		<?php 
         if(!isset($this->data['RequestForWorkingItem']['id']) || in_array($logged_user['CmsGroup']['id'],array(1,6,7)))   
            echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>6));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>7));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 

//if($('RequestForWorkingItemCompanyId'))
//	$('RequestForWorkingItemCompanyId').ajaxLoad('/report_down_payments/load_ajax_zamestnanci/',['RequestForWorkingItemClientId']);


	// add event for change Profese and load list for calculations
    if($('RequestForWorkingItemCompanyId'))
    	$('RequestForWorkingItemCompanyId').addEvents({
    			'change': function(){
    					new Request.JSON({
    						url: '/report_down_payments/load_ajax_zamestnanci/' + this.value + '/',
    						onComplete: (function (json){
    							if (json){
    								if (json.result === true){
    									var sub_select = $('RequestForWorkingItemClientId');
    									sub_select.empty();
    									new Element('option').setHTML('').inject(sub_select);
    									$each(json.data, function(item){
    										new Element('option',{value:item.Client.id,title:item.Client.cislo_uctu})
    											.setHTML(item.Client.name)
    											.inject(sub_select);
    									})
    									//this.currentSelection = this.selectedIndex;
    								} else {
    									this.options[this.currentSelection].selected = 'selected';
    									alert(json.message);
    								}
    							} else {
    								alert('Systemova chyba!!!');
    							}
    						}).bind(this)
    					}).send();
    			},
    			'focus': function(){
    				this.currentSelection = this.selectedIndex;
    			}
    	});
    


    if($('save_close'))
    	$('save_close').addEvent('click',function(e){
    		new Event(e).stop();
            
            	
    		valid_result = validation.valideForm('report_down_payments_edit_formular');
    		if (valid_result == true){
    		  
    			new Request.JSON({
    				url:$('report_down_payments_edit_formular').action,		
    				onComplete:function(json){
    				    if(json.result == true){
    					   click_refresh($('RequestForWorkingItemId').value);
    					   domwin.closeWindow('domwin');
                        }
                        else
                            alert('Chyba aplikace');
    
    				}
    			}).post($('report_down_payments_edit_formular'));
    		} else {
    			var error_message = new MyAlert();
    			error_message.show(valid_result)
    		}
    	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('report_down_payments_edit_formular',{
		'RequestForWorkingItemClientId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit zaměstnance'},
		},
        'RequestForWorkingItemWorkingItemId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit pomůcku'},
		}
	});
	validation.generate('report_down_payments_edit_formular',<?php echo (isset($this->data['RequestForWorkingItem']['id']))?'true':'false';?>);
</script>