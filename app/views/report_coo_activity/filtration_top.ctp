  <?php if(isset($filtration_sum_variables2)){
    $pagination->ignored_params = array();
    $sum_cz = $sum_sk = 0;
    foreach($filtration_sum_variables2 as $sum_vars){
        if(in_array($sum_vars['Company']['id'].'_'.$sum_vars['CmsUser']['id'],$_ignored_items)){continue;}
        
        if($sum_vars['Company']['stat_id'] == 1){ $sum_cz += $sum_vars['0']['srazka_celkem']; }
        else{ $sum_sk += $sum_vars['0']['srazka_celkem'];}
    }
    
    ?>
    <div id="filtration_variables2">
        <div class="sll">
            Celkem CZ: <strong><?= $fastest->price($sum_cz,',- CZK');?></strong><br />
        </div>
        <div class="slr">
           Celkem EUR: <strong><?= $fastest->price($sum_sk,',- EUR');?></strong><br />    
        </div>
    </div>
    <?php }