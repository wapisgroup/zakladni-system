<form action='/orders_cast_posts/move_to_zam_domwin/' method='post' id='formular'>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/id');?>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/company_id');?>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/client_id');?>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/requirements_for_recruitment_id');?>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/type',array('value'=>2));?>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/objednavka',array('value'=>1));?>
    <fieldset>
		<legend>Komentář</legend>
		<?php echo $htmlExt->textarea('ConnectionClientRequirement/text',array('tabindex'=>1,'label'=>'Text','class'=>'long','label_class'=>'long'));?> <br class="clear">
		<?php echo $htmlExt->checkbox('ConnectionClientRequirement/new',null,array('tabindex'=>1,'label'=>'Nová','class'=>'','label_class'=>'long'));?> <br class="clear">		
      
        <?php echo $htmlExt->selectTag('ConnectionClientRequirement/company_work_position_id',$company_work_position_list,null,array('label'=>'Profese','class'=>'long','label_class'=>'long'));?><br/>
	    <?php echo $htmlExt->selectTag('ConnectionClientRequirement/company_money_item_id',$money_item_list=array(),null,array('label'=>'Forma odměny','class'=>'long','label_class'=>'long'),null,false);?><br/>
		<?php echo $htmlExt->inputDate('ConnectionClientRequirement/from',array('tabindex'=>1,'value'=>Date ('Y-m-d'),'label'=>'Datum nástupu','class'=>'','label_class'=>'long'));?> <br class="clear">		
	</fieldset>
	<div class='formular_action'>
        <input type='button' value='Tisk' id='PrintForms' />
		<input type='button' value='Zaměstnat' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>
    $('ConnectionClientRequirementNew').addEvent('change', function(){
            new Request.JSON({
			url: '/orders_cast_posts/load_profese/' + $('ConnectionClientRequirementCompanyId').value + '/'+ this.checked + '/',
			onComplete: (function (json){
				if (json){
					if (json.result === true){
						var sub_select = $('ConnectionClientRequirementCompanyWorkPositionId');
						sub_select.empty();
                        $('ConnectionClientRequirementCompanyMoneyItemId').empty();
                        new Element('option',{value:'',title:'Zvolte profesi'}).setHTML('Zvolte profesi').inject(sub_select);
						$each(json.data, function(val,id){
                            new Element('option',{value:id,title:val})
								.setHTML(val)
								.inject(sub_select);
						})
					} else {
						this.options[this.currentSelection].selected = 'selected';
						alert(json.message);
					}
				} else {
					alert('Systemova chyba!!!');
				}
			}).bind(this)
		}).send();
    })    

	// add event for change Profese and load list for calculations
	$('ConnectionClientRequirementCompanyWorkPositionId').addEvent('change', function(){
		new Request.JSON({
			url: '/orders_cast_posts/load_calculation_for_profese/' + this.value + '/'+ $('ConnectionClientRequirementNew').checked + '/',
			onComplete: (function (json){
				if (json){
					if (json.result === true){
						var sub_select = $('ConnectionClientRequirementCompanyMoneyItemId');
						sub_select.empty();
						$each(json.data, function(item){
						    if($('ConnectionClientRequirementNew').checked == true){
						      new Element('option',{value:item,title:'kalkulace #'+item}).setHTML('kalkulace #'+item).inject(sub_select);
						    }
                            else{
    							doprava = 'Doprava '+(item.doprava != 0 ? 'Ano' : 'Ne');
    							stravenka = 'Stravenka '+(item.stravenka != 0 ? 'Ano' : 'Ne');
    							ubytovani = 'Ubytování '+(item.cena_ubytovani_na_mesic != 0 ? 'Ano' : 'Ne');
                                body = item.cista_mzda_z_pracovni_smlouvy_na_hodinu + '/' + item.cista_mzda_faktura_zivnostnika_na_hodinu + '/' + item.cista_mzda_dohoda_na_hodinu + '/' + item.cista_mzda_cash_na_hodinu;
    							
                                if(item.pausal == 1)
                                    body = item.ps_pausal_hmm + '/' + item.odmena_fakturace_1 + '/' + item.odmena_fakturace_2
                                
                                new Element('option',{value:item.id,title:item.name + '|' + item.stravenka + '|' + item.fakturacni_sazba_na_hodinu})
    								.setHTML(item.name + ' - ' + body
    								+ ' - ' + ubytovani +' / '+ doprava	+ ' / ' + stravenka
    								)
    								.inject(sub_select);
                            }    
						})
					} else {
						this.options[this.currentSelection].selected = 'selected';
						alert(json.message);
					}
				} else {
					alert('Systemova chyba!!!');
				}
			}).bind(this)
		}).send();
	});

	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_add_as_employment');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		if($('ConnectionClientRequirementCompanyWorkPositionId').value != ''){
		  if(confirm(get_confirm_text())){
    			new Request.JSON({
    				url:$('formular').action,		
    				onComplete:function(json){
    					if (json){
    						if (json.result === true){
    							var tr = $('tr_connection_id_' + json.data.connection_id);
    							var tbody = $('table_zamestnani');
    							var tr_neobsazeno = tbody.getElement('.neobsazeno');
    							if (!tr_neobsazeno) {
    								alert('Pravdepodobne doslo k chybe, do DB byl vlozen zaznam o zamestnani, ale byl presazen limit. Proved te refresh a overte seznam zamestnanych');
    							} else {
    								if (tr && tbody) {
    									// odstranim radek
    									tr.dispose();
    									var new_tr = new Element('tr').inject(tr_neobsazeno,'before').setHTML('<td> ' + json.data.client_name + ' </td><td> ' + json.data.cms_user_name + ' </td><td> ' + json.data.created + ' </td><td> ' + json.data.od + ' </td><td> ' + json.data.pozice + ' </td><td> ' + json.data.money_item + ' </td><td><a class="ta client_info" href="/clients/edit/' + json.data.client_id + '/domwin/only_show" title="Informace o klientovi">Info</a> <a href="/clients/add_activity/' + json.data.client_id + '" class="ta phone"  title="Přidání aktivity">Add Activity</a></td>');
    									new_tr.getElement('.client_info').addEvent('click', call_domwin_client_info.bindWithEvent(this));
    									new_tr.getElement('.phone').addEvent('click', call_domwin_add_activity.bindWithEvent(this));
    									tr_neobsazeno.dispose();
    									domwin.closeWindow('domwin_add_as_employment');
    								} else {
    									alert('Chyba JS, pro aktualni informace provedte reload okna');
    								}
    							}
    						} else {
    							alert(json.message);
    						}
    					} else {
    						alert('Chyba aplikace')
    					}
    					
    					
    				}
    			}).post($('formular'));
           } 
		}
		else
			alert('Musíte vyplnit profesi');

	});
    
    /**
     * funkce ktera vraci shrnuti pri propusteni klienta
     */
    function get_confirm_text(){
        /**
         * nastavnei promennych pouzitych do textu
         */
        var datum_natupu = $('calbut_ConnectionClientRequirementFrom').value;
        var profese = $('ConnectionClientRequirementCompanyWorkPositionId').options[$('ConnectionClientRequirementCompanyWorkPositionId').selectedIndex].title;
        var forma = $('ConnectionClientRequirementCompanyMoneyItemId').options[$('ConnectionClientRequirementCompanyMoneyItemId').selectedIndex].getHTML();

        var komentar = $('ConnectionClientRequirementText').value;
        /**
         * slozeni textu propusteni
         */
        var text  = "Shrnutí zaměstnání :\n\n";
            text += "Datum nástupu: "+datum_natupu+"\n";
            text += "Profese: "+profese+"\n";
            text += "Forma odměny: "+forma+"\n";
            text += "Komentář: "+komentar;
            
        //return    
        return text;
    } 
    
    /**
     * 
     */
   $('PrintForms').addEvent('click',function(e){
		e.stop();
		domwin.newWindow({
			id			: 'domwin_print_forms',
			sizes		: [580,350],
			scrollbars	: true,
			title		: 'Tisk formulářů',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: '/orders_cast_posts/show_print_forms/'+$('ConnectionClientRequirementCompanyId').value+'/',
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	})
    
</script>