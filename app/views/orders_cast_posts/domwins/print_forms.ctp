	<?php echo $htmlExt->hidden('client_id');?>
    <fieldset>
		<legend>Vyberte formulář který chcete tisknout</legend>
		<?php echo $htmlExt->selectTag('template_form_id',$forms_list,null,array('label'=>'Formulář','class'=>'long','label_class'=>'long'));?><br/>
	</fieldset>
	<div class='formular_action'>
        <input type='button' value='Zobrazit' id='ShowForm' />
		<input type='button' value='Zavřít' id='ClosePrintForms'/>
	</div>
<script>
  	$('ClosePrintForms').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_print_forms');});

    $('ShowForm').addEvent('click',function(e){
            new Event(e).stop(); 
            domwin.newWindow({
    			id			: 'domwin_forms',
    			sizes		: [1000,1000],
    			scrollbars	: true,
    			title		: 'Zobrazení formuláře',
    			languages	: false,
    			type		: 'AJAX',
    			ajax_url	: '/form_templates/generate_form_new/'+$('TemplateFormId').value+'/'+$('ConnectionClientRequirementClientId').value+'/0/',
    			closeConfirm: true,
    			max_minBtn	: false,
    			modal_close	: false,
    			remove_scroll: false
    		}); 
    });

</script>