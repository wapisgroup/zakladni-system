<form action='/orders_cast_posts/remove_from_cl/' method='post' id='formular'>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/id');?>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/client_id');?>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/company_id');?>
	<?php echo $htmlExt->hidden('ConnectionClientRequirement/requirements_for_recruitment_id');?>
    <?php echo $htmlExt->hidden('client_id',array('value'=>$client_id));?>
	<?php echo $htmlExt->hidden('hodnoceni',array('value'=>$hodnoceni));?>
    <fieldset>
		<legend>Důvod odstranění</legend>
		<?php echo $htmlExt->selectTag('ConnectionClientRequirement/reason',$duvod_uvolneni_list,null,array('label'=>'Důvod uvolnění','class'=>'long','label_class'=>'long'),null,false);?><br/>
		<div id="problem_box" class="none">
                <?php echo $htmlExt->selectTag('problem', array(), null, array('class'=>'long','label_class'=>'long','tabindex' => 2, 'label' => 'Problémy'), null, false); ?><br />
                <?php echo $htmlExt->inputDate('datum', array('class'=>'','label_class'=>'long','tabindex' => 3, 'label' => 'Datum')); ?><br />
        </div>
        
        <?php echo $htmlExt->textarea('ConnectionClientRequirement/text',array('tabindex'=>3,'label'=>'Komentář','class'=>'long','label_class'=>'long'));?> <br class="clear">
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Odstranit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script type='text/javascript'>
     /**
     * zmeny druhy aktivity
     */ 
    $('ConnectionClientRequirementReason').ajaxLoad('/misconducts/load_problem_list/',['Problem']);  
    $('ConnectionClientRequirementReason').addEvent('change',function(e){
        new Event(e).stop(); 
        /**
         * při druhu zaměstnání se volá novy typ activity
         * Kategorie : Zamestnani
         * akce : prideleni pracovnich pomucek
         * kvuli zjednoduseni je zmena action, pro odchyceni history komponentou
         **/
        if(this.value == 6){
            if($('Hodnoceni').value == '0'){
                alert('Klient nemá žádné hodnocení, daný klient nebyl nejspíše vůbec zaměstnán!');
                this.value = 1;
            }
            else {          
                $('problem_box').removeClass('none');
                set_problem_box_to('invalid');
            }    
        }
        else{
            $('problem_box').addClass('none');
            set_problem_box_to('valid');
        }
    }); 
        
    function set_problem_box_to(type){
        if(type == 'valid')
           $('problem_box').getElements('input,select').removeClass('require').removeClass('invalid').addClass('valid');
        else
            $('problem_box').getElements('input,select').removeClass('valid').addClass('invalid');
    }
    

	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_remove_curr_add');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		new Request.JSON({
			url:$('formular').action,
			onComplete:function(json){
				if (json) {
					if (json.result === true) {
						domwin.closeWindow('domwin_remove_curr_add');
						/*
						 * Presun z cekaci listiny na nominacni listinu
						 * avsak je brano v potaz to, kdo ho zamestnal, tzn. z jake skupiny cms_user
						 * tak do te nominacni tabulky se prenese
						 */
						
						var tr = $('tr_connection_id_'+json.connection_id);
						if (tr){
							var tbody = $('table_listina_'+json.nl_target);
							if (tbody){
							    tr.getElements('td')[1].dispose();//telefon odstranit
                                tr.getElements('td')[1].dispose();//mesto odstranit
                                tr.getElements('td')[1].dispose();//okres odstranit
                                tr.getElements('td')[1].dispose();//opp odstranit
                                tr.getElements('td')[1].dispose();//pohovor odstranit
                                //tr.getElements('td')[1].setHTML(json.priradil); 
								tbody.adopt(tr);
								var td_posibility = tr.getElement('td:last-child');
								// nove moznosti
								td_posibility.setHTML('<a href="/orders_cast_posts/remove_from_nl/' + json.connection_id + '/' + json.client_id + '" class="ta delete remove_listina">Odtranit</a> <a href="/orders_cast_posts/move_from_nl_to_cl/'+json.connection_id+'/' + json.client_id + '" class="ta nabor_pozice move_cl" title="Presunout na ČL">Presunout na cl</a> <a href="/clients/edit/' + json.client_id + '/domwin/only_show" class="ta client_info" title="Informace o klientovi">Info</a> <a href="/clients/add_activity/' + json.client_id + '" class="ta phone" title="Přidání aktivity">Add Activity</a>')
								td_posibility.getElement('.remove_listina').addEvent('click',remove_client_from_listina_table.bindWithEvent(this,[tbody.get('id')]));
								td_posibility.getElement('.move_cl').addEvent('click',move_client_to_cl_table.bindWithEvent(this,[tbody.get('id')]));
								td_posibility.getElement('.client_info').addEvent('click',call_domwin_client_info.bindWithEvent(this));
								td_posibility.getElement('.phone').addEvent('click',call_domwin_add_activity.bindWithEvent(this));
							}
						}
						
						tab = 1;
                        if(json.nl_target == 'in'){tab=2;}
                        else if(json.nl_target == 'en'){tab=3;}
						domtab.goTo(tab);
					}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).post($('formular'));

	});
    
    
    validation.define('formular',{
        'Problem': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit problem'},
		},
        'calbut_Datum': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum'},
		}
	});

	validation.generate('formular',false);
    set_problem_box_to('valid'); // odzacatku je valid
    
</script>