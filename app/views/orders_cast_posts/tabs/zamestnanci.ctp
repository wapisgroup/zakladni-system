 <table class='table'>
    	<thead>
	    	<tr>
	    		<th>Klient</th>
	    		<th>Přiřadil</th>
	    		<th>Přiřazen</th>
	    		<th>Datum nástupu</th>
	    		<th>Pozice</th>
	    		<th>Typ odměny</th>
	    		<th>Možnosti</th>
	    	</tr>
	</thead>
	<tbody id='table_zamestnani'>
			<?php if (isset($zamestnanci_list) && count($zamestnanci_list)>0):?>
			<?php foreach($zamestnanci_list as $client):?>
			<tr>
	    		<td><?php echo $client['Client']['name'];?> </td>
	    		<td><?php echo $client['CmsUser']['name'];?> </td>
	    		<td><?php echo $fastest->czechDate($client['ConnectionClientRequirement']['created']);?> </td>
	    		<td><?php echo $fastest->czechDate($client['ConnectionClientRequirement']['from']);?> </td>
	    		<td><?php echo $client['CompanyWorkPosition']['name'];?> </td>
	    		<td><?php 
                    if($client['ConnectionClientRequirement']['new'] == 0)
                        echo $fastest->made_forma_odmeny($client['CompanyMoneyItem']['name'],$client['CompanyMoneyItem']['doprava'],$client['CompanyMoneyItem']['cena_ubytovani_na_mesic'],$client['CompanyMoneyItem']['stravenka']);
                    else
                        echo '';
                    ?> 
                </td>
	    		<td>
	    			<a class="ta client_info" href="/clients/edit/<?php echo $client['Client']['id'];?>/domwin/only_show" title="Informace o klientovi">Info</a>
				    <a class="ta phone" href="/clients/add_activity/<?php echo $client['Client']['id'];?>" title="Přidání aktivity">Add Activity</a>
		        </td>
	    	</tr>
			<?php endforeach;?>
			<?php endif;?>
			<?php for($i=0; $i<($detail['CompanyOrderItem']['count_of_free_position'] - count($zamestnanci_list));$i++):?>
			<tr class='neobsazeno'>
				<td colspan='6' align='center'>Neobsazeno</td>
			</tr>		
			<?php endfor;?>
	</tbody>
</table>