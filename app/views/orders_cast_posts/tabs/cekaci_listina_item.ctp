 <table class='table'>
    	<thead>
	    	<tr>
	    		<th>Klient</th>
	    		<th>Telefon</th>
                <th>Město</th>
                <th>Okres</th>
	    		<th>Nohavice-blúza-tričko-obuv</th>
                <th>Pohovor</th>
                <th>Přiřadil</th>
	    		<th>Přiřazen</th>
	    		<th>Možnosti</th>
	    	</tr>
	</thead>
	<tbody id='table_cekaci_listina'>
			<?php if (isset($cekaci_listina_list) && count($cekaci_listina_list)>0):?>
			<?php foreach($cekaci_listina_list as $client):
            
            $class = null;
            if(isset($client['ConnectionClientRequirement']['cl_notification']) && $client['ConnectionClientRequirement']['cl_notification'] == 1){
                $class = 'color_red';
            }
            else if(isset($client['ConnectionClientRequirement']['cl_notification']) && $client['ConnectionClientRequirement']['cl_notification'] == 3){
                $class = 'color_orange';
            }
            ?>
			<tr id='tr_connection_id_<?php echo $client['ConnectionClientRequirement']['id'];?>' class="<?= $class;?>">
	    		<td><?php echo $client['Client']['name'];?></td>
	    		<td><?php echo $client['Client'][$client['Client']['mobil_active']];?> </td>
                <td><?php echo $client['Client']['mesto'];?></td>
                <td><?php echo $client['Countrie']['name'];?></td>
                <td><?php echo $client['Client']['opp_nohavice'].'-'.$client['Client']['opp_bluza'].'-'.$client['Client']['opp_tricko'].'-'.$client['Client']['opp_obuv'];?> </td>
	    		<td><?php echo $fastest->value_to_yes_no($client['Client']['os_pohovor'],'long',true);?></td>
                <td><?php echo $client['CmsUser']['name'];?></td>
                <td><?php echo $fastest->czechDate($client['ConnectionClientRequirement']['created']);?></td>
	    		<td>
	    			<?php if ($logged_user['CmsGroup']['id'] == 8): // poked se jedna o EN?>
					<em>Nepovoleno</em>
				<?php else:?>
	    			<a href="/orders_cast_posts/remove_from_cl/<?php echo $client['ConnectionClientRequirement']['id'];?>" class="ta delete remove_client_from_cl" onclick='return false;'>Odtranit</a> 
                    <?php
						/*
						* pokud se jedna o predbeznou objednavku, nezobrazuj tlacitko na prevod na zamestnance
						*/
					?>
					<?php if($detail['CompanyOrderItem']['order_status'] != -1):?>
	 		   			<a href="/orders_cast_posts/move_to_zam_domwin/<?php echo $client['ConnectionClientRequirement']['id'];?>" class="ta nabor_pozice move_zam" onclick='return false;' title='Zaměstnat'>Zamestnant</a>
					<?php endif;?>
                    <a class="ta client_info" href="/clients/edit/<?php echo $client['Client']['id'];?>/domwin/only_show" title="Informace o klientovi">Info</a>
			        <a class="ta phone" href="/clients/add_activity/<?php echo $client['Client']['id'];?>/1/<?php echo $client['ConnectionClientRequirement']['id'];?>" title="Přidání aktivity">Add Activity</a>
		
            	<?php endif;?>
				</td>
	    	</tr>
			<?php endforeach;?>
			<?php else :?>
			<?php endif;?>
	 </tbody>
</table>
<div class="win_save">
		<?php echo $html->link('Vytisknout přehled','/orders_cast_posts/print_prehled/'.$this->data['CompanyOrderItem']['id'].'/',array('class'=>'mr5 button','target'=>'_blank'));?>
</div>
<script type='text/javascript'>
	
	/*
	 * inicializace event pro otevreni domwinu pro zamestnani klienta
	 */
	$$('.move_zam').addEvent('click',zamestnat_klient_domwin.bindWithEvent(this));
	
	/*
	 * funkce Otevreni domwinu pro zamestnani klienta
	 */
	function zamestnat_klient_domwin(e){
		e.stop();
		var obj = e.target;
		domwin.newWindow({
			id			: 'domwin_add_as_employment',
			sizes		: [580,350],
			scrollbars	: true,
			title		: 'Zaměstnani čekatele',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: obj.getProperty('href'),
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	}
	
	/*
	 * inicializace event pro otevreni domwinu pro presun
	 * z cekaci listiny zpet na nominacni listinu
	 */
	$$('.remove_client_from_cl').addEvent('click',odebrani_klient_z_cekaci_listina_domwin.bindWithEvent(this));
	
	/*
	 * funkce Otevreni domwinu pro presun
	 * z cekaci listiny zpet na nominacni listinu
	 */
	function odebrani_klient_z_cekaci_listina_domwin(e){
		e.stop();
		var obj = e.target;
		domwin.newWindow({
			id			: 'domwin_remove_curr_add',
			sizes		: [580,300],
			scrollbars	: true,
			title		: 'Odebrat uživatele z čekací listiny',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: obj.getProperty('href'),
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	}
	
	
	
	
</script>