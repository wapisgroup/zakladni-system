<div style='width:98%;height:440px; overflow:auto; border:0px solid red; margin:0px 0px 0px 1%; min-height:296px;' id='right_pozice'>
    <table class='table'>
    <thead>
	  	<tr>
	   		<th>Klient</th>
	   		<th>Přiřadil</th>
	   		<th>Přiřazen</th>
	   		<th>Možnosti</th>
	   	</tr>
	</thead>
	<tbody id='table_listina_<?php echo $typ;?>'>
			<?php if (isset($nominacni_listina_list) && count($nominacni_listina_list)>0):?>
			<?php foreach($nominacni_listina_list[$typ] as $client):?>
			<tr id='tr_connection_id_<?php echo $client['ConnectionClientRequirement']['id'];?>' class='<?php echo ($client['Client']['stav']== 2)?"color_red":"";?>'>
	    		<td><?php echo $client['Client']['name'];?> </td>
                <td><?php echo $client['CmsUser']['name'];?> </td>
	    		<td><?php echo $fastest->czechDate($client['ConnectionClientRequirement']['created']);?></td>
	    		<td>
	    			<?php if ($logged_user['CmsGroup']['id'] == 9 && $typ == 'cm'):?>
					<em>Nepovoleno</em>
					<?php else:?>
	    			<a href="/orders_cast_posts/remove_from_nl/<?php echo $client['ConnectionClientRequirement']['id'];?>/<?php echo $client['Client']['id'];?>" class="ta delete remove_listina" onclick='return false;'>Odtranit</a>
	    			<a href="/orders_cast_posts/move_from_nl_to_cl/<?php echo $client['ConnectionClientRequirement']['id'];?>/<?php echo $client['Client']['id'];?>" class="ta nabor_pozice move_cl" onclick='return false;' title='Presunout na ČL'>Presunout na cl</a>
				    <a class="ta client_info" href="/clients/edit/<?php echo $client['Client']['id'];?>/domwin/only_show" title="Informace o klientovi">Info</a>
				    <a class="ta phone" href="/clients/add_activity/<?php echo $client['Client']['id'];?>" title="Přidání aktivity">Add Activity</a>
		
                	<?php endif;?>
				</td>
	    	</tr>
			<?php endforeach;?>
			<?php else :?>
			<?php endif;?>
	 </tbody>
    </table>
</div>    
<script>
	$('table_listina_<?php echo $typ;?>').getElements('.remove_listina').addEvent('click',remove_client_from_listina_table.bindWithEvent(this,['table_listina_<?php echo $typ;?>']));
	$('table_listina_<?php echo $typ;?>').getElements('.move_cl').addEvent('click',move_client_to_cl_table.bindWithEvent(this));
</script>