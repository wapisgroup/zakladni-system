<?php
if(isset($regexp) && (in_array($logged_user['CmsGroup']['id'],array(9,41,56,57)) || isset($regexp_show))){
    echo $html->link('A-K','#',array('class'=>'filtration_alphabet '.($regexp == 'a-k' ? 'selected' : ''),'rel'=>'a-k'));
    echo $html->link('L-R','#',array('class'=>'filtration_alphabet '.($regexp == 'l-r' ? 'selected' : ''),'rel'=>'l-r'));
    echo $html->link('S-Z','#',array('class'=>'filtration_alphabet '.($regexp == 's-z' ? 'selected' : ''),'rel'=>'s-z'));
}

?>
<table class='table' id='client_list_<?php echo $typ;?>'>
	<tr><th colspan='2'>Jméno</th></tr>
	<?php	
        if (isset($client_list) && count($client_list)>0):
			foreach($client_list as $client):
				$class = ($client['Client']['stav']== 2)?"color_red":"";
				
				if (in_array($client['Client']['id'],$pouziti_klienti))
					$class.=' none';
				echo "<tr id='client_list_item_".$client['Client']['id'] ."' class='".$class."'><td>".$client['Client']['name'] ."</td><td>
					<a href='/orders_cast_posts/add_to_nl/$order_template_id/$company_id/".$client['Client']['id'] ."' rel='client_id=".$client['Client']['id'] ."' class='ta add add_to_curr_possition'>Přidat</a>
					|
					<a href='/clients/edit/".$client['Client']['id'] ."/domwin/only_show' class='ta client_info'>Info</a>
				</td></tr>";
			endforeach;
		else:
			echo "<tr><td colspan='2' class='text_center'>K dané kvalifikaci nebyl nalezen klient.</td></td>";
		endif;
	?>
</table> 
<script>
	/*
	 * Pridani klienta ze seznamu klientu na nominacni listinu
	 * dojde k vytvoreni noveho radku do seznamu nominacni listiny
	 * Vzdy dochazi k tomu, ze se klient presune pouze na nominacni listinu dane skupiny
	 * uzivatelu. klient se v seznamu klientu schova
	 * Dale dojde k inicializaci funkci pro odebrani z nominacni listiny a presunu do cekaci listiny
	 */
	function add_client_to_listina_table(tbody, data){
		var tbody = $(tbody);
		var td_html = '<td>' + data.Client.name + '</td><td>' + "<?php echo $logged_user['CmsUser']['name'];?>" + '</td><td>' + data.date + '</td><td>';
		
		 <?php if ($logged_user['CmsGroup']['id'] == 8): // pokud se jedna o EN?>
			td_html += '<em>Nepovoleno</em>';
		<?php else:?>
			td_html += '<a href="/orders_cast_posts/remove_from_nl/' + data.connection_id + '/' + data.Client.id + '" class="ta delete remove_listina">Odtranit</a> <a href="/orders_cast_posts/move_from_nl_to_cl/'+data.connection_id+'/' + data.Client.id + '" class="ta nabor_pozice move_cl" title="Presunout na ČL">Presunout na cl</a>';
			td_html += ' <a href="/clients/edit/' + data.Client.id + '/domwin/only_show" class="ta client_info" title="Informace o klientovi">Info</a>';
			td_html += ' <a href="/clients/add_activity/' + data.Client.id + '" class="ta phone" title="Přidání aktivity">Add Activity</a>';
	 	<?php endif;?>
		td_html += '</td>';		
		var tr = new Element('tr',{id:'tr_connection_id_' + data.connection_id}).inject(tbody).setHTML(td_html);
		
		if (tr.getElement('.remove_listina'))
		 	tr.getElement('.remove_listina').addEvent('click',remove_client_from_listina_table.bindWithEvent(this,[tbody.get('id')]));
		
		if (tr.getElement('.move_cl')) 
			tr.getElement('.move_cl').addEvent('click',move_client_to_cl_table.bindWithEvent(this,[tbody.get('id')]));
	  
        if (tr.getElement('.client_info')) 
			tr.getElement('.client_info').addEvent('click',call_domwin_client_info.bindWithEvent(this));
            
        if (tr.getElement('.phone')) 
			tr.getElement('.phone').addEvent('click',call_domwin_add_activity.bindWithEvent(this));    

    }
    
	
	/*
	 * Funkce pro schovani klientu v seznamu klientu
	 * Tato funkce se vola po pridani klientu do nominacni listiny
	 */
	function hide_client(id){
		var row = $('client_list_item_' + id);
		if (row)
			row.addClass('none');
	}
	
	/*
	 * inicializace funkci a JSON pozadavku pro kliknuti na klienta v seznamu klientu
	 * a naslednemu prevodu na nominacni listinu
	 */
    $('client_list_<?php echo $typ;?>').getElements('.add_to_curr_possition').addEvent('click', function(e){
		new Event(e).stop();
        var rel = this.getProperty('rel').split('=');
        var client_id = rel[1];
        var href = this.getProperty('href');
        
        new Request.JSON({
				 url: '/nabor_orders/is_blacklist/' + client_id + '/',
				 onComplete: function(json){
				    if (json){
						if (json.result === true){
							    do_move_client_from_clientlist_to_nl(href,client_id);
						} else {
							    if (confirm('Tento klient se nachází na černé listině. Chcete i přesto umístit klineta na nominační listinu?')){
									do_move_client_from_clientlist_to_nl(href,client_id);
							    }
						}
				    } else {
						alert('Chyba behem komunikace s DB');
				    }
				 }
		}).send();
	});
    
    function do_move_client_from_clientlist_to_nl(href,client_id){
        new Request.JSON({
			url: href,
       		data : {'data[history]':'save'},
			onComplete: function(json){
				if (json) {
				   	if (json.result === true) {
				   		add_client_to_listina_table('table_listina_<?php echo $typ;?>',json.data);
						hide_client(json.data.Client.id);
				   	}
				   	else {
				   		alert(json.message);
				   	}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).send();
    }
    
    //kvuli ajax obnoveni
    $$('.client_info').addEvent('click', call_domwin_client_info.bindWithEvent(this));
    
    if($('RequirementsForRecruitmentSettingCareerItemId').value != '')
        $$('.filtration_alphabet').addEvent('click', function(){
            $('client_list').setHTML('Nahrávám...');
    		new Request.HTML({
    			url: '/orders_cast_posts/load_client/' + $('RequirementsForRecruitmentSettingCareerItemId').value + '/<?php echo $company_id;?>/<?php echo $order_template_id;?>/<?= $typ;?>/' + this.getProperty('rel')+'/<?= (int)isset($regexp_show);?>/',
    			update: $('client_list')
    		}).send();
    	});
</script>   