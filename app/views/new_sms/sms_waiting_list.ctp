 <table class='table'>
    	<thead>
	    	<tr>
	    		<th>Klient</th>
	    		<th>Telefon</th>
                <th>Přiřadil</th>
	    		<th>Přiřazen</th>
	    		<th>Možnosti</th>
	    	</tr>
	</thead>
	<tbody id='table_cekaci_listina'>
			<?php if (isset($sms_waiting_list) && count($sms_waiting_list)>0):?>
			<?php foreach($sms_waiting_list as $key=>$client):?>
			<tr id='tr_connection_id_<?php echo $client['SmsWaitingList']['id'];?>' class='<?php echo(($key%2 )?'od ':'')?>'>
	    		<td><?php echo $client['Client']['name'];?></td>
	    		<td><?php echo $client['Client'][$client['Client']['mobil_active']];?> </td>
               	<td><?php echo $client['CmsUser']['name'];?></td>
                <td><?php echo $fastest->czechDate($client['SmsWaitingList']['created']);?></td>
	    		<td>
	    			<a href="/new_sms/remove_client_from_wl/<?php echo $client['SmsWaitingList']['id'];?>" class="ta delete remove_client_from_wl" onclick='return false;'>Odtranit</a> 
				</td>
	    	</tr>
			<?php endforeach;?>
			<?php endif;?>
	 </tbody>
</table>
<script type='text/javascript'>

if ($$('.remove_client_from_wl')){
	$$('.remove_client_from_wl').addEvent('click', function(e){
		new Event(e).stop();
        
        new Request.JSON({
    			url:this.href,
    			onComplete: (function(json){
    				if (json) {
    				   	if (json.result === true) {	
    				   	   this.getParent('tr').dispose();
    				   	}
    				   	else {
    				   		alert(json.message);
    				   	}
    				} else {
    					alert('Chyba aplikace');
    				}
    			}).bind(this)
		    }).send();
    });
}        	
</script>