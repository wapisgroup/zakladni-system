<div class="win_fce top">
<a href='/faqs/access_edit/<?php echo $faq_id;?>' class='button edit' id='company_access_add_new' title='Přidání'>Přidat přístup</a>
</div>
<table class='table' id='table_accesss'>
	<tr>
		<th>#ID</th>
		<th>Skupina</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($access_list) && count($access_list) >0):?>
    	<?php foreach($access_list as $access_item):?>
    	<tr>
    		<td><?php echo $access_item['FaqAccess']['id'];?></td>
    		<td><?php echo $access_item['CmsGroup']['name'];?></td>
    		<td>
    			<?php
                if($logged_user['CmsGroup']['id'] == 1){
                ?>
                	<a title='Odstranit přístup 'class='ta trash' href='/faqs/access_trash/<?php echo $access_item['FaqAccess']['id'];?>'/>trash</a>
        		<?php }?>
            
            </td>
    	</tr>
    	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této otázce nebyly nadefinovány přístupy</td>
	</tr>
	<?php endif;?>
</table>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyTemplateClose' class='button'/>
	</div>
<script>
	$('ListCompanyTemplateClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin');});

	$('table_accesss').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tento přístup?')){
			new Request.HTML({
				url:this.href,
				onComplete: function(){
				    domwin.loadContent('domwin');
				}
			}).send();
		}
	});
	
	$('domwin').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_edit',
			sizes		: [880,590],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
    
faqs</script>
</script>