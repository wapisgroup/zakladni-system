<div class="sll100">
    <?php echo $htmlExt->selectTag('group',$cms_group_list,null,array('label'=>'Skupina','class'=>'long','label_class'=>'long'));?> &nbsp;&nbsp;<a class="ta add" href="#" id="add_group" title="Přidat skupinu">Přidat skupinu</a><br />
    <?php echo $htmlExt->selectTag('user',$cms_user_list,null,array('label'=>'Uživatel','class'=>'long','label_class'=>'long'));?>&nbsp;&nbsp;<a class="ta add" href="#" id="add_user" title="Přidat jednotlivce">Přidat jednotlivce</a><br />
</div>
<fieldset>
				<legend>Příjemce</legend>
<table class='table' id='to_table'>
	<tr>
		<th>Příjemce</th>
		<th>Přidal</th>
		<th>Vytvořeno</th>
		<th>Ignorovat</th>
		<th>Propojení</th>
		<th></th>
	</tr>
	<?php
    //pr($connection_list);
         if (isset($connection_list) && count($connection_list) > 0):?>
		<?php foreach($connection_list as $item):?>
		<tr>
			<td><?php echo (!empty($item['CmsUser']['name']) ? $item['CmsUser']['name'] : $item['CmsGroup']['name']);?></td>
			<td><?php echo $item['AddCmsUser']['name'];?></td>
			<td><?php echo $fastest->czechDateTime($item['ConnectionMailTemplate']['created']);?></td>
			<td><?php echo (!empty($item['CmsUser']['name']) ? '<a href="#" class="switch i" id="'.$item['ConnectionMailTemplate']['id'].'" rel="'.$item['ConnectionMailTemplate']['ignore'].'" >'.$fastest->value_to_yes_no($item['ConnectionMailTemplate']['ignore']) : '');?></td>
			<td><?php echo (!empty($item['CmsGroup']['name']) && in_array($item['CmsGroup']['id'],array(2,3,4,18,30,31,32)) ? '<a href="#" class="switch c" id="'.$item['ConnectionMailTemplate']['id'].'" rel="'.$item['ConnectionMailTemplate']['company_connection'].'" >'.$fastest->value_to_yes_no($item['ConnectionMailTemplate']['company_connection']) : '');?></td>
			<td><a href="#" rel="<?php echo $item['ConnectionMailTemplate']['id'];?>" class="ta delete">Odebrat</a></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
</fieldset>
<script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 

        // pridani
		$$('.add').addEvent('click', function(e){
			new Event(e).stop();
            
            type = this.getPrevious('select').id;
            id = $(type).options[$(type).selectedIndex].value;
            title = $(type).options[$(type).selectedIndex].title;
            cms_user_name = "<?php echo $logged_user['CmsUser']['name'];?>";
            var allow_groups = ['2', '3', '4','18','30','31','32'];


            new Request.JSON({
				url:'/mail_templates/add_to_list/'+$('MailTemplateId').value+'/'+type.toLowerCase()+'/'+id,		
				onComplete:function(json){
					if(json){
					   if(json.result == true){
		     		       now = "<?php echo date('d.m.y H:i:s');?>";
                           
					       td_html = '<td>'+title+'</td>';
					       td_html += '<td>'+cms_user_name+'</td>';
					       td_html += '<td>'+now+'</td>';
					       td_html += '<td>'+(type == 'User' ? '<a href="#" class="switch i" rel="0" id="'+json.id+'">Ne</a>' : '')+'</td>';
					       td_html += '<td>'+(type == 'Group' && allow_groups.contains(id) ? '<a href="#" class="switch c" rel="0" id="'+json.id+'">Ne</a>' : '')+'</td>';
					       td_html += '<td><a href="#" class="ta delete" rel="'+json.id+'">Smazat</a></td>';
                           
					       var tr = new Element('tr').inject($('to_table')).setHTML(td_html);
	
                		   tr.getElement('.delete').addEvent('click',trash_to_list.bindWithEvent(this));
                           
                		   if(tr.getElement('.switch'))
                              tr.getElement('.switch').addEvent('click',switch_ignore.bindWithEvent(this));
					   
                           $(type).options[$(type).selectedIndex].dispose();
                           $(type).reset();
                       }
                       else{
                            alert('Chyba pri ulozeni prijemce. Prosim zkuste to pozdeji.');
                       }
					}
                    else
                        alert('Chyba aplikace');
				}
			}).send();
            	 
		});
    
        
        function trash_to_list(e){
        	e.stop();
    		var obj = e.target; 
    		new Request.JSON({
    			url:'/mail_templates/trash_to_list/'+obj.rel,
    			onComplete: (function(json){
    				if (json) {
    				   	if (json.result === true) {					
    						obj.getParent('tr').dispose();
    				   	}
    				   	else {
    				   		alert('Chyba pri ukladani');
    				   	}
    				} else {
    					alert('Chyba aplikace');
    				}
    			}).bind(this)
    		}).send();
        }
        
        /**
         * funkce meni ignore stav - cms_user
         * nebo propojeni firem - cms_group
         */
         function switch_ignore(e){
        	e.stop();
                        
    		var obj = e.target;
            
            var type = (obj.hasClass('i') ? 'i' : 'c');
            var rel =  (obj.rel==1 ? 0 : 1);
    		new Request.JSON({
    			url:'/mail_templates/switch_ignore/'+type+'/'+obj.id+'/'+rel,
    			onComplete: (function(json){
    				if (json) {
    				   	if (json.result === true) {					
    						obj.setHTML((obj.rel==1 ? 'Ne' : 'Ano'));
                            obj.rel = rel;
    				   	}
    				   	else {
    				   		alert('Chyba pri ukladani');
    				   	}
    				} else {
    					alert('Chyba aplikace');
    				}
    			}).bind(this)
    		}).send();
        }
		

		
		//smazani radku prijemce
		$('to_table').getElements('.delete').addEvent('click',trash_to_list.bindWithEvent(this));
		$('to_table').getElements('.switch').addEvent('click',switch_ignore.bindWithEvent(this));


</script>