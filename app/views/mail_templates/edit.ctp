<form action='/mail_templates/edit/' method='post' id='mail_templates_edit_formular'>
	<?php echo $htmlExt->hidden('MailTemplate/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<li class="ousko"><a href="#krok1">Nastavení příjemců</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní informace</legend>
				<?php echo $htmlExt->input('MailTemplate/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->textarea('MailTemplate/text',array('tabindex'=>2,'label'=>'Popis','class'=>'long','label_class'=>'long','style'=>'height:200px'));?> <br class="clear">
			</fieldset>			
		</div>
        <div class="domtabs field">
            <?php echo $this->renderElement('../mail_templates/to_list');?>
        </div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 

	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		
		
			new Request.JSON({
				url:$('mail_templates_edit_formular').action,		
				onComplete:function(){
					click_refresh($('MailTemplateId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('mail_templates_edit_formular'));
	
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>