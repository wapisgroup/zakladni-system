<div class="domtabs admin2_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Seznam Profesí</a></li>
            <li class="ousko"><a href="#krok2">Seznam Zaměstnanců</a></li>
		</ul>
</div>
	<div class="domtabs admin2_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Profese</legend>
                <div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>Profese</th>
						<th>Počet hodin</th>
					</tr>
					<?php 
                   // pr($show_detail);
					if (isset($show_detail) && count($show_detail) > 0){
						foreach($show_detail as $detail):									
						?>
						<tr>
							<td><?php echo $detail['CompanyWorkPosition']['name'];?></td>
							<td><?php echo $detail['0']['celkem_hodin'];?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td colspan='2'>Žádní zaměstnanci</td></tr>"; 
					?>
					</table></div> 
				<br />
			</fieldset>
		</div>
        <div class="domtabs field">
			<fieldset>
				<legend>Zaměstnanci</legend>
				<table class='table' id='rating_table'>
					<tr>
						<th>Zaměstnanec</th>
						<th>Počet hodin</th>
					</tr>
					<?php 
					if (isset($zamestnanci_list) && count($zamestnanci_list) > 0){
						foreach($zamestnanci_list as $zam):?>
						<tr>
							<td><?php echo $zam['Client']['name'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['celkem_hodin'];?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádní zaměstnanci</td></tr>"; 
					?>
				</table> 
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin2_dom'}); 
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');});
</script>