<?php
function list_element($data, $settings = array(),$level = 0 ){
	$modelName = 'Defualt';
	$fieldName = 'name';
	$ul_id = 'tree_ul_id';
	$prefix = null;
	
	extract($settings);
	
	//$data = generate_links($data,$settings);
	
	$tabs = "\n" . str_repeat("\t", $level * 2);
	$li_tabs = $tabs . "\t";
    
	$output = $tabs. "<ul".(($ul_id != null)?" id='".$ul_id."'":"").">";
	if (isset($data) && count($data) > 0){
		foreach ($data as $key=>$val){
			if (substr($val['url'],0,8) == 'domwin::'){
				$class_ = 'menu_domwin';
				$val['url'] = substr($val['url'],8);
			} else {
				$class_ = '';
			}
			$output .= $li_tabs . "<li><a class='".$class_."' title='".$val['caption']."' href='".$val['url']."'>".$val['caption']."</a>";
			if(isset($val['child']) && is_array($val['child']) && count($val['child'])>0){
				$settings['ul_id'] = null;
				$output .= list_element($val['child'], $settings, $level+1);
				$output .= $li_tabs . "</li>";
			} else {
				$output .= "</li>";
			}
		}
	}
	$output .= $tabs . "</ul>";
        return $output;
 } 
?>
<?php echo list_element(isset($menu_items)?$menu_items:array() , array('ul_id'=>'navmenu')); ?>
