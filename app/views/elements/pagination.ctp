<?php //pr($paging);?>
<?php if(isset($paging)):?>
<div class="pagination">
<?php
    if($pagination->setPaging($paging)):
    $leftArrow = 'Předchozí strana';
    $rightArrow = 'Další strana';
    
    $prev = $pagination->prevPage($leftArrow,false);
    $prev = $prev?$prev:$leftArrow;
    $next = $pagination->nextPage($rightArrow,false);
    $next = $next?$next:$rightArrow;

    $pages = $pagination->pageNumbers(" | ");
	/*echo '<div>';
    echo '<div style="float:left">'.$pagination->result().'</div>';
    echo '<div style="float:right">'.$prev.' '.$pages.' '.$next.'</div>';
    echo '<div style="clear:both"></div>';
    echo '</div>';*/
	
//	echo '<div style="text-align:center">'.$pagination->result().'<br/>'.$prev.' '.$pages.' '.$next.'</div>';
	//echo '<div style="text-align:center">'.$prev.' '.$pages.' '.$next.'</div>';
	//if ($paging['pageCount']>1){
	echo '<span class="pag_left">'.$prev.'</span>';
	echo '<span class="pag_right">'.$next.'</span>';
	echo '<span class="pag_center">'.$pages.'</span>';
	echo ' <br class="clear" /> ';
	//}
    endif;
	echo '<a href="'.$pagination->_generateUrl().'" id="refresh">Refresh</a>';
	
	
?>
</div> 

 <script language="JavaScript" type="text/javascript">

$("refresh")
	.addEvent("click",click_refresh)
	.addClass('hidden');
function click_refresh(id){
	//automatic_logout.resetTime();
	//preloader(true);
	new Request.HTML({
		url:$('refresh').getProperty('href'),
		update:"items",
		onComplete: function(){
			preloader(false);
			if (id && $('tr_id_' + id)) {
				$('tr_id_' + id).addClass('last_change'); 
				if ($('tr_id_' + id).hasClass('varianta_item')){
					var classname = $('tr_id_' + id).getPrevious('.normal_item').getElement('.parent_varianty').getProperty('href').substring(1);
					$('tabulka_item').getElements('.' + classname).setStyle('display','table-row');
				}
			}
				
		}
	}).send()
};

</script>
<?php endif;?>