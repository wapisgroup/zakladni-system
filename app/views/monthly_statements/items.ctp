<?php
$sum = 0;
$currency = 0;
$centers = $companies = array();

function mydate($type,$date_db,$date_selected, $format = false){
    list($year,$month,$date) = explode('-',$date_db);
    $date = $year.'-'.$month;
    $fastest = new FastestHelper();
    /**
     * Jedná se o stejný datum tedy pouzžijeme záznam z db
     */
    if($date == $date_selected){
        $datum = $date_db;
    }
    else{//JINE datum pouzijem tedy od prvniho/posledniho v mesici podle typu
        //$_d = new DateTime$date_selected);
        list($year,$month) = explode('-',$date_selected);
        $pocet_dnu_v_mesici = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $datum = $date_selected.($type == 0 ?'-01':'-'.$pocet_dnu_v_mesici);
    }    
    
    if ($format === true)
	return $fastest->czechDate($datum);
    else
	return $datum;
}

function date_dif($start, $end){
    return round(((strtotime($end) - strtotime($start))/60/60/24) + 1,0);
}

function get_stredisko($data){
    if($data['ConnectionAuditEstate']['centre'] != '')
        return $data['ConnectionAuditEstate']['centre'];
    else if($data['ConnectionAuditEstate']['company_foreign_id'] > 0)
        return $data['CompanyForeign']['ulice'].', '.$data['CompanyForeign']['mesto'];
    else if($data['ConnectionAuditEstate']['client_foreign_id'] > 0)
        return $data['ClientForeign']['ulice'].', '.$data['ClientForeign']['mesto'];
    else
        return null;        
}

?>
<div id="obal_tabulky">
<table class='table' id='rating_table'>
			<tr>
			    <th>Majetek</th>
			    <th>IMEI/SN/VIN</th>
			    <th>Osoba</th>
			    <th>Středisko</th>
                <th>Činnost</th>
                <th>Zakázka</th>
			    <!--th>Celkem za mesic</th-->
			    <th>Měsíčně</th>
			    <th>Pronájem od</th>
			    <th>Pronájem do</th>
			</tr>
			<?php 
			    if (isset($estate_list) && count($estate_list) > 0){

				foreach($estate_list as $item): 
                
                    $stredisko = $item['0']['stredisko'];
                    if($stredisko == -1){$stredisko = get_stredisko($item);}
                    ?>
        			<tr>
        			    <td>(<?= $item['ConnectionAuditEstate']['id'];?>)<?php echo $item['AuditEstate']['name'];?></td>
        			    <td><?php echo $item['AuditEstate']['imei_sn_vin'];?></td>
        			    <td><?php echo $item['0']['klient'];?></td>
        			    <td><?php echo $stredisko;?></td>
        			    <td><?php echo $item['0']['cinnost'];?></td>
                        <td><?php echo $item['0']['spolecnost'];?></td>
        			    <!--td><?php echo $fastest->price($item['AuditEstate']['hire_price'],$currency_list[$item['AuditEstate']['currency']],null,2);?></td-->
        			    <td><?php

        			    $pocet_dnu = date_dif(mydate(0,$item['ConnectionAuditEstate']['created'],$year.'-'.$month), mydate(1,$item['ConnectionAuditEstate']['to_date'],$year.'-'.$month));
                        $pocet_dnu_v_danem_mesici = date('t',strtotime("$year-$month-01"));

                        if($item['ConnectionAuditEstate']['created'] == $item['ConnectionAuditEstate']['to_date']){
                            if ($item['ConnectionAuditEstate']['fa'] == 1){
                                $price = $item['AuditEstate']['hire_price'] / $pocet_dnu_v_danem_mesici;
                            } else {
                                $price = 0;
                            }
                        }
                        else{

        				    $price = $item['AuditEstate']['hire_price'] / $pocet_dnu_v_danem_mesici * $pocet_dnu;
        				}
                        echo $fastest->price($price,$currency_list[$item['AuditEstate']['currency']],null,2);
        			    ?></td>
        			    <td><?php echo mydate(0,$item['ConnectionAuditEstate']['created'],$year.'-'.$month,true);?></td>
        			    <td><?php echo mydate(1,$item['ConnectionAuditEstate']['to_date'],$year.'-'.$month,true);?></td>
        			</tr>
        			<?php 
    			    $sum += $price;
    			    $currency = $item['AuditEstate']['currency'];
    		
    			    //pridani do listu dle stredisek
    			    $centers[$stredisko] = (isset($centers[$stredisko])? $centers[$stredisko] : 0) + $price;
    		
                    //pridani do listu dle stredisek
    			    $companies[$item['0']['spolecnost']] = (isset($companies[$item['0']['spolecnost']])? $companies[$item['0']['spolecnost']] : 0) + $price;

        	?>
			<?php endforeach; ?>
			<tr>
			    <td colspan=7>&nbsp;</td>
			</tr> 
			<tr id='Sum'>
			    <td colspan=3></td>
			    <td><strong>Celkem</strong></td>
			    <td><?=$fastest->price($sum,$currency_list[$currency],null,2);?></td>
			    <td colspan = 2>&nbsp;</td>
			</tr>
			<?php } else { ?>
			<tr id='NoneSearch'>
			    <td colspan=7>Žádný majetek</td>
			</tr>
			<?php };?>
		    </table>
</div> 
<br />
<div class="sll">
<?php foreach($centers as $name=>$center){
echo 'Celkem za středisko: <strong>'.$name.'</strong> = '.$fastest->price($center,$currency_list[$currency],null,2).'<br />';
}?>
</div>
<div class="slr">
<?php foreach($companies as $name=>$sum_c){
echo 'Celkem za podnik: <strong>'.$name.'</strong> = '.$fastest->price($sum_c,$currency_list[$currency],null,2).'<br />';
}?>
</div>
<br/>