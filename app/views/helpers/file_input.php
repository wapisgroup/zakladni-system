<?php 
class FileInputHelper extends Helper {
	var $helpers = array('HtmlExt');
	var $btns = array(
		array('browse.png','Procházet','icon browse_icon'),
		array('delete.png','Smazat','icon2 smazat_icon'),
		array('upload.png','Nahrát','icon2 upload_icon'),
		array('deactive.png','Zastavit','icon2 stop_upload_icon'),
		array('info.gif','Náhled','icon2 nahled_icon')
	);
	
	function render($fieldName, $attributes = array()) {
		$output = array();
		$attributes['id'] = $id = Inflector::camelize(strtr($fieldName,array('/'=>'_')));
		$input = 'data['.strtr($fieldName,array('/'=>'][')).']';
		# jestlize je v attributes hodnota value tak ji pouzij, jinak ji automaticky vytahni z  this->data
		if (isset($attributes['value']))
			$value = $attributes['value'];
		else
			$value = $this->HtmlExt->tagValue($fieldName);
			
		$output[] = '<div id="'.$id.'Uploader" style="position:relative">';
		# pokud je nadefinoval atribut label, tak ho zobraz
		if (isset($attributes['label'])) 
			$output[] = '<label>'.$attributes['label'].':</label>';	
		
		$output[] = '<input class="input_file_over text" type="text" readonly="readonly" value="'.$value.'" />';
		$output[] = '<input type="file" class="input_file" title="Procházet" class="icon" name="upload_file"/>';
		
		//if(!empty($value))
		foreach($this->btns as $img)
			$output[] = '<img src="/css/fastest/upload/'.$img[0].'" title="'.$img[1].'" alt="'.$img[1].'" class="'.$img[2].'" />';
		
		
		$output[] = '<input type="hidden" id="'.$id.'" class="db_element" name="'.$input.'" value="'.$value.'"/>';
		
		// START progress
		$output[] = '<div class="progress_div"><div class="progress_time"><strong class="upload_title">Průběh nahrávání, soubor: <span>soubor.ex</span></strong><div>';
		$output[] = '<div class="progress_shower"></div>';
		$output[] = '<div class="inner">';
		$output[] = '<span>Rychlost:</span><em>0 Kb/s</em>';
		$output[] = '<span>Průměrná rychlost:</span><em>0 Kb/s</em>';
		$output[] = '<span>Velikost:</span><em>0 Kb</em><br class="clear" />';
		$output[] = '</div></div></div></div>';
		// END progress
		
		$output[] = '</div>';
		$output[] = '<script type="text/javascript">new uploader('.json_encode($attributes).');</script>';
		
		return implode("\n",$output);
	}
}
?>