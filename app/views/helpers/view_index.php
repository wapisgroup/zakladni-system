<?php
class ViewIndexHelper extends AppHelper {
	var $helpers = array('HtmlExt','Fastest');
	
	function generateColgroup($arr = array(), $posibility = true, $checkbox = true){
            //pr($arr);
            $output = array('<colgroup>');
            if($checkbox) $output[] = '<col class="ta_check"/>';
            foreach($arr as $col)
                    $output[] = '<col'.(($col[1] != null)?' class="'.$col[1].'"':'').' '.(isset($col[2])?' class="colgroup_'.$col[2].'"':'').' />';
            if($posibility) $output[] = '<col class="posibility"/>';
            $output[] = '</colgroup>';
            return implode("\n",$output);
	}
	
	function generate_td($item, $td, $viewVars = array()){
            $value =  @$item[$td['model']][$td['col']];
            switch($td['type']){
                case 'text':
                    if (!empty($td['fnc'])){
                        list($fnc, $params) = explode('#', $td['fnc']);
                        return $this->Fastest->$fnc($value,$params);
                    } else {
                        return $value;
                    }
                    break;
                case 'plaintext':
                    return strip_tags($value);
                    break;    
                case 'procenta':
                    return $value .(empty($value)?'':'%');
                    break;
                case 'cislo_uctu':
                    return ltrim($value,'-');
                    break;    
                case 'download':
                    return ((($value!='')?'<a href="'.$td['fnc'].'/null/null/'.$value.'">Otevřít soubor</a>':''));
                    break;
                case 'datetime':
                    return ($value!='0000-00-00 00:00:00' && $value != null)?$this->Fastest->czechDateTime($value,$td['fnc']):'';
                    break;
                case 'date':
                    return ($value !='0000-00-00' && $value !='0000-00-00 00:00:00' && $value != null)?$this->Fastest->czechDate($value, $td['fnc']):'';
                    break;
                case 'var':
                    return @$viewVars[$td['fnc']][$value];
                    break;
                case 'viewVars':
                    return @$viewVars[$td['fnc']][$value];
                    break;
                case 'hidden':
                    return;
                    break;
                case 'money':
                    return number_format($value, 0, ',', ' ');
                    break;
                case 'money2':
                    return number_format($value, 2, '.', ' ');
                    break;  
                case 'pocet_dni_po_splatnosti':
                    $po_splatnosti = (strtotime(date('Y-m-d')) - strtotime($item[$td['model']]['datum_splatnosti'])) / 3600 /24;
                    if ($po_splatnosti > 0) {
                        if ($item[$td['model']]['datum_uhrady'] != '0000-00-00')
                            return 'Uhrazeno';
                        else 
                            return $po_splatnosti;
                    } else
                        return ;
                    break;
		}
	}
	
	function generate_posibility($renderSetting,$item = array(),$logged_user){
		$permission_list = $logged_user['CmsGroup']['permission'][$renderSetting['controller']];
		$posibility = $renderSetting['posibility'];
		$id = $item[$renderSetting['items']['id']['model']]['id'];
		$basic_path = $this->params['url']['url'];
        $posibility_link = $output = array();
		//pro coo_double
		$model_class_coo = ($renderSetting['items']['id']['model'] == 'CompanyView' ? 'CompanyView' : 'Company' );
		if($renderSetting['items']['id']['model'] == 'ClientView')
			$model_class_coo = 'ClientView';
				
	    //inicializace posibility linku
        if(isset($renderSetting['posibility_link']))
            $posibility_link = $renderSetting['posibility_link'];
	
        
		// rendrovani pro skupiny podle práv i mimo companies, protoze zde neni uveden group_id
		if(isset($renderSetting['permGroupId']))
			$permGroupName = $logged_user['CmsGroup']['permission']['companies']['group_id'];

		foreach($posibility as $action){
		    $link = null;
			list($act, $label, $perm) = explode('|',$action);
            
            /**
             * pokud je nastaven link v controlleru k teto akci zavolej ji
             */
            if(array_key_exists($act,$posibility_link))
                $link = $posibility_link[$act];

			if ( // moznost  [vse]  - pro vsechny polozky
				(isset($permission_list[$perm]) && $permission_list[$perm] == 3)
			||	//  nebo moznost [jen sve] -  porovnani cms_user_id hidden
				(
					isset($permission_list[$perm]) && $permission_list[$perm] == 2
				&&				
					isset($item[$renderSetting['items']['id']['model']]['cms_user_id'])
				&&
					$item[$renderSetting['items']['id']['model']]['cms_user_id'] == $logged_user['CmsUser']['id']
				)
			||	//  nebo moznost [jen sve] - controller companies
				(
					isset($permission_list[$perm]) && $permission_list[$perm] == 2
				&&
					isset($permission_list["group_id"])  && $permission_list["group_id"]!='coordinator_double'
				&&
					isset($item[$model_class_coo][$permission_list["group_id"]])
                &&    
                    $item[$model_class_coo][$permission_list["group_id"]] == $logged_user['CmsUser']['id']
				)
			||	//  nebo moznost [jen sve] - jedna se o nove nastaveni pro coordinatora v pripade double
				(
					isset($permission_list[$perm]) && $permission_list[$perm] == 2
				&&
					isset($permission_list["group_id"]) && $permission_list["group_id"]=='coordinator_double'
				&&
					(
						$item[$model_class_coo]['coordinator_id'] == $logged_user['CmsUser']['id']
						||
						$item[$model_class_coo]['coordinator_id2'] == $logged_user['CmsUser']['id']
					)
				)
				
			||	//  nebo moznost [jen sve] - mimo controller companies, nutne v possibillity mit hidden polozky  CM,SM,COO _id
				(
					isset($permission_list[$perm]) && $permission_list[$perm] == 2
				&&
					isset($renderSetting['permGroupId'])
				&&
					$item[$renderSetting['items'][$permGroupName]['model']][$permGroupName] == $logged_user['CmsUser']['id']
				)
			||	//  nebo specialni pro vypis clientu pro client_manager_id pokud ma moznost[jen sve]  pro odebrani ze zamestani
				(
					isset($permission_list[$perm]) && $permission_list[$perm] == 2
				&&
					isset($item[$renderSetting['modelClass']]['client_manager_id'])
				&&
					$item[$renderSetting['modelClass']]['client_manager_id'] == $logged_user['CmsUser']['id']
				)
   	        ||	//  nova moznost typ napojeni pro moznosti
                (
    				isset($permission_list[$perm]) && $permission_list[$perm] == 2
    				&&
    				isset($permission_list["group_id_posibility"])
                    &&
                    $item[$renderSetting['modelClass']][$permission_list["group_id_posibility"]] == $logged_user['CmsUser']['id']
                ) 
            ||	//  pristup k posibiliy jen sve, dle jednotlive pozice v primarni tabulce controlleru
                //  napr u estoru nakupujici, stavbyvedouci apod 
                (
                    (
    					isset($permission_list[$perm]) && $permission_list[$perm] == 2
    				    &&
    					isset($renderSetting["only_his_posibility"])
                    )
                    &&
                    (
                       (
                           !is_array($renderSetting["only_his_posibility"])
                           && 
    					   $item[$renderSetting['modelClass']][$renderSetting["only_his_posibility"]] == $logged_user['CmsUser']['id']    
    				   )
                       ||
                       (
                          is_array($renderSetting["only_his_posibility"])
                          &&
                          self::find_group_id_allow($item[$renderSetting['modelClass']],$renderSetting["only_his_posibility"],$logged_user['CmsUser']['id'])
                       )
                    )
                
                )         
			){
				//genereuj moznosti
				switch($act){
					case 'status':
						$status = $status1 = $item[$renderSetting['modelClass']]['status'];
						if($renderSetting['controller'] == 'cms_users')	
							$status = '_new'.$item[$renderSetting['modelClass']]['status'];
						$output[] = "<a title='$label' class='ta status$status' onclick='return false;' href='/$basic_path/$act/$id/$status1/'>$act</a>";
						break;
                    case 'add_estate':
						$status = $item[$renderSetting['modelClass']]['stav'];
                        if ($status == 0){
                            $url = "/$basic_path/add_estate/$id/";
                            $label = 'Přidělení majetku';
                        } else { 
                            $url = "/$basic_path/remove_estate/$id/";
                            $label = 'Vrácení majetku';
                        }
						$output[] = "<a title='$label' class='ta add_estate_$status' href='$url' onclick='return false' >$act</a>";
						break;
                    case 'delete_estate':
						$status = $item[$renderSetting['modelClass']]['stav'];
                        if ($status == 0){
						  $output[] = "<a title='$label' class='ta trash' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
					    }
                    	break;    
					case 'stop':
						$status = $item[$renderSetting['items']['stop_payment']['model']]['stop_payment'];
						$output[] = "<a title='$label' class='ta stop$status' onclick='return false;' href='/$basic_path/$act/$id/$status/'>$act</a>";
						break;
					case 'edit':
                        if($link == null)
                            $link = $id;
                        else
                            $link = self::genereate_link($item,$link);    
						$output[] = "<a title='$label' class='ta edit' onclick='return false;' href='/$basic_path/$act/$link'>$act</a>";
						break;
                   case 'attachs':
                        if($link == null)
                            $link = $id;
                        else
                            $link = self::genereate_link($item,$link);    
                        $class_act = $act;
                        if(isset($item[0]['count_attachment']) && $item[0]['count_attachment'] > 0)
                            $class_act = 'attach_exist';    
						$output[] = "<a title='$label' class='ta $class_act' onclick='return false;' href='/$basic_path/$act/$link'>$act</a>";
						break; 
                   case 'download':
                        if($link == null)
                            $link = $id;
                        else
                            $link = self::genereate_link($item,$link);    
                            
						$output[] = "<a title='$label' class='ta $act' href='/$basic_path/$act/$link'>$act</a>";
						break;        
                    case 'show_podklady':
						$company_id = $item[$renderSetting['items']['company']['model']]['id'];
						$year = $item[$renderSetting['items']['year']['model']]['year'];
						$month = $item[$renderSetting['items']['month']['model']]['month'];
						$output[] = "<a title='$label' class='ta show' onclick='return false;' href='/$basic_path/$act/$company_id/$year/$month'>$act</a>";
						break;    
					case 'trash':
						$output[] = "<a title='$label' class='ta trash' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;
					case 'odpracovane_hodiny':
						if(isset($item[$renderSetting['modelClass']]['id']) && $item[$renderSetting['modelClass']]['id'] != "")
							$id = $item[$renderSetting['modelClass']]['id'];
                            
                        if($link == null)
                            $link = $id."/".CURRENT_YEAR."/".CURRENT_MONTH;
                        else
                            $link = self::genereate_link($item,$link);    
						$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$link'>$act</a>";
					    
						//$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id/".CURRENT_YEAR."/".CURRENT_MONTH."'>$act</a>";
						break;
                    case 'ucetni_dokumentace':
						if(isset($item['ConnectionClientRequirement']['id']) && $item['ConnectionClientRequirement']['id'] != "")
							$id = $item['ConnectionClientRequirement']['id'];
					        
                        $forma = $item['CompanyMoneyItem']['forma'];
	                       
                        $output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id/$forma'>$act</a>";
						
                        break;  
                   case 'client_info': //zobrazeni karty klienta
						$client_id= $item[$renderSetting['items']['client_id']['model']]['id'];
						$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$client_id/'>$act</a>";
						break;      
					case 'uzavrit_odpracovane_hodiny':	
                        $stav = $item[$renderSetting['items']['stav']['model']]['stav'];
                        if($link == null){
    						$id = $item[$renderSetting['items']['stav']['model']]['id'];
    						$company_id = $item[$renderSetting['modelClass']]['company_id'];
    						$client_id = $item[$renderSetting['modelClass']]['client_id'];
                            $link = $client_id."/".$company_id."/".$id."/".CURRENT_MONTH."/".CURRENT_YEAR."/";
                        }    
                        else
                            $link = self::genereate_link($item,$link);    
							
						
						if ($stav == 1)
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$link'>$act</a>";
						break;
                    case 'autorizace_dochazky':	
                        $stav = $item[$renderSetting['items']['stav']['model']]['stav'];
                        if($link == null){
    						$id = $item[$renderSetting['items']['stav']['model']]['id'];
                            $link = $id;
                        }    
                        else
                            $link = self::genereate_link($item,$link);    
							
						
						if ($stav == 2)
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$link'>$act</a>";
						break;	    	
					case 'nabor_pozice':		
						$label .= ' - '.$item[$renderSetting['items']['firma']['model']]['name'];
						$label .= ' - '.$item[$renderSetting['items']['id']['model']]['name'];
						$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;
					case 'autorizace':
						$stav = $item[$renderSetting['items']['stav']['model']]['stav'];
						if ($stav == 1)
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;
                    case 'return_order':
						$stav = $item[$renderSetting['modelClass']]['status'];
						if ($stav == 1)
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;    
                    case 'auth':
						$stav = $item[$renderSetting['items']['id']['model']]['prevod'];
						if ($stav == 0)
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;    
					case 'rozvazat_prac_pomer':
                        if($link == null)
                            $link = $id;
                        else
                            $link = self::genereate_link($item,$link);    
                    
						$stav = $item[$renderSetting['modelClass']]['stav'];
						$klient = isset($item[$renderSetting['modelClass']]['name']) ? "- s klientem ".$item[$renderSetting['modelClass']]['name'] : "";
						if ($stav == 2)
							$output[] = "<a title='$label $klient' class='ta $act' onclick='return false;' href='/$basic_path/$act/$link'>$act</a>";
						break;
                    case 'employ_on_project':
                        if($link == null)
                            $link = $id;
                        else
                            $link = self::genereate_link($item,$link);    
                    
						$stav = $item[$renderSetting['modelClass']]['stav'];
                        $klient = isset($item[$renderSetting['modelClass']]['name']) ? "- klienta ".$item[$renderSetting['modelClass']]['name'] : "";
						if ($stav != 2)
							$output[] = "<a title='$label $klient' class='ta $act' onclick='return false;' href='/$basic_path/$act/$link'>$act</a>";
						break;      
                    case 'duplicity_client':
						$stav = $item[$renderSetting['modelClass']]['stav'];
						$parent_id = $item[$renderSetting['modelClass']]['parent_id'];
						$klient = isset($item[$renderSetting['modelClass']]['name']) ? "- s klientem ".$item[$renderSetting['modelClass']]['name'] : "";
						if ($stav == 2 && $parent_id == 0)
							$output[] = "<a title='$label $klient' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;       
					case 'zmena_pp':
    	            case 'zmena_pp_new':
						$stav = $item[$renderSetting['modelClass']]['stav'];
						$company_id = $item['ConnectionClientRequirement']['company_id'];
						$company_work_position_id = $item['ConnectionClientRequirement']['company_work_position_id'];
						$klient = isset($item[$renderSetting['modelClass']]['name']) ? "- s klientem ".$item[$renderSetting['modelClass']]['name'] : "";
						if ($stav == 2)
							$output[] = "<a title='$label $klient' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id/$company_id/$company_work_position_id'>$act</a>";
						break;
					case 'zmena_pp_admin':
						$stav = $item[$renderSetting['modelClass']]['stav'];
						$company_id = $item['ConnectionClientRequirement']['company_id'];
						$klient = isset($item[$renderSetting['modelClass']]['name']) ? "- s klientem ".$item[$renderSetting['modelClass']]['name'] : "";
						if ($stav == 2)
							$output[] = "<a title='$label $klient' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id/$company_id'>$act</a>";
						break;
					case 'money':
						$id = $item[$renderSetting['items']['id']['model']]['id'];
						$stav = $item[$renderSetting['items']['stav']['model']]['stav'];
						$stop_payment = $item[$renderSetting['items']['stop_payment']['model']]['stop_payment'];
						if ($stav == 3 && $stop_payment == 0) // neni pozastavena vyplata a je schvalena
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;
					case 'money2':
						$year = $item[$renderSetting['items']['id']['model']]['year'];
						$stav = $item[$renderSetting['items']['id']['model']]['stav'];
						$month = $item[$renderSetting['items']['id']['model']]['month'];
						if ($stav == 3 || $stav == 4)
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id/$year/$month'>$act</a>";
						break;
                    case 'uhrazeno':
						$id = $item[$renderSetting['items']['id']['model']]['id'];
						$uhrazeno = $item[$renderSetting['items']['date_of_paid']['model']]['date_of_paid'];
					
                    	if ($uhrazeno == '' || $uhrazeno == 0) // neni vyplneno datum uhrazeni
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;
				    case 'pick_up':
						$id = $item[$renderSetting['items']['id']['model']]['id'];
						$vyzvednuti = $item[$renderSetting['items']['date_of_pickup']['model']]['date_of_pickup'];
					
                    	if ($vyzvednuti == '' || $vyzvednuti == 0) // neni vyplneno datum vyzvednuti
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						break;
					case 'campaign_stav':
						$stav 	= $item[$renderSetting['items']['id']['model']]['stav'];
						$email 	= $item[$renderSetting['items']['id']['model']]['mail'];
						
						if ($stav == 1 && !empty($email)){
							
						} else {
							$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$id'>$act</a>";
						}
						break;
                    case 'fakturovano':
						$stav = $item['ConnectionFaAuditEstate']['id'];
						
                        if($stav == ''){
    						if($link == null)
                                $link = $id;
                            else
                                $link = self::genereate_link($item,$link);    
                        
                            $output[] = "<a title='$label' class='ta $act' target='_blank' href='/$basic_path/$act/$link'>$act</a>";
                        }
                        break;
                    case 'fakturovano_client':
                        $stav = '1' ; //$item['ClientBuy']['id']; //if(ClientBuy.pay_date != "","Fakturováno","Nefakturováno") as stav

                        if($stav == ''){
                            if($link == null)
                                $link = $id;
                            else
                                $link = self::genereate_link($item,$link);

                            $output[] = "<a title='$label' class='ta $act' target='_blank' href='/$basic_path/$act/$link'>$act</a>";
                        }
                        break;
                    case 'print_detail':
                        if($link == null)
                            $link = $id;
                        else
                            $link = self::genereate_link($item,$link);    
                    
                        $output[] = "<a title='$label' class='ta $act' target='_blank' href='/$basic_path/$act/$link'>$act</a>";
                        break;    
					default:
                        /**
                         * nove nastaveni default
                         * pokud je v controlleru renderSettingu possibility_link tak nastav link z nej pro prislusnou akci
                         * switch po te bude zbytecny, treba i predelat controller aby se posibilty zadavali jako slozene pole, ne jen string
                         * nejlepe objektove vytvotrit tridu ktera toto cele nastavi 
                         */                   
                        if($link == null)
                            $link = $id;
                        else
                            $link = self::genereate_link($item,$link);    
                            
						$output[] = "<a title='$label' class='ta $act' onclick='return false;' href='/$basic_path/$act/$link'>$act</a>";
						//die($act.' Nenadefinovano!!');
						break;
				}
			}
		}
		return implode("\n",$output);
	}
	
	function filtration($renderSetting,$viewVars,$logged_user){
	    $set = $renderSetting['filtration'];
	    $output = array();
	    $output[] = "<form id='filtration_view_index' action='".(isset($renderSetting['filtration_url'])?$renderSetting['filtration_url']:'/'.$renderSetting['controller'].'/')."'><div id='pole'>";
				
            if(isset($renderSetting['filtration_update']))
                $output[] = "<input type='hidden' name='update_box_name' value='".$renderSetting['filtration_update']."'/>";
        
            /**
             * Predefinovana filtrace uz v url
             */
            if(isset($this->params['url'])){
		foreach($this->params['url'] as $key_u=>$val_u){
		    if(strpos($key_u,'filtration_') !== false){
		        $output[] = "<input type='hidden' name='".$key_u."' value='".$val_u."'/>";
		    }
                }
	    }         
                
	    foreach($set as $key => $data){
                $original_key = $key;
                $prefix = (substr($key,0,3) == 'GET')?'':'filtration_';
                $key = 	(substr($key,0,3) == 'GET')?substr($key,4):$key;
                
                @list($type, $caption, $datas) = explode('|',$data);
                switch ($type){
                    case 'text':
                        $output[] = "<span><label>$caption:</label><input type='text' id='filtr_$key' name='$prefix$key' class='fltr_input'  /> </span>";			
                        break;
                    case 'select':
                    case 'select2':
                        $povoleni = null;
                        if(substr($key,-10) == 'admin_ctrl'){
                            $povoleni = array(1,4,5,9);
                            $output_zaloha = $output;
                            $keys = explode('#',$key);
                            $key = $keys[0];
                        }                    
                        
                        $output[] = "<span><label>$caption:</label><select id='filtr_$key' name='$prefix$key' class='fltr_select'>"; 
                        if (isset($viewVars[$datas])){
                            $output[] = "<option value='".(($key=='recruiter')?-1:'')."'  ".(($key=='recruiter' && !in_array($logged_user['CmsGroup']['id'],array(1,5)))? 'disabled="disabled"' : '').">-- všechny --</option>";
                                    
                            foreach($viewVars[$datas] as $key_value => $caption){
                                /**
                                * pokud se jedna o select2
                                * value obsahuje to co capiton
                                * vyuziva se zde tzv. fulltext
                                */
                                if($type == 'select2')
                                    $key_value = $caption;
								
				    $selected =  (isset($_GET[$prefix.$key]) && $_GET[$prefix.$key] == $key_value)?'selected="selected"':'';					
                                    if (substr($key_value,0,8) == 'DISABLED')
                                        $output[] = "<option title='$caption' value='' disabled='disabled' style='background:#F3E4EE'>$caption</option>";
                                    else
                                        $output[] = "<option title='$caption' value='$key_value' $selected>$caption</option>";
			    }
			}
			
                        $output[] = "</select></span>";	

			// pokud neni v povolenych vymaz tento select aby jej nevidel
			if(isset($povoleni) && $povoleni != null && !in_array($logged_user['CmsGroup']['id'],$povoleni))
			    $output = $output_zaloha;
			break;
		    case 'date':
                        $output[] = "<span><label>$caption:</label><input type='text' id='filtr_$key' name='$prefix$key' class='fltr_input'  /><img src='/css/fastest/icons/calendar.png' id='filtr_$key"."Img'/> </span>";
                        $output[] = "<script>";
                        $output[] = "$('filtr_$key"."Img').addEvent('click',function(e){new Event(e).stop(); displayCalendar($('filtr_$key'),'yyyy-mm-dd',this,true);});";
                        $output[] = "</script>";
                        break;
                    case 'date_month':
                        $output[] = "<span><label>$caption:</label><input type='text' id='filtr_$key' name='$prefix$key' class='fltr_input'  /><img src='/css/fastest/icons/calendar.png' id='filtr_$key"."Img'/> </span>";
                        $output[] = "<script>";
                        $output[] = "window.addEvent('domready',function(){new MonthCal($('filtr_$key'));});";
                //	$output[] = "$('filtr_$key"."Img').addEvent('click',function(e){new Event(e).stop(); displayCalendar($('filtr_$key'),'yyyy-mm-dd',this,true);});";
                        $output[] = "</script>";
                        break;    
		    case 'date_f_t':
                        $output[] = "<span>";
                        $output[] = "<label>$caption - od:</label><input type='text' id='filtr_$key"."_from' name='$prefix$key-from' class='fltr_input cal'  /><img src='/css/fastest/icons/calendar.png' class='calbut' id='filtr_$key"."ImgFrom'/> ";
                        $output[] = "<label>$caption - do:</label><input type='text' id='filtr_$key"."_to' name='$prefix$key-to' class='fltr_input cal'  /><img src='/css/fastest/icons/calendar.png' class='calbut' id='filtr_$key"."ImgTo'/> ";
                        $output[] = "</span>";
                        $output[] = "<script>";
                        $output[] = "$('filtr_$key"."ImgFrom').addEvent('click',function(e){new Event(e).stop(); displayCalendar($('filtr_$key"."_from'),'yyyy-mm-dd',this,true);});";
                        $output[] = "$('filtr_$key"."ImgTo').addEvent('click',function(e){new Event(e).stop(); displayCalendar($('filtr_$key"."_to'),'yyyy-mm-dd',this,true);});";
                        $output[] = "</script>";
                        break;
		    case 'checkbox':
			$output[] = "<span><label>$caption:</label><input type='checkbox' id='filtr_$key' name='filtration_$key' class='fltr_checkbox find_form'  /> </span>";			
			break;
		}
	    }
            $output[] = '</div>';
            if(isset($renderSetting['filtration_button_br']) && $renderSetting['filtration_button_br'] == true)
                $output[] = '<br />';
            
            $output[] = $this->HtmlExt->submit('Filtrovat',array('id'=>'filtr_button','class'=>'filtr_button')).$this->HtmlExt->button('Zrušit',array('id'=>'filtr_button_cancel','class'=>'filtr_button_cancel'));
	    if (count($set) > 6)
		$output[] = $this->HtmlExt->button('',array('title'=>'Filtrovat','class'=>'filtr_more','rel'=>count($set))).'&nbsp; ';
		
            if(isset($renderSetting['filtration_next_button'])){
                foreach($renderSetting['filtration_next_button'] as $button)
                    $output[] = $this->HtmlExt->button($button['label'],$button['options']); 
            }
            $output[] = '</form>';
            return implode("\n",$output);
	}
	
	function top_action($renderSetting = array(), $logged_user = array()){
		$permission_list = $logged_user['CmsGroup']['permission'][$renderSetting['controller']];
		$output = array('<ul>');
		$basic_path = $renderSetting['controller'];
		foreach($renderSetting['top_action'] as $key_action=>$top_action){
			list($caption, $url, $description, $permission) = explode('|',$top_action);
		
			if (isset($permission_list[$permission]) && in_array($permission_list[$permission],array(2,3))){
				$output[] =  "<li><a href='/$basic_path/$url' class='$url' title='$description'>$caption</a></li>\n";
			}
            
            /**
             * pokud se jedna o Tisk
             */
            if($url == 'print')
			 	$output[] =  "<li><a href='#' onclick='print(); return false;' class='print' title='$description'>$caption</a></li>\n";
	
		}
		$output[] = '</ul><br class="clear" />';	
		return implode("\n",$output);
	}
	
	function legend_render($renderSetting = array()){
		$output = array('<ul>');
		foreach($renderSetting['posibility'] as $item){
			list($class,$caption, $permission) = explode('|',$item);
			switch($class){
				case 'status':
					$output[] = "<li><a href='#' class='status1 legend_icon'>&nbsp;</a>Položka je aktivni - deaktivovat<br/></li>";
					$output[] = "<li><a href='#' class='status0 legend_icon'>&nbsp;</a>Položka není aktivní - aktivovat<br/></li>";
					break;
				case 'stop':
					$output[] = "<li><a href='#' class='stop1 legend_icon'>&nbsp;</a>Výplata je pozastavena<br/></li>";
					$output[] = "<li><a href='#' class='stop0 legend_icon'>&nbsp;</a>Výplata není pozastavena<br/></li>";
					break;
				default:
					$output[] = "<li><a href='#' class='$class legend_icon'>&nbsp;</a>$caption<br/></li>";
					break;
			}
		}
		$output[] = '</ul>';
		return implode("\n",$output);
	}
	
    
    private function genereate_link($item,$link){
        $out = null;
        $link = ltrim($link,'/');
        $link = rtrim($link,'/');
        $links = explode('/',$link);
        
        foreach ($links as $item_link){
            if($item_link == "#CURRENT_YEAR#"){
                $out .= CURRENT_YEAR.'/';
            }
            else if($item_link == "#CURRENT_MONTH#"){
                $out .= CURRENT_MONTH.'/';
            }
            else{
                
                list($model,$col) = explode('.',$item_link);
                $out .= $item[$model][$col].'/';
            }
        }
         
        
        return $out;
    }
    
    /**
     * Funkce se snazi najit v items
     * zda ma takova osoba podle id, pristup k teto moznosti
     * a vyhledavame ve vice polich
     * @uses eb_companies
     */
    private function find_group_id_allow($items = array(),$group_ids = array(),$logged_id){
        foreach($group_ids as $group_id){
            if(isset($items[$group_id]) && $items[$group_id] == $logged_id)
                return true;
        }
        return false;
    }
	
}
?>