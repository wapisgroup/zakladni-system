<?php
class FastestHelper extends Helper {
	var $helpers = array('Html','HtmlExt');
	
	function CisloNaSlovo($cislo, $nula = false,$typ='normal') {
		if ($typ =='sklon'){
			$jednotky = array("", "jednoho","dvou","tří","čtyř","pěti","šesti","sedmi","osmi","devíti");
			$mezi = array(11=>"jedenácti",12=>"dvanácti",13=>"třinácti",14=>"čtrnácti",15=>"patnácti",16=>"šestnácti",17=>"sedmnácti",18=>"osmnácti",19=>"devatenácti");
			$desitky = array("", "deset","dvaceti","třiceti","čtyřiceti","padesáti","šedesáti","sedmdesáti","osmdesáti","devadesáti");
		} else {
			$jednotky = array("", "jedna","dva","tři","čtyři","pět","šest","sedm","osm","devět");
			$mezi = array(11=>"jedenáct",12=>"dvanáct",13=>"třináct",14=>"čtrnáct",15=>"patnáct",16=>"šestnáct",17=>"sedmnáct",18=>"osmnáct",19=>"devatenáct");
			$desitky = array("", "deset","dvacet","třicet","čtyřicet","padesát","šedesát","sedmdesát","osmdesát","devadesát");
		}

		$cislo = (string) ltrim(round($cislo), 0);
		$delka = strlen($cislo);

		if($cislo==0)  return $nula ? "nula":false;             //ošetření 0
		elseif($delka==1)        
			return $jednotky[$cislo];  //1 řád - jednotky
		elseif($delka==2) {                                 //2 řády - desítky
			$desitkyAJednotky = $cislo{0}.$cislo{1};
			if($desitkyAJednotky==10) echo "deset";
			elseif($desitkyAJednotky<20) {
				return $mezi[$desitkyAJednotky];
			} else {
				return $desitky[$cislo{0}].$jednotky[$cislo{1}];
			}
		}
		elseif($delka==3) {                                 //3 řády - stovky
			if($cislo{0}==1)     
				return "sto".$this->CisloNaSlovo(substr($cislo,1));
			elseif($cislo{0}==2) 
				return "dvěstě".$this->CisloNaSlovo(substr($cislo,1));
			elseif($cislo{0}==3 OR $cislo{0}==4) 
				return $jednotky[$cislo{0}]."sta".$this->CisloNaSlovo(substr($cislo,1));
			else                 
				return $jednotky[$cislo{0}]."set".$this->CisloNaSlovo(substr($cislo,1));
		}
		elseif($delka==4) {                                //4 řády - tisíce
			if($cislo{0}==1) 
				return "tisíc".$this->CisloNaSlovo(substr($cislo,1));
			elseif($cislo{0}<5) 
				return $jednotky[$cislo{0}]."tisíce".$this->CisloNaSlovo(substr($cislo,1));
			else             
				return $jednotky[$cislo{0}]."tisíc".$this->CisloNaSlovo(substr($cislo,1));
		}
		elseif($delka==5) {                                //5 řádů - desítky tisíc
			$desitkyTisic = $cislo{0}.$cislo{1};
			if($desitkyTisic==10)      
				return "desettisíc".$this->CisloNaSlovo(substr($cislo,2));
			elseif($desitkyTisic<20)   
				return $mezi[$desitkyTisic]."tisíc".$this->CisloNaSlovo(substr($cislo,2));
			elseif($desitkyTisic<100)  
				return $desitky[$cislo{0}].$jednotky[$cislo{1}]."tisíc".$this->CisloNaSlovo(substr($cislo,2));
		}
		elseif($delka==6) {                                //6 řádů - stovky tisíc
			if($cislo{0}==1)  {
				if($cislo{1}.$cislo{2}==00)         
					return "stotisíc".$this->CisloNaSlovo(substr($cislo,3));
				else
	                                return "sto".$this->CisloNaSlovo(substr($cislo,1));
			} elseif($cislo{0}==2)                  
				return "dvěstě".$this->CisloNaSlovo(substr($cislo,1));
			elseif($cislo{0}==3 OR $cislo{0}==4)  
				return $jednotky[$cislo{0}]."sta".$this->CisloNaSlovo(substr($cislo,1));
			else
				return $jednotky[$cislo{0}]."set".$this->CisloNaSlovo(substr($cislo,1));
		}
		return false;
	}

	function price($price,$mena_suf=',- Kč',$mena_pref=null,$round=0){
    	$white = array(" " => "");
		return $mena_pref.number_format(strtr($price,$white), $round, '.', ' ').$mena_suf;
	
	}
	
	function price_array($price,$mena_suf){
    	$white = array(" " => "");
		if ($mena_suf['kurz']!=null)
			$price = ($price/strtr($mena_suf['kurz'],',','.'))*$mena_suf['pocet'];
		return $mena_suf['pref'].number_format(strtr($price,$white), $mena_suf['mista'], '.', ' ').$mena_suf['suf'];
	
	}
	
	function array2list($data,$model,$primary = 'id', $secundary = 'name',$lang = false){
		$output = array();
		foreach($data as $item){
			if ($lang != false)
				$output[$item[$model][$primary]] = $item[$model][$secundary][$lang];
			else
				$output[$item[$model][$primary]] = $item[$model][$secundary];
		}
		return $output;
	}
	
	function czechDate($date, $format = ''){
		if ($format == '') $format = 'd.m.Y';
    	if ($date!='0000-00-00' && $date!='0000-00-00 00:00' && $date!=null)
			return date($format,strtotime($date)); 
		else
			return;
	}
	
	function CzechDateTime($date){
    	if ($date!='0000-00-00 00:00:00' && $date!=null){
		
		$date = strtotime($date);
    	$mesice = array ("ledna", "února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince");	
    	return date('d',$date).'.'.date ("m",$date).'.'.date('y',$date).' '.date('H:i:s',$date);
		}
	}
	
	/* FAST LINK */
	function fastLink($array = null){
		$html = new HtmlHelper(); 
		$output = '';
		$count = count($array);
		$i = 0;
		if (isset($array)):
		foreach ($array as $caption => $link){
			$i++;
			if ($i == 1) {
				$output .= $html->link($caption,$link,array('title'=>$caption)) .' > '; 
			} elseif ($i < $count){
				$output .= $html->link($caption,$link.'/',array('title'=>$caption)) .' > '; 
			} else {
				$output .= 	'<b>'.$caption.'</b>';
			}
		}
		endif;
		return $output;
	} 

	function page_title($array = null){	
		arsort($array);
		$output = '';
		$count = count($array);
		$i = 0;
		if (isset($array)):
		foreach ($array as $caption => $link){
			$i++;
			if ($i == 1) {
				$output .= $caption. ' |  '; 
			} elseif ($i < $count){
				$output .= ' '.$caption.' | '; 
			} else {
				$output .= 	$caption;
			}
		}
		endif;
		return $output;
		}
		
	/* nahrazeni stavu za ikonu */
	function status_to_ico($value,$url){
		if($value == 1){
            if(isset($this->Html)){
			   return $this->Html->image('../css/icons/status/'.$url.'.png',array('alt'=>$value,'title'=>$value));
            }
            else
               return $this->YN($value); 
        }
        else 
			return '';
	}
    
    /* nahrazeni stavu za ikonu */
	function status_to_ico2($value,$url,$reverse = false){
        $title = $value;
       
	    if(strstr($url,'-')){
	        if(substr_count($url,'-') == 1)
                list($url,$title) = explode('-',$url);
            else        
                list($url,$title,$reverse) = explode('-',$url);    
        }
       
		if(($value == 0 && !$reverse) || ($reverse && $value == 1 ))
			return $this->Html->image('../css/icons/status/'.$url.'.png',array('alt'=>$title,'title'=>$title));
        else 
			return '';
	}
    
    /* nahrazeni hodnoty za text ano ne */
	function value_to_yes_no($value,$type = 'long',$reverse = false){
        $tmp = array(
            'long'=>array('Ano','Ne'),
            'short'=>array('A','N')
        );
		if(($value != null && $value != 0 && !$reverse) || ($reverse && $value == 1 ))
			return $tmp[$type][0];
		else 
			return $tmp[$type][1];
	}
    
    /**
     * nove funkce ktera vytvari plnohodnotnou formu odmeny
     * @return String Formy odmeny
     */
     function made_forma_odmeny ($name,$doprava,$ubytovani,$stravenka = null){
        $ub = self::value_to_yes_no($ubytovani);
        $do = self::value_to_yes_no($doprava);
        $str = self::value_to_yes_no($stravenka);
        
        if($stravenka == null)
            return String::insert(':name - Ubytování :ub / Doprava :do', array('name' => $name, 'ub' => $ub,  'do' => $do));
        else   
            return String::insert(':name - Ubytování :ub / Doprava :do / Stravenky :str', array('name' => $name, 'ub' => $ub,  'do' => $do,  'str' => $str));
     }
	
	/* orezani textu */
	function orez($text,$limit,$img=false)
	{
		if (strlen($text) <= $limit) {
			$output = strip_tags ($text);
		} else {
			$text =  strip_tags($text);
		    $text = substr($text, 0, $limit+1);
			$pos = strrpos($text, " "); // v PHP 5 by se dal použít parametr offset
			if ($img=true)
				$output = substr($text, 0, ($pos ? $pos : -1)) . "...";
			else
			$output = strip_tags (substr($text, 0, ($pos ? $pos : -1)) . "...");
		}
		return $output;
	}
	
	/* orezani textu */
	function orez2($text,$limit,$img=false)
	{
		if (strlen($text) <= $limit) {
			$output = strip_tags ($text);
		} else {
			//$text =  strip_tags($text);
		    $text = substr($text, 0, $limit+1);
			$pos = strrpos($text, " "); // v PHP 5 by se dal použít parametr offset
			if ($img=true)
				$output = substr($text, 0, ($pos ? $pos : -1)) . "...";
			else
			$output = substr($text, 0, ($pos ? $pos : -1) . "...");
		}
		return $output;
	}
    
    /* orezani textu */
	function orez3($text,$limit,$img=false)
	{
		if (strlen($text) <= $limit) {
			$output = strip_tags ($text);
		} else {
			//$text =  strip_tags($text);
            $all_text = $text;
		    $text = substr($text, 0, $limit+1);
			$pos = strrpos($text, " "); // v PHP 5 by se dal použít parametr offset
			if ($img=true)
				$output = substr($text, 0, ($pos ? $pos : -1)) . "...";
			else
			    $output = substr($text, 0, ($pos ? $pos : -1) . "...");
                
            $output = "<div title='".nl2br($all_text)."'>".$output."<div>";    
		}
		return $output;
	}
	
	/* velikost souboru */
	function velikost($soubor) {
		$size = @filesize("./".$soubor);
		if($size < 1024) {$size = ($size); $k = " B";}
		if($size >= 1024) {$size = ($size / 1024); $k = " kB";}
		if($size >= 1024) {$size = ($size / 1024); $k = " MB";}
		return round($size, 1).$k; /* 1 = zaokrouhlování na jedno desetinné místo */
	} 
	
	
	
	
	/* ikona souboru */
	function file_icon($attach)
	{
		$pripona = strtolower(end(Explode(".", $attach)));
					
				
					$pripona_img = array('jpg', 'jpeg', 'png', 'gif');
					$pripona_excel = array('xls', 'ods');
					$pripona_word = array('doc', 'odt');
					$pripona_pdf = array('pdf');
					$pripona_html = array('html', 'htm', 'php','css','ctp');
					$pripona_ppt = array('ppt', 'ptc');
					
					 
					 
					if (in_array($pripona, $pripona_img)){$icon=$this->Html->image('../css/icons/file/images.gif',array('class'=>'icons','alt'=>$pripona,'title'=>$pripona));}
					elseif (in_array($pripona, $pripona_excel)){$icon=$this->Html->image('../css/icons/file/excel.gif',array('class'=>'icons','alt'=>$pripona,'title'=>$pripona));}
					elseif (in_array($pripona, $pripona_word)){$icon=$this->Html->image('../css/icons/file/word.gif',array('class'=>'icons','alt'=>$pripona,'title'=>$pripona));}
					elseif (in_array($pripona, $pripona_pdf)){$icon=$this->Html->image('../css/icons/file/pdf.gif',array('class'=>'icons','alt'=>$pripona,'title'=>$pripona));}
					elseif (in_array($pripona, $pripona_html)){$icon=$this->Html->image('../css/icons/file/html.gif',array('class'=>'icons','alt'=>$pripona,'title'=>$pripona));}
					elseif (in_array($pripona, $pripona_ppt)){$icon=$this->Html->image('../css/icons/file/ppt.gif',array('class'=>'icons','alt'=>$pripona,'title'=>$pripona));}
					else {$icon=$this->Html->image('../css/icons/file/unknown.gif',array('class'=>'icons','alt'=>$pripona,'title'=>$pripona));}
	return $icon;
	return $pripona;
	}

function IsMobile()
{
  // funkcí ERegI zjistím výskyt některé části řídícího řetězce
  // v řetězci identifikace prohlížeče
  $ret = ERegI(QuoteMeta("UP.LINK|MIDP|UP.BROWSER|NOKIA|MOT|SEC-".
  "|WAP|ERICSSON|SAMSUNG|SIE-|PHONE|PANASONIC".
  "|MITSU|LG|PORTALMMM|BLACKBERRY|SYMBIAN|PHILIPS".
  "|SENDO|KLONDIKE|SAGEM|MOBILE|ALCATEL|SONY"),
  $_SERVER["HTTP_USER_AGENT"]);
  // vrácení návratové hodnoty
  return $ret;
} 


function get_size($size)
     {
         if ($size < 1024)
          {
              return round($size,2).' Byte';
          }
         elseif ($size < (1024*1024))
          {
              return round(($size/1024),2).' Kb';
          }
         elseif ($size < (1024*1024*1024))
          {
              return round((($size/1024)/1024),2).' Mb';
          }
         elseif ($size < (1024*1024*1024*1024))
          {
              return round(((($size/1024)/1024)/1024),2).' Gb';
          }
          elseif ($size < (1024*1024*1024*1024*1024))
          {
              return round((((($size/1024)/1024)/1024)/1024),2).' Tb';
          }
    }
	
	function YN($value){
		$l = array('Ne','Ano');
		return $l[$value];
	}
	
    
    /**
     * funkce pro prumer z pole
     * @since 6.11.09
     * @author Sol 
     */
    function array_avg($array,$round_count = 1){  
		return round(array_sum($array)/count($array),$round_count);
	}
    
    
    /**
     * funcke ktera zpracovava nastaveni z renderSetting a nastavuje
     * prislusnou tridu pro dany radek ktery splnuje podminku
     * @param $not_equal - nastaveni porovnani = nebo !=
     * 
     * Nova uprava, do settingu muzeme zadat vice podminek(multiarray)
     */
    function set_class_for_tr($setting = array(),$item,$not_equal = false){
        if($setting == null)
            return true;
        else{
            if(!isset($setting[0]))
                $setting = array(0=>$setting); 
                
            foreach($setting as $set){
                if($not_equal === false && $item[$set['model']][$set['col']] == $set['value'])
                    return $set['class'];
                else if($not_equal === true && $item[$set['model']][$set['col']] != $set['value'])
                    return $set['class'];     
                else continue;      
            }
            return false; 
        }
    }
    
     /**
     * funcke ktera zpracovava nastaveni z renderSetting a nastavuje
     * prislusnou tridu pro dany radek ktery splnuje podminku
     * @param $not_equal - nastaveni porovnani = nebo !=
     */
    function set_id_for_tr($setting = array(),$item){
        if($setting == null)
            return false;
        else{
            if(isset($item[$setting['model']][$setting['col']]))
                return $item[$setting['model']][$setting['col']];
            else return false;        
        }
    }
    
    
    /**
     * funkce pouze odstranuje diakritiku pomoci strtr
     * postupne se musi pridat veskere znaky
     * @author Sol
     * @created 29.12.09
     */
     public function removing_diacritical($text){
             static $convertTable = array (
    
            'á' => 'a', 'Á' => 'A', 'ä' => 'a', 'Ä' => 'A', 'č' => 'c',
    
            'Č' => 'C', 'ď' => 'd', 'Ď' => 'D', 'é' => 'e', 'É' => 'E',
    
            'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'í' => 'i',
    
            'Í' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ľ' => 'l', 'Ľ' => 'L',
    
            'ĺ' => 'l', 'Ĺ' => 'L', 'ň' => 'n', 'Ň' => 'N', 'ń' => 'n',
    
            'Ń' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ö' => 'o', 'Ö' => 'O',
    
            'ř' => 'r', 'Ř' => 'R', 'ŕ' => 'r', 'Ŕ' => 'R', 'š' => 's',
    
            'Š' => 'S', 'ś' => 's', 'Ś' => 'S', 'ť' => 't', 'Ť' => 'T',
    
            'ú' => 'u', 'Ú' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ü' => 'u',
    
            'Ü' => 'U', 'ý' => 'y', 'Ý' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y',
    
            'ž' => 'z', 'Ž' => 'Z', 'ź' => 'z', 'Ź' => 'Z',
    
            );
            
            return strtr($text, $convertTable);
     }
     
     
     /**
      * funkce napsana pro export do excelu CLIENTVIEW
      * je nutne rozparsovat string podle <br/> a vybrat z nej pouze urcitou cast po explode 
      */
	function parse_string($string,$col){
        $tmp = explode('<br/>',$string);
          
        if(isset($tmp[$col]))
            return $tmp[$col];
        else
            return false;    
	}
     
     
    /**
     * funkce ktera vypisuje na dashboardu pole
     */ 
    function dashboard_data_list($data){
        
         foreach($data as $key=>$item){
            echo '<tr>';
            echo '<td>'.$item['label'].'</td>';
            echo '<td>';
            if(!is_array($item['value'])){
            
                if(isset($item['type']) && $item['type'] == 'money')
                    echo $fastest->price($item['value'],',-');
                else
                    echo $item['value'];   
            }
            else{
                foreach($item['value'] as $id=>$val){
                    if(isset($item['type']) && $item['type'] == 'money')
                        echo $fastest->price($val,',-');
                    else
                        echo $val;  
                        
                    if(isset($item['value'][$id+1]))
                        echo '</td><td>';     
                }
                
            }        
                     
            echo '</td>';
            echo '</tr>';
        }
    } 
    
    
    function show_money_item_setting($typ,$data,$list){
        foreach($data as $item){
            if($item['ConnectionMoneyItemSetting']['typ'] == $typ && isset($list[$item['ConnectionMoneyItemSetting']['money_item_type_id']]))
                echo '<li>'.$list[$item['ConnectionMoneyItemSetting']['money_item_type_id']].$this->Html->link('Smazat','/companies/delete_money_item_setting/'.$item['ConnectionMoneyItemSetting']['id'],array('class'=>'ta status0 delete_setting')).'</li>';
        }
    }
    
    /**
     * funkce zobrazuje setting(busy,srazky,priplatky v nove dochazce, a spravne renderuje value a maximalku)
     * <th>i</th>
     * <th>název</th>
     * <th>a/n</th>
     * <th>typ</th>
     * <th>hodnota</th>
     */
    function make_money_item_setting_in_employee($typ,$data,$list_of_typ,$connection_cwh_to_mis){
        $row_i = 1;
        
        foreach($data as $ms_item_id=>$ms_item_){           
            $ms_item = $ms_item_['ConnectionMoneyItemSetting'];
            $connection = $ms_item_['MoneyItemConnectionTool'];
            
            /**
             * Pokud existuje takovyto typ ulozn v DB a je aktivni zobraz, nastavenou hodnotu a mesicni
             * zakrtni radek
             */;
            if(isset($connection_cwh_to_mis[$typ][$ms_item_id])){
                $ms_item['yn'] = 1;
                $ms_item['hodnota'] = $connection_cwh_to_mis[$typ][$ms_item_id]['val'];
                $ms_item['monthly'] = $connection_cwh_to_mis[$typ][$ms_item_id]['monthly'];
            }
            
            echo '<tr>';
                echo '<td>'.$row_i.'.</td>';
                echo '<td>'.$ms_item['name'].'</td>';
                echo '<td>'.$this->HtmlExt->checkbox('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/yn',null,array('checked'=>(isset($ms_item['yn']) && $ms_item['yn']==1? true :false),'class'=>'yn_setting')).'</td>';
                echo '<td>'.strtolower($list_of_typ[$ms_item['typ']]).'</td>';
                echo '<td>'.$this->HtmlExt->input('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/value',array('value'=>($ms_item['typ'] == 4 ? 0:$ms_item['hodnota']),'class'=>'float one_item '.(isset($ms_item['yn']) && $ms_item['yn']==1? '' :'read_info'))).'</td>';
                echo $this->HtmlExt->hidden('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/typ',array('value'=>$ms_item['typ']));
                echo $this->HtmlExt->hidden('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/max',array('value'=>$ms_item['hodnota']));
                echo $this->HtmlExt->hidden('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/name',array('value'=>$ms_item['name']));
                echo $this->HtmlExt->hidden('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/phm',array('value'=>(isset($ms_item['phm'])?$ms_item['phm']:0)));
                echo $this->HtmlExt->hidden('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/con_col',array('value'=>$connection['col']));
                echo $this->HtmlExt->hidden('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/con_typ',array('value'=>$connection['typ']));
            echo '</tr>';
            
            echo '<tr>';
                echo '<td></td>';
                echo '<td>Celkem za měsíc</td>';
                echo '<td></td>';
                echo '<td>měsíčně</td>';
                echo '<td>'.$this->HtmlExt->input('ConnectionClientWorkingHourToMoneySetting/'.$ms_item['type_id'].'/'.$ms_item_id.'/monthly',array('readonly','readonly','class'=>'read_info')).'</td>';
            echo '</tr>';
            
            $row_i++;
        }
       
    }
    
    /* - old verison
        $class_value = 'short float salary money_item_setting_'.$data['type_id'];
        $class_max = 'short float for_null_past_money_item_change';                       
        $setting_val = array('rel'=>$data['typ'],'label'=>$data['name'].($data['typ'] == 0?"":" (hod)"),'class'=>$class_value,'value'=>($data['hodnota'] != ''?$data['hodnota']:0));
        $setting_max = array('class'=>$class_max,'disabled'=>'disabled','value'=>($data['hodnota_max'] != ''?$data['hodnota_max']:$data['hodnota']));             

        if(in_array($data['type_id'],array(0,2))){
            echo $this->HtmlExt->input('ClientWorkingHour/money_setting_'.$data['type_id'].'/'.$id,$setting_val);
            echo "<label class='label_short'>/</label>";
            echo $this->HtmlExt->input('ClientWorkingHour/money_setting_'.$data['type_id'].'/'.$id.'_max',$setting_max)."<br />";
        }
        else{
            $setting_val = array('rel'=>$data['typ'],'class'=>$class_value,'disabled'=>'disabled','value'=>($data['hodnota'] != ''?$data['hodnota']:0));
            echo '<label>&nbsp;</label>'.$this->HtmlExt->input('ClientWorkingHour/money_setting_'.$data['type_id'].'/'.$id,$setting_val);
            echo "<label class='label_short'>".($data['typ'] == 0?"%":"")."</label><br />";
        }        
    */    
     
}	
?>