<?php
class HtmlExtHelper extends Helper {
	/**
  	* Holds tag templates.
  	*
  	* @access public
  	* @var array
  	*/
	var $tags = array(
				'input' 		=> 	'<input name="data[%s]" %s />',
				'selectstart' 	=> 	'<select name="data[%s]" %s>',
				'selectoption' 	=> 	'<option value="%s" %s>%s</option>',
				'selectempty' 	=> 	'<option value="" %s>&nbsp;</option>',
                'selectend' 	=> 	'</select>',
                'password' 		=> 	'<input type="password" name="data[%s]" %s/>',
                'checkbox' 		=> 	'<input type="checkbox" name="data[%s]" %s/>',
                'radio' 		=> 	'<input type="radio" name="data[%s]" id="%s" %s />%s',
				'hidden' 		=> 	'<input type="hidden" name="data[%s]" %s/>',
				'button' 		=> 	'<input type="button" %s/>',
				'textarea' 		=> 	'<textarea name="data[%s]" %s cols="5" rows="5">%s</textarea>',
				'form' 			=> 	'<form %s>',
				'submit' 		=> 	'<input type="submit" %s/>',
				'selectmultiplestart' => '<select name="data[%s][]" %s>',
				'var' 			=> 	'<var %s>%s</var>',
	);
	function inputDate($fieldName, $htmlAttributes = array(), $return = false) {
		//loadHelper('html'); $html =& new HtmlHelper();
		$this->setFormTag($fieldName);
		$id = '';
		foreach ($this->fields as $field) $id .= Inflector::camelize($field);
		$out = array();			
    if(isset($htmlAttributes["value"]))
        $cur_value=$htmlAttributes["value"];
    else
        $cur_value=$this->tagValue($fieldName);
		if (($cur_value == '0000-00-00 00:00:00') || ($cur_value== '0000-00-00')) 
			$value = '';
		else 
			$value =  $cur_value; 
			
		$htmlAttributes['value']= $value;
		$htmlAttributes['id']= 'calbut_'.$id;
		$htmlAttributes['readonly']= 'readonly';
		//id="calbut_'.$id.'"
		$out[] = $this->input($fieldName, $htmlAttributes, $return); 
		//$out[] = '<img src="/css_default/icons/calendar.gif" title="'.lang_kalendar.'" alt="'.lang_kalendar.'" class="icon calendar" />';
		$out[] = '<script language="JavaScript" type="text/javascript">
			$("calbut_'.$id.'").addEvent("click",function(e){
				new Event(e).stop();
				displayCalendar("calbut_'.$id.'","yyyy-mm-dd",this,true);
			});
		</script>';
	
		return  implode("\n", $out);
	}
	
	function inputDateTime($fieldName, $htmlAttributes = array(), $return = false) {
		//loadHelper('html'); $html =& new HtmlHelper();
		$this->setFormTag($fieldName);
		$id = '';
		foreach ($this->fields as $field) $id .= Inflector::camelize($field);
		$out = array();			
	
		if (($this->tagValue($fieldName) == '0000-00-00 00:00:00') || ($this->tagValue($fieldName) == '0000-00-00')) 
			$value = '';
		else 
			$value =  $this->tagValue($fieldName); 
			
		$htmlAttributes['value']= $value;
		$htmlAttributes['id']= 'calbut_'.$id;
		
		$out[] = $this->input($fieldName, $htmlAttributes, $return); 
		//$out[] = '<img src="/css_default/icons/calendar.gif" title="'.lang_kalendar.'" alt="'.lang_kalendar.'" class="icon calendar" id="calbut_'.$id.'"/>';
		$out[] = '<script language="JavaScript" type="text/javascript">
			$("calbut_'.$id.'").addEvent("click",function(e){
				new Event(e).stop();
				displayCalendar($("calbut_'.$id.'"),"yyyy-mm-dd hh:ii",this,true);
			});
		</script>';
	
		return  implode("\n", $out);
	}
	
	function input($fieldName, $htmlAttributes = array(), $return = false) {
         	$this->setFormTag($fieldName);
         	$out = '';		
            $default = null;
            
            if (isset($htmlAttributes['default'])) {
                $default  = $htmlAttributes['default'];
            }    
			
		if (!isset($htmlAttributes['value'])) {
            	//$htmlAttributes['value'] = $this->tagValue($fieldName);
            	$htmlAttributes['value'] = $this->existInArray($fieldName,$default);
         	}

         	if (isset($htmlAttributes['help'])){
         		$id_help_t = $this->getId().'_tips';
         		$id_help = $this->getId().'_a';
         		$out .= "<a class='help' href='#' id='{$id_help}' onmouseover='tooltip_{$id_help_t}.show();' onmouseout = 'tooltip_{$id_help_t}.hide();'></a>";
         		$out .= "<script type='text/javascript'> tooltip_{$id_help_t} = new tooltips('{$id_help}','{$id_help_t}','{$htmlAttributes['help']}')</script>";
         		unset($htmlAttributes['help']);
         	}
         	
         	if (isset($htmlAttributes['label'])){
         		if (isset($htmlAttributes['label_class'])){
         			$label_class = ' class="'.$htmlAttributes['label_class'].'" ';
         			unset($htmlAttributes['label_class']);
         		} else 
         			$label_class = '';
         			
         		$out .= "<label for='".$this->getId()."' {$label_class}>{$htmlAttributes['label']}:</label>";
         		unset($htmlAttributes['label']);
         	}
         	
         	if (!isset($htmlAttributes['type'])) {
             	$htmlAttributes['type'] = 'text';
         	}
 
         	if (!isset($htmlAttributes['id']) && $fieldName != null) {
         		$id = '';
         		foreach ($this->fields as $field)
         			$id .= Inflector::camelize($field);
             	$htmlAttributes['id'] = $id;
         	}
 
         	if ($this->tagIsInvalid($this->fields)) {
             	if (isset($htmlAttributes['class']) && trim($htmlAttributes['class']) != "") {
                 	$htmlAttributes['class'] .= ' form_error';
             	} else {
                 	$htmlAttributes['class'] = 'form_error';
             	}
         	}
		
		//if ($fieldName != null) {
         	//	$htmlAttributes['name'] = null;
         	//}
		 $hint = false;
         if (isset($htmlAttributes['hint'])){ 
            $hint = $htmlAttributes['hint'];
            unset($htmlAttributes['hint']);
         }   
         	
         $out .= $this->output(
         			sprintf(
         				$this->tags['input'], 
         				implode('][',$this->fields) ,
         				$this->_parseAttributes(
         					$htmlAttributes, 
         					null, 
         					' ', 
         					' '
         				)
         			), 
         			$return
         		);
                
         /**
          * @author Jakub Matuš
          * @created 8.11.2011 
          */       
         if ($hint !== false){  
            $out .= '<p class="row-hint" id="'.$this->getId().'-hint"><span class="info">'.$hint.'</span></p>';
         }   
		return $out;
     }
     
     
     
     /**
  	* Returns value of $fieldName. False if the tag does not exist.
  	*
  	* @param string $fieldName     Fieldname as "First/Seceond/Third/..." string
  	* @return string htmlspecialchars Value of the named tag.
  	* @access public
  	*/
	function tagValue($fieldName, $escape = false) {
		$obj = $this->existInArray($fieldName);
     	if ($obj !== false){
				return ife($escape, h($obj), $obj);
     	}
         	return false;
     }
     
     function existInArray($fieldName,$default = null){
     	$this->setFormTag($fieldName);
     	$obj = $this->data;
		foreach ($this->fields as $field){
			if (isset($obj[$field])){
				$obj = $obj[$field];
                
                if($default != null && $obj == 0){$obj = $default;}
            }    
            else if($default != null)
                $obj = $default;    
			else 
				return false;
		}
		
		return $obj;
     }
     
     /**
  	* Sets this helper's model and field properties to the slash-separated value-pair in $tagValue.
  	*
  	* @param string $tagValue A field name, like "First/Seceond/Third/..."
  	* @return
  	* @access public
  	*/
     function setFormTag($tagValue) {
     	return $this->fields = explode("/", $tagValue);
     }
     
     /**
  	* Returns false if given FORM field has no errors. Otherwise it returns the constant set in the array Model->validationErrors.
  	*
  	* @param string $model Model name as string
  	* @param string $field     Fieldname as string
  	* @return boolean True on errors.
  	* @access public
  	*/
     function tagIsInvalid($fields) {
     	$obj = $this->validationErrors;
		foreach ($fields as $field)
			if ($obj[$field])
				$obj = $obj[$field];
         return empty($obj) ? 0 : $obj;
     }

     /**
  	* Returns a formatted SELECT element.
  	*
  	* @param string $fieldName Name attribute of the SELECT, like "First/Seceond/Third/..."
  	* @param array $optionElements Array of the OPTION elements (as 'value'=>'Text' pairs) to be used in the SELECT element
  	* @param mixed $selected Selected option
  	* @param array $selectAttr Array of HTML options for the opening SELECT element
  	* @param array $optionAttr Array of HTML options for the enclosed OPTION elements
  	* @param boolean $show_empty If true, the empty select option is shown
  	* @param  boolean $return         Whether this method should return a value
  	* @return string Formatted SELECT element
  	* @access public
  	*/
     function selectTag($fieldName, $optionElements, $selected = null, $selectAttr = array(), $optionAttr = null, $showEmpty = true, $return = false) {
     	$this->setFormTag($fieldName);
						 
         	if ($this->tagIsInvalid($this->fields)) {
            	if (isset($selectAttr['class']) && trim($selectAttr['class']) != "") {
                 	$selectAttr['class'] .= ' form_error';
             	} else {
                 	$selectAttr['class'] = 'form_error';
             	}
         	}
         	if (!isset($selectAttr['id'])) {
         		$id = '';
         		foreach ($this->fields as $field)
         			$id .= Inflector::camelize($field);
             	$selectAttr['id'] = $id;
         	}
            
            if (isset($selectAttr['disabled']) && $selectAttr['disabled'] != 'disabled') 
                unset($selectAttr['disabled']);
         	
 		
         	if (isset($selectAttr['help'])){
         		$id_help_t 	= 	$this->getId().'_tips';
         		$id_help 		= 	$this->getId().'_a';
         		$select [] 		=	"<a class='help' href='#' id='{$id_help}' onmouseover='tooltip_{$id_help_t}.show();' onmouseout = 'tooltip_{$id_help_t}.hide();'></a>";
         		$select [] 		=	"<script type='text/javascript'> tooltip_{$id_help_t} = new tooltips('{$id_help}','{$id_help_t}','{$selectAttr['help']}')</script>";
         		unset($selectAttr['help']);
         	}
         	
         	if (isset($selectAttr['label'])){
         		if (isset($selectAttr['label_class'])){
         			$label_class = ' class="'.$selectAttr['label_class'].'" ';
         			unset($selectAttr['label_class']);
         		} else 
         			$label_class = '';
         			
         		$select [] 		=	"<label for='".$this->getId()."'  {$label_class}>{$selectAttr['label']}:</label>";
         		unset($selectAttr['label']);
         	}
         	
         	if (!is_array($optionElements)) {
          	return null;
         	}
 
         	if (!isset($selected)) {
          	$selected = $this->tagValue($fieldName);
         	}
 
			//if (!isset($selectAttr['div']) or $selectAttr['div'] !== false){
			//	$w_num = '';
			//	if (isset($selectAttr['class'])){
			//		$classes = explode(' ',strtr($selectAttr['class'], array('  '=>' ','   '=>' ')));
			//		foreach($classes as &$class){
			//			if (substr($class,0,1) == 'w' && is_numeric(substr($class,1,1)))
			//				$w_num = 'w'.(substr($class,1) - 4);
			//		}
			//	}
			//	$select[] = '<div class="obalselect '.$w_num.'">';
			//}
	
			if (isset($selectAttr['ajax_link'])){
				$select[] = '<input type="hidden" value="'.$selectAttr['ajax_link'].'" id="'.$selectAttr['id'].'AjaxLink"/>';
				unset($selectAttr['ajax_link']);
			}
			if (isset($selectAttr['ajax_link_get'])){
				$select[] = '<input type="hidden" value="'.$selectAttr['ajax_link_get'].'" id="'.$selectAttr['id'].'AjaxLinkGet"/>';
				unset($selectAttr['ajax_link_get']);
			}
	
	
         	if (isset($selectAttr) && array_key_exists("multiple", $selectAttr)) {
          	$select[] = sprintf($this->tags['selectmultiplestart'], implode('][',$this->fields) , $this->parseHtmlOptions($selectAttr));
         	} else {
          	$select[] = sprintf($this->tags['selectstart'], implode('][',$this->fields) , $this->parseHtmlOptions($selectAttr));
         	}
 
         	if ($showEmpty == true) {
         	  $optionAttrforEmpty = $optionAttr;
         	  $optionAttrforEmpty['class'] = 'option_empty';
          	  $select[] = sprintf($this->tags['selectempty'], $this->parseHtmlOptions($optionAttrforEmpty));
         	}
 
         	foreach ($optionElements as $name => $title) {
          	$optionsHere = $optionAttr;
 
             	if (($selected != null) && ($selected == $name)) {
               	$optionsHere['selected'] = 'selected';
             	} elseif (is_array($selected) && in_array($name, $selected)) {
               	$optionsHere['selected'] = 'selected';
             	}
				if (isset($optionAttr['title']) && is_array($optionAttr['title']))
					$optionsHere['title'] = $optionAttr['title'][$name];
				else
					$optionsHere['title'] = h($title);
									
             	$select[] = sprintf($this->tags['selectoption'], $name, $this->parseHtmlOptions($optionsHere), h($title));
         	}
 
         	$select[] = sprintf($this->tags['selectend']);
			
			
			
         	return $this->output(implode("\n", $select), $return);
     }
     
      /**
  	* Creates a checkbox widget.
  	*
  	* @param  string  $fieldName Name of a field, like this "First/Seceond/Third/..."
  	* @deprecated  string  $title
  	* @param  array    $htmlAttributes Array of HTML attributes.
  	* @param  boolean $return  Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
  	* @return mixed    Either string or echos the value, depends on AUTO_OUTPUT and $return.
  	* @access public
  	*/
     function checkbox($fieldName, $title = null, $htmlAttributes = array(), $return = false) {
    		$value = $this->tagValue($fieldName);
         	$notCheckedValue = 0;
			$output  = '';
			
         	if (!isset($htmlAttributes['id'])) {
         		$id = '';
         		foreach ($this->fields as $field)
         			$id .= Inflector::camelize($field);
             	$htmlAttributes['id'] = $id;
         	}
 
			if (isset($htmlAttributes['help'])){
         		$id_help_t = $this->getId().'_tips';
         		$id_help = $this->getId().'_a';
         		$output .= "<a class='help' href='#' id='{$id_help}' onmouseover='tooltip_{$id_help_t}.show();' onmouseout = 'tooltip_{$id_help_t}.hide();'></a>";
         		$output .= "<script type='text/javascript'> tooltip_{$id_help_t} = new tooltips('{$id_help}','{$id_help_t}','{$htmlAttributes['help']}')</script>";
         		unset($htmlAttributes['help']);
         	}
         	
         	if (isset($htmlAttributes['label'])){
         		if (isset($htmlAttributes['label_class'])){
         			$label_class = ' class="'.$htmlAttributes['label_class'].'" ';
         			unset($htmlAttributes['label_class']);
         		} else 
         			$label_class = '';
         			
         		$output .= "<label for='".$this->getId()."'  {$label_class}>{$htmlAttributes['label']}:</label>";
         		unset($htmlAttributes['label']);
         	}
 
         	if (isset($htmlAttributes['checked'])) {
             	if ($htmlAttributes['checked'] == 'checked' || intval($htmlAttributes['checked']) === 1 || $htmlAttributes['checked'] === true) {
                 	$htmlAttributes['checked'] = 'checked';
             	} else {
                 	$htmlAttributes['checked'] = null;
                 	$notCheckedValue = -1;
             	}
         	} else {
             	if (isset($htmlAttributes['value'])) {
                 	$htmlAttributes['checked'] = ($htmlAttributes['value'] == $value) ? 'checked' : null;
 
                 	if ($htmlAttributes['value'] == '0') {
                     	$notCheckedValue = -1;
                 	}
             	} else {
                 //	$model = new $this->model;
                 //	$db =& ConnectionManager::getDataSource($model->useDbConfig);
                 //	$value = $db->boolean($value);
                 	$htmlAttributes['checked'] = $value ? 'checked' : null;
                 	$htmlAttributes['value'] = 1;
             	}
         	}
			
			if (isset($htmlAttributes['ajax_link'])){
				$output .= '<input type="hidden" value="'.$htmlAttributes['ajax_link'].'" id="'.$htmlAttributes['id'].'AjaxLink"/>';
				unset($htmlAttributes['ajax_link']);
			}
			$neco =  array('value' => $notCheckedValue, 'id' => $htmlAttributes['id'] . '_');
			if(isset($htmlAttributes['disabled']) && $htmlAttributes['disabled'] != false) 
				$neco["disabled"]='disabled';
            $output .= $this->hidden($fieldName,$neco, true);
         	$output .= sprintf($this->tags['checkbox'], implode('][',$this->fields), $this->_parseAttributes($htmlAttributes, null, '', ' '));
         	return $this->output($output, $return);
 	}
 
 	/**
  	* Creates a hidden input field.
  	*
  	* @param  string  $fieldName Name of a field, like this "First/Seceond/Third/..."
  	* @param  array    $htmlAttributes Array of HTML attributes.
  	* @param  boolean $return  Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
  	* @return mixed    Either string or echos the value, depends on AUTO_OUTPUT  and $return.
  	* @access public
  	*/
     function hidden($fieldName, $htmlAttributes = array(), $return = false) {
        	$this->setFormTag($fieldName);
        	if (!isset($htmlAttributes['value'])) {
             	$htmlAttributes['value'] = $this->tagValue($fieldName);
         	}
         	if (!isset($htmlAttributes['id'])) {
         		$id = '';
         		foreach ($this->fields as $field)
         			$id .= Inflector::camelize($field);
             	$htmlAttributes['id'] = $id;
         	}
         	return $this->output(sprintf($this->tags['hidden'], implode('][',$this->fields), $this->_parseAttributes($htmlAttributes, null, ' ', ' ')), $return);
  	}

  	 /**
  	* Creates a password input widget.
  	*
  	* @param  string  $fieldName Name of a field, like this "First/Seceond/Third/..."
  	* @param  array    $htmlAttributes Array of HTML attributes.
  	* @param  boolean $return Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
  	* @return mixed    Either string or echos the value, depends on AUTO_OUTPUT and $return.
  	* @access public
  	*/
    	function password($fieldName, $htmlAttributes = array(), $return = false) {
         	$this->setFormTag($fieldName);
         	$out = '';			
			
		if (!isset($htmlAttributes['value'])) {
            	$htmlAttributes['value'] = $this->tagValue($fieldName);
         	}

         	if (isset($htmlAttributes['help'])){
         		$id_help_t = $this->getId().'_tips';
         		$id_help = $this->getId().'_a';
         		$out .= "<a class='help' href='#' id='{$id_help}' onmouseover='tooltip_{$id_help_t}.show();' onmouseout = 'tooltip_{$id_help_t}.hide();'></a>";
         		$out .= "<script type='text/javascript'> tooltip_{$id_help_t} = new tooltips('{$id_help}','{$id_help_t}','{$htmlAttributes['help']}')</script>";
         		unset($htmlAttributes['help']);
         	}
         	
         	if (isset($htmlAttributes['label'])){
         		if (isset($htmlAttributes['label_class'])){
         			$label_class = ' class="'.$htmlAttributes['label_class'].'" ';
         			unset($htmlAttributes['label_class']);
         		} else 
         			$label_class = '';
         			
         		$out .= "<label for='".$this->getId()."'  {$label_class}>{$htmlAttributes['label']}:</label>";
         		unset($htmlAttributes['label']);
         	}
         	
         	if (!isset($htmlAttributes['type'])) {
             	$htmlAttributes['type'] = 'password';
         	}
 
         	if (!isset($htmlAttributes['id'])) {
         		$id = '';
         		foreach ($this->fields as $field)
         			$id .= Inflector::camelize($field);
             	$htmlAttributes['id'] = $id;
         	}
 
         	if ($this->tagIsInvalid($this->fields)) {
             	if (isset($htmlAttributes['class']) && trim($htmlAttributes['class']) != "") {
                 	$htmlAttributes['class'] .= ' form_error';
             	} else {
                 	$htmlAttributes['class'] = 'form_error';
             	}
         	}
         	
         	
         $out .= $this->output(
         			sprintf(
         				$this->tags['input'], 
         				implode('][',$this->fields) ,
         				$this->_parseAttributes(
         					$htmlAttributes, 
         					null, 
         					' ', 
         					' '
         				)
         			), 
         			$return
         		);
		if (!isset($htmlAttributes['validation']))
			//$out.= '<span class="novalidation"></span>';
			$out.= '';
         	return $out;
     }
 	
 	/**
  	* Creates a textarea widget.
  	*
  	* @param  string  $fieldName Name of a field, like this "First/Seceond/Third/..."
  	* @param  array    $htmlAttributes Array of HTML attributes.
  	* @param  boolean $return  Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
  	* @return mixed    Either string or echos the value, depends on AUTO_OUTPUT and $return.
  	* @access public
  	*/
     function textarea($fieldName, $htmlAttributes = array(), $return = false) {
     	$this->setFormTag($fieldName);
		$out = '';
         	$value = $this->tagValue($fieldName);
         	if (!empty($htmlAttributes['value'])) {
             	$value = $htmlAttributes['value'];
             	unset($htmlAttributes['value']);
         	}
         	if (!isset($htmlAttributes['id'])) {
         		$id = '';
         		foreach ($this->fields as $field)
         			$id .= Inflector::camelize($field);
             	$htmlAttributes['id'] = $id;
         	}
			
			if (isset($htmlAttributes['help'])){
         		$id_help_t = $this->getId().'_tips';
         		$id_help = $this->getId().'_a';
         		$out .= "<a class='help' href='#' id='{$id_help}' onmouseover='tooltip_{$id_help_t}.show();' onmouseout = 'tooltip_{$id_help_t}.hide();'></a>";
         		$out .= "<script type='text/javascript'> tooltip_{$id_help_t} = new tooltips('{$id_help}','{$id_help_t}','{$htmlAttributes['help']}')</script>";
         		unset($htmlAttributes['help']);
         	}
         	
         	if (isset($htmlAttributes['label'])){
         		if (isset($htmlAttributes['label_class'])){
         			$label_class = ' class="'.$htmlAttributes['label_class'].'" ';
         			unset($htmlAttributes['label_class']);
         		} else 
         			$label_class = '';
         			
         		$out .= "<label for='".$this->getId()."'  {$label_class}>{$htmlAttributes['label']}:</label>";
         		unset($htmlAttributes['label']);
         	}
			
         	if ($this->tagIsInvalid($this->fields)) {
             	if (isset($htmlAttributes['class']) && trim($htmlAttributes['class']) != "") {
               	$htmlAttributes['class'] .= ' form_error';
             	} else {
                 	$htmlAttributes['class'] = 'form_error';
             	}
         	}
         	$out.= $this->output(sprintf($this->tags['textarea'], implode('][',$this->fields), $this->_parseAttributes($htmlAttributes, null, ' '), $value), $return);
			return $out;
     }
     
      /**
  	* Creates a set of radio widgets.
  	*
  	* @param  string  $fieldName Name of a field, like this "First/Seceond/Third/..."
  	* @param  array    $options            Radio button options array
  	* @param  array    $inbetween      String that separates the radio buttons.
  	* @param  array    $htmlAttributes Array of HTML attributes.
  	* @param  boolean $return  Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
  	* @return mixed    Either string or echos the value, depends on AUTO_OUTPUT and $return.
  	* @access public
  	*/
     function radio($fieldName, $options, $inbetween = null, $htmlAttributes = array(), $return = false) {
     	$this->setFormTag($fieldName);
         	$value = isset($htmlAttributes['value']) ? $htmlAttributes['value'] : $this->tagValue($fieldName);
         	$out = array();
 
            if (!isset($htmlAttributes['id'])) {
         		$id = '';
         		foreach ($this->fields as $field)
         			$id .= Inflector::camelize($field);
             	$htmlAttributes['id'] = $id;
         	}
            
         	foreach ($options as $optValue => $optTitle) {
   	           
              
             	$optionsHere = array('value' => $optValue);
             	if ($value !== false && $optValue == $value) {
                 	$optionsHere['checked'] = 'checked';
             	}
             	$parsedOptions = $this->parseHtmlOptions(array_merge($htmlAttributes, $optionsHere), null, '', ' ');

                $individualTagName = $htmlAttributes['id'].'_'.$optValue;         
                //$individualTagName = "{$this->fields[sizeof($this->fields)-1]}_{$optValue}";
             	$out[] = sprintf($this->tags['radio'], implode('][',$this->fields), $individualTagName, $parsedOptions, $optTitle);
         	}
 
         	$out = join($inbetween, $out);
         	return $this->output($out ? $out : null, $return);
  	}
  	
  	/**
  	* Creates a submit widget.
 	*
  	* @param  array    $htmlAttributes Array of HTML attributes.
  	* @param  boolean $return Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
  	* @return mixed    Either string or echos the value, depends on AUTO_OUTPUT and $return.
  	* @access public
  	*/
     function submit($caption = 'Submit', $htmlAttributes = array(), $return = false) {
     	$htmlAttributes['value'] = $caption;
		unset($htmlAttributes['div']);
       	return $this->output(sprintf($this->tags['submit'], $this->_parseAttributes($htmlAttributes, null, '', ' ')), $return);
    }
	
	/**
  	* Creates a button widget.
 	*
  	* @param  array    $htmlAttributes Array of HTML attributes.
  	* @param  boolean $return Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
  	* @return mixed    Either string or echos the value, depends on AUTO_OUTPUT and $return.
  	* @access public
  	*/
     function button($caption = 'Button', $htmlAttributes = array(), $return = false) {
     	$htmlAttributes['value'] = $caption;
        	return $this->output(sprintf($this->tags['button'], $this->_parseAttributes($htmlAttributes, null, '', ' ')), $return);
    	}
	
    	
    /**
  	* This is very WYSIWYG unfriendly, use HtmlHelper::url() to get contents of "action" attribute. Version 0.9.2.
  	* @deprecated Version 0.9.2. Will not be available after 1.1.x.x
  	* @see FormHelper::create()
 	*/
   	function formTag($target = null, $type = 'post', $htmlAttributes = array()) {
     	$htmlAttributes['action']	=	$this->urlFor ($target);
         	$htmlAttributes['method']	=	$type == 'get' ? 'get' : 'post';
         	$type == 'file' ? $htmlAttributes['enctype'] = 'multipart/form-data' : null;
 
         	$append = '';
 
         	if (isset($this->params['_Token']) && !empty($this->params['_Token'])) {
          	$append .= '<p style="display: inline; margin: 0px; padding: 0px;">';
              	$append .= $this->hidden('_Token/key', array('value' => $this->params['_Token']['key'], 'id' => '_TokenKey' . mt_rand()), true);
              	$append .= '</p>';
         	}
 
         	return sprintf($this->tags['form'], $this->parseHtmlOptions($htmlAttributes, null, '')) . $append;
     }
    	
  	/**
  	* @deprecated Name changed to 'url'. Version 0.9.2.
  	* @see HtmlHelper::url()
  	*/
     function urlFor($url) {
     	return $this->url($url);
     }
    	
    function getId($fields = null){
    	if ($fields == null){
    		$fields = $this->fields;
    	}
    	$id = '';
       	foreach ($fields as $field)
       		$id .= Inflector::camelize($field);
        		
       	return strtr($id,array('|'=>''));
    }
	/**
	* @deprecated Name changed to '_parseAttributes'. Version 0.9.2.
	* @see HtmlHelper::_parseAttributes()
	* @param  array  $options Array of options.
	* @param  array  $exclude Array of options to be excluded.
	* @param  string $insertBefore String to be inserted before options.
	* @param  string $insertAfter  String to be inserted ater options.
	* @return string
	*/
	function parseHtmlOptions($options, $exclude = null, $insertBefore = ' ', $insertAfter = null) {
		if (!is_array($exclude)) {
			$exclude = array();
		}
		
		if (is_array($options)) {
			$out = array();

			foreach($options as $k => $v) {
				if (!in_array($k, $exclude)) {
					$out[] = "{$k}=\"{$v}\"";
				}
			}
			$out = join(' ', $out);
			return $out ? $insertBefore . $out . $insertAfter : null;
		} else {
			return $options ? $insertBefore . $options . $insertAfter : null;
		}
	}
	
	/**
  	* Creates a var_text widget.
  	*
  	* @param  string  $fieldName Name of a field, like this "First/Seceond/Third/..."
  	* @param  array    $htmlAttributes Array of HTML attributes.
  	* @param  boolean $return  Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
  	* @return mixed    Either string or echos the value, depends on AUTO_OUTPUT and $return.
  	* @access public
  	*/
     function var_text($fieldName, $htmlAttributes = array(), $return = false) {
     	$this->setFormTag($fieldName);
		$out = '';
         	$value = $this->tagValue($fieldName);
         	if (!empty($htmlAttributes['value'])) {
             	$value = $htmlAttributes['value'];
             	unset($htmlAttributes['value']);
         	}
         	if (!isset($htmlAttributes['id'])) {
         		$id = '';
         		foreach ($this->fields as $field)
         			$id .= Inflector::camelize($field);
             	$htmlAttributes['id'] = $id;
         	}
			         	
         	if (isset($htmlAttributes['label'])){
         		if (isset($htmlAttributes['label_class'])){
         			$label_class = ' class="'.$htmlAttributes['label_class'].'" ';
         			unset($htmlAttributes['label_class']);
         		} else 
         			$label_class = '';
         			
         		$out .= "<label for='".$this->getId()."'  {$label_class}>{$htmlAttributes['label']}:</label>";
         		unset($htmlAttributes['label']);
         	}
			
			if (isset($htmlAttributes['show_type'])){
				switch($htmlAttributes['show_type']){
					case 'date':
						$fastest = new FastestHelper(); 
						$value = $fastest->czechDate($value);
						break;
					case 'checkbox':
						$value = ($value==1)?'Ano':'Ne';
						break;
                    case 'list':
                        if(isset($htmlAttributes['list']) && isset($htmlAttributes['list'][$value]))
						  $value = $htmlAttributes['list'][$value];
						break;    
					default:
					
						break;
				}
				unset($htmlAttributes['show_type']);
			}
			
         	$out.= $this->output(sprintf($this->tags['var'], $this->_parseAttributes($htmlAttributes, null, ' '), $value), $return);
			return $out;
     }
	 
}
 ?>