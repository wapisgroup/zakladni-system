<?php
App::import('Vendor','PHPExcel',array('file' => 'excel/PHPExcel.php'));
App::import('Vendor','PHPExcelWriter',array('file' => 'excel/PHPExcel/Writer/Excel2007.php'));

class ExcelHelper extends AppHelper {
    
    var $xls;
    var $sheet;
    var $data;
    var $blacklist = array();
    var $vikends = array();
    public $styls = array(
        'svatky' => array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
        		'color'=>array('rgb'=>'00FFAA')
        	)
        ),
        'vikend' => array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
        		'color'=>array('rgb'=>'CFCFCF')
        	)
        ),
        'header' => array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
        		'color'=>array('rgb'=>'F1F2F4')
        	)
        ),
    );
    
    function excelHelper() {
        $this->xls = new PHPExcel();
        $this->sheet = $this->xls->getActiveSheet();
        $this->sheet->getDefaultStyle()->getFont()->setName('Verdana');
    }
                 
    function generate(&$data, $title = 'Report', $headers = false) {
        $this->data =& $data;
        $this->_title($title);
        if ($headers === false)
            $this->_headers();
        else
            $this->_selfHeaders($headers);
            
        $this->_rows();
        $this->_output($title);
        return true;
    }
    
    function _title($title) {
        $this->sheet->setCellValue('A2', $title);
        $this->sheet->getStyle('A2')->getFont()->setSize(14);
        $this->sheet->getRowDimension('2')->setRowHeight(23);
    }

    function _selfHeaders($headers = array(), $position = 4) {
        $i=0;
        foreach ($headers as $field => $caption) {
            if (!in_array($field,$this->blacklist)) {
                $this->sheet->setCellValueByColumnAndRow($i, $position, $caption);
            }
            
            if (!in_array($field,$this->vikends)){$class = 'header';}
            else{$class = 'vikend';}
            
            $this->sheet->getStyleByColumnAndRow($i, $position)->applyFromArray(am($this->styls[$class],array('font'=>array('bold'=>true))));
         
            $i++;
        }
        
        for ($j=1; $j<$i; $j++) {
            $this->sheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($j))->setAutoSize(true);
        }
    }

    function _headers() {
        $i=0;
        foreach ($this->data[0] as $field => $value) {
            if (!in_array($field,$this->blacklist)) {
                $columnName = Inflector::humanize($field);
                $this->sheet->setCellValueByColumnAndRow($i++, 4, $columnName);
            }
        }
        $this->sheet->getStyle('A4')->getFont()->setBold(true);
        $this->sheet->getStyle('A4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->sheet->getStyle('A4')->getFill()->getStartColor()->setRGB('969696');
        $this->sheet->duplicateStyle( $this->sheet->getStyle('A4'), 'B4:'.$this->sheet->getHighestColumn().'4');
        for ($j=1; $j<$i; $j++) {
            $this->sheet->getColumnDimension(PHPExcel_Cell::stringFromColumnIndex($j))->setAutoSize(true);
        }
    }
        
    function _rows() {
        $i=5;
        foreach ($this->data as $row) {
            $j=0;
            foreach ($row as $field => $value) {
                if(!in_array($field,$this->blacklist)) {
                    $value = strtr($value,array('<br/>' => "\n", '<br>'=>"\n",'<br />'=>"\n",'<BR/>' => "\n", '<BR>'=>"\n",'<BR />'=>"\n"));
                    if (strpos($value, "\n") !== false){
                        $this->sheet->getDefaultStyle()->getAlignment()->setWrapText(true);
                        //$value .= "TRUE";
                    } else {
                        $this->sheet->getDefaultStyle()->getAlignment()->setWrapText(false);
                       // $value .= "FALSE";
                    }
                    $value = strip_tags($value);
                    $this->sheet->setCellValueByColumnAndRow($j++,$i, $value);
                }
            }
            $i++;
        }
    }
            
    function _output($title) {
        header("Content-type: application/vnd.ms-excel"); 
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = new PHPExcel_Writer_Excel5($this->xls);
        $objWriter->setTempDir(TMP);
        $objWriter->save('php://output');
    }
    
    
    /** Specificke reporty **/
    function render_report_naklady_lektor_predpis_items($items, $souhrn){
        $title = 'Kurzy - Předpisové náklady na lektora';
        $this->_title($title);
        // nastaveni hlavicky
        $this->_selfHeaders(
            array(
                'Kat.',
		'Lektor',
		'Cena za školení',
		'Počet dní',
		'Náklady na lektora',
		'Cena VL',
		'Počet dnů VL',
		'Náklady na VL',
		'Cestovné celkem',
		'Počet dní cestovného',
		'Ubytování',
		'Počet dní ubytovné',
		'Ostatní',
		'Náklady celkem'
            )
        );
	if(isset($items) && count($items)>0):
            $i = 5;
            foreach($items as $id=>$row):
                $this->sheet->setCellValueByColumnAndRow(0,$i, $row['hodnoceni']);
		$this->sheet->setCellValueByColumnAndRow(1,$i, $row['lektor_name']);
		$this->sheet->setCellValueByColumnAndRow(2,$i, isset($row['cena_za_skoleni'])?implode("\n",array_unique($row['cena_za_skoleni'])):0);
		$this->sheet->setCellValueByColumnAndRow(3,$i, $row['pocet_dnu']);
		$this->sheet->setCellValueByColumnAndRow(4,$i, $row['lektorovne_celkem']);
		$this->sheet->setCellValueByColumnAndRow(5,$i, (isset($row['cena_za_vl'])?implode("\n",array_unique($row['cena_za_vl'])):0));
		$this->sheet->setCellValueByColumnAndRow(6,$i, $row['vl_pocet_dnu']);
		$this->sheet->setCellValueByColumnAndRow(7,$i, $row['vl_lektorovne_celkem']);
		$this->sheet->setCellValueByColumnAndRow(8,$i, isset($row['cena_za_cestovne'])?array_sum($row['cena_za_cestovne']):0);
		$this->sheet->setCellValueByColumnAndRow(9,$i, $row['cestovne_pocet_dnu']);
		$this->sheet->setCellValueByColumnAndRow(10,$i, isset($row['cena_za_ubytovani'])?array_sum($row['cena_za_ubytovani']):0);
		$this->sheet->setCellValueByColumnAndRow(11,$i, $row['ubytovani_pocet_dnu']);
		$this->sheet->setCellValueByColumnAndRow(12,$i, array_sum($row['mobilka_lektor_cena'])  + array_sum($row['ostatni_lektor_cena']));
		$this->sheet->setCellValueByColumnAndRow(13,$i, 
				 number_format(
					$row['lektorovne_celkem'] + 
					$row['vl_lektorovne_celkem'] + 
					(isset($row['cena_za_cestovne'])?array_sum($row['cena_za_cestovne']):0) + 
					(isset($row['cena_za_ubytovani'])?array_sum($row['cena_za_ubytovani']):0) + 
					array_sum($row['mobilka_lektor_cena']) + 
					array_sum($row['ostatni_lektor_cena']),
					2, ',',' '
				));
                $i++;
            endforeach;
	endif;
        
         $this->_selfHeaders(
            array(
                'Kat.',
		'Cena za školení',
		'Počet dní',
		'Náklady na lektora',
		'Cena VL',
		'Počet dnů VL',
		'Náklady na VL',
		'Cestovné celkem',
		'Počet dní cestovného',
		'Ubytování',
		'Počet dní ubytovné',
		'Ostatní',
		'Náklady celkem'
            ), $i+8
        );
       
        if(isset($souhrn) && count($souhrn)>0):
            $i += 10;
            foreach($souhrn as $id=>$row):
            
		$this->sheet->setCellValueByColumnAndRow(0,$i, $id);
		$this->sheet->setCellValueByColumnAndRow(1,$i,  @round(array_sum($row['cena_za_skoleni'])/count($row['cena_za_skoleni'])));
		$this->sheet->setCellValueByColumnAndRow(2,$i,  $row['pocet_dnu']);
		$this->sheet->setCellValueByColumnAndRow(3,$i,  $row['lektorovne_celkem']);
		$this->sheet->setCellValueByColumnAndRow(4,$i,  array_sum($row['cena_za_vl']));
		$this->sheet->setCellValueByColumnAndRow(5,$i,  $row['vl_pocet_dnu']);
		$this->sheet->setCellValueByColumnAndRow(6,$i,  $row['vl_lektorovne_celkem']);
		$this->sheet->setCellValueByColumnAndRow(7,$i,  array_sum($row['cena_za_cestovne']));
		$this->sheet->setCellValueByColumnAndRow(8,$i,  $row['cestovne_pocet_dnu']);
		$this->sheet->setCellValueByColumnAndRow(9,$i,  array_sum($row['cena_za_ubytovani']));
		$this->sheet->setCellValueByColumnAndRow(10,$i,  $row['ubytovani_pocet_dnu']);
		$this->sheet->setCellValueByColumnAndRow(11,$i,  array_sum($row['mobilka_lektor_cena']) + array_sum($row['ostatni_lektor_cena']));
		$this->sheet->setCellValueByColumnAndRow(12,$i,  number_format(
					$row['lektorovne_celkem'] + 
					$row['vl_lektorovne_celkem'] + 
					(isset($row['cena_za_cestovne'])?array_sum($row['cena_za_cestovne']):0) + 
					(isset($row['cena_za_ubytovani'])?array_sum($row['cena_za_ubytovani']):0) + 
					array_sum($row['mobilka_lektor_cena']) + 
					array_sum($row['ostatni_lektor_cena'])
					,
					2, ',',' '
				));
                $i++;
            endforeach;
	endif;

        
        $this->_output($title);
        return true;
    }
    
    /** Specificke reporty **/
    function render_print_employees($items,$new_items,$headers,$setting){
        extract($setting);
        $this->_title($title);
        if ($headers === false)
            $this->_headers();
        else
            $this->_selfHeaders($headers);
        
        /**
         * Musíme nastavit hlavičku každému klientovi
         * Pak postupně vypíšeme jednotlivé směny 
         * a za každým klientem je prázdný řádek
         */    
    	if(isset($items) && count($items)>0):
                $i = 5;
                foreach($items as $id=>$item):
                    //hlavicka
                    $this->sheet->setCellValueByColumnAndRow(0,$i, $new_items[$id]);
                    $this->sheet->getStyleByColumnAndRow(0, $i)->applyFromArray($this->styls['header']);
                    $this->sheet->mergeCells('A'.$i.':'.$this->sheet->getHighestColumn().$i);
                    /**
                     * zde vyrendrovani radku s vyplnennyma hodinama
                     * po jednotlivych smenach
                     */
                    $this->data = $item;
                                       
                	$start 	= $this->data['ConnectionClientRequirement']['from'];
                	$end 	= ($this->data['ConnectionClientRequirement']['to']== '0000-00-00')?date("Y-m-d"):$this->data['ConnectionClientRequirement']['to'];	
                	
                		
                	// render list of disabled fields
                	$not_to_disabled = array();
                	for ($j=1; $j <= $pocet_dnu_v_mesici; $j++){
                		$curr_date = $year.'-'.$month.'-'.((strlen($j) == 1)?'0'.$j:$j);
                		if (strtotime($start) <= strtotime($curr_date) && strtotime($end) >= strtotime($curr_date))
                			$not_to_disabled[] = $j;
                	} 
					$smena_captions = array(
						1	=> 'Ranní',
						2	=> 'Odpolední',
						3	=> 'Noční'
					);
                    $smenost_zobrazena = 0;
            		for($k = 1; $k <= 3; $k++):
            				if (($k == 2 && $this->data['ClientWorkingHour']['setting_shift_working_id'] == 1) || 
            					($k == 3 && in_array($this->data['ClientWorkingHour']['setting_shift_working_id'],array(1,2)))
            					|| (empty($this->data['ClientWorkingHour']['setting_shift_working_id']))
            				) { }
                            else{
                                //nazev smennosti
                                $this->sheet->setCellValueByColumnAndRow(0,$i+$k, $smena_captions[$k]);
            					$current_day = $last_day;
            					
                                //vypsani jednotlivych dnu                                
                                for($l=0; $l<$pocet_dnu_v_mesici; $l++):
                                
                                    $value_box = (isset($this->data['ClientWorkingHour']['days']['_' .$k. '_' .($l+1)]) ? $this->data['ClientWorkingHour']['days']['_' .$k. '_' .($l+1)] : ' ');
            						$this->sheet->setCellValueByColumnAndRow(1+$l,$i+$k,$value_box);
                                    if (in_array(($l+1),$svatky)) 
        								$this->sheet->getStyleByColumnAndRow(1+$l, $i+$k)->applyFromArray($this->styls['svatky']);
        							else if (in_array($current_day,array(6,7))) 
                                        $this->sheet->getStyleByColumnAndRow(1+$l, $i+$k)->applyFromArray($this->styls['vikend']);

                                    $current_day = ($current_day == 7)?$current_day=1:$current_day+1;
            					endfor;
                                $smenost_zobrazena++;
            				}
            		endfor;
            		
                    $this->sheet->setCellValueByColumnAndRow(0,$i+$smenost_zobrazena+1,' ');
            		$i = $i+$smenost_zobrazena+2;		
                endforeach;
    	endif;
        

        $this->_output($title);
        return true;
    }
}
?>