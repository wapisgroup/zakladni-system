<form action='/at_projects/centre_edit/' method='post' id='setting_project_centre_formular'>
	<?php echo $htmlExt->hidden('AtProjectCentre/id');?>
	<?php echo $htmlExt->hidden('AtProjectCentre/at_project_id');?>
	<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
            <?php if(isset($centre_id) && $centre_id != null){?>
            <li class="ousko"><a href="#krok3">Profese</a></li>
            <?php }?>
		</ul>
</div>
	<div class="domtabs admin_dom2">
		<div class="domtabs field">
			<fieldset>
				<legend>Středisko</legend>
				<?php echo $htmlExt->input('AtProjectCentre/name',array('tabindex'=>1,'label'=>'Název', 'class'=>'','label_class'=>'long'));?> <br class="clear">
		        <?php echo $htmlExt->selectTag('AtProjectCentre/spravce_majetku_id',$spravce_list,null,array('label'=>'Správce majetku', 'class'=>'','label_class'=>'long'));?><br /> 
                <?php echo $htmlExt->selectTag('AtProjectCentre/reditel_strediska_id',$spravce_list,null,array('label'=>'Ředitel', 'class'=>'','label_class'=>'long'));?><br /> 
                <?php echo $htmlExt->selectTag('AtProjectCentre/type_id',array(1=>'Středisko',2=>'Činnost'),null,array('label'=>'Typ', 'class'=>'','label_class'=>'long'),null,false);?><br />
                <div class="<?= ($this->data['AtProjectCentre']['type_id'] == 1)?'none':'';?>" id='parent'>
                <?php echo $htmlExt->selectTag('AtProjectCentre/parent_id',$center_list,null,array('label'=>'Středisko', 'class'=>'','label_class'=>'long'));?><br />
                </div>
        	</fieldset>
	    </div>
        <div class="domtabs field">
             <?php echo $this->renderElement('../at_projects/centre/work_position/index'); ?>
        </div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'ssave_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'cclose','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
     $('AtProjectCentreTypeId').addEvent('change', function(e){
         if (this.value == 1){
             $('AtProjectCentreParentId').value = '';
             $('parent').addClass('none');
         } else {
             $('parent').removeClass('none');
         }
     });


	function load_content_param(){
        domtab.goTo(1);
    }
    var domtab = new DomTabs({'className':'admin_dom2'}); 
	$('ssave_close').addEvent('click',function(e){
		new Event(e).stop();

		valid_result = validation.valideForm('setting_project_centre_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_project_centre_formular').action,		
				onComplete:function(){
					domwin.loadContent('domwin',{'onComplete': 'load_content_param'}); 
					domwin.closeWindow('domwin_profession');

				}
			}).post($('setting_project_centre_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('cclose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_profession');});
	validation.define('setting_project_centre_formular',{
		'AtProjectCentreName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('setting_project_centre_formular',<?php echo (isset($this->data['AtProjectCentre']['id']))?'true':'false';?>);
</script>