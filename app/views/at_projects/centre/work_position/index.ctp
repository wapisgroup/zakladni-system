<div class="win_fce top">
<a href='/at_projects/work_position_edit/<?php echo $centre_id;?>' class='button' id='project_centre_wp_new'>Přidat profesi</a>
</div>
<table class='table' id='table_kontakty2'>
	<tr>
		<th>ID</th>
		<th>Název</th>
        <th>Uživ. skupina</th>
		<th>Upraveno</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($profesion_list) && count($profesion_list) >0):?>
	<?php foreach($profesion_list as $item):?>
	<tr>
		<td><?php echo $item['AtProjectCentreWorkPosition']['id'];?></td>
        <td><?php echo $item['AtProjectCentreWorkPosition']['name'];?></td>
        <td><?php echo _isIsset($item['AtProjectCentreWorkPosition']['cms_group_id'],null,$cms_group_list);?></td>
		<td><?php echo $fastest->czechDate($item['AtProjectCentreWorkPosition']['updated']);?></td>
		<td><?php echo $fastest->czechDate($item['AtProjectCentreWorkPosition']['created']);?></td>
		<td>
			<a title='Editace položky' 	class='ta edit' href='/at_projects/work_position_edit/<?php echo $centre_id;?>/<?php echo $item['AtProjectCentreWorkPosition']['id'];?>'/>edit</a>
			<?php if(in_array($logged_user['CmsGroup']['id'],array(1,5))){?>
            <a title='Odstranit položku'class='ta trash' href='/at_projects/work_position_trash/<?php echo $item['AtProjectCentreWorkPosition']['id'];?>'/>trash</a>
		    <?php } ?>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K tomuto středisku nebyly nadefinovány žádné profese.</td>
	</tr>
	<?php endif;?>
</table>
<script>

	$('project_centre_wp_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_project_profession',
			sizes		: [500,250],
			scrollbars	: true,
			title		: 'Přidání nové profese',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
			}); 
	});
	if(	$('table_kontakty2').getElements('.trash'))
	$('table_kontakty2').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto profesi?')){
			new Request.JSON({
				url:this.href,
				onComplete: (function(){this.getParent('tr').dispose();}).bind(this)
			}).send();
		}
	});
	if(	$('table_kontakty2').getElements('.edit'))
	$('table_kontakty2').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_project_profession',
			sizes		: [500,250],
			scrollbars	: true,
			title		: 'Editace profese',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>