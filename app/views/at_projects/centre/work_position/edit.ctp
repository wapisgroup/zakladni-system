<form action='/at_projects/work_position_edit/' method='post' id='centre_wp_form'>
	<?php echo $htmlExt->hidden('AtProjectCentreWorkPosition/id');?>
	<?php echo $htmlExt->hidden('AtProjectCentreWorkPosition/at_project_centre_id');?>
    <?php echo $htmlExt->hidden('old_group_id');?>

			<fieldset>
				<legend>Profese</legend>
				<?php echo $htmlExt->input('AtProjectCentreWorkPosition/name',array('tabindex'=>1,'label'=>'Název', 'class'=>'','label_class'=>'long'));?> <br class="clear">
		        <?php echo $htmlExt->selectTag('AtProjectCentreWorkPosition/cms_group_id',$cms_group_list,null,array('label'=>'Uživatelská skupina','label_class'=>'long'));?><br /> 
           
            </fieldset>
	
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close_wp','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close_wp','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	function load_content_param(){
        domtab.goTo(1);
    }
    
	$('save_close_wp').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('centre_wp_form');
		if (valid_result == true){
			new Request.JSON({
				url:$('centre_wp_form').action,		
				onComplete:function(){
					domwin.loadContent('domwin_profession',{'onComplete': 'load_content_param'}); 
					domwin.closeWindow('domwin_project_profession');

				}
			}).post($('centre_wp_form'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close_wp').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_project_profession');});
	validation.define('centre_wp_form',{
		'AtProjectCentreWorkPositionName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('centre_wp_form',<?php echo (isset($this->data['AtProjectCentreWorkPosition']['id']))?'true':'false';?>);
</script>