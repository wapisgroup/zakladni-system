<?php
    $type_list = array(1=>'Středisko',2=>'Činnost');
    $centre_list_all[0] = '...';
?>
<div class="win_fce top">
<a href='/at_projects/centre_edit/<?php echo $project_id;?>' class='button' id='company_centre_add_new'>Přidat středisko</a>
</div>
<table class='table' id='table_centre'>
	<tr>
		<th>ID</th>
		<th>Typ</th>
		<th>Název</th>
		<th>Středisko</th>
		<th>Upraveno</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($centre_list) && count($centre_list) >0):?>
	<?php foreach($centre_list as $item):?>
	<tr>
		<td><?php echo $item['AtProjectCentre']['id'];?></td>
        <td><?php echo $type_list[$item['AtProjectCentre']['type_id']];?></td>
        <td><?php echo $item['AtProjectCentre']['name'];?></td>
        <td><?php echo $centre_list_all[$item['AtProjectCentre']['parent_id']];?></td>
		<td><?php echo $fastest->czechDate($item['AtProjectCentre']['updated']);?></td>
		<td><?php echo $fastest->czechDate($item['AtProjectCentre']['created']);?></td>
		<td>
			<a title='Editace položky' 	class='ta edit' href='/at_projects/centre_edit/<?php echo $project_id;?>/<?php echo $item['AtProjectCentre']['id'];?>'/>edit</a>
			<?php if(in_array($logged_user['CmsGroup']['id'],array(1,5))){?>
            <a title='Odstranit položku'class='ta trash' href='/at_projects/centre_trash/<?php echo $item['AtProjectCentre']['id'];?>'/>trash</a>
		    <?php } ?>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K tomuto projektu nebyly nadefinovány žádné střediska.</td>
	</tr>
	<?php endif;?>
</table>
<script>
	$('company_centre_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_profession',
			sizes		: [600,600],
			scrollbars	: true,
			title		: 'Přidání nového střediska',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
			}); 
	});
	if(	$('table_centre').getElements('.trash'))
	$('table_centre').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat toto středisko?')){
			new Request.JSON({
				url:this.href,
				onComplete: (function(){this.getParent('tr').dispose();}).bind(this)
			}).send();
		}
	});
	if(	$('table_centre').getElements('.edit'))
	$('table_centre').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_profession',
			sizes		: [600,600],
			scrollbars	: true,
			title		: 'Editace střediska',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>