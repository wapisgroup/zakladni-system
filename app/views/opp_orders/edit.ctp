<form action="/opp_orders/edit/" method="post" id='opp_order_form'>
<?php echo $htmlExt->hidden('OppOrder/id');?>
<table class="table tabulka">
	<tr>
		<th>Druh</th>
		<th>Typ</th>
		<th>Velikost</th>
		<th>Pocet</th>
		<th>Cena CZ</th>
        <th>Cena EU</th>
        <th>Podnik</th>
		<th>Potvrzeni</th>
		<th>Moznosti</th>
	</tr>
	<tbody id='sub_table'>
		<tr class="od">
			<td><?php echo $htmlExt->selectTag('OppOrderItem/-1/type_id',$type_list, null, array('class'=>'select_type CE max_size','id'=>'first_select_type'));?></td>
			<td><select class='select_name CE max_size' name='data[OppOrderItem][-1][name]'><option>---</option></select></td>
			<td><select class='select_size CE max_size' name='data[OppOrderItem][-1][size]'><option>----</option></select></td>
			<td><input class='input_count' type="text" name='data[OppOrderItem][-1][count]' value="0"/></td>
			<td><input class='input_price_cz' type="text" name='data[OppOrderItem][-1][price_cz]'value="0" readonly="readonly"/></td>
            <td><input class='input_price_eu' type="text" name='data[OppOrderItem][-1][price_eu]'value="0" readonly="readonly"/></td>
            <td><?php echo $htmlExt->selectTag('OppOrderItem/-1/company_id',$company_list, null, array('class'=>'select_company'));?></td>
			<td><input type="checkbox" name='data[OppOrderItem][-1][stav]'/></td>
			<td>
				<a href='#' class="ta add add_row" id='clone_row'>Add</a>
				<a href='#' class="ta delete delete_row none CE">Delete</a>
			</td>
		</tr>
		<?php if (isset($this->data['OppOrderItem']) && count($this->data['OppOrderItem']) > 0):?>
		<?php foreach($this->data['OppOrderItem'] as $k => $itm):?>
		<tr>
			<td><?php echo $htmlExt->selectTag('OppOrderItem/'.$k.'/type_id',$type_list, null, array('class'=>'select_type CE max_size'));?></td>
			<td><?php echo $htmlExt->selectTag('OppOrderItem/'.$k.'/name',$names[$itm['type_id']], null, array('class'=>'select_name CE max_size'));?></td>
			<td><?php echo $htmlExt->selectTag('OppOrderItem/'.$k.'/size',$sizes[$itm['name']], null, array('class'=>'select_size CE max_size'));?></td>
			<td><?php echo $htmlExt->input('OppOrderItem/'.$k.'/count',array('class'=>'input_count'));?></td>
			<td><?php echo $htmlExt->input('OppOrderItem/'.$k.'/price_cz',array('class'=>'input_price_cz','readonly'=>'readonly'));?></td>
            <td><?php echo $htmlExt->input('OppOrderItem/'.$k.'/price_eu',array('class'=>'input_price_eu','readonly'=>'readonly'));?></td>
			<td><?php echo $htmlExt->selectTag('OppOrderItem/'.$k.'/company_id',$company_list, null, array('class'=>'select_company'),null,false);?></td>
            <td><?php echo $htmlExt->checkbox('OppOrderItem/'.$k.'/stav',array('class'=>'input_stav'));?></td>
			<td>
				<a href='#' class="ta delete delete_row CE">Delete</a>
			</td>
		</tr>
		<?php endforeach;?>
		<?php endif;?>
	</tbody>
</table>
<div id="choose_template" class="none">
   <?php echo $htmlExt->selectTag('OppOrder/opp_template_id',$template_list, null, array('class'=>'','label'=>'Vyberte šablonu OPP'));?><br />    
</div>
<div class='win_save'>
	<?php if ($this->data['OppOrder']['status'] != 1):?>
	<input type="button" id='save_order' value="Odeslat objednávku" />
	<?php endif;?>
	<input type="button" id='close_order' value='Zavřít' />
</div>
</form>
<script language="javascript">
   	var logged_user = '<?php echo $logged_user['CmsUser']['id'];?>';
    var empty_value = '--- Zvolte hodnotu ---';
    
	$('close_order').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin');	
	})
    
    if($('OppOrderId').value != '')
        change_size_value_to_rel(); //pri zacatku editaci nastav spravne selecty
    else{
        if(logged_user == 1){
            $('sub_table').addClass('none');
            $('choose_template').removeClass('none');
            $('save_order').addClass('none');
            
            var children = [$('first_select_type')];  
            $('OppOrderOppTemplateId').addEvent('change', function(){
                //vytvoreni spravneho linku pro nacteni povolenych typu
                $$('.select_type').each(function(itm){
                    itm.empty();
            		itm.ajaxLoad('/opp_orders/load_typ2/'+this.value+'/',[itm.getParent('tr').getElement('.select_name')]);
            	},this);
                
                //request
    			new Request.JSON({
    				url: '/opp_orders/load_kind/' + this.value,
    				onComplete: (function(json){
    				    if(json != false){
        					children.each(function(obj){
        						$(obj).empty();
        						new Element('option', {title:empty_value, value:''}).setHTML(empty_value).inject($(obj));
        					});
        					child = children[0];
     
            				$each(json, function(value, id){
            				    vals = value.split('|');
                                value = vals[0];
                                rel = vals[1];
            					new Element('option', {title:value, value:id,rel:rel}).setHTML(value).inject($(child));
            				}, this);
    	       
                        }
                        else{
                            /**
                             * vyresetuj pokud se jedna o prazdne pole
                             */
                       	    child = children[0];
                            $(child).empty();
                            new Element('option', {title:empty_value, value:''}).setHTML(empty_value).inject($(child));
                        }  
                        
                        $('sub_table').removeClass('none');
                        $('choose_template').addClass('none');
                        $('save_order').removeClass('none');                   
    				}).bind(this)
    			}).send();
		    });
        }
    }   
     
    function change_size_value_to_rel(){
        $each($('sub_table').getElements('tr').getElement('select.select_size'),function(sel){
            $each(sel.options,function(opt){
                     if(opt.value != ""){ 
                        val_array = opt.title.split('|');
                        opt.title = val_array[0];
                        opt.setHTML(val_array[0]);
                        opt.setProperty('rel',val_array[1]); 
                     }   
            });
        })
    }

	if($('save_order'))
	$('save_order').addEvent('click', function(e){
		e.stop();
		$('sub_table').getElement('tr').getElements('input, select').setProperty('disabled','disabled');
        
		new Request.JSON({
			url: $('opp_order_form').getProperty('action'),
			onComplete: function(json){
				if (json){
					if (json.result === true){
						alert('Objednávka byla zaznamenána');
						domwin.closeWindow('domwin');
						click_refresh();
					} else {
						alert(json.message);
					}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).send($('opp_order_form'));
	})

	$$('.delete_row').addEvent('click', function(e){
		e.stop();
		this.getParent('tr').dispose();
	})

	function valid_row(){
		var tr = $('sub_table').getElement('tr');
		var output = Array();
		
		if (tr.getElement('.select_type').value == '')  output.push('Musite zvolit druh opp');
		if (tr.getElement('.select_name').value == '')  output.push('Musite zvolit typ opp');
		if (tr.getElement('.select_size').value == '')  output.push('Musite zvolit velikost opp');
        if (tr.getElement('.select_company').value == '')  output.push('Musite zvolit podnik');
		if (tr.getElement('.input_count').value == '' && tr.getElement('.input_count').value != '0')  output.push('Musite zvolit pocet opp');

		if (output.length > 0){
			var al = new MyAlert();
			al.show(output);
		} else {
			return true;
		}
	}

	$('clone_row').addEvent('click', function(e){
		e.stop();
		
		if (valid_row()){
			var tr = this.getParent('tr'), clone = tr.clone();
			$('sub_table').adopt(clone);
		
            clone.removeClass('od');
			clone.getElement('.add_row').dispose();
			clone.getElement('.delete_row').removeClass('none');
            
            var load_type_url = '/opp_orders/load_typ/';
            if(logged_user == 1){ load_type_url = '/opp_orders/load_typ2/'+$('OppOrderOppTemplateId').value+'/';}
			clone.getElement('.select_type').ajaxLoad(load_type_url,[clone.getElement('.select_name')]);
		
            clone.getElement('.select_name').addEvent('change',function(e){select_name(e);});
			clone.getElement('.select_size').addEvent('change',function(e){select_size(e);});
            clone.getElement('.select_company').getElement('option.option_empty').dispose();
            
			tr.getElement('.select_name').empty();
			tr.getElement('.select_size').empty();
             
			tr.getElements('.select_type, .input_count, .input_price_cz, .input_price_eu').setValue('');
		
			var uni = uniqid();
			increase_name(clone,1,uni);
		}
	})
	
	function increase_name(parent,index,prefix){
 		parent.getElements('input, select').each(function(el){
 			atrs = el.name.split('][');
 			prt = atrs[index];
			atrs[index] = prefix;
 		 	el.name = atrs.join('][');
	 	})
 	}

    if(logged_user != 1)
    	$$('.select_type').each(function(itm){
    		itm.ajaxLoad('/opp_orders/load_typ/',[itm.getParent('tr').getElement('.select_name')]);
    	});
	if($('OppOrderId').value != '' && logged_user == 1){
	    $$('.select_type').each(function(itm){
    		itm.ajaxLoad('/opp_orders/load_typ2/'+$('OppOrderOppTemplateId').value+'/',[itm.getParent('tr').getElement('.select_name')]);
    	});
	}
    
    $$('.select_name').addEvent('change',function(e){
		select_name(e);
	});
    
    function select_name(e){
        var itm = e.target;
        var children = [itm.getParent('tr').getElement('.select_size')];    
		new Request.JSON({
			url: '/opp_orders/load_size/' + itm.value,
			onComplete: (function(json){
			    if(json != false){
					children.each(function(obj){
						$(obj).empty();
						new Element('option', {title:empty_value, value:''}).setHTML(empty_value).inject($(obj));
					});
					child = children[0];

    				$each(json, function(value, id){
    				    vals = value.split('|');
                        value = vals[0];
                        rel = vals[1];
    					new Element('option', {title:value, value:id,rel:rel}).setHTML(value).inject($(child));
    				}, itm);
       
                }
                else{
                    /**
                     * vyresetuj pokud se jedna o prazdne pole
                     */
               	    child = children[0];
                    $(child).empty();
                    new Element('option', {title:empty_value, value:''}).setHTML(empty_value).inject($(child));
                }                     
			}).bind(itm)
		}).send();
    }    
	
	function select_size(e){
		var itm = e.target;
		new Request.JSON({
			url: '/opp_orders/load_count_price/'+itm.options[itm.selectedIndex].getProperty('rel'),
			onComplete: function(json){
				if (json){
					if (json.result === true){
						tr = itm.getParent('tr');
						tr.getElement('.input_count').value = json.data.OppItem.count;
						tr.getElement('.input_price_cz').value = json.data.OppItem.price_cz;
                        tr.getElement('.input_price_eu').value = json.data.OppItem.price_euro;
					} else {
						alert(json.mesage);
					}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).send();
	}
	
	$$('.select_size').addEvent('change',function(e){
		select_size(e);
	});
</script>