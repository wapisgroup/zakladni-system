<div class="win_fce top">
<a href='/audit_estates_loss_events/attachs_edit/<?php echo $audit_estates_loss_event_id;?>' class='button edit' id='company_attach_add_new' title='Přidání nové přílohy'>Přidat přílohu</a>
</div>
<table class='table' id='table_attachs'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Typ</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($attachment_list) && count($attachment_list) >0):?>
	<?php foreach($attachment_list as $attachment_item):?>
	<tr>
		<td><?php echo $attachment_item['AuditEstateLossEventAttachment']['id'];?></td>
		<td><?php echo $attachment_item['AuditEstateLossEventAttachment']['name'];?></td>
		<td><?php echo $attachment_item['SettingAttachmentType']['name'];?></td>
		<td><?php echo $fastest->czechDateTime($attachment_item['AuditEstateLossEventAttachment']['created']);?></td>
		<td>
			<a title='Editace přílohy "<?php echo $attachment_item['AuditEstateLossEventAttachment']['name'];?>"' 	class='ta edit' href='/audit_estates_loss_events/attachs_edit/<?php echo $audit_estates_loss_event_id;?>/<?php echo $attachment_item['AuditEstateLossEventAttachment']['id'];?>'/>edit</a>
			<a title='Odstranit přílohu'class='ta trash' href='/audit_estates_loss_events/attachs_trash/<?php echo $audit_estates_loss_event_id;?>/<?php echo $attachment_item['AuditEstateLossEventAttachment']['id'];?>'/>trash</a>
			<a title='Stáhnout přílohu'class='ta download' href='/audit_estates_loss_events/attachs_download/audit_estates_loss_events|attachment|<?php echo $attachment_item['AuditEstateLossEventAttachment']['file'];?>/<?php echo $attachment_item['AuditEstateLossEventAttachment']['alias_'];?>/'/>download</a>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této události nebyly nadefinovány přílohy</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($attachment_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListAuditEstateLossEventAttachmentClose' class='button'/>
	</div>
<script>
	$('ListAuditEstateLossEventAttachmentClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach');});

	$('table_attachs').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto přílohu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('domwin_attach').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_attach_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>