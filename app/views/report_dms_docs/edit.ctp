<form action='/report_dms_docs/edit/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('ReportDmsDoc/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<?php echo $htmlExt->input('ReportDmsDoc/name',array('tabindex'=>1,'label'=>'Název', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
			    <?php echo $htmlExt->selectTag('ReportDmsDoc/report_dms_type_id',$type_list,null,array('tabindex'=>2,'label'=>'Typ', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
    			<?php echo $htmlExt->input('ReportDmsDoc/sender',array('tabindex'=>3,'label'=>'Odesílatel', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
    			<?php echo $htmlExt->input('ReportDmsDoc/addressee_name',array('tabindex'=>4,'label'=>'Adresát - jméno', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
    			<?php echo $htmlExt->selectTag('ReportDmsDoc/at_project_id',$project_list,null,array('tabindex'=>5,'label'=>'Adresát - projekt', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
    			<?php echo $htmlExt->selectTag('ReportDmsDoc/at_project_centre_id',$centre_list,null,array('tabindex'=>6,'label'=>'Adresát - středisko', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
    			<?php echo $htmlExt->selectTag('ReportDmsDoc/report_dms_type_delivery_id',$type_delivery_list,null,array('tabindex'=>7,'label'=>'Způsob fyzického doručení', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
    			<?php echo $htmlExt->checkbox('ReportDmsDoc/delivery',null,array('tabindex'=>8,'label'=>'Fyzicky doručeno', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
    			<?php echo $htmlExt->textarea('ReportDmsDoc/description',array('tabindex'=>9,'label'=>'Popis', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
			     
                
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
    $('ReportDmsDocAtProjectId').ajaxLoad('/report_dms_docs/load_at_project_centre_list/',['ReportDmsDocAtProjectCentreId']);
    
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('ReportDmsDocId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
		'ReportDmsDocName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['ReportDmsDoc']['id']))?'true':'false';?>);
</script>