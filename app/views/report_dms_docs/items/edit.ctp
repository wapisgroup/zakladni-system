<form id='add_edit_group_formular' action='/report_dms_docs/item_edit/<?= $model;?>/' method='post'>
	<?php echo $htmlExt->hidden($model.'/id');?>
	<fieldset>
		<legend>Základní</legend>
		<?php echo $htmlExt->input($model.'/name',array('label_class'=>'long','class'=>'long','label'=>'Název'));?><br />
	</fieldset>	
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditGroupSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditGroupClose'/>
	</div>
</form>
<script>
var model = "<?= $model;?>";
var go_to = (model == "ReportDmsType"?0:1);

$('AddEditGroupClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_item_edit');});

$('AddEditGroupSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('add_edit_group_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('add_edit_group_formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
							domwin.loadContent('domwin_groups',{goto:go_to});
							domwin.closeWindow('domwin_item_edit');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
					
				}
			}).post($('add_edit_group_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	
	validation.define('add_edit_group_formular',{
		'<?= $model;?>Name': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}		
	});
	validation.generate('add_edit_group_formular',<?php echo (isset($this->data[$model]['id']))?'true':'false';?>);

</script>