<?php
/**
 * pokud je nastaven duplicitni mod vloz view
 */
if($duplicity_domwin)
    echo $this->renderElement('../clients/duplicity/index'); 

if($only_show){
    $domtabs = 'admin_dom_show';
    $form_id = 'client_edit_formular_show';
}
else{
    $form_id = 'client_edit_formular';
    $domtabs = 'admin_dom';
}
?>
<div id="client_edit_<?php echo ($duplicity_domwin == true ? 'half' : 'full');?>">
<form action='/import_clients/edit/' method='post' id='<?php echo $form_id?>'>
	<?php echo $htmlExt->hidden('ImportClient/id'); ?>
	<input type="hidden" name="data[NewConnection]" id="new_connection" value="0" />
	
	<div class="domtabs <?php echo $domtabs?>_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>			
		</ul>
	</div>
	<div class="domtabs <?php echo $domtabs?>">
		<div class="domtabs field">
			<fieldset>
				<legend>Osobní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('ImportClient/jmeno', array('tabindex' => 1, 'label' =>'Jméno')); ?><br />
					<?php echo $htmlExt->input('ImportClient/prijmeni', array('tabindex' => 3,'label' => 'Příjmení')); ?><br />
				</div>
				<div class="slr">  
					<?php echo $htmlExt->selectTag('ImportClient/pohlavi_list', $pohlavi_list, null,array('tabindex' => 2, 'label' => 'Pohlaví')); ?><br />
					<?php echo $htmlExt->inputDate('ImportClient/datum_narozeni', array('tabindex' =>4, 'label' => 'Datum narození')); ?><br />
				</div>
			</fieldset>	
			<fieldset>
				<legend>Kontaktní údaje</legend>
				<div class="sll3">  
                    <p>Telefony uvádějte ve formátu (00420123456789)</p>
					<?php echo $htmlExt->input('ImportClient/mobil1', array('tabindex' => 5, 'label' =>'Telefon', 'class' => 'mobile integer', 'MAXLENGTH' => 14)); ?><br />		
					<?php echo $htmlExt->input('ImportClient/mobil2', array('tabindex' => 6, 'label' =>'Telefon 2', 'class' => 'mobile integer', 'MAXLENGTH' => 14)); ?><br />
					<?php echo $htmlExt->input('ImportClient/mobil3', array('tabindex' => 7, 'label' =>'Telefon 3', 'class' => 'mobile integer', 'MAXLENGTH' => 14)); ?><br />
		              <br />
                    <?php echo $htmlExt->input('ImportClient/email', array('tabindex' =>8, 'label' => 'Email')); ?><br />
			
					<label>Souhlasí se zpracovanim údajů a kontaktováním:</label><?php echo $htmlExt->checkbox('ImportClient/agree_contact', null, array('checked' => 'checked')); ?><br/>
				</div>
				<div class="sll15">  
                    <p>&nbsp;</p>
					<?php echo $htmlExt->radio('ImportClient/mobil_active', array('mobil1' => 'Platný','mobil2' => 'Platný', 'mobil3' => 'Platný'), '<br />', array('class' => 'radio')); ?><br />
                    <?php if(isset($this->data['ImportClient']['id'])) { ?>
                        <p><br />
                            <a href='/clients/add_activity/<?php echo $this->data['ImportClient']['id'];?>' class='button edit add_activity' title='Přidání novou aktivitu'>Přidat aktivitu</a>
				        </p>
                    <?php }?>
                </div>
                <div class="sll3" > 
                    <p>&nbsp;</p>
                    <?php //pr($this->data['ImportClient']);?>
					<?php echo $htmlExt->input('ImportClient/ulice', array('tabindex' => 9, 'label' =>'Ulice')); ?><br />
					<?php echo $htmlExt->input('ImportClient/mesto', array('tabindex' => 10, 'label' =>'Město')); ?><br />		
                    <?php echo $htmlExt->input('ImportClient/psc', array('tabindex' => 11, 'label' =>'PSČ')); ?><br />
                    <?php echo $htmlExt->selectTag('ImportClient/countries_id', $client_countries_list, null,array('tabindex' => 12, 'label' => 'Okres'), null, false); ?><br />
                    <?php echo $htmlExt->selectTag('ImportClient/kraj_id', $client_kraj_list, null,array('tabindex' => 12, 'label' => 'Kraj'), null, false); ?><br />
					<?php echo $htmlExt->selectTag('ImportClient/stat_id', $client_stat_list, null,array('tabindex' => 12, 'label' => 'Stát')); ?><br />
					
				</div>
			</fieldset>
            <fieldset>
                <legend>Hledám</legend>
                    <div class="sll">
                        <?php echo $htmlExt->selectTag('ImportClient/hledam', $form_www_hledam, null,array( 'label' => 'Hledám')); ?><br />
                        <?php echo $htmlExt->selectTag('ImportClient/work_stat',array(0=>'Celý stát')+$client_stat_list, null,array( 'label' => 'Stát')); ?><br />
                    
                    </div>
                    <div class="slr">
                        <?php echo $htmlExt->selectTag('ImportClient/profese', $form_www_profese, null,array( 'label' => 'Profese')); ?><br />
                        <?php echo $htmlExt->selectTag('ImportClient/work_kraj',array(0=>'Celý stát')+$client_kraj_list, null,array( 'label' => 'Kraj')); ?><br />
                    
                    </div>
            </fieldset>
			<fieldset>
                <legend>Životopis</legend>
                    <?php 
                    if (isset($this->data['ImportClient']['files']) && !empty($this->data['ImportClient']['files'])){ 
                        foreach ($this->data['ImportClient']['files'] AS $file)
                           // echo '<var>'.$html->link('Stáhnout životopis','http://www.atgroup.sk/uploaded/form_zivotopis/'.$file,array('target'=>'_blank'),null,false).'</var>';
                    } else {
                        echo '<p>Nejsou připojeny žádné životopisy.</p>';
                        
                    }?>
            </fieldset>
			
	
			<fieldset>
				<legend>Pracovní zkušenosti</legend>
				<?php if(!isset($only_show)): ?>
					<div class="sll">
						<?php echo $htmlExt->selectTag('ImportClient/kvalifikace_list', $kvalifikace_list, null,  array('tabindex' => 13, 'label' => 'Profese')); ?>
						<?php echo $htmlExt->input('CarrerItems/data_name', array('tabindex' => 14,  'label' => 'Název zaměstnavatele')); ?><br />
				        <?php echo $htmlExt->input('CarrerItems/data_od', array('tabindex' => 16,  'label' => 'Období od')); ?>
						<?php echo $htmlExt->input('CarrerItems/data_do', array('tabindex' => 17,  'label' => 'Období do')); ?><br />
					    <label></label><input style="margin-top:10px; margin-left:3px;" type="button" value="Přidat pracovní zkušenost" tabindex="18" class="button" id="add_kvalifikace" />
					
                	</div>
					<div class="slr" >  
					  	<?php echo $htmlExt->textarea('CarrerItems/data_popis', array('tabindex' =>  15,'label' =>'Popis pracovní náplně', 'class' => 'long', 'rows' => 3)); ?><br/>
					
					</div><br />
				<br />
				<br />
				<?php endif; ?>
					<table class="tabulka" id='kvalifikace_list'>
					<?php if(isset($kvalifikace_list_item)): ?>
						<?php foreach($kvalifikace_list_item as $kval): ?>
						<tr class="kvalifikace_row_<?php echo $kval['ImportConnectionClientCareerItem']['id'] ?>">	
							<td width="150px;">Kvalifikace:</td>
							<td><?php echo $kvalifikace_list[$kval['ImportConnectionClientCareerItem']['setting_career_item_id']]; ?><input type='hidden' name='data[ImportClient][kvalifikace_list_item][]' value='<?php echo
    $kvalifikace_list[$kval['ImportConnectionClientCareerItem']['setting_career_item_id']]; ?>'/></td>
							<td>
								<?php if(!isset($only_show)): ?>
									<a href='#' rel="<?php echo $kval['ImportConnectionClientCareerItem']['id'] ?>" class='remove_from_kvalifikace_list'>Odstranit</a>
								<?php endif; ?>
							</td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ImportConnectionClientCareerItem']['id'] ?>">
							<td>Název zaměstnavatele:</td>
							<td colspan="2"><?php echo $kval['ImportConnectionClientCareerItem']['name']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ImportConnectionClientCareerItem']['id'] ?>">
							<td>Období:</td>
							<td colspan="2"><strong>od</strong> <?php echo $kval['ImportConnectionClientCareerItem']['od']; ?> -
                                <strong>do</strong> <?php echo $kval['ImportConnectionClientCareerItem']['do']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ImportConnectionClientCareerItem']['id'] ?>">
							<td>Popis pracovní náplně:</td>
							<td colspan="2"><?php echo $kval['ImportConnectionClientCareerItem']['popis']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ImportConnectionClientCareerItem']['id'] ?>">
							<td colspan="3" class="line">
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ImportConnectionClientCareerItem']['id'] .
    '/name', array('value' => $kval['ImportConnectionClientCareerItem']['name'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ImportConnectionClientCareerItem']['id'] .
    '/popis', array('value' => $kval['ImportConnectionClientCareerItem']['popis'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ImportConnectionClientCareerItem']['id'] .
    '/od', array('value' => $kval['ImportConnectionClientCareerItem']['od'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ImportConnectionClientCareerItem']['id'] .
    '/do', array('value' => $kval['ImportConnectionClientCareerItem']['do'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ImportConnectionClientCareerItem']['id'] .
    '/setting_career_item_id', array('value' => $kval['ImportConnectionClientCareerItem']['setting_career_item_id'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
					</table>
			</fieldset>
			
			
	
			<fieldset>
				<legend>Certifikáty a zkoušky</legend>
			<?php if(!isset($only_show)): ?>
				<div class="sll">
						<?php echo $htmlExt->selectTag('CertifikatyItems/certifikaty_list', $certifikaty_list, null,  array('tabindex' => 19, 'label' => 'Název certifikátu')); ?>
						<?php echo $htmlExt->input('CertifikatyItems/certifikaty_platnost', array  ('tabindex' => 21, 'label' => 'Platný do', 'MAXLENGTH' => 4, 'class' =>
    'integer')); ?>
            	</div>
					<div class="slr" >  
							<label></label><input type="button" value="Přidat nový certifikát" class="button" tabindex="22" id="add_certifikat" />
				 </div><br />
				 <?php echo $htmlExt->input('CertifikatyItems/certifikaty_komentar', array('tabindex' =>  20, 'class' => 'long', 'label_class' => 'long', 'label' => 'Komentář')); ?><br />
				
				<br />
				<br />
			<?php endif; ?>
					<table class="tabulka" id='certifikaty_list'>
					<?php if(isset($certifikaty_list_item)): ?>
						<?php foreach($certifikaty_list_item as $cert): ?>
						<tr class="kvalifikace_row_<?php echo $cert['ImportConnectionClientCertifikatyItem']['id'] ?>">	
							<td>Typ:</td>
							<td><?php echo $certifikaty_list[$cert['ImportConnectionClientCertifikatyItem']['setting_certificate_id']]; ?></td>
							<td></td>
							<td>
								<?php if(!isset($only_show)): ?>
									<a href='#' rel="<?php echo $cert['ImportConnectionClientCertifikatyItem']['id'] ?>" class='remove_from_certifikaty_list'>Odstranit</a>
								<?php endif; ?>
							</td>
						</tr>
						
						<tr class="kvalifikace_row_<?php echo $cert['ImportConnectionClientCertifikatyItem']['id'] ?>">
							<td>Komentář:</td>
							<td><?php echo $cert['ImportConnectionClientCertifikatyItem']['komentar']; ?></td>
							<td>Platný do:</td>
							<td><?php echo $cert['ImportConnectionClientCertifikatyItem']['platnost']; ?></td>
						</tr>
						
						<tr class="kvalifikace_row_<?php echo $cert['ImportConnectionClientCertifikatyItem']['id'] ?>">
							<td colspan="4" class="line">
								<?php echo $htmlExt->hidden('CertifikatyItem/' . $cert['ImportConnectionClientCertifikatyItem']['id'] .
    '/komentar', array('value' => $cert['ImportConnectionClientCertifikatyItem']['komentar'])); ?>
								<?php echo $htmlExt->hidden('CertifikatyItem/' . $cert['ImportConnectionClientCertifikatyItem']['id'] .
    '/platnost', array('value' => $cert['ImportConnectionClientCertifikatyItem']['platnost'])); ?>
								<?php echo $htmlExt->hidden('CertifikatyItem/' . $cert['ImportConnectionClientCertifikatyItem']['id'] .
    '/list', array('value' => $cert['ImportConnectionClientCertifikatyItem']['setting_certificate_id'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
					</table>
			</fieldset>
		<?php if(isset($this->data['ImportClient']['id'])) { ?>		
		<?php } ?>	
			
			<fieldset>
				<legend>Vzdělání</legend>
					
				<div class="sll">  
					<?php echo $htmlExt->selectTag('ImportClient/dosazene_vzdelani_list', $dosazene_vzdelani_list, null,
array('tabindex' => 23, 'label' => 'Dosažené vzdělání')); ?>
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('ImportClient/dosazene_vzdelani_obor', $kvalifikace_list, null,
array('tabindex' => 23, 'label' => 'Obor')); ?>
					
				</div>
				<br />
			</fieldset>
			<fieldset>
				<legend>Poznamka</legend>
					<?php echo $htmlExt->textarea('ImportClient/poznamka', array('label' =>'Poznamka', 'class' => 'long', 'label_class' => 'long')); ?><br/>
					<?php echo $htmlExt->input('ImportClient/spec_id', array('tabindex' => 8, 'label' =>'Speciální ID', 'class' => 'long', 'label_class' => 'long')); ?><br />
			</fieldset>
			<fieldset>
				<legend>Stav importu</legend>
				<?php echo $htmlExt->selectTag('ImportClient/import_stav', $stav_importu_list, null,array('label' => 'Stav importu', 'class' => 'long', 'label_class' => 'long')); ?>
			</fieldset>
			<?php if(isset($this->data['ImportClient']['id'])) { ?>
				<div class="krok">
					<a href='#krok2' class='admin_dom_move next'>Další krok</a>
				</div>
			<?php } ?> 
		</div>
		       
		
	</div>
	<div class="win_save">
		<?php if(!isset($only_show)): ?>
			<?php echo $htmlExt->button('Uložit', array('id' => 'save_close', 'class' =>
  'button')); ?>
		<?php endif; ?>
		<?php echo $htmlExt->button('Zavřít', array('id' => 'close', 'class' =>
'button')); ?>
	</div>
</form>
</div>
<?php $dataPocetProfesi = (isset($kvalifikace_list_item) ? count($kvalifikace_list_item) :
0); ?>
 <script language="javascript" type="text/javascript">
 var form_id = '<?php echo $form_id;?>';
 
	var domtab = new DomTabs({'className':'<?php echo $domtabs?>'}); 
	 $(form_id).getElements('.float, .integer').inputLimit();
     
     
     $$('.add_activity').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_message_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});

	 
	$(form_id).getElements('.radio').addEvent('click',function(e){
        
		if($('ImportClient' + ucfirst(this.value)).value == ''){
           e.stop();  
           alert('Hodnota telefonu je prázdna, telefon musí být vyplńen aby mohl být platný.');
        }
        else if($('ImportClient' + ucfirst(this.value)).value.length < 14){
           e.stop();  
           alert('Telefon musí mít 14 znaků, aby mohl být platný.');
        }
	});
	 
	 
	if($('ImportClientStatId'))
		$('ImportClientStatId').ajaxLoad('/clients/load_ajax_okresy/',['ImportClientCountriesId']);
	
	var dataPocetProfesi = <?php echo $dataPocetProfesi; ?>;
	var pocetProfesi = ((dataPocetProfesi==0) ? 0 : dataPocetProfesi);

	/* VALIDATION CLIENT TO RECRUITER */
	
//	$('calbut_ImportClientDatumNarozeni').addEvent('change', function(e){
//		validuj_number();
//	});
//	
//	$('ImportClientMobil').addEvent('change', function(e){
//		validuj_number();
//	});
	
     $(form_id).getElements('.mobile').addEvent('change', function(e){
        e.stop();
        check_number(this.value);
     });
     
     $('ImportClientPrijmeni').addEvent('change', function(e){
        e.stop();
        check_surname(this.value);
     });
     
     
     // zjisteni zda je cislo jiz v db a zobrazeni seznamu u koho
     // by Sol
     function check_number(value){
        var clients_list = '';
        var i = 1;
        
        if (value == ''){
		
		} else if (value.length < 14){
			alert('Mobil je krátký - zadejte 14 znaků');
		} else {
		    new Request.JSON({
			    url:'/clients/check_number/'+value,		
				onComplete:function(json){
			     	if (json.result == true){		     	   
			     	   if(json.data != null)
                           $each(json.data,function(item){
                                clients_list += i+". "+item+"\n";
                                i++;
                           });
					   alert("Toto číslo "+value+" je již použito \nČíslo využívá :\n\n"+clients_list);
					}
				}
			}).send();		      
        }  
     }
     
      // zjisteni zda je prijmeni jiz v db a zobrazeni seznamu
     // by Sol
     function check_surname(value){
        var clients_list = '';
        var i = 1;
        
        if (value != ''){
		    new Request.JSON({
			    url:'/clients/check_surname/'+value,		
				onComplete:function(json){
			     	if (json.result == true){		     	   
			     	   if(json.data != null)
                           $each(json.data,function(item){
                                clients_list += i+". "+item+"\n";
                                i++;
                           });
					   alert("Toto přijmení "+value+" již existuje v systému.\nPřijmení se shoduje s:\n\n"+clients_list);
					}
				}
			}).send();		      
        }  
     }
     
	//nepouziva se - od 26.10.2009
	function validuj_number(){
		if ($('ImportClientMobil').value == ''){
		
		} else if ($('ImportClientMobil').value.length<12){
			alert('Mobil je krátký - zadejte 12 znaků');
		} else {
			<?php if(!isset($this->data['ImportClient']['id'])) { ?>
			var url = '/clients/valid_requiter/'+$('ImportClientMobil').value;
			if ($('calbut_ImportClientDatumNarozeni').value != '')
				url += ('/' + $('calbut_ImportClientDatumNarozeni').value);
			new Request.JSON({
					url:url,		
					onComplete:function(json){
						if (json.result === 1){
							if (confirm('Klient ' + json.client.ImportClient.name + ' již existuje, chcete nahrát jeho údaje ?')){
								
								$('ImportClientJmeno').removeClass('require');
								$('ImportClientPrijmeni').removeClass('require');
								$('ImportClientMobil').setProperty('readonly');
								//$('ImportClientPrijmeni').removeClass('require');
								
								
								$('ImportClientId').value=json.client.ImportClient.id;	
								$('ImportClientJmeno').value=json.client.ImportClient.jmeno;	
								$('ImportClientPrijmeni').value=json.client.ImportClient.prijmeni;	
								$('ImportClientPohlaviList').value=json.client.ImportClient.pohlavi_list;	
								$('calbut_ImportClientDatumNarozeni').value=json.client.ImportClient.datum_narozeni;	
								$('ImportClientEmail').value=json.client.ImportClient.email;	
								$('ImportClientTelefon1').value=json.client.ImportClient.telefon1;	
								$('ImportClientTelefon2').value=json.client.ImportClient.telefon2;	
								$('ImportClientUlice').value=json.client.ImportClient.ulice;	
								$('ImportClientMesto').value=json.client.ImportClient.mesto;	
								$('ImportClientPsc').value=json.client.ImportClient.psc;	
								$('ImportClientStatId').value=json.client.ImportClient.stat_id;	
								$('ImportClientDosazeneVzdelaniList').value=json.client.ImportClient.dosazene_vzdelani_list;	
								//$('ImportClientHodnoceniKvalitaPrace').value=json.client.ImportClient.hodnoceni_kvalita_prace;	
								//$('ImportClientHodnoceniAlkohol').value=json.client.ImportClient.hodnoceni_alkohol;	
								//$('ImportClientHodnoceniSpolehlivost').value=json.client.ImportClient.hodnoceni_spolehlivost;	
								//$('ImportClientHodnoceniBlacklist').value=json.client.ImportClient.hodnoceni_blacklist;	
								
								$('new_connection').value = 1;
							
								if (json.kval){
									$each(json.kval,function(item){
										$('ImportClientKvalifikaceList').value = item.SettingCareerItem.id;
										$('CarrerItemsDataOd').value = item.ImportConnectionClientCareerItem.od;
										$('CarrerItemsDataName').value = item.ImportConnectionClientCareerItem.name; 
										$('CarrerItemsDataPopis').value = item.ImportConnectionClientCareerItem.popis
										$('CarrerItemsDataDo').value = item.ImportConnectionClientCareerItem.do
										add_kvalifikace();
									});
								
								}
							
							
							} else {
								$('ImportClientMobil').value = '';
							}
						} else if (json.result === -1){
							alert('Klienta  ' + json.client.ImportClient.name + ' již máte ve Vaší databázi!');
							$('ImportClientMobil').value = '';
						} else {
							$('ImportClientMobil').removeClass('require');
							$('ImportClientMobil').removeClass('invalid');
							$('ImportClientMobil').addClass('valid');
						} 
					}
				}).send();
			
			<?php } else { ?>
			
				new Request.JSON({
					url:'/clients/valid_requiter_edit/'+$('ImportClientMobil').value,		
					onComplete:function(json){
						if (json.result === 1){
							alert('Toto číslo je již použito');
							$('ImportClientMobil').value = '';
						}
					}
				}).send();
			
			<?php } ?>
		}	
	};
	

	$$('.remove_from_kvalifikace_list').addEvent('click', remove_from_kvalifikace_list.bindWithEvent(this));
	
	/*ADD KVALIFIKACE */
	if($('add_kvalifikace'))
		$('add_kvalifikace').addEvent('click', add_kvalifikace.bindWithEvent(this));
	
	function add_kvalifikace(e){
		if (e)
			new Event(e).stop();
		var select = $('ImportClientKvalifikaceList');
		var rand_id = uniqid();
		
		// osetreni roku datumu
	//	if(($('CarrerItemsDataOd').value!="" && $('CarrerItemsDataOd').value.length!=4)||($('CarrerItemsDataDo').value!="" && $('CarrerItemsDataDo').value.length!=4)) 
    //  alert('Období musí mít 4 znaky');
	//	else {
    		var table = $('kvalifikace_list');
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Kvalifikace').inject(tr);
    			new Element('td').setHTML(select.getOptionText()).inject(tr);
    			var del = new Element('a',{href:'#',rel:rand_id}).inject(new Element('td').inject(tr)).setHTML('Odstranit').addEvent('click', remove_from_kvalifikace_list.bindWithEvent());
    			del.addClass('remove_from_kvalifikace_list');
    			
    			
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Název zaměstnavatele:').inject(tr);
    			new Element('td',{colspan:2}).setHTML($('CarrerItemsDataName').value).inject(tr);
    			
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Období:').inject(tr);
    			new Element('td').setHTML("od "+$('CarrerItemsDataOd').value).inject(tr);
    			new Element('td').setHTML("do "+$('CarrerItemsDataDo').value).inject(tr);
    		
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Popis pracovní náplně:').inject(tr);
    			new Element('td',{colspan:2}).setHTML($('CarrerItemsDataPopis').value).inject(tr);
    		
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td',{colspan:3}).addClass('line').inject(tr);
    		
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataName').value,name:'data[ClientCarrerItems]['+rand_id+'][name]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataPopis').value,name:'data[ClientCarrerItems]['+rand_id+'][popis]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataOd').value,name:'data[ClientCarrerItems]['+rand_id+'][od]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataDo').value,name:'data[ClientCarrerItems]['+rand_id+'][do]'}).inject(tr);
    		new Element('input',{type:'hidden',value:select.value,name:'data[ClientCarrerItems]['+rand_id+'][setting_career_item_id]'}).inject(tr);
    		
    		$('CarrerItemsDataName').value 	 = '';
    		$('CarrerItemsDataPopis').value  = '';
    		$('CarrerItemsDataOd').value	 = '';
    		$('CarrerItemsDataDo').value	 = '';
    		$('ImportClientKvalifikaceList').value = '';
			pocetProfesi++;
   //	  }
	};
	
	function remove_from_kvalifikace_list(e){
		var event=new Event(e);
		event.stop();
		var obj = event.target;
		
		if (confirm('Opravdu si přejete odstranit tuto kvalifikaci?')){
			var id = '.kvalifikace_row_'+obj.getProperty('rel');
			$('kvalifikace_list').getElements(id).dispose();
		
		}
		pocetProfesi--;
	}
	
	
	$$('.remove_from_certifikaty_list').addEvent('click', remove_from_certifikaty_list.bindWithEvent(this));
	
	/*ADD CERTIFIKAT */
	if($('add_certifikat'))
	$('add_certifikat').addEvent('click', function(e){
		new Event(e).stop();
		var select = $('CertifikatyItemsCertifikatyList');
		var rand_id = uniqid();
		
		var table = $('certifikaty_list');
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td').setHTML('Typ').inject(tr);
			new Element('td').setHTML(select.getOptionText()).inject(tr);
			new Element('td').setHTML('').inject(tr);
			var del = new Element('a',{href:'#',rel:rand_id}).inject(new Element('td').inject(tr)).setHTML('Odstranit').addEvent('click', remove_from_certifikaty_list.bindWithEvent());
			del.addClass('remove_from_certifikaty_list');
			
			
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td').setHTML('Komentář:').inject(tr);
			new Element('td').setHTML($('CertifikatyItemsCertifikatyKomentar').value).inject(tr);
			new Element('td').setHTML('Platný do:').inject(tr);
			new Element('td').setHTML($('CertifikatyItemsCertifikatyPlatnost').value).inject(tr);
		
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td',{colspan:4}).addClass('line').inject(tr);
		
		new Element('input',{type:'hidden',value:$('CertifikatyItemsCertifikatyKomentar').value,name:'data[CertifikatyItem]['+rand_id+'][komentar]'}).inject(tr);
		new Element('input',{type:'hidden',value:$('CertifikatyItemsCertifikatyPlatnost').value,name:'data[CertifikatyItem]['+rand_id+'][platnost]'}).inject(tr);
		new Element('input',{type:'hidden',value:$('CertifikatyItemsCertifikatyList').value,name:'data[CertifikatyItem]['+rand_id+'][list]'}).inject(tr);
	
	 
    		$('CertifikatyItemsCertifikatyKomentar').value	 = '';
    		$('CertifikatyItemsCertifikatyPlatnost').value	 = '';
    		$('CertifikatyItemsCertifikatyList').value = '';
    
    });
	
	
	function remove_from_certifikaty_list(e){
		var event=new Event(e);
		event.stop();
		var obj = event.target;
		
		if (confirm('Opravdu si přejete odstranit tento certifikát?')){
			var id = '.kvalifikace_row_'+obj.getProperty('rel');
			$('certifikaty_list').getElements(id).dispose();
		
		}
	}

	if($(form_id).getElement('input[id=save_close]'))
	$(form_id).getElement('input[id=save_close]').addEvent('click',function(e){
	    var this_form_id = $('client_edit_formular');
		new Event(e).stop();
		valid_result = validation.valideForm(this_form_id);
		
		if(pocetProfesi==0) 
			if(valid_result == true)
				valid_result = Array("Musite zvolit profesi");
			else
				valid_result[valid_result.length]="Musite zvolit profesi";


		if ($('new_connection').value == 0 || confirm('Vámy zadaný klient již existuje v databázi, chcete jej přiřadit pod Vás?')){
		   	var request = new Request.JSON({ 
				url:$(this_form_id).action,		 
				onComplete:function(json){
						click_refresh($('ImportClientId').value); 
						domwin.closeWindow('domwin'); 
				}
			});
			
			if (valid_result == true){
			    button_preloader($('save_close'));
				request.post($(this_form_id));
                button_preloader_disable($('save_close'));
			} else {
				var error_message = new MyAlert();
				error_message.show(valid_result)
			}
		}
	});
	
	$$('.integer, .float').inputLimit();	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define(form_id,{
		'ImportClientJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		},
		'ImportClientMobil1': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit mobil'},
			'length': {'condition':{'min':14,'max':14}, 'err_message':'Mobil musí mít 14 znaků'},		
		},
		'ImportClientPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'},
		}
	});
	validation.generate(form_id,<?php echo (isset($this->data['ImportClient']['id'])) ?
'true' : 'false'; ?>);
	
	<?php if(isset($only_show)): ?>
	$(form_id).getElements('input, select, textarea')
		.addClass('read_info')
		.setProperty('readonly','readonly');
	<?php endif; ?>
    
    if($('client_trash')){		
        $('client_trash').addEvent('click',function(e){ 
           new Event(e).stop(); 
           if(confirm('Opravdu chcete smazat tohoto klienta?')){
                new Request.JSON({
        			    url:this.href,	
        				onComplete:(function(){
        			         $('duplicity_client_edit_view').empty();
                             $('duplicity_client_ul').getElement('li[rel='+this.rel+']').dispose();
                             slider_cl_dup.toggle();
        				}).bind(this)
                }).send();
           } 
        });
    }
</script>