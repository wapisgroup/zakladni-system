<div class="win_fce top">
<a href='/report_down_payments/exception_add/' class='button edit' id='company_add' title='Přidání'>Přidat firmu</a>
</div>
<table class='table' id='recruiter_table'>
	<tr>
		<th>Firma</th>
		<th>Přidal</th>
		<th>Přiřazen</th>
		<th></th>
	</tr>
	<?php if (isset($company_exception_list) && count($company_exception_list) > 0):?>
		<?php foreach($company_exception_list as $item):?>
		<tr>
			<td><?php echo $item['Company']['name'];?></td>
			<td><?php echo $item['CmsUser']['name'];?></td>
			<td><?php echo $fastest->czechDate($item['CompanyExceptionPayment']['created']);?></td>
			<td><a href="#" rel="<?php echo $item['CompanyExceptionPayment']['id'];?>" class="ta trash">Odebrat</a></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<script>	
	$('recruiter_table').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat toto spojení?')){
			new Request.HTML({
				url:'/report_down_payments/exception_trash/'+ this.rel,
				onComplete: (function(){
					this.getParent('tr').dispose();
				}).bind(this)
			}).send();
		}
	});
	
	
	$('company_add').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_company_add',
			sizes		: [500,250],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>