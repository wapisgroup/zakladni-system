<form action='' method='post' id='kb_export'>
		
	<div class="domtabs admin2_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Vytvoření exportu</a></li>
			<li class="ousko"><a href="#krok1">Historie exportu</a></li>
		</ul>
</div>
	<div class="domtabs admin2_dom">
		<div class="domtabs field">
			
            <div class="sll">
                <?php echo $htmlExt->inputDate('date_to_pay',array('label'=>'Datum uhrazení'));?><br />
                <?php echo $htmlExt->checkbox('transfer_money',null,array('label'=>'Převod měny'));?>
            </div>
            <div class="slr">
                <?php echo $htmlExt->selectTag('parent_company',$parent_company_list,null,array('label'=>'Nadřazená společnost'),null,false);?>
                <div id="sk_type" class="none">
                    <?php echo $htmlExt->selectTag('type_export',array('kb'=>'Komerční banka','ss'=>'Slovenská spořitelna','tatra'=>'Tatra Banka'),null,array('label'=>'Typ exportu'),null,false);?>
                </div>
            </div>
            <br />
            <div id="transfer_money_box" class="none">
                <strong style='color:red; text-align:center; display:block;'>
            		Upozornění: Tato akce změní aktuální hodnoty částek a přepočte podle Vámi nastaveného kurzu. Změna bude viditelná pouze v exportovaném souboru do banky!!! 
            	</strong>
                <div class="sll">
                <?php echo $htmlExt->selectTag('tm_type',array(1=>'EUR -> CZK',2=>'CZK -> EUR'),null,array('label'=>'Typ'),null,false);?>
                </div>
                <div class="slr">
                <?php echo $htmlExt->input('tm_rate',array('label'=>'Kurz','class'=>'integer'));?>
                </div>
                <br />
           </div>
           <div id="kb_export_list">
                <?php echo $this->renderElement('../report_down_payments/kb_items'); ?>
           </div>

		</div>
        <div class="domtabs field">
            <?php echo $this->renderElement('../report_down_payments/kb_history'); ?>
        </div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Vytvořit export',array('id'=>'save'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin2_dom'});
    var cz = [1,2,5,7]; 
    
    $('ParentCompany').addEvent('change',function(e){
        if(!cz.contains(this.value.toInt())){
            $('sk_type').removeClass('none');

        }
        else {
            $('sk_type').addClass('none');
             }
        
		new Event(e).stop();
		   new Request.HTML({
			    url:'/report_down_payments/load_parent_company_employees/'+this.value,
                update: 'kb_export_list',		
				onComplete:function(){
				}
			}).send();
	});
      
    $('kb_export').getElements('.integer').inputLimit();
    
    if($('TransferMoney'))
    $('TransferMoney').addEvent('change',function(e){
        if(this.checked){
            $('transfer_money_box').removeClass('none');
        }
        else{
            $('transfer_money_box').addClass('none');
            $('transfer_money_box').getElements('input, select').reset();
        }
    })    
    

	$('save').addEvent('click',function(e){
	   e.stop();
       valid_result = validation.valideForm('kb_export');
       
       if($('TransferMoney') && $('TransferMoney').checked && ($('TmRate').value == '' || $('TmRate').value == 0))
        alert('Kurz nemůže být prázdný ani nula!');
       else{ 
           if (valid_result == true){
               //odeslani zaskrtnutych id
               new Request.JSON({
        			url:'/report_down_payments/kb/',
        			onComplete:function(json){
        				if(json){
        				    if(json.result == true){
        				        alert('Export byl úspěšně vytvořen, nyní Vám bude nabídnut ke stažení.');
                                window.location = '/report_down_payments/download/'+json.type+'/'+json.file;
                                domwin.loadContent('domwin');
        				    }
                            else{
                                alert(json.message);
                            }   
        				}                
                        else
                            alert('Chyba aplikace.');
        			}
        	   }).post($('kb_export'));
            } else {
        		var error_message = new MyAlert();
        		error_message.show(valid_result)
      		}
        }
	});

    validation.define('kb_export',{
		'calbut_DateToPay': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum uhrazení'},
		}
	});
	validation.generate('kb_export',false);


	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>