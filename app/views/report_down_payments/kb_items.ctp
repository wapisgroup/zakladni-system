 <fieldset>
				<legend>Zaměstnanci</legend>
<div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th><input id="check_all2" type="checkbox"/></th>
						<th>Přidal</th>
						<th>Firma</th>
						<th>Zaměstnanec</th>
						<th>Č.účtu</th>
						<th>Částka</th>
						<th>Vytvořeno</th>
					</tr>
					<?php 
					if (isset($zamestnanci_list) && count($zamestnanci_list) > 0){
						foreach($zamestnanci_list as $zam):
						?>
						<tr>
							<td><?php echo $htmlExt->checkbox('id/'.$zam['ReportDownPayment']['id']);?></td>
							<td><?php echo $zam['CmsUser']['name'];?></td>
							<td><?php echo $zam['Company']['name'];?></td>
                            <td><?php echo $zam['Client']['name'];?></td>
							<td><?php echo (isset($zam['ParentClient']) && $zam['ParentClient']['id'] != ''?$zam['ParentClient']['cislo_uctu']:$zam['Client']['cislo_uctu']);?></td>
							<td><?php echo $zam['ReportDownPayment']['amount'];?></td>
							<td><?php echo $fastest->czechDateTime($zam['ReportDownPayment']['created']);?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádní zaměstnanci</td></tr>"; 
					?>
					</table></div> 
				<br />
			</fieldset>
<script>
// START check all checkbox in row
	$('check_all2').addEvent('click',function(e){
       
		if(this.checked){
				$('rating_table').getElements('input[type=checkbox]').each(function(item){
					if(item.disabled==false)
						item.setProperty('checked','checked');
				});		
		}
		else
			$('rating_table').getElements('input[type=checkbox]').removeProperty('checked');
	});
	// END check all checkbox in row
</script>            