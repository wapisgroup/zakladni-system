<form action='/report_down_payments/paid_all/' method='post' id='report_down_payments_uhrazeno'>
	<?php echo $htmlExt->hidden('ReportDownPayment/ids');?>
	<?php echo $htmlExt->hidden('ReportDownPayment/save',array('value'=>1));?>

<p class="text_center">Datum plati pro všechny zaškrtnuté klienty!!!</p>
		<fieldset>
				<legend>Základní</legend>
				<div class="sll100">  
                    <?php echo $htmlExt->inputDate('ReportDownPayment/date_of_paid',array('tabindex'=>3,'label'=>'Uhrazeno','value'=>date('Y-m-d')));?> <br class="clear">
				</div>
		</fieldset>


	
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>6));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>7));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">



    if($('save_close'))
    	$('save_close').addEvent('click',function(e){
    		new Event(e).stop();
    		
    		valid_result = validation.valideForm('report_down_payments_uhrazeno');
    		if (valid_result == true){
    			new Request.JSON({
    				url:$('report_down_payments_uhrazeno').action,		
    				onComplete:function(){
    					click_refresh();
    					domwin.closeWindow('domwin_uhrazeno');
    
    				}
    			}).post($('report_down_payments_uhrazeno'));
    		} else {
    			var error_message = new MyAlert();
    			error_message.show(valid_result)
    		}
    	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_uhrazeno');});
</script>