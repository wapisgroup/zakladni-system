<form id='add_edit_company_formular' action='/client_employees/opp_transfer/' method='post'>

    <table class="table" id="opp_item_table">
        <tr>
            <th>#</th>
            <th>Druh</th>
            <th>Typ</th>
            <th>Velikost</th>
            <th>Cena</th>
            <th>Datum přidělení</th>
            <th>Nárok</th>
        </tr>
        <?php 
        if(isset($items))
            foreach($items as $item){
                echo '<tr>';
                echo '<td>'.$htmlExt->checkbox('OppTransfer/'.$item['ConnectionClientOpp']['id'],null,array()).'</td>';
                echo '<td>'.$item['ConnectionClientOpp']['typ'].'</td>';
                echo '<td>'.$item['ConnectionClientOpp']['name'].'</td>';
                echo '<td>'.$item['ConnectionClientOpp']['size'].'</td>';
                echo '<td>'.$item['ConnectionClientOpp']['price'].'</td>';
                echo '<td>'.$fastest->czechDate($item['ConnectionClientOpp']['created']).'</td>';
                echo '<td>'.$item['ConnectionClientOpp']['narok'].'</td>';
                echo '</tr>';
            }
        
        ?>
    </table>
    <br />
    <h2>Přenést OPP na klienta:</h2>
    <?php 
    
    echo $htmlExt->selectTag('company_id',$company_list,null,array('Podnik')).'<br />';
    echo $htmlExt->selectTag('connection_client_id',array('--- Vyberte podnik ---'),null,array('Podnik'),null,false).'<br />';
    
    ?>
    
    <div class='formular_action'>
            <input type='button' value='Přenést OPP' id='TransferOppSave'/>
    		<input type='button' value='Zavřít' id='ClientHistoryOppClose'/>
    </div>
</form>
<script>
if($('ClientHistoryOppClose'))
$('ClientHistoryOppClose').addEvent('click',function(){domwin.closeWindow('domwin')});

var client_id = '<?= $client_id;?>';
$('CompanyId').ajaxLoad('/client_employees/load_employees_in_company/',['ConnectionClientId'],'/'+client_id);

	$('TransferOppSave').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_company_formular');
		if (valid_result == true){
			button_preloader($('TransferOppSave'));
			new Request.JSON({
				url:$('add_edit_company_formular').action,		
				onComplete:function(){
					button_preloader_disable($('TransferOppSave'));
					click_refresh();
					domwin.closeWindow('domwin');
				}
			}).post($('add_edit_company_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_company_formular',{
		'ConnectionClientId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit klienta na kterého chcete opp přenést.'}
		}
	});
	validation.generate('add_edit_company_formular',false);
	

</script>
