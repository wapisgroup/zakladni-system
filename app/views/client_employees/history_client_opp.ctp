<table class="table" id="opp_item_table">
    <tr>
        <th>Druh</th>
        <th>Typ</th>
        <th>Velikost</th>
        <th>Cena</th>
        <th>Datum přidělení</th>
        <th>Nárok</th>
        <?if(isset($return)){echo '<th>Možnosti</th>';}?>
    </tr>
    <?php 
    if(isset($items))
        foreach($items as $item){
            echo '<tr>';
            echo '<td>'.$item['ConnectionClientOpp']['typ'].'</td>';
            echo '<td>'.$item['ConnectionClientOpp']['name'].'</td>';
            echo '<td>'.$item['ConnectionClientOpp']['size'].'</td>';
            echo '<td>'.$item['ConnectionClientOpp']['price'].'</td>';
            echo '<td>'.$fastest->czechDate($item['ConnectionClientOpp']['created']).'</td>';
            echo '<td>'.$item['ConnectionClientOpp']['narok'].'</td>';
            
            if(isset($return)){
                echo '<td>';
                if($logged_user['CmsGroup']['cms_group_superior_id'] == 1){
                    echo '<a href="/client_employees/client_opp_remove/'.$item['ConnectionClientOpp']['id'].'/" class="ta trash delete_opp_connection" title="Odebrat OPP">trash</a>';
                }
                echo '</td>';
            }
                
            echo '</tr>';
        }
    
    ?>
</table>
<?php if(!isset($close)){?> 
<div class='formular_action'>
		<input type='button' value='Zavřít' id='ClientHistoryOppClose'/>
</div>
<?}?>   
<script>
if($('ClientHistoryOppClose'))
$('ClientHistoryOppClose').addEvent('click',function(){domwin.closeWindow('domwin_history_client_opp')});


if($$('.delete_opp_connection')){
    $$('.delete_opp_connection').addEvent('click',function(e){
        e.stop();
        if(confirm("Opravdu chcete odebrat uživateli OPP?!!")){
            new Request.JSON({
			    url:this.href,		
				onComplete:(function(json){
				    if(json){
    			     	if (json.result == true){
    			     	   this.getParent('tr').dispose();
    			     	}
                        
                        alert(json.message);
                    }
				}).bind(this)
			}).send();	
        }
    })    
}
</script>
