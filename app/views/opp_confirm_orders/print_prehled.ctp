<div id="box_print">
    <div class="sll">
    <?php
    	echo $htmlExt->var_text(null, array('label'=>'Autor','class'=>'read_info','value'=>$opp_order_items[0]['CmsUser']['name'])).'<br />';	   	  
    	echo $htmlExt->var_text(null, array('label'=>'Pro firmu','class'=>'read_info','value'=>$opp_order_items[0]['Company']['name'])).'<br />';	   	   
    ?>
    </div> 
    <div class="slr">
    
    <?php
    	echo $htmlExt->var_text(null, array('label'=>'Datum objednávky','class'=>'read_info','value'=>$fastest->czechDate($opp_order_items[0]['OppOrder']['created']))).'<br />';	   	  
    	//echo $htmlExt->var_text(null, array('label'=>'Pro firmu','class'=>'read_info','value'=>$opp_order_items[0]['Company']['name'])).'<br />';	   	   
    ?>
    </div>
</div>
<br /> 
<table class='table'>
	<thead>
    	<tr>
    		<th>Druh</th>
    		<th>Typ</th>
    		<th>Velikost</th>
    		<th>Pocet</th>
            <th>Obj. Počet</th>
    		<th>Cena CZ</th>
            <th>Cena EU</th>
    		<th>Potvrzeni</th>
    	</tr>
    </thead>
	<tbody id='sub_table'>
		    <?php if (isset($opp_order_items) && count($opp_order_items)>0):?>
			<?php foreach($opp_order_items as $item):?>
			<tr>
	    		<td><?php echo $item['OppOrderItem']['type_id'];?> </td>
                <td><?php echo $item['OppOrderItem']['name'];?> </td>
                <td><?php echo $item['OppOrderItem']['size'];?> </td>
                <td><?php echo $item['OppOrderItem']['count'];?> </td>
                <td><?php echo $item['OppOrderItem']['count_order'];?> </td>
                <td><?php echo $fastest->price($item['OppOrderItem']['price_cz']);?></td>
                <td><?php echo $fastest->price($item['OppOrderItem']['price_eu'],' EUR');?></td>
	    		<td><?php echo $fastest->value_to_yes_no($item['OppOrderItem']['stav']);?></td>
	    	</tr>
			<?php endforeach;?>
			<?php else :?>
			<?php endif;?>
	</tbody>
</table>
        <br />
        <p>
            Prevzal  ...............................
        </p>
<script>
window.print();
</script>