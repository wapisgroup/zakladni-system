<div id="box_print">
    <div class="sll">
    <?php
    	echo $htmlExt->var_text(null, array('label'=>'Autor','class'=>'read_info','value'=>$mv_order_items[0]['CmsUser']['name'])).'<br />';
    ?>
    </div> 
    <div class="slr">
    
    <?php
    	echo $htmlExt->var_text(null, array('label'=>'Datum objednávky','class'=>'read_info','value'=>$fastest->czechDate($mv_order_items[0]['MvOrder']['created']))).'<br />';	   	  
    	//echo $htmlExt->var_text(null, array('label'=>'Pro firmu','class'=>'read_info','value'=>$mv_order_items[0]['Company']['name'])).'<br />';	   	   
    ?>
    </div>
</div>
<br /> 
<table class='table'>
	<thead>
    	<tr>
    		<th>Druh</th>
    		<th>Typ</th>
    		<th>Pocet</th>
            <th>Obj. Počet</th>
    		<th>Cena CZ</th>
            <th>Cena EU</th>
    		<th>Potvrzeni</th>
    	</tr>
    </thead>
	<tbody id='sub_table'>
		    <?php if (isset($mv_order_items) && count($mv_order_items)>0):?>
			<?php foreach($mv_order_items as $item):?>
			<tr>
	    		<td><?php echo $item['MvOrderItem']['type_id'];?> </td>
                <td><?php echo $item['MvOrderItem']['name'];?> </td>
                <td><?php echo $item['MvOrderItem']['count'];?> </td>
                <td><?php echo $item['MvOrderItem']['count_order'];?> </td>
                <td><?php echo $fastest->price($item['MvOrderItem']['price_cz']);?></td>
                <td><?php echo $fastest->price($item['MvOrderItem']['price_eu'],' EUR');?></td>
	    		<td><?php echo $fastest->value_to_yes_no($item['MvOrderItem']['stav']);?></td>
	    	</tr>
			<?php endforeach;?>
			<?php else :?>
			<?php endif;?>
	</tbody>
</table>
<script>
window.print();
</script>