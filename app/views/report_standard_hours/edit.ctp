<form action='/report_standard_hours/edit/' method='post' id='report_down_payments_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/id');?>

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní</legend>
			
					<?php echo $htmlExt->input('CompanyWorkPosition/standard_hours',array('tabindex'=>1,'label'=>'Normo hodina','class'=>'float'));?> <br class="clear">				
			
			</fieldset>
		</div>
	</div>
	
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>6));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>7));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	// input limit
	$('report_down_payments_edit_formular').getElements('.float').inputLimit();
	
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
			new Request.JSON({
				url:$('report_down_payments_edit_formular').action,		
				onComplete:function(){
					click_refresh($('CompanyWorkPositionId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('report_down_payments_edit_formular'));

	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	</script>