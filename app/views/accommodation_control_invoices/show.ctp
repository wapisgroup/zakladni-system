<form action='' method='post' id='setting_activity_edit_formular'>
		
	<div class="domtabs admin2_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Seznam Zaměstnanců</a></li>
		</ul>
</div>
	<div class="domtabs admin2_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Zaměstnanci</legend>
                <div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th>Zaměstnanec</th>
						<th>Počet dnů</th>
						<th>Možnosti</th>
					</tr>
					<?php 
					if (isset($zamestnanci_list) && count($zamestnanci_list) > 0){
						foreach($zamestnanci_list as $zam):
						?>
						<tr>
							<td class='td_client'><?php echo $zam['Client']['name'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['accommodation_days_count'];?></td>
							
							<td>
							<a href="/accommodation_control_invoices/detail/<?php echo $zam['ClientWorkingHour']['id']; ?>" class="detail" title="Detail položky">Detail</a>
							
							</td>	
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádní zaměstnanci</td></tr>"; 
					?>
					</table></div> 
				<br />
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin2_dom'}); 

	$$('.detail').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_detail',
			sizes		: [900,150],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
            remove_scroll : false
		}); 
	});
	
  
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');});
</script>