<form action='/allocation_working_items/edit' method='post' id='client_add_activity_form'>
            <?php echo $htmlExt->hidden('connection_client_requirement_id');?>
			<fieldset>
				<legend>Přidělení pracovních pomůcek</legend>
					<?php echo $htmlExt->selectTag('client_id', $client_list, null, array('class'=>'long','label_class'=>'long','tabindex' => 1, 'label' => 'Klient'),array('title'=>(array('0') + $client_list_title))); ?><br />
                    <?php //echo $htmlExt->selectTag('pomucky', $activity_pomucky_list, null, array('class'=>'long','label_class'=>'long','tabindex' => 1, 'label' => 'Typ'), null, false); ?>
           <?php 
                    foreach($activity_pomucky_list as $id=>$name){
                        echo $htmlExt->checkbox('_pomucky/'.$id, null, array('label_class'=>'long','label' => $name)).'<br />'; 
                    }
           ?>
           
            </fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>
    /**
     * při vybrani klienta se nacte jeho connection_id pro historii
     */
    $('ClientId').addEvent('change',function(e){
        new Event(e).stop(); 
        
        if(this.value != '')
            $('ConnectionClientRequirementId').value = this.options[this.selectedIndex].title; 
    });

	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_add_activity_form');
		
		if (valid_result == true){
			new Request.JSON({
				url:$('client_add_activity_form').action,		
				onComplete:function(json){
				    if(json.result == true){
				       click_refresh();
					   domwin.closeWindow('domwin');
                    }
                    else
                       alert(json.message);
				}
			}).post($('client_add_activity_form'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});

	validation.define('client_add_activity_form',{
		'ClientId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit klienta'},
		}
	});

	validation.generate('client_add_activity_form',false);
    
</script>