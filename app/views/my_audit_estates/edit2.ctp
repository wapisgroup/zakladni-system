<form action='/my_audit_estates/manage_estate/' method='post' id='formular'>
<?php echo  $htmlExt->hidden('audit_estate_id');?>
<?php echo  $htmlExt->hidden('old_connection_id');?>
<?php echo  $htmlExt->hidden('renting_connection');?>
<?php echo  $htmlExt->hidden('from_old',array('value'=>$old['ConnectionAuditEstate']['created']));?>
       
            <fieldset>
		      <legend>Leasingový zaměstnanec</legend>
			     <?php echo $htmlExt->inputDate('do',array('tabindex'=>1,'label'=>'Vrací majetek ke dni','class'=>'','label_class'=>'long'));?> <br class="clear">		
			  </fieldset>
            
            <fieldset>
    		    <legend>Přebírám</legend>
                <?php echo $htmlExt->inputDate('od',array('tabindex'=>1,'label'=>'Majetek od','class'=>'','label_class'=>'long'));?> <br class="clear">		
    		</fieldset>

	<div class='formular_action'>
		<input type='button' value='Předat majetek' id='PredatMajetekSaveAndClose' />
		<input type='button' value='Zavřít' id='PredatMajetekClose'/>
	</div>
</form>
<script>

    function get_date(date, separator){
        date = date.split(separator);           
        return date[2] +'.' + date[1] + '.' + date[0];
    }

    from_old = get_date($('FromOld').value, '-');

	$('PredatMajetekClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	$('PredatMajetekSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
        valid_result = validation.valideForm('formular');


        if( $('calbut_Do').value == ''){ 
			if(valid_result == true)
				valid_result = Array("Musite zvolit datum vracení");
			else
				valid_result[valid_result.length]="Musite datum vracení";   
        }         
        else{
            if($('calbut_Do').value <= $('FromOld').value)
                if(valid_result == true)
				    valid_result = Array("Chyba datum vracení je menší než datum přebrání majetku.\nToto datum musí být min. o jeden den větší než přebrání majetku.\n\n Přebrání majetku bylo : "+from_old);
                else
				    valid_result[valid_result.length]="Chyba datum vracení je menší než staré datum přebrání majetku.\nToto datum musí být min. o jeden den větší než přebrání majetku.\n\n Přebrání majetku bylo : "+from_old;   
        } 
               
        if( $('calbut_Od').value == '') 
			if(valid_result == true)
				valid_result = Array("Musite zvolit datum nového přidělení");
			else
				valid_result[valid_result.length]="Musite zvolit datum nového přidělení";        
        else{
            if($('calbut_Od').value <= $('calbut_Do').value)
                if(valid_result == true)
				    valid_result = Array("Chyba datum nového přidělení je menší než datum vrácení.\nToto datum musí být min. o jeden den větší než datumu vrácení.");
                else
				    valid_result[valid_result.length]="Chyba datum nového přidělení je menší než datum vrácení.\nToto datum musí být min. o jeden den větší než datumu vrácení.";   
        }        


      
        if (valid_result == true){
            button_preloader($('PredatMajetekSaveAndClose'));
			new Request.JSON({
				url:$('formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
                            if(json.message)
                                alert(json.message);
                            click_refresh($('AuditEstateId').value);    
                            domwin.closeWindow('domwin');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
                    
                    button_preloader_disable($('PredatMajetekSaveAndClose'));
				}
			}).post($('formular'));
       } 
       else {
		  var error_message = new MyAlert();
		  error_message.show(valid_result)
	   }      
	});
    
    validation.define('formular',{	
        'CompanyWorkPositionId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit profesi'},
		}
	});
	validation.generate('formular',false);
</script>	