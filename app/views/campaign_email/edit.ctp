
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Text</a></li>
			<li class="ousko"><a href="#krok1">Přílohy</a></li>
			<li class="ousko"><a href="#krok2">Odběratelé</a></li>
			<li class="ousko"><a href="#krok3">Logace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
            <div class="sll">
           	    <?php echo $htmlExt->input('CampaignEmail/email_from',array('readonly'=>(isset($this->data['CampaignEmail']['id']))?'readonly':false,'label'=>'Email-od'));?><br/>
            </div>
            <div class="slr">
                <?php echo $htmlExt->input('CampaignEmail/email_from_name',array('readonly'=>(isset($this->data['CampaignEmail']['id']))?'readonly':false,'label'=>'Email-od název'));?><br/>
            </div>
		 	<?php echo $htmlExt->input('CampaignEmail/name',array('readonly'=>(isset($this->data['CampaignEmail']['id']))?'readonly':false,'label'=>'Předmět emailu','class'=>'long','label_class'=>'long'));?><br/>
		 	<?php //echo $htmlExt->textarea('CampaignEmail/text',array('readonly'=>(isset($this->data['CampaignEmail']['id']))?'readonly':false,'label'=>'Text emailu','class'=>'long','label_class'=>'long','style'=>'height:300px'));?>
		  	<label class="long">Text:</label>
              <?php echo $wysiwyg->render('CampaignEmail/text',null,$logged_user['domena_upload']);?>
              <br class="clear" />
			
        </div>
        <script language="javascript" type="text/javascript">
            function file_upload(file_name){
                var li = new Element('li',{html:'Soubor: ' + file_name}).inject($('attach_list'));
                var id = uniqid();
                new Element('input',{type:'hidden', name:'data[CampaignEmail][attach]['+id+']',class:'prilohy', value:file_name}).inject(li);
                new Element('a',{href:'#',html:" ->   Smazat"}).inject(li).addEvent('click',function(e){e.stop(); this.getParent('li').dispose();})
                //uploader_CampaignEmailAttaches.delete_file();
            }
        </script>
        <div class="domtabs field">
            <fieldset>
                <legend>Přílohy</legend>
                <div class='sll'>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/campaign_email/upload_attach/',
								'path_to_file'	=> 'uploaded/campaign_email/',
								'status_path' 	=> '/get_status.php',
							),
							'upload_type'	=> 'php',
							'label'			=> 'Soubor',
                            'onComplete'    => 'file_upload'
						);
					?>
					<?php echo $fileInput->render('CampaignEmail/attaches',$upload_setting);?> <br class="clear">
				</div>
                <div class="slr">
                    <fieldset>
                    <legend>Seznam příloh</legend>
                    <ul id="attach_list">
                    <?php if (isset($this->data['CampaignEmail']['attach']) && is_array($this->data['CampaignEmail']['attach']) && count($this->data['CampaignEmail']['attach'])>0):?>
                        <?php foreach($this->data['CampaignEmail']['attach'] as $file):?>
                        <li><?php echo $file;?></li>
                        <?php endforeach;?>
                    <?php endif;?>
                    </ul>
                    </fieldset>
                </div>
            </fieldset>
        </div>
		<div class="domtabs field">
		<?php if (!isset($this->data['CampaignEmail']['id'])):?>
			<form action='/campaign_email/load_client/' method='post' id='load_client_form'  onsubmit="return false;">
				<div style='border:1px dotted #EEE; margin-bottom:5px '>
					<div style='float:left; width:27%;'>
						<?php echo $htmlExt->selectTag('LoadClient/pohlavi',$pohlavi_list,null,array('label'=>'Pohlaví'))?><br/>
						<?php echo $htmlExt->selectTag('LoadClient/profese',$setting_career_item_list,null,array('label'=>'Profese'))?><br/>
						<label>Jen uchazeči</label> <?php echo $htmlExt->checkbox('LoadClient/uchazeci');?>
					</div>
					<div style='float:left; width:27%;'>
						<?php echo $htmlExt->selectTag('LoadClient/certifikat',$setting_certificate_list,null,array('label'=>'Certifikát'))?><br/>
						<?php echo $htmlExt->selectTag('LoadClient/vzdelani',$setting_education_list,null,array('label'=>'Vzdělání'))?><br/>
						<label>Interní nábor</label> <?php echo $htmlExt->checkbox('LoadClient/interni');?>
					</div>
					<div style='float:left;width:29%;'>
						<?php echo $htmlExt->input('LoadClient/mesto',array('label'=>'Město'))?><br/>
						<?php echo $htmlExt->selectTag('LoadClient/stat',$setting_stat_list,null,array('label'=>'Stát'))?><br/>
					</div>
					<div style='float:left;width:15%;'>
						<input type='button' value='Filtrovat'	onclick='return false;'  id='filtr_client_go' 		style='margin-left:30px; margin-top:3px; width:90px'/>
						<input type='button' value='Zrušit' 	onclick='return false;'  id='filtr_client_cancel'	style='margin-left:30px; margin-top:3px; width:90px'/>
					</div>
					<br/>
				</div>
			</form>
			<?php endif;?>
			<?php if (!isset($this->data['CampaignEmail']['id'])):?>
				<div style='border:1px dotted #EEE; width:426px' id='choose_parent_div'>
					<?php echo $this->renderElement('../sms/choose_list');?>
				</div>
			<?php endif;?>
			<?php if (!isset($this->data['CampaignEmail']['id'])):?>
			<div style='float:left; width:42px; text-align:center; padding-top:12%; padding-left:8px'>
				<a href='#' id='move_right_selected' class='ta right_select' 	title='Vybrat vybrané položky'></a> <br />
				<a href='#' id='move_right_all' 	 class='ta right_all'		title='Vybrat všechny položky'></a> <br /><br /><br />
				<a href='#' id='move_left_selected'  class='ta left_select'		title='Zrušit vybrané položky'></a><br /> 
				<a href='#' id='move_left_all' 		 class='ta left_all'		title='Zrušit všechny položky'></a><br />
			</div>
			<?php endif;?>
			<div id='send_parent_div' style='width:<?php echo (isset($this->data['CampaignEmail']['id']))?'876px':'390px';?>; float:right; border:1px dotted #EEE; height:300px;'>
				<ul id='send_list'>
					<?php if (isset($sended_messages)):?>
						<?php foreach($sended_messages as $msg):?>
						<li>
							<span class='col0'><?php echo $msg['CampaignEmailMessage']['client_name'];?></span>
							<span class='col1'><?php echo $msg['CampaignEmailMessage']['email'];?></span>
							<span class='col2'><span class='title'><span class='ta sms_success'></span></span></span>
						</li>
						<?php endforeach;?>
					<?php endif;?>
				</ul>
				 	
			</div>
			<br />
		</div>
		<div class="domtabs field">
		 	<?php echo $htmlExt->textarea('CampaignEmail/logs',array('label'=>'Logace','class'=>'long','label_class'=>'long','style'=>'height:300px','readonly'=>'readonly'));?><br/>
		</div>
	</div>
	<div class='formular_action' id='form_buttons'>
		<?php if (!isset($this->data['CampaignEmail']['id'])):?>
		<input type='button' value='Odeslat' id='send_sms_now' />
		<?php endif;?>
		<input type='button' value='Zavřít' id='close_sms_sender'/>
	</div>
	<div id='progress' class='none' style='width:200px; height:16px; padding:1px; position:absolute; top:45px; right:32px; border:1px solid #EFEFEF; text-align:center'>
		<div class='bar' style='background:green url(/css/fastest/action/progress_bar.png); width:0%; height:16px;'></div>
		<div class='label' style='position:absolute; top:1px; left:1px; width:200px; height:16px;'></div>
	</div>
<script type="text/javascript">
<!--
	var domtab = new DomTabs({'className':'admin_dom'});
    $$('.wysiwyg').makeWswg();

	function select_unselect(e){
		var event = new Event(e),obj = event.target, ul = obj.getParent('ul');
		event.stop();

		if (obj.get('tag') != 'li') 
			obj = obj.getParent('li');
		
		if (!event.shift){
			if (!event.control){
				ul.getElements('.choose_li').removeClass('selected');
			}	
			if (obj.hasClass('selected'))
				obj.removeClass('selected');
			else
				obj.addClass('selected');
		} else {
			if (sel_index < obj.getProperty('rel').toInt()){
				for (i=sel_index; i <= obj.getProperty('rel'); i++){	
					ul.getElements('.choose_li')[i].addClass('selected');
				}
			} else {
				for (i=sel_index; i >= obj.getProperty('rel'); i--){	
					ul.getElements('.choose_li')[i].removeClass('selected');
				}
			}
		}
		sel_index = obj.getProperty('rel').toInt();
	}
	
	// select item
	if ($('choose_list'))
		$('choose_list').getElements('.choose_li').addEvent('click',select_unselect.bindWithEvent(this)); 

	// move all left
	if($('move_left_all'))
		$('move_left_all').addEvent('click',function(e){
			new Event(e).stop();
			$('send_list').getElements('.choose_li').addClass('selected');
			move_left();
		});
	
	// move selected right
	if($('move_left_selected'))
		$('move_left_selected').addEvent('click',function(e){
			new Event(e).stop();
			move_left();
		});

	function move_left(){
		$('send_list').getElements('.selected').each(function(item){
			$('choose_list').adopt(item);
            item.getElement('.sms_stav').addClass('none');
		});
	}

	
	

	// nalezeni dle filtrace
	if($('filtr_client_go'))
		$('filtr_client_go').addEvent('click',function(e){
			new Event(e).stop();
			$('choose_list').fade(0);
			$('choose_parent_div').addClass('preloader');
			new Request.HTML({
				url: '/campaign_email/load_client/',
				update: 'choose_parent_div',
				onComplete: function(){
					$('choose_list').getElements('.choose_li').each(function(item){
						if($('send_list').getElement('.phone_number[value=' + item.getElement('.phone_number').value + ']')){
							item.dispose();
						}
					});
					reindex('choose_list');
					$('choose_parent_div').removeClass('preloader');
					$('choose_list').fade(1);
				}
			}).send($('load_client_form'));
		})
	
	
	function send_sms_item(group_id){
		var item = $('send_list').getElement('.wait_for_delivery');
		if (item){
			var icon = item.getElement('.title').getElement('.ta');
			icon.removeClass('sms_wait');
			icon.addClass('sms_progress');
            
            var data_send = $H({
            	'data[CampaignEmail][email]'		   : 	item.getElement('.phone_number').value,
				'data[CampaignEmail][email_from]'	   : 	$('CampaignEmailEmailFrom').value,
				'data[CampaignEmail][email_from_name]' : 	$('CampaignEmailEmailFromName').value,
				'data[CampaignEmail][client_id]'	   : 	item.getElement('.client_id').value,
				'data[CampaignEmail][client_name]'     :	item.getElement('.client_name').value,
				'data[CampaignEmail][group_id]'	       :	group_id,
				'data[CampaignEmail][text]'		       :	$('CampaignEmailText').value,
				'data[CampaignEmail][name]'		       :	$('CampaignEmailName').value
            })
            
            $$('.prilohy').each(function(hidden){
                data_send[hidden.getProperty('name')]  = hidden.value;
            });
            
			var request = new Request.JSON({
				url: '/campaign_email/send_item/',
				data: data_send,
				onComplete: function(json){
					if (json){
						icon_c = item.getElement('.title').getElement('.ta');
						if (json.result === true){
							item.removeClass('wait_for_delivery');
							item.addClass('delivered');
							icon_c.setProperty('title','Odeslano');
							$('CampaignEmailLogs').value += "Odeslano na " + json.email + "\n"
							
							icon_c.removeClass('sms_progress');
							icon_c.addClass('sms_success');
							
						} else {
							icon_c.setProperty('title','Chyba během odesilani: ' + json.message);
							$('CampaignEmailLogs').value += 'Chyba během odesilani: ' + json.message + ' Email: ' + json.email + "\n";
							icon_c.removeClass('sms_progress');
							icon_c.addClass('sms_error');
							item.removeClass('wait_for_delivery');
							item.addClass('undelivered');
						}
						progress_step ++;
						redraw_progress();
						send_sms_item(group_id);
					} else {
						alert('Chyba aplikace');
					}
				}
			}).send();
		} else {
			new Request.JSON({
				url: '/campaign_email/save_log/',
				data: {
					'data[CampaignEmail][id]' 	: group_id,
					'data[CampaignEmail][logs]' 	: $('CampaignEmailLogs').value
				},
				onComplete: function(json){
					if (json){
						if (json.result === true){
							progress_step ++;
							redraw_progress();
							$('progress').getElement('.bar').setStyle('width','0%');
							$('progress').getElement('.label').setHTML('Odesílání dokončeno.');
							progress_count = 0;
							progress_step = 0;
							click_refresh(group_id);
							$('form_buttons').fade(1);
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
				}
			}).send();
		}
	}

	var progress_count = 0;
	var progress_step = 0;
	// validace
	function valid_sms_sender(){
		error = [];
		if ($('CampaignEmailName').value == '') error.push('Musíte vyplnit předmět emailu');
		if ($('CampaignEmailText').value == '') error.push('Musíte vyplnit text emailu');
		if ($('CampaignEmailEmailFrom').value == '') error.push('Musíte vyplnit odchozí email');
		if ($('CampaignEmailEmailFromName').value == '') error.push('Musíte vyplnit popis odchozího emailu');
		if ($('send_list').getElements('.wait_for_delivery').length == 0) error.push('Musíte zvolit příjemce Emailu');

		if (error.length > 0){
			var error_message = new MyAlert();
			error_message.show(error);
			return false;
		} else {
			return true;
		}
	}
	
	// odeslani SMS
	if($('send_sms_now'))
		$('send_sms_now').addEvent('click',function(e){
			new Event(e).stop();
            if ($$('.disabled_save').length != 0)
    			alert('Je aktivní HTML mód editoru. Přepněte jej prosím do normální módu.');
    		else {
    			if (valid_sms_sender()){
    				//$('form_buttons').fade(0);
    			//	$('send_sms_now').addClass('none');
    				progress_count = $('send_list').getElements('.wait_for_delivery').length + 2;
    				$('progress').removeClass('none');
                    var data_send = $H({
    					'data[CampaignEmail][name]' : $('CampaignEmailName').value,
    					'data[CampaignEmail][text]' : $('CampaignEmailText').value,
    					'data[CampaignEmail][email_from]' : $('CampaignEmailEmailFrom').value,
    					'data[CampaignEmail][email_from_name]' : $('CampaignEmailEmailFromName').value,
    				});
                    $$('.prilohy').each(function(hidden){
                        data_send[hidden.getProperty('name')]  = hidden.value;
                    });
                    $$('.wysiwyg').killWswg();
    				new Request.JSON({
    					url: '/campaign_email/edit/',
    					data: data_send,
    					onComplete: function(json){
    						if (json){
    							if (json.result === true) {
    								send_sms_item(json.id);
    								progress_step ++;
    								redraw_progress();
    							} else {
    								$('CampaignEmailLogs').value += json.message + "\n";
    								alert(json.message);
    							}
    						} else {
    							alert('Chyba aplikace')
    						}
    					}
    				}).send();
    			}
            }
		});
	
	// reindexace rel
	function reindex(parent_){
		if($type (parent_) != 'array') parent_ = [parent_];
		$each(parent_, function(parentE){
			var parentE = $(parentE);
			parentE.getElements('.choose_li').each(function(li,index){
				li.setProperty('rel',index);
			});
		});
	}

	function redraw_progress(){
		var bar = $('progress').getElement('.bar');
		var label = $('progress').getElement('.label');
		var percento = 100 * progress_step / progress_count;
		bar.setStyle('width',percento + '%');
		label.setHTML('Odesílání Emailů - ' + Math.round(percento) + '%')
	}

	//move all right
	if($('move_right_all'))
	$('move_right_all').addEvent('click',function(e){
		new Event(e).stop();
		$('choose_list').getElements('.choose_li').addClass('selected');
		move_right();
	});
	
	// move selected right
	if($('move_right_selected'))
	$('move_right_selected').addEvent('click',function(e){
		new Event(e).stop();
		move_right();
	});
	
	function move_right(){
		$('send_parent_div').addClass('preloader');
		var error_move = [];
		$('choose_list').getElements('.selected').each(function(item){
			var exist_user = $('send_list').getElement('.phone_number[value=' + item.getElement('.phone_number').value + ']');
			if(!exist_user){
				$('send_list').adopt(item);
				item.removeClass('selected');
				item.getElement('.sms_stav').removeClass('none');
			} else {
				item.removeClass('selected');
				error_move.push('Klient ' + item.getElement('.client_name').value + ' nebyl přidán do seznamu, toto cislo již v seznamu je u jména ' + exist_user.getParent('li').getElement('.client_name').value + '.');
			}
		});
		if (error_move.length > 0){
			
			var error_move_message = new MyAlert();
			error_move_message.show(error_move,{button_label:'Ok','caption':'Upozornění'});
		}
		reindex(['choose_list','send_list']);
		$('send_parent_div').removeClass('preloader');
	}

	$('close_sms_sender').addEvent('click',function(e){
		new Event(e).stop();
        $$('.wysiwyg').killWswg();
		domwin.closeWindow('domwin');
	});
//-->
</script>