<div class="win_fce top">
<a href='/campaign_email/blacklist_add/<?php echo $this->data['CampaignEmailBlacklist']['id'];?>' class='button edit' id='recruiter_add_new' title='Přidání'>Přidat uživatele</a>
</div>
<table class='table' id='recruiter_table'>
	<tr>
		<th>Email</th>
        <th>Realizátor</th>
		<th>Přiřazen</th>
		<th></th>
	</tr>
	<?php if (isset($item_list) && count($item_list) > 0):?>
		<?php foreach($item_list as $item):?>
		<tr>
			<td><?php echo $item['CampaignEmailBlacklist']['email'];?></td>
            <td><?php echo $item['AtCompany']['name'];?></td>
			<td><?php echo $fastest->czechDate($item['CampaignEmailBlacklist']['created']);?></td>
			<td><a href="#" rel="<?php echo $item['CampaignEmailBlacklist']['id'];?>" class="ta trash">Odebrat</a></td>
		</tr>
		<?php endforeach;?>
	<?php endif;?>
</table> 
<script>	
	$('recruiter_table').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat toto spojení?')){
			new Request.HTML({
				url:'/campaign_email/blacklist_trash/'+ this.rel,
				onComplete: (function(){
					this.getParent('tr').dispose();
				}).bind(this)
			}).send();
		}
	});
	
	
	$('recruiter_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_recruiter_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>