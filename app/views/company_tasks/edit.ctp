<form action='/company_tasks/edit/' method='post' id='company_tasks_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyTask/id');?>
	
	<fieldset>
		<legend>Prosím vyplňte následující údaje</legend>
		<?php if (!isset($this->data['CompanyTask']['id']) || $this->data['CompanyTask']['creator_id'] == $logged_user['CmsUser']['id']):?>
			<?php echo $htmlExt->input('CompanyTask/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
			<div class='sll'>
				<?php echo $htmlExt->inputDate('CompanyTask/termin',array('tabindex'=>1,'label'=>'Termín'));?> <br class="clear">
				<?php echo $htmlExt->selectTag('CompanyTask/cms_user_id',$cms_user_list,null,array('tabindex'=>2,'label'=>'Pro'));?> <br class="clear">
			</div>
			<div class='slr'>
				<?php echo $htmlExt->selectTag('CompanyTask/company_tasks_type_id',$company_tasks_list,null,array('tabindex'=>2,'label'=>'Typ aktivity'));?> <br class="clear">
				<?php echo $htmlExt->selectTag('CompanyTask/company_id',$company_list,null,array('tabindex'=>2,'label'=>'Firma'));?> <br class="clear">
			</div>
			<br class='clear' />
			<?php echo $htmlExt->textarea('CompanyTask/text',array('tabindex'=>1,'label'=>'Popis','class'=>'long','label_class'=>'long'));?><br class='clear' />
			<?php if (isset($this->data['CompanyTask']['id']) && $this->data['CompanyTask']['cms_user_id'] == $logged_user['CmsUser']['id']):?>
				<?php echo $htmlExt->textarea('CompanyTask/result',array('tabindex'=>1,'label'=>'Řešení','class'=>'long','label_class'=>'long'));?><br class='clear' />
			<?php endif;?>
		<?php else:?>
			<?php echo $htmlExt->var_text('CompanyTask/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
			<div class='sll'>
				<?php echo $htmlExt->var_text('CompanyTask/termin',array('tabindex'=>1,'label'=>'Termín','show_type'=>'date'));?> <br class="clear">
				<?php echo $htmlExt->var_text('CmsUserTo/name',array('tabindex'=>1,'label'=>'Pro'));?> <br class="clear">
			</div>
			<div class='slr'>
				<?php echo $htmlExt->var_text('SettingTaskType/name',array('tabindex'=>2,'label'=>'Typ aktivity'));?> <br class="clear">
				<?php echo $htmlExt->var_text('Company/name',array('tabindex'=>2,'label'=>'Firma'));?> <br class="clear">
			</div>
			<br class='clear' />
			<?php echo $htmlExt->var_text('CompanyTask/text',array('tabindex'=>1,'label'=>'Popis','class'=>'long','label_class'=>'long'));?><br class='clear' />
			<?php if (isset($this->data['CompanyTask']['id']) && $this->data['CompanyTask']['cms_user_id'] == $logged_user['CmsUser']['id']):?>
				<?php echo $htmlExt->textarea('CompanyTask/result',array('tabindex'=>1,'label'=>'Řešení','class'=>'long','label_class'=>'long'));?><br class='clear' />
			<?php else:?>
				<?php echo $htmlExt->var_text('CompanyTask/result',array('tabindex'=>1,'label'=>'Řešení','class'=>'long','label_class'=>'long'));?><br class='clear' />
			<?php endif;?>
		<?php endif;?>
		
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyTaskSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyTaskClose'/>
	</div>
</form>
<script>
	$('AddEditCompanyTaskClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	
	$('AddEditCompanyTaskSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_tasks_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('company_tasks_edit_formular').action,		
				onComplete:function(){
					click_refresh($('CompanyTaskId').value);
					domwin.closeWindow('domwin');
				}
			}).post($('company_tasks_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_tasks_edit_formular',{
		'CompanyTaskName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název úkolu'},
		}
	});
	validation.generate('company_tasks_edit_formular',<?php echo (isset($this->data['CompanyTask']['id']))?'true':'false';?>);
</script>