<form action='/setting_mlm_recruiters/edit/' method='post' id='setting_mlm_recruiters_edit_formular'>
	<?php echo $htmlExt->hidden('ConnectionMlmRecruiter/id');
  // stare zaznamy
  //echo $htmlExt->hidden('OldData/cms_user_id',array('value'=>$this->data["ConnectionMlmRecruiter"]["cms_user_id"]));
  echo $htmlExt->hidden('OldData/parent_id',array('value'=>$this->data["ConnectionMlmRecruiter"]["parent_id"]));
  echo $htmlExt->hidden('OldData/mlm_group_id',array('value'=>$this->data["ConnectionMlmRecruiter"]["mlm_group_id"]));
  echo $htmlExt->hidden('OldData/onlyUpdate',array('value'=>$onlyUpdate ));
   
  ?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<div class="sll">  
					<?php echo $htmlExt->selectTag('ConnectionMlmRecruiter/mlm_group_id',$mlm_group_list,null,array('label'=>'Skupina MLM'),null,true);?><br />
				</div><br />
			</fieldset>
			<fieldset>
				<legend>Recruiter</legend>
				<div class="sll"> 
        <?php 
          //print_r($this->data);
          if(empty($this->data))
            echo $htmlExt->selectTag('ConnectionMlmRecruiter/cms_user_id',$recruiters_list,null,array('label'=>'Recruiter'),null,false);
          else{
            echo $htmlExt->hidden('ConnectionMlmRecruiter/cms_user_id',array('value'=>$this->data["ConnectionMlmRecruiter"]["cms_user_id"]));
            echo  $htmlExt->var_text('CmsUser/name',array('label'=>'Recruiter'));
          }  
            ?> <br />
				</div><br />
					</fieldset>
					
	 	<div id="recruiter"></div>	
		
	</div>
 </div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
  var ParentId = <?php echo  (isset($this->data['ConnectionMlmRecruiter']['parent_id']))?$this->data['ConnectionMlmRecruiter']['parent_id']:0;?>;
  var CmsUserId = <?php echo  (isset($this->data['ConnectionMlmRecruiter']['cms_user_id']))?$this->data['ConnectionMlmRecruiter']['cms_user_id']:0;?>;
      
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_mlm_recruiters_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_mlm_recruiters_edit_formular').action,		
				onComplete:function(){
					click_refresh($('ConnectionMlmRecruiterId').value);
					domwin.closeWindow('domwin')
				}
			}).post($('setting_mlm_recruiters_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_mlm_recruiters_edit_formular',{

	});
	validation.generate('setting_mlm_recruiters_edit_formular',<?php echo (isset($this->data['ConnectionMlmRecruiter']['id']))?'true':'false';?>);

  //alert($('ConnectionMlmRecruiterMlmGroupId').value);
  if($('ConnectionMlmRecruiterMlmGroupId').value==3 || $('ConnectionMlmRecruiterMlmGroupId').value==2) {
    	 new Request.HTML({
		  		url: '/setting_mlm_recruiters/load_recruiters/' + $('ConnectionMlmRecruiterMlmGroupId').value+'/'+ParentId+'/'+CmsUserId,
		  		update:'recruiter'
		  	}).send();
  }


	$('ConnectionMlmRecruiterMlmGroupId').addEvent('change', function(){
		if( this.value > 1){
		  	new Request.HTML({
		  	  //<?php echo $id ;?> + '/'
		  		url: '/setting_mlm_recruiters/load_recruiters/' + this.value + '/' + ParentId+'/'+CmsUserId,
		  		update:'recruiter'
		  	}).send();
		}
		else $('recruiter').empty();
	});

</script>