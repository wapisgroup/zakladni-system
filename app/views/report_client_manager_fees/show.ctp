<form action='/setting_activity_types/edit/' method='post' id='setting_activity_edit_formular'>
	<?php echo $htmlExt->hidden('SettingActivityType/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Seznam Firem</a></li>
			<li class="ousko"><a href="#krok2">Seznam Zaměstnanců</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Firmy</legend>
				<table class='table' id='rating_table'>
					<tr>
						<th>Firma</th>
						<th>Počet hodin</th>
					</tr>
					<?php 
					if (isset($firmy_list) && count($firmy_list) > 0){
						foreach($firmy_list as $firma_id => $firma):?>
						<tr>
							<td><?php echo $firma;?></td>
							<td><?php echo array_sum($tmp[$firma_id]);?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádne firmy</td></tr>"; 
					?>
				</table> 
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Zaměstnanci</legend>
				<table class='table' id='rating_table'>
					<tr>
						<th>Zaměstnanec</th>
						<th>Počet hodin</th>
					</tr>
					<?php 
					if (isset($zamestnanci_list) && count($zamestnanci_list) > 0){
						foreach($zamestnanci_list as $zam):?>
						<tr>
							<td><?php echo $zam['Client']['name'];?></td>
							<td><?php echo $zam['ClientWorkingHour']['celkem_hodin'];?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádní zaměstnanci</td></tr>"; 
					?>
				</table> 
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 

	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_show');});
</script>