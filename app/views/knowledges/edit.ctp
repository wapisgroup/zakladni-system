<script type="text/javascript">
	function upload_file_complete(url){
		alert('Soubor byl nahrán.');
	}
</script>
<form action='/knowledges/edit/' method='post' id='knowledge_edit_formular'>
	<?php echo $htmlExt->hidden('Knowledge/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<div class="sll">  
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/knowledges/upload_attach/',
								'path_to_file'	=> 'uploaded/knowledges/',
								'status_path' 	=> '/get_status.php',
							),
							'upload_type'	=> 'php',
							'onComplete'	=> 'upload_file_complete',
							'label'			=> 'Soubor'
						);
					?>
					<?php echo $fileInput->render('Knowledge/file',$upload_setting);?> <br class="clear">
				<?php 
					if (isset($this->data['Knowledge']['file'])){
						echo '<label>Soubor</label>';
						echo $html->link('Otevřít soubor','attachs_download/knowledges|'.$this->data['Knowledge']['file'].'/'.$this->data['Knowledge']['name'],array('target'=>'_blank','title'=>'Otevřít soubor'),null,false);
					}
				?>
				</div>
				<div class="slr">  
					<?php echo $htmlExt->input('Knowledge/name',array('tabindex'=>1,'label'=>'Název'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('knowledge_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('knowledge_edit_formular').action,		
				onComplete:function(){
					click_refresh($('KnowledgeId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('knowledge_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('knowledge_edit_formular',{
		'KnowledgeName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('knowledge_edit_formular',<?php echo (isset($this->data['Knowledge']['id']))?'true':'false';?>);
</script>