      <div id="obal_tabulky">
			<table class='table' id='rating_table'>
					<tr>
						<th>Firma</th>
						<th>Osoba</th>
						<th>Pronájem od</th>
						<th>Vráceno</th>
					</tr>
					<?php 
					if (isset($history_list) && count($history_list) > 0){
						foreach($history_list as $item):
						?>
						<tr>
							<td><?php echo $item['CompanyAuditEstate']['name'];?></td>
							<td><?php echo $item['CompanyPerson']['name'];?></td>
							<td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['created']);?></td>
							<td><?php echo $fastest->czechDate($item['ConnectionAuditEstate']['to_date']);?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td></td>Žádná historie</td></tr>"; 
					?>
					</table>
       </div> 
				<br />

	<div class="win_save">
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>

 
 <script language="javascript" type="text/javascript">
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
</script>