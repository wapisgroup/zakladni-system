<div id='main_page'>
	<div class='sll'>
		<div class="box">
			<h3>Novinky ve vývoji:</h3>
			<div class="table">
				<h4>29. dubna 2009</h4>
				<div>Spuštěna testovací verze</div>
			</div>
			<div class="table">
				<h4>27. dubna 2009</h4>
				<div>Zahájení práce na testovací verzi</div>
			</div>
		</div>
	</div>
	<div class='slr'>
		<div class='box'>
			<h3>Denní report - Managing Director:</h3>
			 <div class="table">
				<table>
					<tr><th>Datum:</th><td>23. leden 2009</td></tr>
					<tr><th>Celkový počet zaměstnanců:</th><td>489</td></tr>
					<tr><th>Celkový počet uchazečů:</th><td>3907</td></tr>

					<tr><th>Celkový počet Client Managerů:</th><td>18</td></tr>
					<tr><th>Celkový počet aktivních středisek:</th><td>89</td></tr>
					<tr><th>Celkový počet obchodních příležitostí:</th><td>383</td></tr>

					<tr><th>Celkový počet požadavků:</th><td>78</td></tr>

					<tr><th>Celkový počet zadaných volných míst:</th><td>486</td></tr>
					<tr><th>Celkový obsazených volných míst uchazeči:</th><td>145</td></tr>
					<tr><th>Celkový obsazených volných míst zaměstnanci:</th><td>61</td></tr>

					<tr><th>Statistika měsíčních výkazů aktuálního měsíce:</th><td>leden</td></tr>

					<tr><th>Počet otevřených výkazů AM:</th><td>50</td></tr>
					<tr><th>Počet uzavřených výkazů AM:</th><td>0</td></tr>
					<tr><th>Počet otevřených výkazů účetní:</th><td>0</td></tr>
					<tr><th>Počet uzavřených výkazů účetní:</th><td>0</td></tr>
				</table>
			</div>
		</div>
	</div>
	<br />
</div>
