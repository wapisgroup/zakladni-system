<div>
	<?php echo $htmlExt->hidden('connection_client_at_company_work_position_id',array('value' => $connection_id));?>
	<div class="sll3" >  
		<?php echo $htmlExt->var_text('Client/jmeno',array('label'=>'Jméno','class'=>'read_info'));?> <br/>
		<?php echo $htmlExt->var_text('Client/prijmeni',array('label'=>'Příjmení','class'=>'read_info'));?> <br/>
	</div>
	<div class="sll3" >  
		<?php echo $htmlExt->var_text('ConnectionClientAtCompanyWorkPosition/datum_nastupu',array('label'=>'Datum nástupu','class'=>'read_info'));?> <br/>
		<?php echo $htmlExt->var_text('ConnectionClientAtCompanyWorkPosition/datum_to', array('label'=>'Datum ukončení','class'=>'read_info'));?>
	</div>
	<div class='sll3'>
		<?php echo $htmlExt->selectTag('month2',$mesice_list2,$month,array('label'=>'Měsíc', 'class'=>'odpracovane_hodiny_change_date'));?> <br/>
		<?php echo $htmlExt->selectTag('year2', $actual_years_list,$year,array('label'=>'Rok', 'class'=>'odpracovane_hodiny_change_date'),null,true);?>
	</div>
</div>
<br />
<div id="odpracovane_hodiny_element">
	<?php echo $this->renderElement('../internal_employees/element',array('month'=>$month, 'year' =>$year));?>
</div> 
<script language="javascript" type="text/javascript">

	// init event for onChange year and month
	$$('.odpracovane_hodiny_change_date').addEvents({
		'change': function(e){
			this.dont_change_focus = true;
			$('odpracovane_hodiny_element').fade(0);
			if (!$('made_change_in_form') || $('made_change_in_form').value == 0 || confirm('Byly provedeny změny, po provedeni datum_tohodatum_to kroku nebudou změněné udaje uloženy. Pokud je chcete uložit, klikněte nejprve na tlačitko Uložit. Chcete pokračovat?')){
				new Request.HTML({
					url: '/internal_employees/odpracovane_hodiny/' + $('ConnectionClientAtCompanyWorkPositionId').value + '/' + $('Year2').value + '/' + $('Month2').value + '/true',
					update: $('odpracovane_hodiny_element'),
					onComplete: function(){
						$('odpracovane_hodiny_element').fade(1);
					}
				}).send();
			} else {
				new Event(e).sdatum_top();
				this.options[this.currentSelection].selected = 'selected';
				$('odpracovane_hodiny_element').fade(1);
			}
			this.dont_change_focus = false;
		},
		'focus': function(){
			if (!this.dont_change_focus || this.dont_change_focus == false){
				this.currentSelection = this.selectedIndex;
			}
		}
	});
    
     new FormHint();

</script>