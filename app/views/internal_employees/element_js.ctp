<script language="javascript" type="text/javascript">
	/**
	 * Nastaveni
	 */
	var domtab = new DomTabs({'className':'admin_dom'});
	var admin_save = false;
    var stat_id = <?php echo $stat_id;?>;
    var payment_sum = <?php echo $payment_sum;?>;
    var mzda_setting = '<?php echo json_encode($mzda_setting);?>';
    var permission_to_editation_max = '<?php echo $permission_to_editation_max;?>';
    
    // prazdne inputy vloz 0
	$('ClientWorkingHoursInt_edit_formular').getElements('.float').each(function(item){if (item.value == '') item.value = '0';});
    // input limit
	$('ClientWorkingHoursInt_edit_formular').getElements('.integer, .float').inputLimit();
    //change?
	$('odpracovane_hodiny_element').getElements('input, select').addEvent('change', function(){
		$('made_change_in_form').value = 1;
	});
    //editace maximalek
    if(permission_to_editation_max == true){
        $('odpracovane_hodiny_element').getElements('input.maximalky').removeProperty('disabled');
    }
    
    /**
     * Stravenky
     */
        $('food_days_all').addEvent('click', my_select_all.bindWithEvent(this,['food_day']));
    	// fce na hromadne zaskrtnuti
    	function my_select_all(e,className){
    		var event = new Event(e);
    		event.stop();
    		obj = event.target;
    
    		if (!obj.zaskrkly){
    			$('ClientWorkingHoursInt_edit_formular').getElements('.'+className+':enabled').setProperty('checked','checked');
    			obj.zaskrkly = true;
    		} else {
    			$('ClientWorkingHoursInt_edit_formular').getElements('.'+className).removeProperty('checked');
    			try {
    				delete obj.zaskrkly;
    			} catch (e){
    				obj.zaskrkly = null;
    			}
    		}
    	}
    
    
    // function for reacound day/hours
	$$('.hodiny').addEvent('change', recount_hours);
	function recount_hours(){
		var NH = $('ClientWorkingHoursIntStandardHours').value.toFloat();
		var count = 0;
		var vikendy = 0;
		var svatky = 0;
		var prescas = 0;
		var dovolena = 0;
		var neplacene_volno = 0;
		var ocr = 0;
		var neomluvena_absence = 0;
		var pracovni_neschopnost = 0;
		var nahradni_volno = 0;
		var paragraf = 0;
		
		var s1 = 0, s2 = 0, s3 = 0;
		
		$$('.hodiny').each(function(item){
			switch(item.value){
				case 'PN':
						pracovni_neschopnost ++;
					break;
				case 'OČR':
						ocr ++;
					break;
				case 'D':
						dovolena ++;
					break;
				case 'NV':
						neplacene_volno ++;
					break;
				case 'A':
						neomluvena_absence ++;
					break;
				case 'V':
						nahradni_volno ++;
					break;
				case 'PG':
						paragraf ++;
					break;
				default:
					if (item.value == '') item.value = 0;
					
					if (item.hasClass('s1')) s1 += item.value.toFloat();
					if (item.hasClass('s2')) s2 += item.value.toFloat();
					if (item.hasClass('s3')) s3 += item.value.toFloat();
					
					// celkem
					count += item.value.toFloat();
					
					// vikend
					if (item.hasClass('vikend')) vikendy += item.value.toFloat();
					// svatky
					if (item.hasClass('svatky')) svatky += item.value.toFloat();
					// prescas
					if (item.value.toFloat() > NH) prescas += (item.value.toFloat() - NH);
					break;
			}
            
            //kontrola zda jsme nesmazali dovolenou
            $('ClientWorkingHoursIntZbyvaDovolene').value = $('DovolenaZbyvaDovolene').value - dovolena;
		});

		
		$('ClientWorkingHoursIntCelkemHodin').value = count;
		$('ClientWorkingHoursIntSvatky').value = svatky;
		$('ClientWorkingHoursIntPrescasy').value = prescas;
		$('ClientWorkingHoursIntVikendy').value = vikendy;
		$('ClientWorkingHoursIntDovolena').value = dovolena;
		$('ClientWorkingHoursIntNeplaceneVolno').value = neplacene_volno;
		$('ClientWorkingHoursIntOcestrovaniClenaRodiny').value = ocr;
		$('ClientWorkingHoursIntNeomluvenaAbsence').value =  neomluvena_absence;
		$('ClientWorkingHoursIntPracovniNeschopnost').value =  pracovni_neschopnost;
		$('ClientWorkingHoursIntNahradniVolno').value =  nahradni_volno;
		$('ClientWorkingHoursIntParagraf').value =  paragraf;
    		
		$('ClientWorkingHoursIntHourS1').value = s1;
		$('ClientWorkingHoursIntHourS2').value = s2;
		$('ClientWorkingHoursIntHourS3').value = s3;
		
		calculate_salary_cost();
	};
    
    // add event dblclick on textinput
	$('ClientWorkingHoursInt_edit_formular').getElements('.hodiny:enabled').addEvent('dblclick', function(){
		// call
		var obj = this;
		domwin.newWindow({
			id			: 'domwin_hodiny_od_do',
			sizes		: [300,170],
			scrollbars	: true, 
			title		: 'Vložte čas práce',
			languages	: false,
			type		: 'DOM',
			dom_id		: $('dblclick_input'),
			closeConfirm: false,
			max_minBtn	: false,
			modal_close	: true,
			remove_scroll: false,
			dom_onRender: function(){
				var choose_select = $('domwin_hodiny_od_do').getElement('.choose_input');
				var choose_do_it  = $('domwin_hodiny_od_do').getElement('.choose_do_it');
				
				var choose_do_div = $('domwin_hodiny_od_do').getElement('.choose_do_div');
				var choose_od_div = $('domwin_hodiny_od_do').getElement('.choose_od_div');
				
			
				
				choose_select.addEvent('change', function(){
					if (this.value == 1){
						choose_do_div.removeClass('none');
						choose_od_div.removeClass('none');
					} 
                    else {
						choose_do_div.addClass('none');
						choose_od_div.addClass('none');
                        
					}
                    
                    if (this.value == 'D'){
						$('domwin_hodiny_od_do').getElement('.dovolena_info').removeClass('none');
                        $('domwin_hodiny_od_do').getElement('.dovolena_info').getChildren('var').setHTML($('ClientWorkingHoursIntZbyvaDovolene').value);
                    }    
                    else    
                        $('domwin_hodiny_od_do').getElement('.dovolena_info').addClass('none');
				});
				
				choose_do_it.addEvent('click', function(e){
					new Event(e).stop();
					
					var choose_od = $('domwin_hodiny_od_do').getElement('.choose_od_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_od_m').value;
					var choose_do = $('domwin_hodiny_od_do').getElement('.choose_do_h').value + ':' + $('domwin_hodiny_od_do').getElement('.choose_do_m').value;
				
					if (choose_select.value == 1)
						obj.value = compare_times(choose_od,choose_do);					
					else {
						obj.value = choose_select.value;

                        if(obj.value == 'D'){
                            var pocet = $('domwin_hodiny_od_do').getElement('.dovolena_info').getChildren('var').getHTML();
                            
                            if(pocet == 0 || pocet < 0)
                                alert('Nárok na dovolenou jde do záporných čísel!');
                            
                            pocet--;
                            $('ClientWorkingHoursIntZbyvaDovolene').value = pocet; 
                        }
					}
					domwin.closeWindow('domwin_hodiny_od_do');
					recount_hours();
				});
			}
		});
	});
	
	function compare_times(time1, time2){
		var s1 = time1.split(':'), s2 = time2.split(':'), td;
		t1 = new Date(1970, 0, 0, s1[0] ? s1[0] : 0, s1[1] ? s1[1] : 0, s1[2] ? s1[2] : 0);
		t2 = new Date(1970, 0, ((s2[0] < s1[0]) || (s2[0]==s1[0] && s2[1] < s1[1]))?1:0, s2[0] ? s2[0] : 0, s2[1] ? s2[1] : 0, s2[2] ? s2[2] : 0);
		td = (t2 - t1)/60/1000;
		//zbytek = ((td % 60) < 10)?'0'+(td % 100):
		zbytek = (td % 60);
		zbytek = (1 / (60 / zbytek));
		output = (td / 60).toInt() + zbytek;
		return output;
	}
	
	
    
    /**
     * Generovani mzdy celkem
     */        
    function calculate_salary_cost(){
		var pocet_hodin = $('ClientWorkingHoursIntCelkemHodin').value.toFloat();	
		var salary = $('ClientWorkingHoursIntSalaryFix').value.toFloat();
        var stravne = $('ClientWorkingHoursIntFoodTickets').value.toFloat();//stravenky
        		
        var drawbacks = 0;
        var odmeny = 0;	
        $each($$('.salary_drawback'),function(item){
            
            var add = ((item.id)+'Add');
            if($(add)){
                if($(add).checked){ drawbacks += item.value.toFloat();}
            }
            else if(item.value > 0)
                drawbacks += item.value.toFloat();    
        })
        
		var odmena_1 = $('ClientWorkingHoursIntOdmena1').value.toFloat();
		var odmena_2 = $('ClientWorkingHoursIntOdmena2').value.toFloat();
        var odmena_3 = $('ClientWorkingHoursIntOdmena3').value.toFloat();
        var odmeny = odmena_1 + odmena_2 + odmena_3;
        
		//var salary_total = pocet_hodin * salary;
		var salary_total = salary;//pouze Hruba mzda		 
        var celkem_mzda = (salary_total-drawbacks)+odmeny+stravne;
		$('ClientWorkingHoursIntSalaryTotal').value = (stat_id == 1 ?  Math.round(celkem_mzda) : celkem_mzda);
	
        
        var zbyva_k_vyplate_ = celkem_mzda - payment_sum;
        $('Zalohy').value = payment_sum;
        $('ChangeMaxZalohyNow').value = (stat_id == 1 ? Math.floor(zbyva_k_vyplate_) : zbyva_k_vyplate_);
        
    };        
    
    // osetreni maximalek sazeb
	$$('.salary').addEvent('change', function(){
		var max = ((this.id)+'Max');

		if(this.value.toFloat() > $(max).value.toFloat()){
			alert('Překročili jste maximální hodnotu! Bude nastavena maximální hodnota.');
			this.value=$(max).value.toFloat();
		}	
        calculate_salary_cost();
	});
    
    $$('.salary_drawback').addEvent('change',calculate_salary_cost);
    
    /**
     * Srazky, tl. prirazeni 
     */
    if($('ClientWorkingHoursIntDrawbackCloseCwAdd'))
        $('ClientWorkingHoursIntDrawbackCloseCwAdd').addEvent('click', calculate_salary_cost);
    if($('ClientWorkingHoursIntDrawbackActivityAdd'))
        $('ClientWorkingHoursIntDrawbackActivityAdd').addEvent('click', calculate_salary_cost);    
    if($('ClientWorkingHoursIntDrawbackAdd'))
        $('ClientWorkingHoursIntDrawbackAdd').addEvent('click', calculate_salary_cost); 
    
    
    $('ClientWorkingHoursIntStopPayment').addEvent('change', function(e){
        if(this.checked)
            $('StopPaymentNow').value = 1;
        else
            $('StopPaymentNow').value = 0;    
    });
     
    /**
     * Zpusobene skody
     */
	$$('.salary_damage').addEvent('change', function(){
	   if(this.value > 0){
	       $('ClientWorkingHoursIntStopPayment').checked = true;
	       $('ClientWorkingHoursIntStopPayment').fireEvent('change');
	   }
	})  
    
    
    /**
      * nacteni defaultnich hodnot pro danou formu odmeny a projekt
      * v pripade ze se nejdna o ulozneou dochazku
      */ 
     function load_mzda_setting(){
        setting = JSON.decode(mzda_setting);
        $each(setting,function(val,col){  
            if(typeof val == 'object'){val = '';}    
            if(col.substr(0,8) == 'default_' && $('ClientWorkingHoursInt'+(col.substr(8).camelCase2().capitalize())) && (val != '')){
                $('ClientWorkingHoursInt'+(col.substr(8).camelCase2().capitalize())).value = val;
            }    
        })
        
        calculate_salary_cost();
     }
     if($('ClientWorkingHoursIntId').value == '')
        load_mzda_setting();
    
    /**
     * hlidani srazek za aktivity
     * pokud uz je jednou ulozena a hodnoty se lisi tak zobrazime upozorneni
     * a moznost aktualizace
     */
     function check_drawback_activity(){
        if($('ClientWorkingHoursIntId').value != '' && $('ClientWorkingHoursIntDrawbackActivity').value.toFloat() != $('DifferentDrawbackActivity').value.toFloat()){
            $('drawback_activity_different_box').removeClass('none');
            var text = "Aktuální srážka za aktivity je: "+$('DifferentDrawbackActivity').value.toFloat();
            $('drawback_activity_different_box').getElement('img').setProperty('title',text).setProperty('alt',text)
        }
     }
     check_drawback_activity();
     $('drawback_activity_different_box').addEvent('click',function(){
        if(!this.hasClass('none')){
            if(confirm("Opravdu chcete aktualizovat srážku za aktivity?")){
                $('ClientWorkingHoursIntDrawbackActivity').value = $('DifferentDrawbackActivity').value.toFloat();
                $('drawback_activity_different_box').addClass('none');
            }
        }
     })
     
     
     /**
     * hlidani srazek za uzavření docházek pro KOO a MR
     * pokud uz je jednou ulozena a hodnoty se lisi tak zobrazime upozorneni
     * a moznost aktualizace
     */
     function check_drawback_close_cw(){
        if($('ClientWorkingHoursIntId').value != '' && $('ClientWorkingHoursIntDrawbackCloseCw').value.toFloat() != $('DifferentDrawbackCloseCw').value.toFloat()){
            $('drawback_close_cw_different_box').removeClass('none');
            var text = "Aktuální srážka za uzavření docházek je: "+$('DifferentDrawbackCloseCw').value.toFloat();
            $('drawback_close_cw_different_box').getElement('img').setProperty('title',text).setProperty('alt',text)
        }
     }
     check_drawback_close_cw();
     $('drawback_close_cw_different_box').addEvent('click',function(){
        if(!this.hasClass('none')){
            if(confirm("Opravdu chcete aktualizovat srážku za uzavření docházek?")){
                $('ClientWorkingHoursIntDrawbackCloseCw').value = $('DifferentDrawbackCloseCw').value.toFloat();
                $('drawback_close_cw_different_box').addClass('none');
            }
        }
     })
     
     
     
     
     
    /**
     * Zalohy
     */
        var zbyva_k_vyplate_ = $('ClientWorkingHoursIntSalaryTotal').value - payment_sum;
        $('Zalohy').value = payment_sum;
        $('ChangeMaxZalohyNow').value = (stat_id == 1 ? Math.floor(zbyva_k_vyplate_) : zbyva_k_vyplate_);
        
        /**
         * zavolani domwinu pro vytvoreni pozadavku pro zalohu
         */
         if($$('.add_down_payments')){
        	$$('.add_down_payments').addEvent('click', function(e){
        		new Event(e).stop();
                
                var type = this.getProperty('rel');
                
                /**
                 * limit k propalceni
                 */
                var payment_limit =  $('ChangeMaxZalohyNow').value;
                
                	domwin.newWindow({
            			id			: 'domwin_down_payments',
            			sizes		: [350,185],
            			scrollbars	: false, 
            			title		: (type == 0 ? 'Vložte požadavek na zálohu' : 'Vložte požadavek na VVH'),
            			languages	: false,
            			type		: 'AJAX',
                        ajax_url	: '/internal_employees/add_down_payments/'+type+'/',
                        post_data	: {
                                save:0,
                                limit:payment_limit,
                                celkem_hodin:$('ClientWorkingHoursIntCelkemHodin').value,
                                company_id:$('ClientWorkingHoursIntAtCompanyId').value,
                                connection_client_requirement_id:$('ClientWorkingHoursIntConnectionClientAtCompanyWorkPositionId').value,
                                year:$('ClientWorkingHoursIntYear').value,
                                month:$('ClientWorkingHoursIntMonth').value,
                                client_id:$('ClientWorkingHoursIntClientId').value,
                                stat_id:stat_id
                        },
            			closeConfirm: false,
            			max_minBtn	: false,
            			modal_close	: true,
            			remove_scroll: false
                    });
            });     
        }     
    
    // save button
    if($('only_save'))
    	$('only_save').addEvent('click', function(e){
    		new Event(e).stop();
    
    		// odstraneni disabled u maximalek aby jse ulozili
    		$('ClientWorkingHoursIntSalaryFixMax').removeProperty('disabled');
            
            $each($$('.odpracovane_hodiny').getElements('input'),function(item){
                item.removeProperty('disabled');
            })
    
            //prepocteni hodin pred save
            recount_hours();
    
            button_preloader($('only_save'));
    		new Request.JSON({
    			url: $('ClientWorkingHoursInt_edit_formular').getProperty('action'),
    			onComplete: function(json){
    				if (json){
    					if (json.result === true){
    						if(admin_save == true){
    							new Request.HTML({
    								url: '/internal_employees/odpracovane_hodiny/' + $('ConnectionClientAtCompanyWorkPositionId').value + '/' + $('Year2').value + '/' + $('Month2').value + '/true',
    								update: $('odpracovane_hodiny_element'),
    								onComplete: function(){
    									$('odpracovane_hodiny_element').fade(1);
    								}
    							}).send();
    						}
    						else {
                                click_refresh($('ConnectionClientAtCompanyWorkPositionId').value);
                                domwin.closeWindow('domwin_zamestnanci_hodiny');
    						}    
    					} else {
    						alert(json.message);
    					}
    				} else {
    					alert('Chyba aplikace!');
    				}
                    
                    button_preloader_disable($('only_save'));
    			}
    		}).send($('ClientWorkingHoursInt_edit_formular'));
       });
       
    /**
     * zavolani domwinu pro pridani problemu
     */
     if($('add_misconducts')){
    	$('add_misconducts').addEvent('click', function(e){
    		new Event(e).stop();
                        
            	domwin.newWindow({
        			id			: 'domwin_add_misconducts',
        			sizes		: [600,320],
        			scrollbars	: false, 
        			title		: 'Vložte negativní hodnocení',
        			languages	: false,
        			type		: 'AJAX',
                    ajax_url	: '/misconducts/add/1/',
                    post_data	: {
                            client_id:$('ClientWorkingHoursIntClientId').value,
                            year:$('ClientWorkingHoursIntYear').value,
                            month:$('ClientWorkingHoursIntMonth').value
                    },
        			closeConfirm: false,
        			max_minBtn	: false,
        			modal_close	: true,
        			remove_scroll: false
                });
        });     
    }
       
    //zavreni
    $('only_close').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_zamestnanci_hodiny')
	});    
</script>