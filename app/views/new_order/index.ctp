<div id="NewOrderBox">
    <div id="OrdersBox">
        <?php echo $htmlExt->hidden('ActiveOrderId',array('value'=>0));?>
        <div id="OrderList">
            <div class="filtration_small"><?php echo $viewIndex->filtration($renderSetting['orders'],$this->viewVars, $logged_user);?></div>
            <form action='#' method='post' id='items_form'>
            	<div id='items_orders'><?php echo $this->renderElement('../new_order/items',array('items'=>$items_orders,'renderSetting'=>$renderSetting['orders'],'paging'=>$paging_orders));?></div>
            </form>
        </div>
        <div id="DetailOrderBox" class="none">
        
        </div>
    </div>
    <div id="ClientsBox">
        <div class="filtration_small"><?php echo $viewIndex->filtration($renderSetting['clients'],$this->viewVars, $logged_user);?></div>
        <form action='#' method='post' id='items_form'>
        	<div id='items_clients'><?php echo $this->renderElement('../new_order/items',array('items'=>$items_clients,'renderSetting'=>$renderSetting['clients'],'paging'=>$paging_clients));?></div>
        </form>
    </div>
    <br />
</div>
<script>
$$('.filtr_button').addEvent('click', function(e){
	new Event(e).stop();   
	new Request.HTML({
		update: this.getParent('form').getElement('input[name=update_box_name]').value,
		url: this.getParent('form').getProperty('action')
	}).get(this.getParent('form'));
});

if ($$('.filtr_button_cancel')){
	$$('.filtr_button_cancel').addEvent('click', function(e){
		new Event(e).stop();
		this.getParent('form').getElements('.fltr_input, .fltr_select').each(function(item){item.value = ''})
		new Request.HTML({
			update: this.getParent('form').getElement('input[name=update_box_name]').value,
			url: this.getParent('form').getProperty('action'),
			onComplete: function(){
			}
		}).get(this.getParent('form'));
	});
}

function check_active_order(){
    if($('DetailOrderBox').hasClass('none'))
        $('ActiveOrderId').value = 0;
}
check_active_order();

if ($('move_client_to_order')){
	$('move_client_to_order').addEvent('click', function(e){
		new Event(e).stop();
		
        var active_order_id = $('ActiveOrderId').value;
        var active_clients_id =  new Array();
        $each($('items_clients').getElements('tbody input[type=checkbox]'),function(item){
           if(item.checked){
                active_clients_id.push(item.getParent('tr').getProperty('id'));
           } 
        });
        
        if(active_order_id != '' && active_order_id > 0){
            var update_order = new Request.HTML({
    			update: 'DetailOrderBox',
    			url: '/orders_cast_posts/nabor_pozice/'+active_order_id+'/new_order/',
    			onComplete: function(){}
		    });
            
            var multi_add = new Request.JSON({
    			url:'/orders_cast_posts/multi_add_to_nl/',
    			data : {
    			     'data[order_id]':active_order_id,
    			     'data[clients][]':active_clients_id
                },
    			onComplete: (function(json){
    				if (json) {
    				   	if (json.result === true) {	
    				   	    message = 'Přidání proběhlo úspešně';
                            
    						if(json.wrong_clients != '')
    						  message = message+json.wrong_clients;
                            
                            update_order.send();
                            alert(message);
    				   	}
    				   	else {
    				   		alert(json.message);
    				   	}
    				} else {
    					alert('Chyba aplikace');
    				}
    			}).bind(this)
		    })
            
            new Request.JSON({
    				 url: '/nabor_orders/is_blacklist/0/1/',
                     data : {
        			     'data[clients][]':active_clients_id
                     },
    				 onComplete: function(json){
    				    if (json){
    						if (json.result === true){
    							    multi_add.send();
    						} else {
    						    message = "Někteří klienti se nachází na černé listině. Chcete i přesto umístit klienty na nominační listinu?\n\nKlienti na černé listině:\n";
    						    if(json.wrong_clients != '')
    						        message = message+json.wrong_clients;
							    if (confirm(message)){
									multi_add.send();
							    }
    						}
    				    } else {
    						alert('Chyba behem komunikace s DB');
    				    }
    				 }
    		}).send();
       
    		
        
        }
        else
            alert('Nemužete přesouvat klienty, nemáte žádnou aktivní objednávku!!!');
	});
}


posun = 203;
window.addEvents({
	'domready' : function(){
		basic_body_size_c = $('items_clients').getElement('tbody').getStyle('height').toInt();
		if (window.getSize().y - posun < basic_body_size_c)
			$('items_clients').getElement('tbody').setStyle('height',window.getSize().y - posun);
            
        basic_body_size_o = $('items_orders').getElement('tbody').getStyle('height').toInt();
		if (window.getSize().y - posun < basic_body_size_o)
			$('items_orders').getElement('tbody').setStyle('height',window.getSize().y - posun);  
			
		var Tips_help = new Tips($$('.div_title'),{
			showDelay: 400,
			hideDelay: 400,
			className: 'detail_table_tip'
		});
		
	},
	'resize': function(){
		if (window.getSize().y - posun < basic_body_size_c)
			$('items_clients').getElement('tbody').setStyle('height',window.getSize().y - posun);
        if (window.getSize().y - posun < basic_body_size_o)    
			$('items_orders').getElement('tbody').setStyle('height',window.getSize().y - posun);   
	}
})

if ($$('.filtr_more')){
$$('.filtr_more').addEvent('click', function(){
    var filtr = this.getParent('.filtration_small');
	if (filtr.hasClass('big_filtr')){
		filtr.removeClass('big_filtr');
		filtr.removeClass('up');
		filtr.tween('height', 44);
	} else {
		var pocet = this.getProperty('rel');
		
		var vyska_filtrace = Math.ceil(pocet/3)*23;
		filtr.addClass('big_filtr');
		filtr.addClass('up');
		filtr.tween('height', vyska_filtrace);
	}
	return false;
});
}	
</script>