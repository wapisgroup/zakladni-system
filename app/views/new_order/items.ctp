<?php  
//pr($renderSetting);
if (isset($pagination) && isset($paging)) 	$pagination->setPaging($paging); ?> 
<div class="pagination_top"><?php //echo $this->renderElement('pagination',array('paging'=>$paging));?></div>
<table id='tabulka_item' class='table'>
<?php 
	echo '<thead><tr>';
	$th = array();
	if (isset($renderSetting['items'])){
		if (!isset($renderSetting['no_checkbox'])) echo '<th class="no_print"><input type="checkbox" id="check_all"/></th>';
		foreach($renderSetting['items'] as &$item_setting){
			list($caption, $model, $col, $type, $fnc) = explode('|',$item_setting);
			//pr($col);
			$item_setting = compact(array("caption", "model","col","type","fnc"));	
			$th[] = $colth =  $pagination->sortByTh($col, $caption, $model);
			echo '<th'.(($colth[1] != null)?' class="'.$colth[1].' table_th_'.$col.'"':' class="table_th_'.$col.'"').'>'.(($type != 'hidden')?$colth[0]:'').'</th>'; 
			//echo '<th'.(($colth[1] != null)?' class="'.$colth[1].' table_th_'.$col.'"':' class="table_th_'.$col.'"').'><span><div class="table_header_over"></div></span>'.(($type != 'hidden')?$colth[0]:'').'</th>';
		}
		if (!empty($renderSetting['posibility'])) echo '<th class="no_print">Možnosti</th>';
	}
	echo '</tr></thead>';
	//pr($th);
	echo $viewIndex->generateColgroup($th);
	echo'<tbody>';
	$index = 0;
	$class_tr = $tr_id = '';
	if (isset($items) && count($items)>0)
		foreach ($items as $num => $item):
            /**
             * pokud je nastavena podminka pro zobrazeni tridy pro jednotlivy radek
             */
            if(isset($renderSetting['class_tr']))
                $class_tr = $fastest->set_class_for_tr($renderSetting['class_tr'],$item);
                
            /**
             * pokud je nastavena podminka pro zobrazeni id pro jednotlivy radek
             */
            if(isset($renderSetting['tr_id']) && $renderSetting['tr_id'] == true)
                $tr_id = 'id="'.$fastest->set_id_for_tr($renderSetting['tr_id'],$item).'"';   
                
			echo '<tr class="'.(($index == 1)?'od ':'').$class_tr.'" '.$tr_id.' >';
				if (!isset($renderSetting['no_checkbox'])){ 
					$checkbox_id = $data = $disabled = null;
					if (isset($renderSetting['checkbox_setting'])){ 
						if(in_array($item[$renderSetting['checkbox_setting']['model']][$renderSetting['checkbox_setting']['col']],$renderSetting['checkbox_setting']['stav_array'])){
							if((isset($renderSetting['checkbox_setting']['col2']) && $item[$renderSetting['checkbox_setting']['model']][$renderSetting['checkbox_setting']['col2']]==0) || !isset($renderSetting['checkbox_setting']['col2']))
							{
								$checkbox_id = $item[$renderSetting['checkbox_setting']['model']]['id'];
								$data =  "data[".$renderSetting['checkbox_setting']['model']."][id][".$item[$renderSetting['checkbox_setting']['model']]["id"]."]";
								if($renderSetting['checkbox_setting']['month_year']){
									$data .=  "[year][".$item[$renderSetting['checkbox_setting']['model']]['year']."]";
									$data .=  "[month][".$item[$renderSetting['checkbox_setting']['model']]['month']."]";
								}
							}
							else $disabled = "DISABLED";
						}
						else $disabled = "DISABLED";
					}

					echo '<td class="no_print"><input type="checkbox" id="checkbox_'.$checkbox_id.'" name="'.$data.'" '.$disabled.'/></td>';
				}
                $count_skip_td = 0; //pocet preskakovanych td
                $prefix = null; //prefix td, treba nejaky text v te bunce
				foreach($renderSetting['items'] as $key => $td){
				   /**
                    * nova funkce ktera slucuje td v radku
				    */ 
				   if(isset($renderSetting['items_group'])){
				        /**
				         * pri splneni podminky pouze
                         * zbytecne testovat pokud podminka neni nastavena, protoze potom colspan nema vyznam???
				         */
                        $conditon = $renderSetting['items_group']['condition'];
                        if($item[$conditon['model']][$conditon['col']] == $conditon['value']){
                    
    				        //pokud je tento sloupce zacatkem colspanu
                            if($key == $renderSetting['items_group']['start']){
                                //pokud je jiny colspan
                                if(isset($renderSetting['items_group']['colspan_data'])){
                                    $td['model'] = $renderSetting['items_group']['colspan_data']['model'];
                                    $td['col'] = $renderSetting['items_group']['colspan_data']['col'];                           
                                }
                                //pokud je nastaven nejaky prefix pro colspan td
                                if(isset($renderSetting['items_group']['text']))
                                    $prefix = $renderSetting['items_group']['text'];
                                
                                echo '<td colspan="'.$renderSetting['items_group']['count'].'" class="'.(isset($renderSetting['items_group']['css'])?$renderSetting['items_group']['css']:'').'">'.$prefix.$viewIndex->generate_td($item,$td,$this->viewVars).'</td>';
                                /**
                                 * nastaveni poctu td koliks e ma preskocit
                                 * musi tam byt -1 protoze v podminka se rovna nule!
                                 */
                                $count_skip_td = $renderSetting['items_group']['count']-1;
                            }
                            else
                                if($count_skip_td == 0)
                                    echo '<td>'.$viewIndex->generate_td($item,$td,$this->viewVars).'</td>';
                                else
                                    $count_skip_td --;   
                        }
                        //pokud podminka neni splnena tak pokracuj klasicky
                        else 
                            echo '<td>'.$viewIndex->generate_td($item,$td,$this->viewVars).'</td>';          
                   }
                   else 
					   echo '<td>'.$viewIndex->generate_td($item,$td,$this->viewVars).'</td>';
				}
				if (!empty($renderSetting['posibility']))
					echo '<td class="no_print">'.$viewIndex->generate_posibility($renderSetting,$item,$logged_user).'</td>';
			echo '</tr>';
			$index = ($index ==0)?1:0;
		endforeach;
	else
		echo '<tr><td colspan="10" style="text-align:center">Nic nebylo nenalezeno ...</td></tr>';
?>
</tbody></table>
<br class="clear" />
<?php echo '<span class="total">Celkem nalezeno: '.$paging['total'].'</span>'; ?>

<?php echo $this->renderElement('pagination',array('paging'=>$paging));?>

<script>
// START check all checkbox in row
	$('check_all').addEvent('click',function(){
		if (this.checked){
				$('tabulka_item').getElements('input[type=checkbox]').each(function(item){
					if(item.disabled==false)
						item.setProperty('checked','checked');
				});		
		}
		else
			$('tabulka_item').getElements('input[type=checkbox]').removeProperty('checked');
	});
	// END check all checkbox in row
    
if ($('items_orders')){
    var detail_order = $('DetailOrderBox');
    $('items_orders').getElements('tbody tr').removeEvents('click');
	$('items_orders').getElements('tbody tr').addEvent('click', function(e){
		new Event(e).stop();
		
        detail_order.removeClass('none');
        detail_order.setHTML('Nahravam...');
        
        basic_body_size_clients = $('items_clients').getElement('tbody').getStyle('height').toInt();
        set_height = basic_body_size_clients / 100;

        $('items_orders').getElement('tbody').setProperty('style','height:'+(set_height*40)+'px; overflow: auto;');
        $('DetailOrderBox').setProperty('style','height:'+(set_height*60)+'px; overflow: auto;');
        
        $('ActiveOrderId').value = this.getProperty('id');
        
		new Request.HTML({
			update: 'DetailOrderBox',
			url: '/orders_cast_posts/nabor_pozice/'+this.getProperty('id')+'/new_order/',
			onComplete: function(){
			}
		}).get(this.getParent('form'));
	});
}


if($('items_clients'))
$('items_clients').getElements('tbody tr .edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin',
			sizes		: <?php echo (isset($renderSetting['domwin_setting']['sizes']))?$renderSetting['domwin_setting']['sizes']:'[600,900]'?>,
			scrollbars	: <?php echo (isset($renderSetting['domwin_setting']['scrollbars']))?$renderSetting['domwin_setting']['scrollbars']:'true'?>,
			languages	: <?php echo (isset($renderSetting['domwin_setting']['languages']))?$renderSetting['domwin_setting']['languages']:'false'?>,
			defined_lang: [<?php echo implode(',',array_map(create_function('$item', 'return  \'"\'.$item.\'"\';'),array_keys($languages))); ?>],
			title		: this.title,
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false
		}); 
});
</script>
