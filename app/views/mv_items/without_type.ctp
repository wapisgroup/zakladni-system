 <fieldset>
				<legend>OPP</legend>
                <p>Vyberte kategorii, zaškrtněte jednotlivé řádky, kterým chcete 
                        danou kategorii přiřadit a stikněte <strong>Přidat kategorii</strong>.<br />
                   Po vyplnění všech OPP, uložte nastavení tlačítkem -> <strong>Uložit OPP</strong><br />
                   Ukádájí se pouze zaškrtnuté řádky, které mají vyplněný druh.    
                </p>
                <div class="sll">
                    <?php echo $htmlExt->selectTag('cat', $type_list, null,array('class' => '','label'=>'Kategorie')); ?>
                </div>
                <div class="slr">
                    <?php echo $htmlExt->button('Přidat kategorii',array('id'=>'add_cat','tabindex'=>4));?>
                </div>
                <br />
                <div id="obal_tabulky">
				<table class='table' id='rating_table'>
					<tr>
						<th><input id="check_all2" type="checkbox"/></th>
						<th>Kód</th>
						<th>Název</th>
						<th>Velikost</th>
                        <th>Druh</th>
					</tr>
					<?php 
					if (isset($without_type_list) && count($without_type_list) > 0){
						foreach($without_type_list as $item):
						?>
						<tr>
							<td><?php echo $htmlExt->checkbox('id/'.$item['OppItem']['id']);?></td>
							<td><?php echo $item['OppItem']['code'];?></td>
							<td><?php echo $item['OppItem']['name'];?></td>
                            <td><?php echo $item['OppItem']['size'];?></td>
							<td><?php echo $htmlExt->input('type/'.$item['OppItem']['id'],array('readonly'=>'readonly','class'=>'read_info'));?></td>
						</tr>
					<?php 
						endforeach;
					}
					else echo "<tr id='NoneSearch'><td>Žádné OPP bez kategorie</td></tr>"; 
					?>
					</table></div> 
				<br />
			</fieldset>
<div style="text-align: center;">    
		<?php echo $htmlExt->button('Uložit OPP',array('id'=>'saveOpp','tabindex'=>4));?>
</div>
<script>
// START check all checkbox in row
	$('check_all2').addEvent('click',function(e){
       
		if(this.checked){
				$('rating_table').getElements('input[type=checkbox]').each(function(item){
					if(item.disabled==false)
						item.setProperty('checked','checked');
				});		
		}
		else
			$('rating_table').getElements('input[type=checkbox]').removeProperty('checked');
	});
	// END check all checkbox in row
    
    //prirazeni kategorie
    $('add_cat').addEvent('click',function(e){
        var cat = $('Cat').value;
        if(cat != ''){
            $('rating_table').getElements('input[type=checkbox]').each(function(item){
                  if(item.checked){
                    item.getParent('tr').getElement('input[type=text]').value = cat;
                  }
    		});	
        }
    })    
    
    //function load_content_param(){ domtab.goTo(1); }  
	$('saveOpp').addEvent('click',function(e){
		new Event(e).stop();
			new Request.JSON({
				url:$('setting_import_edit_formular').action,		
				onComplete:function(){
					//domwin.loadContent('domwin',{'onComplete': 'load_content_param'});
                    //alert('OPP bylo nastaveno'); 
                    //click_refresh();
					//domwin.closeWindow('domwin');
				}
			}).post($('setting_import_edit_formular'));
	});
</script>            