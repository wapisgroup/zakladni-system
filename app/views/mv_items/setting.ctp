<form action="/opp_items/setting" method="post" id='form_opp'>
	<fieldset>
		<legend>Nastavení kurzu CZK/EURO</legend>
		<div class="sll">
			<?php echo $htmlExt->input('OppItem/rate',array('label'=>'Kurz','class'=>'float'));?><br/>
		</div>
		<div class="slr"></div>
		<br />
	</fieldset>
	<div class="win_save">
		<input type="button" value="Uložit" id='save_form'  class="button"/>
		<input type="button" value="Zavřít" id='close_form' class="button"/>
	</div>
</form>
<script>
	$$('.flaot, .integer').inputLimit();

	$('save_form').addEvent('click', function(e){
		e.stop();
		new Request.JSON({
			url: $('form_opp').getProperty('action'),
			onComplete: function(json){
				if (json){
					if (json.result === true){
						click_refresh();
						domwin.closeWindow('domwin');
					} else {
						alert(json.message);
					}
				} else {
					alert('Chyba aplikace');
				}
			}
		}).send($('form_opp'));
	})
	
	$('close_form').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin');
	})
</script>