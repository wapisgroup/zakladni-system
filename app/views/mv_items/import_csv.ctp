<form action='/opp_items/import_csv/' method='post' id='setting_import_edit_formular' enctype="multipart/form-data">
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
            <li class="ousko"><a href="#krok1">OPP bez kategorie</a></li>
		</ul>
</div>
    <script>
        function import_complete(file){
             add_log('Import proběhl vpořádkum soubor:'+file);
             start_upload(file);
        }
        
        function add_log(text){
            //console.log($('import').getHTML());
            $('import').setHTML($('import').getHTML()+"<br />"+text);
        }
        function load_content_param(){ domtab.goTo(1); }  
        function start_upload(file){
            new Request.JSON({
				url:'/opp_items/import_validate/'+file+'/'+$('ImportPohoda').checked,		
				onComplete:function(json){	    
                    if(json){
                        if(json.result == true){
                            add_log('Validace proběhla vpořádku, soubor se uploaduje...Čekejte');
                            new Request.JSON({
                				url:'/opp_items/import/'+file+'/'+$('ImportPohoda').checked,		
                				onComplete:function(json){
                				    if(json){
                                        if(json.result == true){
                                            add_log('Import byl úspěšný');
                                            alert('Soubor byl úspěšně importován.');
                                            if($('ImportPohoda').checked){
                                                domwin.loadContent('domwin',{'onComplete': 'load_content_param'});
                                            }
                                        }
                                        else{
                                            alert(json.message);
                                        }
                                    }      
                				}
                    		}).send();
                            
                        }
                        else{
                            alert("Soubor neni validni:\n"+json.message);
                        }
                    }
				}
			}).send();
        }
    </script>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Soubor</legend>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/opp_items/upload_file/',
								'path_to_file'	=> 'uploaded/imports/',
								'status_path' 	=> '/get_status.php',
							),
							'upload_type'	=> 'php',
							'label'			=> 'Soubor',
                            'filename_type' => 'alias',
                            'file_ext'      => array('csv'),
                            'onComplete'    => 'import_complete'
						);
					?>
                    <?php echo $htmlExt->checkbox('Import/pohoda', null, array('label'=>'Pohoda import?')); ?> <br />
    
					<?php echo $fileInput->render('Import/file',$upload_setting);?> <br class="clear">
			                </fieldset>
            <fieldset>
                <legend>Průběh importu</legend>
                <div id="import">
                    &nbsp;&nbsp;<strong>Zpracování importu:</strong> <span id="import_file">nahrajte soubor...</span><br />                    
                </div>
            </fieldset>
		</div>
        <div class="domtabs field" id="withoutTypeBox">
            <?php echo $this->renderElement('../opp_items/without_type'); ?>
        </div>
	</div>
    <div style="text-align: center;">    
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});
   	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
    </script>