<form action='/clients/messages_edit/' method='post' id='client_attachs_edit_formular'>

			<?php echo $htmlExt->hidden('ClientMessage/id');?>
			<?php echo $htmlExt->hidden('ClientMessage/client_id');?>
			<?php echo $htmlExt->hidden('Client/id',array('value'=>$this->data['ClientMessage']['client_id']));?>
			<fieldset>
				<legend>Zpráva</legend>
				<?php echo $htmlExt->input('ClientMessage/name',array('tabindex'=>1,'label'=>'Předmět','class'=>'long','label_class'=>'long'));?> <br class="clear">
        <?php echo $htmlExt->textarea('ClientMessage/text',array('tabindex'=>1,'label'=>'Text','class'=>'long','label_class'=>'long'));?> <br class="clear">
			</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
	<input type='hidden' name='render' value='<?php echo ((isset($_POST['from']))?'historie_list':'messages/index');?>' />
</form>
<script>

	
	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_message_add');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_attachs_edit_formular');
	
		<?php if (isset($_POST['from'])):?>
		var update_div =$("domwin_messages");
		<?php else :?>
		var update_div = $("domwin_messages").getElement(".CB_ImgContainer");
		<?php endif;?>
		
		if (valid_result == true){
			new Request.HTML({
				url:$('client_attachs_edit_formular').action,		
				update: update_div,
				onComplete:function(){
					domwin.closeWindow('domwin_message_add');
				}
			}).post($('client_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('client_attachs_edit_formular',{
		'ClientMessageName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		},
		'ClientMessageText': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit text'},
		}
	});
	validation.generate('client_attachs_edit_formular',<?php echo (isset($this->data['ClientMessage']['id']))?'true':'false';?>);
</script>