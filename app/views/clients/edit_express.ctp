<form action='/clients/edit_express/' method='post' id='client_edit_formular'>
	<?php echo $htmlExt->hidden('Client/id'); ?>
	<input type="hidden" name="data[NewConnection]" id="new_connection" value="0" />
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Osobní údaje</legend>
				<div class="sll">  
					<?php echo $htmlExt->input('Client/jmeno', array('tabindex' => 1, 'label' =>'Jméno')); ?><br />
					<?php echo $htmlExt->input('Client/prijmeni', array('tabindex' => 3,'label' => 'Příjmení')); ?><br />
				</div>
				<div class="slr">  
					<?php echo $htmlExt->selectTag('Client/pohlavi_list', $pohlavi_list, null, array('tabindex' => 2, 'label' => 'Pohlaví'), null, false); ?><br />

				</div>
			</fieldset>	
			<fieldset>
				<legend>Kontaktní údaje</legend>
				<div class="sll">  
                    <p>Telefony uvádějte ve formátu (00420123456789)</p>
					<?php echo $htmlExt->input('Client/mobil1', array('tabindex' => 5, 'label' => 'Telefon', 'class' => 'mobile integer', 'MAXLENGTH' => 14)); ?><br />		
					
				</div>
				<div class="slr">  

                </div>
			</fieldset>	
			<fieldset>
				<legend>Adresa trvalého bydliště</legend>
					<div class="sll" > 
                     <?php echo $htmlExt->selectTag('Client/stat_id', $client_stat_list, null, array('tabindex' => 12, 'label' => 'Stát'), null, false); ?><br />
                    </div>
					<div class="slr" >  
				    </div>
			</fieldset>
			
			
	
			<fieldset>
				<legend>Pracovní zkušenosti</legend>
				<?php if(!isset($only_show)): ?>
					<div class="sll">
						<?php echo $htmlExt->selectTag('Client/kvalifikace_list', $kvalifikace_list, null,
  array('tabindex' => 13, 'label' => 'Profese')); ?>
						<?php echo $htmlExt->input('CarrerItems/data_name', array('tabindex' => 14,
  'label' => 'Název zaměstnavatele')); ?><br />
					</div>
					<div class="slr" >  
						<?php echo $htmlExt->input('CarrerItems/data_od', array('tabindex' => 16,
  'label' => 'Období od')); ?>
						<?php echo $htmlExt->input('CarrerItems/data_do', array('tabindex' => 17,
  'label' => 'Období do')); ?><br />
						<label></label><input type="button" value="Přidat pracovní zkušenost" tabindex="18" class="button" id="add_kvalifikace" />
						
					 </div><br />
					 <?php echo $htmlExt->input('CarrerItems/data_popis', array('tabindex' =>
  15, 'class' => 'long', 'label_class' => 'long', 'label' =>
    'Popis pracovní náplně')); ?><br />
			
				<br />
				<br />
				<?php endif; ?>
					<table class="tabulka" id='kvalifikace_list'>
					<?php if(isset($kvalifikace_list_item)): ?>
						<?php foreach($kvalifikace_list_item as $kval): ?>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">	
							<td>Kvalifikace:</td>
							<td><?php echo $kval['SettingCareerItem']['name']; ?><input type='hidden' name='data[Client][kvalifikace_list_item][]' value='<?php echo
    $kval['SettingCareerItem']['id']; ?>'/></td>
							<td>
								<?php if(!isset($only_show)): ?>
									<a href='#' rel="<?php echo $kval['ConnectionClientCareerItem']['id'] ?>" class='remove_from_kvalifikace_list'>Odstranit</a>
								<?php endif; ?>
							</td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">
							<td>Název zaměstnavatele:</td>
							<td colspan="2"><?php echo $kval['ConnectionClientCareerItem']['name']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">
							<td>Období:</td>
							<td>od <?php echo $kval['ConnectionClientCareerItem']['od']; ?></td>
							<td>do <?php echo $kval['ConnectionClientCareerItem']['do']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">
							<td>Popis pracovní náplně:</td>
							<td colspan="2"><?php echo $kval['ConnectionClientCareerItem']['popis']; ?></td>
						</tr>
						<tr class="kvalifikace_row_<?php echo $kval['ConnectionClientCareerItem']['id'] ?>">
							<td colspan="3" class="line">
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/name', array('value' => $kval['ConnectionClientCareerItem']['name'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/popis', array('value' => $kval['ConnectionClientCareerItem']['popis'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/od', array('value' => $kval['ConnectionClientCareerItem']['od'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/do', array('value' => $kval['ConnectionClientCareerItem']['do'])); ?>
								<?php echo $htmlExt->hidden('ClientCarrerItems/' . $kval['ConnectionClientCareerItem']['id'] .
    '/setting_career_item_id', array('value' => $kval['SettingCareerItem']['id'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
					</table>
			</fieldset>
			
		</div>
		
	</div>
	<div class="win_save">

        <?php echo $htmlExt->button('Uložit', array('id' => 'save_close', 'class' =>  'button')); ?>
		<?php echo $htmlExt->button('Zavřít', array('id' => 'close', 'class' => 'button')); ?>
	</div>
</form>
<?php $dataPocetProfesi = (isset($kvalifikace_list_item) ? count($kvalifikace_list_item) : 0); ?>
<script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	 $('client_edit_formular').getElements('.float, .integer').inputLimit();

    $('client_edit_formular').getElements('.mobile').addEvent('change', function(e){
        e.stop();
        check_number(this.value);
     });
     
     
     // zjisteni zda je cislo jiz v db a zobrazeni seznamu u koho
     // by Sol
     function check_number(value){
        var clients_list = '';
        var i = 1;
        
        if (value == ''){
		
		} else if (value.length < 14){
			alert('Mobil je krátký - zadejte 14 znaků');
		} else {
		    new Request.JSON({
			    url:'/clients/check_number/'+value,		
				onComplete:function(json){
			     	if (json.result == true){		     	   
			     	   if(json.data != null)
                           $each(json.data,function(item){
                                clients_list += i+". "+item+"\n";
                                i++;
                           });
					   alert("Toto číslo "+value+" je již použito \nČíslo využívá :\n\n"+clients_list);
					}
				}
			}).send();		      
        }  
     }


	$$('.remove_from_kvalifikace_list').addEvent('click', remove_from_kvalifikace_list.bindWithEvent(this));
	
	/*ADD KVALIFIKACE */
	if($('add_kvalifikace'))
		$('add_kvalifikace').addEvent('click', add_kvalifikace.bindWithEvent(this));
	
	function add_kvalifikace(e){
		if (e)
			new Event(e).stop();
		var select = $('ClientKvalifikaceList');
		var rand_id = uniqid();
		
		// osetreni roku datumu
	//	if(($('CarrerItemsDataOd').value!="" && $('CarrerItemsDataOd').value.length!=4)||($('CarrerItemsDataDo').value!="" && $('CarrerItemsDataDo').value.length!=4)) 
    //  alert('Období musí mít 4 znaky');
	//	else {
    		var table = $('kvalifikace_list');
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Kvalifikace').inject(tr);
    			new Element('td').setHTML(select.getOptionText()).inject(tr);
    			var del = new Element('a',{href:'#',rel:rand_id}).inject(new Element('td').inject(tr)).setHTML('Odstranit').addEvent('click', remove_from_kvalifikace_list.bindWithEvent());
    			del.addClass('remove_from_kvalifikace_list');
    			
    			
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Název zaměstnavatele:').inject(tr);
    			new Element('td',{colspan:2}).setHTML($('CarrerItemsDataName').value).inject(tr);
    			
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Období:').inject(tr);
    			new Element('td').setHTML("od "+$('CarrerItemsDataOd').value).inject(tr);
    			new Element('td').setHTML("do "+$('CarrerItemsDataDo').value).inject(tr);
    		
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td').setHTML('Popis pracovní náplně:').inject(tr);
    			new Element('td',{colspan:2}).setHTML($('CarrerItemsDataPopis').value).inject(tr);
    		
    		var tr = new Element('tr').inject(table);
    			tr.addClass('kvalifikace_row_'+rand_id);
    			new Element('td',{colspan:3}).addClass('line').inject(tr);
    		
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataName').value,name:'data[ClientCarrerItems]['+rand_id+'][name]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataPopis').value,name:'data[ClientCarrerItems]['+rand_id+'][popis]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataOd').value,name:'data[ClientCarrerItems]['+rand_id+'][od]'}).inject(tr);
    		new Element('input',{type:'hidden',value:$('CarrerItemsDataDo').value,name:'data[ClientCarrerItems]['+rand_id+'][do]'}).inject(tr);
    		new Element('input',{type:'hidden',value:select.value,name:'data[ClientCarrerItems]['+rand_id+'][setting_career_item_id]'}).inject(tr);
    		
    		$('CarrerItemsDataName').value 	 = '';
    		$('CarrerItemsDataPopis').value  = '';
    		$('CarrerItemsDataOd').value	 = '';
    		$('CarrerItemsDataDo').value	 = '';
    		$('ClientKvalifikaceList').value = '';
			pocetProfesi++;
   //	  }
	};
	
	function remove_from_kvalifikace_list(e){
		var event=new Event(e);
		event.stop();
		var obj = event.target;
		
		if (confirm('Opravdu si přejete odstranit tuto kvalifikaci?')){
			var id = '.kvalifikace_row_'+obj.getProperty('rel');
			$('kvalifikace_list').getElements(id).dispose();
		
		}
		pocetProfesi--;
	}	 
	 
	if($('ClientStatId'))
		$('ClientStatId').ajaxLoad('/clients/load_ajax_okresy/',['ClientCountriesId']);
	
	var dataPocetProfesi = <?php echo $dataPocetProfesi; ?>;
	var pocetProfesi = ((dataPocetProfesi==0) ? 0 : dataPocetProfesi);


	if($('save_close'))
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_edit_formular');
		
		if(pocetProfesi==0) 
			if(valid_result == true)
				valid_result = Array("Musite zvolit profesi");
			else
				valid_result[valid_result.length]="Musite zvolit profesi";


		if ($('new_connection').value == 0 || confirm('Vámy zadaný klient již existuje v databázi, chcete jej přiřadit pod Vás?')){
		
			var request = new Request.JSON({ 
				url:$('client_edit_formular').action,		 
				onComplete:function(json){
					//if(json.result == false){
						//alert('fasdsa');
					//	alert(json.message);
					//}
					//else{
						
						click_refresh($('ClientId').value); 
						domwin.closeWindow('domwin');
                        button_preloader_disable($('save_close')); 
					//}
				}
			});
			
			if (valid_result == true){
                button_preloader($('save_close'));
				request.post($('client_edit_formular'));
			} else {
				var error_message = new MyAlert();
				error_message.show(valid_result)
			}
		}
	});
	
	$$('.integer, .float').inputLimit();	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('client_edit_formular',{
		'ClientJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		},
		'ClientMobil1': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit mobil'},
			'length': {'condition':{'min':14,'max':14}, 'err_message':'Mobil musí mít 14 znaků'},		
		},
		'ClientPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'},
		}
	});
	validation.generate('client_edit_formular',<?php echo (isset($this->data['Client']['id'])) ? 'true' : 'false'; ?>);
	
</script>