<?php
// pr($history_item_list);
?>
<div class="sll" style="margin: 10px 0 0 10px;">
<a href='/clients/add_activity/<?php echo (isset($this->data['Client']['id']) ? $this->data['Client']['id'] : $client_id);?>' class='button edit add_activity' title='Přidání novou aktivitu'>Přidat aktivitu</a>
</div>				 
<div class="slr noprint">
<?php echo $htmlExt->selectTag('category_id2', $history_category, null,array('tabindex' => 1, 'label' =>'Kategorie')); ?><br />		
</div>
<br /><br />
<div id="history_items2">
<?php echo $this->renderElement('../clients/activity/items'); ?>
</div>

<?php if(isset($client_id)) echo "<input type='hidden' id='ClientId2' value='".$client_id."'/>"; ?>

<script>

     $('CategoryId2').addEvent('change',function(e){
        var client_id = null;
        
        if($('ClientId') && $('ClientId').value != '')
            client_id = $('ClientId').value;
        else if($('ClientId2'))
            client_id = $('ClientId2').value;    
        
		new Event(e).stop();
		   new Request.HTML({
			    url:'/clients/load_client_activity/'+ client_id +'/'+this.value+'/items/',
                update: 'history_items2',		
				onComplete:function(){
				}
			}).send();
	});
    
    $$('.add_activity').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_message_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			post_data	: {from:'editace'},
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
      
    
</script>