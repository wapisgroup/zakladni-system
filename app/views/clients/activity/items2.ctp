<?php
//pr($history_item_list);

if(function_exists("create_data") !== true){
function create_data($data,$htmlExt){
    $datas = $data['HistoryData'];
    
    $i = 1;
    echo "<tr>";
    
    foreach($datas as $key=>$item){
         echo "<td>"; 
            echo $htmlExt->var_text('',array('class'=>(isset($last_item['HistoryData'][$key]) && $item['value'] != $last_item['HistoryData'][$key]['value'] ? 'color_blue' : ''),'label_class'=>'strong','label'=>$item['caption'],'value'=>$item['value']));    
         echo "</td>";     

         if(($i % 4) == 0){ 
            echo "</tr><tr>";
            $i = 0;
         }
            
         $i++;
    }
    
    echo "</tr>";
}
}
?>
<table class='scroll_table'>
    <thead>
	<tr>
        <th width="125px;">Vytvořeno</th>
		<th width="125px;">Uživatel</th>
		<th width="125px;">Kategorie</th>
		<th>Akce</th>
		<th></th>
		<!-- <th>Detail</th> -->
	</tr>
    </thead>
   
           
    <tbody>
     
    
	<?php 
    //pr($history_item_list);
    if (isset($history_item_list) && count($history_item_list) > 0):?>
		<?php foreach($history_item_list as $msg):
        
            if(in_array($msg['HistoryItem']['action_id'],$edit_action_ids)){
            ?>
    		<tr style="background-color:#EBEBED;" id="<?php echo $msg['HistoryItem']['id'];?>">
                <td><?php echo $fastest->czechDateTime($msg['HistoryItem']['created']);?></td>
    			<td><?php echo $msg['CmsUser']['name'];?></td>
    			<td><?php echo $history_category[$msg['HistoryItem']['category_id']];?></td>
    			<td><?php echo $msg['HistoryType']['name'];?></td>
                <td>
                    <?php if(isset($permission['delete_activity']) && $permission['delete_activity'] == 1){?>
                    <a class="ta trash" href="/clients/activity_delete/<?php echo $msg['HistoryItem']['id'];?>">Smazat</a>
                     <?php } ?>
                </td>		
    		</tr>
            <tr>
                <td colspan="4">
                    <table class="stop_height">
                        <?php create_data($msg,$htmlExt);?> 
                    </table>
                </td>
            </tr>
            
    		<?php 
            }
        endforeach;?>
	<?php endif;?>
    
    <!-- Zpráva o vytvoření  -->
    	<tr style="background-color:#EBEBED;">
            <td><?php echo $fastest->czechDate($this->data['Client']['created']);?></td>
			<td>systém</td>
			<td>Info</td>
			<td>Přidání klienta</td>	
		</tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
    </tbody>
</table> 
<script>
if($('history_items2') && $('history_items2').getElements('tr')){    
    $('history_items2').getElements('a.trash').addEvent('click',function(e){
		new Event(e).stop();
        if(confirm("Opravdu chcete smazat tuto aktivitu?")){
            new Request.JSON({
    			    url:this.href,	
    				onComplete:(function(){
    				    tr = this.getParent('tr');
                        id = tr.getProperty('id');
                        
    				    if(this.getParent('tbody').getElement('tr[id="data_'+id+'"]')){
    				        this.getParent('tbody').getElement('tr[id="data_'+id+'"]').dispose();
    				    }
                        tr.dispose();
                        
    				}).bind(this)
    		}).send();
        }
	});
}
</script>
