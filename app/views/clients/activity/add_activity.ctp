<form action='/clients/add_activity/' method='post' id='client_add_activity_form'>
	<?php echo $htmlExt->hidden('client_id',array('value'=>$client_id));?>
	<?php echo $htmlExt->hidden('hodnoceni',array('value'=>$hodnoceni));?>
    <?php echo $htmlExt->hidden('from_type',array('value'=>$from_type));?>
    <?php echo $htmlExt->hidden('cl_name',array('value'=>$cl_name));?>
    <?php echo $htmlExt->hidden('from_ccr_id',array('value'=>$from_ccr_id));?>
	<fieldset>
		<legend>Aktivita</legend>
		<?php echo $htmlExt->selectTag('kind', $kind_of_client_activity, null, array('class'=>'long','label_class'=>'long','tabindex' => 1, 'label' => 'Druh aktivity'), null, false); ?><br />
		        <div id="kn_box" class="">
                        <strong style='color:red; text-align:center; display:block;'>
                    		Poznámka: Pokud chcete přidat aktivitu klientu kterého jste přidali na ČL, musíte vybrat ke které objednávce aktivitu tvoříte. 
                    	</strong>
                        <?php echo $htmlExt->selectTag('forma_kontaktu', $forma_kontaktu_coo_list, null, array('class'=>'long','label_class'=>'long','tabindex' => 1, 'label' => 'Forma kontaktu')); ?><br />
                        <?php echo $htmlExt->selectTag('ccr_id', $ccr_list, null, array('class'=>'long','label_class'=>'long','tabindex' => 1, 'label' => 'Objednávka')); ?><br />
                </div>
                <div id="pomucky_box" class="none">
                        <?php echo $htmlExt->selectTag('pomucky', $activity_pomucky_list, null, array('class'=>'long','label_class'=>'long','tabindex' => 1, 'label' => 'Přidělení pracovních pomůcek'), null, false); ?><br />
                </div>
                <div id="problem_box" class="none">
                        <?php echo $htmlExt->selectTag('problem', array(), null, array('class'=>'long','label_class'=>'long','tabindex' => 2, 'label' => 'Problémy'), null, false); ?><br />
                        <?php echo $htmlExt->inputDate('datum', array('class'=>'','label_class'=>'long','tabindex' => 3, 'label' => 'Datum')); ?><br />
                </div>
                <div id="text_box" class="">
                        <?php echo $htmlExt->textarea('text',array('tabindex'=>33,'label'=>'Zpráva','class'=>'long','label_class'=>'long'));?> <br class="clear">
                </div>
		<div id="blacklist_box" class="">
			<fieldset>
				<legend>Hodnocení klienta</legend>
				<div class="sll" >  
					<?php echo $htmlExt->selectTag('ClientRating/kvalita_prace', $hodnoceni_list2, null, array('tabindex'=>3,'label'=>'Kvalita práce'),null,false);?>
					<?php echo $htmlExt->selectTag('ClientRating/spolehlivost', $hodnoceni_list2, null, array('tabindex'=>5,'label'=>'Docházka'),null,false);?>
					<?php echo $htmlExt->selectTag('ClientRating/zamestnan', $ano_ne_list, null, array('tabindex'=>7,'label'=>'Byl už zaměstnán v AT?'),null,false);?>
							
				</div>
				<div class="slr" >  
					<?php echo $htmlExt->selectTag('ClientRating/alkohol', $hodnoceni_list3, null, array('tabindex'=>4,'label'=>'Má problémy s alkoholem'),null,false);?><br />
					<?php echo $htmlExt->selectTag('ClientRating/blacklist', $doporuceni_pro_nabor, null, array('tabindex'=>6,'label'=>'Doporučení pro nábor'),null,false);?>
					
				</div><br />
				<?php echo $htmlExt->textarea('ClientRating/text',array('tabindex'=>8,'label'=>'Komentář k hodnocení','class'=>'long','label_class'=>'long'));?> <br class="clear">
			 </fieldset>
       </div>
    </fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>
    $('blacklist_box').addClass('none');
    var allow_activity_for_coo = ['1','2','4','7'];
    /**
     * zmeny druhy aktivity
     */ 
    $('Kind').ajaxLoad('/misconducts/load_problem_list/',['Problem']);  
    $('Kind').addEvent('change',function(e){
        new Event(e).stop(); 

        if(this.value == 7){
            $('FormaKontaktu').addClass('require').addClass('invalid').removeClass('valid');
        }
        else{
            $('FormaKontaktu').removeClass('require').removeClass('invalid').addClass('valid');
        }

        /**
         * při druhu zaměstnání se volá novy typ activity
         * Kategorie : Zamestnani
         * akce : prideleni pracovnich pomucek
         * kvuli zjednoduseni je zmena action, pro odchyceni history komponentou
         **/
        if(this.value == 5){
            if($('Hodnoceni').value == '0'){
                alert('Klient nemá žádné hodnocení, daný klient nebyl nejspíše vůbec zaměstnán!');
                this.value = 1;
            }
            else {
		        $('blacklist_box').addClass('none');
                $('problem_box').removeClass('none');
                $('kn_box').addClass('none');
                $('Text').removeClass('require').removeClass('invalid').addClass('valid');
                set_problem_box_to('invalid');
            }    
        } else if (this.value == '6'){
			// pokud je blacklist
		    $('blacklist_box').removeClass('none');
            $('problem_box').addClass('none');
            $('kn_box').addClass('none');
    		set_problem_box_to('valid');
    		$('Text').removeClass('require').removeClass('invalid').addClass('valid');
    	    $('text_box').addClass('none');
        }
        else if (allow_activity_for_coo.contains(this.value)){
			// pokud je coo
		    $('kn_box').removeClass('none');
            $('problem_box').addClass('none');
            $('blacklist_box').addClass('none');
            set_problem_box_to('valid');
            $('text_box').removeClass('none');
            $('Text').removeClass('valid').addClass('invalid');
        }
        else{
            $('kn_box').addClass('none');
            $('problem_box').addClass('none');
            $('blacklist_box').addClass('none');
            set_problem_box_to('valid');
            $('text_box').removeClass('none');
            $('Text').removeClass('valid').addClass('invalid');
        }
    }); 
        
    function set_problem_box_to(type){
        if(type == 'valid')
           $('problem_box').getElements('input,select').removeClass('require').removeClass('invalid').addClass('valid');
        else
            $('problem_box').getElements('input,select').removeClass('valid').addClass('invalid');
    }
    
    
	
	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_message_add');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_add_activity_form');
		
		if (valid_result == true){
		    button_preloader($('AddEditClientMessageSaveAndClose'));
			new Request.HTML({
				url:$('client_add_activity_form').action,
                <?php if(isset($this->data['from']) && $this->data['from'] == 'editace'){?>
                    update: 'history_items',		
                <?php } else if(isset($update)){?>
                    update: '<?php echo $update;?>',		
                <?php }?>    
				onComplete:function(){
					domwin.closeWindow('domwin_message_add');
                    button_preloader_disable($('AddEditClientMessageSaveAndClose'));
				}
			}).post($('client_add_activity_form'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});

	validation.define('client_add_activity_form',{
		'Text': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit text'},
		},
        'Problem': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit problem'},
		},
        'FormaKontaktu': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit formu kontaktu'},
		},
        'calbut_Datum': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum'},
		}
	});

	validation.generate('client_add_activity_form',false);
    set_problem_box_to('valid'); // odzacatku je valid
    $('FormaKontaktu').removeClass('require').removeClass('invalid').addClass('valid');
</script>