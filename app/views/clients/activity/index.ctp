<?php
// pr($history_item_list);
?>
<div class="sll" style="margin: 10px 0 0 10px;">
<a href='/clients/add_activity/<?php echo $this->data['Client']['id'];?>' class='button edit add_activity' title='Přidání novou aktivitu'>Přidat aktivitu</a>
</div>				 
<div class="slr noprint">
<?php echo $htmlExt->selectTag('category_id', $history_category, null,array('tabindex' => 1, 'label' =>'Kategorie')); ?><br />		
</div>
<br /><br />
<div id="history_items">
<?php echo $this->renderElement('../clients/activity/items'); ?>
</div>

<script>
	    
     $('CategoryId').addEvent('change',function(e){
		new Event(e).stop();
		   new Request.HTML({
			    url:'/clients/load_client_activity/'+ $('ClientId').value +'/'+this.value,
                update: 'history_items',		
				onComplete:function(){
				}
			}).send();
	});
      
</script>