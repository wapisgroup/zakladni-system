
<?//print_r($this->logged_user);?>
<form action='/clients/rating_add/<?php echo $client_id;?>' method='post' id='client_attachs_edit_formular'>
<fieldset>
	<legend>Hodnocení klienta</legend>
		<div class="sll" >  
			<?php echo $htmlExt->selectTag('ClientRating/kvalita_prace', $hodnoceni_list2, null, array('label'=>'Kvalita práce'),null,false);?>
			<?php echo $htmlExt->selectTag('ClientRating/spolehlivost', $hodnoceni_list2, null, array('label'=>'Docházka'),null,false);?>
						
		</div>
		<div class="slr" >  
			<?php echo $htmlExt->selectTag('ClientRating/alkohol', $hodnoceni_list3, null, array('label'=>'Má problémy s alkoholem'),null,false);?>
			<?php echo $htmlExt->selectTag('ClientRating/blacklist', $doporuceni_pro_nabor, null, array('label'=>'Černá listina'),null,false);?>
			
		</div>
        <br />
		<?php echo $htmlExt->textarea('ClientRating/text',array('tabindex'=>8,'label'=>'Komentář k hodnocení','class'=>'long','label_class'=>'long'));?> <br class="clear">

</fieldset>
<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>

	
	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_rating_add');});

	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_attachs_edit_formular');
		
		if (valid_result == true){
			new Request.JSON({
				url:$('client_attachs_edit_formular').action,		
				//update: $("domwin"),
				onComplete:function(json){
					tr = new Element('tr').inject($('rating_table'));
					new Element('td').inject(tr).setHTML('<?php echo $logged_user_name;?>');
					new Element('td').inject(tr).setHTML($('ClientRatingKvalitaPrace').options[$('ClientRatingKvalitaPrace').selectedIndex].getHTML());
					new Element('td').inject(tr).setHTML($('ClientRatingSpolehlivost').options[$('ClientRatingSpolehlivost').selectedIndex].getHTML());
					new Element('td').inject(tr).setHTML($('ClientRatingAlkohol').options[$('ClientRatingAlkohol').selectedIndex].getHTML());
					new Element('td').inject(tr).setHTML($('ClientRatingBlacklist').options[$('ClientRatingBlacklist').selectedIndex].getHTML());
					new Element('td').inject(tr).setHTML('<?php echo date('d.m.Y');?>');
					$('rating_add_new').addClass('none');
					if($('NoneSearch')!=null) $('NoneSearch').addClass('none');
					domwin.closeWindow('domwin_rating_add');
				}
			}).post($('client_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('client_attachs_edit_formular',{
		'ClientMessageName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		},
		'ClientMessageText': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit text'},
		}
	});
	validation.generate('client_attachs_edit_formular',<?php echo (isset($this->data['ClientMessage']['id']))?'true':'false';?>);
</script>