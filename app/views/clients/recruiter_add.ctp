<form action='/clients/recruiter_add/<?php echo $client_id;?>' method='post' id='client_attachs_edit_formular'>
	<fieldset>
			<legend>Recruiter</legend>
					<?php echo $htmlExt->selectTag('ConnectionClientRecruiter/cms_user_id', $cms_user_list, null, array('tabindex'=>23,'label'=>'Recruiter'));?> <br class="clear">
			</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
	<input type='hidden' name='render' value='<?php echo ((isset($_POST['from']))?'historie_list':'messages/index');?>' />
</form>
<script>

	
	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_recruiter_add');});
	
	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_attachs_edit_formular');
		
		if (valid_result == true){
			new Request.JSON({
				url:$('client_attachs_edit_formular').action,		
				//update: $("domwin"),
				onComplete:function(json){
					tr = new Element('tr').inject($('recruiter_table'));
					new Element('td').inject(tr).setHTML(json.CmsUser.name);
					new Element('td').inject(tr).setHTML('<?php echo date('d.m.Y');?>');
					domwin.closeWindow('domwin_recruiter_add');
				}
			}).post($('client_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('client_attachs_edit_formular',{
		'ClientMessageName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		},
		'ClientMessageText': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit text'},
		}
	});
	validation.generate('client_attachs_edit_formular',<?php echo (isset($this->data['ClientMessage']['id']))?'true':'false';?>);
</script>