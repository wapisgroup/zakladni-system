<div class="win_fce top">
<a href='/clients/rating_add/<?php echo $this->data['Client']['id'];?>' class='button edit' id='rating_add_new' title='Přidání hodnocení'>Přidat hodnocení</a>
</div>
<table class='table' id='rating_table'>
	<tr>
		<th>Přidal</th>
		<th>Kvalita práce</th>
		<th>Docházka</th>
		<th>Má problémy s alkoholem?</th>
		<th>Doporučení pro nábor</th>
		<th>Přidáno</th>
		<th></th>
	</tr>
	<?php 

    $kvalita = Set::extract($ratings,'{n}.ClientRating.kvalita_prace');
    $spolehlivost = Set::extract($ratings,'{n}.ClientRating.spolehlivost');
    $alkohol = Set::extract($ratings,'{n}.ClientRating.alkohol');
    $blacklist = Set::extract($ratings,'{n}.ClientRating.blacklist');
    rsort($blacklist);
    
	if (isset($ratings) && count($ratings) > 0){
		foreach($ratings as $rating):?>
		<tr>
			<td><?php echo $rating['CmsUser']['name'];?></td>
			<td><?php echo $hodnoceni_list2[$rating['ClientRating']['kvalita_prace']];?></td>
			<td><?php echo $hodnoceni_list2[$rating['ClientRating']['spolehlivost']];?></td>
			<td><?php echo $hodnoceni_list3[$rating['ClientRating']['alkohol']];?></td>
			<td><?php echo $doporuceni_pro_nabor[$rating['ClientRating']['blacklist']];?></td>
			<td><?php echo $fastest->czechDate($rating['ClientRating']['created']);?></td>
			<td><a href="#" class="display_comment" rel="<?php echo $rating['ClientRating']['id'];?>">Komentář</a></td>
		</tr>
        <tr id="<?php echo $rating['ClientRating']['id'];?>" class="comment"><td>Komentář</td><td colspan="6"><?php echo $rating['ClientRating']['text'];?></td></tr>
	<?php 
		endforeach;
        
       echo " <tr><td colspan=7>&nbsp;</td></tr>
            <tr>
                <td>Celkem</td>
                <td>".$fastest->array_avg($kvalita)."</td>
                <td>".$fastest->array_avg($spolehlivost)."</td>
                <td>".$fastest->array_avg($alkohol)."</td>
                <td>".$doporuceni_pro_nabor[$blacklist[0]]."</td>
                <td colspan = '2'></td>
            </tr>";    
	}
	else echo "<tr id='NoneSearch'><td></td>Žádne hodnocení nenalezeno</td></tr>"; 
	?>
</table> 
<script>
	var addRatingButton = <?php echo (isset($MyRating) && $MyRating!="") ? 'true' : 'false';?>;
	if(addRatingButton) // pokud je uz dany uzivatel pridal tomuto client_id hlasovani skryj tl. addd_rating
		$('rating_add_new').addClass('none');
	
    //zobrazeni a skryvani komentare
    $('rating_table').getElements('.comment').each(function(item){	
        item.addClass('none');  
    });  
        //po kliknuti
       	$$('.display_comment').addEvent('click',function(e){
       	    new Event(e).stop();
            item = $(this.rel);
            if(item.hasClass('none'))
                item.removeClass('none');
            else  
                item.addClass('none');  
            return false;
        });
    
	$('rating_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_rating_add',
			sizes		: [800,300],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>