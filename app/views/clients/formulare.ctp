    <fieldset class="none">
		<legend>Výběr formulářové šablony</legend>
			<?php echo $htmlExt->selectTag('form_template',$form_template_list,null,array('label'=>'Šablony','class'=>'long','label_class'=>'long'));?><br/>					
	</fieldset>
<br />
<div >
<table class='table' id="form_list">
	<tr>
		<th>Formulář</th>
		<th>Firma</th>
		<th>Stav</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php 
	if (isset($client_form_list) && count($client_form_list) > 0){
		foreach($client_form_list as $form):?>
		<tr>
			<td><?php echo $form['FormData']['name'];?></td>
			<td><?php echo $form['Company']['name'];?></td>
            <td><?php echo $form['FormData']['history'] == 1 ? 'Archivováno' : 'Aktivní'; ?></td>
			<td><?php echo $fastest->czechDate($form['FormData']['created']);?></td>
			<td>
				<?php echo $html->link('Editovat','/form_templates/edit_client_form/'.$form['FormData']['id'],array('class'=>'edit_client_form'));?> 
				<?php //echo $html->link('Smazat','/form_templates/delete_client_form/'.$form['FormData']['id'],array('class'=>'del_client_form'));?> 
				<?php echo $html->link('Tisknout','/form_templates/print_client_form/'.$form['FormData']['id'],array('class'=>'print_client_form','target'=>'_blank'));?>
			</td>
		</tr>
	<?php 
		endforeach;
	}
	else echo "<tr id='NoneSearch'><td></td>Žádne formuláře nenalezeny</td></tr>"; 
	?>
</table>

    <style>
        .plus_point,
        .minus_point,
        .non_point {
            display: inline-block; width: 12px; height: 12px; float: left
        }
        .plus_point {background: url("http://framework.wapis.cz/css/other/menu/plus.gif") repeat scroll 0 0 }
        .minus_point {background: url("http://framework.wapis.cz/css/other/menu/minus.gif") repeat scroll 0 0}
        .non_point {background: url("http://framework.wapis.cz/css/other/menu/normal.gif") repeat scroll 0 0}

        .frmul li {margin:5px 0px;}
    </style>

<fieldset id="FormDocument">

<ul class="frmul">
    <? if(Count($firstLevel) > 0): ?>
    <? foreach($firstLevel as $id=>$name): ?>
    <li class="first_level">
       <? if(isset($secondLevel[$id]) && Count($secondLevel[$id]) > 0): ?>
            <span class="plus_point"></span>
       <?php else:?>
            <span class="normal_point"></span>
       <?php endif;?>
       <?=  ' '.$htmlExt->checkbox('ClientFormDocument/'.$id, null ,array( 'value'=>1 )).' '.$name; ?>

       <? if(isset($secondLevel[$id]) && Count($secondLevel[$id]) > 0): ?>
       <ul class='none'>
            <? foreach($secondLevel[$id] as $id2=>$name2): ?>

            <li class="second_level">
                <? if(isset($thirdLevel[$id2]) && Count($thirdLevel[$id2]) > 0): ?>
                    <span class="plus_point"></span>
                <?php else:?>
                    <span class="non_point"></span>
                <?php endif;?>
                <?= $htmlExt->checkbox('ClientFormDocument/'.$id2, null ,array( 'value'=>1 )).' '.$name2; ?>

            <? if(isset($thirdLevel[$id2]) && Count($thirdLevel[$id2]) > 0): ?>
                <ul class='none'>
                <? foreach($thirdLevel[$id2] as $id3=>$name3): ?>

                   <li class="third_level"><span class="non_point"></span><?=  $htmlExt->checkbox('ClientFormDocument/'.$id3, null ,array( 'value'=>1 )).' '.$name3; ?></li>

                <? endforeach; ?>
                </ul>
            <? endif; ?>

        <? endforeach; ?>
            </li>
       </ul>
    <? endif; ?>
   </li>
<? endforeach; ?>
 <? endif; ?>
</ul>
</fieldset>
<style>
#FormDocument{
    padding-left: 30px;

}
#FormDocument input{
    margin:0px;
}
.second_level{
    margin-left:15px;
   }
.third_level{
    margin-left:30px;
}
#FormDocument ul {margin-left: 20px;}
</style>
</div>
<script>

    $$('.plus_point').addEvent('click',function(){
        if (this.hasClass('plus_point')){
            this.getNext('ul').removeClass('none');
            this.removeClass('plus_point').addClass('minus_point');
        } else if (this.hasClass('minus_point')){
            this.getNext('ul').addClass('none');
            this.addClass('plus_point').removeClass('minus_point');
        }
    });

	$('FormTemplate').addEvent('change',function(e){
		new Event(e).stop();

		if(this.value != ''){
			domwin.newWindow({
				id			: 'domwin_forms',
				sizes		: [1000,1000],
				scrollbars	: true,
				title		: 'Šablona - '+ this.options[this.selectedIndex].title,
				languages	: false,
				type		: 'AJAX',
				ajax_url	: '/form_templates/generate_form/'+this.value +'/'+ $('ClientId').value,
				closeConfirm: true,
				max_minBtn	: false,
				modal_close	: false,
				remove_scroll: false
			}); 
		}
	});

	$$('.del_client_form').addEvent('click',function(e){
		new Event(e).stop();

		if (confirm('Opravdu si přejet smazat tento formulář?')){
			new Request.HTML({
				url:this.href,
				onComplete: (function(){
					this.getParent('tr').dispose();
				}).bind(this)
			}).send();
		}

	});


	$$('.edit_client_form').addEvent('click',function(e){
		new Event(e).stop();

		domwin.newWindow({
				id			: 'domwin_edit_forms',
				sizes		: [1000,1000],
				scrollbars	: true,
				title		: 'Editace - formuláře ',
				languages	: false,
				type		: 'AJAX',
				ajax_url	: this.href,
				closeConfirm: true,
				max_minBtn	: false,
				modal_close	: false,
				remove_scroll: false
		}); 


	});


</script>	