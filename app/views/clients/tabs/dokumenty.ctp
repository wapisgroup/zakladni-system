<fieldset>
			<legend>Dokumenty</legend>
            <div class="noprint">
		<?php if(!isset($only_show)): ?>
			 <div class="sll">
					<?php echo $htmlExt->selectTag('ClientDocument/client_document_id', $documents_list, null,  array('tabindex' => 19, 'label' => 'Dokument')); ?>
					<?php echo $htmlExt->inputDate('ClientDocument/datum', array  ('tabindex' => 21, 'label' => 'Datum')); ?>
        	 </div>
			 <div class="slr" >  
					<label></label><input type="button" value="Přidat nový dokument" class="button" tabindex="22" id="add_client_document" />
			 </div><br />
			 <?php echo $htmlExt->input('ClientDocument/komentar', array('tabindex' =>  20, 'class' => 'long', 'label_class' => 'long', 'label' => 'Komentář')); ?><br />
			
			<br />
			<br />
            </div>
		<?php endif; ?>
				<table class="tabulka" id='client_documents_list'>
				    <?php if(isset($client_documents_list)): ?>
						<?php foreach($client_documents_list as $item): ?>
						<tr class="kvalifikace_row_<?php echo $item['ClientDocumentItem']['id'] ?>">	
							<td style="width:65px;">Typ:</td>
							<td style="width:30%"><?php echo $item['ClientDocument']['name']; ?></td>
							<td style="width:65px;"></td>
							<td>
								<?php if(!isset($only_show)): ?>
									<a href='#' rel="<?php echo $item['ClientDocumentItem']['id'] ?>" class='remove_from_documents_list'>Odstranit</a>
								<?php endif; ?>
							</td>
						</tr>
						
                        <tr class="kvalifikace_row_<?php echo $item['ClientDocumentItem']['id'] ?>">
						    <td style="width:65px;">Komentář:</td>
							<td><?php echo $item['ClientDocumentItem']['komentar']; ?></td>
						    <td style="width:65px;">Datum:</td>
							<td><?php echo $fastest->czechDate($item['ClientDocumentItem']['datum']); ?></td>
                        </tr>
                        
						<tr class="kvalifikace_row_<?php echo $item['ClientDocumentItem']['id'] ?>">
							<td colspan="4" class="line">
								<?php echo $htmlExt->hidden('ClientDocumentItem/' . $item['ClientDocumentItem']['id'] .    '/komentar', array('value' => $item['ClientDocumentItem']['komentar'])); ?>
								<?php echo $htmlExt->hidden('ClientDocumentItem/' . $item['ClientDocumentItem']['id'] .    '/datum', array('value' => $item['ClientDocumentItem']['datum'])); ?>
								<?php echo $htmlExt->hidden('ClientDocumentItem/' . $item['ClientDocumentItem']['id'] .    '/client_document_id', array('value' => $item['ClientDocumentItem']['client_document_id'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</table>
</fieldset>
<script>
/*Dokumenty - pridani/mazani */
	if($('add_client_document'))
	$('add_client_document').addEvent('click', function(e){
		new Event(e).stop();
		var select = $('ClientDocumentClientDocumentId');
		var rand_id = uniqid();
		
		var table = $('client_documents_list');
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td').setHTML('Typ').inject(tr);
			new Element('td').setHTML(select.getOptionText()).inject(tr);
			new Element('td').setHTML('').inject(tr);
			var del = new Element('a',{href:'#',rel:rand_id}).inject(new Element('td').inject(tr)).setHTML('Odstranit').addEvent('click', remove_from_documents_list.bindWithEvent());
			del.addClass('remove_from_documents_list');
			
			
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td').setHTML('Komentář:').inject(tr);
			new Element('td').setHTML($('ClientDocumentKomentar').value).inject(tr);
			new Element('td').setHTML('Datum:').inject(tr);
            date = $('calbut_ClientDocumentDatum').value.split('-');
			new Element('td').setHTML(date[2]+'.'+date[1]+'.'+date[0]).inject(tr);
		
		var tr = new Element('tr').inject(table);
			tr.addClass('kvalifikace_row_'+rand_id);
			new Element('td',{colspan:4}).addClass('line').inject(tr);
		
		new Element('input',{type:'hidden',value:$('ClientDocumentKomentar').value,name:'data[ClientDocumentItem]['+rand_id+'][komentar]'}).inject(tr);
		new Element('input',{type:'hidden',value:$('calbut_ClientDocumentDatum').value,name:'data[ClientDocumentItem]['+rand_id+'][datum]'}).inject(tr);
		new Element('input',{type:'hidden',value:$('ClientDocumentClientDocumentId').value,name:'data[ClientDocumentItem]['+rand_id+'][client_document_id]'}).inject(tr);
	
        $('ClientDocumentKomentar').value = $('calbut_ClientDocumentDatum').value = $('ClientDocumentClientDocumentId').value = '';
	});
	
	
	function remove_from_documents_list(e){
		var event=new Event(e);
		event.stop();
		var obj = event.target;
		
		if (confirm('Opravdu si přejete odstranit tento dokument?')){
			var id = '.kvalifikace_row_'+obj.getProperty('rel');
			$('client_documents_list').getElements(id).dispose();
		
		}
	}
    
    $$('.remove_from_documents_list').addEvent('click', remove_from_documents_list.bindWithEvent(this));

</script>            