<fieldset>
	<legend>Číslo účtu</legend>
		<?php echo $htmlExt->input('Client/Cu/prefix', array('MAXLENGTH' => 6,'class'=>'short integer','label_class'=>'small','tabindex' => 4,'label' => 'Číslo účtu', 'disabled'=>(($add_ucet || $edit_ucet? false : 'disabled')))); ?><label class='label_short'>-</label>
		<?php echo $htmlExt->input('Client/Cu/cislo_uctu', array('MAXLENGTH' => 10,'class'=>'medium integer','tabindex' => 4,'disabled'=>(($add_ucet || $edit_ucet? false : 'disabled')))); ?><label class='label_short'>/</label>
		<?php echo $htmlExt->input('Client/Cu/kod_banky', array('MAXLENGTH' => 4,'class'=>'short integer','tabindex' => 4, 'disabled'=>(($add_ucet || $edit_ucet? false : 'disabled')))); ?><br />
		                    
        <label>Zkontrolováno</label><?php echo $htmlExt->checkbox('Client/cislo_uctu_control', null, array('disabled'=>(($edit_ucet ? false : 'disabled')))); ?><br/>

</fieldset>
<script>

    if($('ClientId').value != '' && $('OldCisloUctu').value != ''){
       $('ClientCuPrefix').addEvent('change',change_cu);
       $('ClientCuCisloUctu').addEvent('change',change_cu);
       $('ClientCuKodBanky').addEvent('change',change_cu);
    }

    function change_cu(){
        $('NewCisloUctu').value = $('ClientCuPrefix').value+'-'+$('ClientCuCisloUctu').value+'/'+$('ClientCuKodBanky').value;
        if($('NewCisloUctu').value != $('OldCisloUctu').value)
            $('ChangeCisloUctuNow').value = 1;
    }
</script>
<div id="client_documents">
</div>