
<table class='table dotaznik'>
  
	<tr>
      <th style='width:140px'>Příjmení, jméno, tituly:</th>
		<td colspan="3">
			<?php echo $htmlExt->input('Client/dotaznik/jmeno_prijmeni_titul');?>
		</td>
	</tr>
	<tr>
      <th>Rodné příjmení::</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/psc');?></td>
      <th>Další předchozí příjmení::</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/trvale_bydliste_cislo');?></td>
	</tr>
	<tr>
      <th>Datum narození:</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/psc');?></td>
      <th>Místo narození:</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/trvale_bydliste_cislo');?></td>
	</tr>  
	<tr>
      <th>Rodné číslo:</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/psc');?></td>
      <th>Státní příslušnost:</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/trvale_bydliste_cislo');?></td>
	</tr>
	<tr>
      <th style='width:140px'>Trvalé bydliště:</th>
		<td colspan="3">
			<?php echo $htmlExt->input('Client/dotaznik/jmeno_prijmeni_titul');?>
		</td>
	</tr>
	<tr>
		<th style='width:140px'>PSČ:</th>
		<td colspan="3">
			<?php echo $htmlExt->input('Client/dotaznik/jmeno_prijmeni_titul');?>
		</td>
	</tr>
	<tr>
      <th>Telefonické spojení:</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/psc');?>
		<?php echo $htmlExt->input('Client/dotaznik/psc');?></td>
      <th>e-mail:</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/trvale_bydliste_cislo');?></td>
	</tr>
</table>


<h3><strong>Rodinní příslušníci</strong></h3>
<table class='table dotaznik'>
	<tr>
		<th style='width:440px'>Rodinný stav (pro registr pojištěnců-zák.o org.a provádění.soc.zabezp.):</th>
		<td>
			<?php echo $htmlExt->input('Client/dotaznik/jmeno_prijmeni_titul');?>
		</td>
	</tr>
	<tr>
		<th style='width:440px'>Děti (pro účely prac.právní a účely důch. pojištění): </th>
		<td>
			<?php echo $htmlExt->input('Client/dotaznik/jmeno_prijmeni_titul');?>
		</td>
	</tr>
	<tr>
		<th>Jméno:</th>
		<th>Datum narození (pro plátce ZP např. v příp. NV):</th>
	</tr>
  
<?php for ($i=1; $i<=4; $i++):?>  
    <tr>
      <td><?php echo $htmlExt->input('Client/dotaznik/rod_deti_narozen_'.$i);?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/rod_deti_inval_'.$i);?></td>
   </tr>
<?php endfor;?>   
</table>




<h3><strong>Vzdělání</strong></h3>
<table class='table dotaznik'>
  <tr>
      <th>Stupeň vzdělání</th>
      <th>Škola/obor</th>
      <th>Počet tříd/semestrů</th>
      <th>Studium od-do</th>
      <th>Druh zkoušky/datum zkoušky</th>
  </tr>
  <tr>
      <th>Základní</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
  <tr>
      <th>Učební obor</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/ucnak_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/ucnak_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/ucnak_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/ucnak_druh_zkousky');?></td>
  </tr>
  <tr>
      <th>Uč.obor s maturitou</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
	<tr>
      <th>SO bez maturity</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
	</tr>
	<tr>
      <th>ÚSV</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
<tr>
      <th>ÚSO s maturitou</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
<tr>
      <th>Bakalářské </th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
<tr>
      <th>Vysokoškolské</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
<tr>
      <th>Doktorské</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
<tr>
      <th>Jiné</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>


<tr><th colspan="5">&nbsp;</th></tr>
<tr>
      <th>Nedokončené</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
<tr>
      <th>Probíhající</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_skola');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_pocet_roku');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_od_do');?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zakladni_druh_zkousky');?></td>
  </tr>
</table>

<h3><strong>Máte v době uzavření pracovního poměru uzavřen další prac. poměr?</strong></h3>
<table class='table dotaznik'>
  <tr>
		<th>Zaměstnavatel</th>
		<th>Pracovní poměr uzavřen na dobu od-do</th>
		<th>Pracovní úvazek</th>
  </tr>
  
<?php for ($i=1; $i<=3; $i++):?>  
  
  <tr>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_zarazeni_'.$i);?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_od_'.$i);?></td>
  </tr>
<?php endfor; ?>
 
</table>

<h3><strong>Jste poživatelem důchodu?</strong> (údaj potřebný pro účely plnění ohlašovací povinnosti zaměstnavatele a přihlášení ke zdrav. pojištění)</h3>
<table class='table dotaznik'>
  <tr>
		<th>Druh důchodu</th>
		<th>Důch. vyměřen podle zákona/§/písm.</th>
		<th>Důch. vyplácen od (ČSSP,MV,MO)</th>
		<th>Datum výměru</th>
  </tr>
  
<?php for ($i=1; $i<=2; $i++):?>  
  
  <tr>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_zarazeni_'.$i);?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_od_'.$i);?></td>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_do_'.$i);?></td>
  </tr>
<?php endfor; ?>
 
</table>


<h3><strong>Jste osobou se zdrav. postižením? </strong> ? (údaj nutný k plnění povinnosti zaměstnavatele vést evidenci těchto osob vyplývající ze zákona o zaměstnanosti)</h3>
<table class='table dotaznik'>
  
  <tr>
	  <th style='width:140px'>Plně invalidní (os. s těžším zdrav.postižením)</th>
      <td>od : <?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
      <td>do : <?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
  <tr>
	  <th style='width:140px'>Částečně invalidní</th>
      <td>od : <?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
      <td>do : <?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
  <tr>
	  <th style='width:140px'>Osoba zdravotně znevýhodněná</th>
      <td>od : <?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
      <td>do : <?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
 
</table>


<h3><strong> </strong></h3>
<table class='table dotaznik'>
 <tr>
	  <th>Zdravotní pojišťovna – přiložte kopii karty </th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
 
</table>

<h3><strong>Pro účely registru pojištěnců o osobách účastných nemocenského pojištění a důch. pojištění z důvodu výdělečné činnosti a dobrovolné účasti na důch. pojištění uveďte:</strong> (s úč. od 1.7.2005-zák. o org. a provádění soc. zabezp.). Údaj se týká posledního předchozího zaměstnavatele </h3>
<table class='table dotaznik'>
 <tr>
	  <th>Název a adresa cizozemského nositele pojištění:</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
 <tr>
	  <th>Cizozemské číslo pojištění:</th>
      <td><?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
 
</table>


<h3><strong>Vyplní cizí státní příslušník </strong> (viz výše uvedený předpis):</h3>
<table class='table dotaznik'>
 <tr>
	  <th>Pohlaví:</th>
      <td >Žena </td>
	  <td><?php echo $htmlExt->checkbox('Client/dotaznik/zam_sidlo_'.$i);?></td>
	  <td>Muž </td>
	  <td><?php echo $htmlExt->checkbox('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
 <tr>
	  <th>Adresa pobytu na úz. ČR:</th>
      <td colspan="4"><?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
 
</table>


<h3><strong></strong> </strong></h3>
<table class='table dotaznik'>
 <tr>
	  <td style="width:450px;">Tímto uděluji souhlas zaměstnavateli se zpracováním všech údajů v tomto dotazníku za účelem vedení evidence zaměstnanců, vyžadovaných zvláštními zákony, jakož i zaměstnavatelem vedené personální evidence zaměstnanců.
Prohlašuji, že jsem uvedl/a přesné údaje a o změnách budu zaměstnavatele informovat písemně do 15 dnů od jejich účinnosti, pokud zákon nestanoví jinak, aby mohl tyto údaje včas aktualizovat.
:</td>
    <td>Datum : <?php echo $htmlExt->input('Client/dotaznik/zam_sidlo_'.$i);?></td>
  </tr>
 <tr>
	<td colspan="2"><br />Podpis<br /><br /></td>
  </tr>
 
</table>