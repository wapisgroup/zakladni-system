<?php 
$forma = $fastest->made_forma_odmeny($detail['CompanyMoneyItem']['name'], $detail['CompanyMoneyItem']['doprava'], $detail['CompanyMoneyItem']['cena_ubytovani_na_mesic'], $detail['CompanyMoneyItem']['stravenka']);
?>
<form action='/clients/zmena_pp/<?php echo $client_id;?>/<?php echo $company_id;?>' method='post' id='formular'>
<?php echo  $htmlExt->hidden('connection_client_requirements_id');?>
<?php echo  $htmlExt->hidden('old_company_work_position_id',array('value'=>$detail['CompanyWorkPosition']['id']));?>
<?php echo  $htmlExt->hidden('from_old');?>
<?php echo  $htmlExt->hidden('scenar',array('value'=>0));// DEFAULTNE 0 = zadne OPP ?> 
<?php echo  $htmlExt->hidden('profese',array('value'=>$detail['CompanyWorkPosition']['name']));?>
<?php echo  $htmlExt->hidden('forma',array('value'=>$forma));?>
<?php echo  $htmlExt->hidden('client_id',array('value'=>$client_id));?>
       
            <fieldset>
		      <legend>Starý pracovního poměr</legend>
			<?php echo $htmlExt->var_text('profese',array('label'=>'Profese','label_class'=>'long','value'=>$detail['CompanyWorkPosition']['name']));?> <br class="clear">		
			<?php echo $htmlExt->var_text('forma',array('label'=>'Forma odměny','label_class'=>'long','value'=>$forma));?> <br class="clear">		
			<?php echo $htmlExt->inputDate('do',array('tabindex'=>1,'label'=>'Datum ukončení','class'=>'','label_class'=>'long'));?> <br class="clear">		
			</fieldset>
            
            <fieldset>
    		    <legend>Nový pracovní poměr</legend>
                <?php echo $htmlExt->inputDate('od',array('tabindex'=>1,'label'=>'Datum nástupu','class'=>'','label_class'=>'long'));?> <br class="clear">		
    			<?php echo $htmlExt->selectTag('company_work_position_id',$company_work_position_list,null,array('label'=>'Profese','class'=>'long','label_class'=>'long'));?><br/>
    			<?php echo $htmlExt->selectTag('company_money_item_id',$money_item_list=array(),null,array('label'=>'Forma odměny','class'=>'long','label_class'=>'long'),null,false);?><br/>
    		</fieldset>

            <fieldset id="opp_box" class="none">
		      <legend>OPP</legend>
               <table class="table">
                    <tr>
                        <th>Druh</th>
                        <th>Typ</th>
                        <th>Velikost</th>
                        <th>Cena</th>
                        <th>Datum přidělení</th>
                        <th>Nárok</th>
                        <th></th>
                    </tr>
                    <?php 
                    if(isset($opp_list))
                        foreach($opp_list as $item){
                            echo '<tr>';
                            echo '<td>'.$item['ConnectionClientOpp']['typ'].'</td>';
                            echo '<td>'.$item['ConnectionClientOpp']['name'].'</td>';
                            echo '<td>'.$item['ConnectionClientOpp']['size'].'</td>';
                            echo '<td>'.$item['ConnectionClientOpp']['price'].'</td>';
                            echo '<td>'.$fastest->czechDate($item['ConnectionClientOpp']['created']).'</td>';
                            echo '<td>'.$item['ConnectionClientOpp']['narok'].'</td>';
                            echo '<td>'.$htmlExt->selectTag('Opp/'.$item['ConnectionClientOpp']['id'].'/var',($item['ConnectionClientOpp']['narok'] == 'ne' ? $opp_rozvazat_pp_ne : $opp_rozvazat_pp),null,null,null,false);
                            echo $htmlExt->hidden('Opp/'.$item['ConnectionClientOpp']['id'].'/narok',array('value'=>$item['ConnectionClientOpp']['narok']));
                            echo $htmlExt->hidden('Opp/'.$item['ConnectionClientOpp']['id'].'/price',array('value'=>$item['ConnectionClientOpp']['price']));
                            echo '</td>';
        			       
                            echo '</tr>';
                        }
                    
                    ?>
                </table> 
            </fieldset>  

	<div class='formular_action'>
		<input type='button' value='Změnit' id='VyhoditSaveAndClose' />
		<input type='button' value='Zavřít' id='VyhoditClose'/>
	</div>
</form>
<script>
    var opp_box = '<?php echo(!empty($opp_list) ? true : false);?>';
    var dochazka = '<?php echo $dochazka;?>';
    
    if(opp_box == true) {$('opp_box').removeClass('none')}
    
// add event for change Profese and load list for calculations
	$('CompanyWorkPositionId').addEvents({
			'change': function(){
					new Request.JSON({
						url: '/report_requirements_for_recruitments/load_calculation_for_prefese_second/' + this.value + '/',
						onComplete: (function (json){
							if (json){
								if (json.result === true){
									var sub_select = $('CompanyMoneyItemId');
									sub_select.empty();
									//new Element('option').setHTML('').inject(sub_select);
									$each(json.data, function(item){
										doprava = 'Doprava '+(item.doprava != 0 ? 'Ano' : 'Ne');
										ubytovani = 'Ubytování '+(item.cena_ubytovani_na_mesic != 0 ? 'Ano' : 'Ne');
	
										new Element('option',{value:item.id,title:item.name + '|' + item.stravenka + '|' + item.fakturacni_sazba_na_hodinu})
											.setHTML(item.name + ' - ' + item.cista_mzda_z_pracovni_smlouvy_na_hodinu + '/' + item.cista_mzda_faktura_zivnostnika_na_hodinu + '/' + item.cista_mzda_dohoda_na_hodinu + '/' + item.cista_mzda_cash_na_hodinu
											+ ' - ' + ubytovani +' / '+ doprava	
											)
											.inject(sub_select);
									})
									//this.currentSelection = this.selectedIndex;
								} else {
									this.options[this.currentSelection].selected = 'selected';
									alert(json.message);
								}
							} else {
								alert('Systemova chyba!!!');
							}
						}).bind(this)
					}).send();
			},
			'focus': function(){
				this.currentSelection = this.selectedIndex;
			}
	});
    
    if(opp_box  == true){
        $('calbut_Do').addEvent('change',function(){opp_fnc();});
        $('CompanyWorkPositionId').addEvent('change',function(){opp_fnc();});    
    }    
    
    function opp_fnc(){
        var from = $('FromOld').value;
        var scenar = $('Scenar');
        var to = $('calbut_Do').value;
        var old_cwp_id = $('OldCompanyWorkPositionId').value;
        var cwp_id = $('CompanyWorkPositionId').value;
        
        /**
         * Pripad c.1
         * pracoval více jak měsíc a je nutné nastavit co s danými opp
         */
        if(compare_date(from,to) == false){
           /**
            * Pokud je však nastavená stejná profese, jen se mění forma odměny tak se přenášejí dané opp
            */
            if(old_cwp_id == cwp_id){
                scenar.value = 1;
                $('opp_box').addClass('none');
                alert("Nastavili jste stejnou profesi, klientovy OPP budou přeneseny z starého pracovního poměru.\n\nUpozornění:\nJe nutné aby jste zkontrolovaly po změně prac. poměru poslední docházku(přeuložili, pro korektní uložení docházky a srážek za OPP!)");
            }
            else{
                scenar.value = 2;
                $('opp_box').removeClass('none');
            }        
        }
        /**
         * Pripad c.2 
         * datumy jsou stejne, mohu nastat dve situace
         */
        else{
            /**
             * Prvni je ze se COO spletl a potrebuje pouze zmenit pracovni pomer
             * tedy dochazka neexistuje a nebo ma nulove pracovni hodiny
             * tudiz muzeme opp vratit na sklad a dat do kose connection
             */
             if(dochazka == false){
                scenar.value = 3;
                $('opp_box').addClass('none');
                alert('Opp budou vráceny na sklad. Neexistuje docházka pro daného klienta, tudíž se jedná o chybné zaměstnání a OPP byli nepoužity.');
             }
             else{
               /**
                * Pokud je však nastavená stejná profese, jen se mění forma odměny tak se přenášejí dané opp
                */
                if(old_cwp_id == cwp_id){
                    scenar.value = 1;
                    $('opp_box').addClass('none');
                    alert('Nastavili jste stejnou profesi, klientovy OPP budou přeneseny z starého pracovního poměru.');
                }
                else{
                    scenar.value = 2;
                    $('opp_box').removeClass('none');
                }    
             }
        }  
    }

    function get_date(date, separator){
        date = date.split(separator);           
        return date[2] +'.' + date[1] + '.' + date[0];
    }
    
    function compare_date(date1,date2){
        date1 = date1.split('-');
        date2 = date2.split('-');                    
        if(date1[0] == date2[0] && date1[1] == date2[1])
         return true;
        else
         return false;    
    }
   
    from_old = get_date($('FromOld').value, '-');
        
        
	$('VyhoditClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_zamestnanci_hodiny');});
	$('VyhoditSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
        valid_result = validation.valideForm('formular');


        if( $('calbut_Do').value == '') 
			if(valid_result == true)
				valid_result = Array("Musite zvolit datum ukončení");
			else
				valid_result[valid_result.length]="Musite datum ukončení";
        else{
            if($('calbut_Do').value <= $('FromOld').value)
                if(valid_result == true)
				    valid_result = Array("Chyba datum ukončení je menší než staré datum nástupu.\nToto datum musí být min. o jeden den větší než datumu nástupu.\n\nDatum nástupu bylo : "+from_old);
                else
				    valid_result[valid_result.length]="Chyba datum ukončení je menší než staré datum nástupu.\nToto datum musí být min. o jeden den větší než datumu nástupu.\n\nDatum nástupu bylo : "+from_old;   
        }         
                
        if( $('calbut_Od').value == '') 
			if(valid_result == true)
				valid_result = Array("Musite zvolit datum nástupu");
			else
				valid_result[valid_result.length]="Musite zvolit datum nástupu";        
        else{
            if($('calbut_Od').value <= $('calbut_Do').value)
                if(valid_result == true)
				    valid_result = Array("Chyba datum nástupu je menší než datum ukončení.\nToto datum musí být min. o jeden den větší než datumu ukončení.");
                else
				    valid_result[valid_result.length]="Chyba datum nástupu je menší než datum ukončení.\nToto datum musí být min. o jeden den větší než datumu ukončení.";   
        }        


      
        if (valid_result == true){
            button_preloader($('VyhoditSaveAndClose'));
			new Request.JSON({
				url:$('formular').action,		
				onComplete:function(json){
					if (json){
						if (json.result === true){
                            if(json.message)
                                alert(json.message);
                            click_refresh($('ClientId').value);    
                            domwin.closeWindow('domwin_zamestnanci_hodiny');
						} else {
							alert(json.message);
						}
					} else {
						alert('Chyba aplikace');
					}
                    
                    button_preloader_disable($('VyhoditSaveAndClose'));
				}
			}).post($('formular'));
       } 
       else {
		  var error_message = new MyAlert();
		  error_message.show(valid_result)
	   }      
	});
    
    validation.define('formular',{	
        'CompanyWorkPositionId': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit profesi'},
		}
	});
	validation.generate('formular',false);
</script>	