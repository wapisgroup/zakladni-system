<form action="/mv_orders/edit/" method="post" id='mv_order_form'>
<?php echo $htmlExt->hidden('MvOrder/id');?>
<table class="table tabulka">
	<tr>
		<th>Druh</th>
		<th>Typ</th>
		<th>Pocet</th>
		<th>Cena CZ</th>
        <th>Cena EU</th>
		<th>Potvrzeni</th>
		<th>Moznosti</th>
	</tr>
	<tbody id='sub_table'>
		<tr class="od">
			<td><?php echo $htmlExt->selectTag('MvOrderItem/-1/type_id',$type_list, null, array('class'=>'select_type CE max_size','id'=>'first_select_type'));?></td>
			<td><select class='select_name CE max_size' name='data[MvOrderItem][-1][name]'><option>---</option></select></td>
			<td><input class='input_count' type="text" name='data[MvOrderItem][-1][count]' value="0"/></td>
			<td><input class='input_price_cz' type="text" name='data[MvOrderItem][-1][price_cz]'value="0" readonly="readonly"/></td>
            <td><input class='input_price_eu' type="text" name='data[MvOrderItem][-1][price_eu]'value="0" readonly="readonly"/></td>
			<td><input type="checkbox" name='data[MvOrderItem][-1][stav]'/></td>
			<td>
				<a href='#' class="ta add add_row" id='clone_row'>Add</a>
				<a href='#' class="ta delete delete_row none CE">Delete</a>
			</td>
		</tr>
		<?php if (isset($this->data['MvOrderItem']) && count($this->data['MvOrderItem']) > 0):?>
		<?php foreach($this->data['MvOrderItem'] as $k => $itm):?>
		<tr>
			<td><?php echo $htmlExt->selectTag('MvOrderItem/'.$k.'/type_id',$type_list, null, array('class'=>'select_type CE max_size'));?></td>
			<td><?php echo $htmlExt->selectTag('MvOrderItem/'.$k.'/name',$names[$itm['type_id']], null, array('class'=>'select_name CE max_size'));?></td>
			<td><?php echo $htmlExt->input('MvOrderItem/'.$k.'/count',array('class'=>'input_count'));?></td>
			<td><?php echo $htmlExt->input('MvOrderItem/'.$k.'/price_cz',array('class'=>'input_price_cz','readonly'=>'readonly'));?></td>
            <td><?php echo $htmlExt->input('MvOrderItem/'.$k.'/price_eu',array('class'=>'input_price_eu','readonly'=>'readonly'));?></td>
            <td><?php echo $htmlExt->checkbox('MvOrderItem/'.$k.'/stav',array('class'=>'input_stav'));?></td>
			<td>
				<a href='#' class="ta delete delete_row CE">Delete</a>
			</td>
		</tr>
		<?php endforeach;?>
		<?php endif;?>
	</tbody>
</table>
<div class='win_save'>
    <?php if (isset($new)):?>
	<input type="button" id='save_order' value="Odeslat objednávku" />
    <?php endif;?>
	<input type="button" id='close_order' value='Zavřít' />
</div>
</form>
<script language="javascript">
   	var logged_user = '<?php echo $logged_user['CmsUser']['id'];?>';
    var empty_value = '--- Zvolte hodnotu ---';
    
	$('close_order').addEvent('click', function(e){
		e.stop();
		domwin.closeWindow('domwin');
	});

    if($('MvOrderId').value != ''){
        //change_size_value_to_rel(); //pri zacatku editaci nastav spravne selecty
    } else {
        if(logged_user == 1){
            if($('choose_template'))
                $('choose_template').removeClass('none');

           // if($('save_order'))
           //     $('save_order').addClass('none');
        }
    }

	if($('save_order')){
        $('save_order').addEvent('click', function(e){
            e.stop();
            $('sub_table').getElement('tr').getElements('input, select').setProperty('disabled','disabled');

            new Request.JSON({
                url: $('mv_order_form').getProperty('action'),
                onComplete: function(json){
                    if (json){
                        if (json.result === true){
                            alert('Objednávka byla zaznamenána');
                            domwin.closeWindow('domwin');
                            click_refresh();
                        } else {
                            alert(json.message);
                        }
                    } else {
                        alert('Chyba aplikace');
                    }
                }
            }).send($('mv_order_form'));
        })
    }

	$$('.delete_row').addEvent('click', function(e){
		e.stop();
		this.getParent('tr').dispose();
	});

	function valid_row(){
		var tr = $('sub_table').getElement('tr');
		var output = Array();

		if (tr.getElement('.select_type').value == '')  output.push('Musite zvolit druh MV');
		if (tr.getElement('.select_name').value == '')  output.push('Musite zvolit typ MV');
		if (tr.getElement('.input_count').value == '' && tr.getElement('.input_count').value != '0')  output.push('Musite zvolit pocet MV');

		if (output.length > 0){
			var al = new MyAlert();
			al.show(output);
		} else {
			return true;
		}
	}

	$('clone_row').addEvent('click', function(e){
		e.stop();

		if (valid_row()){
			var tr = this.getParent('tr'), clone = tr.clone();
			$('sub_table').adopt(clone);

            clone.removeClass('od');
			clone.getElement('.add_row').dispose();
			clone.getElement('.delete_row').removeClass('none');

            var load_type_url = '/mv_orders/load_typ/';
			clone.getElement('.select_type').ajaxLoad(load_type_url,[clone.getElement('.select_name')]);




			tr.getElements('.select_type, .input_count, .input_price_cz, .input_price_eu').setValue('');

			var uni = uniqid();
			increase_name(clone,1,uni);
		}
	});

	function increase_name(parent,index,prefix){
 		parent.getElements('input, select').each(function(el){
 			atrs = el.name.split('][');
 			prt = atrs[index];
			atrs[index] = prefix;
 		 	el.name = atrs.join('][');
	 	})
 	}



    $$('.select_type').addEvent('change',function(e){
            select_type(e);
    	});

       function select_type(e){
           var itm = e.target;
                   var children = [itm.getParent('tr').getElement('.select_name')];
                   new Request.JSON({
                       url: '/mv_orders/load_typ/' + itm.value,
                       onComplete: (function(json){
                           if(json != false){
                               children.each(function(obj){
                                   $(obj).empty();
                                   new Element('option', {title:empty_value, value:''}).setHTML(empty_value).inject($(obj));
                               });
                               child = children[0];

                               $each(json, function(value, id){
                                   vals = value.split('|');
                                   value = vals[0];
                                   rel = vals[1];
                                   new Element('option', {title:value, value:id,rel:rel}).setHTML(value).inject($(child));
                               }, itm);

                           }
                           else{
                               /**
                                * vyresetuj pokud se jedna o prazdne pole
                                */
                               child = children[0];
                               $(child).empty();
                               new Element('option', {title:empty_value, value:''}).setHTML(empty_value).inject($(child));
                           }
                       }).bind(itm)
                   }).send();
       }


    $$('.select_name').addEvent('change',function(e){
        select_size(e);
	});

   function select_size(e){
        var itm = e.target;
        new Request.JSON({
            url: '/mv_orders/load_count_price/'+itm.options[itm.selectedIndex].getProperty('rel'),
            onComplete: function(json){
                if (json){
                    if (json.result === true){
                        tr = itm.getParent('tr');
                        tr.getElement('.input_count').value = json.data.MvItem.pocet;
                        tr.getElement('.input_price_cz').value = json.data.MvItem.cena_cz;
                           tr.getElement('.input_price_eu').value = json.data.MvItem.cena_eur;
                    } else {
                        alert(json.mesage);
                    }
                } else {
                    alert('Chyba aplikace');
                }
            }
        }).send();
    }



</script>