<?php echo $htmlExt->selectTag('NewMoneyItem/fakturacka_typ',array(0=>'Hodnota',1=>'Koeficient'),null,array('label'=>'Typ fakturační sazby','class'=>''),null,false);?><br/>
<?php echo $htmlExt->input('NewMoneyItem/fakturacka',	array('label'=>'Fakturovaná částka','class'=>'integer'));?><br/>
<div class="sll">
    <fieldset>
        <legend>Odměny - Pracovní smlouva</legend>
    	<?php echo $htmlExt->selectTag('NewMoneyItem/pp_typ',array(0=>'Hodinovka',1=>'Měsíčně'),null,array('label'=>'Typ','class'=>''),null,false);?><br />
    	<?php echo $htmlExt->input('NewMoneyItem/pp_max',	array('label'=>'Maximálka(v hrubém)','class'=>'integer '));?><br/>
        <?php echo $htmlExt->input('NewMoneyItem/pp_stravenky',	array('label'=>'Srážka stravenky','class'=>'integer '));?><br/>
        <?php echo $htmlExt->input('NewMoneyItem/pp_doprava',	array('label'=>'Srážka doprava','class'=>'integer '));?><br/>
    </fieldset>
    <fieldset>
        <legend>Odměny - ŽL</legend>
    	<?php echo $htmlExt->selectTag('NewMoneyItem/zl_typ',array(0=>'Hodinovka',1=>'Měsíčně'),null,array('label'=>'Typ','class'=>''),null,false);?><br />
    	<?php echo $htmlExt->input('NewMoneyItem/zl_max',	array('label'=>'Maximálka(v hrubém)','class'=>'integer '));?><br/>
        <?php echo $htmlExt->input('NewMoneyItem/zl_stravenky',	array('label'=>'Srážka stravenky','class'=>'integer '));?><br/>
        <?php echo $htmlExt->input('NewMoneyItem/zl_doprava',	array('label'=>'Srážka doprava','class'=>'integer '));?><br/>
    </fieldset>
</div>
<div class="slr">
    <fieldset>
        <legend>Odměny - Dohoda</legend>
    	<?php echo $htmlExt->selectTag('NewMoneyItem/do_typ',array(0=>'Hodinovka',1=>'Měsíčně'),null,array('label'=>'Typ','class'=>''),null,false);?><br />
    	<?php echo $htmlExt->input('NewMoneyItem/do_max',	array('label'=>'Maximálka(v hrubém)','class'=>'integer '));?><br/>
        <?php echo $htmlExt->input('NewMoneyItem/do_stravenky',	array('label'=>'Srážka stravenky','class'=>'integer '));?><br/>
        <?php echo $htmlExt->input('NewMoneyItem/do_doprava',	array('label'=>'Srážka doprava','class'=>'integer '));?><br/>
    </fieldset>
    <fieldset>
        <legend>Odměny - Abc</legend>
    	<?php echo $htmlExt->selectTag('NewMoneyItem/cash_typ',array(0=>'Hodinovka',1=>'Měsíčně'),null,array('label'=>'Typ','class'=>''),null,false);?><br />
    	<?php echo $htmlExt->input('NewMoneyItem/cash_max',	array('label'=>'Maximálka(v hrubém)','class'=>'integer '));?><br/>
        <?php echo $htmlExt->input('NewMoneyItem/cash_stravenky',	array('label'=>'Srážka stravenky','class'=>'integer '));?><br/>
        <?php echo $htmlExt->input('NewMoneyItem/cash_doprava',	array('label'=>'Srážka doprava','class'=>'integer '));?><br/>
    </fieldset>
</div>
<br />