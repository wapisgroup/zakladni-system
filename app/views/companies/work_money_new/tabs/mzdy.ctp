<table class="table">
        <thead>
            <tr>
                <td>Stravenky</td>
                <td>Doprava</td>
                <td>PP</td>
                <td>Dohoda</td>
                <td>ŽL</td>
                <td>Abc</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $htmlExt->selectTag('NewMoneyItemForm/stravenky',$ano_ne_checkbox,null,array('class'=>'long2'),null,false);?></td>
                <td><?php echo $htmlExt->selectTag('NewMoneyItemForm/doprava',$ano_ne_checkbox,null,array('class'=>'long2'),null,false);?></td>
                <td><?php echo $htmlExt->checkbox('NewMoneyItemForm/pp',null,array())?></td>
                <td><?php echo $htmlExt->checkbox('NewMoneyItemForm/do',null,array())?></td>
                <td><?php echo $htmlExt->checkbox('NewMoneyItemForm/zl',null,array())?></td>
                <td><?php echo $htmlExt->checkbox('NewMoneyItemForm/cash',null,array())?></td>
                <td><a href="#" id="add_new_money_item_form" class="ta status1" title="Přidat">Přidat</a></td>
            </tr>
        </tbody>
</table>
<ul id="NewMoneyItemFormBox">
<?php 
if(isset($is_set_forms)){
    foreach($is_set_forms as $form){
        echo '<li>';
        echo '<strong>'.$form['NewMoneyItemForm']['name'].'</strong> - stravenky '.$fastest->value_to_yes_no($form['NewMoneyItemForm']['stravenky']).' / doprava '.$fastest->value_to_yes_no($form['NewMoneyItemForm']['doprava']).' ';
        echo $html->link('Smazat','/companies/delete_money_item_form/'.$form['NewMoneyItemForm']['id'],array('class'=>'ta status0 delete_form')).'</li>';
        echo '</li>';
    }
}

?>
</ul>