<fieldset>
				<legend>Vložení možného počtu odpracovaných hodin</legend>
				<?php echo $htmlExt->input('NewMoneyItem/pocet_dni_v_mesici',										array('label'=>'Počet pracovních dnů v měsíci','class'=>'edit_small'));?><br class='clear' />
				<?php echo $htmlExt->input('NewMoneyItem/normo_hodina',												array('label'=>'Denní pracovní fond','class'=>'edit_small '));?><br class='clear' />
				<?php echo $htmlExt->input('NewMoneyItem/pocet_hodin_prescasu_mesicne',								array('label'=>'Počet hodin přesčasů měsíčně','class'=>'edit_small'));?><br class='clear' />
				<?php echo $htmlExt->input('NewMoneyItem/pocet_hodin_zakladniho_pracovniho_fondu',					array('class'=>'zero','label'=>'Pracovní fond měsíčně','disabled'=>'disabled'));?><br class='clear' />
				<?php echo $htmlExt->input('NewMoneyItem/celkovy_pocet_hodin_mesicne',								array('class'=>'zero','disabled'=>'disabled','label'=>'Počet hodin měsíčně celkem'));?><br class='clear' />
</fieldset>