<fieldset class="dva_sloupce_za_label">
				<legend>Vložení nákladů na zaměstnance mimo mzdy</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne',	array('class'=>'zero','disabled'=>'disabled','label'=>'Celkové náklady firmy na zaměstnance bez mzdy měsíčně'));?><br class='clear' />
				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/cena_ubytovani_na_mesic',array('Ne','Ano'),null,array('label'=>'Cena ubytování na měsíc'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/cena_ubytovani_na_mesic',array('class'=>'edit_small short slide_field zero'));?><br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/cena_pracovnich_odevu_mesicne',array('Ne','Ano'),null,array('label'=>'Cena pracovních oděvů měsíčně'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/cena_pracovnich_odevu_mesicne',array('class'=>'edit_small short slide_field zero'));?><br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/dalsi_naklady_mesicne',array('Ne','Ano'),null,array('label'=>'Další náklady měsíčně'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/dalsi_naklady_mesicne',array('class'=>'edit_small short slide_field zero'));?><br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/stravenka',array('Ne','Ano'),null,array('label'=>'Stravenky náklady denně'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/stravenka',array('class'=>'edit_small short slide_field zero'));?><br /></div>
				</div>
				
				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/doprava',array('Ne','Ano'),null,array('label'=>'Doprava měsíčně'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/doprava',array('class'=>'edit_small short slide_field zero'));?><br /></div>
				</div>
</fieldset>