<?php
echo $only_show == true ? '<div id="myDisable">' : '';
?>
<form action='/companies/work_money_edit_new/' method='post' id='company_work_money_new_edit_formular'>
	<?php echo $htmlExt->hidden('NewMoneyItem/id');?>
	<?php echo $htmlExt->hidden('NewMoneyItem/company_id');?>
	<?php echo $htmlExt->hidden('NewMoneyItem/company_work_position_id');?>
	<?php echo $htmlExt->hidden('NewMoneyItem/company_money_validity_id');?>

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
            <li class="ousko"><a href="krok1">Odměny</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field" >
            <fieldset class="dva_sloupce_za_label_new">
                <legend>Nastavení</legend>
                <?php echo $htmlExt->input('NewMoneyItem/fakturacka',	array('label'=>'Fakturačka','class'=>'integer short'));?><br/>
                <?php echo $htmlExt->input('NewMoneyItem/normo_hodina',	array('label'=>'Denní pracovní fond','class'=>'integer short'));?><br/>
               	<?php echo $htmlExt->input('NewMoneyItem/pocet_dni_dovolene_za_rok',	array('label'=>'Počet dní dovolené za rok','class'=>'integer short'));?><br/>
                <?php //echo $htmlExt->selectTag('NewMoneyItem/stravne_typ',$list_stravne_typ,null,array('label'=>'Stravné na den - typ','class'=>''),null,false);?>
    	        <?php //echo $htmlExt->input('NewMoneyItem/stravne',	array('label'=>'Stravné na den','class'=>'integer'));?>
                <div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/stravenky',array('Ne','Ano'),null,array('label'=>'Stravenky','class'=>'short'),null,false);?>
					<div><?php echo $htmlExt->input('NewMoneyItem/stravenky',array('class'=>'edit_small short slide_field zero'));?><br /></div>
				</div>
                
                <div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/stravne',array('Ne','Ano'),null,array('label'=>'Stravné','class'=>'short'),null,false);?>
					<div><?php echo $htmlExt->input('NewMoneyItem/stravne',array('class'=>'edit_small short slide_field zero'));?><br /></div>
				</div>
                <?php echo $htmlExt->input('NewMoneyItem/cestovne',	array('label'=>'Cestovné','class'=>'integer short','label_class'=>'clear'));?><br/>
            </fieldset>
            <fieldset>
                <legend>Základní hrubá mzda</legend>
    		    <?php echo $htmlExt->selectTag('NewMoneyItem/mzda_typ',$list_mzda_typ,null,array('label'=>'Základní hrubá mzda - typ','class'=>''),null,false);?><br />
    	        <?php echo $htmlExt->input('NewMoneyItem/zakladni_mzda',	array('label'=>'Základní hrubá mzda','class'=>'integer'));?><br/>
                <label></label><span id="prepocetZHM"></span>
            </fieldset>
            <fieldset>
        		<legend>Bonusy</legend>
                <?php echo $htmlExt->selectTag('list_bonus',$bonus_list,null,array('label'=>'Přidat ze seznamu')); ?>
                <a href='/companies/load_detail_for_money_item/0/' class="ta add" id='add_from_list_bonus' title='Přidat bonus'>Přidat ze seznamu</a><br />
               
        		<table class="table" id='bonus_list'>
        			<thead>
                	<tr>
                        <th>Název</th>
                        <th>Typ</th>
                        <th>Hodnota</th>        	        	
                        <th style='width:50px'>Možnosti</th>
        			</tr> 
        			</thead>
        			<tbody>
        				<tr class="enter_row">	
        					<td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/0/new_0/name',array('tabindex'=>17)); ?></td>
        					<td><?php echo $htmlExt->selectTag('ConnectionMoneyItemSetting/0/new_0/typ',$list_type_of_drawback_and_bonus,null,array('tabindex'=>17)); ?></td>
                            <td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/0/new_0/hodnota',array('tabindex'=>17,'class'=>'integer val')); ?></td>
        					<td>
        						<a href='#' class="ta add" id='add_new_bonus' title='Přidat bonus'>Přidat</a>
        						<a href="#" rel='1' class="ta none delete remove_bonus_row"></a>
                                <?php echo $htmlExt->hidden('ConnectionMoneyItemSetting/0/new_0/phm',array()); ?>
                                <?php echo $htmlExt->hidden('ConnectionMoneyItemSetting/0/new_0/money_item_connection_tool_id',array()); ?>
        					</td>
        				</tr>	
                    	<?php if(isset($this->data['ConnectionMoneyItemSetting'][0]) && count($this->data['ConnectionMoneyItemSetting'][0])>0): ?>
        				<?php foreach($this->data['ConnectionMoneyItemSetting'][0] as $key => $kval): ?>
        				<tr class="education_row_<?php echo $kval['id'] ?>">	
        					<td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/0/' . $key . '/name',array('value'=>$kval['name'],'disabled'=>'disabled','tabindex'=>17)); ?></td>
        					<td><?php echo $htmlExt->selectTag('ConnectionMoneyItemSetting/0/' . $key.'/typ',$list_type_of_drawback_and_bonus,$kval['typ'],array('disabled'=>'disabled','tabindex'=>17)); ?></td>
                            <td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/0/' . $key . '/hodnota',array('value'=>$kval['hodnota'],'disabled'=>'disabled','tabindex'=>17)); ?></td>
        					<td><a href="#" rel='1' class="ta delete remove_bonus_row"></a></td>
        				</tr>	
        				<?php endforeach; ?>
        				<?php endif; ?>
        			</tbody>
        		</table>
        	</fieldset>
            <fieldset>
        		<legend>Příplatek</legend>
                
                <?php echo $htmlExt->selectTag('list_extra_pay',$extra_pay_list,null,array('label'=>'Přidat ze seznamu')); ?>
                <a href='/companies/load_detail_for_money_item/1/' class="ta add" id='add_from_list_extra_pay' title='Přidat příplatek'>Přidat ze seznamu</a><br />
                
        		<table class="table" id='extra_pay_list'>
        			<thead>
                	<tr>
                        <th>Název</th>
                        <th>Typ</th>
                        <th>Hodnota</th>        	        	
                        <th style='width:50px'>Možnosti</th>
        			</tr> 
        			</thead>
        			<tbody>
        				<tr class="enter_row">	
        				    <td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/1/new_0/name',array('tabindex'=>17)); ?></td>
                            <td><?php echo $htmlExt->selectTag('ConnectionMoneyItemSetting/1/new_0/typ',$list_type_of_extra_pay,null,array('tabindex'=>17)); ?></td>
                            <td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/1/new_0/hodnota',array('tabindex'=>17,'class'=>'integer')); ?></td>
        					<td>
        						<a href='#' class="ta add" id='add_new_extra_pay' title='Přidat příplatek'>Přidat</a>
        						<a href="#" rel='1' class="ta none delete remove_extra_pay_row"></a>
                                <?php echo $htmlExt->hidden('ConnectionMoneyItemSetting/1/new_0/phm',array()); ?>
                                <?php echo $htmlExt->hidden('ConnectionMoneyItemSetting/1/new_0/money_item_connection_tool_id',array()); ?>
        					</td>
        				</tr>	
                    	<?php if(isset($this->data['ConnectionMoneyItemSetting'][1]) && count($this->data['ConnectionMoneyItemSetting'][1])>0): ?>
        				<?php foreach($this->data['ConnectionMoneyItemSetting'][1] as $key => $kval): ?>
        				<tr class="education_row_<?php echo $kval['id'] ?>">	
        					<td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/1/' . $key . '/name',array('value'=>$kval['name'],'disabled'=>'disabled','tabindex'=>17)); ?></td>
        					<td><?php echo $htmlExt->selectTag('ConnectionMoneyItemSetting/1/' . $key.'/typ',$list_type_of_extra_pay,$kval['typ'],array('disabled'=>'disabled','tabindex'=>17)); ?></td>
                            <td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/1/' . $key . '/hodnota',array('value'=>$kval['hodnota'],'disabled'=>'disabled','tabindex'=>17)); ?></td>
        					<td><a href="#" rel='1' class="ta delete remove_extra_pay_row"></a></td>
        				</tr>	
        				<?php endforeach; ?>
        				<?php endif; ?>
        			</tbody>
        		</table>
        	</fieldset>
            <fieldset>
        		<legend>Srážky</legend>
                <?php echo $htmlExt->selectTag('list_drawback',$drawback_list,null,array('label'=>'Přidat ze seznamu')); ?>
                <a href='/companies/load_detail_for_money_item/2/' class="ta add" id='add_from_drawback_list' title='Přidat srážku'>Přidat ze seznamu</a><br />
                
        		<table class="table" id='drawback_list'>
        			<thead>
                	<tr>
                        <th>Název</th>
                        <th>Typ</th>
                        <th>Hodnota</th>        	        	
                        <th style='width:50px'>Možnosti</th>
        			</tr> 
        			</thead>
        			<tbody>
        				<tr class="enter_row">	
        					<td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/2/new_0/name',array('tabindex'=>17)); ?></td>
        					<td><?php echo $htmlExt->selectTag('ConnectionMoneyItemSetting/2/new_0/typ',$list_type_of_drawback_and_bonus,null,array('tabindex'=>17)); ?></td>
                            <td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/2/new_0/hodnota',array('tabindex'=>17,'class'=>'integer val')); ?></td>
        					<td>
        						<a href='#' class="ta add" id='add_new_drawback' title='Přidat srážku'>Přidat</a>
        						<a href="#" rel='1' class="ta none delete remove_drawback_row"></a>
        				        <?php echo $htmlExt->hidden('ConnectionMoneyItemSetting/2/new_0/money_item_connection_tool_id',array()); ?>
                        	</td>
        				</tr>	
                    	<?php if(isset($this->data['ConnectionMoneyItemSetting'][2]) && count($this->data['ConnectionMoneyItemSetting'][2])>0): ?>
        				<?php foreach($this->data['ConnectionMoneyItemSetting'][2] as $key => $kval): ?>
        				<tr class="education_row_<?php echo $kval['id'] ?>">	
        					<td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/2/' . $key . '/name',array('value'=>$kval['name'],'disabled'=>'disabled','tabindex'=>17)); ?></td>
        					<td><?php echo $htmlExt->selectTag('ConnectionMoneyItemSetting/2/' . $key.'/typ',$list_type_of_drawback_and_bonus,$kval['typ'],array('disabled'=>'disabled','tabindex'=>17)); ?></td>
                            <td><?php echo $htmlExt->input('ConnectionMoneyItemSetting/2/' . $key . '/hodnota',array('value'=>$kval['hodnota'],'disabled'=>'disabled','tabindex'=>17)); ?></td>
        					<td><a href="#" rel='1' class="ta delete remove_drawback_row"></a></td>
        				</tr>	
        				<?php endforeach; ?>
        				<?php endif; ?>
        			</tbody>
        		</table>
        	</fieldset>
            <br />
    	</div>
	</div>
	<div class='formular_action'>
		<input type='submit' value='Uložit' id='AddEditCompanyWorkMoneySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkMoneyClose'/>
	</div>
</form>
<?php
echo $only_show == true ? '</div>' : '';
?>
<?php echo $this->renderElement('../companies/work_money_new/js');?>