<script>
domtab = new DomTabs({'className':'admin_dom'}); 
var only_show = <?php echo (isset($only_show) && $only_show!="") ? $only_show : 'false';?>;
var edit_small = <?php echo (isset($edit_small) && $edit_small!="") ? $edit_small : 'false';?>;


// pouze zobrazeni a zamezit editaci
if(only_show){
	$('company_work_money_new_edit_formular').getElements('input,select,textarea').setProperty('disabled','disabled');
	$('AddEditCompanyWorkMoneySaveAndClose').addClass('hidden');
	$('AddEditCompanyWorkMoneyClose').removeProperty('disabled');
    
    if(edit_small){
        $('AddEditCompanyWorkMoneySaveAndClose').removeClass('hidden');
        $('AddEditCompanyWorkMoneySaveAndClose').removeAttribute('disabled');
        $('company_work_money_new_edit_formular').getElements('input.edit_small, select.edit_small').removeProperty('disabled').setStyle('background','#FFF');

    }
}

$('company_work_money_new_edit_formular').getElements('.integer').inputLimit();
$('AddEditCompanyWorkMoneyClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_money');});

function myround(num){return Math.round(num * 100) / 100;}
/**
 * Prepocet ZHM
 */
function prepocet_zhm(){
    var pocet_dnu_v_mesici = 20;
    nh = $('NewMoneyItemNormoHodina').value.toFloat();
    typ = $('NewMoneyItemMzdaTyp').value;
    hodnota = $('NewMoneyItemZakladniMzda').value.toFloat();
    
    var ret = 0;
    if(typ == 1){
        ret = myround((nh*pocet_dnu_v_mesici)*hodnota);
    }
    else{
        ret = myround(hodnota/(nh*pocet_dnu_v_mesici));
    }
    
    $('prepocetZHM').setHTML((typ==1 ? 'měsíčně ':'na hodinu')+' '+ret+',-');
}

$('NewMoneyItemMzdaTyp').addEvent('change',prepocet_zhm);
$('NewMoneyItemZakladniMzda').addEvent('change',prepocet_zhm);
$('NewMoneyItemNormoHodina').addEvent('change',prepocet_zhm);
prepocet_zhm();


/**
 * Pridani priplatku definovaneho z DB
 */
$('add_from_list_extra_pay').addEvent('click',function(e){
   e.stop(); 
   if($('ListExtraPay').value == '')
    return false;
   
   add_form_db(this.href,'ListExtraPay','extra_pay_list');   
});

/**
 * Pridani srazek ze seznamu
 */
$('add_from_drawback_list').addEvent('click',function(e){
   e.stop(); 
   if($('ListDrawback').value == '')
    return false;
    
   add_form_db(this.href,'ListDrawback','drawback_list');  
});    

/**
 * Pridani bonusu ze seznamu
 */
$('add_from_list_bonus').addEvent('click',function(e){
   e.stop(); 
   if($('ListBonus').value == '')
    return false;
    
   add_form_db(this.href,'ListBonus','bonus_list');  
});  

function add_form_db(href,value_id,table){
    new Request.JSON({
		url:href + $(value_id).value,		
		onComplete:function(json){
		    if(json){
		       if(json.result){
                   $each(json.data,function(value,item){
                        if($(item)){
                            $(item).value = value;
                        }
                   });
                   
                   add_row(table);        
		       }
		    }
		}
   }).send();
}



$('company_work_money_new_edit_formular').getElements('.delete').addEvent('click', delete_row.bindWithEvent(this));

function delete_row(e){
	e.stop();
	obj = e.target;
	count = obj.getProperty('rel').toInt() - 1;
	tr = obj.getParent('tr');
	if (count > 0){
		tr.getNext('tr').dispose();
	}
	tr.dispose();
}


/* Bonusy */
$('add_new_bonus').addEvent('click',function(){
	add_row('bonus_list');
});

/* Srazky */
$('add_new_drawback').addEvent('click',function(){
	add_row('drawback_list');
});

/* Priplatky */
$('add_new_extra_pay').addEvent('click',function(){
	add_row('extra_pay_list');
});


/**
  * Pridani noveho radku do kurzu, jedna se o duplikaci existujicich a naslednou upravu
  * name a id elementu
  */
 function add_row(table){
 	var uni = uniqid();
     var lasts = $(table).getElement('tbody').getElements('.enter_row');
     after = lasts[lasts.length-1];
     
     var valid = validate_row(after);
     if(valid == true){
         lasts.each(function(last){
         	var clon = last.clone(true);
    	     increase_name(clon,2,uni);
        	 clon.inject(after,'after');
         
    	     last.getElements('input, select').setValue('');
    		 last.getElements('.to_empty').empty();	
    	 
    		 clon.getElements('.add').dispose();	
    		 clon.getElements('.delete').removeClass('none');	
    		 clon.getElements('input, select').setProperty('disabled','disabled');
         
        	 clon.getElements('.delete').addEvent('click',delete_row.bindWithEvent(this));
        	 clon.removeClass('enter_row');
        	 after = clon;
         })
     }
     else
        alert(valid);
     
     return;     
 }
 
 function validate_row(row){
   var theads = row.getParent('table').getElements('thead th');
   var theads_txt = Array();

   if(theads.length == 4)
      theads_txt = ['Název','Hodnota','Typ'];
   else
      theads_txt = ['Hodnota','Typ'];
       
   //TODO:: v pripade ze by se rozsirovaly tabulky a menily nazvy, tak se da udelat prochazeni thcek, a serazenych, dle tableindex
   //       jinak se seradi dle typu(inputy,selecty) a nesedi pak s th  
   //   theads.each(function(th){theads_txt.push(th.getHTML())})
    
   
   var els = row.getElements('input[type=text], select');
   var errors  = Array();
   var skip_max = false; 
   els.each(function(el,i){
      if(el.value == ''){
         errors.push(theads_txt[i]+ ' nesmí být prázdný.');
      }
   })
   
   if(errors.length < 1){
      return true;
   }
   else
      return makeResultMsg(errors);
 }
 function makeResultMsg(data){
        return data.join("\n");
 }
 
 /**
  * Funkce na zviseni cislene hodnoty v name elementu, jedna se o pripady
  * ze dany element je soucasti pole, ktere se zpracovava na bazi php
  */
 function increase_name(parent,index,prefix){
 	parent.getElements('input, select').each(function(el){
 		atrs = el.name.split('][');
 		prt = atrs[index];
		atrs[index] = prefix;
 	 	el.name = atrs.join('][');
 	})
 	
 	
 }

//pro funkci ktera se vola az po nacteni loadcontent
	function load_content_param(){
        domtab.goTo(2);
    }

	$('AddEditCompanyWorkMoneySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		
			$('company_work_money_new_edit_formular').getElements('input, select').removeProperty('disabled');
			var request = new Request.JSON({
					url: $('company_work_money_new_edit_formular').action, 
					onComplete: function(json){
					   if(json.result === true){  
					     domwin.loadContent('domwin_pozice',{'onComplete': 'load_content_param'}); 
						 domwin.closeWindow('domwin_work_money');
                       }
                       else
                        alert(json.message); 
					}
			});	
			request.post($('company_work_money_new_edit_formular'));
	});
    
    //hide/unhide inputu pro nastaveni po zmene selectu
	$('company_work_money_new_edit_formular').getElements('.slide_block').each(function(item){	
		var slider_js = item.getElement('div');
		var select = item.getElement('select');
		var input = item.getElement('input');

		if(input.value == 0)
			slider_js.addClass('none');
		else select.value=1;

		select.addEvent('change', function(e){
			new Event(e).stop();
			if (slider_js.hasClass('none') != true){		
				item.getElements('input').each(function(inputy){inputy.value = 0;});
                slider_js.addClass('none');
			}else{
			   if(select.id == 'SelectStravenky'){close_another_slider_and_null('SelectStravne');}
               if(select.id == 'SelectStravne'){close_another_slider_and_null('SelectStravenky');}
               slider_js.removeClass('none');
			}
			return false;
		});
	
	});
    
    function close_another_slider_and_null(id){
        var item = $(id).getParent('.slide_block').getElement('div');
        item.addClass('none');
        $(id).value = 0; 
        item.getElements('input').each(function(inputy){inputy.value = 0;});
    }

</script>