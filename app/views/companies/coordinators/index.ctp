<div class="win_fce top">
<a href='/companies/coordinators_edit/' class='button' id='company_contact_add_new'>Přidat uživatele</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>#ID</th>
		<th>Uživatel</th>
		<th>Přiřazen</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($coordinators_list) && count($coordinators_list) >0):?>
	<?php foreach($coordinators_list as $contact_item):?>
	<tr>
		<td><?php echo $contact_item['CompanyCoordinator']['id'];?></td>
		<td><?php echo $contact_item['CmsUser']['name'];?></td>
		<td><?php echo $fastest->czechDateTime($contact_item['CompanyCoordinator']['created']);?></td>
		<td>
			<a title='Editace položy' 	class='ta edit' href='/companies/coordinators_edit/<?php echo $this->data['Company']['id'];?>/<?php echo $contact_item['CompanyCoordinator']['id'];?>'/>edit</a>
			<a title='Do kosiku' 		class='ta trash' href='/companies/coordinators_trash/<?php echo $contact_item['CompanyCoordinator']['id'];?>'/>trash</a>
		</td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této společnosti nebyli nadefinováni koordinátoři</td>
	</tr>
	<?php endif;?>
</table>
<script>
	$('company_contact_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_kontakty_add',
			sizes		: [580,320],
			scrollbars	: true,
			title		: 'Přidání nového kontaktu',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href+$('CompanyId').value+'/',
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
	
	$('table_kontakty').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tohoto uživatele?')){
			new Request.JSON({
				url:this.href,
				onComplete: (function(){
				    this.getParent('tr').dispose();
				}).bind(this)
			}).send();
		}
	});
	
	$('table_kontakty').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_kontakty_add',
			sizes		: [580,320],
			scrollbars	: true,
			title		: 'Editace kontaktu',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>