<form action='/companies/tasks_edit/' method='post' id='company_tasks_edit_formular'>
	<div class="domtabs admin_dom2_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1" id='first_domtab'>Úkol</a></li>
		<?php if(!empty($this->data['CompanyTask']['id']) && isset($this->data['CompanyTask']['complate']) && $this->data['CompanyTask']['complate'] == 0 && $this->data['CompanyTask']['cms_user_id'] == $logged_user['CmsUser']['id']):?>
			<li class="ousko"><a href="#krok2" id='do_close_taks'>Uzavřít úkol - nová aktivita</a></li>
		<?php endif;?>
		</ul>
	</div>
	<div class="domtabs admin_dom2">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('CompanyTask/id');?>
			<?php echo $htmlExt->hidden('CompanyTask/company_id');?>
			<fieldset>
			<?php if((isset($this->data['CompanyTask']['complate']) && $this->data['CompanyTask']['complate'] == 1) || (isset($this->data['CompanyTask']['id']) && $this->data['CompanyTask']['creator_id'] != $logged_user['CmsUser']['id'])):?>
				<legend>Informace o úkolu</legend>
				<?php echo $htmlExt->hidden('CompanyTask/name');?> <br class="clear">
				<?php echo $htmlExt->var_text('CompanyTask/name',array('id'=>'CompanyTaskNameHidden','tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php echo $htmlExt->var_text('CompanyTask/termin',array('show_type'=>'date','tabindex'=>1,'label'=>'Termín'));?> <br class="clear">
					<?php echo $htmlExt->var_text('CmsUser/name',array('tabindex'=>2,'label'=>'Pro'),null,false);?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->hidden('CompanyTask/setting_task_type_id', array('id'=>'CompanyTaskSettingTaskTypeId'));?> 
					<?php echo $htmlExt->var_text('SettingActivityType/name',array('id'=>'CompanyTaskSettingTaskTypeIdHidden','tabindex'=>2,'label'=>'Typ úkolu'));?> <br class="clear">
					<?php echo $htmlExt->var_text('Company/name', array('tabindex'=>2,'label'=>'Firma'));?> <br class="clear">
				</div>
				<br class='clear' />
				<?php echo $htmlExt->var_text('CompanyTask/text',array('tabindex'=>1,'label'=>'Popis','class'=>'long','label_class'=>'long'));?><br class='clear' />
			<?php else:?>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('CompanyTask/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php echo $htmlExt->inputDate('CompanyTask/termin',array('tabindex'=>1,'label'=>'Termín'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CompanyTask/cms_user_id',$cms_user_list,(!isset($this->data['CompanyTask']['cms_user_id'])?$logged_user['CmsUser']['id']:null),array('tabindex'=>2,'label'=>'Pro'),null,false);?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('CompanyTask/setting_task_type_id',$company_activity_type_list,null,array('tabindex'=>2,'label'=>'Typ úkolu'));?> <br class="clear">
					<?php echo $htmlExt->var_text('Company/name', array('tabindex'=>2,'label'=>'Firma'));?> <br class="clear">
				</div>
				<br class='clear' />
				<?php echo $htmlExt->textarea('CompanyTask/text',array('tabindex'=>1,'label'=>'Popis','class'=>'long','label_class'=>'long'));?><br class='clear' />
			<?php endif;?>
			</fieldset>
		</div>
	<?php if(!empty($this->data['CompanyTask']['id']) && isset($this->data['CompanyTask']['complate']) && $this->data['CompanyTask']['complate'] == 0):?>
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('CompanyActivity/company_id',array('value'=>$this->data['CompanyTask']['company_id']));?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('CompanyActivity/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php echo $htmlExt->inputDate('CompanyActivity/activity_datetime',array('tabindex'=>1,'label'=>'Datum'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CompanyActivity/company_contact_id',$comapany_contact_list,null,array('tabindex'=>3,'label'=>'Kontakt'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyActivity/mesto',array('value'=>$this->data['Company']['mesto'],'tabindex'=>2,'label'=>'Město'));?> <br class="clear">
					<?php echo $htmlExt->selectTag('CompanyActivity/company_activity_type_id',$company_activity_type_list,null,array('tabindex'=>4,'label'=>'Typ aktivity'));?> <br class="clear">
				</div>
				<br class='clear' />
				<?php echo $htmlExt->textarea('CompanyActivity/text',array('tabindex'=>5,'label'=>'Popis','class'=>'long','label_class'=>'long'));?><br class='clear' />
			</fieldset>
		</div>
	<?php endif;?>
	</div>
	<div class='formular_action'>
		<?php if(
				!isset($this->data['CompanyTask']['id'])  // pokud jde o novy zaznam
			|| 
				(isset($this->data['CompanyTask']['complate']) && $this->data['CompanyTask']['complate'] == 0 && $this->data['CompanyTask']['creator_id'] == $logged_user['CmsUser']['id']) // pokud je a neni uzavren
			):?>
		<input type='button' value='Uložit' 		id='AddEditCompanyTaskSaveAndClose' />
		<?php endif;?>
		<input type='button' value='Zavřít' 		id='AddEditCompanyTaskClose'/>
		<input type='button' value='Uzavřít úkol' 	id='AddEditCompanyTaskCloseTask' class='none'/>
		<input type='button' value='Zpět' 			id='AddEditCompanyTaskBack' class='none'/>
	</div>
</form>
<script>

	var domwin_name = '<?php echo $domwin_name;?>';
	var domtab2 = new DomTabs({'className':'admin_dom2'}); 
	
	$('AddEditCompanyTaskClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow(domwin_name);});
	
	
	if($('AddEditCompanyTaskCloseTask'))
		$('AddEditCompanyTaskCloseTask').addEvent('click', function(e){
			new Event(e).stop();
			if (domwin_name == 'domwin') { // ze strany reportu
				var request = new Request.JSON({url: '/companies/tasks_complate/', onComplete:function(){ click_refresh($('CompanyTaskId').value); domwin.closeWindow(domwin_name);}});
			} else {
				var request = new Request.HTML({url: '/companies/tasks_complate/', update: $('domwin_tasks').getElement('.CB_ImgContainer'), onComplete:function(){domwin.closeWindow(domwin_name);}});
			}
			request.post($('company_tasks_edit_formular'));
		});
	
	if($('AddEditCompanyTaskBack'))
		$('AddEditCompanyTaskBack').addEvent('click', function(e){
			new Event(e).stop();
			if($('AddEditCompanyTaskSaveAndClose'))	$('AddEditCompanyTaskSaveAndClose').removeClass('none');
			if($('AddEditCompanyTaskClose'))		$('AddEditCompanyTaskClose').removeClass('none');
			if($('AddEditCompanyTaskCloseTask'))	$('AddEditCompanyTaskCloseTask').addClass('none');
			if($('AddEditCompanyTaskBack'))			$('AddEditCompanyTaskBack').addClass('none');
			domtab2.goTo(0);
		});
	
	if($('do_close_taks'))
		$('do_close_taks')
			.removeEvents('click')
			.addEvent('click', function(e){
				new Event(e).stop();
				domtab2.goTo(1);
				$('CompanyActivityCompanyActivityTypeId').value = $('CompanyTaskSettingTaskTypeId').value;
				$('CompanyActivityName').value = 'Vyřešeno: ' + $('CompanyTaskName').value;
				
				if($('AddEditCompanyTaskSaveAndClose')) $('AddEditCompanyTaskSaveAndClose').addClass('none');
				if($('AddEditCompanyTaskClose')) 		$('AddEditCompanyTaskClose').addClass('none');
				if($('AddEditCompanyTaskCloseTask'))	$('AddEditCompanyTaskCloseTask').removeClass('none');
				if($('AddEditCompanyTaskBack'))			$('AddEditCompanyTaskBack').removeClass('none');
				
				$('first_domtab')
					.removeEvents('click')
					.addEvent('click', function(e){
						new Event(e).stop();
						if($('AddEditCompanyTaskSaveAndClose'))		$('AddEditCompanyTaskSaveAndClose').removeClass('none');
						if($('AddEditCompanyTaskClose'))			$('AddEditCompanyTaskClose').removeClass('none');
						if($('AddEditCompanyTaskCloseTask'))		$('AddEditCompanyTaskCloseTask').addClass('none');
						if($('AddEditCompanyTaskBack'))				$('AddEditCompanyTaskBack').addClass('none');
						domtab2.goTo(0);
					})
			});
	
	if(domwin.get_data(domwin_name,'from') == 'activity_edit'){
		var request = new Request.JSON({url:$('company_tasks_edit_formular').action + '?from=activity_edit',onComplete:function(){alert('Nový úkol byl uložen.');domwin.closeWindow('domwin_tasks_add')}});
	} else {
		if (domwin_name == 'domwin') { // ze strany reportu	
			var request = new Request.JSON({url:$('company_tasks_edit_formular').action, onComplete:function(){click_refresh($('CompanyTaskId').value);domwin.closeWindow(domwin_name); }});
		} else {
			var request = new Request.HTML({url:$('company_tasks_edit_formular').action, update: $('domwin_tasks').getElement('.CB_ImgContainer'), onComplete:function(){domwin.closeWindow('domwin_tasks_add'); }});
		}
	}

	if($('AddEditCompanyTaskSaveAndClose'))
		$('AddEditCompanyTaskSaveAndClose').addEvent('click',function(e){
			
			new Event(e).stop();
			valid_result = validation.valideForm('company_tasks_edit_formular');
			if (valid_result == true){
				request.post($('company_tasks_edit_formular'));
			} else {
				var error_message = new MyAlert();
				error_message.show(valid_result)
			}
		});
	
	validation.define('company_tasks_edit_formular',{
		'CompanyTaskName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název úkolu'},
		}
	});
	validation.generate('company_tasks_edit_formular',<?php echo (isset($this->data['CompanyTask']['id']))?'true':'false';?>);
</script>