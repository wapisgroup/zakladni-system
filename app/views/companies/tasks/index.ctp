<div class="win_fce top">
<a href='/companies/tasks_edit/<?php echo $company_id;?>' class='button' id='company_contact_add_new'>Přidat úkol</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>Firma</th>
		<th>Název</th>
		<th>Zadal</th>
	<?php if($logged_user['CmsGroup']['id']	== 1 || $logged_user['CmsGroup']['id'] == 5):?>
		<th>Pro</th>
	<?php endif;?>
		<th>Termín</th>
		<th>Splněno</th>
		<th>Datum splnění</th>
		<th>Datum vytvoření</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($contact_list) && count($contact_list) >0):?>
	<?php foreach($contact_list as $contact_item):?>
	<tr>
		<td><?php echo $contact_item['Company']['name'];?></td>
		<td title='<?php echo $contact_item['CompanyTask']['name'];?>'><?php echo $fastest->orez($contact_item['CompanyTask']['name'],40);?></td>
		<td><?php echo $contact_item['CmsUserFrom']['name'];?></td>
	<?php  if($logged_user['CmsGroup']['id']	== 1 || $logged_user['CmsGroup']['id'] == 5):?>
		<td><?php echo $contact_item['CmsUserTo']['name'];?></td>
	<?php  endif;?>
		<td><?php echo $fastest->czechDate($contact_item['CompanyTask']['termin']);?></td>
		<td><?php echo $fastest->YN($contact_item['CompanyTask']['complate']);?></td>
		<td><?php 
			if ($contact_item['CompanyTask']['complate'] == 1)
				if($contact_item['CompanyTask']['complate_date'] == '0000-00-00')
					echo 'closed in the old vers.';
				else
					echo $fastest->czechDate($contact_item['CompanyTask']['complate_date']);
		?></td>
		<td><?php echo $fastest->czechDate($contact_item['CompanyTask']['created']);?></td>
		<td>
			<a title='Editace položky' 	class='ta edit' href='/companies/tasks_edit/<?php echo $company_id;?>/<?php echo $contact_item['CompanyTask']['id'];?>'/>edit</a>
			<?php if(in_array($logged_group,array(1,5))){?>
            <a title='Odstranit položku'class='ta trash' href='/companies/tasks_trash/<?php echo $company_id;?>/<?php echo $contact_item['CompanyTask']['id'];?>'/>trash</a>
		    <?php } ?>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='<?php echo (($logged_user['CmsGroup']['id'] == 1 || $logged_user['CmsGroup']['id'] == 5)?7:6);?>'>K této společnosti nebyl nadefinován úkol.</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($contact_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyTaskClose' class='button'/>
	</div>
<script>
	$('ListCompanyTaskClose').addEvent('click', function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin_tasks');
	});

	$('company_contact_add_new').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_tasks_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: 'Přidání nového úkolu',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
			}); 
	});
	
	$('table_kontakty').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tento úkol?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin_tasks').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
	
	$('table_kontakty').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_tasks_add',
			sizes		: [580,390],
			scrollbars	: true,
			title		: 'Editace tasks',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>