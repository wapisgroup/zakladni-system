<form action='/companies/order_close/' method='post' id='uzavrit'>
	<?php echo $htmlExt->hidden('CompanyOrderItem/id', array('value'=>$order_id));?>
    <fieldset>
			<?php echo $htmlExt->selectTag('CompanyOrderItem/type_close', array(2=>'Uzavřít objednávku',3=>'Zrušit objednávku'),null,array('label'=>'Typ', 'class'=>'long','label_class'=>'long'),null,false);?><br />
            <br />
			<?php echo $htmlExt->textarea('CompanyOrderItem/closed_comment',array('label'=>'Komentář', 'class'=>'long','label_class'=>'long'));?><br />
	
    
    </fieldset>
    <div class='formular_action'>
        <input type='button' value='Uložit' id='Save'/>
        <input type='button' value='Zavřít' id='AddEditUzavriClose'/>
    </div>
</form>	
<script>
	$('AddEditUzavriClose').addEvent('click',function(e){new Event(e).stop(); 
		domwin.closeWindow('domwin_uzavrit');
	});
    
    $('Save').addEvent('click',function(e){
        new Event(e).stop(); 
    	new Request.JSON({
	   				url: '/companies/order_close/' + $('CompanyOrderItemId').value +'/'+ $('CompanyOrderItemTypeClose').value,
	   				data: {'data[text]':$('CompanyOrderItemClosedComment').value },
                    onComplete: function(json){
	   					if (json) {
	   						if (json.result === true) {
	   							domwin.loadContent('domwin_pozice');
	   							domwin.closeWindow('domwin_uzavrit');
	   						}
	   						else {
	   							alert(json.message);
	   						}
	   					}
	   					else {
	   						alert('Chyba apliakce');
	   					}
	   				}
	   			}).send();	
    });
</script>	