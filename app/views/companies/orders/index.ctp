<div class="win_fce top">
<a href='/companies/order_edit/<?php echo $company_id;?>' class='button edit' id='company_order_add_new' title='Přidání nové objednávky'>Přidat objednávku</a>
</div>
<table class='table' id='table_orders'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Typ</th>
		<th>Datum objednávky</th>
		<th>Datum nástupu</th>
		<th>Objednávka</th>
		<th>Statistika</th>
		<th>Status</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($order_list) && count($order_list) >0):?>
    	<?php foreach($order_list as $order_item):?>
    	<tr class="<?php if($order_item['0']['expire'] == 1) echo "color_red";?>">
    		<td><?php echo $order_item['CompanyOrderItem']['id'];?></td>
    		<td><?php echo $order_item['CompanyOrderItem']['name'];?></td>
    		<td><?php echo $order_type_list[$order_item['CompanyOrderItem']['order_type']];?></td>

    	    <td><?php echo $fastest->czechDate($order_item['CompanyOrderItem']['order_date']);?></td>
    	    <td><?php echo $fastest->czechDateTime($order_item['CompanyOrderItem']['start_datetime']);?></td>
    	
            <td><?php echo $order_item['0']['objednavka'];?></td>
            <td><?php echo $order_item['0']['statistika'];?></td>
    		
            <td><?php echo $order_stav_list[$order_item['CompanyOrderItem']['order_status']];?></td>
    		<td>
    			<a title='Editace objednávky "<?php echo $order_item['CompanyOrderItem']['name'];?>"' 	class='ta edit' href='/companies/order_edit/<?php echo $company_id;?>/<?php echo $order_item['CompanyOrderItem']['id'];?>'/>edit</a>
    		<?php if($order_item['CompanyOrderItem']['order_status'] == -1 ){?>
  	 			<a title='Vytvořit závaznout objednávku "<?php echo $order_item['CompanyOrderItem']['name'];?>"' 	class='ta order_definite' href='/companies/order_definite/<?php echo $order_item['CompanyOrderItem']['id'];?>'/>zavazna</a>
              
            <?php }
             if($order_item['CompanyOrderItem']['order_status'] == 1){ ?>
                <a title='Vytvořit kopii objednávky "<?php echo $order_item['CompanyOrderItem']['name'];?>"' 	class='ta order_generate' href='/companies/order_edit/<?php echo $company_id;?>/<?php echo $order_item['CompanyOrderItem']['id'];?>/-1/1'/>generate</a>
                <a title='Uzavřít objednávku "<?php echo $order_item['CompanyOrderItem']['name'];?>"' 	class='ta order_finally' href='/companies/close_order_domwin/<?php echo $order_item['CompanyOrderItem']['id'];?>'/>finally</a>
            <?php }
            if($logged_user['CmsGroup']['id'] == 1){
            ?>
            	<a title='Odstranit objednávku 'class='ta trash' href='/companies/order_trash/<?php echo $order_item['CompanyOrderItem']['id'];?>'/>trash</a>
    		<?php }?>
            <!--
            	<a title='WWW objednávka 'class='ta show' href='/companies/www_objednavka/<?php //echo $company_id;?>/<?php //echo $order_item['CompanyOrderItem']['id'];?>'/>WWW</a>
    		-->
            
            </td>
    	</tr>
    	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>K této společnosti nebyla nadefinována objednávka</td>
	</tr>
	<?php endif;?>
</table>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyTemplateClose' class='button'/>
	</div>
<script>
	$('ListCompanyTemplateClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_pozice');});

	$('table_orders').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto objednávku?')){
			new Request.HTML({
				url:this.href,
				//update: $('domwin_pozice').getElement('.CB_ImgContainer'),
				onComplete: function(){
				    domwin.loadContent('domwin_pozice');
				}
			}).send();
		}
	});
	
	$('domwin_pozice').getElements('.edit, .order_generate, .show').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_pozice_add',
			sizes		: [880,625],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
    
    /*
	 * Uzavreno objednavky, posleze jiz nepude tato objednavka
	 * publikovana
	 */
	if ($('domwin_pozice').getElements('.order_finally')){
		$('domwin_pozice').getElements('.order_finally').addEvent('click',function(e){
	       	e.stop();
	
    		domwin.newWindow({
    			id			: 'domwin_uzavrit',
    			sizes		: [600,350],
    			scrollbars	: true,
    			title		: 'Jak chcete uzavřít tuto objednávku?',
    			languages	: false,
    			type		: 'AJAX',
    			ajax_url	: this.href,
    			closeConfirm: true,
    			max_minBtn	: false,
    			modal_close	: false,
    			remove_scroll: false
    		}); 
		});
	}
    
    if ($('domwin_pozice').getElements('.order_definite')){
		$('domwin_pozice').getElements('.order_definite').addEvent('click',function(e){
	       	e.stop();
	
    		domwin.newWindow({
    			id			: 'domwin_definite',
    			sizes		: [600,600],
    			scrollbars	: true,
    			title		: 'Vytvořit závaznou objednávku?',
    			languages	: false,
    			type		: 'AJAX',
    			ajax_url	: this.href,
    			closeConfirm: true,
    			max_minBtn	: false,
    			modal_close	: false,
    			remove_scroll: false
    		}); 
		});
	}
    
</script>