<form action='/companies/www_objednavka/' method='post' id='company_template_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyOrderItem/company_id', array('value'=>$company_id));?>
	<?php echo $htmlExt->hidden('CompanyOrderItem/id');?>
	<?php echo $htmlExt->hidden('CompanyOrderItem/order_status'); ?>

	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">WWW nabídka</a></li>
        </ul>
	</div>
	<div class="domtabs admin_dom">
		
         <div class="domtabs field">
            <fieldset>
                <legend>Informace pro www stránky</legend>
               		
                    <div class="sll">
                       <?php echo $htmlExt->checkbox('CompanyOrderItem/www_show',null,array('label'=>'Zobrazovat na WWW stránce'));?><br /> 
                    </div>
                    <div class="slr">
                    
                    </div>
                    <br />
                    <?php echo $htmlExt->input('CompanyOrderItem/www_name', array('class'=>'long','label'=>'Název pozice','label_class'=>'long'));?> <br />
               		<?php echo $htmlExt->textarea('CompanyOrderItem/www_napln', array('class'=>'long','label'=>'Náplň práce','label_class'=>'long'));?> <br />
               		<?php echo $htmlExt->textarea('CompanyOrderItem/www_pozadujeme', array('class'=>'long','label'=>'Požadujeme','label_class'=>'long'));?> <br />
               		<?php echo $htmlExt->textarea('CompanyOrderItem/www_dalsi_informace', array('class'=>'long','label'=>'Další informace','label_class'=>'long'));?> <br />
		
            </fieldset>
            <?php //pr($this->data)?>
         </div> 
         
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyOrderItemSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyOrderItemClose'/>
	</div>
</form>
<script>
	/*
	 * Zakladni funkce
	 */
	var domtab = new DomTabs({'className':'admin_dom'}); 
    $$('.integer, .float').inputLimit();
    
   
	/*
	 * Zavreni Domwinu, bez ulozeni
	 */
	$('AddEditCompanyOrderItemClose').addEvent('click',function(e){new Event(e).stop(); 
		domwin.closeWindow('domwin_pozice_add');
	});
	

    
        
    
	/*
	 * Ulozeni www objednavky
	 */
	if ($('AddEditCompanyOrderItemSaveAndClose')) {
	   	$('AddEditCompanyOrderItemSaveAndClose').addEvent('click', function(e){
	   		new Event(e).stop();
	            button_preloader($('AddEditCompanyOrderItemSaveAndClose'));
	   			new Request.JSON({
	   				url: $('company_template_edit_formular').action,
	   				onComplete: function(json){
	   					if (json) {
	   						if (json.result === true) {
	                            button_preloader_disable($('AddEditCompanyOrderItemSaveAndClose'));
	   							domwin.closeWindow('domwin_pozice_add');
       						}
	   						else {
	   							alert(json.message);
	   						}
	   					}
	   					else {
	   						alert('Chyba apliakce');
	   					}
	   				}
	   			}).post($('company_template_edit_formular'));
	   	
	   });
	}
	
	
</script>