<form action='/companies/order_definite/<?php echo $order_id;?>' method='post' id='uzavrit'>
	 <fieldset>
			<?php echo $htmlExt->inputDateTime('CompanyOrderItem/start_datetime',array('label'=>'Nástup (datum/čas)', 'class'=>'','label_class'=>''));?><br />
     </fieldset>
     <br />
     <fieldset>
        <legend>Čekací listina</legend>
        <table class="table">
        <?php
            	if (isset($cekaci_listina) && count($cekaci_listina) > 0){
						foreach($cekaci_listina as $cl):
						?>
						<tr>
							<td><?php echo $htmlExt->checkbox('id/'.$cl['ConnectionClientRequirement']['id']);?></td>
                            <td><?php echo $cl['Client']['name'];?></td>
                    	</tr>
				<?php 
						endforeach;
				}
				else echo "<tr id='NoneSearch'><td></td>Žádní klienti na čakcí listině.</td></tr>"; 
        ?>
        </table>
     </fieldset>
    <div class='formular_action'>
        <input type='button' value='Uložit' id='Save'/>
        <input type='button' value='Zavřít' id='AddEditUzavriClose'/>
    </div>
</form>	
<script>
	$('AddEditUzavriClose').addEvent('click',function(e){new Event(e).stop(); 
		domwin.closeWindow('domwin_definite');
	});
    
    $('Save').addEvent('click',function(e){
        new Event(e).stop(); 
    	new Request.JSON({
	   				url: $('uzavrit').action,
                    onComplete: function(json){
	   					if (json) {
	   						if (json.result === true) {
	   							domwin.loadContent('domwin_pozice');
	   							domwin.closeWindow('domwin_definite');
	   						}
	   						else {
	   							alert(json.message);
	   						}
	   					}
	   					else {
	   						alert('Chyba apliakce');
	   					}
	   				}
	   			}).post($('uzavrit'));	
    });
</script>	