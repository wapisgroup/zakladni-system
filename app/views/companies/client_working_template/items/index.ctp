<div class="win_fce top">
<a href='/companies/cw_template_item_edit/<?php echo $company_id;?>/<?php echo $template_id;?>/' class='button edit' id='company_contact_add_new' title='Přidání'>Přidat</a>
</div>
<table class='table' id='table_kontakty'>
	<tr>
		<th>Rok</th>
        <th>Měsíc</th>
		<th>Vytvořeno</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($template_items_list) && count($template_items_list) >0):?>
	<?php foreach($template_items_list as $item):?>
	<tr>
		<td><?php echo $item['CompanyClientWorkingTemplateItem']['rok'];?></td>
        <td><?php echo $item['CompanyClientWorkingTemplateItem']['mesic'];?></td>
		<td><?php echo $fastest->czechDate($item['CompanyClientWorkingTemplateItem']['created']);?></td>
		<td>
			<a title='Editace položky' 	class='ta edit' href='/companies/cw_template_item_edit/<?php echo $company_id;?>/<?php echo $template_id;?>/<?php echo $item['CompanyClientWorkingTemplateItem']['id'];?>'/>edit</a>         
            <?php if(in_array(6,array(1,5))){?>
            <a title='Odstranit položku'class='ta trash' href='/companies/cw_template_item_trash/<?php echo $company_id;?>/<?php echo $contact_item['CompanyActivity']['id'];?>'/>trash</a>
		    <?php } ?>
        </td>
	</tr>
	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='7'>K této šabloně nebyly nadefinovány žádne položky docházek</td>
	</tr>
	<?php endif;?>
</table>
<?php // pr($contact_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='CWItemClose' class='button'/>
	</div>
<script>
	$('CWItemClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_cw_templates');});

	$('domwin_cw_templates').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto aktivitu?')){
			new Request.HTML({
				url:this.href,
				update: $('domwin').getElement('.CB_ImgContainer'),
				onComplete: function(){}
			}).send();
		}
	});
    
	$('domwin_cw_templates').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_cw_item_edit',
			sizes		: [700,500],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});

    
</script>