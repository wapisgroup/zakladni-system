<form action='/companies/cw_template_item_edit/' method='post' id='mail_templates_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyClientWorkingTemplateItem/id');?>
	<?php echo $htmlExt->hidden('CompanyClientWorkingTemplateItem/company_id');?>
    <?php echo $htmlExt->hidden('CompanyClientWorkingTemplateItem/company_client_working_template_id');?>
    <?php echo $htmlExt->hidden('stat_id',array('value'=>$stat_id));?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní informace</legend>
				<div class='sll'>
            		<?php echo $htmlExt->selectTag('CompanyClientWorkingTemplateItem/mesic',$mesice_list2,$month,array('label'=>'Měsíc', 'class'=>'odpracovane_hodiny_change_date'));?> <br/>
                    <?php echo $htmlExt->selectTag('CompanyClientWorkingTemplateItem/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost', 'class'=>'odpracovane_hodiny_change_date'),null,false);?> <br /> 
				
            	</div>
                <div class='slr'>
                	<?php echo $htmlExt->selectTag('CompanyClientWorkingTemplateItem/rok', $actual_years_list,$year,array('label'=>'Rok', 'class'=>'odpracovane_hodiny_change_date'),null,true);?>
            	</div>
                <div id="odpracovane_hodiny_element">
                	<?php echo $this->renderElement('../companies/client_working_template/items/element');?>
                </div> 			
            
            </fieldset>			
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	$$('.wysiwyg').makeWswg();
	var domtab = new DomTabs({'className':'admin_dom'}); 
    
    if($('CompanyClientWorkingTemplateItemId').value != ''){
        $('CompanyClientWorkingTemplateItemRok').setProperty('disabled','disabled');
        $('CompanyClientWorkingTemplateItemMesic').setProperty('disabled','disabled');
        $('CompanyClientWorkingTemplateItemSettingShiftWorkingId').setProperty('disabled','disabled');
    }
    
    // init event for onChange year and month
	$$('.odpracovane_hodiny_change_date').addEvents({
		'change': function(e){
			$('odpracovane_hodiny_element').fade(0);
			if ($('CompanyClientWorkingTemplateItemRok').value != '' && $('CompanyClientWorkingTemplateItemMesic').value != ''){
				new Request.HTML({
					url: '/companies/cw_template_item_render_element/' + $('CompanyClientWorkingTemplateItemRok').value + '/' + $('CompanyClientWorkingTemplateItemMesic').value+ '/' + $('CompanyClientWorkingTemplateItemSettingShiftWorkingId').value+'/' + $('StatId').value,
					update: $('odpracovane_hodiny_element'),
					onComplete: function(){
						$('odpracovane_hodiny_element').fade(1);
					}
				}).send();
			} else {
				new Event(e).stop();
				//this.options[this.currentSelection].selected = 'selected';
				$('odpracovane_hodiny_element').fade(1);
			}
		}
	});

	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
        $('CompanyClientWorkingTemplateItemRok').removeProperty('disabled','disabled');
        $('CompanyClientWorkingTemplateItemMesic').removeProperty('disabled','disabled');
        $('CompanyClientWorkingTemplateItemSettingShiftWorkingId').removeProperty('disabled','disabled');
			new Request.JSON({
				url:$('mail_templates_edit_formular').action,		
				onComplete:function(json){
				    if(json){
				        if(json.result == true){
				            domwin.loadContent('domwin_cw_templates');
	   				        domwin.closeWindow('domwin_cw_item_edit');
				        }
                        else{
                            alert(json.message);
                        }
				    }
                    $('CompanyClientWorkingTemplateItemRok').setProperty('disabled','disabled');
                    $('CompanyClientWorkingTemplateItemMesic').setProperty('disabled','disabled');
                    $('CompanyClientWorkingTemplateItemSettingShiftWorkingId').setProperty('disabled','disabled');
				}
			}).post($('mail_templates_edit_formular'));
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); $$('.wysiwyg').killWswg(); domwin.closeWindow('domwin_cw_item_edit');});
</script>