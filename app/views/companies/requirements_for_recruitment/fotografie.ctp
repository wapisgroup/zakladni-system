<br />
<script>
	function upload_file_complete(url){
		var ul = $('foto_list');
		var li = new Element('li',{'class':'obal_span'}).inject(ul);
		new Element('img', {src:'/uploaded/requirement/foto/small/' + url,'class':'nahrada_za_foto'}).inject(li);
		
		new Element('input', {type:'hidden', name:'data[RequirementsForRecruitment][imgs][]', value:url + '|', 'class':'file_caption_value'}).inject(li);
		var submenu = new Element('div', {'class':'submenu'}).inject(new Element('span').inject(li));
			var zoom = new Element('a', {'href':'/uploaded/requirement/foto/large/' + url,'target':'_blank','rel':'clearbox[detail_fotogalerie]'})
				.inject(submenu)
			new Element('img', {'class':'icon',title:'Zvětšit fotografii',src:'/css/fastest/upload/zoom.png'})
				.inject(zoom)
			new Element('img', {'class':'icon',title:'Smazat fotografii',src:'/css/fastest/upload/delete.png'})
				.inject(submenu)
				.addEvent('click', img_delete_button.bind());
			new Element('img', {'class':'icon',title:'Editovat popis fotografie',src:'/css/fastest/upload/edit.png'})
				.inject(submenu)
				.addEvent('click', title_edit_button.bind());
		
		
		document.slideshow.ajax_init($('foto_list'));	
		mysort.addItems(li);
		
		$('RequirementsForRecruitmentFotoUploader').getElement('.input_file_over').value = '';
		$('RequirementsForRecruitmentFotoUploader').getElements('.browse_icon, .input_file').setStyle('display','inline');
		$('RequirementsForRecruitmentFotoUploader').getElement('.smazat_icon').setStyle('display','none');
	}
</script>
<div>
	<?php $fileinput_setting = array(
		'label'			=> 'Fotografie',
		'upload_type' 	=> 'php',
		'filename_type'	=> 'unique',
		'file_ext'		=> array('jpg'),
		'paths' 		=> array(
			'path_to_file'	=> 'uploaded/requirement/foto/',
			//'path_to_ftp'	=> 'uploaded/reality/',
			'upload_script'	=> '/companies/requirement_upload_foto/',
			'status_path' 	=> '/get_status.php',
		),
		'methods'=>array(
			array('methods'	=>	array(array(
				'method'		=>	'resize',
				'new_width'		=>	800,
				'new_height'	=>	600
			)),
			'addon_path'=> 'large/'
			),
			array('methods'	=>	array(array(
				'method'		=>	'resize',
				'new_width'		=>	80,
				'new_height'	=>	80
			)),
			'addon_path'=> 'small/'
			)
		),
		'onComplete'	=> 'upload_file_complete'
	);
	

	
	?>
	<?php echo $fileInput->render('RequirementsForRecruitment/foto',$fileinput_setting);?>
</div>
<br /> 
<ul class='foto_list' id='foto_list' style='position:relative;'>
	<?php //pr($this->data['RequirementsForRecruitment']);
		if (isset($this->data['RequirementsForRecruitment']['imgs']) && !empty($this->data['RequirementsForRecruitment']['imgs']) && count($this->data['RequirementsForRecruitment']['imgs']) > 0){
			foreach($this->data['RequirementsForRecruitment']['imgs'] as $img){
				list($file, $caption) = explode('|',$img);
				echo "<li class='obal_span'><img src='/uploaded/requirement/foto/small/$file' title='$caption' alt='$caption' class='nahrada_za_foto tip_win'/><input class='file_caption_value' type='hidden' name='data[RequirementsForRecruitment][imgs][]' value='$img'/><span><div class='submenu'><a href='/uploaded/requirement/foto/large/$file' title='$caption' target='_blank' rel='clearbox[detail_fotogalerie]'><img src='/css/fastest/upload/zoom.png' title='Zvětšit fotografii' class='img_zoom_button icon' /></a><img src='/css/fastest/upload/delete.png' title='Smazat fotografii' class='img_delete_button icon' /><img src='/css/fastest/upload/edit.png' title='Editovat popis fotografie' class='title_edit_button icon' /></div></span></li>";
			}
		
		}
	?>

</ul>
<div id='editable_title'>
	<textarea style='width:95%'></textarea><br/>
	<p style='text-align:center'>
		<input type='button' class='button' value='Změnit' id='apply_editable_title'/>
		<input type='button' class='button' value='Zrušit' id='close_editable_title'/>
	</p>
</div>
<br />
<script>

function title_edit_button(e){
	var event = new Event(e);
	event.stop();
	$('editable_title').setStyle('display','block');
	textarea = $('editable_title').getElement('textarea');
	curr_img = event.target.getParent('li').getElement('img');
	curr_img.addClass('edit_now');
	textarea.value = curr_img.getProperty('title');
}

function img_delete_button(e){
	var event = new Event(e);
	event.stop();
	if (confirm('Opravdu si přejete odstranit tuto fotografii?'))
		event.target.getParent('li').dispose();
}

$$('.title_edit_button').addEvent('click', title_edit_button.bind());
$$('.img_delete_button').addEvent('click', img_delete_button.bind());

$('close_editable_title').addEvent('click', function(e){
	new Event(e).stop();
	$('editable_title').setStyle('display','none');
});

$('apply_editable_title').addEvent('click', function(e){
	new Event(e).stop();
	textarea = $('editable_title').getElement('textarea');
	element = $('foto_list').getElement('.edit_now');
	element
		.setProperties({
			title: textarea.value,
			alt: textarea.value
		})
		.removeClass('edit_now');
	
	hidden = element.getNext('.file_caption_value');
	hidden_text = hidden.value.split('|');
	hidden_text[1] = textarea.value;
	hidden.value = hidden_text[0] + '|' + hidden_text[1];
	textarea.value = '';
	$('editable_title').setStyle('display','none');
});

var mysort = new Sortables($('foto_list'), {
  constrain: false,
  clone: true
});

document.slideshow.ajax_init($('foto_list'));	


var Tips_help = new Tips($$('.tip_win'),{
		showDelay: 100,
		hideDelay: 400,
		className: 'tip_win',
		offsets: {'x': -3, 'y': 70},
		fixed: true
});


</script>
<br />