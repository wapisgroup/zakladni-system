			<fieldset>
				<legend>Mzdové podmínky</legend>
				<div class='sll'>
					<?php echo $htmlExt->input('RequirementsForRecruitment/salary', array('label'=>'Mzda v čistém od-do'));?> <br class='clear' />
					<?php echo $htmlExt->textarea('RequirementsForRecruitment/mzdove_podminky_poznamka', array('label'=>'Komentář ke mzdě'));?> <br />
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/character_of_payment_list', $charakter_mzdy_list,null, array('label'=>'Charakter mzdy'));?> <br class='clear' />
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/including_of_accommodation', $ano_ne_list,null, array('label'=>'Hrazené ubytování'));?> <br class='clear' />
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/including_of_transport', $ano_ne_list,null, array('label'=>'Hrazená doprava'));?> <br class='clear' />
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/including_of_meals_ticket', $ano_ne_list,null, array('label'=>'Stravenky'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<fieldset>
						<legend>Forma zaměstnání</legend>
						<label>Pracovní smlouva:</label><?php echo $htmlExt->checkbox('RequirementsForRecruitment/checkbox_ps', array('label'=>'Pracovní smlouva'));?> <br />
						<label>Dohoda:</label><?php echo $htmlExt->checkbox('RequirementsForRecruitment/checkbox_do', array('label'=>'Dohoda'));?> <br />
						<label>Smlouva o dílo:</label><?php echo $htmlExt->checkbox('RequirementsForRecruitment/checkbox_sod', array('label'=>'Smlouva o dílo'));?> <br />
					</fieldset>
				</div>
			</fieldset>