<fieldset>
<legend>Souhrné info</legend>
<div class='sl3'>
<?php 

	echo $htmlExt->input('RequirementsForRecruitment/name', array('label'=>'Požadovaná profese', 'class'=>'long', 'label_class'=>'long')).'<br />';
	echo $htmlExt->input('RequirementsForRecruitment/job_city', array('label'=>'Pracoviště město','class'=>'long', 'label_class'=>'long')).'<br />';
	echo $htmlExt->textarea('RequirementsForRecruitment/comment', array('label'=>'Poznámka k volnému místu', 'class'=>'long', 'label_class'=>'long')).'<br />';
	echo $htmlExt->textarea('RequirementsForRecruitment/napln_prace', array('label'=>'Náplň práce', 'class'=>'long', 'label_class'=>'long')).'<br />';
	echo $htmlExt->input('RequirementsForRecruitment/salary', array('label'=>'Mzda v čistém od-do', 'class'=>'long', 'label_class'=>'long')).'<br />';
	echo $htmlExt->textarea('RequirementsForRecruitment/mzdove_podminky_poznamka', array('label'=>'Komentář ke mzdě','class'=>'long', 'label_class'=>'long')).'<br />';
	echo $htmlExt->selectTag('RequirementsForRecruitment/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost','class'=>'long', 'label_class'=>'long'))."<br />";
	echo $htmlExt->textarea('RequirementsForRecruitment/komentar_smennosti', array('label'=>'Komentář k směnnosti', 'class'=>'long', 'label_class'=>'long')).'<br />';
?>
</div>
</fieldset>
<script>

<?php if($print) {?>

	$('myDisable').getElements('label').each(function(item){
	next = item.getNext() ;
	var br = new Element('br');

	//console.log(item.getNext().get('tag'));
	//alert(item.getNext().get('tag'));
		if(next.get('tag') == 'textarea'){
			text = next.value;
			next.dispose();
			newtext = new Element('p').setHTML(text);
			newtext.inject(item,'after');
			br.inject(item,'after');
		}
		else if(next.get('tag') == 'input'){
			if(next.getProperty('type') == 'text'){
				text = next.value;
				next.dispose();
				newtext = new Element('span').setHTML(text);
				newtext.inject(item,'after');
			}
			else{
				next = next.getNext();
				text = (next.checked ? ' Ano' : ' Ne');
				next.dispose();
				newtext = new Element('span').setHTML(text);
				newtext.inject(item,'after');
			}
		}
		else if(next.get('tag') == 'select'){		
			text = next.getOptionText();
			next.dispose();
			newtext = new Element('span').setHTML(text);
			newtext.inject(item,'after');
		}

	});

	


	//window.print();

<?php }?>


</script>
