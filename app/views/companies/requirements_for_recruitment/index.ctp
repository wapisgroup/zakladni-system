<div id='CompanyWorkPositionObal'>
	<div class="win_fce top">
		<a href='/companies/nabor_edit/<?php echo $company_id;?>' class='button edit add_edit_item' id='company_nabor_add_new' title='Přidání požadavku na nábor'>Přidání požadavku na nábor</a>
	</div>
	<table class='table' id='table_kontakty'>
		<tr>
			<th>#ID</th>
			<th>Název</th>
			<th>Počet míst</th>
			<th>Počet náhradníků</th>
			<th>Nabízená mzda</th>
			<th>Možnosti</th>
		</tr>
		<?php if (isset($items) && count($items) >0):?>
		<?php foreach($items as $item):?>
		<tr>
			<td><?php echo $item['RequirementsForRecruitment']['id'];?></td>
			<td><?php echo $item['RequirementsForRecruitment']['name'];?></td>
			<td><?php echo $item['RequirementsForRecruitment']['count_of_free_position'];?></td>
			<td><?php echo $item['RequirementsForRecruitment']['count_of_substitute'];?></td>
			<td><?php echo $item['RequirementsForRecruitment']['salary'];?></td>
			<td>
				<a class='ta edit add_edit_item' title='Editace položky náboru <?php echo $item['RequirementsForRecruitment']['name'];?>' href='/companies/nabor_edit/<?php echo $company_id;?>/<?php echo $item['RequirementsForRecruitment']['id'];?>'/>Editace</a>
			</td>
		</tr>
		<?php endforeach;?>
		<?php else:?>
		<tr>
			<td colspan='5'>K této společnosti nebyl nadefinován náborový požadavek</td>
		</tr>
		<?php endif;?>
	</table>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyWorkPositionClose' class='button'/>
	</div>
</div>
<?php //pr($work_position_list);?>
<script>



	$('ListCompanyWorkPositionClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_nabor_list')});
	
	$('CompanyWorkPositionObal').getElements('.add_edit_item').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_nabor_edit',
			sizes		: [680,520],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
	
</script>