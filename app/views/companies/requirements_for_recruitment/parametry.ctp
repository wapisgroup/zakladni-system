<?php echo $htmlExt->hidden('RequirementsForRecruitment/id');?>
			<?php // echo $htmlExt->hidden('RequirementsForRecruitment/company_id');?>
			<fieldset>
				<legend>Parametry pozice</legend>
				<?php echo $htmlExt->input('RequirementsForRecruitment/name', array('label'=>'Požadovaná profese', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<?php echo $htmlExt->textarea('RequirementsForRecruitment/comment', array('label'=>'Poznámka k volnému místu', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/setting_career_item_id', $setting_career_list,null, array('label'=>'Profese dle číselníku'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/company_work_position_id', $company_position_list ,null, array('label'=>'Profese dle smlouvy'));?> <br class='clear' />
				</div>
				<?php echo $htmlExt->textarea('RequirementsForRecruitment/napln_prace', array('label'=>'Náplň práce', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->input('RequirementsForRecruitment/education', array('label'=>'Požadované vzdělání'));?> <br class='clear' />
					<?php echo $htmlExt->input('RequirementsForRecruitment/profession_duration', array('label'=>'Délka praxe'));?> <br class='clear' />
					<?php echo $htmlExt->input('RequirementsForRecruitment/age_restriction', array('label'=>'Věkové omezení'));?> <br class='clear' />
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('RequirementsForRecruitment/certification', array('label'=>'Certifikáty a zkoušky'));?> <br class='clear' />
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/sex_list', $vhodne_pro_list,null, array('label'=>'Je vhodné pro'));?> <br class='clear' />
				</div>
				<div class='sll'>
					<?php echo $htmlExt->selectTag('RequirementsForRecruitment/setting_shift_working_id', $smennost_list,null, array('label'=>'Směnnost'));?> <br class='clear' />
				</div><br />
					<?php echo $htmlExt->textarea('RequirementsForRecruitment/komentar_smennosti', array('label'=>'Komentář k směnnosti', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
				<div class='sll'>
					<?php echo $htmlExt->input('RequirementsForRecruitment/working_time', array('label'=>'Pracovní doba'));?> <br class='clear' />
				</div>
				<div class='sll'>
					<?php echo $htmlExt->input('RequirementsForRecruitment/standard_hours', array('label'=>'Normo hodiny'));?> <br class='clear' />	
				</div>
			</fieldset>