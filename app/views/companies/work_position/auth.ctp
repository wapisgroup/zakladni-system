<form action='/companies/work_money_auth/<?php echo $id.'/-1/';?>' method='post' id='company_work_position_edit_formular'>
	<?php //echo $htmlExt->hidden('CompanyWorkPosition/id');?>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/company_id');?>
	<fieldset>
		<legend>Autorizace pro pozici : <?php echo $pozice_name;?></legend>
		
		<?php echo $htmlExt->selectTag('CompanyMoneyValidity/choose', $choose_list, null,array('label'=>'Akce','label_class'=>'long','class'=>'long'),null,false);?><br />
		<div id="choose_1">
			<?php echo $htmlExt->selectTag('CompanyMoneyValidity/profese2', $company_work_position_list2, null,array('label'=>'Profese','label_class'=>'long','class'=>'long'),null,false);?><br />
		</div>
		<div id="choose_2">
			<?php echo $htmlExt->selectTag('CompanyMoneyValidity/profese', $company_work_position_list, null,array('label'=>'Profese','label_class'=>'long','class'=>'long'),null,false);?><br />
	
			<?php echo $htmlExt->input('CompanyMoneyValidity/platnost_od',array('tabindex'=>1,'label'=>'Platnost od','class'=>'long','label_class'=>'long'));?> <br class="clear">
		</div>
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyWorkPositionSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkPositionClose'/>
	</div>
</form>
<script>
	new MonthCal($('CompanyMoneyValidityPlatnostOd'));

	// skryj moznosti a save
	$('AddEditCompanyWorkPositionSaveAndClose').addClass('none');
	$('choose_1').addClass('none');
	$('choose_2').addClass('none');
	
	//vyber akce
	$('CompanyMoneyValidityChoose').addEvent('change',function(e){
		e.stop();
		if(this.value == 1){
			$('choose_1').removeClass('none');
			$('choose_2').addClass('none');
			$('AddEditCompanyWorkPositionSaveAndClose').removeClass('none');
		}
		else if(this.value == 2){	
			$('choose_2').removeClass('none');
			$('choose_1').addClass('none');
			$('AddEditCompanyWorkPositionSaveAndClose').removeClass('none');
		}
		else{
			$('choose_1').addClass('none');
			$('choose_2').addClass('none');
			$('AddEditCompanyWorkPositionSaveAndClose').addClass('none');
		}
	});
	
	
	$('AddEditCompanyWorkPositionClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_position_auth');});
	
	$('AddEditCompanyWorkPositionSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		if($('CompanyMoneyValidityPlatnostOd').value != "" || $('CompanyMoneyValidityChoose').value == 1)
		{
		  button_preloader($('AddEditCompanyWorkPositionSaveAndClose'));
			new Request.JSON({
				url:$('company_work_position_edit_formular').action,		
				//update: $('domwin_pozice').getElement('.CB_ImgContainer'),
				onComplete:function(JSON){
					if(JSON == true){
						domwin.loadContent('domwin_pozice');
						domwin.closeWindow('domwin_work_position_auth');
						//domtab.goTo(0);
					}
					else
						alert(""+JSON+"");
				
                    button_preloader_disable($('AddEditCompanyWorkPositionSaveAndClose'));
                }
			}).post($('company_work_position_edit_formular'));
		}
		else 
			alert('Vyplňte platnost od');

	});
	
</script>