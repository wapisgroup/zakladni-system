<form action='/companies/work_money_validity_edit/' method='post' id='company_work_position_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyMoneyValidity/id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyValidity/company_id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyValidity/company_work_position_id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyValidity/platnost_od_old');?>
	<fieldset>
		<legend>Zadejte údaje</legend>		
		<div class='sll'>
			<?php echo $htmlExt->input('CompanyMoneyValidity/fakturacka',array('tabindex'=>1,'label'=>'Fakturační částka'));?> <br class="clear">			
		</div>
		<div class='slr'>
			<?php echo $htmlExt->input('CompanyMoneyValidity/platnost_od',array('tabindex'=>1,'label'=>'Platnost od'));?> <br class="clear">
		</div>
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyWorkPositionSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkPositionClose'/>
	</div>
</form>
<script>
	new MonthCal($('CompanyMoneyValidityPlatnostOd'));
	
	

	$('AddEditCompanyWorkPositionClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_position_edit');});
	
	$('AddEditCompanyWorkPositionSaveAndClose').addEvent('click',function(e){
		if($('CompanyMoneyValidityPlatnostOd').value > $('CompanyMoneyValidityPlatnostOdOld').value)
		{
			new Event(e).stop();
			new Request.HTML({
				url:$('company_work_position_edit_formular').action,		
				update: $('domwin_pozice').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_work_position_edit');
				}
			}).post($('company_work_position_edit_formular'));
		}
		else
			alert('Platnost je příliš malá, stará platnost je '+$('CompanyMoneyValidityPlatnostOdOld').value);
	});
	
</script>