<form action='/companies/new_work_money_auth/<?php echo $id.'/';?>' method='post' id='company_work_position_edit_formular'>
	<?php //echo $htmlExt->hidden('CompanyWorkPosition/id');?>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/company_id');?>
	<fieldset>
		<legend>Autorizace pro pozici : <?php echo $pozice_name;?></legend>
			<?php echo $htmlExt->selectTag('CompanyMoneyValidity/profese', $company_work_position_list, null,array('label'=>'Profese','label_class'=>'long','class'=>'long'),null,false);?><br />
	
			<?php echo $htmlExt->input('CompanyMoneyValidity/platnost_od',array('tabindex'=>1,'label'=>'Platnost od','class'=>'long','label_class'=>'long'));?> <br class="clear">
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyWorkPositionSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkPositionClose'/>
	</div>
</form>
<script>
	new MonthCal($('CompanyMoneyValidityPlatnostOd'));	
	
	$('AddEditCompanyWorkPositionClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_position_auth');});
	
	$('AddEditCompanyWorkPositionSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		if($('CompanyMoneyValidityPlatnostOd').value != "")
		{
		  button_preloader($('AddEditCompanyWorkPositionSaveAndClose'));
			new Request.JSON({
				url:$('company_work_position_edit_formular').action,		
				onComplete:function(JSON){
					if(JSON == true){
						domwin.loadContent('domwin_pozice');
						domwin.closeWindow('domwin_work_position_auth');
						//domtab.goTo(0);
					}
					else
						alert(""+JSON+"");
				
                    button_preloader_disable($('AddEditCompanyWorkPositionSaveAndClose'));
                }
			}).post($('company_work_position_edit_formular'));
		}
		else 
			alert('Vyplňte platnost od');

	});
	
</script>