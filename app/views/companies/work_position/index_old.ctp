<div id='CompanyWorkPositionObal'>
	<div class="win_fce top">
		<a href='/companies/work_position_add/<?php echo $company_id;?>' class='button edit' id='company_work_position_add_new' title='Přidání pracovní pozice'>Přidat pracovní pozici</a>
	</div>
	<table class='table' id='table_kontakty'>
		<tr>
			<!--th>#ID</th-->
			<th>Název</th>
			<th>Fakturace</th>
			<th>Platnost</th>
			<th>Typ / Ubytovaní / Marže / Max</th>
			<th>Možnosti</th>
		</tr>
		<?php if (isset($work_position_list) && count($work_position_list) >0):?>
		<?php foreach($work_position_list as $work_position_item):?>
		<tr>
			<!--td><?php echo $work_position_item['CompanyWorkPosition']['id'];?></td-->
			<td><?php echo (($work_position_item['CompanyWorkPosition']['parent_id']  != -1)?'->  ':'').$work_position_item['CompanyWorkPosition']['name'];?></td>
			<td><?php echo $work_position_item['CompanyWorkPosition']['fakturacka'];?> CZK</td>
			<td><?php echo $fastest->czechDate($work_position_item['CompanyWorkPosition']['platnost_od']);?> ->  <?php echo $fastest->czechDate($work_position_item['CompanyWorkPosition']['platnost_do']);?> </td>
			<td>
				<?php foreach($work_position_item['CompanyMoneyItem'] as $subitem):?>
					<?php echo $subitem['name'] . ' / ' .(($subitem['cena_ubytovani_na_mesic'] > 0)?'A':'N') . ' / '.$subitem['provinzni_marze'] . '% / ' . $subitem['vypocitana_celkova_cista_maximalni_mzda_na_hodinu'] ;?><a title='Definice odměn pro <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money' href='/companies/work_money_edit/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>/<?php echo $subitem['id'];?>'/>money</a><br />
				<?php endforeach;?>			
			</td>
			<td>
				<a title='Editace položky <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta edit' href='/companies/work_position_edit/<?php echo $company_id;?>/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/>edit</a>
				<a title='Nová kalkulace pro  <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta money_add' href='/companies/work_money_edit/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/>add_money</a>
			<?php if($work_position_item['CompanyWorkPosition']['parent_id']  == -1):?>
				<a title='Nová subpoložka pro  <?php echo $work_position_item['CompanyWorkPosition']['name'];?>' 	class='ta add_subitem' href='/companies/work_money_add_subitem/<?php echo $work_position_item['CompanyWorkPosition']['id'];?>'/>add_subitem</a>
			<?php endif;?>
				
			</td>
		</tr>
		<?php endforeach;?>
		<?php else:?>
		<tr>
			<td colspan='5'>K této společnosti nebyla nadefinována pracovní pozice</td>
		</tr>
		<?php endif;?>
	</table>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyWorkPositionClose' class='button'/>
	</div>
</div>
<script>
	$('ListCompanyWorkPositionClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_pozice')});
	
	$('CompanyWorkPositionObal').getElements('.edit, .add_subitem').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_work_position_edit',
			sizes		: [580,320],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
	
	$('CompanyWorkPositionObal').getElements('.money, .money_add').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_work_money',
			sizes		: [780,480],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>