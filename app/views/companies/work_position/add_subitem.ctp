<form action='/companies/money_add_subitem/' method='post' id='company_work_position_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/parent_id',array('value'=>$this->data['CompanyWorkPosition']['id']));?>
	<?php echo $htmlExt->hidden('CompanyWorkPosition/company_id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyValidity/company_id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyValidity/platnost_od');?>
	<?php echo $htmlExt->hidden('CompanyMoneyValidity/platnost_do');?>
	<?php echo $htmlExt->hidden('CompanyMoneyValidity/old_money_validity_id',array('value'=>$this->data['CompanyMoneyValidity']['id']));?>
	<fieldset>
		<legend>Zadejte údaje</legend>
		<?php echo $htmlExt->input('CompanyWorkPosition/name',array('tabindex'=>1,'label'=>'Název pozice','class'=>'long','label_class'=>'long'));?> <br class="clear">
		<?php echo $htmlExt->input('CompanyMoneyValidity/fakturacka',array('tabindex'=>1,'label'=>'Fakturační částka','class'=>'long','label_class'=>'long'));?> <br class="clear">
		<br/>		
	</fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyWorkPositionSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkPositionClose'/>
	</div>
</form>
<script>
	$('AddEditCompanyWorkPositionClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_position_edit');});
	
	$('AddEditCompanyWorkPositionSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
			new Request.HTML({
				url:$('company_work_position_edit_formular').action,		
				update: $('domwin_pozice').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_work_position_edit');
				}
			}).post($('company_work_position_edit_formular'));

	});
</script>	
