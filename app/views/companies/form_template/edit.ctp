<form action='/companies/form_template_edit/' method='post' id='mail_templates_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyFormTemplate/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základní informace</legend>
				<?php echo $htmlExt->input('CompanyFormTemplate/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<label class="long">Kód:</label>
				<?php echo $wysiwyg->render('CompanyFormTemplate/text',array('tabindex'=>2));?> <br class="clear">
			</fieldset>			
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close'));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close'));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	$$('.wysiwyg').makeWswg();
	var domtab = new DomTabs({'className':'admin_dom'}); 

	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		$$('.wysiwyg').killWswg();
		if ($$('.disabled_save').length != 0)
			alert('Je aktivní HTML mód editoru. Přepněte jej prosím do normální módu.');
		else {
			new Request.JSON({
				url:$('mail_templates_edit_formular').action,		
				onComplete:function(){
					domwin.loadContent('domwin');
	   				domwin.closeWindow('domwin_form_template_edit');
				}
			}).post($('mail_templates_edit_formular'));
		}
	
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); $$('.wysiwyg').killWswg(); domwin.closeWindow('domwin_form_template_edit');});
</script>