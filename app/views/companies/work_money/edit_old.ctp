<form action='/companies/work_position_edit/' method='post' id='company_work_money_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyWorkMoney/id');?>
	<?php echo $htmlExt->hidden('CompanyWorkMoney/company_id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
			<li class="ousko"><a href="#krok2">Nastaveni</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Typ zaměstnání</legend>
				<ul class='typ_prace_ul'>
					<li><input type='checkbox' id='TypPracePracovniSmlouva'/>Pracovní smlouva<br class='clear;'/></li>
					<li><input type='checkbox' id='TypPraceDohodaOPP'/>Dohoda o PP<br class='clear;'/></li>
					<li><input type='checkbox' id='TypPraceFakturace'/>Fakturace<br class='clear;'/></li>
					<li><input type='checkbox' id='TypPraceHotovost'/>Hotovost<br class='clear;'/></li>
				</ul>
			</fieldset>
			<fieldset>
				<legend>Zadejte údaje</legend>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyWorkMoney/fa_hour',				array('tabindex'=>'1','class'=>'float calc','label'=>'Fa. sazba za hodinu'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/pocet_prescasu',		array('tabindex'=>'3','class'=>'float calc','label'=>'Počet hodinu přesčasů'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/ps_nad_minimalku',		array('tabindex'=>'5','class'=>'float calc','label'=>'Pracovní smluva - nad minimálku v hrubém'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/cena_ubytovani',		array('tabindex'=>'7','class'=>'float calc','label'=>'Cena ubytování'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/prispevek_cestovne',	array('tabindex'=>'9','class'=>'float calc','label'=>'Vlož príspevok na cestovné'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/zivnost_na_hodinu',	array('disabled'=>'disabled','tabindex'=>'11','class'=>'float calc','label'=>'Výplata faktúra živnostníka na hodinu'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyWorkMoney/pocet_hodin_zpf',		array('tabindex'=>'2','class'=>'float calc','label'=>'Počet hodin ZPF'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/marze',				array('tabindex'=>'4','class'=>'float calc','label'=>'Požadovaná marže'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/cena_pracovnich_odevu',array('tabindex'=>'6','class'=>'float calc','label'=>'Cena pracovních oděvů'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/prispevek_na_stravne',	array('tabindex'=>'8','class'=>'float calc','label'=>'Vlož príspevok na stravné'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/dni_dovolene',			array('tabindex'=>'10','class'=>'float calc','label'=>'Počet dní dovolené a svátků ročne zap. do nákladů'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/vyplata_dohoda_hodina',array('disabled'=>'disabled','tabindex'=>'12','class'=>'float calc','label'=>'Výplata dohoda na hodinu'));?> <br class="clear">
				</div>
				<br/>		
			</fieldset>
			<fieldset>
				<legend>informační údaje</legend>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyWorkMoney/celkovy_pocet_hodin',array('readonly'=>'readonly','tabindex'=>13,'label'=>'Celkový počet hodin měsíčně'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/maximalni_naklady_mesicne',array('readonly'=>'readonly','tabindex'=>15,'label'=>'Maximální náklady měsíčně'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/celkove_naklady_mesicne',array('readonly'=>'readonly','tabindex'=>17,'label'=>'Celkové náklady na zaměstanance měsíčně'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/hruba_maximalka_po_odecet',array('readonly'=>'readonly','tabindex'=>19,'label'=>'Hrubá max. na hodinu po odečtení ubytování a odvodů'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/celkova_hruba_mesicni_mzda',array('readonly'=>'readonly','tabindex'=>21,'label'=>'Celková hrubá mzda měsíčně'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/celkova_cista_maximalni_mzda_na_hodinu',array('readonly'=>'readonly','tabindex'=>23,'label'=>'Celková čistá mzda na hodinu'));?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyWorkMoney/celkova_fakturace_mesicne',array('readonly'=>'readonly','tabindex'=>14,'label'=>'Celková fakturace mesíčně'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/celkove_naklady_na_hodinu',array('readonly'=>'readonly','tabindex'=>16,'label'=>'Celkové náklady na zaměstance na hodinu'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/ps_minimalka_v_hrubem',array('readonly'=>'readonly','tabindex'=>18,'label'=>'Pracovní smlouva - minimálka v hrubém'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/cash_na_hodinu',array('readonly'=>'readonly','tabindex'=>20,'label'=>'Vypočtený doplatek abc na hodinu'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/cista_mzda_s_pracovni_smlouvy_na_hodinu',array('readonly'=>'readonly','tabindex'=>22,'label'=>'Čistá mzda z pracovní smlouvy na hodinu'));?> <br class="clear">
				</div>
				<br />				
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Zadejte údaje</legend>
				<div class='sll'>
					<?php echo $htmlExt->input('CompanyWorkMoney/procenta_odvodu_zamestnance',		array('class'=>'float calc','value'=>'12.5','tabindex'=>24,'label'=>'Procenta odvodu zaměstnance'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/procenta_dane_z_prijmu',			array('class'=>'float calc','value'=>'15','tabindex'=>26,'label'=>'Procenta daně z příjmu'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/dalsi_danove_odpocty',				array('class'=>'float calc','value'=>'0','tabindex'=>28,'label'=>'Další daňové odpočty'));?> <br class="clear">						
				</div>
				<div class='slr'>
					<?php echo $htmlExt->input('CompanyWorkMoney/procenta_odvodu_zamestnavatele',	array('class'=>'float calc','value'=>'35','tabindex'=>25,'label'=>'Procenta odvodu zaměstnavatele'));?> <br class="clear">
					<?php echo $htmlExt->input('CompanyWorkMoney/odpocitatelna_polozka',			array('class'=>'float calc','value'=>'2700','tabindex'=>27,'label'=>'Odpočítatelná položka'));?> <br class="clear">						
				</div>
			</div>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditCompanyWorkMoneySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkMoneyClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('company_work_money_edit_formular').getElements('.float, .integer').inputLimit();
	
	function myround(num){
		return Math.round(num * 10) / 10;
	}
	
	function log(text){
		
	}

	$('TypPracePracovniSmlouva').addEvent('click', function(){});
	$('TypPraceDohodaOPP').addEvent('click', function(){
		if(this.checked)
			$('CompanyWorkMoneyVyplataDohodaHodina').removeProperty('disabled');
		else {
			$('CompanyWorkMoneyVyplataDohodaHodina').setProperties({'disabled':'disabled'});
			$('CompanyWorkMoneyVyplataDohodaHodina').value = '0';
		}
		prepocet()
	});
	
	$('TypPraceFakturace').addEvent('click', function(){
		if(this.checked)
			$('CompanyWorkMoneyZivnostNaHodinu').removeProperty('disabled');
		else {
			$('CompanyWorkMoneyZivnostNaHodinu').setProperties({'disabled':'disabled'});
			$('CompanyWorkMoneyZivnostNaHodinu').value = '0';
		}
		prepocet()
	});
	$('TypPraceHotovost').addEvent('click', function(){});
	
	function prepocet(){
		
		
		var fakturacka_za_hodinu = $('CompanyWorkMoneyFaHour');
		var pocet_hodin_zakladniho_pracovniho_fondu = $('CompanyWorkMoneyPocetHodinZpf');
		var pocet_hodin_prescasu_mesicne = $('CompanyWorkMoneyPocetPrescasu');
		var celkovy_pocet_hodin_mesicne = $('CompanyWorkMoneyCelkovyPocetHodin');
		var celkova_fakturace_mesicne = $('CompanyWorkMoneyCelkovaFakturaceMesicne');
		var pozadovana_marze = $('CompanyWorkMoneyMarze');
		var maximalni_naklady_mesicne = $('CompanyWorkMoneyMaximalniNakladyMesicne');
		var cena_ubytovani_na_mesic = $('CompanyWorkMoneyCenaUbytovani');
		var cena_pracovnich_odevu_mesicne = $('CompanyWorkMoneyCenaPracovnichOdevu');
		var celkove_naklady_firmy_na_zamestance_mesicne = $('CompanyWorkMoneyCelkoveNakladyMesicne');
		var celkove_naklady_firmy_na_zamestance_nahodinu = $('CompanyWorkMoneyCelkoveNakladyNaHodinu');
		var hruba_maximalka_na_hodinu_po_odecteni = $('CompanyWorkMoneyHrubaMaximalkaPoOdecet')
		var cista_mzda_z_pracovni_smlouvy_na_hodinu = $('CompanyWorkMoneyCistaMzdaNaHodinu');
		var celkova_cista_maximalni_mzda_na_hodinu = $('CompanyWorkMoneyCelkovaCistaMaximalniMzdaNaHodinu');
		var cista_mzda_s_pracovni_smlouvy_na_hodinu = $('CompanyWorkMoneyCistaMzdaSPracovniSmlouvyNaHodinu');


		var pracovni_smlouva_minimalka_v_hrubem = $('CompanyWorkMoneyPsMinimalkaVHrubem');
		var pracovni_smlouva_nad_minimalka_v_hrubem = $('CompanyWorkMoneyPsNadMinimalku');
		var celkova_hruba_mzda_mesicne = $('CompanyWorkMoneyCelkovaHrubaMesicniMzda');
		var prispevek_na_stravne = $('CompanyWorkMoneyPrispevekNaStravne');
		var prispevek_na_cestovne = $('CompanyWorkMoneyPrispevekCestovne');
		var pocet_dni_dovolene = $('CompanyWorkMoneyDniDovolene');
		var faktura_zivnostnika_na_hodinu = $('CompanyWorkMoneyZivnostNaHodinu');
		var vyplata_dohoda_na_hodinu = $('CompanyWorkMoneyVyplataDohodaHodina');
		var vypocteny_doplatek_cash_na_hodinu = $('CompanyWorkMoneyCashNaHodinu');
	
		
		// NADEFINOVANE KONSTANTY
		var procenta_odvodu_zamestnance = $('CompanyWorkMoneyProcentaOdvoduZamestnance').value;	//12.5%
		var procenta_odvodu_zamestnavatele = $('CompanyWorkMoneyProcentaOdvoduZamestnavatele').value;	//35%
		var procenta_dane_z_prijmu = $('CompanyWorkMoneyProcentaDaneZPrijmu').value; //15%
		var odpocitatelna_polozka = $('CompanyWorkMoneyOdpocitatelnaPolozka').value;//2070;		
		var dalsi_danove_odpocty = $('CompanyWorkMoneyDalsiDanoveOdpocty').value;//0;
		
		/* radek 9 */
		celkovy_pocet_hodin_mesicne.value = pocet_hodin_zakladniho_pracovniho_fondu.value.toFloat() + pocet_hodin_prescasu_mesicne.value.toFloat();
		/* radek 10 */
		celkova_fakturace_mesicne.value = fakturacka_za_hodinu.value.toFloat() * celkovy_pocet_hodin_mesicne.value.toFloat();
		/* radek 13 */
		maximalni_naklady_mesicne.value = myround((fakturacka_za_hodinu.value.toFloat() / (1 + pozadovana_marze.value.toFloat() / 100)) * celkovy_pocet_hodin_mesicne.value.toFloat());
		/* radek 26 */
		pracovni_smlouva_minimalka_v_hrubem.value = 8000;
		/* radek 28 */
		celkova_hruba_mzda_mesicne.value = pracovni_smlouva_minimalka_v_hrubem.value.toFloat() + pracovni_smlouva_nad_minimalka_v_hrubem.value.toFloat();
	
		var odvody_zamestnavatel = celkova_hruba_mzda_mesicne.value.toFloat() / 100 * procenta_odvodu_zamestnavatele.toFloat();
		var zaklad_dane  = celkova_hruba_mzda_mesicne.value.toFloat() + odvody_zamestnavatel.toFloat();
		var naklady_mesicne_na_dovolenou = (zaklad_dane.toFloat() / 20) * (pocet_dni_dovolene.value.toFloat() / 12);
		var odvody_zamestnanec = celkova_hruba_mzda_mesicne.value.toFloat() / 100 * procenta_odvodu_zamestnance.toFloat();
		var celkove_mzdove_naklady_mesicne_bez_prac_smlouvy = celkova_hruba_mzda_mesicne.value.toFloat() + odvody_zamestnavatel.toFloat() + prispevek_na_stravne.value.toFloat() + prispevek_na_cestovne.value.toFloat() + naklady_mesicne_na_dovolenou.toFloat();
		
		/* radek 18 */
		celkove_naklady_firmy_na_zamestance_mesicne.value = myround(cena_ubytovani_na_mesic.value.toFloat() + cena_pracovnich_odevu_mesicne.value.toFloat() + celkove_mzdove_naklady_mesicne_bez_prac_smlouvy + (faktura_zivnostnika_na_hodinu.value.toFloat() * celkovy_pocet_hodin_mesicne.value.toFloat()) + (vyplata_dohoda_na_hodinu.value.toFloat() * celkovy_pocet_hodin_mesicne.value.toFloat() * 1.15));
		
		/* radek 19 */
		celkove_naklady_firmy_na_zamestance_nahodinu.value = myround(celkove_naklady_firmy_na_zamestance_mesicne.value.toFloat() / celkovy_pocet_hodin_mesicne.value.toFloat());
		
		/* radek 20 */
		hruba_maximalka_na_hodinu_po_odecteni.value = myround((maximalni_naklady_mesicne.value.toFloat() - cena_ubytovani_na_mesic.value.toFloat() - cena_pracovnich_odevu_mesicne.value.toFloat()) / celkovy_pocet_hodin_mesicne.value.toFloat());
		
		/* radek 51 */
		vypocteny_doplatek_cash_na_hodinu.value = myround(fakturacka_za_hodinu.value.toFloat() / (1 + pozadovana_marze.value.toFloat() / 100) - celkove_naklady_firmy_na_zamestance_nahodinu.value.toFloat());
	
		var dan_pred_slevami = zaklad_dane.toFloat() / 100 * procenta_dane_z_prijmu.toFloat();
		var dan_po_slevach = ((dan_pred_slevami.toFloat() - odpocitatelna_polozka.toFloat() - dalsi_danove_odpocty.toFloat())>0)?(dan_pred_slevami.toFloat() - odpocitatelna_polozka.toFloat() - dalsi_danove_odpocty.toFloat()):0;
		var cista_mzda_mesicne_bez_prispevku = celkova_hruba_mzda_mesicne.value.toFloat() - odvody_zamestnanec.toFloat() - dan_po_slevach.toFloat();
		var cista_mzda_na_hodinu_bez_prispevku = cista_mzda_mesicne_bez_prispevku.toFloat() / celkovy_pocet_hodin_mesicne.value.toFloat();
		var cista_mzda_zamestnance_mesicne = cista_mzda_mesicne_bez_prispevku.toFloat() + prispevek_na_cestovne.value.toFloat() + prispevek_na_stravne.value.toFloat();
	
		/* radek 24 */
		cista_mzda_s_pracovni_smlouvy_na_hodinu.value = myround(cista_mzda_zamestnance_mesicne.toFloat() / celkovy_pocet_hodin_mesicne.value.toFloat());
		/* radek 22 */
		celkova_cista_maximalni_mzda_na_hodinu.value = myround(vypocteny_doplatek_cash_na_hodinu.value.toFloat() + cista_mzda_s_pracovni_smlouvy_na_hodinu.value.toFloat() + faktura_zivnostnika_na_hodinu.value.toFloat() + vyplata_dohoda_na_hodinu.value.toFloat());
			
	}

	$('company_work_money_edit_formular').getElements('.calc').addEvent('change', prepocet);

	
	
	$('CompanyWorkMoneyFaHour').value = 200;
	$('CompanyWorkMoneyPocetHodinZpf').value = 160;
	$('CompanyWorkMoneyPocetPrescasu').value = 20;
	$('CompanyWorkMoneyMarze').value = 20;
	$('CompanyWorkMoneyPsNadMinimalku').value = 6500;
	$('CompanyWorkMoneyCenaUbytovani').value = 3300;
	$('CompanyWorkMoneyCenaPracovnichOdevu').value = 0;
	$('CompanyWorkMoneyPrispevekNaStravne').value = 3300;
	$('CompanyWorkMoneyPrispevekCestovne').value = 1000;
	$('CompanyWorkMoneyDniDovolene').value  = 30;
	$('CompanyWorkMoneyZivnostNaHodinu').value = 0;
	$('CompanyWorkMoneyVyplataDohodaHodina').value = 0;
	
	prepocet();

	
	$('AddEditCompanyWorkMoneyClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_money_edit');});
	
	$('AddEditCompanyWorkMoneySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_work_money_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('company_work_money_edit_formular').action,		
				update: $('domwin_pozice').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_work_money_edit');
				}
			}).post($('company_work_money_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('company_work_money_edit_formular',{
		'CompanyWorkMoneyName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('company_work_money_edit_formular',<?php echo (isset($this->data['CompanyWorkMoney']['id']))?'true':'false';?>);
	
</script>