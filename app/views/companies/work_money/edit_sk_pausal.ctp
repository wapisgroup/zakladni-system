<?php
echo $only_show == true ? '<div id="myDisable">' : '';
?>
<form action='/companies/work_money_edit/' method='post' id='company_work_money_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/pausal');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/h1000');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/company_id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/company_work_position_id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/company_money_validity_id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/mhhd');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/mhhd_hodina_default');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/vlozeny_prispevek_na_stravne_default');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/vlozeny_prispevek_na_cestovne_default');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/cista_mzda_1001');?>
	<?php if (!isset($this->data['CompanyMoneyItem']['id']) || isset($save_test)):?>
		<fieldset>
			<legend>Načtení kalkulací</legend>
			<div class='sll3'><?php echo $htmlExt->selectTag('Company/list', $company_list, null,array('label'=>'Firma','label_class'=>'long_3c','class'=>'long_3c'));?><br class='clear'/></div>
			<div class='sll3'><?php echo $htmlExt->selectTag('Position/list', array(), null,array('label'=>'Prac. pozice','label_class'=>'long_3c','class'=>'long_3c'));?><br class='clear'/></div>
			<div class='sll3'><?php echo $htmlExt->selectTag('Money/list', array(), null,array('label'=>'Kalkulace','label_class'=>'long_3c','class'=>'long_3c'));?><br class='clear'/></div>
		</fieldset>
	<?php endif;?>
	<?php if (isset($z_reportu)):?>
		<fieldset>
			<legend>Schválení kalkulace</legend>
			<label>Schváleno:</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/schvaleno');?><br/>
		</fieldset>
	<?php endif;?>
	<fieldset>
		<legend>Výstupní informace</legend>
		<?php echo $htmlExt->input('CompanyMoneyItem/provinzni_marze',											array('class'=>'zero','disabled'=>'disabled','label'=>'Provozní marže'));?><br class='clear' />
		<?php echo $htmlExt->input('CompanyMoneyItem/koeficient_navyseni',	array('class'=>'zero','label'=>'Koeficient navýšení'));?><br class='clear' />
		<?php echo $htmlExt->input('CompanyMoneyItem/celkova_fakturace_mesicne',								array('class'=>'zero','disabled'=>'disabled','label'=>'Celková fakturace měsíčně'));?><br class='clear' />
			
        <?php echo $htmlExt->input('CompanyMoneyItem/fakturacni_sazba_na_hodinu',								array('class'=>'zero none','label_class'=>'none','disabled'=>(isset($fa_disabled))?'disabled':false,'label'=>'Fakturační sazba na hodinu'));?>
		<?php echo $htmlExt->input('CompanyMoneyItem/vypocitana_celkova_cista_maximalni_mzda_na_hodinu',		array('class'=>'zero none','label_class'=>'none','disabled'=>'disabled','label'=>'Celková maximální mzda'));?>			
	</fieldset>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="krok1">Náklady mimo mzdu</a></li>
			<li class="ousko none"><a href="krok2">Počet odpracovaných hodin</a></li>
			<li class="ousko none"><a href="krok3">Formy mzdy</a></li>
			<li class="ousko"><a href="krok3">Forma mzdy</a></li>
			<li class="ousko"><a href="krok4">Odměny</a></li>
			<li class="ousko"><a href="krok5">Nastaveni</a></li>
			<li class="ousko"><a href="krok6">Informativní pole</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<!-- Naklady mimo mzdu -->
		<div class="domtabs field"> 
			<fieldset class="dva_sloupce_za_label">
				<legend>Vložení nákladů na zaměstnance mimo mzdy</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne',	array('class'=>'zero','disabled'=>'disabled','label'=>'Celkové náklady firmy na zaměstnance bez mzdy měsíčně'));?><br class='clear' />
				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/cena_ubytovani_na_mesic',array('Ne','Ano'),null,array('label'=>'Cena ubytování na měsíc'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/cena_ubytovani_na_mesic',array('class'=>'short slide_field zero'));?><br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/cena_pracovnich_odevu_mesicne',array('Ne','Ano'),null,array('label'=>'Cena pracovních oděvů měsíčně'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/cena_pracovnich_odevu_mesicne',array('class'=>'short slide_field zero'));?><br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/dalsi_naklady_mesicne',array('Ne','Ano'),null,array('label'=>'Další náklady měsíčně'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/dalsi_naklady_mesicne',array('class'=>'short slide_field zero'));?><br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/stravenka',array('Ne','Ano'),null,array('label'=>'Stravenky náklady denně'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/stravenka',array('class'=>'short slide_field zero'));?><br /></div>
				</div>
				
				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/doprava',array('Ne','Ano'),null,array('label'=>'Doprava měsíčně'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/doprava',array('class'=>'short slide_field zero'));?><br /></div>
				</div>
			</fieldset>
		</div>
		<!-- Pocet odpracovaných hodin -->	
		<div class="domtabs field none"> 
			<fieldset>
				<legend>Vložení možného počtu odpracovaných hodin</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_dni_v_mesici',										array('label'=>'Počet pracovních dnů v měsíci'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/normo_hodina',												array('label'=>'Denní pracovní fond'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_hodin_prescasu_mesicne',								array('label'=>'Počet hodin přesčasů měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_hodin_zakladniho_pracovniho_fondu',					array('class'=>'zero','label'=>'Pracovní fond měsíčně','disabled'=>'disabled'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkovy_pocet_hodin_mesicne',								array('class'=>'zero','disabled'=>'disabled','label'=>'Počet hodin měsíčně celkem'));?><br class='clear' />
			</fieldset>
		</div>
		<!-- Formy mzdy -->	
		<div class="domtabs field none"> 
			<fieldset class="dva_sloupce_za_label">
				<legend>Vložení způsobu vyplácení mzdy</legend>
				<label>Bude mzda vyplácaná formou pracovní smlouvy?</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/checkbox_pracovni_smlouva',								array('label'=>'Bude mzda vyplácaná formou pracovní  smlouvy?'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pracovni_smlouva_celkova_hruba_mzda_mesicne',	array('class'=>'short','disabled'=>'disabled','label'=>'Hrubá mzda měsíčně'))."<span style='line-height: 25px; font-size:9pt;'>/ 0</span><br />";?>
				<?php echo $htmlExt->input('CompanyMoneyItem/mhhd_hodina_now',array('class'=>'short','disabled'=>'disabled','label'=>'Hrubá hodinová mzda'))."<span style='line-height: 25px; font-size:9pt;'>/ 0</span><br />";?>
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_z_pracovni_smlouvy_na_hodinu',					array('class'=>'zero','disabled'=>($this->data['CompanyMoneyItem']['checkbox_pracovni_smlouva'] == 0)?'disabled':null,'label'=>'Čistá mzda z pracovní smlouvy na hodinu'));?><br class='clear' />
				
				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/stravne',array('Ne','Ano'),null,array('label'=>'Příspěvek na stravné',($this->data['CompanyMoneyItem']['checkbox_pracovni_smlouva'] == 0)?'disabled="disabled"':null),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/vlozeny_prispevek_na_stravne',array('class'=>'short slide_field zero','disabled'=>'disabled'));?><br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/cestovne',array('Ne','Ano'),null,array('label'=>'Příspěvek na cestovné',($this->data['CompanyMoneyItem']['checkbox_pracovni_smlouva'] == 0)?'disabled="disabled"':null),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/vlozeny_prispevek_na_cestovne',array('class'=>'short slide_field zero'));?><br /></div>
				</div>	

				<label>Bude mzda vyplácaná formou dohody o vykonaní práce?</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/checkbox_dohoda',										array('label'=>'Bude mzda vyplácaná formou dohody o vykonaní práce?'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_mzdove_naklady_firmy_mesicne_bez_dohody',			array('class'=>'zero','disabled'=>'disabled','label'=>'Celkové mzdové náklady firmy měsíčně na dohodu'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_dohoda_na_hodinu',								array('class'=>'zero','disabled'=>($this->data['CompanyMoneyItem']['checkbox_dohoda'] == 0)?'disabled':null,'label'=>'Čistá mzda - dohoda na hodinu'));?><br class='clear' />
				

				<label>Bude mzda vyplácaná formou faktury živnostníkovi?</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/checkbox_faktura',										array('label'=>'Bude mzda vyplácaná formou faktury živnostníkovi?'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika',		array('class'=>'zero','disabled'=>'disabled','label'=>'Celkové mzdové náklady firmy měsíčně na živnostníka'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_faktura_zivnostnika_na_hodinu',					array('class'=>'zero','disabled'=>($this->data['CompanyMoneyItem']['checkbox_faktura'] == 0)?'disabled':null,'label'=>'Čistá mzda - faktura živnostníka na hodinu'));?><br class='clear' />

				<label>Bude mzda vyplácaná formou abc?</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/checkbox_cash',											array('label'=>'Bude mzda vyplácaná formou abc?'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_mzdove_naklady_firmy_mesicne_bez_cash',			array('class'=>'zero','disabled'=>'disabled','label'=>'Celkové mzdové náklady firmy měsíčně na abc'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_cash_na_hodinu',								array('class'=>'zero','disabled'=>($this->data['CompanyMoneyItem']['checkbox_cash'] == 0)?'disabled':null,'label'=>'Čistá mzda - abc na hodinu'));?><br class='clear' />
		            
            </fieldset>
		</div>
        	<!-- Forma mzdy - pausal -->	
		<div class="domtabs field"> 
			<fieldset class="dva_sloupce_za_label">
				
               	<?php echo $htmlExt->input('CompanyMoneyItem/ps_pausal_hmm',array('class'=>'zero','label'=>'Hrubá mzda měsíčně'));?><br class='clear' />
			   	<?php echo $htmlExt->selectTag('CompanyMoneyItem/ps_pausal_naklady_dov',array('Ano','Ne'),null,array('label'=>'Započítat náklady na dovolenou a svátky'),null,false);?>
				
                <div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/stravne',array('Ne','Ano'),null,array('label'=>'Příspěvek na stravné'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/ps_pausal_stravne',array('class'=>'short slide_field zero'));?><br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/cestovne',array('Ne','Ano'),null,array('label'=>'Příspěvek na cestovné'),null,false);?>
					<div><?php echo $htmlExt->input('CompanyMoneyItem/ps_pausal_cestovne',array('class'=>'short slide_field zero'));?><br /></div>
				</div>	        
            </fieldset>
		</div>
		<!-- Odměny -->
		<div class="domtabs field"> 
			<fieldset class="tri_sloupce_za_label">
				<legend>Odměny</legend>
				<div class="pudlik_left_310"><strong>Fakturační odměna | Koeficient | Max. k výplatě | Náklady</strong></div>
				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/odmena_1',array('Ne','Ano'),null,array('label'=>'Odměna 1'),null,false);?>
					<div>
						<?php echo $htmlExt->input('CompanyMoneyItem/odmena_fakturace_1',array('class'=>'short zero float'));?>
						<?php echo $htmlExt->input('CompanyMoneyItem/odmena_koeficient_1',array('class'=>'short zero float'));?>
						<?php echo $htmlExt->input('CompanyMoneyItem/odmena_max_1',array('class'=>'short zero float','disabled'=>'disabled'));?>
						<?php echo $htmlExt->input('CompanyMoneyItem/odmena_naklady_1',array('class'=>'short zero float'));?>
					<br /></div>
				</div>

				<div class="slide_block clear">
					<?php echo $htmlExt->selectTag('Select/odmena_2',array('Ne','Ano'),null,array('label'=>'Odměna 2'),null,false);?>
					<div>
						<?php echo $htmlExt->input('CompanyMoneyItem/odmena_fakturace_2',array('class'=>'short zero float'));?>
						<?php echo $htmlExt->input('CompanyMoneyItem/odmena_koeficient_2',array('class'=>'short zero float'));?>
						<?php echo $htmlExt->input('CompanyMoneyItem/odmena_max_2',array('class'=>'short zero float','disabled'=>'disabled'));?>
						<?php echo $htmlExt->input('CompanyMoneyItem/odmena_naklady_2',array('class'=>'short zero float'));?>
					<br /></div>
				</div>
			</fieldset>
		</div>
		<!-- Nastaveni -->	
		<div class="domtabs field"> 
			<fieldset>
				<legend>Zadejte údaje</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/dalsi_danove_odpocty',										array('class'=>'zero','label'=>'Vlož další daňové odpočty'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_dni_dovolene_svatku_zapocitanych_do_nakladu',		array('label'=>'Počet dní dovoelné a svátků ročne započítaných do nákladů'));?><br class='clear' />
				
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_deti',												array('label'=>'Počet dětí'));?><br class='clear' />
				
				<?php echo $htmlExt->input('CompanyMoneyItem/procentuelni_odvod_zamestnance',							array('class'=>'none','label_class'=>'none','label'=>'Procentuální odvod zaměstnance'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/procentuelni_odvod_zamestnavatele',						array('class'=>'none','label_class'=>'none','label'=>'Procentuální odvod zaměstnavatele'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/procenta_dane_z_prijmu',									array('class'=>'none','label_class'=>'none','label'=>'Procenta daně z příjmu'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/procenta_dane_z_prijmu_dohoda',							array('class'=>'none','label_class'=>'none','label'=>'Procenta daně z příjmu - dohoda'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/odvody_dohoda',											array('class'=>'none','label_class'=>'none','label'=>'Odvody dohoda'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/odpocitatelna_polozka',									array('class'=>'none','label_class'=>'none','label'=>'Odpočitatelná položka'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/vlozena_procenta_nakladu_na_zivnostnika',					array('class'=>'none','label_class'=>'none','label'=>'Vlož procenta nákladů na živnostníka'));?><br class='clear' />
				<?php // echo $htmlExt->input('CompanyMoneyItem/vlozena_procenta_nakladu_na_dohodu',						array('label'=>'Vlož procenta nákladů na dohodu o vykonaní práce'));?>
				<?php echo $htmlExt->input('CompanyMoneyItem/vlozena_procenta_nakladu_na_cash',							array('class'=>'none','label_class'=>'none','label'=>'Vlož procenta nákladů  na abc'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/danovy_bonus_za_kazde_dite',								array('class'=>'none','label_class'=>'none','label'=>'Daň. bonus za každé dítě'));?><br class='clear' />
			</fieldset>
		</div>
		<!-- Informativní pole -->	
		<div class="domtabs field"> 
			<fieldset>
				<legend>Hidden fields</legend>
					<?php echo $htmlExt->input('CompanyMoneyItem/celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy',	array('class'=>'zero','disabled'=>'disabled','label'=>'Celkové náklady'));?><br class='clear' />	
				<!-- Informativní pole 
				<?php echo $htmlExt->input('CompanyMoneyItem/pracovni_smlouva_celkova_hruba_mzda_mesicne',				array('class'=>'zero','disabled'=>'disabled','label'=>'Pracovní smlouva - celková hrubá mzda měsíčně'));?><br class='clear' />
				-->	
				<?php echo $htmlExt->input('CompanyMoneyItem/naklady_mesicne_na_dovolene_svatky',						array('class'=>'zero','disabled'=>'disabled','label'=>'Náklady měsíčně na dovolenky a státní svátky'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_naklady_firmy_na_zamestnance_mesicne',				array('class'=>'zero','disabled'=>'disabled','label'=>'Celkové náklady firmy na zaměstnance měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_mesicne_bez_prispevku',							array('class'=>'zero','disabled'=>'disabled','label'=>'Čistá mzda měsíčně bez příspěvků'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_zamestnance_mesicne',							array('class'=>'zero','disabled'=>'disabled','label'=>'Čistá mzda zaměstnance měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/danovy_bonus_celkem',										array('class'=>'zero','disabled'=>'disabled','label'=>'Danovy bonus celkem'));?><br class='clear' />
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='submit' value='Uložit' id='AddEditCompanyWorkMoneySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkMoneyClose'/>
	</div>
</form>
<?php
echo $only_show == true ? '</div>' : '';
?>
<script>
domtab = new DomTabs({'className':'admin_dom'}); 
var only_show = <?php echo (isset($only_show) && $only_show!="") ? $only_show : 'false';?>;

// pouze zobrazeni a zamezit editaci
if(only_show){
	$('company_work_money_edit_formular').getElements('input,select,textarea').setProperty('disabled','disabled');
	$('AddEditCompanyWorkMoneySaveAndClose').addClass('hidden');
	$('AddEditCompanyWorkMoneyClose').removeProperty('disabled');
}

	
	/**
	*  Nacteni seznamu pozic k dane firme
	**/
	if ($('CompanyList'))
		$('CompanyList').ajaxLoad('/companies/load_ajax_position/',['PositionList','MoneyList']);
	if($('PositionList'))
		$('PositionList').ajaxLoad('/companies/load_ajax_money/',['MoneyList'],'/true');
	if($('MoneyList'))
		$('MoneyList').addEvent('change', function(){
			new Request.JSON({
				url: '/companies/load_ajax_money_detail/' + this.value,
				onComplete: function(json){
					$each(json, function(value,item){
						input = $('CompanyMoneyItem' + item.camelCase2().capitalize());
						//console.log(('CompanyMoneyItem' + item.camelCase2().capitalize()));
						if (input){
							switch (input.getProperty('type')){
								case 'text':
									input.value = value;

									if(input.value != 0){			
										if(input.getParent('div.slide_block')){
											input.getParent('div.slide_block').getElement('select').value=1;
											input.getParent('div.slide_block').getElement('select').getNext('div').setProperty('style','margin: 0px; overflow: hidden; position: static; height: auto;');
											input.getParent('div.slide_block').getElement('select').getNext('div').getChildren('div').setProperty('style','margin:0px;');
										}
									}
									else {
										if(input.getParent('div.slide_block')){
											input.getParent('div.slide_block').getElement('select').value=0;
											input.getParent('div.slide_block').getElement('select').getNext('div').setProperty('style','margin: 0px; overflow: hidden; position: static; height: 0px;');
											input.getParent('div.slide_block').getElement('select').getNext('div').getChildren('div').setProperty('style','margin: -25px 0px 0px;');
										}
									}
									break;
								case 'checkbox':
									if (value == 1){
										input.setProperty('checked','checked');
									} else 
										input.removeProperty('checked');
									//input.fireEvent('click');
									break;
							}
						}
					});

					load_from_inputs();
					vypocet();
					insert_to_input();
				}
			}).send()
		});

	//hide/unhide inputu pro nastaveni po zmene selectu
	$('company_work_money_edit_formular').getElements('.slide_block').each(function(item){	
		var slider_js = new Fx.Slide(item.getElement('div'));
		
		var select = item.getElement('select');
		var input = item.getElement('input');
		
		if(input.value == 0)
			slider_js.hide();
		else select.value=1;

		select.addEvent('change', function(e){
			new Event(e).stop();
			if (slider_js.open==true){
				//if(input.id != 'CompanyMoneyItemVlozenyPrispevekNaStravne')
				//input.value = 0;
				item.getElements('input').each(function(inputy){
					inputy.value = 0;
							
				});


				load_from_inputs();
				vypocet();
				insert_to_input();
			}
			else {
				if(input.id == 'CompanyMoneyItemVlozenyPrispevekNaStravne'){
					input.value = myround($('CompanyMoneyItemVlozenyPrispevekNaStravneDefault').value * $('CompanyMoneyItemPocetDniVMesici').value);
				
					load_from_inputs();
					vypocet();
					insert_to_input();
				}
			}
			slider_js.toggle();
			return false;
		});
	
	});
	

	$('company_work_money_edit_formular').getElements('.float, .integer').inputLimit();
	$('company_work_money_edit_formular').getElements('.zero').each(function(item){
		if (item.value == '') item.value = '0.00';
	});

	function kontrola_selectu(id_select){
		var inputsOk = true;
		if($(id_select).value == 1){
			$(id_select).getNext('div').getElements('input').each(function(item){
				if(item.value == 0)
						inputsOk = false;
			});

			if(inputsOk)
				return true;
			else 
				return false;			
		}
		else
			return true;
	}

    //pro funkci ktera se vola az po nacteni loadcontent
	function load_content_param(){
        domtab.goTo(1);
    }
    
	$('AddEditCompanyWorkMoneySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		
		if(kontrola_selectu('SelectOdmena1') && kontrola_selectu('SelectOdmena2')){
			$('company_work_money_edit_formular').getElements('input').removeProperty('disabled');
			<?php if (isset($z_reportu)){?>
			var request = new Request.JSON({url: $('company_work_money_edit_formular').action, onComplete: function(){click_refresh();domwin.closeWindow('domwin');}});
			<?php }
			else if(isset($save_test)){
			?>
	       	var request = new Request.JSON({
					url: $('company_work_money_edit_formular').action, 
					//update: $('domwin_pozice').getElement('.CB_ImgContainer'), 
					onComplete: function(json){
					   if(json.result === true){  
					     domwin.loadContent('domwin_pozice',{'onComplete': 'load_content_param'}); 
						 domwin.closeWindow('domwin_work_money');
                       }
                       else
                        alert(json.message); 
					}
			});
			<?php }
			else{
			?>
			var request = new Request.HTML({
					url: $('company_work_money_edit_formular').action, 
					update: $('domwin_pozice').getElement('.CB_ImgContainer'), 
					onComplete: function(){
						domwin.closeWindow('domwin_work_money');
					}
			});
			<?php }?>
			
			request.post($('company_work_money_edit_formular'));
		}
		else 
			alert('Zvolili jste odměnu a nenastavili všechny pole! Pole nesmí být nulové.');

	});
	
	$('AddEditCompanyWorkMoneyClose').addEvent('click', function(e){
		new Event(e).stop();
		<?php if (isset($z_reportu)):?>
		domwin.closeWindow('domwin');
		<?php else:?>
		domwin.closeWindow('domwin_work_money');
		<?php endif;?>
	});
	

	function myround(num){
		return Math.round(num * 10) / 10;
	}
	
    //zmena jestli se pocitaji naklady
    $('CompanyMoneyItemPsPausalNakladyDov').addEvent('change',function(e){
        load_from_inputs();
		vypocet();
		insert_to_input();
    });


	function load_from_inputs(){
		celkovy_pocet_hodin_mesicne = 								$('CompanyMoneyItemCelkovyPocetHodinMesicne').value.toFloat();
		pocet_hodin_zakladniho_pracovniho_fondu = 					$('CompanyMoneyItemPocetHodinZakladnihoPracovnihoFondu').value.toFloat();
		pocet_hodin_prescasu_mesicne = 								$('CompanyMoneyItemPocetHodinPrescasuMesicne').value.toFloat();

		celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne = 	$('CompanyMoneyItemCelkoveNakladyFirmyNaZamestnanceBezMzdyMesicne').value.toFloat();
		cena_ubytovani_na_mesic = 									$('CompanyMoneyItemCenaUbytovaniNaMesic').value.toFloat();
		cena_pracovnich_odevu_mesicne = 							$('CompanyMoneyItemCenaPracovnichOdevuMesicne').value.toFloat();
		dalsi_naklady_mesicne = 									$('CompanyMoneyItemDalsiNakladyMesicne').value.toFloat();

		provinzni_marze = 											$('CompanyMoneyItemProvinzniMarze').value.toFloat();
		celkove_naklady_firmy_na_zamestnance_mesicne = 				$('CompanyMoneyItemCelkoveNakladyFirmyNaZamestnanceMesicne').value.toFloat();
		celkova_fakturace_mesicne = 								$('CompanyMoneyItemCelkovaFakturaceMesicne').value.toFloat();
		fakturacni_sazba_na_hodinu = 								$('CompanyMoneyItemFakturacniSazbaNaHodinu').value.toFloat();

		vypocitana_celkova_cista_maximalni_mzda_na_hodinu = 		$('CompanyMoneyItemVypocitanaCelkovaCistaMaximalniMzdaNaHodinu').value.toFloat();
		celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy = 	$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezPracSmlouvy').value.toFloat();
		checkbox_pracovni_smlouva = 								$('CompanyMoneyItemCheckboxPracovniSmlouva').value.toFloat();
		pracovni_smlouva_celkova_hruba_mzda_mesicne = 				$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').value.toFloat();
		vlozeny_prispevek_na_stravne = 								$('CompanyMoneyItemVlozenyPrispevekNaStravne').value.toFloat();
		vlozeny_prispevek_na_cestovne = 							$('CompanyMoneyItemVlozenyPrispevekNaCestovne').value.toFloat();
		pocet_dni_dovolene_svatku_zapocitanych_do_nakladu = 		$('CompanyMoneyItemPocetDniDovoleneSvatkuZapocitanychDoNakladu').value.toFloat();
		naklady_mesicne_na_dovolene_svatky = 						$('CompanyMoneyItemNakladyMesicneNaDovoleneSvatky').value.toFloat();
		cista_mzda_z_pracovni_smlouvy_na_hodinu = 					$('CompanyMoneyItemCistaMzdaZPracovniSmlouvyNaHodinu').value.toFloat();
		procentuelni_odvod_zamestnance = 							$('CompanyMoneyItemProcentuelniOdvodZamestnance').value.toFloat();
		procentuelni_odvod_zamestnavatele = 						$('CompanyMoneyItemProcentuelniOdvodZamestnavatele').value.toFloat();
		
		procenta_dane_z_prijmu = 									$('CompanyMoneyItemProcentaDaneZPrijmu').value.toFloat();
		procenta_dane_z_prijmu_dohoda = 							$('CompanyMoneyItemProcentaDaneZPrijmuDohoda').value.toFloat();
		odvody_dohoda =												$('CompanyMoneyItemOdvodyDohoda').value.toFloat();
		
		odpocitatelna_polozka = 									$('CompanyMoneyItemOdpocitatelnaPolozka').value.toFloat();
		dalsi_danove_odpocty = 										$('CompanyMoneyItemDalsiDanoveOdpocty').value.toFloat();
	
		cista_mzda_mesicne_bez_prispevku = 							$('CompanyMoneyItemCistaMzdaMesicneBezPrispevku').value.toFloat();
		cista_mzda_zamestnance_mesicne = 							$('CompanyMoneyItemCistaMzdaZamestnanceMesicne').value.toFloat();

		celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika = 		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezZivnostnika').value.toFloat();
		checkbox_faktura = 											$('CompanyMoneyItemCheckboxFaktura').value.toFloat();
		cista_mzda_faktura_zivnostnika_na_hodinu = 					$('CompanyMoneyItemCistaMzdaFakturaZivnostnikaNaHodinu').value.toFloat();
		vlozena_procenta_nakladu_na_zivnostnika = 					$('CompanyMoneyItemVlozenaProcentaNakladuNaZivnostnika').value.toFloat();

		celkove_mzdove_naklady_firmy_mesicne_bez_dohody = 			$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezDohody').value.toFloat();
		checkbox_dohoda = 											$('CompanyMoneyItemCheckboxDohoda').value.toFloat();
		cista_mzda_dohoda_na_hodinu = 								$('CompanyMoneyItemCistaMzdaDohodaNaHodinu').value.toFloat();
		//vlozena_procenta_nakladu_na_dohodu = 						$('CompanyMoneyItemVlozenaProcentaNakladuNaDohodu').value.toFloat();

		celkove_mzdove_naklady_firmy_mesicne_bez_cash = 			$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezCash').value.toFloat();
		checkbox_cash = 											$('CompanyMoneyItemCheckboxCash').value.toFloat();
		cista_mzda_cash_na_hodinu = 								$('CompanyMoneyItemCistaMzdaCashNaHodinu').value.toFloat();
		vlozena_procenta_nakladu_na_cash = 							$('CompanyMoneyItemVlozenaProcentaNakladuNaCash').value.toFloat();
		pocet_dni_v_mesici = 										$('CompanyMoneyItemPocetDniVMesici').value.toFloat();
		danovy_bonus_celkem 		= 								$('CompanyMoneyItemDanovyBonusCelkem').value.toFloat();
		pocet_deti 					= 								$('CompanyMoneyItemPocetDeti').value.toInt();
		danovy_bonus_za_kazde_dite 	= 								$('CompanyMoneyItemDanovyBonusZaKazdeDite').value.toFloat();
		stravenka = 												$('CompanyMoneyItemStravenka').value.toFloat();
		normo_hodina = 												$('CompanyMoneyItemNormoHodina').value.toFloat();
		doprava = 													$('CompanyMoneyItemDoprava').value.toFloat();
		min_hruba_mzda = 											$('CompanyMoneyItemMhhd').value.toFloat();
		min_hruba_hod_mzda = 										$('CompanyMoneyItemMhhdHodinaDefault').value.toFloat();
		hruba_hod_mzda = 											$('CompanyMoneyItemMhhdHodinaNow').value.toFloat();

		//odmeny
		odmena_fakturace_1 = 										$('CompanyMoneyItemOdmenaFakturace1').value.toFloat();
		odmena_koeficient_1 = 										$('CompanyMoneyItemOdmenaKoeficient1').value.toFloat();
		odmena_max_1 = 												$('CompanyMoneyItemOdmenaMax1').value.toFloat();
		odmena_naklady_1 = 											$('CompanyMoneyItemOdmenaNaklady1').value.toFloat();

		odmena_fakturace_2 = 										$('CompanyMoneyItemOdmenaFakturace2').value.toFloat();
		odmena_koeficient_2 = 										$('CompanyMoneyItemOdmenaKoeficient2').value.toFloat();
		odmena_max_2 = 												$('CompanyMoneyItemOdmenaMax2').value.toFloat();
		odmena_naklady_2 = 											$('CompanyMoneyItemOdmenaNaklady2').value.toFloat();
	
        //nove pausal
        koeficient_navyseni = 										$('CompanyMoneyItemKoeficientNavyseni').value.toFloat();
		ps_pausal_hmm = 									     	$('CompanyMoneyItemPsPausalHmm').value.toFloat();
		ps_pausal_cestovne =										$('CompanyMoneyItemPsPausalCestovne').value.toFloat();
		ps_pausal_stravne = 										$('CompanyMoneyItemPsPausalStravne').value.toFloat();
	
    }
	
	function insert_to_input(){
		$('CompanyMoneyItemCelkovyPocetHodinMesicne').value 						= celkovy_pocet_hodin_mesicne;
		$('CompanyMoneyItemPocetHodinZakladnihoPracovnihoFondu').value 				= pocet_hodin_zakladniho_pracovniho_fondu;
		$('CompanyMoneyItemPocetHodinPrescasuMesicne').value 						= pocet_hodin_prescasu_mesicne;

		$('CompanyMoneyItemCelkoveNakladyFirmyNaZamestnanceBezMzdyMesicne').value 	= celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne;
		$('CompanyMoneyItemCenaUbytovaniNaMesic').value 							= cena_ubytovani_na_mesic;
		$('CompanyMoneyItemCenaPracovnichOdevuMesicne').value 						= cena_pracovnich_odevu_mesicne;
		$('CompanyMoneyItemDalsiNakladyMesicne').value 								= dalsi_naklady_mesicne;
		
		$('CompanyMoneyItemProvinzniMarze').value 									= provinzni_marze;
		$('CompanyMoneyItemCelkoveNakladyFirmyNaZamestnanceMesicne').value 			= celkove_naklady_firmy_na_zamestnance_mesicne;
		$('CompanyMoneyItemCelkovaFakturaceMesicne').value 							= celkova_fakturace_mesicne;
		$('CompanyMoneyItemFakturacniSazbaNaHodinu').value 							= fakturacni_sazba_na_hodinu;

		$('CompanyMoneyItemVypocitanaCelkovaCistaMaximalniMzdaNaHodinu').value 		= vypocitana_celkova_cista_maximalni_mzda_na_hodinu;
		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezPracSmlouvy').value 	= celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy;
		$('CompanyMoneyItemCheckboxPracovniSmlouva').value 							= checkbox_pracovni_smlouva;
		$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').value 			= pracovni_smlouva_celkova_hruba_mzda_mesicne;
		$('CompanyMoneyItemVlozenyPrispevekNaStravne').value 						= vlozeny_prispevek_na_stravne;
		$('CompanyMoneyItemVlozenyPrispevekNaCestovne').value 						= vlozeny_prispevek_na_cestovne;
		$('CompanyMoneyItemPocetDniDovoleneSvatkuZapocitanychDoNakladu').value 		= pocet_dni_dovolene_svatku_zapocitanych_do_nakladu;
		$('CompanyMoneyItemNakladyMesicneNaDovoleneSvatky').value 					= naklady_mesicne_na_dovolene_svatky;
		$('CompanyMoneyItemCistaMzdaZPracovniSmlouvyNaHodinu').value 				= cista_mzda_z_pracovni_smlouvy_na_hodinu;
		$('CompanyMoneyItemProcentuelniOdvodZamestnance').value 					= procentuelni_odvod_zamestnance;
		$('CompanyMoneyItemProcentuelniOdvodZamestnavatele').value 					= procentuelni_odvod_zamestnavatele;
		
		$('CompanyMoneyItemProcentaDaneZPrijmu').value 								= procenta_dane_z_prijmu;
		$('CompanyMoneyItemProcentaDaneZPrijmuDohoda').value 						= procenta_dane_z_prijmu_dohoda;
		$('CompanyMoneyItemOdvodyDohoda').value 									= odvody_dohoda;
		
		$('CompanyMoneyItemOdpocitatelnaPolozka').value 							= odpocitatelna_polozka;
		$('CompanyMoneyItemDalsiDanoveOdpocty').value 								= dalsi_danove_odpocty;
		$('CompanyMoneyItemCistaMzdaMesicneBezPrispevku').value 					= cista_mzda_mesicne_bez_prispevku;
		$('CompanyMoneyItemCistaMzdaZamestnanceMesicne').value 						= cista_mzda_zamestnance_mesicne;

		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezZivnostnika').value 	= celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika;
		$('CompanyMoneyItemCheckboxFaktura').value 									= checkbox_faktura;
		$('CompanyMoneyItemCistaMzdaFakturaZivnostnikaNaHodinu').value 				= cista_mzda_faktura_zivnostnika_na_hodinu;
		$('CompanyMoneyItemVlozenaProcentaNakladuNaZivnostnika').value 				= vlozena_procenta_nakladu_na_zivnostnika;

		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezDohody').value 		= celkove_mzdove_naklady_firmy_mesicne_bez_dohody;
		$('CompanyMoneyItemCheckboxDohoda').value 									= checkbox_dohoda;
		$('CompanyMoneyItemCistaMzdaDohodaNaHodinu').value 							= cista_mzda_dohoda_na_hodinu;
		//$('CompanyMoneyItemVlozenaProcentaNakladuNaDohodu').value 					= vlozena_procenta_nakladu_na_dohodu;

		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezCash').value 			= celkove_mzdove_naklady_firmy_mesicne_bez_cash;
		$('CompanyMoneyItemCheckboxCash').value 									= checkbox_cash;
		$('CompanyMoneyItemCistaMzdaCashNaHodinu').value 							= cista_mzda_cash_na_hodinu;
		$('CompanyMoneyItemVlozenaProcentaNakladuNaCash').value 					= vlozena_procenta_nakladu_na_cash;
		$('CompanyMoneyItemPocetDniVMesici').value									= pocet_dni_v_mesici;
		$('CompanyMoneyItemDanovyBonusCelkem').value 								= danovy_bonus_celkem;
		$('CompanyMoneyItemPocetDeti').value 										= pocet_deti
		$('CompanyMoneyItemDanovyBonusZaKazdeDite').value 							= danovy_bonus_za_kazde_dite;
		$('CompanyMoneyItemStravenka').value										= stravenka;
		$('CompanyMoneyItemNormoHodina').value										= normo_hodina;
		$('CompanyMoneyItemDoprava').value											= doprava;
		$('CompanyMoneyItemMhhdHodinaDefault').value    							= min_hruba_hod_mzda;
		$('CompanyMoneyItemMhhdHodinaNow').value    							    = hruba_hod_mzda;
		$('CompanyMoneyItemMhhdHodinaNow').getNext('span').setHTML('/ '+min_hruba_hod_mzda); 

		//odmeny
		$('CompanyMoneyItemOdmenaMax1').value = odmena_max_1;
		$('CompanyMoneyItemOdmenaMax2').value = odmena_max_2;
	
		if ($('CompanyMoneyItemProvinzniMarze').value > 15)
			$('CompanyMoneyItemProvinzniMarze').removeClass('invalid');
		else 
			$('CompanyMoneyItemProvinzniMarze').addClass('invalid');

		// osetreni maximální hodnoty cestovenho pokud je vetsi jak nastavena hodnota v globalnim nastavenim vloz globalni nastaveni
		if ($('CompanyMoneyItemVlozenyPrispevekNaCestovne').value.toFloat() > $('CompanyMoneyItemVlozenyPrispevekNaCestovneDefault').value.toFloat()){
			alert('Tento prispevek nemůže být větší než '+$('CompanyMoneyItemVlozenyPrispevekNaCestovneDefault').value);
			$('CompanyMoneyItemVlozenyPrispevekNaCestovne').value = $('CompanyMoneyItemVlozenyPrispevekNaCestovneDefault').value;
		}

	}
	
	function vypocet(){

		danovy_bonus_celkem = danovy_bonus_za_kazde_dite * pocet_deti;	
	
		pocet_hodin_zakladniho_pracovniho_fondu = normo_hodina * pocet_dni_v_mesici;
		celkovy_pocet_hodin_mesicne = pocet_hodin_zakladniho_pracovniho_fondu + pocet_hodin_prescasu_mesicne;
		
		celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne = 
			cena_ubytovani_na_mesic + 
			cena_pracovnich_odevu_mesicne + 
			dalsi_naklady_mesicne + 
			doprava + 
			stravenka * pocet_dni_v_mesici;
		
		cista_mzda_zamestnance_mesicne  = myround(cista_mzda_z_pracovni_smlouvy_na_hodinu * celkovy_pocet_hodin_mesicne); 
		cista_mzda_mesicne_bez_prispevku  = cista_mzda_zamestnance_mesicne - vlozeny_prispevek_na_stravne - vlozeny_prispevek_na_cestovne;		
		pracovni_smlouva_celkova_hruba_mzda_mesicne = (cista_mzda_mesicne_bez_prispevku > 335.5)?myround((cista_mzda_mesicne_bez_prispevku - (odpocitatelna_polozka / 100 * procenta_dane_z_prijmu) - danovy_bonus_celkem)/ (1 - procentuelni_odvod_zamestnance / 100 - procenta_dane_z_prijmu / 100 + (procenta_dane_z_prijmu / 100 * procentuelni_odvod_zamestnance/100))):myround((cista_mzda_mesicne_bez_prispevku - danovy_bonus_celkem)/ (1-procentuelni_odvod_zamestnance/100));
		
		//naklady_mesicne_na_dovolene_svatky = myround(pracovni_smlouva_celkova_hruba_mzda_mesicne * (1 - (procentuelni_odvod_zamestnance / 100)) / 20 * pocet_dni_dovolene_svatku_zapocitanych_do_nakladu / 12);		
		
		
		celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy  = 
			myround(
				pracovni_smlouva_celkova_hruba_mzda_mesicne * 
				(1 + procentuelni_odvod_zamestnavatele / 100) +  
				vlozeny_prispevek_na_stravne + 
				vlozeny_prispevek_na_cestovne + 
				naklady_mesicne_na_dovolene_svatky
			);	
		
		celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika = myround(cista_mzda_faktura_zivnostnika_na_hodinu * (1 + vlozena_procenta_nakladu_na_zivnostnika / 100) * celkovy_pocet_hodin_mesicne);
		
		temp_dohoda = ((cista_mzda_dohoda_na_hodinu * celkovy_pocet_hodin_mesicne) > odpocitatelna_polozka)?
			((cista_mzda_dohoda_na_hodinu * celkovy_pocet_hodin_mesicne) - (odpocitatelna_polozka * (procenta_dane_z_prijmu_dohoda/100)))/ (1 - procenta_dane_z_prijmu_dohoda / 100):
			(cista_mzda_dohoda_na_hodinu * celkovy_pocet_hodin_mesicne);
		
		celkove_mzdove_naklady_firmy_mesicne_bez_dohody =  myround(temp_dohoda * (1 + odvody_dohoda /100));
		
		
        
		celkove_mzdove_naklady_firmy_mesicne_bez_cash = myround(cista_mzda_cash_na_hodinu*(1 + vlozena_procenta_nakladu_na_cash / 100) * celkovy_pocet_hodin_mesicne);
		//celkove_naklady_firmy_na_zamestnance_mesicne = celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne + celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy + celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika + celkove_mzdove_naklady_firmy_mesicne_bez_dohody + celkove_mzdove_naklady_firmy_mesicne_bez_cash+odmena_naklady_1+odmena_naklady_2;
		//celkova_fakturace_mesicne = myround(fakturacni_sazba_na_hodinu *celkovy_pocet_hodin_mesicne)+odmena_fakturace_1+odmena_fakturace_2;  
		//provinzni_marze = myround((celkova_fakturace_mesicne - celkove_naklady_firmy_na_zamestnance_mesicne) / celkova_fakturace_mesicne * 100);
		vypocitana_celkova_cista_maximalni_mzda_na_hodinu = cista_mzda_z_pracovni_smlouvy_na_hodinu + cista_mzda_faktura_zivnostnika_na_hodinu + cista_mzda_dohoda_na_hodinu + cista_mzda_cash_na_hodinu;
		
		//vypocet min hrube hodinove mzdy	
		min_hruba_hod_mzda = myround(min_hruba_mzda / celkovy_pocet_hodin_mesicne);
		hruba_hod_mzda = myround(pracovni_smlouva_celkova_hruba_mzda_mesicne / celkovy_pocet_hodin_mesicne);
		if(hruba_hod_mzda < min_hruba_hod_mzda){ 
			$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').addClass('invalid');
			$('CompanyMoneyItemMhhdHodinaNow').addClass('invalid');
		}
		else{ 
			$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').removeClass('invalid');
			$('CompanyMoneyItemMhhdHodinaNow').removeClass('invalid');
		}	


		// vypocet max odmeny
		odmena_max_1 = myround(odmena_fakturace_1 / odmena_koeficient_1);
		odmena_max_2 = myround(odmena_fakturace_2 / odmena_koeficient_2);
	
    /**
     * nove pocitani
     * od 5.11.09
     * by Sol 
     */
        //vypocet celkove fakturace mesicne
        koeficient_navyseni = (koeficient_navyseni == 0 ? 1 : koeficient_navyseni);
        celkova_fakturace_mesicne = myround(
        (  ps_pausal_hmm * 
            (1 + procentuelni_odvod_zamestnavatele / 100) * 
            koeficient_navyseni
        )+
        odmena_fakturace_1+
        odmena_fakturace_2
	    );   
        //naklady dovolene
        //alert(procentuelni_odvod_zamestnance + '-' + procentuelni_odvod_zamestnavatele);
        if($('CompanyMoneyItemPsPausalNakladyDov').value == 0)
            naklady_mesicne_na_dovolene_svatky = myround((ps_pausal_hmm * 1.5) / 10.5);
        else 
            naklady_mesicne_na_dovolene_svatky = 0;
        
        //ostatni naklady
        ostatni_naklady = myround(ps_pausal_cestovne + ps_pausal_stravne + celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne);
        
        //naklady celkem
        celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy  = 
			myround(
				(ps_pausal_hmm + naklady_mesicne_na_dovolene_svatky) *
				(1 + procentuelni_odvod_zamestnavatele / 100) +  
				ostatni_naklady
			);
        celkove_naklady_firmy_na_zamestnance_mesicne = 
            celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy + 
            odmena_naklady_1+
            odmena_naklady_2;
	
        
        //marze
       	provinzni_marze = myround(
           (celkova_fakturace_mesicne - celkove_naklady_firmy_na_zamestnance_mesicne) / 
           celkova_fakturace_mesicne * 100
        );
	
    }
	
	$('company_work_money_edit_formular').getElements('input').addEvent('change', function(){
		load_from_inputs();
		vypocet();
		insert_to_input();
 	});
	
	//load_from_inputs(); vypocet(); insert_to_input();
	
	
	$('CompanyMoneyItemCheckboxPracovniSmlouva').removeEvents('change');
	$('CompanyMoneyItemCheckboxFaktura').removeEvents('change');
	$('CompanyMoneyItemCheckboxDohoda').removeEvents('change');
	$('CompanyMoneyItemCheckboxCash').removeEvents('change');
	
</script>