<div id='CompanyWorkMoneyObal'>
	<div class="win_fce top">
		<a href='/companies/work_money_edit/<?php echo $company_work_position_id;?>' class='button edit' id='company_money_position_add_new' title='Přidání pracovní pozice'>Nadefinovat novou odměnu</a>
	</div>
	<table class='table' id='table_work_money'>
		<tr>
			<th>#ID</th>
			<th>Název</th>
			<th>max cena</th>
			<th>cena za hod</th>
		</tr>
		<?php if (isset($work_money_list) && count($work_money_list) >0):?>
		<?php foreach($work_money_list as $work_money_item):?>
		<tr>
			<td><?php echo $work_money_item['CompanyWorkMoney']['id'];?></td>
			<td><?php echo $work_money_item['CompanyWorkMoney']['name'];?></td>
			<td><?php echo $work_money_item['CompanyWorkMoney']['max_price'];?></td>
			<td><?php echo $work_money_item['CompanyWorkMoney']['price_per_hour'];?></td>
			<td>
				<a title='Editace položky <?php echo $work_money_item['CompanyWorkMoney']['name'];?>' 	class='ta edit' href='/companies/work_money_edit/<?php echo $company_work_position_id;?>/<?php echo $work_money_item['CompanyWorkMoney']['id'];?>'/>edit</a>
			</td>
		</tr>
		<?php endforeach;?>
		<?php else:?>
		<tr>
			<td colspan='5'>K této profesi nebyla nadefinována finanční odměna</td>
		</tr>
		<?php endif;?>
	</table>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyWorkMoneyClose' class='button'/>
	</div>
</div>
<script>
	$('ListCompanyWorkMoneyClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_work_money')});
	
	$('CompanyWorkMoneyObal').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_work_money_edit',
			sizes		: [780,480],
			scrollbars	: true,
			title		: this.title,
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
	
</script>