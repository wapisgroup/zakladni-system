
<form action='/companies/work_money_edit/' method='post' id='company_work_money_edit_formular'>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/id');?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/company_id',array('value'=>16));?>
	<?php echo $htmlExt->hidden('CompanyMoneyItem/company_work_position_id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="krok1">Základní­ informace</a></li>
			<li class="ousko"><a href="krok2">Nastaveni</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Načtení kalkulací</legend>
				<div><?php echo $htmlExt->selectTag('Company/list', $company_list, null,array('label'=>'Firma'));?><br class='clear'/></div>
				<div><?php echo $htmlExt->selectTag('Position/list', array(), null,array('label'=>'Prac. pozice'));?><br class='clear'/></div>
				<div><?php echo $htmlExt->selectTag('Money/list', array(), null,array('label'=>'Kalkulace'));?><br class='clear'/></div>
			</fieldset>
			<fieldset>
				<legend>Vložení možného počtu odpracovaných hodin</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/celkovy_pocet_hodin_mesicne',								array('disabled'=>'disabled','label'=>'Celkový počet hodin měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_hodin_zakladniho_pracovniho_fondu',					array('label'=>'Počet hodin základního pracovního fondu'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_hodin_prescasu_mesicne',								array('label'=>'Počet hodin přesčasů měsíčně'));?><br class='clear' />
			</fieldset>
			<fieldset>
				<legend>Vložení nákladů na zaměstnance mimo mzdy</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne',	array('disabled'=>'disabled','label'=>'Celkové náklady firmy na zaměstnance bez mzdy měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cena_ubytovani_na_mesic',									array('label'=>'Cena ubytování na měsíc'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cena_pracovnich_odevu_mesicne',							array('label'=>'Cena pracovních oděvů měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/dalsi_naklady_mesicne',									array('label'=>'Další náklady měsíčně'));?><br class='clear' />
			</fieldset>
			<fieldset>
				<legend>Vložení fakturační sazby</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/provinzni_marze',											array('disabled'=>'disabled','label'=>'Provozní marže'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkova_fakturace_mesicne',								array('disabled'=>'disabled','label'=>'Celková fakturace měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/fakturacni_sazba_na_hodinu',								array('disabled'=>(isset($fa_disabled))?'disabled':false,'label'=>'Fakturační sazba na hodinu'));?><br class='clear' />
			</fieldset>
			<fieldset>
				<legend>Vložení způsobu vyplácení mzdy a stanovení max. čisté hodinové sazby</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/vypocitana_celkova_cista_maximalni_mzda_na_hodinu',		array('disabled'=>'disabled','label'=>'Vypočítaná celková čistá maximální mzda na hodinu'));?><br class='clear' />
				<label>Bude mzda vyplácaná formou pracovní  smlouvy?</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/checkbox_pracovni_smlouva',								array('label'=>'Bude mzda vyplácaná formou pracovní  smlouvy?'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_z_pracovni_smlouvy_na_hodinu',					array('disabled'=>'disabled','label'=>'Čistá mzda z pracovní smlouvy na hodinu'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pracovni_smlouva_celkova_hruba_mzda_mesicne',				array('disabled'=>($this->data['CompanyMoneyItem']['checkbox_pracovni_smlouva'] == 0)?'disabled':null,'label'=>'Pracovní smlouva - celková hrubá mzda měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/vlozeny_prispevek_na_stravne',								array('disabled'=>($this->data['CompanyMoneyItem']['checkbox_pracovni_smlouva'] == 0)?'disabled':null,'label'=>'Vlož příspěvek na stravné'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/vlozeny_prispevek_na_cestovne',							array('disabled'=>($this->data['CompanyMoneyItem']['checkbox_pracovni_smlouva'] == 0)?'disabled':null,'label'=>'Vlož příspěvek na cestovné'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_dni_dovolene_svatku_zapocitanych_do_nakladu',		array('label'=>'Počet dní dovoelné a svátků ročne započítaných do nákladů'));?><br class='clear' />

				<label>Bude mzda vyplácaná formou faktury živnostníkovi?</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/checkbox_faktura',										array('label'=>'Bude mzda vyplácaná formou faktury živnostníkovi?'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika',		array('disabled'=>'disabled','label'=>'Celkové mzdové náklady firmy měsíčně bez živnostníka'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_faktura_zivnostnika_na_hodinu',					array('disabled'=>($this->data['CompanyMoneyItem']['checkbox_faktura'] == 0)?'disabled':null,'label'=>'Čistá mzda - faktura živnostníka na hodinu'));?><br class='clear' />

				<label>Bude mzda vyplácaná formou dohody o vykonaní práce?</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/checkbox_dohoda',										array('label'=>'Bude mzda vyplácaná formou dohody o vykonaní práce?'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_mzdove_naklady_firmy_mesicne_bez_dohody',			array('disabled'=>'disabled','label'=>'Celkové mzdové náklady firmy měsíčně bez dohodu'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_dohoda_na_hodinu',								array('disabled'=>($this->data['CompanyMoneyItem']['checkbox_dohoda'] == 0)?'disabled':null,'label'=>'Čistá mzda - dohoda na hodinu'));?><br class='clear' />
				
				<label>Bude mzda vyplácaná formou abc?</label><?php echo $htmlExt->checkbox('CompanyMoneyItem/checkbox_cash',											array('label'=>'Bude mzda vyplácaná formou abc?'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_mzdove_naklady_firmy_mesicne_bez_cash',			array('disabled'=>'disabled','label'=>'Celkové mzdové náklady firmy měsíčně bez abc'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_cash_na_hodinu',								array('disabled'=>($this->data['CompanyMoneyItem']['checkbox_cash'] == 0)?'disabled':null,'label'=>'Čistá mzda - abc na hodinu'));?><br class='clear' />
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
				<legend>Zadejte údaje</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/normo_hodina',array('label'=>'Normo hodina'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/procentuelni_odvod_zamestnance',							array('label'=>'Procentuální odvod zaměstnance'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/procentuelni_odvod_zamestnavatele',						array('label'=>'Procentuální odvod zaměstnavatele'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/procenta_dane_z_prijmu',									array('label'=>'Vlož procenta daně z příjmu'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/odpocitatelna_polozka',									array('label'=>'Odpočitatelná položka'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/dalsi_danove_odpocty',										array('label'=>'Vlož další daňové odpočty'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/vlozena_procenta_nakladu_na_zivnostnika',					array('label'=>'Vlož procenta nákladů na živnostníka'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/vlozena_procenta_nakladu_na_dohodu',						array('label'=>'Vlož procenta nákladů na dohodu o vykonaní práce'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/vlozena_procenta_nakladu_na_cash',							array('label'=>'Vlož procenta nákladů  na abc'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/pocet_dni_v_mesici',										array('label'=>'Počet dní v měsíci'));?><br class='clear' />
			</fieldset>
			<fieldset>
				<legend>Hidden fields</legend>
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy',	array('disabled'=>'disabled','label'=>'Celkové mzdové náklady firmy měsíčně bez  pracovní smlouvy'));?><br class='clear' />	
				<?php echo $htmlExt->input('CompanyMoneyItem/naklady_mesicne_na_dovolene_svatky',						array('disabled'=>'disabled','label'=>'Náklady měsíčně na dovolenky a státní svátky'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/celkove_naklady_firmy_na_zamestnance_mesicne',				array('disabled'=>'disabled','label'=>'Celkové náklady firmy na zaměstnance měsíčně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/odvody_zamestnanec',										array('disabled'=>'disabled','label'=>'Odvody zaměstnanec'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/odvody_zamestnavatel',										array('disabled'=>'disabled','label'=>'Odvody zaměstnavatel'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/zaklad_dane',												array('disabled'=>'disabled','label'=>'Základ daně'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/dan_pred_slevami',											array('disabled'=>'disabled','label'=>'Daň před slevami'));?><br class='clear' />				
				<?php echo $htmlExt->input('CompanyMoneyItem/dan_po_slevach',											array('disabled'=>'disabled','label'=>'Daň po slevách'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_mesicne_bez_prispevku',							array('disabled'=>'disabled','label'=>'Čistá mzda měsíčně bez příspěvků'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_na_hodinu_bez_prispevku',						array('disabled'=>'disabled','label'=>'Čistá mzda na hodinu bez příspěvků'));?><br class='clear' />
				<?php echo $htmlExt->input('CompanyMoneyItem/cista_mzda_zamestnance_mesicne',							array('disabled'=>'disabled','label'=>'Čistá mzda zaměstnance měsíčně'));?><br class='clear' />
			
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='submit' value='Uložit' id='AddEditCompanyWorkMoneySaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditCompanyWorkMoneyClose'/>
	</div>
</form>
<script>
	/**
	*  Nacteni seznamu pozic k dane firme
	**/
	$('CompanyList').ajaxLoad('/companies/load_ajax_position/',['PositionList','MoneyList']);
	$('PositionList').ajaxLoad('/companies/load_ajax_money/',['MoneyList']);
	$('MoneyList').addEvent('change', function(){
		new Request.JSON({
			url: '/companies/load_ajax_money_detail/' + this.value,
			onComplete: function(json){
				$each(json, function(value,item){
					input = 'CompanyMoneyItem' + item.camelCase2().capitalize();
					$(input).value = value;
				});
			}
		}).send()
		
	});
	
	domtab = new DomTabs({'className':'admin_dom'}); 
	$('company_work_money_edit_formular').getElements('.float, .integer').inputLimit();
	
	$('AddEditCompanyWorkMoneySaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		$('company_work_money_edit_formular').getElements('input').removeProperty('disabled');
		new Request.HTML({
			url: $('company_work_money_edit_formular').action,
			update: $('domwin_pozice').getElement('.CB_ImgContainer'),
			onComplete: function(){
				domwin.closeWindow('domwin_work_money');
			}
		}).post($('company_work_money_edit_formular'));
	});
	
	function myround(num){
		return Math.round(num * 10) / 10;
	}
	
	$('CompanyMoneyItemCheckboxPracovniSmlouva').addEvent('click', function(){
		if (!this.checked){
			$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').value = 0;
			$('CompanyMoneyItemVlozenyPrispevekNaStravne').value = 0;
			$('CompanyMoneyItemVlozenyPrispevekNaCestovne').value = 0;
			$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').setProperty('disabled','disabled');
			$('CompanyMoneyItemVlozenyPrispevekNaStravne').setProperty('disabled','disabled');
			$('CompanyMoneyItemVlozenyPrispevekNaCestovne').setProperty('disabled','disabled');
		} else {
			$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').value = 8000;
			$('CompanyMoneyItemVlozenyPrispevekNaStravne').value = 165 * $('CompanyMoneyItemPocetDniVMesici').value;
			$('CompanyMoneyItemVlozenyPrispevekNaCestovne').value = 1000;
			$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').removeProperty('disabled','disabled');
			$('CompanyMoneyItemVlozenyPrispevekNaStravne').removeProperty('disabled','disabled');
			$('CompanyMoneyItemVlozenyPrispevekNaCestovne').removeProperty('disabled','disabled');
		}
		
		load_from_inputs(); vypocet(); insert_to_input();
	});
	
	$('CompanyMoneyItemCheckboxFaktura').addEvent('click', function(){
		if (!this.checked){
			$('CompanyMoneyItemCistaMzdaFakturaZivnostnikaNaHodinu').value = 0;
			$('CompanyMoneyItemCistaMzdaFakturaZivnostnikaNaHodinu').setProperty('disabled','disabled');
		} else {
			$('CompanyMoneyItemCistaMzdaFakturaZivnostnikaNaHodinu').removeProperty('disabled','disabled');
		}
		
		load_from_inputs(); vypocet(); insert_to_input();
	});
	
	
	$('CompanyMoneyItemCheckboxDohoda').addEvent('click', function(){
		if (!this.checked){
			$('CompanyMoneyItemCistaMzdaDohodaNaHodinu').value = 0;
			$('CompanyMoneyItemCistaMzdaDohodaNaHodinu').setProperty('disabled','disabled');
		} else {
			$('CompanyMoneyItemCistaMzdaDohodaNaHodinu').removeProperty('disabled','disabled');
		}
		
		load_from_inputs(); vypocet(); insert_to_input();
	});
	
	$('CompanyMoneyItemCheckboxCash').addEvent('click', function(){
		if (!this.checked){
			$('CompanyMoneyItemCistaMzdaCashNaHodinu').value = 0;
			$('CompanyMoneyItemCistaMzdaCashNaHodinu').setProperty('disabled','disabled');
		} else {
			$('CompanyMoneyItemCistaMzdaCashNaHodinu').removeProperty('disabled','disabled');
		}
		
		load_from_inputs(); vypocet(); insert_to_input();
	});
	
	
	
	function load_from_inputs(){
		celkovy_pocet_hodin_mesicne = 								$('CompanyMoneyItemCelkovyPocetHodinMesicne').value.toFloat();
		pocet_hodin_zakladniho_pracovniho_fondu = 					$('CompanyMoneyItemPocetHodinZakladnihoPracovnihoFondu').value.toFloat();
		pocet_hodin_prescasu_mesicne = 								$('CompanyMoneyItemPocetHodinPrescasuMesicne').value.toFloat();

		celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne = 	$('CompanyMoneyItemCelkoveNakladyFirmyNaZamestnanceBezMzdyMesicne').value.toFloat();
		cena_ubytovani_na_mesic = 									$('CompanyMoneyItemCenaUbytovaniNaMesic').value.toFloat();
		cena_pracovnich_odevu_mesicne = 							$('CompanyMoneyItemCenaPracovnichOdevuMesicne').value.toFloat();
		dalsi_naklady_mesicne = 									$('CompanyMoneyItemDalsiNakladyMesicne').value.toFloat();

		provinzni_marze = 											$('CompanyMoneyItemProvinzniMarze').value.toFloat();
		celkove_naklady_firmy_na_zamestnance_mesicne = 				$('CompanyMoneyItemCelkoveNakladyFirmyNaZamestnanceMesicne').value.toFloat();
		celkova_fakturace_mesicne = 								$('CompanyMoneyItemCelkovaFakturaceMesicne').value.toFloat();
		fakturacni_sazba_na_hodinu = 								$('CompanyMoneyItemFakturacniSazbaNaHodinu').value.toFloat();

		vypocitana_celkova_cista_maximalni_mzda_na_hodinu = 		$('CompanyMoneyItemVypocitanaCelkovaCistaMaximalniMzdaNaHodinu').value.toFloat();
		celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy = 	$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezPracSmlouvy').value.toFloat();
		checkbox_pracovni_smlouva = 								$('CompanyMoneyItemCheckboxPracovniSmlouva').value.toFloat();
		pracovni_smlouva_celkova_hruba_mzda_mesicne = 				$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').value.toFloat();
		vlozeny_prispevek_na_stravne = 								$('CompanyMoneyItemVlozenyPrispevekNaStravne').value.toFloat();
		vlozeny_prispevek_na_cestovne = 							$('CompanyMoneyItemVlozenyPrispevekNaCestovne').value.toFloat();
		pocet_dni_dovolene_svatku_zapocitanych_do_nakladu = 		$('CompanyMoneyItemPocetDniDovoleneSvatkuZapocitanychDoNakladu').value.toFloat();
		naklady_mesicne_na_dovolene_svatky = 						$('CompanyMoneyItemNakladyMesicneNaDovoleneSvatky').value.toFloat();
		cista_mzda_z_pracovni_smlouvy_na_hodinu = 					$('CompanyMoneyItemCistaMzdaZPracovniSmlouvyNaHodinu').value.toFloat();
		procentuelni_odvod_zamestnance = 							$('CompanyMoneyItemProcentuelniOdvodZamestnance').value.toFloat();
		procentuelni_odvod_zamestnavatele = 						$('CompanyMoneyItemProcentuelniOdvodZamestnavatele').value.toFloat();
		odvody_zamestnanec = 										$('CompanyMoneyItemOdvodyZamestnanec').value.toFloat();
		odvody_zamestnavatel = 										$('CompanyMoneyItemOdvodyZamestnavatel').value.toFloat();
		zaklad_dane = 												$('CompanyMoneyItemZakladDane').value.toFloat();
		procenta_dane_z_prijmu = 									$('CompanyMoneyItemProcentaDaneZPrijmu').value.toFloat();
		dan_pred_slevami = 											$('CompanyMoneyItemDanPredSlevami').value.toFloat();
		odpocitatelna_polozka = 									$('CompanyMoneyItemOdpocitatelnaPolozka').value.toFloat();
		dalsi_danove_odpocty = 										$('CompanyMoneyItemDalsiDanoveOdpocty').value.toFloat();
		dan_po_slevach = 											$('CompanyMoneyItemDanPoSlevach').value.toFloat();
		cista_mzda_mesicne_bez_prispevku = 							$('CompanyMoneyItemCistaMzdaMesicneBezPrispevku').value.toFloat();
		cista_mzda_na_hodinu_bez_prispevku = 						$('CompanyMoneyItemCistaMzdaNaHodinuBezPrispevku').value.toFloat();
		cista_mzda_zamestnance_mesicne = 							$('CompanyMoneyItemCistaMzdaZamestnanceMesicne').value.toFloat();

		celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika = 		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezZivnostnika').value.toFloat();
		checkbox_faktura = 											$('CompanyMoneyItemCheckboxFaktura').value.toFloat();
		cista_mzda_faktura_zivnostnika_na_hodinu = 					$('CompanyMoneyItemCistaMzdaFakturaZivnostnikaNaHodinu').value.toFloat();
		vlozena_procenta_nakladu_na_zivnostnika = 					$('CompanyMoneyItemVlozenaProcentaNakladuNaZivnostnika').value.toFloat();

		celkove_mzdove_naklady_firmy_mesicne_bez_dohody = 			$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezDohody').value.toFloat();
		checkbox_dohoda = 											$('CompanyMoneyItemCheckboxDohoda').value.toFloat();
		cista_mzda_dohoda_na_hodinu = 								$('CompanyMoneyItemCistaMzdaDohodaNaHodinu').value.toFloat();
		vlozena_procenta_nakladu_na_dohodu = 						$('CompanyMoneyItemVlozenaProcentaNakladuNaDohodu').value.toFloat();

		celkove_mzdove_naklady_firmy_mesicne_bez_cash = 			$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezCash').value.toFloat();
		checkbox_cash = 											$('CompanyMoneyItemCheckboxCash').value.toFloat();
		cista_mzda_cash_na_hodinu = 								$('CompanyMoneyItemCistaMzdaCashNaHodinu').value.toFloat();
		vlozena_procenta_nakladu_na_cash = 							$('CompanyMoneyItemVlozenaProcentaNakladuNaCash').value.toFloat();
		pocet_dni_v_mesici = 										$('CompanyMoneyItemPocetDniVMesici').value.toFloat();
	}
	
	function insert_to_input(){
		$('CompanyMoneyItemCelkovyPocetHodinMesicne').value 						= celkovy_pocet_hodin_mesicne;
		$('CompanyMoneyItemPocetHodinZakladnihoPracovnihoFondu').value 				= pocet_hodin_zakladniho_pracovniho_fondu;
		$('CompanyMoneyItemPocetHodinPrescasuMesicne').value 						= pocet_hodin_prescasu_mesicne;

		$('CompanyMoneyItemCelkoveNakladyFirmyNaZamestnanceBezMzdyMesicne').value 	= celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne;
		$('CompanyMoneyItemCenaUbytovaniNaMesic').value 							= cena_ubytovani_na_mesic;
		$('CompanyMoneyItemCenaPracovnichOdevuMesicne').value 						= cena_pracovnich_odevu_mesicne;
		$('CompanyMoneyItemDalsiNakladyMesicne').value 								= dalsi_naklady_mesicne;
		
		$('CompanyMoneyItemProvinzniMarze').value 									= provinzni_marze;
		$('CompanyMoneyItemCelkoveNakladyFirmyNaZamestnanceMesicne').value 			= celkove_naklady_firmy_na_zamestnance_mesicne;
		$('CompanyMoneyItemCelkovaFakturaceMesicne').value 							= celkova_fakturace_mesicne;
		$('CompanyMoneyItemFakturacniSazbaNaHodinu').value 							= fakturacni_sazba_na_hodinu;

		$('CompanyMoneyItemVypocitanaCelkovaCistaMaximalniMzdaNaHodinu').value 		= vypocitana_celkova_cista_maximalni_mzda_na_hodinu;
		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezPracSmlouvy').value 	= celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy;
		$('CompanyMoneyItemCheckboxPracovniSmlouva').value 							= checkbox_pracovni_smlouva;
		$('CompanyMoneyItemPracovniSmlouvaCelkovaHrubaMzdaMesicne').value 			= pracovni_smlouva_celkova_hruba_mzda_mesicne;
		$('CompanyMoneyItemVlozenyPrispevekNaStravne').value 						= vlozeny_prispevek_na_stravne;
		$('CompanyMoneyItemVlozenyPrispevekNaCestovne').value 						= vlozeny_prispevek_na_cestovne;
		$('CompanyMoneyItemPocetDniDovoleneSvatkuZapocitanychDoNakladu').value 		= pocet_dni_dovolene_svatku_zapocitanych_do_nakladu;
		$('CompanyMoneyItemNakladyMesicneNaDovoleneSvatky').value 					= naklady_mesicne_na_dovolene_svatky;
		$('CompanyMoneyItemCistaMzdaZPracovniSmlouvyNaHodinu').value 				= cista_mzda_z_pracovni_smlouvy_na_hodinu;
		$('CompanyMoneyItemProcentuelniOdvodZamestnance').value 					= procentuelni_odvod_zamestnance;
		$('CompanyMoneyItemProcentuelniOdvodZamestnavatele').value 					= procentuelni_odvod_zamestnavatele;
		$('CompanyMoneyItemOdvodyZamestnanec').value 								= odvody_zamestnanec;
		$('CompanyMoneyItemOdvodyZamestnavatel').value 								= odvody_zamestnavatel;
		$('CompanyMoneyItemZakladDane').value 										= zaklad_dane;
		$('CompanyMoneyItemProcentaDaneZPrijmu').value 								= procenta_dane_z_prijmu;
		$('CompanyMoneyItemDanPredSlevami').value 									= dan_pred_slevami;
		$('CompanyMoneyItemOdpocitatelnaPolozka').value 							= odpocitatelna_polozka;
		$('CompanyMoneyItemDalsiDanoveOdpocty').value 								= dalsi_danove_odpocty;
		$('CompanyMoneyItemDanPoSlevach').value 									= dan_po_slevach;
		$('CompanyMoneyItemCistaMzdaMesicneBezPrispevku').value 					= cista_mzda_mesicne_bez_prispevku;
		$('CompanyMoneyItemCistaMzdaNaHodinuBezPrispevku').value 					= cista_mzda_na_hodinu_bez_prispevku;
		$('CompanyMoneyItemCistaMzdaZamestnanceMesicne').value 						= cista_mzda_zamestnance_mesicne;

		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezZivnostnika').value 	= celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika;
		$('CompanyMoneyItemCheckboxFaktura').value 									= checkbox_faktura;
		$('CompanyMoneyItemCistaMzdaFakturaZivnostnikaNaHodinu').value 				= cista_mzda_faktura_zivnostnika_na_hodinu;
		$('CompanyMoneyItemVlozenaProcentaNakladuNaZivnostnika').value 				= vlozena_procenta_nakladu_na_zivnostnika;

		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezDohody').value 		= celkove_mzdove_naklady_firmy_mesicne_bez_dohody;
		$('CompanyMoneyItemCheckboxDohoda').value 									= checkbox_dohoda;
		$('CompanyMoneyItemCistaMzdaDohodaNaHodinu').value 							= cista_mzda_dohoda_na_hodinu;
		$('CompanyMoneyItemVlozenaProcentaNakladuNaDohodu').value 					= vlozena_procenta_nakladu_na_dohodu;

		$('CompanyMoneyItemCelkoveMzdoveNakladyFirmyMesicneBezCash').value 			= celkove_mzdove_naklady_firmy_mesicne_bez_cash;
		$('CompanyMoneyItemCheckboxCash').value 									= checkbox_cash;
		$('CompanyMoneyItemCistaMzdaCashNaHodinu').value 							= cista_mzda_cash_na_hodinu;
		$('CompanyMoneyItemVlozenaProcentaNakladuNaCash').value 					= vlozena_procenta_nakladu_na_cash;
		$('CompanyMoneyItemPocetDniVMesici').value									= pocet_dni_v_mesici;
		
		if ($('CompanyMoneyItemProvinzniMarze').value > 15)
			$('CompanyMoneyItemProvinzniMarze').removeClass('invalid');
		else 
			$('CompanyMoneyItemProvinzniMarze').addClass('invalid');

	}
	
	function vypocet(){
	
	
		celkovy_pocet_hodin_mesicne = pocet_hodin_zakladniho_pracovniho_fondu + pocet_hodin_prescasu_mesicne;
		celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne = cena_ubytovani_na_mesic + cena_pracovnich_odevu_mesicne + dalsi_naklady_mesicne;
		//vlozeny_prispevek_na_stravne = 165*pocet_dni_v_mesici;
		odvody_zamestnavatel = myround(pracovni_smlouva_celkova_hruba_mzda_mesicne / 100  * procentuelni_odvod_zamestnavatele);
		zaklad_dane = pracovni_smlouva_celkova_hruba_mzda_mesicne + odvody_zamestnavatel;
		naklady_mesicne_na_dovolene_svatky = (zaklad_dane / 20 ) * (pocet_dni_dovolene_svatku_zapocitanych_do_nakladu / 12);
		celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy = pracovni_smlouva_celkova_hruba_mzda_mesicne + odvody_zamestnavatel + vlozeny_prispevek_na_stravne + vlozeny_prispevek_na_cestovne + naklady_mesicne_na_dovolene_svatky;
		
		
		celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika = myround(cista_mzda_faktura_zivnostnika_na_hodinu * (1 + vlozena_procenta_nakladu_na_zivnostnika / 100) * celkovy_pocet_hodin_mesicne);
		celkove_mzdove_naklady_firmy_mesicne_bez_dohody = myround(cista_mzda_dohoda_na_hodinu * (1 + vlozena_procenta_nakladu_na_dohodu / 100) * celkovy_pocet_hodin_mesicne);
		celkove_mzdove_naklady_firmy_mesicne_bez_cash = myround(cista_mzda_cash_na_hodinu * (1 + vlozena_procenta_nakladu_na_cash / 100) * 	celkovy_pocet_hodin_mesicne);
		
		
		celkove_naklady_firmy_na_zamestnance_mesicne = 
			celkove_naklady_firmy_na_zamestnance_bez_mzdy_mesicne + // ok
			celkove_mzdove_naklady_firmy_mesicne_bez_prac_smlouvy +  // ok
			celkove_mzdove_naklady_firmy_mesicne_bez_zivnostnika + // ok
			celkove_mzdove_naklady_firmy_mesicne_bez_dohody + // ok
			celkove_mzdove_naklady_firmy_mesicne_bez_cash; //ok
			
		
		celkova_fakturace_mesicne =myround( fakturacni_sazba_na_hodinu * celkovy_pocet_hodin_mesicne);
		provinzni_marze = myround((celkova_fakturace_mesicne - celkove_naklady_firmy_na_zamestnance_mesicne) / celkova_fakturace_mesicne * 100);		
		
		
		odvody_zamestnanec = myround(pracovni_smlouva_celkova_hruba_mzda_mesicne / 100  * procentuelni_odvod_zamestnance);	
		
		dan_pred_slevami = myround(zaklad_dane / 100 * procenta_dane_z_prijmu);	
		dan_po_slevach = ((dan_pred_slevami - odpocitatelna_polozka - dalsi_danove_odpocty) > 0)?(dan_pred_slevami - odpocitatelna_polozka - dalsi_danove_odpocty):0;
		
		cista_mzda_mesicne_bez_prispevku = pracovni_smlouva_celkova_hruba_mzda_mesicne - odvody_zamestnanec - dan_po_slevach;
		cista_mzda_zamestnance_mesicne = cista_mzda_mesicne_bez_prispevku + vlozeny_prispevek_na_stravne + vlozeny_prispevek_na_cestovne;
		cista_mzda_z_pracovni_smlouvy_na_hodinu = myround(cista_mzda_zamestnance_mesicne / celkovy_pocet_hodin_mesicne);	
		
		vypocitana_celkova_cista_maximalni_mzda_na_hodinu = 
			cista_mzda_z_pracovni_smlouvy_na_hodinu + 
			cista_mzda_faktura_zivnostnika_na_hodinu + 
			cista_mzda_dohoda_na_hodinu + 
			cista_mzda_cash_na_hodinu;
	
		cista_mzda_na_hodinu_bez_prispevku = myround(cista_mzda_mesicne_bez_prispevku / celkovy_pocet_hodin_mesicne);		
		// ok	
	}
	
	$('company_work_money_edit_formular').getElements('input').addEvent('change', function(){
		load_from_inputs();
		vypocet();
		insert_to_input();
 	});
	
	//load_from_inputs(); vypocet(); insert_to_input();
	
	
	$('CompanyMoneyItemCheckboxPracovniSmlouva').removeEvents('change');
	$('CompanyMoneyItemCheckboxFaktura').removeEvents('change');
	$('CompanyMoneyItemCheckboxDohoda').removeEvents('change');
	$('CompanyMoneyItemCheckboxCash').removeEvents('change');
	
</script>