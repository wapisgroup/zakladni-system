<table class='table'>
	<tr><th>Jméno</th><th></th></tr>
	<?php 
	//print_r($neco);
	
	if (isset($client_list) && count($client_list)>0):
			foreach($client_list as $id => $name):
			if(!in_array($id,$neco)){
				echo "<tr><td>$name</td><td>
					<a href='/report_requirements_for_recruitments/add_uchazec/$requirement_id/$company_id/$id' rel='client_id=$id' class='add_to_curr_possition'>Přidat</a>
					|
					<a href='/clients/edit/$id/domwin/only_show' class='client_info'>Info</a>
				</td></tr>";
			}
			else 
				echo "<tr><td class='color_red'>$name</td><td> <a href='/clients/edit/$id/domwin/only_show' class='client_info'>Info</a></td></tr>";
			endforeach;
		else:
			echo "<tr><td colspan='2' class='text_center'>K dané kvalifikaci nebyl nalezen klient.</td></td>";
		endif;
	?>
</table>

<script>

$$('.add_to_curr_possition').addEvent('click', function(e){
		new Event(e).stop();
		volne = $('right_pozice').getElement('.neobsazeno');
		dnes = '<?php echo date('d.m.Y')?>';
		umistovatel = '<?php echo $umistovatel;?>';
		cms_group_id = '<?php echo $group_id;?>';

		if (volne){
			new Request.JSON({
				url: this.getProperty('href'),
                data : {'data[neco]':1},
				onComplete: (function(json){
					if (json.result === true){

						nazev = this.getParent('tr').getElement('td').getHTML();
						volne.removeClass('neobsazeno');
						volne.addClass('obsazeno');
						volne.getElement('.td_client').setHTML(nazev);
						this.getParent('tr').addClass('none');
						
						if (volne.hasClass('nahradnici'))
							volne.getElement('.td_status').setHTML('Náhradník');
						else
							volne.getElement('.td_status').setHTML('Umístěn');

						volne.getElement('.td_prirazen').setHTML(umistovatel);
						volne.getElement('.td_od').setHTML(dnes);
						nahradnici_class = ((volne.hasClass('nahradnici'))?'none':'');
						
						if(cms_group_id != 8){
							new Element('a',{href:'/report_requirements_for_recruitments/remove_uchazec/' + json.id, class:'remove_from_curr_position'}).setHTML('Odstranit ').inject(volne.getElement('.pos_del')).addEvent('click', call_domwin_for_remove_uchazec.bindWithEvent(this));
							new Element('a',{href:'/report_requirements_for_recruitments/add_zamestnanec/' + json.id, class:'add_as_employment ' + nahradnici_class}).setHTML('Zaměstnat ').inject(volne.getElement('.pos_del')).addEvent('click', call_domwin_for_add_zamestnanec.bindWithEvent(this));
							new Element('a',{href:'/report_requirements_for_recruitments/remove_zamestnanec/' + json.id, class:'remove_as_employment none'}).setHTML('Propustit ').inject(volne.getElement('.pos_del')).addEvent('click', call_domwin_for_del_zamestnanec.bindWithEvent(this));
						}
						new Element('a',{href:'/clients/edit/' + this.getProperty('rel').substring(10)+'/domwin/only_show', class:'client_info'}).setHTML('Info').inject(volne.getElement('.pos_del')).addEvent('click', call_domwin_client_info.bindWithEvent(this));
						
						new Element('input', {type:'hidden',class:'client_id', value:this.getProperty('rel').substring(10)}).inject(volne.getElement('.pos_del'));
						new Element('input', {type:'hidden',class:'connection_id', value:json.id}).inject(volne.getElement('.pos_del'));
						new Element('input', {type:'hidden',class:'company_id', value:<?php echo $company_id;?>}).inject(volne.getElement('.pos_del'));
						new Element('input', {type:'hidden',class:'requirement_id', value:<?php echo $requirement_id;?>}).inject(volne.getElement('.pos_del'));
					} 
					else if (json.result == -1)	{}
						
					else {
						alert(json.message);
					}
				}).bind(this)
			}).send();
		} else {
			alert('vse je jiz obsazeno');
		}
	});
	
	$$('.client_info').addEvent('click', call_domwin_client_info.bindWithEvent(this));
  
	function call_domwin_client_info (e){
	var event = new Event(e); 
	obj = new Event(e).target;
 	event.stop();

		domwin.newWindow({
			id			: 'domwin',
			sizes		: [780,800],
			scrollbars	: true,
			title		: 'Informace o klientovi',
			languages	: false,
			type		: 'AJAX',
			ajax_url	: obj.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	};
</script>