<form action='/report_requirements_for_recruitments/edit_express/' method='post' id='company_nabor_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Pracovište</a></li>
			<li class="ousko"><a href="#krok2">Parametry pozice</a></li>
			<li class="ousko"><a href="#krok3">Místa</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
            	<legend>Kontakty a pracoviště</legend>
            	<div class='sll'>
            		<?php echo $htmlExt->selectTag('RequirementsForRecruitment/company_id', $company_list,null, array('label'=>'Firma'));?>  <br class='clear' />
            	</div>
            	<div class='slr'>
            		<?php echo $htmlExt->input('RequirementsForRecruitment/job_city', array('label'=>'Pracoviště město'));?> <br class='clear' />
            	</div><br class='clear' />
            	<div>
            </fieldset>
		</div>
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('RequirementsForRecruitment/id');?>
			<?php // echo $htmlExt->hidden('RequirementsForRecruitment/company_id');?>
			<fieldset>
				<legend>Parametry pozice</legend>
				<?php echo $htmlExt->input('RequirementsForRecruitment/name', array('label'=>'Požadovaná profese', 'class'=>'long', 'label_class'=>'long'));?> <br class='clear' />
			</fieldset>
		</div>
		<div class="domtabs field">
			<fieldset>
			<div class='sll'>
				<?php echo $htmlExt->input('RequirementsForRecruitment/count_of_free_position', array('label'=>'Počet volných míst'));?> <br class='clear' />
			</div>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditRequirementsForRecruitmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditRequirementsForRecruitmentClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	var externi = <?php echo (isset($disabledSave) && $disabledSave!="") ? true : 'false';?>;

	if(externi) // pokud je zamezen save skryj tl. save
		$('AddEditRequirementsForRecruitmentSaveAndClose').addClass('none');


	$('RequirementsForRecruitmentCompanyId').addEvent('change', function(){
		new Request.JSON({
			url: '/companies/load_profesion_by_company/' + this.value,
			onComplete: function(json){
				if (json){
					if (json.result === true){
						
						$('RequirementsForRecruitmentJobCity').value = json.data.mesto;
						if(json.data.mesto != null)
							$('RequirementsForRecruitmentJobCity').removeClass('require');
						else
							$('RequirementsForRecruitmentJobCity').addClass('invalid');
						
					} 
				} else {
					alert('chyba aplikace');
				}
			}
		}).send();
	});
	

	$('AddEditRequirementsForRecruitmentClose').addEvent('click',function(e){new Event(e).stop(); 
	<?php if (isset($z_reportu)):?>
		domwin.closeWindow('domwin');
	<?php else:?>
		domwin.closeWindow('domwin');
	<?php endif;?>
	});
	
	$('AddEditRequirementsForRecruitmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('company_nabor_edit_formular');
		if (valid_result == true){
			<?php if (isset($z_reportu)):?>
			var request = new Request.JSON({url:$('company_nabor_edit_formular').action, onComplete:function(){domwin.closeWindow('domwin');click_refresh($('RequirementsForRecruitmentId').value);}})			
			<?php else:?>
			var request = new Request.HTML({url:$('company_nabor_edit_formular').action, update: $('domwin').getElement('.CB_ImgContainer'), onComplete:function(){domwin.closeWindow('domwin');}})			
			<?php endif;?>
			request.post($('company_nabor_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});

	validation.define('company_nabor_edit_formular',{
		'RequirementsForRecruitmentName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit požadovanou profesi'}
		},
		'RequirementsForRecruitmentSalary': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit mzda od-do'}
		},
		'RequirementsForRecruitmentJobCity': {
			'testReq': {'condition':'not_empty','err_message':'Není zvoleno město'}
		}
	});
	validation.generate('company_nabor_edit_formular',<?php echo (isset($this->data['RequirementsForRecruitment']['id']))?'true':'false';?>);
	

</script>