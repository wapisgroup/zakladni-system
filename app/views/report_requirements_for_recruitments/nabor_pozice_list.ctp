<?php
	$company_id = $detail['RequirementsForRecruitment']['company_id'];
	$requirement_id = $detail['RequirementsForRecruitment']['id'];
	$title = $detail['RequirementsForRecruitment']['name']." - ".$detail['Company']['name']." - ".$kvalifikace_list[$detail['RequirementsForRecruitment']['setting_career_item_id']];
	if($print)
		echo "<h1>".$title."</h1>";
?>
<div style='width:30%; float:left;border: 0px solid blue; min-height:296px;' class="<?php if($print) echo "noprint";?>">
	<label>Kvalifikace</label><?php echo $htmlExt->selectTag('RequirementsForRecruitment/setting_career_item_id',$kvalifikace_list,$detail['RequirementsForRecruitment']['setting_career_item_id']);?><br/>
	<div id='client_list'>
		<?php if(!$print) echo $this->renderElement('../report_requirements_for_recruitments/nabor_pozice_client_list',array('requirement_id'=>$requirement_id,'company_id'=>$company_id));?>	
	</div>
</div>
<div style='<?php if($print){ echo "width:100%;";} else {echo"width:68%; border:0px solid red; margin:0px 0px 0px 31%; min-height:296px;";}?>' id='right_pozice'>
	<table class='table'>
		<tr>
			<th>Pozice</th>
			<th>Klient</th>
			<th>Přiřazen</th>
			<th>Od</th>
			<th>Status</th>
			<th class="<?php if($print) echo "noprint";?>">Možnosti</th>
		</tr>
		<tr><td colspan='5'><strong>Volné místa</strong></td></tr>
		<?php for($i = 0; $i<$detail['RequirementsForRecruitment']['count_of_free_position']; $i++):?>
		<tr class='<?php echo (isset($add_clients[$i]))?'obsazeno':'neobsazeno noprint';?>'>
			<td><?php echo $i+1;?></td>
			<td class='td_client'><?php 
								if (isset($add_clients[$i])) {
									if(($group_id == 8) && !in_array($add_clients[$i]['Client']['id'],$neco2))
										echo "--------";
									else
										echo $add_clients[$i]['Client']['name']; 
								} 
								else { 
									echo 'Nepřiřazeno';
								}?>
			</td>
			<td class='td_prirazen'><?php echo $add_clients[$i]['CmsUser']['name']?></td>
			<td class='td_od'><?php echo $add_clients[$i]['ConnectionClientRequirement']['created']=="" ? "" : $fastest->czechDate($add_clients[$i]['ConnectionClientRequirement']['created']);?></td>
			<td class='td_status'>
				<?php 
				if (isset($add_clients[$i])):
					switch($add_clients[$i]['ConnectionClientRequirement']['type']){
						case 1: echo 'Umístěn'; break;
						case 2: echo 'Zaměstnanec'; break;
						default: echo ''; break;
					}
				endif;
				?>
			</td>
			<td class='pos_del <?php if($print) echo "noprint";?>'>
				<?php if (isset($add_clients[$i])):?>
					<?php 
						switch($add_clients[$i]['ConnectionClientRequirement']['type']){
							case 1: 
								$remove_uchazec_class = '';
								$add_zamestnanec_class = '';
								$remove_zamestnanec_class = 'none';
							break;
							case 2: 
								$remove_uchazec_class = 'none';
								$add_zamestnanec_class = 'none';
								$remove_zamestnanec_class = '';
							break;
							default: 
								$remove_uchazec_class = 'none';
								$add_zamestnanec_class = 'none';
								$remove_zamestnanec_class = 'none';
							break;
						}
					if(in_array($group_id,array(3,4,1,5,9))){
					?>
					<a href="/report_requirements_for_recruitments/remove_uchazec/<?php echo $add_clients[$i]['ConnectionClientRequirement']['id'];?>" class="remove_from_curr_position <?php echo $remove_uchazec_class;?>">Odstranit</a>
					<a href="/report_requirements_for_recruitments/add_zamestnanec/<?php echo $add_clients[$i]['ConnectionClientRequirement']['id'];?>" class="add_as_employment <?php echo $add_zamestnanec_class;?>">Zaměstnat</a>
					<?}
					if((($group_id == 8) && in_array($add_clients[$i]['Client']['id'],$neco2)) || ($group_id != 8)){
					?>
						| <a href='/clients/edit/<?php echo $add_clients[$i]['Client']['id'];?>/domwin/only_show' class='client_info'>Info</a>
				   <?php } ?>
					<input type="hidden" class="client_id" value="<?php echo $add_clients[$i]['Client']['id'];?>"/>
				  <input type="hidden" class="connection_id" value="<?php echo $add_clients[$i]['ConnectionClientRequirement']['id'];?>"/>
          <input type="hidden" class="company_id" value="<?php echo $detail['RequirementsForRecruitment']['company_id'];?>"/>
          <input type="hidden" class="requirement_id" value="<?php echo $detail['RequirementsForRecruitment']['id'];?>"/>
        <?php 
        endif;?>
			</td>
		</tr>
		<?php endfor;?>
		<tr><td colspan='5'><strong>Náhradníci</strong></td></tr>
		<?php for($j = 0; $j<$detail['RequirementsForRecruitment']['count_of_substitute']; $j++):?>
		
		<tr class='<?php echo (isset($add_clients[$i]))?'obsazeno':'neobsazeno noprint';?> nahradnici'>
			<td><?php echo $j+1;?></td>
			<td class='td_client'><?php if (isset($add_clients[$i])) {

										  if(($group_id == 8) && !in_array($add_clients[$i]['Client']['id'],$neco2))
											echo "--------";
										  else
											echo $add_clients[$i]['Client']['name']; 
										
										} else 
										{ echo 'Nepřiřazeno';}?>
			</td>
			<td class='td_prirazen'><?php echo $add_clients[$i]['CmsUser']['name']?></td>
			<td class='td_od'><?php echo $add_clients[$i]['ConnectionClientRequirement']['created']=="" ? "" : $fastest->czechDate($add_clients[$i]['ConnectionClientRequirement']['created']);?></td>
			<td class='td_status'>
				<?php 
				if (isset($add_clients[$i])):
					echo 'Náhradník';
				endif;
				?>
			</td>
			<td class='pos_del <?php if($print) echo "noprint";?>'>
				<?php if (isset($add_clients[$i])):
					
				if(in_array($group_id,array(3,4,1,5))){
				?>
					<a href="/report_requirements_for_recruitments/remove_uchazec/<?php echo $add_clients[$i]['ConnectionClientRequirement']['id'];?>" class="remove_from_curr_position <?php echo $remove_uchazec_class;?>">Odstranit</a>
					<a href="/report_requirements_for_recruitments/add_zamestnanec/<?php echo $add_clients[$i]['ConnectionClientRequirement']['id'];?>" class="add_as_employment none">Zaměstnat</a>
				<?}
					if((($group_id == 8) && in_array($add_clients[$i]['Client']['id'],$neco2)) || ($group_id != 8)){
					?>
						| <a href='/clients/edit/<?php echo $add_clients[$i]['Client']['id'];?>/domwin/only_show' class='client_info'>Info</a>
				   <?php } ?>
					<input type="hidden" class="client_id" value="<?php echo $add_clients[$i]['Client']['id'];?>"/>
					<input type="hidden" class="connection_id" value="<?php echo $add_clients[$i]['ConnectionClientRequirement']['id'];?>"/>
				  <input type="hidden" class="company_id" value="<?php echo $detail['RequirementsForRecruitment']['company_id'];?>"/>
          <input type="hidden" class="requirement_id" value="<?php echo $detail['RequirementsForRecruitment']['id'];?>"/>
        <?php endif;?>
			</td>
		</tr>
		<?php $i++;?>
		<?php endfor;?>
	</table>
</div>
<a href="/int_requirements/nabor_pozice/<?php echo $detail['RequirementsForRecruitment']['id'];?>?print=true" target="_blank" class="<?php if($print) echo "noprint";?>">Tisknout</a>
<br class='clear'/>

<?php // pr($attachment_list);?>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyAttachmentClose' class='button'/>
	</div>
<script>
<?php if($print){ ?>
	window.print();

<?php } 
else {  
?>
	function remove_uses_client(){
		$('right_pozice').getElements('.client_id').each(function(item){
			var id = item.value;
			var client_list_tr_restore = $('client_list').getElement('a[rel=client_id=' + id + ']')
			if (client_list_tr_restore){
				client_list_tr_restore.getParent('tr').addClass('none');
			}
		});
	}

	$('RequirementsForRecruitmentSettingCareerItemId').addEvent('change', function(){
		new Request.HTML({
			url: '/report_requirements_for_recruitments/load_client/' + this.value + '/' + <?php echo $company_id;?> + '/' + <?php echo $requirement_id;?>,
			update: $('client_list'),
			onComplete: function(){
				remove_uses_client();
			}
		}).send();
	});

	$$('.remove_from_curr_position').addEvent('click', call_domwin_for_remove_uchazec.bindWithEvent(this));
  
  function call_domwin_for_remove_uchazec(e){
    var event = new Event(e);
    event.stop();
		var obj = event.target;
    var tr=obj.getParent('tr');
		
    obj.addClass('kesmazani');
		domwin.newWindow({
			id			: 'domwin_remove_curr_add',
			sizes		: [580,300],
			scrollbars	: true,
			title		: 'Odstranění uchazeče '+ tr.getElement('.td_client').getHTML(),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: '/report_requirements_for_recruitments/remove_dotaznik/'+tr.getElement('.client_id').value+'/'+tr.getElement('.company_id').value+'/'+tr.getElement('.requirement_id').value,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	};
	  
  	
	function remove_from_position(e){
		//var event = new Event(e), 
    obj=$('right_pozice').getElement('.kesmazani');
    tr = obj.getParent('tr'), table = tr.getParent('table'), id = tr.getElement('.client_id').value;
		//event.stop();
		
		obj.removeClass('kesmazani');
		if (!tr.hasClass('neobsazeno')){
			new Request.JSON({
				url: obj.getProperty('href'),
				onComplete: function(json){
					if (json.result === true){
						var client_list_tr_restore = $('client_list').getElement('a[rel=client_id=' + id + ']')
						if (client_list_tr_restore){
							client_list_tr_restore.getParent('tr').removeClass('none');
						}
						
						tr.removeClass('obsazeno');
						tr.addClass('neobsazeno');
			
						tr.getElement('.td_client').setHTML('Nepřiřazeno');
						tr.getElement('.td_prirazen').setHTML('');
						tr.getElement('.td_od').setHTML('');
						tr.getElement('.td_status').setHTML('');
						
						
						// posune polozky nahoru, pokud existuji zaplnene
						tr.getAllNext('.obsazeno').each(function(item_next){
							prev = item_next.getPrevious('.neobsazeno');
							prev.getElement('.td_client').setHTML(item_next.getElement('.td_client').getHTML());
							prev.getElement('.td_status').setHTML(item_next.getElement('.td_status').getHTML());
							
							//prev.getElement('.pos_del').setHTML(item_next.getElement('.pos_del').getHTML());
							prev.getElement('.pos_del').empty();
							prev.getElement('.pos_del').adopt(item_next.getElement('.pos_del').getElements('*'));
							
							if(item_next.hasClass('nahradnici')){
								prev.getElement('.add_as_employment').removeClass('none');
								prev.getElement('.td_status').setHTML('Umístěn');
							}
							
							//prev.getElement('.client_id').value = item_next.getElement('.client_id').value;
							
							prev.removeClass('neobsazeno');
							prev.addClass('obsazeno');
							
							item_next.addClass('neobsazeno');
							item_next.removeClass('obsazeno');
							
							item_next.getElement('.td_client').setHTML('Nepřiřazeno');
							item_next.getElement('.pos_del').setHTML(' ');
							item_next.getElement('.td_status').setHTML(' ');
						});
						
						// odstraneni vsech linku na smazani u neobsazenych radku
						table.getElements('.neobsazeno').each(function(item){
							item.getElement('.pos_del').setHTML('');
						});
					} else {
						alert(json.message);
					}
				}
			}).send();
		} else {
			alert('Hej!! Co chces mazat!! Toto nesmi nastat!!');
		}
	}

	
	
	function vytvorit_zamestnance(e){
		//var event = new Event(e);
		//event.stop();
		obj=$('right_pozice').getElement('.pridanizam');
		tr = obj.getParent('tr'), table = tr.getParent('table'), id = tr.getElement('.client_id').value;
		obj.removeClass('pridanizam');
		
		new Request.JSON({
			url:obj.getProperty('href'),
			onComplete:function(json){
				if (json.result === true){
					obj.addClass('none');
					//obj.getParent('tr').getElement('.remove_as_employment').removeClass('none');
					obj.getParent('tr').getElement('.remove_from_curr_position').addClass('none');
					obj.getParent('tr').getElement('.td_status').setHTML('Zaměstnanec');
					alert('ok');
				} else {
					alert(json.message);
				}
			}
		}).send();
	}
	
	function propustit_zamestnance(e){
		//var event = new Event(e);
		//event.stop();
		obj=$('right_pozice').getElement('.delzam');
    tr = obj.getParent('tr'), table = tr.getParent('table'), id = tr.getElement('.client_id').value;
		obj.removeClass('delzam');
		new Request.JSON({
			url:obj.getProperty('href'),
			onComplete:function(json){
				if (json.result === true){
					obj.addClass('none');
					obj.getParent('tr').getElement('.remove_as_employment').addClass('none');
					obj.getParent('tr').getElement('.remove_from_curr_position').removeClass('none');
					obj.getParent('tr').getElement('.add_as_employment').removeClass('none');
					obj.getParent('tr').getElement('.td_status').setHTML('Umístěn');
					alert('ok');
				} else {
					alert(json.message);
				}
			}
		}).send();
	}
	
	
	//$$('.remove_as_employment').addEvent('click', propustit_zamestnance.bindWithEvent(this));
	//$$('.add_as_employment').addEvent('click', vytvorit_zamestnance.bindWithEvent(this));
	
	$$('.add_as_employment').addEvent('click', call_domwin_for_add_zamestnanec.bindWithEvent(this));
  
  function call_domwin_for_add_zamestnanec(e){
    var event = new Event(e);
    event.stop();
		var obj = event.target;
    var tr=obj.getParent('tr');
		
    obj.addClass('pridanizam');
		domwin.newWindow({
			id			: 'domwin_add_as_employment',
			sizes		: [580,375],
			scrollbars	: true,
			title		: 'Zaměstnání klienta ' + tr.getElement('.td_client').getHTML(),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: '/report_requirements_for_recruitments/add_zam_dotaznik/'+tr.getElement('.client_id').value+'/'+tr.getElement('.company_id').value+'/'+tr.getElement('.requirement_id').value+'/'+tr.getElement('.connection_id').value,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	};
	
	$$('.remove_as_employment').addEvent('click', call_domwin_for_del_zamestnanec.bindWithEvent(this));
  
  function call_domwin_for_del_zamestnanec(e){
    var event = new Event(e);
    event.stop();
		var obj = event.target;
    var tr=obj.getParent('tr');
		
    obj.addClass('delzam');
		domwin.newWindow({
			id			: 'domwin_del_as_employment',
			sizes		: [580,270],
			scrollbars	: true,
			title		: 'Propuštění zaměstnance ' + tr.getElement('.td_client').getHTML(),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: '/report_requirements_for_recruitments/del_zam_dotaznik/'+tr.getElement('.client_id').value+'/'+tr.getElement('.company_id').value+'/'+tr.getElement('.requirement_id').value+'/'+tr.getElement('.connection_id').value,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	};
  

	$('ListCompanyAttachmentClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_nabor_pozice');});
	
	remove_uses_client();
<?php } ?>
</script>