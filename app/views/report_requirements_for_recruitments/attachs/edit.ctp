<script type="text/javascript">
	function upload_file_complete(url){
		alert('Soubor byl nahrán.');
		$('RequirementsAttachmentFile').value = url;
	}
</script>
<form action='/report_requirements_for_recruitments/attachs_edit/' method='post' id='client_attachs_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Příloha</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('RequirementsAttachment/id');?>
			<?php echo $htmlExt->hidden('RequirementsAttachment/requirements_for_recruitment_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('RequirementsAttachment/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/report_requirements_for_recruitments/upload_attach/',
								'path_to_file'	=> 'uploaded/report_requirements_for_recruitments/attachment/'
							),
							'upload_type'	=> 'php',
							'onComplete'	=> 'upload_file_complete',
							'label'			=> 'Soubor'
						);
					?>
					<?php echo $fileInput->render('RequirementsAttachment/file',$upload_setting);?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('RequirementsAttachment/setting_attachment_type_id',$setting_attachment_type_list,null,array('tabindex'=>4,'label'=>'Typ přílohy'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditRequirementsAttachmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditRequirementsAttachmentClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	
	$('AddEditRequirementsAttachmentClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach_add');});
	
	$('AddEditRequirementsAttachmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_attachs_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('client_attachs_edit_formular').action,		
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_attach_add');
				}
			}).post($('client_attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('client_attachs_edit_formular',{
		'RequirementsAttachmentName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('client_attachs_edit_formular',<?php echo (isset($this->data['RequirementsAttachment']['id']))?'true':'false';?>);