<?php echo $htmlExt->selectTag('ClientWorkingHour/accommodation_id',$accommodation_list,null,array('label_class'=>'long2','class'=>'long2','tabindex'=>2,'label'=>'Ubytování'));?> <br class="clear">

<div id="ubytovani"></div>


<div class="slr_small" >  
	<fieldset>
		<legend>Kdy byl klient ubytován</legend>
		<p>
			Klient na této ubytovně bydlel: <strong id="pocet_dni">0</strong> dní<br />
			<span class="right">
				<a href="#" id="all_month_check" title="Vybrat celý měsíc">Vybrat celý měsíc</a> |
				<a href="#" id="all_month_uncheck" title="Odebrat celý měsíc">Odebrat celý měsíc</a>
			</span>
		</p>
		<br />
		
		<?php echo $this->renderElement('../report_requirements_for_recruitments/calendar/ubytovani',array($mesic,$rok));?>
	</fieldset>
</div>

<br class="clear" />
<script language="JavaScript" type="text/javascript">  
if ($('ClientWorkingHourAccommodationId').value != ''){
	new Request.HTML({
  		url: '/report_requirements_for_recruitments/ubytovani/' + $('ClientWorkingHourAccommodationId').value + '/' + <?php echo $rok ?> + '/' + <?php echo $mesic ?>,
  		update:'ubytovani'
	}).send();
}
</script>