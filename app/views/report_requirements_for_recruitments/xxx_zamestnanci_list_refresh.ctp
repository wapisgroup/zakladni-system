<table class='table' id='table_kontakty'>
<tr>
		<th>Jméno</th>
		<th>Datum nástupu</th>
		<th>Normo hod.</th>
		<th>Svátky</th>
		<th>Víkendy</th>
		<th>PN</th>
		<th>Dovelena</th>
		<th>Přesčasy</th>
		<th>Celkem</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($zamestnanci_list) && count($zamestnanci_list)>0):?>
	<?php foreach($zamestnanci_list as $zam):?>
	<tr>
		<td><?php echo $zam['Client']['name'];?></td>
		<td><?php echo $zam['ConnectionClientRequirement']['from'];?></td>
		<td><?php echo $detail['RequirementsForRecruitment']['standard_hours'];?></td>
		<td><?php echo $zam['ClientWorkingHour']['svatky'];?></td>
		<td><?php echo $zam['ClientWorkingHour']['vikendy'];?></td>
		<td><?php echo $zam['ClientWorkingHour']['pracovni_neschopnost'];?></td>
		<td><?php echo $zam['ClientWorkingHour']['dovolena'];?></td>
		<td><?php echo $zam['ClientWorkingHour']['prescasy'];?></td>
		<td><?php echo $zam['ClientWorkingHour']['celkem_hodin'];?></td>
		<td><a title="Odpracované hodiny klienta: <?php echo $zam['Client']['name'];?> za měsíc <?php echo $mesic ?> / <?php echo $rok ?>" href='/report_requirements_for_recruitments/odpracovane_hodiny/<?php echo $zam['ConnectionClientRequirement']['id'];?>/<?php echo $rok;?>/<?php echo $mesic;?>' class='ta edit'>Hodiny</a></td>
	</tr>
	<?php endforeach;?>
	<?php else: ?>
	<tr><td colspan='5'>Na tuto pozici není nadefinován zaměstnanec</td></tr>
<?php endif;?>	
<script>
	
	$('domwin_zamestnanci').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_zamestnanci_hodiny',
			sizes		: [945,660],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
</script>