<div class="sll_big" >  

	<fieldset>
		<legend>Popis ubytování</legend>
		<div class='sll'>
			<label>Název:</label><var><?php echo $detail_ubytovani["Accommodation"]["name"]; ?></var><br />
			<label>Město:</label><var><?php echo $detail_ubytovani["Accommodation"]["city"]; ?></var><br />
			<label>Kontaktní osoba:</label><var><?php echo $detail_ubytovani["Accommodation"]["contact_name"]; ?></var><br />
			<label>Telefon:</label><var><?php echo $detail_ubytovani["Accommodation"]["contact_phone"]; ?></var><br />
		</div>
		
		<div class='slr'>
			<label>Cena:</label><var><?php echo $detail_ubytovani["Accommodation"]["price"]; ?></var><br />
			<label>Max. počet míst:</label><var><?php echo $detail_ubytovani["Accommodation"]["max_count"]; ?></var><br />
			<label>Den platby:</label><var><?php echo $detail_ubytovani["Accommodation"]["day_of_payment"]; ?></var><br />
			<label>Účet:</label><var><?php echo $detail_ubytovani["Accommodation"]["account"]; ?></var><br />
		</div>
		
		<br />
		<br />
		<ul class='foto_list' id='foto_list'>
			<?php //print_r($detail_ubytovani['Accommodation']);
				if (!empty($detail_ubytovani['Accommodation']['imgs']) && count($detail_ubytovani['Accommodation']['imgs']) > 0){
					foreach($detail_ubytovani['Accommodation']['imgs'] as $img){
						list($file, $caption) = explode('|',$img);
						echo "<li class='obal_span'> 
		              <a href='/uploaded/accommodations/foto/large/$file' target='_blank' title='$caption' rel='clearbox[foto_box_detail]'>
		                <img src='/uploaded/accommodations/foto/small/$file' 
		                  title='$caption' alt='$caption' class='box_preview small'/>
		               </a>   
		            </li>";
					}
				
				}
			?>

		</ul>
	</fieldset>
</div>


<script language="JavaScript" type="text/javascript"> 

	$('all_month_check').addEvent('click',function(e){
		new Event(e).stop();	
		$('calendar_ubytovani').getElements('.cal_day').setProperty('checked','checked');
		$('calendar_ubytovani').getElements('.cal_day').addClass('checked');
		pocet_dni = $('calendar_ubytovani').getElements('.checked');
		$('pocet_dni').setHTML(pocet_dni.length);
	});
	
	$('all_month_uncheck').addEvent('click',function(e){
		new Event(e).stop();
		$('calendar_ubytovani').getElements('.cal_day').removeClass('checked');
		$('calendar_ubytovani').getElements('.cal_day').removeProperty('checked');
		pocet_dni = $('calendar_ubytovani').getElements('.checked');
		$('pocet_dni').setHTML(pocet_dni.length);
	});

	var ch = $('calendar_ubytovani').getElements('.cal_day');
		ch.addEvent('click',function(e){
		
			if (this.hasClass('checked')){
				this.removeClass('checked');
			} else {
				this.addClass('checked');
			}
			
			pocet_dni = $('calendar_ubytovani').getElements('.checked');
			$('pocet_dni').setHTML(pocet_dni.length);
	});
	 
document.slideshow.ajax_init('ubytovani');
</script>
