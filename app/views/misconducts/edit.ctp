<form action='/misconducts/edit/' method='post' id='setting_career_edit_formular'>
	<?php echo $htmlExt->hidden('Misconduct/id');?>
	
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní</a></li>
		</ul>
</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Nastavení</legend>
				<?php echo $htmlExt->input('Misconduct/name',array('tabindex'=>1,'label'=>'Název', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Misconduct/frequency',array('tabindex'=>2,'label'=>'Četnost', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Misconduct/go_to_rank_1',array('tabindex'=>3,'label'=>'Posund o hodnoceni 1', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
				<?php echo $htmlExt->input('Misconduct/go_to_rank_2',array('tabindex'=>4,'label'=>'Posund o hodnoceni 2', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
			    <?php echo $htmlExt->checkbox('Misconduct/ignore_freq',null,array('tabindex'=>5,'label'=>'Bez hodnoceni', 'class'=>'long','label_class'=>'long'));?> <br class="clear">
				 
            </fieldset>
		</div>
	</div>
	<div class="win_save">
		<?php echo $htmlExt->button('Uložit',array('id'=>'save_close','tabindex'=>3));?>
		<?php echo $htmlExt->button('Zavřít',array('id'=>'close','tabindex'=>4));?>
	</div>
</form>
 
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('save_close').addEvent('click',function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('setting_career_edit_formular');
		if (valid_result == true){
			new Request.JSON({
				url:$('setting_career_edit_formular').action,		
				onComplete:function(){
					click_refresh($('MisconductId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('setting_career_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});
	validation.define('setting_career_edit_formular',{
		'MisconductName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'}
		}
	});
	validation.generate('setting_career_edit_formular',<?php echo (isset($this->data['Misconduct']['id']))?'true':'false';?>);
</script>