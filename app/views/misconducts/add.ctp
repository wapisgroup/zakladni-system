<form action='/misconducts/add/' method='post' id='client_add_activity_form'>
			<?php echo $htmlExt->hidden('client_id',array('value'=>$client_id));?>
			<?php echo $htmlExt->hidden('hodnoceni',array('value'=>$hodnoceni));?>
            <?php echo $htmlExt->hidden('type',array('value'=>$type));?>
			<?php 
            if(isset($year) && isset($month)){
                echo $htmlExt->hidden('year',array('value'=>$year));
                echo $htmlExt->hidden('month',array('value'=>$month));
            }
            ?>
			<?php echo $htmlExt->hidden('action',array('value'=>'add_misconduct'));?>
			<fieldset>
				<legend>Problém</legend>
                        <?php echo $htmlExt->selectTag('problem',$problem_list, null, array('class'=>'long','label_class'=>'long','tabindex' => 2, 'label' => 'Problémy'), null, false); ?><br />
                        <div id="SmennaBox">
                        <?php echo $htmlExt->selectTag('smennost',array(1=>'Ranní',2=>'Odpolední',3=>'Noční'),null, array('class'=>'','label_class'=>'long','tabindex' => 3, 'label' => 'Směna'),null,false); ?><br />
                        </div>
                        <?php echo $htmlExt->inputDate('datum', array('class'=>'','label_class'=>'long','tabindex' => 3, 'label' => 'Datum')); ?><br />
                        <?php echo $htmlExt->textarea('text',array('tabindex'=>33,'label'=>'Zpráva','class'=>'long','label_class'=>'long'));?> <br class="clear">
            </fieldset>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditClientMessageSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditClientMessageClose'/>
	</div>
</form>
<script>
    var dochazka_model = "ClientWorkingHour";
    if($('Type').value == 1)//int zam
        dochazka_model = "ClientWorkingHoursInt";
    
	$('AddEditClientMessageClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_add_misconducts');});
    $('Problem').addEvent('click',function(e){
        if(this.value != -1)
            $('SmennaBox').addClass('none');
        else if($(dochazka_model+'SettingShiftWorkingId').value != 1)
            $('SmennaBox').removeClass('none');    
    })    
    
    //v pripade jednosmenneho nedavej na vyber
    if($(dochazka_model+'SettingShiftWorkingId').value == 1)
        $('SmennaBox').addClass('none'); 
    
    function valid_date(){
        var year = $('Year').value, month = $('Month').value;
        
        var sd = $('calbut_Datum').value.split('-');

        if((sd[0]+'-'+sd[1]) == (year+'-'+month))
            return true;
        else
            return false;    
    }
    

	$('AddEditClientMessageSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('client_add_activity_form');
        
        
        if($('Problem').value == -1 && $('calbut_Datum').value != '' && valid_date() === false){ 
			if(valid_result == true)
				valid_result = Array("Absenci mužete zadávat pouze v aktuáním měsíci v kterém editujete docházku!");
			else
				valid_result[valid_result.length]="Absenci mužete zadávat pouze v aktuáním měsíci v kterém editujete docházku!";
		}
        
		if (valid_result == true){                
    			var request = new Request.JSON({
    				url:$('client_add_activity_form').action,
    				onComplete:function(){
    				    alert('Problem byl ulozen');
    					domwin.closeWindow('domwin_add_misconducts');
                        
                        if(admin_save)
                            $('only_save').fireEvent('click',e);
                        
                        button_preloader_disable($('AddEditClientMessageSaveAndClose'));    
    				}
    			});
                
                if($('Problem').value == -1){
                    if(confirm('Docházka bude přeuložena, chcete pokračovat?')){
                        var sd = $('calbut_Datum').value.split('-');
                        
                        var day = (sd[2].substr(0,1) == '0' ? sd[2].substr(1): sd[2]);
                        var input_box = dochazka_model+'Days'+$('Smennost').value+day;
                        
                        if($(input_box).value == 'A'){
                            alert('V tomto dni již existuje Absence!!!');
                        }
                        else{
                            val = $(dochazka_model+'NeomluvenaAbsence').value;
                            if(val == '')
                                val = 0;
                            
                            $(input_box).value = 'A';
                            $(dochazka_model+'NeomluvenaAbsence').value =  parseInt(val) + 1; 
                            
                            admin_save = true;
                            button_preloader($('AddEditClientMessageSaveAndClose'));  
    				        request.post($('client_add_activity_form'));
                        } 
                    }
                }
                else{
                    button_preloader($('AddEditClientMessageSaveAndClose'));
    				request.post($('client_add_activity_form'));
                }   
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});

	validation.define('client_add_activity_form',{
        'calbut_Datum': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit datum'},
		}
	});

	validation.generate('client_add_activity_form',false);
</script>