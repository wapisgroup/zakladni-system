<form action='/history_sets/edit/' method='post' id='historyset_edit_formular'>
	<?php echo $htmlExt->hidden('HistorySet/id'); ?>
		
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<fieldset>
				<legend>Základni informace</legend>
				<div class="sll">  
					<?php echo $htmlExt->selectTag('HistorySet/category_id', $history_category, null,array('tabindex' => 1, 'label' =>'Kategorie'), null, false); ?><br />
					<?php echo $htmlExt->input('HistorySet/controller',  array('tabindex' => 3,'label' => 'Controller',null,false)); ?><br />
					<?php echo $htmlExt->input('HistorySet/empty_id', array('tabindex' =>5, 'label' => 'Empty id')); ?><br />
			</div>
				<div class="slr">  
					<?php echo $htmlExt->selectTag('HistorySet/action_id', $history_type_list, null,array('tabindex' => 2, 'label' => 'Akce'), null, false); ?><br />
					<?php echo $htmlExt->input('HistorySet/action', array('tabindex' =>4, 'label' => 'action')); ?><br />
					<?php echo $htmlExt->input('HistorySet/primary_id', array('tabindex' =>6, 'label' => 'PrimaryKey')); ?><br />
				</div>
				<div>
					<?php echo $htmlExt->textarea('HistorySet/colums', array('tabindex' =>7, 'label' => 'Sloupce','class'=>'long','label_class'=>'long')); ?><br />
					<?php echo $htmlExt->textarea('HistorySet/conditions', array('tabindex' =>8, 'label' => 'Conditions','class'=>'long','label_class'=>'long')); ?><br />
				</div>
			</fieldset>
			<fieldset>
				<legend>Data</legend>
				<table class='table' id='data_table'>
					<tr>
						<th>Source</th>
						<th>Path</th>
						<th>Caption</th>
						<th>Type</th>
						<th>Fnc</th>
						<th>Model / List</th>
						<th>Sloupce</th>
						<th><a href='#' class='ta add' id='add_row'>Add</a></th>
					</tr>
				</table>
			</fieldset>
			<div class="win_save">
				<?php echo $htmlExt->button('Generovat XML', array('id' => 'generate_xml', 'class' => 'button')); ?>
			</div>
		</div>
		<div class="win_save">
			<?php echo $htmlExt->button('Uložit', array('id' => 'save_close', 'class' => 'button')); ?>
			<?php echo $htmlExt->button('Zavřít', array('id' => 'close', 'class' =>'button')); ?>
		</div>
	</div>
</form>
 <script language="javascript" type="text/javascript">
	var domtab = new DomTabs({'className':'admin_dom'});

	
	/*
	* Natahnuti dat z textove oblasti
	*/
	 parser=new DOMParser();
	 xmlDoc=parser.parseFromString($('HistorySetColums').value,"text/xml");

	var itms = xmlDoc.getElements('item');
	if (itms){
		itms.each(function(itm){
			add_row();

			var tr_to_change = $('data_table').getElement('tr:last-child');

			tr_to_change.getElement('.source').value = itm.getElement('source').textContent;
			tr_to_change.getElement('.path').value = itm.getElement('path').textContent;
			tr_to_change.getElement('.caption').value = itm.getElement('caption').textContent;
			tr_to_change.getElement('.model').value = itm.getElement('model').textContent;
			tr_to_change.getElement('.func').value = itm.getElement('func').textContent;
			tr_to_change.getElement('.type').value = itm.getElement('name').textContent;

			itm.getElement('cols').getElements('col').each(function(item){
				add_col('11111',tr_to_change.getElement('.cols_td'),item.textContent);
			});
			
			


		});
	}


	
	// generate_xml
	$('generate_xml').addEvent('click',function(e){
		e.stop();
		var xml = "<conf>\n";
		
		var trs = $('data_table').getElements('tr');
		if (trs.length > 1){
			for(i=1; i<trs.length;i++){
				xml += "\t<item>\n";
				xml += "\t\t<source>" + trs[i].getElement('.source').value + "</source>\n"; 
				xml += "\t\t<path>" + trs[i].getElement('.path').value + "</path>\n"; 
				xml += "\t\t<caption>" + trs[i].getElement('.caption').value + "</caption>\n";
				xml += "\t\t<type>\n";
				xml += "\t\t\t<name>" + trs[i].getElement('.type').value + "</name>\n";
				xml += "\t\t\t<model>" + trs[i].getElement('.model').value + "</model>\n";
				xml += "\t\t\t<cols>\n";
				trs[i].getElement('.cols_td').getElements('input').each(function(item){
					xml += "\t\t\t\t<col>" + item.value + "</col>\n";
				});
				xml += "\t\t\t</cols>\n";
				xml += "\t\t</type>\n";
				xml += "\t\t<func>" + trs[i].getElement('.func').value + "</func>\n"; 
				xml += "\t</item>\n";	 
			}	
		}
		xml += "</conf>\n" ;

		$('HistorySetColums').value = xml;

		
	});

	// pridani radku do data
	$('add_row').addEvent('click', add_row.bind());
	
	function add_row(){
		var table = $('data_table').getElement('tbody'), new_id = uniqid(), source_select = {data:'this->data',arg:'Arguments',get:'$_GET'}, type_select = {text:'Textove',date:'Datum',list:'List z configu',db_list:'Loadovani z DB'}; 

		// novy radek
		var tr = new Element('tr').inject(table);

		/*
		* sloupec se zdrojem, vygenerovat select
		*/
		generate_select(new Element('td').inject(tr),{class:'source',name:'data[HistorySet][cols][' + new_id + '][source]'},source_select);

		/*
		* sloupec s xPath
		*/
		new Element('input',{class:'path',name:'data[HistorySet][cols][' + new_id + '][path]'}).inject(new Element('td').inject(tr));

		/*
		* sloupec s caption
		*/
		new Element('input',{class:'caption',name:'data[HistorySet][cols][' + new_id + '][caption]'}).inject(new Element('td').inject(tr));

		/*
		* sloupec s typem, vygenerovat select
		*/
		generate_select(new Element('td').inject(tr),{class:'type',name:'data[HistorySet][cols][' + new_id + '][type]'},type_select);

		/*
		* sloupec s my func
		*/
		new Element('input',{class:'func',name:'data[HistorySet][cols][' + new_id + '][func]'}).inject(new Element('td').inject(tr));

		/*
		* sloupec s model / list
		*/
		new Element('input',{class:'model',name:'data[HistorySet][cols][' + new_id + '][model]'}).inject(new Element('td').inject(tr));

		/*
		* sloupec se cols, dopsat potom
		*/

		var temp_td = new Element('td',{html:'',class:'cols_td'}).inject(tr);
		new Element('a',{href:'#', class:'ta add'}).inject(temp_td).addEvent('click', function(e){e.stop();add_col(new_id, temp_td)});
		// add <br />
		new Element('br').inject(temp_td);

		/*
		* sloupec se possibility
		*/
		new Element('a',{html:'smazat',class:'ta delete',href:'#'}).addEvent('click',function(e){e.stop(); this.getParent('tr').dispose();}).inject(new Element('td').inject(tr));
		
		
			
	};

	function add_col(new_id,temp_td,value){
		new Element('input',{name:'data[HistorySet][cols][' + new_id + '][fields][]',value:value}).inject(temp_td).setStyles({float:'none',width:90});
		new Element('a',{html:'smazat',class:'ta delete',href:'#'})
			.addEvent('click',function(e){
				e.stop(); 
				this.getPrevious('input').dispose();
				this.getNext('br').dispose();
				this.dispose();
			})
			.inject(temp_td);
		new Element('br').inject(temp_td);
	} 

	function generate_select(parent, att, options){
		if  (parent){
			var select = new Element('select',att).inject(parent);
			$each(options, function(caption, value){
				new Element('option',{value:value, html:caption, title:caption}).inject(select);
			});
		} else {
			console.log('neexistuje parent')
		}
	}
	

	 
	 //$('historyset_edit_formular').getElements('.float, .integer').inputLimit();

	/*
	*  ulozeni zaznamu
	*/ 
	if($('save_close'))
		$('save_close').addEvent('click',function(e){
			new Event(e).stop();
			valid_result = validation.valideForm('historyset_edit_formular');
			if (valid_result == true){	
				new Request.JSON({ 
					url:$('historyset_edit_formular').action,		 
					onComplete:function(json){
						if (json){
							if (json.result === true){
								click_refresh($('HistorySetId').value); 
								domwin.closeWindow('domwin');
							} else {
								alert(json.message);
							}
						} else {
							alert('Chyba aplikace')
						}
					}
				}).send($('historyset_edit_formular'));
			} else {
				var error_message = new MyAlert();
				error_message.show(valid_result)
			}
		});

	/*
	* Zavreni okna bez ulozeni
	*/
	$('close').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin');});

	/*
	* Vastaveni validace
	*/
	validation.define('historyset_edit_formular',{
		'ClientJmeno': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit jméno'},
		},
		'ClientMobil1': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit mobil'},
			'length': {'condition':{'min':14,'max':14}, 'err_message':'Mobil musí mít 14 znaků'},		
		},
		'ClientPrijmeni': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit příjmení'},
		}
	});
	validation.generate('historyset_edit_formular',<?php echo (isset($this->data['Client']['id'])) ?'true' : 'false'; ?>);
</script>