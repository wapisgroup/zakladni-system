<form id='add_edit_company_formular' action='/internal_regulations/edit/' method='post'>
	<?php echo $htmlExt->hidden('InternalRegulation/id');?>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Základní informace</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<div class='sll'>	
				<?php echo $htmlExt->selectTag('InternalRegulation/internal_regulation_group_id',$group_list,null,array('label'=>'Kategorie'),null,false);?><br />
			</div>
            <br />
				
			<?php echo $htmlExt->textarea('InternalRegulation/question',	array('class'=>'long','label'=>'Otázka:','label_class'=>'long'));?><br />
			<?php echo $htmlExt->textarea('InternalRegulation/answer',	array('class'=>'long','label'=>'Odpověď','label_class'=>'long'));?><br />
			<?php echo $htmlExt->input('InternalRegulation/contact',array('class'=>'long','label'=>'Kontaktní osoba','label_class'=>'long'));?><br />
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditInternalRegulationSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditInternalRegulationClose'/>
	</div>
</form>
<script>
	
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditInternalRegulationClose').addEvent('click',function(e){
		new Event(e).stop();
		domwin.closeWindow('domwin');
	});
	
	$('AddEditInternalRegulationSaveAndClose').addEvent('click', function(e){
		new Event(e).stop();
		
		valid_result = validation.valideForm('add_edit_company_formular');
		if (valid_result == true){
			button_preloader($('AddEditInternalRegulationSaveAndClose'));
			new Request.JSON({
				url:$('add_edit_company_formular').action,		
				onComplete:function(){
					
					click_refresh($('InternalRegulationId').value);
					domwin.closeWindow('domwin');

				}
			}).post($('add_edit_company_formular'));
            button_preloader_disable($('AddEditInternalRegulationSaveAndClose'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('add_edit_company_formular',{
		'InternalRegulationQuestion': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit otázku'}
		},
        'InternalRegulationAnswer': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit odpověď'}
		}
	});
	validation.generate('add_edit_company_formular',<?php echo (isset($this->data['InternalRegulation']['id']))?'true':'false';?>);
	
</script>
