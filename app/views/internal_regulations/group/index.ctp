<div class="win_fce top">
<a href='/internal_regulations/group_edit/' class='button edit' id='company_order_add_new' title='Přidání novou kategorii'>Přidat kategorii</a>
</div>
<table class='table' id='table_orders'>
	<tr>
		<th>#ID</th>
		<th>Název</th>
		<th>Možnosti</th>
	</tr>
	<?php if (isset($group_list) && count($group_list) >0):?>
    	<?php foreach($group_list as $item):?>
    	<tr>
    		<td><?php echo $item['InternalRegulationGroup']['id'];?></td>
    		<td><?php echo $item['InternalRegulationGroup']['name'];?></td>
    		<td>
    			<a title='Editace skupiny "<?php echo $item['InternalRegulationGroup']['name'];?>"' 	class='ta edit' href='/internal_regulations/group_edit/<?php echo $item['InternalRegulationGroup']['id'];?>'/>edit</a>
                <?php
                if($logged_user['CmsGroup']['id'] == 1){
                ?>
                	<a title='Odstranit kategorii 'class='ta trash' href='/internal_regulations/group_trash/<?php echo $item['InternalRegulationGroup']['id'];?>'/>trash</a>
        		<?php }?>
            
            </td>
    	</tr>
    	<?php endforeach;?>
	<?php else:?>
	<tr>
		<td colspan='5'>Nebyla nadefinována žádna skupina</td>
	</tr>
	<?php endif;?>
</table>
	<div class='formular_action'>
		<input type='button' value='Zavřít' id='ListCompanyTemplateClose' class='button'/>
	</div>
<script>
	$('ListCompanyTemplateClose').addEvent('click', function(e){new Event(e).stop(); domwin.closeWindow('domwin_groups');});

	$('table_orders').getElements('.trash').addEvent('click',function(e){
		new Event(e).stop();
		if (confirm('Opravdu si přejet smazat tuto kategorii?')){
			new Request.HTML({
				url:this.href,
				onComplete: function(){
				    domwin.loadContent('domwin_groups');
				}
			}).send();
		}
	});
	
	$('domwin_groups').getElements('.edit').addEvent('click',function(e){
		new Event(e).stop();
		domwin.newWindow({
			id			: 'domwin_edit',
			sizes		: [450,250],
			scrollbars	: true,
			title		: this.getProperty('title'),
			languages	: false,
			type		: 'AJAX',
			ajax_url	: this.href,
			closeConfirm: true,
			max_minBtn	: false,
			modal_close	: false,
			remove_scroll: false
		}); 
	});
    
    
</script>