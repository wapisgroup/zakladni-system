<script type="text/javascript">
	function upload_file_complete(url){
		alert('Soubor byl nahrán.');
		$('InternalRegulationAttachmentFile').value = url;
	}
</script>
<form action='/internal_regulations/attachs_edit/' method='post' id='attachs_edit_formular'>
	<div class="domtabs admin_dom_links">
		<ul class="zalozky">
			<li class="ousko"><a href="#krok1">Příloha</a></li>
		</ul>
	</div>
	<div class="domtabs admin_dom">
		<div class="domtabs field">
			<?php echo $htmlExt->hidden('InternalRegulationAttachment/id');?>
			<?php echo $htmlExt->hidden('InternalRegulationAttachment/internal_regulation_id');?>
			<fieldset>
				<legend>Prosím vyplňte následující údaje</legend>
				<?php echo $htmlExt->input('InternalRegulationAttachment/name',array('tabindex'=>1,'label'=>'Název','class'=>'long','label_class'=>'long'));?> <br class="clear">
				<div class='sll'>
					<?php 
						$upload_setting = array(
							'paths' => array(
								'upload_script' => '/internal_regulations/upload_attach/',
								'path_to_file'	=> 'uploaded/internal_regulations/attachment/'
							),
							'upload_type'	=> 'php',
							'onComplete'	=> 'upload_file_complete',
							'label'			=> 'Soubor'
						);
					?>
					<?php echo $fileInput->render('InternalRegulationAttachment/file',$upload_setting);?> <br class="clear">
				</div>
				<div class='slr'>
					<?php echo $htmlExt->selectTag('InternalRegulationAttachment/setting_attachment_type_id',$setting_attachment_type_list,null,array('tabindex'=>4,'label'=>'Typ přílohy'));?> <br class="clear">
				</div>
			</fieldset>
		</div>
	</div>
	<div class='formular_action'>
		<input type='button' value='Uložit' id='AddEditInternalRegulationAttachmentSaveAndClose' />
		<input type='button' value='Zavřít' id='AddEditInternalRegulationAttachmentClose'/>
	</div>
</form>
<script>
	var domtab = new DomTabs({'className':'admin_dom'}); 
	$('AddEditInternalRegulationAttachmentClose').addEvent('click',function(e){new Event(e).stop(); domwin.closeWindow('domwin_attach_add');});
	
	$('AddEditInternalRegulationAttachmentSaveAndClose').addEvent('click',function(e){
		new Event(e).stop();
		valid_result = validation.valideForm('attachs_edit_formular');
		if (valid_result == true){
			new Request.HTML({
				url:$('attachs_edit_formular').action,		
				update: $('domwin_attach').getElement('.CB_ImgContainer'),
				onComplete:function(){
					domwin.closeWindow('domwin_attach_add');
				}
			}).post($('attachs_edit_formular'));
		} else {
			var error_message = new MyAlert();
			error_message.show(valid_result)
		}
	});
	
	validation.define('attachs_edit_formular',{
		'InternalRegulationAttachmentName': {
			'testReq': {'condition':'not_empty','err_message':'Musíte vyplnit název'},
		}
	});
	validation.generate('attachs_edit_formular',<?php echo (isset($this->data['InternalRegulationAttachment']['id']))?'true':'false';?>);
</script>    