<?php
class ClientView extends AppModel {
    var $name = 'ClientView';
    private $ignore_en_condition = false;
	//var $useTable = 'clients';

	/* - MAIN
	DROP VIEW if exists `fastest__client_views`;
	CREATE VIEW fastest__client_views AS  
		SELECT (
        CASE Client.mobil_active
         When 'mobil1' THEN Client.mobil1
         When 'mobil2' THEN Client.mobil2
         When 'mobil3' THEN Client.mobil3
        END
        ) as mobil,
        
        (
             IF(cacwp.id <> 0,3,
                    IF(ConnectionClientRequirement.id <> 0,(
                        CASE ConnectionClientRequirement.type
                         When 2 THEN 2
                         When 1 THEN 1
                        END),0)
             )
            
         ) as  stav,
         (
            IF(Client.datum_narozeni <> '',TIMESTAMPDIFF(YEAR,Client.datum_narozeni,NOW()),'-')
         ) as vek,
        Client.mobil_active,Client.id, Client.import_stav, Company.client_manager_id as client_manager_id, 
        			Company.coordinator_id as coordinator_id, Company.coordinator_id2 as coordinator_id2, 
        			Company.self_manager_id as self_manager_id, 
        			Client.client_type as client_type,
        			Client.name, Company.name as company, ClientManager.name as client_manager,
        			Coordinator.name as coordinator,Coordinator2.name as coordinator2, 
		        	Client.created, Client.updated, Client.kos, Client.express, Client.import_adresa,
                    GROUP_CONCAT(DISTINCT SettingCareerItem.name SEPARATOR '<br/>') as Profese,
                    Client.mobil1,Client.mobil2,Client.mobil3,
                     uk.stav as chybna_dokumentace,ConnectionClientRequirement.company_money_item_id,
                     Client.cislo_uctu,Client.cislo_uctu_control,Client.mesto,Client.datum_narozeni,
                     Client.countries_id,Client.externi_nabor,Client.email,Client.private_email,
                     Client.os_cislo,Client.zbyva_dovolene,Client.parent_id,Client.pohlavi_list,
                     Client.jmeno,Client.prijmeni, Countrie.name as okres,Client.os_pohovor,
                     Client.os_pohovor_poznamka,Client.rodne_cislo,Client.info_about_job,
                     Client.ulice,Client.psc,Client.stat_id
        	FROM `fastest__clients` AS Client 
        		LEFT JOIN fastest__connection_client_career_items AS ConnectionClientCareerItem ON (ConnectionClientCareerItem.client_id = Client.id) 
        		LEFT JOIN fastest__setting_career_items AS SettingCareerItem ON (SettingCareerItem.id = ConnectionClientCareerItem.setting_career_item_id) 
        		LEFT JOIN `fastest__connection_client_requirements` as ConnectionClientRequirement ON (ConnectionClientRequirement.client_id = Client.id AND (ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1) AND ConnectionClientRequirement.to='0000-00-00')  
        		LEFT JOIN  fastest__companies as Company ON (ConnectionClientRequirement.company_id = Company.id) 
        		LEFT JOIN fastest__cms_users as ClientManager ON (Company.client_manager_id = ClientManager.id) 
        		LEFT JOIN fastest__cms_users as Coordinator ON (Company.coordinator_id = Coordinator .id) 
        		LEFT JOIN fastest__cms_users as Coordinator2 ON (Company.coordinator_id2 = Coordinator2 .id) 
        		LEFT JOIN fastest__connection_client_at_company_work_positions as cacwp ON (cacwp.client_id = Client.id AND cacwp.datum_to='0000-00-00') 
                LEFT JOIN fastest__company_money_items as cmi ON (cmi.id = ConnectionClientRequirement.company_money_item_id ) 
        		LEFT JOIN fastest__client_ucetni_dokumentace as uk ON (uk.connection_client_requirement_id = ConnectionClientRequirement.id AND uk.forma = cmi.name ) 
                LEFT JOIN fastest__countries as Countrie ON (Countrie.id = Client.countries_id) 

	GROUP BY Client.id
	*/

	/* - ALTERNATIVE - OLD
	CREATE VIEW fastest__client_views AS  
	SELECT Client.id, Client.import_stav, Company.client_manager_id as client_manager_id,  
			Client.name, Company.name as company, ClientManager.name as client_manager, 
			Coordinator.name as coordinator, Client.created, Client.updated, Client.telefon1, 
			Client.stav, Client.kos, SettingCareerItem.name as profese_name,SettingCareerItem.id as profese_id
	FROM `fastest__clients` AS Client 
		LEFT JOIN fastest__connection_client_career_items AS ConnectionClientCareerItem ON (ConnectionClientCareerItem.client_id = Client.id) 
		LEFT JOIN fastest__setting_career_items AS SettingCareerItem ON (SettingCareerItem.id = ConnectionClientCareerItem.setting_career_item_id) 
		LEFT JOIN `fastest__connection_client_requirements` as ConnectionClientRequirement ON (ConnectionClientRequirement.client_id = Client.id AND ConnectionClientRequirement.type = 2)  
		LEFT JOIN  fastest__companies as Company ON (ConnectionClientRequirement.company_id = Company.id) 
		LEFT JOIN fastest__cms_users as ClientManager ON (Company.client_manager_id = ClientManager.id) 
		LEFT JOIN fastest__cms_users as Coordinator ON (Company.coordinator_id = Coordinator .id) 
		
	*/
    
    /*  New FULL with Client - only for export
    
    	DROP VIEW if exists `fastest__client_views`;
	CREATE VIEW fastest__client_views AS  
		SELECT (
        CASE Client.mobil_active
         When 'mobil1' THEN Client.mobil1
         When 'mobil2' THEN Client.mobil2
         When 'mobil3' THEN Client.mobil3
        END
        ) as mobil, Company.client_manager_id as client_manager_id, 
        			Company.coordinator_id as coordinator_id, Company.coordinator_id2 as coordinator_id2, 
        			Company.self_manager_id as self_manager_id, 
        		    Company.name as company, ClientManager.name as client_manager, 
        			Coordinator.name as coordinator,Coordinator2.name as coordinator2, 
                    GROUP_CONCAT(DISTINCT SettingCareerItem.name SEPARATOR '<br/>') as Profese,
                    GROUP_CONCAT(DISTINCT SettingCertificate.name SEPARATOR '<br/>') as Certifikaty,
                    GROUP_CONCAT(DISTINCT Recruiter.name SEPARATOR '<br/>') as Recruiters,
                     uk.stav as chybna_dokumentace,ConnectionClientRequirement.company_money_item_id,
                    Client.*
        	FROM `fastest__clients` AS Client 
        		LEFT JOIN fastest__connection_client_career_items AS ConnectionClientCareerItem ON (ConnectionClientCareerItem.client_id = Client.id) 
        		LEFT JOIN fastest__connection_client_certifikaty_items AS ConnectionClientCertifikatyItem ON (ConnectionClientCertifikatyItem.client_id = Client.id) 
        		LEFT JOIN fastest__connection_client_recruiters AS ConnectionClientRecruiter ON (ConnectionClientRecruiter.client_id = Client.id) 
        		LEFT JOIN fastest__setting_career_items AS SettingCareerItem ON (SettingCareerItem.id = ConnectionClientCareerItem.setting_career_item_id) 
        		LEFT JOIN fastest__setting_certificates AS SettingCertificate ON (SettingCertificate.id = ConnectionClientCertifikatyItem.setting_certificate_id) 
        		LEFT JOIN `fastest__connection_client_requirements` as ConnectionClientRequirement ON (ConnectionClientRequirement.client_id = Client.id AND (ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1) AND ConnectionClientRequirement.to='0000-00-00')  
        		LEFT JOIN  fastest__companies as Company ON (ConnectionClientRequirement.company_id = Company.id) 
        		LEFT JOIN fastest__cms_users as ClientManager ON (Company.client_manager_id = ClientManager.id) 
        		LEFT JOIN fastest__cms_users as Coordinator ON (Company.coordinator_id = Coordinator .id) 
        		LEFT JOIN fastest__cms_users as Coordinator2 ON (Company.coordinator_id2 = Coordinator2 .id) 
        		LEFT JOIN fastest__cms_users as Recruiter ON (Recruiter.id = ConnectionClientRecruiter.cms_user_id) 
        		LEFT JOIN fastest__company_money_items as cmi ON (cmi.id = ConnectionClientRequirement.company_money_item_id ) 
        		LEFT JOIN fastest__client_ucetni_dokumentace as uk ON (uk.connection_client_requirement_id = ConnectionClientRequirement.id AND uk.forma = cmi.name ) 
	GROUP BY Client.id
    
    */
    
    /**
     * funkce obsluhujici status a jeho nastavnei pro 
     * potvrzeni ci vyvraceni podminky pro EN nabor
     */
    function get_ignore_status(){
        return $this->ignore_en_condition;
    }
    
    function set_ignore_status($status){
        $this->ignore_en_condition = $status;
    }
    
    
    function beforeFind($queryData){
		$queryData = parent::beforeFind($queryData);
        /**
         * pokud jsou nastaveny conditions pridej k nim podminky pro ATEP
         * videt pouze od interniho naboru
         * tedy externi_nabor = 0
         */
        if(isset($queryData['conditions']) && !$this->ignore_en_condition){
            $my_con = array(
                'externi_nabor' => 0
            );
    
            $queryData['conditions'] = am($queryData['conditions'],$my_con);
        }
        
        //pr($queryData);
        
		return $queryData;
    }
}
?>