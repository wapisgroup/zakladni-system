<?php
class BonusCooCmView extends AppModel {
    var $name = 'BonusCooCmView';
	var $useTable = 'bonus_coo_cm_views';

	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($item[0]))
					$data[$key][$this->name] = am(@$data[$key][$this->name],$item[0]);
		
			}
		}
		return $data;
    }
	
}


/*
 *
 * create VIEW fastest__bonus_coo_cm_views as
SELECT 
	'50' as procenta,
(
prize /250
) AS na_hodinu,
((
(
(
prize /250
) * vyplatit
) /100
) * 50) as odmena,
	MLM1.client_name,
	MLM1.recruiter_id as umistovatel_id,
 MLM1.recruiter_cms_group_id as umistovatel__cms_group_id,
MLM1.recruiter_name as recruiter_name,
	MLM1.parent_id,
	year,
	month,
	spec_id,
	company_name,
	profese_id,
	celkem_hodin,
	(zbyva-vyplatit) as zbyva,
	vyplatit,
	prize,
	stav,
	pozice_name,
'0' as kos,
'0' as nd
FROM 
	`fastest__bonus_recruiter_views` as MLM1
Where MLM1.mlm_group_id IS NULL AND MLM1.recruiter_cms_group_id IN (3,4)
HAVING na_hodinu IS NOT NULL 



*/




?>