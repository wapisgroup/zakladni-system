<?php
class CmsUser extends AppModel {
    var $name = 'CmsUser';
	var $useTable = 'cms_users';
    var $useDbConfig = 'default';
	
	function beforeSave(){
		if (isset($this->data[$this->name]['other']))
			$this->data[$this->name]['other'] = serialize($this->data[$this->name]['other']);
		
		if (isset($this->data[$this->name]['mlm_setting']))
			$this->data[$this->name]['mlm_setting'] = serialize($this->data[$this->name]['mlm_setting']);
			
	
		
		if (isset($this->data[$this->name]['jmeno']) && isset($this->data[$this->name]['prijmeni']))
			$this->data[$this->name]['name'] = $this->data[$this->name]['prijmeni'] . ' ' . $this->data[$this->name]['jmeno'];
		
		if (isset($this->data[$this->name]['heslo']) && !empty($this->data[$this->name]['heslo'])){
			$this->data[$this->name]['heslo'] = md5($this->data[$this->name]['heslo']);
		} if (isset($this->data[$this->name]['heslo']) && empty($this->data[$this->name]['heslo'])){
			unset($this->data[$this->name]['heslo']);
		}
		
		return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($item[$this->name]['other']))
					$data[$key][$this->name]['other'] = unserialize($item[$this->name]['other']);
				if (isset($item[$this->name]['mlm_setting']))
					$data[$key][$this->name]['mlm_setting'] = unserialize($item[$this->name]['mlm_setting']);

			}
		}
		return $data;
    }
    
    
    /**
     * pripojedni na W db protoze konzultanti jsou definovani tam
     */
    function konzultant_list(){
        //$this->useDbConfig = 'atdy';
        return $this->find('list',array(
            'conditions'=>array(
                'kos'=>0,
                'status'=>1,
                'cms_group_id'=>15
            ),
            'order'=>'name ASC'
        ));
    }
 
}
?>