<?php
class FormData extends AppModel {
    var $name = 'FormData';

	function beforeSave(){	
		if (isset($this->data[$this->name]['values'])){ 				
			$this->data[$this->name]['values'] = serialize($this->data[$this->name]['values']); 
		}
		return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($data[$key][$this->name]['values'])){ 				
					$data[$key][$this->name]['values'] = unserialize($data[$key][$this->name]['values']); 
				}
			}
		}
		return $data;
    }
	
}
?>