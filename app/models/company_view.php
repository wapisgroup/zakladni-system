<?php
class CompanyView extends AppModel {
    var $name = 'CompanyView';
	
	/*
	DROP VIEW if exists `fastest__company_views`;
	CREATE VIEW fastest__company_views as 
	SELECT `Company`.`id` as id, Company.kos,  Company.ico,  
		Company.self_manager_id,Company.setting_stav_id, Company.client_manager_id, 
		Company.coordinator_id,Company.manazer_realizace_id,Company.coordinator_id2, `Company`.`name` as name, 
		`Company`.`updated` as updated, `Company`.`created` as created, 
		`Company`.`status` as status, `SalesManager`.`name` as sales_manager, 
		`ClientManager`.`name` as client_manager, `Coordinator`.`name` as coordinator, `Coordinator2`.`name` as coordinator2, 
		`Company`.`mesto` as mesto, `SettingStav`.`name` as stav ,
		`CompanyManager`.`name` as company_manager, Company.internal_employee,
        Company.parent_id,Company.asistent_obchodu_id,Company.konzultant_dy_id,
        Company.dy_prilezitost,SettingParentCompany.name as nadrazena_spol,
        Company.stat_id ,Company.vip      
	FROM `fastest__companies` AS `Company` 
    LEFT JOIN `fastest__setting_parent_companies` AS `SettingParentCompany` ON (`Company`.`parent_id` = `SettingParentCompany`.`id`) 
	LEFT JOIN `fastest__cms_users` AS `SalesManager` ON (`Company`.`self_manager_id` = `SalesManager`.`id`) 
	LEFT JOIN `fastest__cms_users` AS `ClientManager` ON (`Company`.`client_manager_id` = `ClientManager`.`id`) 
	LEFT JOIN `fastest__cms_users` AS `Coordinator` ON (`Company`.`coordinator_id` = `Coordinator`.`id`) 
	LEFT JOIN `fastest__cms_users` AS `Coordinator2` ON (`Company`.`coordinator_id2` = `Coordinator2`.`id`) 
	LEFT JOIN `fastest__cms_users` AS `CompanyManager` ON (`Company`.`manazer_realizace_id` = `CompanyManager`.`id`) 
	LEFT JOIN `fastest__setting_stavs` AS `SettingStav` ON (`Company`.`setting_stav_id` = `SettingStav`.`id`) 
	LEFT JOIN fastest__connection_client_requirements as ConnectionClientRequirement ON (ConnectionClientRequirement.company_id = Company.id AND ConnectionClientRequirement.type = 2) 
	GROUP BY Company.id
	*/
}
?>