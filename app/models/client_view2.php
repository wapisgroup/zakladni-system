<?php
class ClientView2 extends AppModel {
    var $name = 'ClientView2';
    private $ignore_en_condition = false;
	var $useTable = 'clients';
    
    var $joinSpec = array(
        'ConnectionClientCareerItem'        => array('primaryKey' => 'ConnectionClientCareerItem.client_id','foreignKey' => 'ClientView2.id'),
        'ConnectionClientCertifikatyItem'   => array('primaryKey' => 'ConnectionClientCertifikatyItem.client_id','foreignKey' => 'ClientView2.id'),
        'SettingCareerItem'                 => array('primaryKey' => 'SettingCareerItem.id','foreignKey' => 'ConnectionClientCareerItem.setting_career_item_id'),
        'SettingCertificate'                => array('primaryKey' => 'SettingCertificate.id','foreignKey' => 'ConnectionClientCertifikatyItem.setting_certificate_id'),
        'ConnectionClientRequirement'       => array('primaryKey' => 'ConnectionClientRequirement.client_id','foreignKey' => 'ClientView2.id','conditions'=>array('ConnectionClientRequirement.type' => array(1,2),'ConnectionClientRequirement.to'=>'0000-00-00')),
        'Company'                           => array('primaryKey' => 'Company.id','foreignKey' => 'ConnectionClientRequirement.company_id'),
        'ConnectionClientRecruiter'         => array('primaryKey' => 'ConnectionClientRecruiter.client_id','foreignKey' => 'ClientView2.id'),        
        'Recruiter'                         => array('className'=>'CmsUser','primaryKey' => 'Recruiter.id','foreignKey' => 'ConnectionClientRecruiter.cms_user_id'),        
       
        'Countrie'                          => array('primaryKey' => 'Countrie.id','foreignKey' => 'ClientView2.countries_id'),        
        'Stat'                              => array('primaryKey' => 'Stat.id','foreignKey' => 'ClientView2.stat_id'),        
        'SettingEducation'                  => array('primaryKey' => 'SettingEducation.id','foreignKey' => 'ClientView2.dosazene_vzdelani_list'),        
        'Vzdelani'                          => array('className'=>'SettingCareerItem','primaryKey' => 'Vzdelani.id','foreignKey'=>'ClientView2.dosazene_vzdelani_list')
               
    );
    var $_joinSpec = array();
    
  /*
    var $belongsTo = array(
            'Countrie'=>array(
                'foreignKey'=>'countries_id'
            ),
            'Stat',
            'SettingEducation'=>array(
                'foreignKey'=>'dosazene_vzdelani_list'
            ),
           
    );
  */  
    
        
    
	/* - MAIN
	DROP VIEW if exists `fastest__client_views`;
	CREATE VIEW fastest__client_views AS  
		SELECT (
        CASE Client.mobil_active
         When 'mobil1' THEN Client.mobil1
         When 'mobil2' THEN Client.mobil2
         When 'mobil3' THEN Client.mobil3
        END
        ) as mobil,Client.mobil_active,Client.id, Client.import_stav, Company.client_manager_id as client_manager_id, 
        			Company.coordinator_id as coordinator_id, Company.coordinator_id2 as coordinator_id2, 
        			Company.self_manager_id as self_manager_id, 
        			Client.name, Company.name as company, ClientManager.name as client_manager, 
        			Coordinator.name as coordinator,Coordinator2.name as coordinator2, 
		        	Client.created, Client.updated, Client.stav, Client.kos, Client.express, Client.import_adresa,
                    GROUP_CONCAT(DISTINCT SettingCareerItem.name SEPARATOR '<br/>') as Profese,
                    Client.mobil1,Client.mobil2,Client.mobil3,
                     uk.stav as chybna_dokumentace,ConnectionClientRequirement.company_money_item_id,
                     Client.cislo_uctu,Client.cislo_uctu_control,Client.mesto,Client.datum_narozeni,
                     Client.countries_id,Client.externi_nabor,Client.email
        	FROM `fastest__clients` AS Client 
        		LEFT JOIN fastest__connection_client_career_items AS ConnectionClientCareerItem ON (ConnectionClientCareerItem.client_id = Client.id) 
        		LEFT JOIN fastest__setting_career_items AS SettingCareerItem ON (SettingCareerItem.id = ConnectionClientCareerItem.setting_career_item_id) 
        		LEFT JOIN `fastest__connection_client_requirements` as ConnectionClientRequirement ON (ConnectionClientRequirement.client_id = Client.id AND (ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1) AND ConnectionClientRequirement.to='0000-00-00')  
        		LEFT JOIN  fastest__companies as Company ON (ConnectionClientRequirement.company_id = Company.id) 
        		LEFT JOIN fastest__cms_users as ClientManager ON (Company.client_manager_id = ClientManager.id) 
        		LEFT JOIN fastest__cms_users as Coordinator ON (Company.coordinator_id = Coordinator .id) 
        		LEFT JOIN fastest__cms_users as Coordinator2 ON (Company.coordinator_id2 = Coordinator2 .id) 
        		LEFT JOIN fastest__company_money_items as cmi ON (cmi.id = ConnectionClientRequirement.company_money_item_id ) 
        		LEFT JOIN fastest__client_ucetni_dokumentace as uk ON (uk.connection_client_requirement_id = ConnectionClientRequirement.id AND uk.forma = cmi.name ) 
	GROUP BY Client.id
	*/

    
    /*  New FULL with Client - only for export
    
    	DROP VIEW if exists `fastest__client_views`;
	CREATE VIEW fastest__client_views AS  
		SELECT (
        CASE Client.mobil_active
         When 'mobil1' THEN Client.mobil1
         When 'mobil2' THEN Client.mobil2
         When 'mobil3' THEN Client.mobil3
        END
        ) as mobil, Company.client_manager_id as client_manager_id, 
        			Company.coordinator_id as coordinator_id, Company.coordinator_id2 as coordinator_id2, 
        			Company.self_manager_id as self_manager_id, 
        		    Company.name as company, ClientManager.name as client_manager, 
        			Coordinator.name as coordinator,Coordinator2.name as coordinator2, 
                    GROUP_CONCAT(DISTINCT SettingCareerItem.name SEPARATOR '<br/>') as Profese,
                    GROUP_CONCAT(DISTINCT SettingCertificate.name SEPARATOR '<br/>') as Certifikaty,
                    GROUP_CONCAT(DISTINCT Recruiter.name SEPARATOR '<br/>') as Recruiters,
                     uk.stav as chybna_dokumentace,ConnectionClientRequirement.company_money_item_id,
                    Client.*
        	FROM `fastest__clients` AS Client 
        		LEFT JOIN fastest__connection_client_career_items AS ConnectionClientCareerItem ON (ConnectionClientCareerItem.client_id = Client.id) 
        		LEFT JOIN fastest__connection_client_certifikaty_items AS ConnectionClientCertifikatyItem ON (ConnectionClientCertifikatyItem.client_id = Client.id) 
        		LEFT JOIN fastest__connection_client_recruiters AS ConnectionClientRecruiter ON (ConnectionClientRecruiter.client_id = Client.id) 
        		LEFT JOIN fastest__setting_career_items AS SettingCareerItem ON (SettingCareerItem.id = ConnectionClientCareerItem.setting_career_item_id) 
        		LEFT JOIN fastest__setting_certificates AS SettingCertificate ON (SettingCertificate.id = ConnectionClientCertifikatyItem.setting_certificate_id) 
        		LEFT JOIN `fastest__connection_client_requirements` as ConnectionClientRequirement ON (ConnectionClientRequirement.client_id = Client.id AND (ConnectionClientRequirement.type = 2 OR ConnectionClientRequirement.type = 1) AND ConnectionClientRequirement.to='0000-00-00')  
        		LEFT JOIN  fastest__companies as Company ON (ConnectionClientRequirement.company_id = Company.id) 
        		LEFT JOIN fastest__cms_users as ClientManager ON (Company.client_manager_id = ClientManager.id) 
        		LEFT JOIN fastest__cms_users as Coordinator ON (Company.coordinator_id = Coordinator .id) 
        		LEFT JOIN fastest__cms_users as Coordinator2 ON (Company.coordinator_id2 = Coordinator2 .id) 
        		LEFT JOIN fastest__cms_users as Recruiter ON (Recruiter.id = ConnectionClientRecruiter.cms_user_id) 
        		LEFT JOIN fastest__company_money_items as cmi ON (cmi.id = ConnectionClientRequirement.company_money_item_id ) 
        		LEFT JOIN fastest__client_ucetni_dokumentace as uk ON (uk.connection_client_requirement_id = ConnectionClientRequirement.id AND uk.forma = cmi.name ) 
	GROUP BY Client.id
    
    */
    
    /**
     * funkce obsluhujici status a jeho nastavnei pro 
     * potvrzeni ci vyvraceni podminky pro EN nabor
     */
    function get_ignore_status(){
        return $this->ignore_en_condition;
    }
    
    function set_ignore_status($status){
        $this->ignore_en_condition = $status;
    }
    
    
    function beforeFind($queryData){
        $this->_joinSpec = $this->joinSpec;//preneseni do TMP
        
        $queryData = parent::beforeFind($queryData);
        
        
        
        return $queryData;
        
    }
    
    
}
?>