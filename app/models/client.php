<?php
class Client extends AppModel {
    var $name = 'Client';
	var $useTable = 'clients';
    var $bind_en = false;
    private $ignore_en_condition = false;
    
    function __construct(){
        parent::__construct();
        
        //echo "init Client";
    }
	
	
	function beforeSave(){
		if (isset($this->data[$this->name]['jmeno']) && isset($this->data[$this->name]['prijmeni']))
			$this->data[$this->name]['name'] = $this->data[$this->name]['prijmeni'] . ' ' . $this->data[$this->name]['jmeno'];
			
            
        /**
         * tvorba cisla uctu 
         * Z pole Cu
         * format XXXXXX-XXXXXXXXXX/XXXX
         */    
         if (isset($this->data[$this->name]['Cu'])){
            $this->data[$this->name]['cislo_uctu'] = String::insert(
                ':prefix-:cislo_uctu/:kod_banky',
                array(
                    'prefix'=> $this->data[$this->name]['Cu']['prefix'],
                    'cislo_uctu'=> $this->data[$this->name]['Cu']['cislo_uctu'],
                    'kod_banky'=> $this->data[$this->name]['Cu']['kod_banky']
                )
            );
         }   
            

		return $this->data;
    }

    /**
     * funkce obsluhujici status a jeho nastavnei pro 
     * potvrzeni ci vyvraceni podminky pro EN nabor
     */   
    function set_ignore_status($status){
        $this->ignore_en_condition = $status;
    }


    function beforeFind($queryData){

        /**
         * pokud jsou nastaveny conditions pridej k nim podminky pro ATEP
         * videt pouze od interniho naboru
         * tedy externi_nabor = 0
         */
        if(isset($queryData['conditions']) && !$this->ignore_en_condition){
            $my_con = array(
                'externi_nabor' => 0
            );

            $queryData['conditions'] = am($queryData['conditions'],$my_con);
        }

        //pr($queryData);

        return $queryData;
    }



    function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
			     if(isset($item[$this->name]['mobil_active']))
			         $data[$key][$this->name]['mobil'] = $this->activni_mobil($item);
			
                /**
                 * rozparsovani cisla uctu pro editaci
                 * a pro vypis odstraneni pomlcky pokud nebyl vyuzito pole prefixu a 
                 * tedy je prvni znak pomlcka
                 */
                 if(isset($item[$this->name]['cislo_uctu'])){
                    $prefix = $cislo_uctu = $kod_banky = null;
                    
                    if(strpos($item[$this->name]['cislo_uctu'],'/') !== false){
                        list($cu,$kod_banky) = explode('/',$item[$this->name]['cislo_uctu']);
                    }
                    else{
                        $cu = $item[$this->name]['cislo_uctu'];    
                    }
                    
                    if(strpos($cu,'-') !== false){
                        list($prefix,$cislo_uctu) = explode('-',$cu);
                    }
                    else{
                        $cislo_uctu = $cu;    
                    }
                    
                    
                    $data[$key][$this->name]['Cu']['prefix'] = $prefix;
                    $data[$key][$this->name]['Cu']['cislo_uctu'] = $cislo_uctu;
                    $data[$key][$this->name]['Cu']['kod_banky'] = $kod_banky;
                    
                    
                    $data[$key][$this->name]['cislo_uctu'] = ltrim($item[$this->name]['cislo_uctu'],'-');
                 }
            
            }
		}
		return $data;
    }
    
 
    
    
    /**
     * @author Jakub Matus
     * @param $find data
     * @return mobil
     * 
     * Funkce vraci aktivne zvolene cislo v db
     */
    private function activni_mobil($data){  
        return $data[$this->name][$data[$this->name]['mobil_active']];       
    }

}
?>