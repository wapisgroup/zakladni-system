<?php
class EbCompanyAttachment extends AppModel {
    var $name = 'EbCompanyAttachment';
	
	function beforeSave(){
		if (isset($this->data[$this->name]['name']))
			$this->data[$this->name]['alias_'] = $this->createAlias($this->data[$this->name]['name']);
		return $this->data;
    }
}
?>