<?php
class  Setting extends AppModel {
    var $name = 'Setting';
		
	function beforeSave(){
		
		if (isset($this->data[$this->name]['value']) && (!isset($this->data[$this->name]['no_serialize']) || $this->data[$this->name]['no_serialize'] == false ))
			$this->data[$this->name]['value'] = serialize($this->data[$this->name]['value']);
		
		return $this->data;
	}
	
	function afterFind($data){
		if (isset($data) && count($data)>0 ){
			foreach ($data as $key=>$item){
				if (isset($item[$this->name]['value']) && (!isset($item[$this->name]['no_serialize']) || $item[$this->name]['no_serialize'] == false )){
					$data[$key][$this->name]['value'] = unserialize($item[$this->name]['value']);
				}
			}
		}
		
		return $data;
    }
	
}
?>