<?php
class CompanyOrderItem extends AppModel {
    var $name = 'CompanyOrderItem';
	
	function beforeSave(){	
		if (isset($this->data[$this->name]['imgs'])){
				$this->data[$this->name]['imgs'] = serialize($this->data[$this->name]['imgs']);
		}
		return $this->data;
    	}
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($data[$key][$this->name]['imgs'])){
					$data[$key][$this->name]['imgs'] = unserialize($data[$key][$this->name]['imgs']);
				}
			}
		}
		return $data;
    }
}
?>