<?php
class CompanyMoneyItem extends AppModel {
    var $name = 'CompanyMoneyItem';
	//var $useTable = 'company_work_money';
	
	function beforeSave(){
		if (
			isset($this->data[$this->name]['checkbox_pracovni_smlouva']) &&
			isset($this->data[$this->name]['checkbox_faktura']) &&
			isset($this->data[$this->name]['checkbox_dohoda']) &&
			isset($this->data[$this->name]['checkbox_cash']) &&
            $this->data[$this->name]['pausal'] == 0 &&
            $this->data[$this->name]['h1000'] == 0
		) {
			$this->data[$this->name]['name'] = $this->data[$this->name]['checkbox_pracovni_smlouva'].$this->data[$this->name]['checkbox_dohoda'].$this->data[$this->name]['checkbox_faktura'].$this->data[$this->name]['checkbox_cash'];
		}
        else if(isset($this->data[$this->name]['pausal']) && $this->data[$this->name]['pausal'] == 1) //novy pausal
            $this->data[$this->name]['name'] = '00001';
        else if(isset($this->data[$this->name]['h1000']) && $this->data[$this->name]['h1000'] == 1) //novy pausal
            $this->data[$this->name]['name'] = '1000H';    
        
		return $this->data;
	}
}
?>