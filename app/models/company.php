<?php
class Company extends AppModel {
    var $name = 'Company';
    public $company_data_for_email_notification = array(
       'id','name','self_manager_id','client_manager_id','coordinator_id',
       'coordinator_id2','manazer_realizace_id','mesto','internal_employee',
       'ulice','psc','datum_podpisu_smlouvy'
    );
	
	function beforeSave(){
		if (isset($this->data[$this->name]['name']))
			$this->data[$this->name]['alias_'] = $this->createAlias($this->data[$this->name]['name']);
		return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){

			}
		}
		return $data;
    }
    
    function company_list(){
        return $this->find('list',array(
            'conditions'=>array(
                'kos'=>0,
                'status'=>1
            ),
            'order'=>'name ASC'
        ));
        
    }
}
?>