<?php
class AtCompany extends AppModel {
    var $name = 'AtCompany';
    public $company_data_for_email_notification = array(
       'id','name','manager_id','mesto'
    );
	
	function beforeSave(){
		if (isset($this->data[$this->name]['name']))
			$this->data[$this->name]['alias_'] = $this->createAlias($this->data[$this->name]['name']);
		return $this->data;
    }
}
?>