<?php
class BonusRecruiterView extends AppModel {
    var $name = 'BonusRecruiterView';
	var $useTable = 'bonusb_recruiter_views';

	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($item[0]))
					$data[$key][$this->name] = am(@$data[$key][$this->name],$item[0]);
		
			}
		}
		return $data;
    }
	
}


/*
 *
 *
create VIEW `fastest__bonus_recruiter_views` as

SELECT
ml.id,
 h.year,
 h.month,
ml.mlm_group_id,
ml.closed,
ml.parent_id,
ml.cms_user_id,
cl.spec_id as spec_id,
cl.name as client_name,
co.name as company_name,
pozice.name as pozice_name,
user.name as recruiter_name,
user.id as recruiter_id,
user.cms_group_id as recruiter_cms_group_id,
rr.setting_career_item_id as profese_id,
re.from as datum_nastupu,
cc.name,
cc.prize,
h.stav,
cl.kos,
 h.connection_client_requirement_id,
  IFNULL(h.celkem_hodin,0) as celkem_hodin,

 (250 - IFNULL((Select IF(SUM(celkem_hodin)>=250,250,SUM(celkem_hodin)) as temp FROM fastest__client_working_hours as x WHERE year<=h.year AND month < h.month and  h.connection_client_requirement_id=x.connection_client_requirement_id GROUP BY connection_client_requirement_id),0)) as zbyva,

  IFNULL(IF(h.celkem_hodin > (250 - (Select SUM(celkem_hodin) as temp FROM fastest__client_working_hours as x WHERE year<=h.year AND month < h.month and  h.connection_client_requirement_id=x.connection_client_requirement_id GROUP BY connection_client_requirement_id HAVING temp <250 )),(250 - (Select SUM(celkem_hodin) as temp FROM fastest__client_working_hours as x 
WHERE year<=h.year AND month < h.month and  h.connection_client_requirement_id=x.connection_client_requirement_id 
GROUP BY connection_client_requirement_id 
HAVING temp <250 )),h.celkem_hodin),0) as vyplatit


FROM 
 fastest__client_working_hours as h 
LEFT JOIN fastest__companies co ON (co.id=h.company_id)
LEFT JOIN fastest__connection_client_requirements re ON (re.id=h.connection_client_requirement_id)
LEFT JOIN fastest__cms_users user ON (user.id=re.cms_user_id)
LEFT JOIN fastest__company_work_positions pozice ON (pozice.id=h.company_work_position_id)
LEFT JOIN fastest__clients cl ON (cl.id=h.client_id)
LEFT JOIN fastest__requirements_for_recruitments rr ON (rr.id=h.requirements_for_recruitment_id)
LEFT JOIN fastest__setting_career_cats cc ON (cc.id=rr.setting_career_cat_id)
LEFT JOIN fastest__connection_mlm_recruiters ml ON 
( ml.cms_user_id = user.id
   AND DATE_FORMAT(ml.created,'%Y-%m') <=concat(h.year,'-',if(h.month<10,concat(0,h.month),h.month)) AND     (DATE_FORMAT(ml.closed,'%Y-%m') >= concat(h.year,'-',if(h.month<10,concat(0,h.month),h.month)) OR  ml.closed='0000-00-00')
)

HAVING zbyva<>0 AND datum_nastupu >= '2009-09-01'

*/




?>