<?php
class ConnectionAuditEstate extends AppModel {
    var $name = 'ConnectionAuditEstate';
    public $standart_bind = array('AtCompany','Client','CompanyForeign','ClientForeign');
    public $spolecnost_klient_fields = array(
                    'IF(ConnectionAuditEstate.type = 1,AtCompany.name,IFNULL(CompanyForeign.name,"neuvedeno")) as spolecnost',
                    'IF(ConnectionAuditEstate.type = 1,Client.name,IFNULL(ClientForeign.name,"neuvedeno")) as klient'
    );
}
?>