<?php
class CompanyClientWorkingTemplateItem extends AppModel {
    var $name = 'CompanyClientWorkingTemplateItem';
    
    function beforeSave(){	
		if (isset($this->data[$this->name]['days'])){ 
		     $this->data[$this->name]['days'] = serialize($this->data[$this->name]['days']); 
        }
		return $this->data;
    }
	
	function afterFind($data){
		if (isset($data) && count($data)>0){
			foreach ($data as $key=>$item){
				if (isset($data[$key][$this->name]['days'])){ $data[$key][$this->name]['days'] = unserialize($data[$key][$this->name]['days']); }
			}
		}
		return $data;
    }
}
?>