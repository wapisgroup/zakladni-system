<?php
class AppModel extends Model {
    var $joinSpec = array();
	function __construct(){ 
         parent::__construct(); 
          
         static $changedSet = false; 

         if(!$changedSet){
              $changedSet=true;
            
              $this->query("SET NAMES 'utf8'");
         }
    }
	
	function createAlias($string){
		$trans = array(","=>"","á"=>"a", "ä"=> "a", "č"=>"c", "ď"=>"d", "é"=>"e", "ě"=>"e", "ë"=>"e", "í"=>"i", "&#239;"=>"i", "ň"=>"n", "ó"=>"o", "ö"=>"o", "ř"=>"r", "š"=>"s", "ť"=>"t", "ú"=>"u", "ů"=>"u", "ü"=>"u", "ý"=>"y", "&#255;"=>"y", "ž"=>"z", "Á"=>"A", "Ä"=>"A", "Č"=>"C", "Ď"=>"D", "É"=>"E", "Ě"=>"E", "Ë"=>"E", "Í"=>"I", "&#207;"=>"I", "Ň"=>"N", "Ó"=>"O", "Ö"=>"O", "Ř"=>"R", "Š"=>"S","Ť"=>"T", "Ú"=>"U", "Ů"=>"U", "Ü"=>"U", "Ý"=>"Y", "&#376;"=>"Y", "Ž"=>"Z"," "=>"-","."=>"-","/"=>"-","+"=>'-','"'=>'','?'=>'',';'=>'','&'=>'');
		return strtolower(strtr($string, $trans));
	}
	
	function generateLangList($lang = 'cz',$conditions = null,  $order = null, $limit = null){
 		$result = $this->findAll($conditions, null, $order, $limit);
 		$finalVals = array();
 		foreach ($result as $item){
 			$item[$this->name][$this->displayField][$lang];
 			$finalVals[$item[$this->name][$this->primaryKey]] = $item[$this->name][$this->displayField][$lang];
 		}
 		return $finalVals;
 	}
	
	function find($type, $options = array()) {  
        switch ($type) {  
             case 'superlist':  
                 if(!isset($options['fields']) || count($options['fields']) < 3) {  
                     return parent::find('list', $options);  
                 }  
   
                 if(!isset($options['separator'])) {  
                     $options['separator'] = ' ';  
                 }  
   
                 //$options['recursive'] = -1;                
                 $list = parent::find('all', $options);  
   
                 for($i = 1; $i <= 2; $i++) {  
                     $field[$i] = str_replace($this->alias.'.', '', $options['fields'][$i]);                 
                 }              
   
                 return Set::combine($list, '{n}.'.$this->alias.'.'.$this->primaryKey,  
                                  array('%s'.$options['separator'].'%s',  
                                        '{n}.'.$this->alias.'.'.$field[1],  
                                        '{n}.'.$this->alias.'.'.$field[2]));  
             break;                         
   
             default:                
                 return parent::find($type, $options);  
             break;  
         }  
     } 
     
     function beforeFind($queryData){
		  if (isset($this->joinSpec) && count($this->joinSpec) > 0){		
			
                /* na bindovani klasickych spojeni,
        		 * Zatim pouze pro belongsTo a hasOne
        		 */ 
        		$db =& ConnectionManager::getDataSource($this->useDbConfig);
        		$tablePrefix = $db->config['prefix'];
        		foreach($this->belongsTo as $modelClass => $bel){
        			$model = ClassRegistry::init($modelClass, 'Model');
					$inner_condition = (isset($bel['conditions'])?$bel['conditions']:array());
        			$foreignKey = $bel['foreignKey'];
        			$queryData['joins'][] = array(
        				'type' => 'LEFT',
        				'alias' => $model->name,
        				'table' => $db->name($tablePrefix . $model->useTable),
        				'conditions' => am(
							array($this->alias . '.' . $foreignKey => $db->identifier("{$modelClass}.{$this->primaryKey}")),
							$inner_condition
						),
        				'fields' => $bel['fields']
        			);		
        		}
		        $this->belongsTo = array();
                 
                foreach($this->hasOne as $modelClass => $bel){
        			$model = ClassRegistry::init($modelClass, 'Model');
        			$foreignKey = $bel['foreignKey'];
					$inner_condition = (isset($bel['conditions'])?$bel['conditions']:array());
        			$queryData['joins'][] = array(
        				'type' => 'LEFT',
        				'alias' => $model->name,
        				'table' => $db->name($tablePrefix . $model->useTable),
        				'conditions' => am(
							array($this->alias . '.' . $this->primaryKey => $db->identifier("{$modelClass}.{$foreignKey}")),
							$inner_condition
						),
        				'fields' => $bel['fields']
        			);		
        		}
		          $this->hasOne = array();  
                	
				foreach($this->joinSpec as $aliasModel => $model_arr){
					$className = $aliasModel;
					$conditions = array();
					$foreignKey = Inflector::tableize($this->name).'_id';
					$primaryKey = $this->primaryKey;
					$order		= $this->primaryKey.' ASC';
					$limit		= 100;
					$joinType	= 'LEFT';
					$useTable   = Inflector::pluralize(Inflector::tableize($aliasModel));
					$fields 	= null;
					
					extract($model_arr);
					$backEndModel = ClassRegistry::init($className, 'Model');
					
					$queryData['joins'][] = array(
						'type' 			=> $joinType,
						'alias'	 		=> $aliasModel,
						'table' 		=> $db->name($this->tablePrefix . $backEndModel->useTable),
						'conditions' 	=> am(
							array($foreignKey => $db->identifier("{$primaryKey}")),
							$conditions
						),
						'fields' => $fields
					);
				}
                $this->joinSpec = array();   
			
		}
		return $queryData;
	}
    
    
    
    /**
     * nove funkce pro vytahnuti listu
     * klasicky id, name
     * @param $con - array conditions, am with basic array
     * @param $order - classic order string, default is "name ASC"
     * @return array list
     * basic condition kos=0
     */
    function get_list($con = array(),$order = 'name ASC'){
         $this->query("SET NAMES 'utf8'");
        return $this->find('list',array(
            'conditions'=>am(array('kos'=>0),$con),
            'order'=>$order
        ));
    }
      
}
?>